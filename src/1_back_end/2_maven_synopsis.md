maven - строго определяет структуру каталогов проекта

* **Dependency**
  * groupId: org.spring - название разработчика (org.spring)
  * artefactId: spring-mvc - название структуры проекта + набор библиотек
  * version: 1.0.0-RELEASE
* **Commands**
  * `mvn archetype:generate` - команда показывает список всех архетипов в официальном репозитории
    * `mvn archetype:generate > mvn.txt` - перенаправить вывод в файл
  * `mvn -Dmaven.compiler.source=1.5 -Dmaven.compiler.target=1.5 clean install` - подмена опций при запуске
  * `mvn clean == mvn clean:clean`
* **Для выбора архетипа:**
  1. ввести `mvn archetype:generate`
  2. набрать номер проекта и нажать ввода
  3. ввести groupId, artefactId, version
  4. Получим в каталоге в котором находимся готовый проект
* **Steps**
   * mvn validate - проверка данных на корректность
   * mvn compile - компилирует данные
   * mvn test - выполняет тесты
   * mvn package - создание архива (jar, war). То есть записывает готовые архив в target
   * mvn install - переносить готовый скомпиленный архив проекта в локальный репозиторий maven (которые потом можно использовать)
   * mvn clean package - тоже что и mvn package, только сначало удаляет предыдущий скомпиленный проект (мусор)
   * mvn clean install - аналогично только для install
   * mvn deploy - помещает скомпиленный архив в заданный контейнер сервлетов???
* **Steps documentation**
  * mvn help:help
  * mvn site - создать документацию на базе javadoc (результат будет в папке проека в подкаталоге site)
  * mvn pdf:pdf - создать доку в формате pdf (результат будет в папке проека в подкаталоге pdf)
***
**Модули**  
Каждый module будет собран в свою jar `<moduleName>-<version>.jar` если проект разделен на модули, то предполагается что они собираются независимо
```
Стандартный набор модулей
ms-chassis-lib
    ms-chassis-lib-bom - dependencies, его нужно подключить как зависимость
        - pluginManagement в нем работать не будет 
    ms-chassis-lib-dependencies-parent - dependencies & maven plugins, его нужно наследовать
    ms-parent
    ms-chassis-lib-autoconfigure - для Spring @Configuration
    ms-chassis-lib-starter - для Spring Boot
    ms-chassis-lib-db - liquibase or other + liquibase plugin
    ms-example-lib-mq - xsd schema, generated classes, maven xsd plugin
    ms-chassis-lib-impl - code
ms-example
    ms-example-parent
    ms-example-api - shared интерфейсы для feign, dto, enum, constants
    ms-example-db - liquibase or other + liquibase plugin
    ms-example-mq - xsd schema, generated classes, maven xsd plugin
    ms-example-impl - code
```
***
**Каталоги**

```
# 4 основных
src/main/java - java код
src/main/resources - конфиги, картинки, языковые файлы и прочее
src/test/java - для тестов
src/test/resources - конфиги для тестов

# результат компиляции
target/ - для архивов
target/classes - для скомпиленных классов
target/site - для сгенерированной документации

# В веб приложении
src/main/webapp - для .jsp
src/main/webapp/WEB-INF - web.xml

```
***
Настройка `pom.xml`
```xml

```

Настройка локальных репозиториев `~/.m2/settings.xml`
```xml
<settings>

    <activeProfiles>
        <activeProfile>myprofile</activeProfile>
    </activeProfiles>
	
    <!-- имя и пароль для серверов -->
    <servers>
        <server>
            <id>repo-name-1</id>
            <username>myname</username>
            <password>pass</password>
        </server>
        <server>
            <id>repo-name-2</id>
            <username>myname</username>
            <password>pass</password>
        </server>
    </servers>

    <mirrors>
        <mirror>
            <id>maven-mirror-1</id>
            <mirrorOf>central</mirrorOf>
            <url>https://repo.maven.apache.org/maven2</url>
			<mirrorOf>repo-name-1</mirrorOf> <!-- Зеркало для repo-name-1 -->
        </mirror>
        <mirror>
            <id>maven-mirror-2</id>
            <mirrorOf>central</mirrorOf>
            <url>https://repo.maven.apache.org/maven2</url>
			<mirrorOf>*</mirrorOf> <!-- Зеркало для всех -->
        </mirror>
    </mirrors>
	
    <profiles>
        <profile>
            <id>myprofile</id>
            
            <!--
                настройки можно указывать только для профиля - не глобально,
                включаем загрузку исходников и документации автоматически, иначе можно через IDE
            -->
			<properties>
                <downloadSources>true</downloadSources>
                <downloadJavadocs>true</downloadJavadocs>
            </properties>

            <!-- указываем несколько локальных зеркал maven -->
            <repositories>
                <repository>
                    <id>repo-name-1</id>
                    <url>https://example.com.vtb/repository/repo-name-1</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </snapshots>
                </repository>
                <repository>
                    <id>repo-name-2</id>
                    <url>https://example.com.vtb/repository/repo-name-2</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </snapshots>
                </repository>
            </repositories>

            <!--
                указываем несколько локальных зеркал maven для плагинов
            -->
            <pluginRepositories>
                <pluginRepository>
                    <id>repo-name-1</id>
                    <url>https://example.com.vtb/repository/repo-name-1</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </snapshots>
                </pluginRepository>
                <pluginRepository>
                    <id>repo-name-2</id>
                    <url>https://example.com.vtb/repository/repo-name-2</url>
                    <releases>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </releases>
                    <snapshots>
                        <enabled>true</enabled>
                        <updatePolicy>always</updatePolicy>
                    </snapshots>
                </pluginRepository>
            </pluginRepositories>

        </profile>
    </profiles>
</settings>
```