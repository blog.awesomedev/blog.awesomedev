отключаем ssl в maven:
-Dmaven.resolver.transport=wagon -Dmaven.wagon.http.ssl.insecure=true -Dmaven.wagon.http.ssl.allowall=true -Dmaven.wagon.http.ssl.ignore.validity.dates=true

запуск maven локально без доступа к сети
    0 checksumPolicy в ignore в settings.xml
    0 удалить все файлы _remote.repositories в локальных репозиториях в .m2
    1 переключаем maven в offline (не обязательно)
        mvn dependency:go-offline
    2 запускаем оффлайн
        mvn clean install -Dnsu -Do -Dmaven.legacyLocalRepo=true


-- reset maven cache (NOT THE SAME AS IDEA CACHE)
mvn clean install -U

http://localhost:9069/swagger-ui.html


http://localhost:9069/actuator/health
http://localhost:9069/actuator/info
http://localhost:9069/actuator/conditions
http://localhost:9069/actuator/beans
http://localhost:9069/actuator/heapdump
http://localhost:9069/actuator/loggers - менять уровень логирования на лету

git fetch origin && git reset --hard origin/master
git merge --squash bugfix
git format-patch origin/master --stdout > mypatch.patch
git remote set-url origin ssh://git@url/dbo2/$project_dir.git

-- список авторо веток
git for-each-ref --format='%(color:cyan)%(authordate:format:%m/%d/%Y %I:%M %p)    %(align:25,left)%(color:yellow)%(authorname)%(end) %(color:reset)%(refname:strip=3)' --sort=authordate refs/remotes


-- run visualvm
1. start service
-Dcom.sun.management.jmxremote.port=62080
-Dcom.sun.management.jmxremote.ssl=false 
-Dcom.sun.management.jmxremote.authenticate=false
2. connect visualvm to localhost:62080

-- for debug increase feign timeout
feign.client.config.default.connectTimeout=60000
feign.client.config.default.readTimeout=60000
spring.datasource.hikari.connection-timeout=60000
spring.datasource.hikari.idle-timeout=60000
spring.jdbc.template.query-timeout=60000
spring.session.timeout=60000
server.reactive.session.timeout=30m
server.servlet.session.timeout=30m
spring.mvc.async.request-timeout=60000
server.tomcat.connection-timeout=60000
server.tomcat.keep-alive-timeout=60000
server.jetty.idle-timeout=60000
server.jetty.connection-idle-timeout=60000
server.jetty.threads.idle-timeout=60000ms
hystrix.command.default.execution.timeout.enabled=true
hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds=21000
ribbon.ConnectTimeout=2000
ribbon.ReadTimeout=2000
spring.kafka.retry.topic.attempts=1
-- by service name, e.g. my-service-name
my-service-name.ribbon.ConnectTimeout=2000
my-service-name.ribbon.ReadTimeout=2000

-- disable kubernetes locally
-Dspring.cloud.kubernetes.enabled=false
-Dspring.cloud.kubernetes.ribbon.enabled=false

-- configure ribbon
-Daccount.ribbon.listOfServers=localhost:9069
-Daccount.ribbon.ReadTimeout=5000

-- speed up start, disable hibernate table validation
-Dspring.jpa.hibernate.ddl-auto=none

-- dbo rest
-Ddbo.dictionary.logger.rest.enabled=true
-Ddbo.dictionary.logger.rest.maxPayloadLength=5000

-- dbo access control lib
-Dru.my.app.app2=debug
-Dru.my.app.app2.lib.acl.core=trace

-- dbo logswitcher
-Dlogging.level.ru.my.app.logswitcher=ERROR

-- data event prometheous metric
-Ddata.event.metrics.consumer.enabled=true
-Ddata.event.metrics.producer.enabled=true

-- java ssl
-Djavax.net.debug=ssl:all

-- spring
-Dlogging.level.ru.my.app=ERROR
-Dlogging.level.org.springframework.web=ERROR
-Dlogging.level.org.springframework.boot.autoconfigure=ERROR
-Dlogging.level.org.springframework=ERROR

-- transaction
-Dlogging.level.org.springframework.transaction.interceptor=TRACE

-- spring switch off logs
-Dlogging.level.=ERROR
-Dlogging.level.root=ERROR
-Dlogging.level.ru.my=ERROR

-- hibernate
-Dspring.jpa.properties.hibernate.show_sql=true
-Dspring.jpa.properties.hibernate.format_sql=true
-Dspring.jpa.properties.hibernate.generate_statistics=true
-Dlogging.level.org.hibernate.SQL=DEBUG
-Dlogging.level.org.hibernate.type=TRACE
-Dlogging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE
-Dspring.jpa.properties.hibernate.use_sql_comments=true
-Dspring.jpa.properties.hibernate.type=trace
-Dhibernate.criteria.literal_handling_mode=INLINE
-Dlogging.level.org.hibernate.hql.ast=DEBUG

-- hikari
-Dcom.zaxxer.hikari.housekeeping.periodMs=30000
-Dlogging.level.com.zaxxer.hikari.HikariConfig=DEBUG
-Dlogging.level.com.zaxxer.hikari=TRACE
-Dspring.datasource.hikari.leak-detection-threshold=20000

-- disable log
-Dlogging.level.root=ERROR
-Dlogging.level.ru.my=ERROR
-Dlogging.level.ru.my.app=ERROR
-Dlogging.level.org.springframework.web=ERROR
-Dlogging.level.org.springframework.boot.autoconfigure=ERROR
-Dlogging.level.org.springframework=ERROR
-Dlogging.level.ru.my.app.logswitcher=ERROR
-Dspring.liquibase.enabled=false
-Dlogging.level.org.hibernate=ERROR
-Dspring.jpa.generate-ddl=false
-Djms.enabled=false

-Dspring.jpa.hibernate.ddl-auto=none
-Dspring.jpa.properties.hibernate.cache.use_second_level_cache=false
-Dspring.jpa.properties.hibernate.cache.use_query_cache=false
-Dspring.main.lazy-initialization=true


-- kafka
# show topics on start up
-Dlogging.level.org.apache.kafka=DEBUG

-- p6spy
-Ddecorator.datasource.p6spy.enable-logging=true
-Ddecorator.datasource.p6spy.multiline=true
-Ddecorator.datasource.p6spy.logging=file
-Ddecorator.datasource.p6spy.log-file=c:\0_job\0_prjs\bitbucket\0_logs\account-p6spy.log
-Ddecorator.datasource.p6spy.tracing.include-parameter-values=true

-- spring-startup-analyzer
https://github.com/linyimin0812/spring-startup-analyzer

-javaagent:c:\0_soft\spring-startup-analyzer\lib\spring-profiler-agent.jar
-Dspring-startup-analyzer.admin.http.server.port=8066