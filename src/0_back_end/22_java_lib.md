List of necessary for all big project libraries.

* [manifold](https://github.com/manifold-systems/manifold) - various features like specific measurements, operator overriding, Tuple, Math functions, Preprocessor, extended Reflection API, String Templates, JSON and JSON Schema
  * Note. It is not for production yet
* [Project Lombok](https://projectlombok.org/)
* [MapStruct](https://mapstruct.org/)
* [Zalando Problem](https://github.com/zalando/problem) - convenient way to build json description of an error (exception)
* [Guava: Google Core Libraries for Java](https://github.com/google/guava) - collections & algorithms
* [Apache Commons](https://commons.apache.org/)
  * [Commons Lang](https://github.com/apache/commons-lang) - reflection api, StringUtils, collections utils, date utils
  * [Apache Commons Collections](https://github.com/apache/commons-collections) - collections
* [Vavr](https://github.com/vavr-io/vavr) - functional programming functions, alternative Stream API, Collections
  * Note. Mostly it is an Stream API alternative for old Java versions
* [Jackson](https://www.baeldung.com/jackson) - parse JSON to/from Object, XML to/from Object, convert XML to Json
  * Note. Built in Spring Boot
* Gson - like Jackson but more simple
* **xml parser libs**
  * comparison [1](https://stackoverflow.com/a/5665730). [2](https://www.baeldung.com/java-xml-libraries)
  * [DOM4J](https://mvnrepository.com/artifact/dom4j/dom4j) - popular, bi-directional read, CRUD, supports DOM, SAX, XPath, XLST
  * JDOM - works like DOM4J
  * JAXB (Xerces) - build-in in Java 10- (removed in jdk11), both direction navigation, write operations, more efficient then DOM, can map java Objects with xml (deserialize)
  * StAX - build-in in Java 6+
  * [Jaxen](https://mvnrepository.com/artifact/jaxen/jaxen) - xpath support
* [Hibernate Validator](https://hibernate.org/validator/)
* [Kryo](https://github.com/EsotericSoftware/kryo) - java object serserialization
* [jjwt](https://github.com/jwtk/jjwt) - to work with JWT, encode/decode/check
* DB
  * Hibernate ORM
    * [Hypersistence Optimizer](https://github.com/vladmihalcea/hypersistence-optimizer)
    * [Hibernate Utils](https://github.com/vladmihalcea/hypersistence-utils) (ex. Hypersistence Types)
    * [Hibernate Search](https://hibernate.org/search/) - auto push data to Apache Lucene/Elasticsearch/OpenSearch
    * [Hibernate Envers](https://hibernate.org/orm/envers/) & [Spring Data Envers](https://docs.spring.io/spring-data/envers/docs/current/reference/html/) - auto history tables fo Hibernate ORM
    * [Hibernate Reactive](https://hibernate.org/reactive/) - reactive API for Hibernate ORM
    * Blaze Persistence
      * [Blaze Persistence - Criteria API for JPA backends](https://persistence.blazebit.com/documentation/1.5/core/manual/en_US)
      * [Blaze Persistence - Entity View Module](https://persistence.blazebit.com/documentation/1.6/entity-view/manual/en_US/)
  * DB (JDBC) queries and connection pool spy and analyzers
    * [P6Spy](https://github.com/p6spy/p6spy) - adds ability to intercept and log sql queries, including interception of a most Connection, Statement and ResultSet methods invocations
    * [Datasource Proxy](https://github.com/ttddyy/datasource-proxy) - adds ability to intercept all queries and Connection, Statement and ResultSet method calls
    * [FlexyPool](https://github.com/vladmihalcea/flexy-pool) - adds connection pool metrics (jmx, codahale, dropwizard) and flexible strategies for adjusting pool size on demand
    * [Spring Boot DataSource Decorator](https://github.com/gavlyukovskiy/spring-boot-data-source-decorator) - includes configured P6Spy, Datasource Proxy, FlexyPool
  * [jOOQ](https://www.jooq.org/)
* **Framework**
  * [Apache Calcite](https://calcite.apache.org/) - parse SQL and optimize it to blocks, useful to make own DB or parse SQL to queries to another DB
  * [Apache Avro]() - serialization framework, uses JSON schema for data, serializes various types. It can be used by Apache Hadoop or Spring Cloud Stream Kafka
  * [Eclipse Vert.x](https://vertx.io/) - reactive web framework like Spring MVC
* **boilerplait**
  * [jhipster](https://www.jhipster.tech/) - auto generate microservice with API Gateway, Service Discovery/Registry, istio, ACL with API Gateway, Rate limiting