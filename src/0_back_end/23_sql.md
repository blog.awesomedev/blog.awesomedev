Это конспект по базам данных SQL и вообще принципу работы баз данных

- [Частые вопросы на собеседовании](#частые-вопросы-на-собеседовании)
- [TODO](#todo)
- [Типы данных](#типы-данных)
- [Примеры запросов SQL с поянением](#примеры-запросов-sql-с-поянением)
- [Materialized view \& Materialized column](#materialized-view-materialized-column)
- [Функции в DB](#функции-в-db)
  - [Aggregate function](#aggregate-function)
  - [Window function](#window-function)
- [Subquery (subselect): Types, Correlated subquery](#subquery-subselect-types-correlated-subquery)
- [ON UPDATE и ON DELETE (cascade операции)](#on-update-и-on-delete-cascade-операции)
- [MERGE or ON DUPLICATE KEY UPDATE (слияние)](#merge-or-on-duplicate-key-update-слияние)
- [UpSert](#upsert)
- [Invisible column](#invisible-column)
- [Compression of Table, Index, Column](#compression-of-table-index-column)
- [Computed Columns. Materialized column](#computed-columns-materialized-column)
- [Collation \& Encoding in SQL](#collation-encoding-in-sql)
- [Schema (Схемы)](#schema-схемы)
- [DDL, DML, DCL, TCL](#ddl-dml-dcl-tcl)
- [Тригеры (trigers)](#тригеры-trigers)
- [Why databases use ordered indexes but programming uses hash tables](#why-databases-use-ordered-indexes-but-programming-uses-hash-tables)
- [при сравнении неравно \<\> не найдется null значения](#при-сравнении-неравно-не-найдется-null-значения)
- [В composed key по нескольким полям null != null и допускаются дубли](#в-composed-key-по-нескольким-полям-null-null-и-допускаются-дубли)
- [Soft delete vs Hard delete](#soft-delete-vs-hard-delete)
- [Warming (прогрев)](#warming-прогрев)
- [Cold Storage vs Hot Storage](#cold-storage-vs-hot-storage)
- [Replication (Репликация)](#replication-репликация)
- [Migration (Миграция)](#migration-миграция)
- [Database normalization \& denormalization](#database-normalization-denormalization)
  - [Common](#common)
  - [Related table Denormalization with JSON column](#related-table-denormalization-with-json-column)
- [Foreign Data Wrappers (FDW) и dblink](#foreign-data-wrappers-fdw-и-dblink)
- [unique столбцы большого размера неэффективны](#unique-столбцы-большого-размера-неэффективны)
- [Derived Table](#derived-table)
- [Pagination in SQL](#pagination-in-sql)
- [Common Table Expressions, CTE](#common-table-expressions-cte)
  - [Common](#common-1)
  - [CTE, with recursive](#cte-with-recursive)
- [Limits](#limits)
- [Extensions (важные расширение функций реляционных DB)](#extensions-важные-расширение-функций-реляционных-db)
  - [Graph databases (графовые DB как расширение к реляционной DB)](#graph-databases-графовые-db-как-расширение-к-реляционной-db)
- [Нечеткий поиск, поиск по сходству, поиск по неточному совпадению, полнотекстовой поиск](#нечеткий-поиск-поиск-по-сходству-поиск-по-неточному-совпадению-полнотекстовой-поиск)
  - [Common](#common-2)
  - [Триграммный индекс или Поиск с опечатками](#триграммный-индекс-или-поиск-с-опечатками)
  - [Full Text Search, in Postgres](#full-text-search-in-postgres)
  - [fuzzystrmatch, Levenshtein distance](#fuzzystrmatch-levenshtein-distance)
  - [`unaccent` функция для поиска](#unaccent-функция-для-поиска)
- [Для чего Primary Key и Foreign Key?](#для-чего-primary-key-и-foreign-key)
- [В стандартном subquery нельзя использовать `order by`](#в-стандартном-subquery-нельзя-использовать-order-by)
- [При `distinct` поля из `order by` обязательно должны быть в `select` и про `distinct on`](#при-distinct-поля-из-order-by-обязательно-должны-быть-в-select-и-про-distinct-on)
- [Transactions (Транзакции) и все что к ним относится](#transactions-транзакции-и-все-что-к-ним-относится)
  - [О том как транзакции работают и их паттерны (перенести сюда)](#о-том-как-транзакции-работают-и-их-паттерны-перенести-сюда)
  - [Концепции](#концепции)
    - [ACID концепция](#acid-концепция)
    - [BASE концепция](#base-концепция)
    - [Закон Амдала и влияние многопоточности](#закон-амдала-и-влияние-многопоточности)
    - [CAP (теорема Брюера)](#cap-теорема-брюера)
    - [PACELC теорема](#pacelc-теорема)
  - [Locks (Блокировки)](#locks-блокировки)
  - [2PL (Two-Phase Locking)](#2pl-two-phase-locking)
  - [MVCC](#mvcc)
  - [Common](#common-3)
  - [MVCC vs 2PL и про то что MVCC даже с Serializable isolation level может не предотвратить Phantom Read](#mvcc-vs-2pl-и-про-то-что-mvcc-даже-с-serializable-isolation-level-может-не-предотвратить-phantom-read)
  - [Transaction Isolation level (кратко)](#transaction-isolation-level-кратко)
  - [Какой Transaction isolation level использовать и когда](#какой-transaction-isolation-level-использовать-и-когда)
  - [Transaction vs Lock](#transaction-vs-lock)
  - [Transaction Isolation level](#transaction-isolation-level)
- [Optimistic Lock SQL example](#optimistic-lock-sql-example)
- [Query plan (execution plan)](#query-plan-execution-plan)
- [Index](#index)
  - [Index tree](#index-tree)
  - [Index types](#index-types)
- [Основные типы JOIN](#основные-типы-join)
- [Partitioning](#partitioning)
- [Row Pattern Recognition](#row-pattern-recognition)
- [Scrolled cursors](#scrolled-cursors)
- [Списки инструментов](#списки-инструментов)

# Частые вопросы на собеседовании
* уровни изоляции транзакций
* Типы данных в SQL базах
* как работает `LIKE` и примеры
* Как работает `UNION` и `UNION ALL` и примеры
* `case` и пример
* Все типы `JOIN` и примеры
* SQL подзапрос и пример
* консистентность
* для чего в базах ключи и связи (соблюдение консистентности)
* индексы, один индекс по нескольким столбцам, несколько индексов в одной табл., в чем минусы индексов
* дерево индексов (подробно про принципы)
* уровни нормализации
* `select for update`
* написать запрос с having и агрегатной функцией (e.g. `count()`)
* where vs having
* having + subselect (e.g. удаление дублей), having + join (e.g. удаление дублей)
* sql для optimistic lock
* запросы с `WITH` (e.g. рекурсивный запрос)
* план запроса (`nested` / `merge` / `hash join`, `full table scan`)
* btree index vs hash index
* ACID, CAP
* data set scroll - редкий вопрос

# TODO
* sharding
* postgres sql transaction
* XA transaction
* postgres sql lock
  * `SELECT ... FOR UPDATE`
* Алгоритмы определения консенсуса, такие как Paxos и Raft https://github.com/hashicorp/raft
* Партиционирование https://habr.com/post/273933/
* версионирование `CREATE TABLE ... WITH SYSTEM VERSIONING`
* SAVEPOINT
* shadow tables и trails https://en.wikipedia.org/wiki/Shadow_table

# Типы данных

**Типы**
* `DATE` - format `YYYY-MM-DD`
  * **Даты в SQL вставляются в виде строк**
      ```sql
      INSERT INTO "Тест"
      VALUES(3, '2004-08-12');
      ```
* `DATETIME` - format: `YYYY-MM-DD HH:MI:SS`
* `TIME` - format: `YYYY-MM-DD HH:MI:SS`
* `TIMESTAMP` - format: `HH:MM:SS`, range: `-838:59:59 to 838:59:59`
  * **TIMESTAMP vs DATETIME**
    1. `TIMESTAMP` обычно используют для хранения истории дат изменений или логирования; DATETIME для хранения конкретно даты как данных.
    2. `TIMESTAMP` - **4 байта** в старых DBs и **4+ байта** в новых, `DATETIME` - **8 байт**.
       - **Note.** `TIMESTAMP` в **старых** DB ограничено `1970-2038` потому что **int** (4 байта), в новых DBs это исправлено.
    3. Поведение `TIMESTAMP` зависит от **time zone** сервера. Значение даты отображается в зависимости от текущего часового пояса приложения (к дате добавляется смещение часового пояса, отсчет от **00:00:00 UTC 1 января 1970 года** для **UNIX**). A `DATETIME` хранит как есть.
    4. `timestamp with time zone` - **в Postgres** хранит `datetime` с `timezone`, но не саму `timezone`, а перед сохранением отнимает/прибавляет разницу с Гринвичем и хранит как для Гринвича с `+0` часов, при `select` перед сравнением отнимает/прибавляет часовой пояс и сравнивает с тем что хранится.
    5. `timestamp without time zone` - аналог `timestamp` из **SQL standard** (e.g. в PostgreSQL)
    6. В PostgreSQL `timestamp` ведет себя как `datetime` и не хранит `timezone`, а типа `datetime` нету.
    7. **Note.** Хранить в DB дату+время **нужно только** в **without time zone** (без часового пояса), потому что сервер не знает время клиента и не может сам скорректировать часовой пояс при возврате данных. Сервер **DB** (e.g. Postgres) знает только **свой** часовой пояс и автоматически конвертирует в него, но это не имеет смысла для большинства случаев (т.к. локальное время сервера **DB** бесполезно для клиента делающего запрос).
* `YEAR` - format `YYYY` or `YY`
* `INTERVAL` - промежуток между двумя датами, хранит очень большие даты, можно сравнивать `interval` как числа (кто больше)
  * есть не во всех DBs
  * для задания `interval` можно определить между чем он хранит промежуток (month+year, month+day etc), в каждой DB свои правила
    ```sql
    SELECT INTERVAL '2h 50m' + INTERVAL '10m'; -- 03:00:00
    SELECT interval '1 day' + interval '1 hour'; -- interval '1 day 01:00:00'
    SELECT (DATE '2001-02-16', INTERVAL '100 days') OVERLAPS (DATE '2001-10-30', DATE '2002-10-30'); -- false
    ```
* `INT`
* `DEC(2,3)` (`DECIMAL`) - дробное.
  * `DEC` и `INT` типы записываются без апострафов, остальные с апострафом (включая даты)
    * `'Сэм\'ло'` - апостраф спец символ (или удвоением `'Сэм''ло'`)
* `CHAR`
  * **CHAR vs VARCHAR(n)** - на CHAR не тратит время CPU чтобы изменять длину, зато нету экономии в памяти.
* `VARCHAR(n)` - изменяет свою длину при вставке в нее значения с 0 до n (динамический массив)
  * **Note.** Можно использовать **без параметров** для длинных текстов
* `BLOB` бинарные данные (videos, images, documents, other)
  * размер большой и зависит от DB (гигабайты)
* `CLOB` очень большие текстовые данные
  * размер большой и зависит от DB (гигабайты)
  * **Note.** поведение отличается от `varchar` и может не так работать с кодировками

**Note!!!** Деньги нужно хранить в `DECIMAL(19, 4)`, при этом спец. тип `MONEY` не рекомендуется, потому что **может** иметь проблемы. `MONEY` можно использовать для интернациализации (culture-sensitive). Часто можно встретить и варианты: `DECIMAL(19, 2)` или `DECIMAL(19, 3)`.

**Note.** Сравнивать `datetime/timestamp` в большинстве DBs можно как числа `created_date <= '2013-12-04'`. Происходит авто преобразование `string` в `datetime/timestamp` на основе типа столбца `datetime/timestamp` в DB.  
Если дата хранится в строке, то нужно преобразовать ее вручную (название функций зависит от DBs):  
1 `CAST('20101231 15:13:48.593' AS DATETIME)` - из строки спец. формата в `DATETIME`  
2 `TO_DATE('2013-12-04')` - отсекаем часть с `TIME`  
3 `WHERE update_date >= '2013-05-03'::date AND update_date < ('2013-05-03'::date + '1 day'::interval)`

# Примеры запросов SQL с поянением
```sql
/* Создание базы */
CREATE DATABASE drink;

/* выбор для использования */
USE drink;

/* Например */
CREATE DATABASE IF NOT EXISTS drink;
USE drink;

/* Удаляет базу */
DROP DATABASE [IF EXISTS] db_name;

/*Удаление таблиц, можно удалить только когда все связи установленные через ALTER TABLE удалены*/
DROP TABLE "Sales";


-- many to one
create table user (
  id primary key,
  name varchar
);
create table user_address (
  id primary key, -- id тут не обязателен, но часто удобнее его иметь
  user_id, -- делаем foreign key уникальным, чтобы не было повторов
  -- NOT NULL можно добавить, если связь должна быть обязательна
  -- но с этим могут быть проблемы при использовании ORM библиотек или асинхронного получения даных e.g. в микросервисах
  -- user_id NOT NULL,
  address varchar,
  CONSTRAINT FK_user_address
  FOREIGN KEY (user_id)
  REFERENCES user (id);
);

-- one to one
-- Связи one to one не существует в большинстве DBs, ее эмулируют через one to many делая id foreign key уникальным
-- https://stackoverflow.com/a/15037461
create table user (
  id primary key,
  name varchar
);
create table user_address (
  id primary key, -- id не обязателен, но часто удобнее его иметь; e.g. есть один абстракный метод в коде для всех таблиц
  user_id UNIQUE, -- делаем foreign key уникальным, чтобы не было повторов
  -- NOT NULL можно добавить, если связь должна быть обязательна
  -- но с этим могут быть проблемы при использовании ORM библиотек или асинхронного получения даных e.g. в микросервисах
  -- user_id NOT NULL,
  address varchar,
  CONSTRAINT FK_user_address
  FOREIGN KEY (user_id)
  REFERENCES user (id);
);

-- many to many

/*Выборка NULL, обычным способом (=0 или =NULL) не выбирается*/
SELECT drink
FROM info
WHERE cat IS NULL;

/*LIKE*/
/*% - любые символы*/
/*_ - один любой символ*/
/*для сравнения с учетом регистра LIKE BINARY, НО по умолчанию регистр и так учитывается*/
SELECT *
FROM my_cont
WHERE location LIKE '%CA';

-- перевернутый like, например для сравнения значения с маской хранящейся в табл.
SELECT *
FROM my_cont
WHERE 'my_name' LIKE concat('%', name);

-- Note. like any и like all в разных DBs могут отличаться

-- like any. Пример из Postgres. Преобразуется в множество or
select * from table where value like any (array['%foo%', '%bar%', '%baz%']);

-- like all. Пример из Postgres. Преобразуется в множество and
select * from table where value like all (array['%foo%', '%bar%', '%baz%']);

-- как like any, из Posgres
select * from table where lower(value) similar to '%(foo|bar|baz)%';

/*
    REGEXP - регулярные выражения
    REGEXP является независимой от регистра для нормальных строк (т.е. строк не с двоичными данными)
*/
SELECT *
FROM pet
WHERE name REGEXP '^b';

/*
сравнение с учетом регистра в MySQL
для сравнения с учетом регистра используется REGEXP BINARY
*/
SELECT "a" REGEXP "A", "a" REGEXP BINARY "A"; -- -> 1 0

/*
Можно так проверять регулярки, без обращения к базе
*/
mysql> SELECT 'aXbc' REGEXP '[a-dXYZ]';                 -- -> 1
mysql> SELECT 'aXbc' REGEXP '^[a-dXYZ]$';               -- -> 0

/*Замена >=30 <=60*/
/*Можно использовать с буквами
WHERE BETWEEN 'Д' AND 'О' - все записи начинающиеся с Д, О и всех букв между ними;
*/
SELECT *
FROM drink
WHERE column_name BETWEEN 30 AND 60;

/*NOT следует за WHERE и используется с IN, BETWEEN, LIKE*/
SELECT name
FROM black
WHERE column_name NOT IN ('jon', 'sam');

-- Pairs with IN(...)
SELECT *
FROM PLAYERS
WHERE (First_Id, Second_Id) IN ((1,1), (1,2), (1,3))

SELECT *
FROM PLAYERS
WHERE (First_Id, Second_Id) IN (SELECT 1, 1 FROM SYSIBM.SYSDUMMY1 UNION ALL
                                SELECT 1, 2 FROM SYSIBM.SYSDUMMY1 UNION ALL
                                SELECT 1, 3 FROM SYSIBM.SYSDUMMY1)

-- Она считает число значений в данном столбце, или число строк в таблице.
-- Когда она считает значения столбца, она используется с DISTINCT чтобы производить счет чисел различных значений в данном поле.
SELECT COUNT (DISTINCT snum)
FROM Orders;

-- Чтобы подсчитать общее число строк в таблице, исполяьзуйте функцию COUNT со звездочкой вместо имени поля
-- COUNT со звездочкой включает и NULL и дубликаты, по этой причине DISTINCT не может быть исполяьзован. 
SELECT COUNT (*) FROM Customers;

/*
ВКЛЮЧЕНИЕ ДУБЛИКАТОВ В АГРЕГАТНЫЕ ФУНКЦИИ 
Агрегатные функции могут также ( в большинстве реализаций ) использовать аргумент ALL, который помещается перед именем поля
Различия между ALL и * когда они используются с COUNT -
* ALL использует имя_поля как аргумент.
* ALL не может подсчитать значения NULL. 
Пока * является единственным аргументом который включает NULL значения, и он используется только с COUNT; функции отличные от COUNT игнорируют значения NULL в любом случае. Следующая команда подсчитает(COUNT) число не-NULL значений в поле rating в таблице Заказчиков ( включая повторения ):
*/
SELECT COUNT ( ALL rating ) FROM Customers;

/*удаление всех записей с условием*/
DELETE
FROM clow
WHERE act = "scrym";

INSERT INTO clow(column1, column2)
VALUES('qwe', 'ert');

/*вставляет сразу все строки в таблицу из другой таблицы, можно из нескольких таблиц (это не подзапрос, а синтасис такой)*/
INSERT INTO "result"(отзыв, дата, имя_клиента)
SELECT "Отзывы".*, NULL /*имя клиента null*/
FROM "Отзывы"
WHERE "Отзывы"."Код клиента" = 3;

/*SELECT ... INTO - Вставляет данные в новую таблицу Persons_Backup*/
SELECT LastName,Firstname
INTO Persons_Backup
FROM Persons
WHERE City='Sandnes';

/*Можно и в другую базу данных*/
SELECT *
INTO Persons_Backup IN 'Backup.mdb'
FROM Persons;

/*Можно использовать основные операции*/
UPDATE dog
SET "type" = "type" + 3, "doc" = 'rrr' -- через запятую несколько полей
WHERE type = 'asd';

-- Обновление 1ой табл данными из 2ой (похоже на join). Пример для Postgres
update client c
set name = u.name
from user u
where c.id = u.id;

-- Обновление 1ой табл данными из 2ой (через join). Пример для MySQL
update client c
inner join user u
on c.id = u.name 
set c.name = u.name

-- Создание таблиц
CREATE TABLE "Отзывы"(
	"Код отзыва" INT NOT NULL IDENTITY PRIMARY KEY UNIQUE,
	"Код диска" INT NOT NULL,
	"Код клиента" INT NOT NULL REFERENCES "Клиенты",
	"Отзыв" VARCHAR(50) DEFAULT NULL,
	"Дата отзыва" VARCHAR(50) NOT NULL
);

CREATE TABLE "Отзывы"(
	"Код отзыва" INT NOT NULL IDENTITY,
	"Код диска" INT NOT NULL,
	PRIMARY KEY("Код отзыва")
);

CREATE TABLE "Отзывы"(
	"Код отзыва" INT NOT NULL IDENTITY,
	"Код диска" INT NOT NULL,
	CONSTRAINT FK_Заказ_Клиенты
	FOREIGN KEY ("Код диска")
	REFERENCES "Клиенты" ("Код клиента");
);

-- Изменение таблиц
ALTER TABLE "Заказ"
ADD CONSTRAINT FK_Заказ_Клиенты
FOREIGN KEY ("Код диска")
REFERENCES "Клиенты" ("Код клиента");

ALTER TABLE "Заказ"
ADD COLUMN cont INT NOT NULL IDENTITY FIRST,
ADD PRIMARY KEY (cont);

ALTER TABLE "Заказ"
ADD COLUMN cont VARCHAR(50)
AFTER first_name;
/* BEFORE FIRST LAST */

/*
CHANGE - имя и тип столбца
MODIFY - тип и позиция
ADD - добавление + тип
DROP - удаление столбцов
*/

/*переименование табл*/
ALTER TABLE proj
RENAME TO ropo;

/*переименование столбца в proj*/
ALTER TABLE "Заказ"
CHANGE COLUMN "number" "proj" INT NOT NULL IDENTITY,
ADD PRIMARY KEY("proj");

/* Увеличиваем тип, динну строки */
ALTER TABLE table_proj
MODIFY COLUMN proj VARCHAR(120);

ALTER TABLE table_proj
MODIFY COLUMN proj AFTER con_name;

ALTER TABLE table_proj
DROP COLUMN proj;

ALTER TABLE "Заказ"
DROP CONSTRAINT FK_Заказ_Клиенты;


ALTER TABLE Persons
ADD CHECK (P_Id>0);

/*Для создания нескольких CHECK используйте следующий синтаксис SQL*/
ALTER TABLE Persons
ADD CONSTRAINT chk_Person CHECK (P_Id>0 AND City='Sandnes');

/*Для MySQL*/
ALTER TABLE Persons
DROP CHECK chk_Person;

/*ASC - сортировка по возрастанию
DESC - убыванию(обратная по алфавиту)

order+by+10 - сортировка только 10 строк?
такое используется в инъекциях, чтобы узнать количество строк (если не вернуло ошибку, то строк именно столько)
пример http://www.descom.ch/main.php?id=18646644’+order+by+10 (возможно нужны пробелы)
*/
SELECT  title,  purchased
FROM  movie_table
ORDER  BY  title  ASC,  purchased  DESC;

SELECT SUM(sales)
FROM cook
WHERE name = "nik";/*Результат одно число*/

/*новый_столбец получает значение зависящее от др. столбцов*/
UPDATE dog
SET new_column =
CASE
	WHEN column1 = 2
		THEN val2
	WHEN column2 = 'qwe'
		THEN val3
	ELSE other_val
END;
/*WHERE условие можно писать после END*/

-- case ... then in SELECT
SELECT id, CASE WHEN myname IS NOT NULL THEN myname ELSE 'default' END AS mycolumnname
FROM t1;

-- case ... then in order by
/*Возвращается только одна запись title (о есть только разные названия)*/
SELECT title
FROM job
GROUP BY title
ORDER BY
  title, -- asc by default
  name1 desc nulls first, -- first/last, nulls will be first
  case name2
    when 'ivan' then 0
    when 'bob' then 1
    else 2 end desc, -- 'ivan' will be in the end, rest of sort will work as usual 
  name2 asc; -- (name2 again) can order by column even after ORDER BY CASE used the same column

-- case ... then in select and order by
-- Bypass SELECT+DISTINCT ERROR with ORDER BY CASE
-- ERROR: for SELECT DISTINCT, ORDER BY expressions must appear in select list
select distinct *,
  case user.name
      when 'ivan' then 0
      else 1
      end as orderByIvan -- need the alias in select
from user
order by
  orderByIvan desc, -- need the alias in select
  id asc;

SELECT SUM(sales)
FROM cook
GROUP BY name;/*Результат числа для групп*/

SELECT AVG(sales)
FROM cook
GROUP BY name;

SELECT MIN(sales)
FROM cook
GROUP BY name;

/*
WHERE - это ограничивающее выражение. Оно выполняется до того, как будет получен результат операции.

HAVING - фильтрующее выражение. Оно применяется к результату операции и выполняется уже после того как этот результат будет получен, в отличии от where.
Выражения WHERE используются вместе с операциями SELECT, UPDATE, DELETE, в то время как HAVING только с SELECT и предложением GROUP BY.

WHERE и HAVING можно использовать в одном запросе, тогда первым выполнится WHERE, а после фильтрации выполнится HAVING.

Например, WHERE НЕЛЬЗЯ использовать таким образом:
*/
SELECT name, SUM(salary)
FROM Employees
WHERE SUM(salary) > 1000
GROUP BY name;

/*В данном случае больше подходит HAVING:
*/
SELECT name, SUM(salary)
FROM Employees
GROUP BY name
HAVING SUM(salary) > 1000;

/*
То есть, использовать WHERE в запросах с агрегатными функциями нельзя, для этого и был введен HAVING.
*/
SELECT MAX(sales)
FROM cook
GROUP BY name;

/*Количество столбцов, NULL не считается*/
SELECT COUNT(sales)
FROM cook;

/*Количество столбцов в группе, NULL не считается*/
SELECT COUNT(sales)
GROUP BY name
FROM cook;

/*Количество НЕ повторяющихся столбцов*/
SELECT COUNT(DISTINCT sale)
FROM cook;

/*Только 2 записи начиная с пятой, нумерация с 0*/
SELECT COUNT(DISTINCT "sale")
FROM "cook"
LIMIT 5,2;

/*
  Операции над множествами в SQL
  UNION       - объединение
  INTERSECT   - пересечение
  EXCEPT      - вычитание
  Note. Дублирующиеся строки отфильтровываются, если не указано ALL
*/

-- UNION
/*Обьединение вывода разных таблиц*/
SELECT "title"
FROM "current"
UNION
SELECT "title"
FROM "job"
SELECT "title"
FROM "desired"
ORDER BY "title";/*Только одна сортировка на все UNION*/

/*Включая дубликаты*/
SELECT "title"
FROM "current"
UNION ALL -- не фильтровать дубли
SELECT "title"
FROM "job";
/*При обьединении INT и VARCHAR(20) он может быть преобразован в VARCHAR*/


-- INTERSECT
-- возвращает все строки, содержащиеся в результате и первого, и второго запроса
select name
from user
INTERSECT -- ALL отключит фильтр дублей
select name
from role;  -- вернет все строки где имя user совпадает с названием role

-- EXCEPT
-- возвращает все строки, которые есть в результате первого запроса, но отсутствуют в результате второго
select name
from user
EXCEPT -- ALL отключит фильтр дублей
select name
from role;  -- вернет все строки где имя user НЕ совпадает с названием role

/*
Подзапросы могут использоваться как
SELECT
SELECT список_столбцов
условия WHERE и FROM

И командах INSERT, DELETE, UPDATE, SELECT

Обычно возвращается одно значение, кроме IN
*/

SELECT mc.name,
(SELECT "state"
 FROM zip_code
 WHERE mc.zip_code = zip_code) AS "state"
FROM my_contacts mc;

-- default значение в случае null
-- замена null в результате select через COALESCE, например чтобы сортировать по правилу
select t1.id, COALESCE(t1.name, 'test')
from t1
order by t1.name

/*CHECK*/
CREATE TABLE Persons
(
	P_Id int NOT NULL CHECK (P_Id>0),
	LastName varchar(255) NOT NULL,
	FirstName varchar(255),
	Address varchar(255),
	City varchar(255)
)

CREATE TABLE Persons
(
	P_Id int NOT NULL,
	LastName varchar(255) NOT NULL,
	FirstName varchar(255),
	Address varchar(255),
	City varchar(255),
	CONSTRAINT chk_Person CHECK (P_Id>0 AND City='Sandnes')
)


-- вставка нескольких значений в insert into
INSERT INTO MyTable ( Column1, Column2 )
VALUES
	( Value1, Value2 ),
	( Value1, Value2 ),
	('Robert', 'Smith');

-- вставка нескольких значений в insert into (используя union all)
INSERT INTO MyTable  (FirstCol, SecondCol)
    SELECT  'First' ,1
    	UNION ALL
	SELECT  'Second' ,2
	    UNION ALL
	SELECT  'Third' ,3

-- вставка нескольких значений в insert into (из НЕСКОЛЬКИХ таблиц)
INSERT INTO c (aID, bID) 
     SELECT a.ID, B.ID 
     FROM A, B 
     WHERE A.Name='Me'
     AND B.Class='Math';

    -- или
	INSERT INTO other_table (name, age, sex, city, id, number, nationality)
	SELECT name, age, sex, city, p.id, number, n.nationality
	FROM table_1 p
	INNER JOIN table_2 a ON a.id = p.id
	INNER JOIN table_3 b ON b.id = p.id
	...
	INNER JOIN table_n x ON x.id = p.id
	
-- вставка нескольких значений в insert into (готовых)
INSERT INTO docs (id, rev, content) VALUES
  ('1', '1', 'The earth is flat'),
  ('2', '1', 'One hundred angels can dance on the head of a pin'),
  ('1', '2', `The earth is flat and rests on a bull\'s horn`),
  ('1', '3', 'The earth is like a ball.');

-- используем AS чтобы вставить значение по умолчанию ('NOT_VIEWED' as STATE)
-- также вставляет ВСЕ id из НЕСКОЛЬКИХ табл, как все-со-всеми. Т.е. id обеих табл будут вставлены
insert into REPORT_STATE
SELECT USER_INFO.ID as USER_ID,  REPORT.ID as REPORT_ID, 'NOT_VIEWED' as STATE
from USER_INFO, REPORT; -- тут выплонение для всех записей из USER_INFO, потом для всех REPORT (как цикл)

-- добавление нескольких столбцов за 1 команду
alter table
   table_name
add
   (
   column1_name column1_datatype column1_constraint, 
   column2_name column2_datatype column2_constraint,
   column3_name column3_datatype column3_constraint
   );


-- указание в update другой табл в качестве источника
UPDATE t1 SET c1=c1+1 WHERE c2=(SELECT MAX(c2) FROM t1);

-- update по условию, как join со связной табл
update ud
set assid = s.assid
from sale s 
where ud.id = s.udid;

-- Удалить все строки табл.
TRUNCATE [TABLE] tbl_name;


```

**Представления**
```sql
/*ПРЕДСТАВЛЕНИЯ*/

CREATE VIEW Londonstaff 
AS SELECT * 
FROM Salespeople 
WHERE city = 'London';

CREATE VIEW Bonus 
	AS SELECT DISTINCT snum, sname 
	FROM Elitesalesforce a 
	WHERE 10 < = 
	(SELECT COUNT (*) 
		FROM Elitesalestorce b 
		WHERE a.snum = b.snum); 

/*При изменении представлений запрос перенаправляется к таблицам из которых оно состоит и меняются таблицы*/
UPDATE Salesown 
SET city = 'Palo Alto' 
WHERE snum = 1004;

/*
ОБЪЕДИНЕНИЕ (UNION) и ОБЪЕДИНЕНИЕ ВСЕГО (UNIOM ALL) не разрешаются.
УПОРЯДОЧЕНИЕ ПО(ORDER BY) никогда не используется в определении представлений.
*/

DROP VIEW < view_name >

/*ОПРЕДЕЛЕНИЕ МОДИФИЦИРУЕМОСТИ ПРЕДСТАВЛЕНИЯ*/
/* Оно должно выводиться в одну и только в одну базовую таблицу.
/* Оно должно содержать первичный ключ этой таблицы ( это технически не предписывается стандартом ANSI, но было бы неплохо придерживаться этого).*/
/* Оно не должно иметь никаких полей, которые бы являлись агрегатными функциями.*/
/* Оно не должно содержать DISTINCT в своем определении.*/
/* Оно не должно использовать GROUP BY или HAVING в своем определении.*/
/* Оно не должно использовать подзапросы ( это - ANSI_ограничение которое не предписано для некоторых реализаций )*/
/* Оно может быть использовано в другом представлении, но это представле- ние должно также быть модифицируемыми.*/
/* Оно не должно использовать константы, строки, или выражения значений ( например: comm * 100 ) среди выбранных полей вывода.*/
/* Для INSERT, оно должно содержать любые пол основной таблицы которые имеют ограничение NOT NULL, если другое ограничение по умолчанию, не определено.*/

/*WITH CHECK OPTION - запросы на изменение представления которые всеравно исчезнут (где rating не = 300) теперь будут отклоняться*/
CREATE VIEW Highratings
	AS SELECT cnum, rating
	FROM Customers
	WHERE rating = 300
	WITH CHECK OPTION;

-- статья по VIEW и его поведение, например нельзя вставить значение стоблца если во VIEW оно отфильтровано в WHERE https://habrahabr.ru/post/47031/
```

**Процедуры и функции на примере Postgres**  
`Procedure` отличаются от `function` в **Postgres** только тем, что поддерживают **transaction** внутри себя.  
До **Postgres 11** `Procedure` не возвращали результат в отличии от `function`.
```sql

```

**ALL, ANY (SOME), EXISTS**
```sql
--Все записи где рейтинг большие всех записей
SELECT name
FROM "table"
WHERE rating > ALL (SELECT rating FROM "table2"
					WHERE rating > 3 AND rating < 7);
					
--Все записи где рейтинг больше хотя бы одного рейтинга из другой таблиицы
SELECT name
FROM "table"
WHERE rating > ANY (SELECT rating FROM "table2"
					WHERE rating > 3 AND rating < 7);

select sname
from студент
where EXISTS (select * from группа where gname='ПМ-21' and группа.gnum = студент.sgrp);

SELECT * FROM tbl WHERE id = ANY (ARRAY[1, 2]); -- like in(...), for non-null id only! 
SELECT * FROM tbl WHERE id <> ALL ('{1, 2}'); -- like in(...), for non-null id only! 
SELECT * FROM tbl WHERE id <> ALL (ARRAY[1, 2]); -- like not in(...), for non-null id only! 
SELECT * FROM tbl WHERE NOT (id = ANY ('{1, 2}')); -- like not in(...), for non-null id only! 

SELECT * FROM tbl WHERE (id = ANY ('{1, 2}')) IS NOT TRUE; -- like in(...), for NULL id also!

/*
--Замена IN на EXISTS--
Список студентов группы 'ПМ-21'. Второе это сравнение на первичный/вторичный ключ разных таблиц.
ТО ЕСТЬ можно использовать столбец из внешней таблицы внутри подзапроса.
*/
SELECT "имя_студента"
FROM "Студент"
WHERE EXISTS (SELECT *
  FROM "Группа"
  WHERE "Группа"."имя_группы" = 'ПМ-21'
  AND "Группа"."номер_группы" = "Студент"."номер_группы");
```

**В MySQL**
* `DROP SCHEMA` синоним `DROP DATABASE` в MySQL 5.0.2
* Если база данных удаляется, привеленгии её пользователей НЕ удаляются
* Если база данных по умолчанию удаляется, то база данных по умолчанию unset (сбрасывается использование командой use) а функция DATABASE() возвращает NULL

# Materialized view & Materialized column
Коротко - это view которое генерирует реальную табл, запрос к ней быстрее чем к обычному view.

# Функции в DB
## Aggregate function

**Aggregate function** - значения группы строк группируется в один результат.

* **MySQL**
  * **Advanced Functions**
    * CAST
    * `COALESCE` - подстановка **default** значения в случае **null**, check each param for **null** `SELECT COALESCE(NULL, NULL, 'third_value', 'fourth_value')`, returns`fourth_value`
    * IFNULL
  * **String Functions**
    * CONCAT
    * `CONCAT_WS` - тоже что и `CONCAT`, но можно задать разделители
    * UPPER
    * SUBSTR
    * STRCMP
    * SUBSTRING
    * SUBSTRING_INDEX
    * LENGTH
    * INSERT
    * POSITION
  * **Numeric Functions**
    * POW
    * COUNT
    * ABS
    * FLOOR
    * CEILING
    * RAND
    * SQRT
  * **Date Functions**
    * DATE
    * `NOW` - вернет текущую дату: `create table t1 ( createDate date default Now() )`
    * SYSDATE
    * DATE_ADD
* **PostgreSQL Functions**
  * `quote_literal(string text)` - делает literal из строки, чтобы ее можно было подставлять в SQL

## Window function
Источник: [1](https://en.wikipedia.org/wiki/Window_function_(SQL))

**Window function** - использует значение из одной или нескольких строк, чтобы вычислить условие и вернуть результат для каждой строки из запроса (много результатов для каждой строки). **Window** значит что запрос делиться на **windows**, для каждого **window** считается своя агрегатная функция.  
В отличии от **having** возвращает все строки, не уменьшает их количество.

```sql
select col1, col2, col3,
sum(col4) -- sum() всех значений col4, но только по столбцам в window
  over(
    partiotion by col1 -- делим на windows по одинаковым значениям col1
    order by col2 -- сортируем по col2, но только по столбца в window
    rows between current and 1 following -- количество строк до и после текущей
  ) as sum
from t1;
```

# Subquery (subselect): Types, Correlated subquery
Source: [1](https://data-flair.training/blogs/sql-subquery/), [2](https://en.wikipedia.org/wiki/Correlated_subquery)

**In short:**  
1) **Correlated subquery** - вложенный subquery зависит от результатов внешнего через сравнение через `=` которое в этом случае работает аналогично `in(...)` если бы был `join`  
2) **просто subquery** не сравнивает ничего с результатами внешнего `query`, с ним может быть сделан `join (select ...)` или `exists (select ...)` или он может стоять в `select ... (select ... limit 1) as subquery ...`  
```sql
-- Пример короткого селекта:
-- 1.
select *
from t1 
where (select name from t2) = t1.name;

-- Пример соотнесенного запроса:
select id, name
from t1
where exists (select 1 from t2 where t1.id = t2.t1_id);
-- аналогичный запрос через in:
select * from t1 where id in (select t1_id from t2);
-- аналогичный запрос через join:
select * from t1 join t2 on t1.id = t2.t1_id;
```

**In short 2:**  
1) Из subquery можно взять 1 значение, если он в **select** или **where** или **having** (для having можно тоже?)
```sql
-- select
select *, (select my_name from t2 where t1.id = t2.t1_id limit 1) as sq_name
from t1
order by sq_name;

-- where
select *
from t1
where (select id from t2) = t1.t2_id;
```
1) **Subquery** может возвращать 1+ столбец если он в **from** или **join**: `from (select ...) as sq` и `join (select ...) as sq`
```sql
-- 1. from
select *
from t1, (select id from ...) as t2
where t2.name1 = ... and t2.name2 = ...

-- 2. join
select *
from t1 join (select id from ...) as t2 on t1.id = t2.t1_id
where t2.name1 = ... and t2.name2 = ...
```
3) По subquery в select можно делать `order by`, берем **только 1но** из имен т.к. не можем сортировать по нескольким сразу! 
```sql
-- select, берем 1ое имя из всех имен т.к. каждая запись t1 имеет много имен в t2,
-- аналогично выбора 1 значения из вложенной коллекции т.к
select *, (select name from t2 where t1.id = t2.t1_id limit 1) as sq_name
from t1
order by sq_name
```
4) Нельзя использовать subquery в select в **where** т.к. на этапе выполнения where еще нет результата, но subselect можно использовать в order by
```sql
-- ERROR! Does not work in WHERE
select *, (select name from t2 where t1.id = t2.t1_id limit 1) as sq_name
from t1
where t1.name2=sq_name
```
Некоторые статьи делят subquery на типы.
* **Основные Types**
  * Correlated subquery (synchronized subquery, соотнесенный запрос)
  * Простые вложенные подзапросы (короткий селект)
* **Types:** - не обязательно заполминать
  * Single Row Subquery - return 0+ rows
  * Multiple Row Subquery - return 1+ rows
  * Multiple Column Subqueries - return 1+ row.columns, optionally it can be `select 1 ...`
  * Correlated Subqueries - subquery depends on its parent query
  * Nested Subqueries - We have queries within a query (inner and outer query).

# ON UPDATE и ON DELETE (cascade операции)
`ON UPDATE` и `ON DELETE` - устанавливают поведение полей таблицы связанных с родительскими полями (id <- id)

**Q.** На данный момент, у нас есть защита целостности данных на случай каких-либо манипуляций с таблицами-потомками, но что если внести изменения в родительскую таблицу? Как нам быть уверенными, что таблицы-потомки в курсе всех изменений в родительской таблице?  
**A.** MySQL позволяет нам контролировать таблицы-потомки во время обновления или удаления данных в родительской таблице с помощью подвыражений: ON UPDATE и ON DELETE. MySQL поддерживает 5 действий, которые можно использовать в выражениях ON UPDATE и/или ON DELETE.
* `CASCADE`: если связанная запись родительской таблицы обновлена или удалена, и мы хотим чтобы соответствующие записи в таблицах-потомках также были обновлены или удалены. Что происходит с записью в родительской таблице, тоже самое произойдет с записью в дочерних таблицах. Однако не забывайте, что здесь можно легко попасться в ловушку бесконечного цикла.
* `SET NULL`:если запись в родительской таблице обновлена или удалена, а мы хоти чтобы в дочерней таблице некоторым занчениям было присвоено NULL (конечно если поле таблицы это позволяет)
* `NO ACTION`: смотри RESTRICT
* `RESTRICT`:если связанные записи родительской таблицы обновляются или удаляются со значениями которые уже/еще содержатся в соответствующих записях дочерней таблицы, то база данных не позволит изменять записи в родительской таблице. Обе команды NO ACTION и RESTRICT эквивалентны отсутствию подвыражений ON UPDATE or ON DELETE для внешних ключей.
* `SET DEFAULT`:На данный момент эта команда распознается парсером, но движок InnoDB никак на нее не реагирует.

```sql
CREATE TABLE invoice (
  inv_id  INT AUTO_INCREMENT NOT NULL,
  usr_id  INT NOT NULL,
  prod_id  INT NOT NULL,
  quantity INT NOT NULL,
  PRIMARY KEY(inv_id),
  FOREIGN KEY (usr_id) REFERENCES usr(usr_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  FOREIGN KEY (prod_id) REFERENCES product(prod_id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT
);
```

# MERGE or ON DUPLICATE KEY UPDATE (слияние)
MERGE нету в MySQL - слияние двух таблиц. Т.е. insert или update из одной в другую
Вместо неё используется `INSERT ... ON DUPLICATE KEY UPDATE`

**merge** - это когда, если в добавляемой табл уже есть такое же значение в unique или primary столбце, то оно будет заменено на другое по правило которое написано после key update

**Пример:**
```sql
INSERT INTO t1 (a,b,c) VALUES (1,2,3)
ON DUPLICATE KEY UPDATE c=c+1;
```

# UpSert
Источник: [тут](https://wiki.postgresql.org/wiki/UPSERT)

**UPSERT** - это `update or insert`, если запись есть, то обновить по правилу иначе вставить. Похожа на **merge**.

```sql
-- пример
INSERT INTO tablename (a, b, c) values (1, 2, 10)
ON CONFLICT (a) DO UPDATE SET c = tablename.c + 1; -- обновить столбец по правилу

INSERT INTO students(id, firstname, lastname, gender, d_o_b, email)
VALUES (1516, 'Gerard', 'Woodka', 'M', 'January 27 1995', 'gerard_woodka@hotmail.com')
ON CONFLICT (id) DO NOTHING; -- ничего не делать
```

# Invisible column
Источник: [1](https://medium.com/@bbrumm/what-is-an-invisible-column-in-oracle-and-why-would-you-use-it-90a4c20a8430)

# Compression of Table, Index, Column
Источник: [1](https://docs.microsoft.com/en-us/sql/relational-databases/data-compression/enable-compression-on-a-table-or-index?view=sql-server-ver15)

# Computed Columns. Materialized column

**Computed Columns** аналогичные названия: generated, calculated, virtual, derived, вычисляемые столбцы

**Materialized column** аналогичные названия: stored column

**Computed Columns** (Вычисляемые столбцы) - можно задать правило по которому создается новый столбец в табл. на основе данных из других столбцов. Например так можно сделать поиск по части данных из разных столбцов используя только 1 столбец.

**Для чего:** например если нужен select по вычисленному на основе других столбцу. В некоторых DBs можно добавить **index** для **stored column**.

**Note.** Добавление **stored column** это один из способов **denormalization** DB ради ускорения запроса.

**Materialized Computed Columns** (stored columns) - тоже что и **Computed Columns**, но создает реальный столбец, а не генерирует каждый раз значение, запрос к нему может быть быстрее. Не все DBs поддерживают **Materialized Computed Columns** и в каждой он обьявляется по разному.

**Note.** В `postgres` **non-Materialized column** это **virtual column**, а **Materialized column** это **stored column**.

```sql
-- GENERATED ALWAYS AS - специфичная для PostgreSQL команда, в других DB другие
CREATE TABLE tbl (
  int1    int,
  int2    int,
  product1 bigint GENERATED ALWAYS AS (int1 * int2) STORED -- вычисляется при create и update
  product2 bigint GENERATED ALWAYS AS (int1 * int2) VIRTUAL -- вычисляется при select
);
```

# Collation & Encoding in SQL
Source: [1](https://stackoverflow.com/a/496361), [2](https://stackoverflow.com/a/2344130), [3](https://dev.mysql.com/doc/refman/5.7/en/charset-collate.html)

Character Sets - set of symbols and encodings  
**collation** - set of rules for comparing characters in a character set (set of language-specific rules)

**UTF-8 vs UTF-16 vs UTF-32**
* **UTF-8** has an advantage in the case where ASCII characters represent the majority of characters in a block of text, because UTF-8 encodes all characters into 8 bits (like ASCII). It is also advantageous in that a UTF-8 file containing only ASCII characters has the same encoding as an ASCII file.
* **UTF-16** is better where ASCII is not predominant, since it uses 2 bytes per character, primarily. UTF-8 will start to use 3 or more bytes for the higher order characters where UTF-16 remains at just 2 bytes for most characters.
* **UTF-32** will cover all possible characters in 4 bytes. This makes it pretty bloated. I ca not think of any advantage to using it.

**UTF-8 vs UTF-16 vs UTF-32**
* **UTF-8**: Variable-width encoding, backwards compatible with ASCII. ASCII characters (U+0000 to U+007F) take 1 byte, code points U+0080 to U+07FF take 2 bytes, code points U+0800 to U+FFFF take 3 bytes, code points U+10000 to U+10FFFF take 4 bytes. Good for English text, not so good for Asian text.
* **UTF-16**: Variable-width encoding. Code points U+0000 to U+FFFF take 2 bytes, code points U+10000 to U+10FFFF take 4 bytes. Bad for English text, good for Asian text.
* **UTF-32**: Fixed-width encoding. All code points take four bytes. An enormous memory hog, but fast to operate on. Rarely used.

**Пример работы с кодировками (CHARACTER SET) и COLLATE в SQL**
```sql
CREATE DATABASE db_name
CHARACTER SET utf8mb4
COLLATE utf8mb4_unicode_520_ci;

CREATE TABLE `employee` (
	`employee_id` BIGINT(10) NOT NULL AUTO_INCREMENT,
	`firstname` VARCHAR(50) NULL DEFAULT NULL
	PRIMARY KEY (`employee_id`)
)
COLLATE='latin1_swedish_ci'

-- Изменение кодировки, и collation:
ALTER DATABASE <database_name>
CHARACTER SET utf8
COLLATE utf8_unicode_ci;
		
-- Изменение для табл:
ALTER TABLE <table_name> CONVERT TO CHARACTER SET utf8 COLLATE utf8_unicode_ci;
ALTER TABLE <table_name> MODIFY <column_name> VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci;

-- Можно использовать только для конкретных запросов конкретный collation	
SELECT k
FROM t1
ORDER BY k COLLATE latin1_german2_ci;

SELECT MAX(k COLLATE latin1_german2_ci)
FROM t1;

SELECT k
FROM t1
GROUP BY k
HAVING k = _latin1 'Müller' COLLATE latin1_german2_ci;
```

# Schema (Схемы)
**Schema** - как namespace для БД - база.схема.таблица. В MySql схем нету и схемами там называются таблицы.

Обычно в DB которые поддерживают схемы есть схема по умолчанию, может называться `public` и если схема не указана, то таблицы будут создаваться в `public` схеме. На схемы как и на таблицы можно назначать пользователей, ролик и пр (прав доступа).

```sql
CREATE SCHEMA myschema;
CREATE TABLE myschema.mytable (
 ...
);
```

# DDL, DML, DCL, TCL
* DDL is Data Definition Language - it is used to define data structures: create table, alter table, ...
* DML is Data Manipulation Language - it is used to manipulate data itself: insert, update, delete, ...
* DCL is Data Control Language: GRANT, REVOKE
* TCL is Transaction Control Language: COMMIT, ROLLBACK, SAVEPOINT, SET TRANSACTION

# Тригеры (trigers)
**trigger** - хранимая процедура, выполнение которой можно повесить на события. Синтаксис зависит от БД.
Часто события это: INSERT, DELETE, UPDATE. Список событий зависит от БД.

1. **Schema-level triggers** - запускаются на действия с данными БД
   * After Creation
   * Before Alter
   * After Alter
   * Before Drop
   * After Drop
   * Before Insert
    ```sql
    -- Пример
    /* Триггер на уровне таблицы */
    CREATE OR REPLACE TRIGGER DistrictUpdatedTrigger
    AFTER UPDATE ON district
    BEGIN 
    insert into info values ('table "district" has changed');
    END;

    /* Триггер на уровне строки */
    -- FOR EACH ROW - ключевое слово, для каждой БД разное
    CREATE OR REPLACE TRIGGER DistrictUpdatedTrigger
    AFTER UPDATE ON district FOR EACH ROW
    BEGIN 
    insert into info values ('one string in table "district" has changed');
    END;
    ```
1. **System-level triggers** - на действия самой системой БД: вход, выход, запуск

# Why databases use ordered indexes but programming uses hash tables
Source: [1](https://www.evanjones.ca/ordered-vs-unordered-indexes.html)

пусто

# при сравнении неравно <> не найдется null значения

```sql
select * from t1 where t1.id <> 2; -- не найдет с id = null, но найдет все другие где <>2
```

# В composed key по нескольким полям null != null и допускаются дубли
**composed key** по нескольким полям **допускает дубли** если в нескольких полях стоит **null**.  
Т.к. в этом случае **null != null**.

**Note.** Можно сделать так чтобы null считался значением и дубли не допускались, это делается спец. выражениями DB.
```sql
-- Пример специфичен для Postgres

-- изначально корректны дубли с null
-- 1, 2, null
-- 1, 2, null
-- 1, 2, null

-- нужно выполнить ОБЕ команды
CREATE UNIQUE INDEX favo_3col_uni_idx ON favorites (user_id, menu_id, recipe_id)
WHERE menu_id IS NOT NULL;

CREATE UNIQUE INDEX favo_2col_uni_idx ON favorites (user_id, recipe_id)
WHERE menu_id IS NULL;

-- 1, 2, null
-- 1, 2, null -> ошибка, второй null теперь не допускается т.к. считается значением
```

# Soft delete vs Hard delete

**soft delete** - обычно называется процесс когда удаление из DB чего-то затратно по ресурсам (дерево в DB при удалении перераспределяет узлы и это затратно для больших DB). Вместо удаления в записи или меняется статус (поле) на `deleted = true` или даты срока действия ставятся ниже текущей (срок действия истек). **soft delete** используется для быстрого дебага и восстановления (хотя при этом пользователь может затереть данные через `update` и от этого **soft delete** не поможет).

**Note.** Для **soft delete** будет ограничение для `unique` столбцов, т.к. **soft deleted** (удаленная) запись (строка) может занять какое-то уникальное имя в поле табл. Одно из решений делать composed unique ключ по нескольким столбцам (напр. createDate + name).

**Note.** Один из подходов в **soft delete**: ставить полю `deleted` не `true / false`, а числовое или строковое значения: 1 - удалено для user и можно восстановить, 2 - удалено для user и нельзя восстановить, 3 - удалено для admin и видно для developer.

**hard delete** - соотв. это может быть полное настоящее удаление

**soft delete** vs **hard delete** - для **hard delete** дольше операции удаления (т.к. индекс перестраивается), но не сильно; для **soft delete** дольше операции фильтрации и выборки т.к. есть доп. условие проверки на `deleted = true` и в целом усложняются все запросы. Еще **soft delete** можно заменить на инструменты Аудита (сохранение истории изменения данных в спец. таблицах), напр. **Spring Data Envers** и **Hibernate Envers**. Еще вместо soft delete иногда делают табл. **Archive** куда переносят удаленные записи. Проблема **soft delete** в том что нужно каскадно удалить все дочерние записи и лишние связи.

**Плюс** в **soft delete** в том, что если, например, есть старая таблица со старыми даннными и часть этой старой табл. формируется из другой табл., то при изменении этой другой табл. мы указываем в первой табл. id старой копии данных, чтобы данные в табл остались такими как на момент изменения (e.g. старое решение принималось на основе старых данных и если изменить часть новых данных, то часть данных отчета изменится). **Note.** Возможно есть решения для этого с **Spring Data Envers**, я не смотрел.


# Warming (прогрев)

**Warming** (прогрев) - так называют процесс когда данные подгружаются в DB из других источников, чтобы потом эта DB была использованна в качестве кэша (это не совсем кэш, т.к. не выполняются алгоритмы устаревания, которые должны выполняться для кэша по определению и поэтому кэшем это называться не может, но для быстрого доступа к данным)

**Наприме**, в microservice architecture может использоваться копия таблиц из других сервисов, в которые другие сервисы **реплицируют** данные (например через **Kafka**) при каждом изменении своих данных. При первом обращении сервис смотрит данные в локальных табл., и если их там нету, то отправляет запрос в другой сервис.

# Cold Storage vs Hot Storage

**cold storage** - хранилище для "холодных" данных - это любые данные, которые относительно редко запрашивают (бекапы, медиаконтент, научные данные, архивы). **Зачем:** стоимость хранения информации ниже, времени доступа к данным выше. Это может быть и просто отдельная полноценная DB, но развернутая на медленном и долговечном диске (чтобы иметь доступ к `cold storage` при запросах в `hot storage`, как через прокси).

**Hot Storage** - обычная DB которая находится в использовании прямо сейчас, доступ к данным в реальном времени.

**Один из подходов:** Чтобы сделать что-то вроде кэша, когда при обращении в **hot storage** и если данных там нету запрос шел в **cold storage**. Нужно при запросе в **hot storage** если данные для архива сохранены в DB которая в **cold storage**, то делаем запрос через **Foreign Data Wrappers** (FDW) - обертку, которая переадресует запрос в **cold storage**.

# Replication (Репликация)

**Репликация** - копирование данных из табл. в табл. или из DB в DB.

**Репликация используется** для переноса данных из **hot storage** в **cold storage**, для архивирования данных, для переноса данных из другого приложения, чтобы использовать их в интеграции между приложениями etc. Если DB распределенная (разделена на nodes и запущена на разных серверах), то перенос данных при синхронизации nodes называется **репликацией**.

**Делается**
1. Через запуск чистого sql, например по расписанию (shedule)
2. Одноразовый запуск вручную
3. При использовании ORM библиотек можно делать в коде приложения через функции ORM

# Migration (Миграция)

**Migration** - перенос **данных** или **схемы** таблиц из одной DB в другую.

Может делать через инструменты такие как: [MySQL Workbench: Database Migration](https://www.mysql.com/products/workbench/migrate/) или [Liquibase](https://www.liquibase.org/)

# Database normalization & denormalization

## Common
Источник: [тут](https://en.wikipedia.org/wiki/Database_normalization), [тут](https://habr.com/ru/post/254773/), [тут](https://docs.microsoft.com/ru-ru/office/troubleshoot/access/database-normalization-description), [тут](https://metanit.com/sql/tutorial/2.1.php)

**Нормализация** - приведения структуры БД к виду, обеспечивающему минимальную логическую избыточность, и не имеет целью уменьшение или увеличение производительности или уменьшение или увеличение физического объёма базы данных. **Если коротко:** разделение большой табл. на мелкие со связями по ключам, чтобы соблюсти consistensy и избавиться от лишних данных и как следствие от лишних просмотров и обновлений данных в обмен на большее количество join (что тоже не всегда хорошо).

**Для чего Нормализация:** Уменьшает размер данных (избегает избыточности данных - **data redundancy**), что помогает избежать **update anomaly**. При изменении данных нужно менять только в 1ом месте (т.к. нету дублей). При проектировании DB позволяет проверить корректность. Убирает **inconsistent dependency** - когда атрибуты распологаются по смыслу не в своих столбцах, это делает поиск данных сложным т.к. они не там где надо (путь поиска не тот). **Note.** Аномалии возникают при операции с табл. если в них есть **data redundancy**

**Денормализация** - обратный процесс, иногда повышает производительность. Может улучшить план запроса и производительность т.к. не придется делать join множества табл. в обмен на увеличения размера табл. и замедление операций с ними.

**Способы денормализации:**
1. Денормализация путем **сокращения количества таблиц**. Лучше объединять в одну несколько таблиц, имеющих небольшой размер, содержащих редко изменяемую (как часто говорят, условно-постоянную или нормативно-справочную) информацию, причем информацию, по смыслу тесно связанную между собой.
2. Денормализация путём **ввода дополнительного поля в одну из таблиц**. При этом появляется избыточность данных, требуются дополнительные действия для сохранения целостности БД.

**Типы проектирования:**
1. **Восходящее проектировании** DB - группируем все задуманные атрибуты (поля) по сущностям (таблицам)
2. **Нисходящее проектирование** DB - сначало создаем сущности, а потом их атрибуты и связи

**Понятия:**
* **Функциональная зависимость** - если коротко это **many to one** связь, применяется для нормализации (функция **отображения** одного значения во многие из мат. анализа)
* **Детерминант** - это таблица (ее поле или поля) со стороны **many** в **many to one**. В нормализованной базе данных единственным детерминантом должен быть атрибут, который является первичным ключом. А все остальные атрибуты должны функционально зависеть от первичного ключа.
* **Атрибут** - свойство сущности (**поле таблицы**)
* **Домен атрибута** - множество возможных значений атрибута (**тип данных**)
* **Кортеж** - множество взаимосвязанных атрибутов (**строка таблицы**)
* **Отношение** - конечное множество кортежей (**таблица**)
* **Схема отношения** - конечное множество атрибутов, определяющих некоторую сущность (**структура таблицы**, набор полей таблицы)
* **Проекция** - отношение, полученное из заданного путём удаления и (или) перестановки некоторых атрибутов (результат **select или др**, после применения операция над табл.)
* **Нормальная форма** - требование, предъявляемое к структуре таблиц в теории реляционных баз данных для устранения из базы избыточных (**redundant**) **функциональных зависимостей** между атрибутами (полями таблиц).
* **Метод нормальных форм** - состоит в сборе информации о объектах решения задачи в рамках одного отношения и последующей декомпозиции этого отношения на несколько взаимосвязанных отношений на основе процедур нормализации отношений. (**нормализация**)
* **Цель нормализации** - исключить избыточное дублирование данных, которое является причиной аномалий, возникших при добавлении, редактировании и удалении кортежей (строк таблицы).
* **anomaly** (side-effects, **Аномалия**) - называется такая ситуация в таблице БД, которая приводит к противоречию в БД либо существенно усложняет обработку БД. Причиной является излишнее дублирование данных в таблице, которое вызывается наличием функциональных зависимостей от не ключевых атрибутов. (**inconsistent dependency** и **data redundancy**)
  * **Update anomaly** (Аномалии-модификации) - проявляются в том, что изменение одних данных может повлечь просмотр всей таблицы и соответствующее изменение некоторых записей таблицы.
  * **Deletion anomaly** (Аномалии-удаления) — при удалении какого либо кортежа из таблицы может пропасть информация, которая не связана на прямую с удаляемой записью.
  * **Insertion anomaly** (Аномалии-добавления) - возникают, когда информацию в таблицу нельзя поместить, пока она не полная, либо вставка записи требует дополнительного просмотра таблицы.

**Note.** Некоторые статьи говорят, что важны только **первые три** нормальные формы, остальные используются редко. И советуют не проектировать идеальные DB в ущерб функциональности. **Normalized** - называют именно **3NF** форму и выше.

**Уровни нормализации**
1. **1NF** - нет повторяющихся групп (столбцов с однотипными значениями), т.е. данные должны быть перечислены не столбцами, а строками (**Note.** т.е. в полях не должно быть сложных значений через запятую: Freids: John, Bob, Sam или отдельных новых столбцов по тому же принципу)
2. **2NF** - повторяющиеся данные вынесены в отдельные связанные табл., у табл есть id
3. **3NF** - нету **transitive dependencies**, т.е. если данные применяются в нескольких табл, то они выносятся в отдельную табл.
4. **EKNF**
5. **BCNF**
6. **4NF**
7. **ETNF**
8. **5NF**
9. **DKNF**
10. **6NF**

## Related table Denormalization with JSON column
Для ускорение **select** и уменьшения количества **join** можно перенести данные из **related table** в **column** родительской **table**.

**Note.** There is architectural pattern - divide application on **read & write** model.

Можно использовать **json** column вместо связанной **table**, там будут дубли (`denormalized data` из связной **table**), но не будет дополнительного **join**. Это **json** поле может существовать одновременно со связной **table**, тогда **update** этого **json** поля нужно делать всегда при **update** связной **table** (по `update event`).

# Foreign Data Wrappers (FDW) и dblink

Источник: [habr](https://habr.com/ru/company/postgrespro/blog/308690/), [fwd](https://www.postgresql.org/docs/current/postgres-fdw.html)

Наличие **FDW** зависит от DB.

**Foreign Data Wrappers** (FDW) - маппинг табл из другой **DB** в текущую (табл будет видна как в текущей DB), обертка которая переадресует запросы из одной DB в другую, т.е. SQL выполняется в 1ой DB, а результат достается из 2ой DB (куда переадресован запрос). **Используется** например для связи между **cold storage** и **hot storage**

**dblink** - проброс запроса/ответа из 1ой DB во 2ую DB

```sql
-- FDW
-- 1. устанавливаем расширение postrgres для FDW
CREATE EXTENSION mysql_fdw ;

-- 2. создаем сервер
CREATE SERVER mysql_server_data FOREIGN DATA WRAPPER mysql_fdw
  OPTIONS (host '127.0.0.1', port '3306');

-- 3. привязываем пользователя postrgres к пользователю mysql
CREATE USER MAPPING FOR user SERVER mysql_server_data
  OPTIONS (username 'data', password 'datapass');

-- 4. подключаем табл. из mysql к postgres
CREATE FOREIGN TABLE
  orders_2014 (
    id int,
    customer_id int,
    order_date timestamp)
  SERVER mysql_server_data
    OPTIONS (dbname 'data', table_name 'orders');

-- dblink
CREATE EXTENSION dblink;

SELECT dblink_connect('host=10.1.1.1
 user=user
 password=password
 dbname=oat');

SELECT dblink_exec('Update employee set is_done=true');

UPDATE my_table t SET  column_name=i.column_name, 
FROM  dblink('dbname=other_db','SELECT id, x_column_name FROM my_table')
  as i(id INT, column_name TEXT) where t.id=i.id;
```

# unique столбцы большого размера неэффективны

unique столбцы большого размера неэффективны. В MySQL выдается предупреждение если пытаться сделать столбец unique и он больше 900 байт

# Derived Table
Источник: [1](https://www.wiseowl.co.uk/blog/s360/sql-derived-tables.htm)

**Derived Table** - это таблица полученная подзапросом внутри запроса. Т.е. это табл. из области видимости  
`from (select ...)` выражения. Они не сохраняются в DB как объекты.

**Note.** Таблицы **Derived Table** не могут ссылаться на другие **Derived Table** в SQL-92 внутри `from`. Т.е. **Derived Table** это должны быть константами.

**Lateral Derived Table** - это **Derived Table** которые могут ссылаться на другие **Derived Table** внутри `from`.  
**Note.** Это просто понятие, в разных DBs поведение и определение **Lateral Derived Table** может быть разным.

```sql
select *
from (select user_role_id from role) -- derived table это (select user_role_id from role)
where user_role_id > 2;
```

```sql
-- Пример Lateral Derived Table ссылаются друг на друга.
select *
from LATERAL (select user_role_id from role) as role
from LATERAL (select user_item_id from item where role.id = item.role_id) as item
where user_role_id > 2;
```

# Pagination in SQL
Source: [1](https://vladmihalcea.com/query-pagination-jpa-hibernate/), [2](https://vladmihalcea.com/keyset-pagination-jpa-hibernate/), [3](https://vladmihalcea.com/join-fetch-pagination-spring/), [4](https://vladmihalcea.com/fix-hibernate-hhh000104-entity-fetch-pagination-warning-message/), [blaze criteria api](https://persistence.blazebit.com/documentation/1.6/core/manual/en_US/#pagination), [jooq](https://blog.jooq.org/faster-sql-paging-with-jooq-using-the-seek-method/), [jooq 2](https://blog.jooq.org/why-most-programmers-get-pagination-wrong/), [jooq 3](https://blog.jooq.org/stop-trying-to-emulate-sql-offset-pagination-with-your-in-house-db-framework/)

* **Problem - Count.** Каждый раз делаем `select count(*) ... where ...` в котором `where` имеет те же фильтры что и в `select * ... where ...`. Т.е. поиск выполняется 2 раза.
  * **Solution 1.** **Infinite Scroll** - не запрашивать **page**, а подгружать по мере **scroll** страницы пользователя.
  * **Solution 2.** `select` для **page** за границами количества записей `limit 9999 offset 10` вернет пустую page. Можно не возвращать **total** поле с **count**.
    * **Note.** Может зависеть от **DB**
* **Problem - Size.** При **join** и фильтре **по полям related tables** получаем большую **table** чем родительская к которой сделан `select`, к ней уже нельзя применить `limit ... offset ...`.  
  * **Solution 3. - 2d select.** уменьшить количество записей в `select`. **Pagination** обычно применяется только к **root table** к которой идет запрос (не к **related table** с которыми будет **join**)
    * `1)` Делаем `select id ... from ... join ... where ...`, получаем **id**(s) только тех **rows** которые подходят под фильтр `where`, `2)` делам сам запрос `select ... where id in (ids_1го_select) limit ... offset ...` полая **page** из фильтрованных записей.
    ```sql
    -- 1. выбираем id по фильтру и через in из них выбираем page
    -- в этом варианте дублируются join в 2х запросах
    select ...
    from t1 join t2 join ... -- те же join что и в subselect
    where id in (
      select id ... 
      from t1 join t2 join ... -- много join, те же что в root table
      where ...) -- фильтр в subselect
    limit ... offset ...

    -- 2. простой случай, фильтр только по полям родительской table
    select ...
    from t1 join t2 join ... -- делаем join чтобы получить связные табл (и сериализовать в Object)
    where id in (
      select id ... 
      from t1 ... -- НЕ ДЕЛАЕМ много join т.к. фильтр только по root table
      where ...) -- фильтр в subselect
    limit ... offset ...

    -- 3. 2 отдельных запроса, 1 - берем ids, 2 - подставляем в in
    -- неэффективен, но проще
    select id from ...
    select * from ... where in (ids) limit ... offset ...
    ```
  * **Solution 4. - Keyset Pagination/Seek Method** - используется только `limit`, не используется `offset`, при каждом запросе меняются условия `order by` так чтобы 1ая запись была следующей из новой **page**. Необходимы поля по которым можно строить `order by` для этого
    * **Pros.** Fast
    * **Cons.** Can not pick page number.
    ```sql
    -- where для каждой новой страницы меняется, оставляя только записи следующей page
    SELECT *
    FROM post
    WHERE (create_on, id) < ('2022-11-05 21:12:33', '123') -- новая страницы имеет create_on > N
    ORDER BY create_on DESC
    FETCH FIRST 50 ROWS ONLY
    ```
  * **Solution 5. - Scroll.** - открывается **transaction**, и пошагово наполняется `ResultSet`.
    * **Cons.** Transaction будет открыта все время запроса.
  * **Solution 6. - [dense_rank()](https://docs.oracle.com/cd/B19306_01/server.102/b14200/functions043.htm)/ROW_NUMBER()** - работает как **Keyset Pagination**. Использует `dense_rank()` - нумерует строки в **join** только часть относительно `order by`, т.е. только **parent table** `rows` (все половины **parent table** будут одинакогово **number** по `order by`). Через `where pageNum >= 1 and pageNum <= 10` выбираем только строки с определнным **number**.
    ```sql
    select p
    from Post p
    left join fetch p.comments pc
    where p.id in (
        select id
        from (
          select
             id as id,
             dense_rank() over (order by createdOn ASC) as ranking
          from Post
          where title like :titlePattern
        ) pr
        where ranking <= :maxCount -- where pageNum >= 1 and pageNum <= 10
    )
    ```


# Common Table Expressions, CTE
## Common
Источник: [1](https://docs.microsoft.com/ru-ru/previous-versions/sql/sql-server-2008-r2/ms190766(v=sql.105)?redirectedfrom=MSDN), [2](https://sqlite.org/lang_with.html#mathint)

**Common Table Expressions** (CTE, Обобщенные табличные выражения, ОТВ) - временные таблицы, которые не сохраняются в DB. На них может ссылаться сами на себя, а на них может ссылаться один и тот же запрос (использовать их результат).  
**Note!** После **1го** обращения к **CTE** из другого SQL запроса происходит **очистка CTE** (удаление). И больше обратиться к **CTE** нельзя.

**Для чего CTE:**
* Рекурсивных запросов
* Замены **View** когда она не нужно
* Более гибкий аналог **group by**
* Многократных ссылок на результирующую таблицу из одной и той же инструкции.

**Materialization Hints** - нестандартные **SQL hints** (для Postgress, SQLite) которые подксказывают планировщику как обработать `with as`.
* **AS MATERIALIZED** - создастся настоящая временная таблица, которая подставиться в SQL, т.к. таблица настоящая (а не подзапрос), то оптимизатор не сможет переписать запрос эффективно встроив его в основной запрос (т.к. это уже табл, а не запрос).
* **AS NOT MATERIALIZED** - обработать как **VIEW** или **SUBQUERY**, подстановка CTE в виде подзапроса, оптимизатор сможет переписать его эффективнее. Если эффективнее **AS MATERIALIZED**, то оптимизатор переключится на него.
  ```sql
  WITH w AS NOT MATERIALIZED (SELECT * FROM user)
  SELECT * FROM role JOIN user ON user.role_id = role.id;
  ```

**Примеры CTE:**
```sql
-- Определение CTE
WITH expression_name [ ( column_name [,...n] ) ] -- column_name не обязателен, если имена уникальны!
AS
( CTE_query_definition )

SELECT <column_list> -- обращения к CTE
FROM expression_name;


-- пример 1. простой CTE
WITH Sales_CTE (SalesPersonID, SalesOrderID)
AS (
    SELECT SalesPersonID, SalesOrderID
    FROM Sales.SalesOrderHeader
    WHERE SalesPersonID IS NOT NULL
)

SELECT SalesPersonID, COUNT(SalesOrderID) AS TotalSales -- обращение к CTE
FROM Sales_CTE
GROUP BY SalesPersonID
ORDER BY SalesPersonID;

select * from Sales_CTE; -- ОШИБКА, к CTE уже обращались в другом SQL и оно авто удалилось!
```

## CTE, with recursive
Source: [1](https://habr.com/ru/post/269497/)

**WITH RECURSIVE** (пример для Postgres) — на самом деле это не рекурсия, а вычисление чего-то итерациями до того, как будет выполнено некоторое условие.

**Ограничение.** `WITH RECURSIVE` нельзя использовать **в подзапросах**, можно только с **join**.

**Note.** В разных DBs может отличаться или не существовать.

**Как работает:** как **union** или **union all**, объединяет результат с результатом первой части (anchor), а потом со всеми следующими результатами.

**Для чего:** Для табл. со связыми с самой собой. **E.g.** взять все group с родительской табл. group рекурсивно (все подгруппы группы).

```sql
-- Пример 1.
CREATE TABLE geo (
    id int not null primary key, 
    head_id int references geo(id),  -- рекурсивная связь с этой же табл.
    name varchar(1000)
);

with recursive mytable_recursive(id, head_id, level, somecolumn) as ( -- columns
    -- "anchor" - first part of recursion
    select id, head_id, 0 as level, somecolumn -- 0 is defaule level
    from myschema.mytable
    -- where head_id is null -- search only if 

    union all -- or UNION to remove duplicates

    -- "recursive part" that will be UNION
    select b.id, b.head_id, level + 1, coalesce(b.somecolumn, br.somecolumn)
    from myschema.mytable b
    join mytable_recursive br
    on b.head_id = br.id -- search UP, reverse condition (b.id = br.head_id) for DOWN
)
select id, head_id, max(level) as level, somecolumn -- run search
from mytable_recursive
where somecolumn is not null or head_id is null -- if FIRST value is found or no head row then stop
WHERE level <= 2 ; -- check level if needed
group by id, head_id, somecolumn; -- leave only last recursion step rows (last level)
```

# Limits
Source: [sqlite](https://www.sqlite.org/limits.html)

(добавить сюда)

Максимальное количество:
* Записей для выполнения insert
* Таблиц в join
* Длинны строк
* Max количество столбцов в табл
* Макс количество параметров в запросе

# Extensions (важные расширение функций реляционных DB)

## Graph databases (графовые DB как расширение к реляционной DB)

Источник: [тут](http://leehyungjoo.com/2018/08/20/introduction-to-agensgraph/)

В некоторых реляционные DB можно подключать функции (расширения) для поддержки графов и работать с ними по аналогии с реляционной DB совмещая при этом таблицы и графы.  
**Note.** В PostgreSQL это расширение [AGE (AgensGraph-Extension)](https://www.postgresql.org/about/news/2050/) с поддержкой специального языка запросов [openCypher](https://www.opencypher.org/)

```sql
-- создаем вершину vertex
CREATE VLABEL person;

-- создаем ребро edge
CREATE ELABEL knows;

-- создаем вершины и ребро между ними
CREATE (:person {id: 1, name: 'James', age: 34, gender: 'male'});
CREATE (:person {id: 2, name: 'Jane', age: 28, gender: 'female'});
MATCH (p1: person {id: 1}), (p2: person {id: 2}) CREATE (p1)-[:knows]->(p2);

-- select
SELECT friend.id, count(comments)
FROM
(MATCH (:person {id: 1})-[:knows*1..2]->(friend) 
RETURN friend.id AS id) friend, comment - a relational table
WHERE
friend.id = comment.creatorId AND
comment.createdate > '2016.1.1';
```

# Нечеткий поиск, поиск по сходству, поиск по неточному совпадению, полнотекстовой поиск
## Common
Source: [1](https://habr.com/ru/articles/114997/)

[fuzzy string searching](https://en.wikipedia.org/wiki/Approximate_string_matching) (approximate string matching, Нечеткий поиск) - поиск похожих слов по каким-то правилам

**full text search** (полнотекстовой поиск) - поиск в массиве текста, использует разные алгоритмы **fuzzy string searching**

**Популярные алгоритмы в практике:** `Levenshtein distance` (чаще всего используется), `trigram` (вариант n-gram, как простая альтернатива для `Levenshtein distance`). `trigram` и `Levenshtein distance` встроены в **Postgres** как функции с возможностью конфигурации и подключения словарей.

* Алгоритмы
  * popular - популярные
    * Trie (prefix tree, digital tree) - каждый узел это прошлый символ(ы) + 1 следующий символ, поиск по совпадению как по проход по узлам
    * Levenshtein distance
      * Damerau–Levenshtein distance
    * Bitap
    * [n-gram](https://en.wikipedia.org/wiki/N-gram)
      * [trigram](https://en.wikipedia.org/wiki/Trigram)
  * other
    * BKTree
    * Jaro–Winkler similarity


## Триграммный индекс или Поиск с опечатками

<small>Источник: [тут](https://habr.com/ru/post/78566/)</small>

Каждое слово делится на трехбуквенные сочетания – **триграммы**.

`Пушкин == __п, _пу, пуш, ушк, шки, кин, ин_, н__`

При поиске похожей фразы, ищутся одинаковые триграммы. Чем больше равных триграмм, тем больше фраза схожа с исходной.

**Для чего:** поиск похожий товаров или вариантов слова если сделана опечатка. И/Или для полнотекстового поиска (как **full text search**, ) - поиск по всему тексту, а не только имени.

**Note.** На самом деле данный алгоритм по определению не **full text search**, но иногда его могут так ошибочно называть.

```sql
-- Note. Пример специфичен для PostgreSQL
-- создаем триграмный индекс на поле
CREATE INDEX "tbl_words_trgm_idx" ON "public"."tbl_words"
USING gist ("word" "public"."gist_trgm_ops"); -- или GIN index

-- выдаст похожие слова и их rate - уровень совпаления, сортируем чтобы самые похожие были сверху
select word 
from tbl_words 
where word % 'Пужкин' -- % - оператор поиска триграмма, можно использовать LIKE (это аналог)
-- WHERE t ~ '(foo|bar)' -- поиск по regular expression
order by similarity(word, 'Пужкин') desc, word 
limit 5
```

## Full Text Search, in Postgres
<small>Источник: Обзор алгоритмов ([1](https://habr.com/ru/post/341142/)), про full text search ([1](https://www.postgresql.org/docs/current/textsearch.html), [2](https://docs.microsoft.com/ru-ru/sql/relational-databases/search/full-text-search?view=sql-server-ver15))</small>

**Full Text Search** (FTS) - поиск по неточному совпадению в тексте. **Может** (если настроить) искать по формам слов: синонимам, корням слов, использовать стоп-слово исключенное из индекса FTS etc. Поддержка разных языков в тексте требует отдальной настройки. Для FTS нужно создать по столбцу особый индекс FTS.

**Note.** В разных DBs функции FTS разные, стандарта нет.
**Note.** Обычный **like** работает намного медленнее FTS даже если у столбца есть **index**.

**Как FTS работает (в Postgres):**
1. Парсинг текста на разные классы токенов (numbers, words, complex words, email addresses etc), чтобы сравнивать их по разным правилам. Используется стандартный парсер, но можно обьявить свой.
2. Конвертация tokens в lexemes. Lexeme - это normalized токен, основы слов без суфиксов/префиксов, в нижнем регистре, поиск по фразам или др. Для конвертации используется спец. словарь. Индексируются именно lexemes. Можно подключить свой словарь.
3. Для FTS хранится текст из normalized слов и количества слов (позиций) в тексте для rank (поиску с весами), чтобы поставить текст с большей "плотностью" (dense) совпадающих слов выше. Словари можно объеденять в цепочку (от специяичного -> к общему) для фильтрации по этапам.

**Note.** В **Postgres** для FTS есть **2 типа index**. Эти index не обязательны для FTS, но они ускоряют работу.  
**GIN index** - предпочтителен, хранит слова (lexemes) со ссылками на них в тексте. Не хранит веса (weight), поэтому использует перепроверку табл (recheck), если запрос использует веса слов. Производительность создания индекса GIN можно увеличить - увеличив объем памяти.  
**GiST index** - у него бывают ложные срабатывания (он **lossy**, false match), хранит текст как хэш, где каждое слово - bit (n-bit string). Когда оба слова попадают на ту же bit position происходит false match. В случае коллизии происходит чтение настоящей записи и сравнение по тексту, это затратно. Поэтому GiST не всегда эффективен. Эффективность зависит от количества уникальных слов текста.
```sql
CREATE INDEX name ON table USING GIN (column);
CREATE INDEX name ON table USING GIST (column);
```

**Note.** В **Posgress** для FTS используется ф-ции
1. **to_tsvector()** - для поиска по уже нормализованному тексту с разделителями (`&` и `' '`)
2. **plainto_tsquery()** - нормализует неформатированный текст, между нормализованными словами ставятся `&`
3. **phraseto_tsquery()** - как **plainto_tsquery()**, но использует `<->` (ПРЕДШЕСТВУЕТ) вместо `&`, это полезно при поиске точных последовательностей лексем (т.к. оператор фразового поиска поддерживает порядок лексем)

**Пример FTS в Postgres**
```sql
-- Простой FTS в Postgres, где @@ - оператор FTS
SELECT title
FROM pgweb
WHERE to_tsvector(title || ' ' || body) @@ to_tsquery('create & table') -- поля title + body
ORDER BY last_mod_date DESC
LIMIT 10;

-- plainto_tsquery() для поиска по многословному тексту
SELECT a.* FROM tableA a, tableB b WHERE a.indexed_text_field @@ plainto_tsquery(b.words);

-- аналог plainto_tsquery() через to_tsvector(), заменяем пробелы на &, что делит их на токены
SELECT a.* FROM tableA a, tableB b WHERE a.indexed_text_field @@ to_tsquery(replace(b.words, ' ', '&'));

-- создаем FTS index по столбцам для поиска
CREATE INDEX textsearch_idx ON pgweb USING GIN (textsearchable_index_col);

-- или FTS index на несколько столбцов по правилу
CREATE INDEX pgweb_idx ON pgweb USING GIN (to_tsvector('english', title || ' ' || body));

-- ищем по FTS index
SELECT title
FROM pgweb
WHERE textsearchable_index_col @@ to_tsquery('create & table')
ORDER BY last_mod_date DESC
LIMIT 10;

-- Получаем в Postgres тип to_tsvector - все результаты с весами 
SELECT to_tsvector('english', 'a fat  cat sat on a mat - it ate a fat rats');
-----------------------------------------------------
 'ate':9 'cat':3 'fat':2,11 'mat':7 'rat':12 'sat':4
```

## fuzzystrmatch, Levenshtein distance
Источник: [1](https://en.wikipedia.org/wiki/Levenshtein_distance), [2](https://www.postgresql.org/docs/9.1/fuzzystrmatch.html)

**fuzzy search** (fuzzystrmatch) - ищет неточное совпадение в отличии от FTS, которая ищет точное (но по нормализованному слову). Для реализации используются разные алгоритмы. Можно совмещать **fuzzy search** и **FTS**.  
По ресурсам **fuzzy search** затратен т.к. нужно проверить все варианты совпадений.

**Алгоритмы реализации fuzzy search:**
* Levenshtein distance - выделен как важный
* Soundex
* Metaphone
* Double Metaphone

## `unaccent` функция для поиска
Источник: [1](https://www.postgresql.org/docs/9.1/unaccent.html)

**unaccent** - это не алгоритм поиска, это словарь или алгоритм который удаляет **accents / diacritics** (как нормализация) строки в поле DB. В отличии от словарей из FTS это **фильтрующий словрь**, который передает результат своей работы дальше (например в **FTS**) Так поиск идет только по основе слова.  
В **Postgres** эта функция выделена в отдельный модуль.

# Для чего Primary Key и Foreign Key?

**Primary Key** и **Foreign Key** - нужны чтобы соблюдать **consistency**. И для **normalization**.

В некоторых RDBMS установка foreign key **увеличивает performance** оператора `select`. И **уменьшают performance** для `updates, inserts and deletes` (т.к. по дереву индексов быстрее искать, но при изменении данных нужно время на перестройку дерева индексов).

* **Primary Key** - гарантирует `consistency` **значений** записей (строк **parent** табл.). Гарантирует, что записи уникальны (уникальность id). Без этого id невозможно отличить 2 записи друг от друга только по одному стобцу id.
* **Foreign Key** - гарантирует `consistency` **данных** (строк **child** табл.). которые указывают на другие данные (например указывает на родительскую запись). Гарантирует, что:
  1. **child** запись (строка) всегда указывает на **parent** запись
  2. **child** запись указывают **только на существующую** parent запись. Без **Foreign Key** child запись может указывать на **не существующую** parent запись.

**Когда невозможно применить Primary Key и Foreign Key:** E.g. когда данные в DB **реплицируются** постоянно из другой DB (**e.g.** через Kafka), в этом случае возможно что запись (строка) есть в **child** табл., но ее еще нет в **parent** табл. (т.к. она еще не успела реплицироваться после запроса на репликацию).

# В стандартном subquery нельзя использовать `order by`
Источник: [1](https://stackoverflow.com/questions/34378701/can-i-use-order-by-clause-in-a-subquery)

В **subquery** нельзя использовать `order by`. DBs это не поддерживают, т.к. в этом нет смысла, т.к. результат subquery промежуточный и e.g. при использовании `in(select ... order by ...)` не важно было поле сортировано или нет.

**Исключения** e.g. использование `WITH`
```sql
WITH t AS
(SELECT col1 FROM tab ORDER BY col1) -- ORDER BY работает
SELECT * FROM t;

-- ошибка,  ORDER BY не работает
WITH t AS
( SELECT col1 FROM tab ORDER BY col1
  UNION ALL
  SELECT col1 FROM tab ORDER BY col1
)
SELECT * FROM t;
```

# При `distinct` поля из `order by` обязательно должны быть в `select` и про `distinct on`
Источник: [1](https://stackoverflow.com/a/2821375)

При `distinct` поля из `order by` обязательно должны быть в `select`.  
Возможно это касается не всех DBs (Postgres касается)
```sql
select id from user order by `name`; -- работает
select distinct id, `name` from user order by id, `name`; -- работает
select distinct id from user order by id, `name`; -- ошибка, если есть distinct все поля order by должны быть в select
```

**distinct on** - делает `distinct` только по указанным полям, а не всем. Все поля из `distinct on` должны быть в `order by`.
```sql
select distinct on(`name`) name, id from user
order by id; -- ошибка, нужно указать все что в distinct on
-- order by name, id -- правильно
```

Чтобы обойти это ограничение можно сделать  
`select ... from (select distinct ... from table) as table_unique`
```sql
select *
from user where id in (select distinct id from user)

select * -- или если нужны все поля
from (select distinct on (id) id as uniq_id, * from user) as user_unique 
order by name;
```

# Transactions (Транзакции) и все что к ним относится
## О том как транзакции работают и их паттерны (перенести сюда)

На англ, но понятное пояснение работы с транзакциями (грязное чтенени и т.д.)
Очень понятно и красиво написано, феномены не только из SQL стандарта и это важно, т.к. не смотря на то что в стандарте их нет они реально существуют
в том числе про Optimistic vs Pessimistic
и про то зачем и как использовать более низкий уровень изоляции

Прим. некоторые БД при низком уровне изоляции могут справляться с феноменами более высокого уровня, это не по SQL стандарту, но это нормально.
При этом в некоторых БД Snapshot уровен изоляции если БД работает в режиме MVCC и он слабее Serializable и может не защитить от некоторых проблем.
https://begriffs.com/posts/2017-08-01-practical-guide-sql-isolation.html
## Концепции
### ACID концепция

**ACID** - это 4е свойства которыми характеризуется транзакция.

1. **Atomicity** - берет набор операция и оборачивает их в unit of work, они успешны тогда и только тогда когда успешны все операции. transaction может хранить state change (если она не read only). transaction всегда должна оставлять систему в consistent состоянии, не смотря на то сколько transaction выполняются одновременно.
2. **Consistency** (Согласованность) - каждая успешная транзакция по определению фиксирует только допустимые результаты (Keys, Data types, Checks and Trigger). Если database состоит из нескольких node, то тут выполняется C из CAP и это не тоже C что в ACID. И означает это С, что изменения распространяется на все nodes (это называется multi-master replication). Например, при переводе денег между счетами, сумма денег на счетах не должна измениться.
3. **Isolation** - Прячет uncommitted state changes (данные которые транзакция еще не сохранила и они в процессе обработки) от других транзакций, достигается через pessimistic or optimistic locking. Но locks плохо влияют на параллельное выполнение. Некоторые DB реализуют MVCC - транзакции проходят на snapshot копиях текущего состояния DB и в случае успеха сливаются с основной DB.
4. **Durability** - в случае поломок система после запуска продолжит работу с места где начала, достигается через запись операций в **transaction log** и восстановления их оттуда в случае поломок (для некоторых режимов, например read only может отключаться **transaction log** для повышения скорости работы). Для JMS транзакции не обязательны, хотя и применяются по необходимости. Для файловых систем транзакции обычно не применяются, но если нужно транзакционно обмениваться файлами можно использовать утилиты такие как XADisk.

**Atomicity** - обязательный атрибут транзакций (т.е. оно должно выполняться для любой транзакции), остальные свойства можно обменять на масштабируемость (scalability) или производительность (performance). Т.е. отключить их выполнение или понизить количество их срабатываний и следовательно дополнительные операции, которые они выполняют.

Настроить **durability** (отключить или понизить точность) имеет смысл для highly performing clustered databases, если система не требует транзакций которые durability, в остальных случаях ее лучше не трогать.

**Isolation** (I из ACID) можно обменять целостность данных на производительность.

**Транзакция** - это коллекция read/write операций, успешных только если все операции в коллекции успешно завершены. Или еще называют процессом переноса данных (состояния) - data state transition. Поэтому транзакции должны быть выполнены последовательно (serial) даже если они выполняются параллельно (concurrently executed).  
Транзакция должна собрать параллельные requests (от клиента), т.к. serialization влияет на scalability (видимо имеется ввиду чтобы оптимизировать процесс серийного выполнения параллельных транзакций их нужно с начало собрать и уже потом оптимизировать).

**multi-master replication** - техника записи изменений на другие ноды и решения конфликтов такой записи. Если запись в slaves (зависимые) nodes (узлы, копии DB или независимые части этой DB) выполнится asynchronously, то сломается consistency. И система становится eventually consistent.

В SQL каждая команда выполняется внутри transaction, даже если transaction не запущена явно. Это называется **autocommit**.

При этом система должна работать как буд-то последовательно не смотря на параллельность транзакций (in a serial form) это называют serializability. (прим. возможно **serializable isolation level** это означает выстраивание операций в серию?). На этот процесс влияет **Закон Амдала**

Современные нагруженные системы проектируются из предположения, что ситуации конфликта параллельных транзакций (phenomena) происходят всегда и даже самые простые системы могут обрабатывать 1000 TPS.

В большинстве DBs **Isolation** достигается через lock, но lock увеличивает количество кусков кода которые будут выполнены последовательно (serializable) и следовательно влияет на параллелизм программ. Некоторые DBs используют **MVCC** (видимо тут имеется ввиду, что вместо lock могут использоваться более новые механизмы вроде MVCC)

### BASE концепция

<small>Источник: [тут](https://habr.com/ru/post/328792/), [тут](https://neo4j.com/blog/acid-vs-base-consistency-models-explained/)</small>

**BASE** - альтернатива **ACID**, выбирает **availability** вместо **consistency**. Применяется обычно для **NoSQL** баз.

- **Basic Availability** - система всегда доступна, но может вернуть ошибку или **inconsistent** данные
- **Soft state** - не гарантируется состояние **consistency**, DB может меняться сама со временем становясь **eventual consistency**
- **Eventual consistency** - в конечном итоге система станет **consistent**, когда изменения распостранятся на все данные, при этом полная **consistency** может быть нарушена

**Note.** Когда в чатах сообщения приходят не в том порядке, это следствие использование DB настроенной на **Availability** (т.е. подходит определение **BASE**)

### Закон Амдала и влияние многопоточности

**Закон Амдала** - скорость выполнения программ параллельно ограничена временем, которое нужно для последовательного (serial) выполнения кусочка этой программы. Из этого следует, что с определенного момента само распараллеливание программы по ресурсам будет стоить дороже (в плане скорости работы) чем последовательное выполнение, т.к. на само переключение между процессами тоже тратится время.

Например большинство DB выбирают по умолчанию **relax correctness** и это позволяет достигать лучшей concurrency  
**Note.** т.е. как я понимаю система работает параллельно без синхронизации потоков, что избавляет от траты ресурсов на переход между потоками `(т.е. при транзакциях не используется синхронизация потоков и тразакции работают параллельно, как при optimistic lock)`, но проверяет результат работы и падает в случае если проверка целосности показала ошибку после изменения данных `(после выполнения транзакций)`, при этом хотя на падение и ошибку тратится часть ресурсов это все равно выгоднее чем постоянно синхронизировать потоки. Например алгоритм **MVCC** (см. ниже).

Формула:

### CAP (теорема Брюера)
Источник: [тут](https://www.ibm.com/cloud/learn/cap-theorem)

Из **CAP** можно выбрать **только 2** из 3х свойств.

- **CAP**
  * **Consistency** - Корректность / согласованность, Каждое чтение даст самую последнюю запись или выдаст ошибку, не важно к какому узлу DB будет запрос. Т.е. это значит, что изменения в одном узле сразу распостранятся на остальные.
  * **Availability** - сервис (напр DB) всегда доступен, даже когда много узлов не работает
  * **Partition tolerance** - Устойчивость к распределению в сети (при разнесении узлов DB на разные сервера, узлы синхронизируются между собой), также устойчивость к сбоям сети (узлы работают даже при потери связи, а при восстановлении связи синхронизируются).

**C** (Consistency) - Это не та согласованность, что в ACID. Другими словами, после выполнения обновления данных, все наблюдатели (например пользователи) увидят обновления и система состоящая из разных node ведет себя как единое целое. Т.е. происходит репликация данных между разными nodes (это называют **multi-master replication**).

Согласованность с точки зрения клиента и Согласованность на стороне сервера - различаются, то как видит клиент и то что происходит внутри сервера.

**Типы consistency (Согласованность с точки зрения клиента):**
1. **Strong consistency** - после того как данные записаны процессы чтения могут будут возвращать обновленные данные.
2. **Weak consistency** - после того как данные записаны процессы чтения могут не вернуть обновленные данные сразу, т.к. нужно время чтобы внутренние операции распространили записанные данные на другие nodes системы и уже тогда система вернет данные, после того как данные запишутся целостно. Промежуток времени нужный на внутренние процессы согласования называется окном несогласованности (**inconsistency window**).
3. **Eventual consistency** - частный случай **Weak consistency**, в отсутствии изменений данных, через какой-то промежуток времени после последнего обновления («в конечном счёте») все запросы будут возвращать последнее обновлённое значение. Обновленная запись распространяется в соответствии с параметрами конфигурации и настройками интервалов кэширования. **Другими словами**, после обновления система ждет какое-то время и если это время обновлений не было, то можно всем пользователям показать обновленные данные. Пример такой системы: DNS сервер.
4. **Causal consistency** -
5. **Read-your-writes consistency** -
6. **Session consistency** -
7. **Monotonic read consistency** -
8. **Monotonic write consistency** -

Пояснение: **Strong consistency** это важна штука для таких операций, как например банковские, где важна точность. Многие NoSQL системы разработаны распределенными и в них трудно реализовать Strong consistency, поэтому по умолчанию многие из них ведут себя как **Eventual consistency**.

**Согласованность** на стороне сервера - прим. тут идет сложный процесс распространения репликаций на все узлы, описывать долго

Если в DB нет foreign key, то ее в некоторых источниках называют inconsistent. Т.е. видимо не соблюдается условие C из ACID и связь по ключам id нужна для корректных transactions.

**P** из **CAP** выполняется почти для всех DB. Поэтому в реальных системах при разработке выбор в основном между **C** и **A**.

**Пояснение:**
* Корректность (**Consistency**) - Говорит о том, что система всегда выдаёт только логически непротиворечивые ответы. То есть не бывает такого, что вы добавили в корзину товар, а после рефреша страницы его там не видите.
* Доступность (**Availability**) - Означает, что сервис отвечает на запросы, а не выдаёт ошибки о том, что он недоступен.
* Устойчивость к сбоям сети (**Partition tolerance**) - Означает, что распределённая по кластеру система работает в расчёте на случаи произвольной потери пакетов внутри сети.

**Сравнение выбора из CAP:**
* **AC**: +Availability +Consistency -Parition tolerance
	* система из одного узла, не распределенная и доступная всегда
* **CP**: +Consistency +Partition tolerance -Availability
	* распределенная, чтение доступно всегда, а запись только если транзакции уже завершились (не в процессе)
	* т.е. доступность блокируется на время транзакции
* **AP**: +Availability +Partition tolerance -Consistency
	* распределенная, всегда доступна, но данные по кластеру распостраняются не сразу и какое-то время после записи возвращаются старые данные
	* зато система доступна всегда
	
**Availability** часто важна, т.к. из-за простоя теряются деньги (e.g. для интернет магазинов).  
**Consistency** важна для систем где нужна точность в обмен на задержки (e.g. финансовые системы, банки).

**Note.** Перенести сюда инфу о том как CAP и ACID работают внутри (linearizability и serializability), правда пока не понятно зачем, но статья интересна
	http://www.bailis.org/blog/linearizability-versus-serializability/

### PACELC теорема

[PACELC](https://en.wikipedia.org/wiki/PACELC_theorem) (`IF P -> (C or A), ELSE (L or C)`) - дополнение к **CAP**, даже если DB из 1го узла, то все равно есть выбор между **L** (latency) и **C** (т.е. систем полностью **A** нету). **Note.** Синхронизация данных между узлами тоже называется **L** (latency).

**Другими словами:** Если в системе есть узлы **P**, то выбираем между **С** и **A**, если система из одного узла (нет **P**), то выбираем между **L** и **C**, т.к. даже если нет узлов **P**, то внутри DB все равно происходят операции, которые приведут к задержке **L**.

По **PACELC** есть табл. [рейтинга DBs](https://en.wikipedia.org/wiki/PACELC_theorem#Database_PACELC_ratings) описывающий на что больше рассчитаны системы **A** или **C** (L), в большинстве DBs настраивается **L** (latency) в обмен на **A**.

## Locks (Блокировки)

Источник: [тут](https://vladmihalcea.com/a-beginners-guide-to-database-locking-and-the-lost-update-phenomena/)

В concurrency theory понятие lock это защита целостности (integrity) общих (shared) данных.

Приложение может делегировать работу с lock базе данных (e.g. через ORM). Это упрощает приложение. DB может сама решать проблемы **deadlock**, находя такие проблемы и делая случайное отпускание (releasing) одной из блокировок (competing locks).

**implicit locking** (неявная блокировка) - спрятана за **transaction isolation level**, т.е. она включается неявно сама при установке isolation level. **isolation level** это предопределенные схемы блокировки (predefined locking scheme). Каждая isolation level защищает (preventing) от определенного набора аномалий целостности данных (integrity anomalies).

**explicit locking** (явная блокировка) - делится на **pessimistic** (physical) and **optimistic** (logical) locks.

**database locking** - это locks реализованные в DB, т.е. это **pessimistic locks**. Они подходят для групп операций с данными как целым (**batch processing systems**). Но в реальном приложении операция может распостранятся на несколько транзакция в DB (**multi-request web flow**). В случае если нужно за одну операцию выполнить несколько transaction в DB нужен logical (optimistic) lock. Например, само приложение может выполнить одну транзакцию и ожидать действий пользователя, чтобы выполнить уже другую с учетом первой транзакции, но с новыми данными - это называется **long conversations** (и так для множества транзакций и множества действий пользователя). **optimistic lock** выполняется на уровне приложения и ее рекомендуется сочетать с **repeatable reads** isolation level для реализации (implementing) **logical transactions** (логической транзакции) без потери производительности (в отличии от некоторых других подходов).

**Другими словами**, logical locks и logical transaction (логические блокировка и транзакция) выполняются самим приложением, потому что DB не может узнать когда они начинаются и заканчиваются, а не может она это узнать потому что пользователь может выполнить операции с данными в любой момент времени. Эти данные нужно будет изменить в DB, но наиболее эффективно менять все данные DB с которыми работал пользователь в один момент времени в конце всех операций. А не каждый раз когда пользователь что-то сделал с данными. Т.е. эффективнее одни или несколько транзакций, чем много транзакций на каждое действие пользователя. Поэтому приложения имеют специальный кэш операций, которые выполняет пользователь. Перед commit логической транзакции происходит очистка этого кэша операций от лишних операций, которые перекрываются другими операциями. На этом же этапе происходит оптимизация запросов. И уже после этого все операции пользователя объединенные в один запрос или в batch операций отправляются в DB и выполняются там в **Physical transaction**.

**Physical locks** (pessimistic locking) - делится на **shared** и **exclusive** locks. При этом они могут делать lock как на отдельных **rows** так и на целых **tables**. При этом в JPA можно сделать только **одну блокировку** за раз, если это невозможно выбросится **PersistenceException**.

**Note.** Не совсем ясная инфа из статьи: **READ COMMITTED** использует query-level shared locks and exclusive locks for the current transaction modified data. **REPEATABLE READ** and **SERIALIZABLE** use transaction-level shared locks when reading and exclusive locks when writing. 

**shared lock** (read lock) - блокированные данные можно читать, но нельзя модифицировать

**exclusive lock** (write lock) - блокированные данные нельзя читать или модифицировать

**logical locks** (optimistic locking) - блокировка данных выполняется самим приложением, внутри кода различных библиотек и прочего. При этом **persistence providers** (видимо имеется ввиду **DB** или **ORM** библиотека) может (а может и не поддерживать? в оригинале неясно) поддерживать **optimistic locking** для entities у которых нет **@Version** поля, но **лучше** всегда указывать такой атрибут при работе с optimistic locking.

**implicit locking** vs **Explicit locking** - для большинства приложений **implicit locking** (т.е. установка transaction isolation level) лучший выбор. Но иногда необходимо более fine grained (точное) управление блокировками. Большинство DB поддерживают **query-time exclusive locking directives** такие как **SELECT FOR UPDATE** и **SELECT FOR SHARE**. При этом можно использовать **более низкий уровень изоляции** транзакции (напр. READ COMMITTED) совместно с использованием **share** or **exclusive** locks.

**SELECT … FOR UPDATE** это exclusive lock, блокирует row таблицы. Применяется когда ... 

```sql
-- все другие транзакции которые хотят сделать select for update будут блокированы
BEGIN;

SELECT *
FROM player
WHERE id = 42
FOR UPDATE; -- блокирует пока не произойдет UPDATE

-- тут какая-то логика, делаем то для чего заблокировалли строку

UPDATE player -- UPDATE разблокирует строку
SET score = 853
WHERE id = 42;

COMMIT;
```

**Pessimistic Locking vs. Optimistic Locking** - **Optimistic Locking** полезна когда есть много reads и мало updates или deletes. Optimistic Locking также полезна когда entities на какое-то время нужно сделать detached и Pessimistic locks не могут быть применены. **pessimistic locking** делает lock на уровне DB. pessimistic locking в целом лучше защищает целостность данных.  
**Другими словами** optimistic locking  убеждается, что updates or deletes не будет overwritten or lost молча. В отличии от **Pessimistic Locking** оптимистичная блокировка не делает lock на DB и не может вызвать **deadlock** (при **Pessimistic Locking** есть риск **Deadlock**, если использовать его неправильно).

**Почему появилась Optimistic Locking (Source: [1](https://stackoverflow.com/a/58952004)):** В современных приложениях не используют **read/write** в одной **transaction** и **ACID** больше недостаточно (в 80х годах все операции делались в 1ой **session**), т.к. одновременно несколько пользователей могут делать update в разных транзакциях и вызвать **Lost Update**. Т.е. предотвращается **Lost Update** при **user-think time** (при задержке отправке запроса пользователем и влинивании другого запроса в эту задержку).

В разных движках внутри блокировки работают по разному, например в InnoDB блокируются не сами записи, а **индексы** и есть блокировки:

* **record lock** — блокировка записи индекса
* **gap lock** — блокировка промежутка между, до или после индексной записи
* **next-key lock** — блокировка записи индекса и промежутка перед ней
* **range locks** 
* **predicate locking**

Для реализации **Locks** и **Transaction isolation level** используются **concurrency control mechanisms**. Самые популярные: **2PL** и **MVCC**.

Про разные типы блокировок в InnoDB хорошо: [тут](https://habr.com/ru/post/238513/), [REPEATABLE-READ and READ-COMMITTED Transaction Isolation Levels](https://www.percona.com/blog/2012/08/28/differences-between-read-committed-and-repeatable-read-transaction-isolation-levels/)

## 2PL (Two-Phase Locking)

Источник: [тут](https://vladmihalcea.com/2pl-two-phase-locking/)

**Oracle** - не дает делать **read / write** блокировки явно, только через `for update`  
**Другие DBs** обычно дают делать **read / write** блокировки на row или множестя rows. (аналог `ReentrantReadWriteLock` в Java)

Сами по себе locks не убирают конфликты. Нужны стратегии (протоколы) их применения: как locks будут применены и убраны. **2PL** имеет стратегию которая гарантирует **strict serializability**.

**2PL состоит из фаз:**
1. expanding phase (locks are acquired, and no lock is allowed to be released)
2. shrinking phase (all locks are released, and no other lock can be further acquired)

## MVCC

## Common
Источник: [тут](https://vladmihalcea.com/how-does-mvcc-multi-version-concurrency-control-work/), [тут](https://habr.com/ru/company/postgrespro/blog/442804/), [тут](https://vladmihalcea.com/how-does-mvcc-multi-version-concurrency-control-work/), [тут](https://vladmihalcea.com/write-skew-2pl-mvcc/)

## MVCC vs 2PL и про то что MVCC даже с Serializable isolation level может не предотвратить Phantom Read

Источник [тут](https://vladmihalcea.com/a-beginners-guide-to-the-phantom-read-anomaly-and-how-it-differs-between-2pl-and-mvcc/) (с картинками и примерами)

SQL Server - использует **2PL**  
Oracle, PostgreSQL, and MySQL InnoDB - используют **MVCC**

**Note.** Реализация и использование в **MVCC** уровня изоляции **Serializable** трудно.

**2PL** (Two-Phase Locking) - старый, более медленный, но надежный механизм. Использует **shared** (read) and **exclusive** (write) **locks**. Могут быть row-level locks, предотвращают lost updates, read and write skews. И range of rows locks, предотвращают phantom reads.

**MVCC** (Multi-Version Concurrency Control) - построена на идеи: Readers should not block Writers, and Writers should not block Readers. Использует **optimistic** (похоже на **идеи** optimistic lock в JPA) подход к решению проблем. Как и в 2PL для всех изменяемых данных используется **exclusive lock** иначе может нарушиться **atomicity.** При использовании MVCC в DB может быть **Serializable**, но на самом деле внутри это **Snapshot** уровень (это уровень присущий **MVCC**), который просто имеет другое название и всеравно имеет проблемы с предотвращением феноменов. В отличии от **2PL** в **MVCC** нет возможности реализовать **isolation level** поверх MVCC, поэтому каждая DB использует свои уровни которые пытаются предотвратить так много аномалий как могут. **PostgreSQL** в отличии от других DB реализовала **Serializable Snapshot Isolation** (SSI) level, который лучше чем **Snapshot** у других DB и может предотвратить **Write Skews** (это сложный механизм). Но даже в PostgreSQL это не настоящая **Serializability**, т.к. она подразумевает, что транзакции выполняются последовательно (эквивалентны одному серийному выполнению всех своих команд). В случае PostgreSQL в реализации механизма задействовано время, что может вызвать какие-то особенности не с точки зрения механизма контроля транзакций DB, но с точки зрения приложения. В MySQL алгоритм **MVCC** предотвращае многие проблемы аномалии, т.к. использует свой механизм с блокировками (тоже хороший).

## Transaction Isolation level (кратко)

- **Read phenomena** - типы ошибочного чтения (проблемы параллельного чтения транзакций):
  
  - **lost update** -  при одновременном изменении одного блока данных разными транзакциями одно из изменений теряется. **Любой уровень изоляции кроме NONE из списка ниже предотвращает** `lost update`
  - **dirty reads** - чтение данных откатившейся транзакции A транзакцией B до отката A
  - **non-repeatable reads** - транзакция читает данные несколько раз, но получает разные данные. Потому что эта строка была изменена в промежутке между чтениями
  - **phantom reads** - транзакция читает строки по одному критерию несколько раз, в промежутках между чтениями происходит вставка строк подходящих под критерии. Один и тот же запрос дает разные результаты (фантомные)

- **Алгоритмы блокировки** - используются в **isolation level** блокируют таблицы или столбцы от записи и/или чтения несколькими транзакциями одновременно (изолируют)
  
  1. **Two-Phase Locking (2PL)** - алгоритм использующийся для **serializable isolation** level. Состоит из: shared (read) и exclusive (write) блокировок. Может быть применена на **ОДНУ** строку для предотвращения lost update, read, write. Или на **НЕСКОЛЬКО** строк для предотвращения phantom reads.
  2. **Multi-version concurrency control (MVCC)** - алгоритм использующий копию используемых данных DB для других транзакций пока и номер версии этих данных для проверки, на нем основан алгоритм **snapshot isolation** level. Но это ДРУГОЙ алгоритм.

- **Isolation level** - уровни изоляции такие как у JDBC
  
  - **none** - информирует, что драйвер не поддерживает транзакции, без изоляции
  - **read uncommitted** - no lock on table. Разрешает dirty, non-repeatable, phantom reads
  - **read committed** - lock on committed data. You can read the data that was only commited. запрещает dirty reads
  - **repeatable read** - lock on block of sql (which is selected by using select query). запрещает dirty и non-repeatable reads
  - **serializable** - lock on full table (on which Select query is fired). No other transaction can modify the data on the table. запрещает dirty, non-repeatable, phantom reads
  - **snapshot** - запрещает Dirty Reads, Lost Updates and Read Skews, но возможна Write Skews or Phantom Reads. На практике несработавшая транзакция просто откатится.

**snapshot** — данный вид изоляции не входит в рекомендации стандарта SQL 92, но он реализован во многих СУБД.  
Процессы-читатели не ждут завершения транзакций писателей, а считывают данные, точнее их версию, по состоянию на момент начала своей транзакции.  
На самом деле создается копия данных на момент начала транзакций, которая используется другими транзакциями. Этот уровень **слабее serializable**, но **сильнее остальных**. Хотя изоляция и не полная.  
Она НЕ блокирует другие транзакции от чтения данных. Другие транзакции меняющие данные НЕ блокируют snapshot транзакцию от чтения меняемых ими данных.

| Isolation level  | Dirty reads | Non-repeatable reads | Phantoms |
| ---------------- | ----------- | -------------------- | -------- |
| Read Uncommitted | yes         | yes                  | yes      |
| Read Committed   | no          | yes                  | yes      |
| Repeatable Read  | no          | no                   | yes      |
| Serializable     | no          | no                   | no       |

- **Настройка**
  - Глобально через `hibernate.connection.isolation=REPEATABLE_READ`
  - Отдельно
    
    ```java
      // READ COMMITTED - значение по умолчанию???
      @Transactional(isolation=Isolation.READ_COMMITTED)
      public void someTransactionalMethod(User user) {
      // Interact with testDAO
      }
    ```

## Какой Transaction isolation level использовать и когда

* **Когда и что использовать**. Источник табл. [тут](https://developer.jboss.org/message/865105#865105)
  * **Для финансовых систем** (для денег).  
    Рекомендуется использовать **Repeatable Read** или **Optimistic locking** (например объявив поле `@Version` в ORM библиотеке). **Optimistic locking** нужна, чтобы убедиться, что не будет **lost update**, если другая транзакция уже изменила данные.
  * Для приложений типа **Decision Center data access**.
    Рекомендуется **read committed**
* **Использование блокировок и транзакций на практике.**  
    Согласно [этому](https://stackoverflow.com/questions/35068324/why-should-i-need-application-level-repeatable-reads#comment57934910_35105898) ответу **optimistic lock** + **READ_COMMITED** предотвращает **lost updates**. При этом **Repeatable-Read** предотвращает **read and write skew** в отличии от **READ_COMMITED**.  

    isolation level выше чем read uncommited это pessimistic locking (реализован через него), т.е. сам lock делается на используемом в transaction момент ресурсе.

    Если в DB высокая конкуренция transactions и fail при optimistic lock происходят часто, то будет высокий failure rate. Pressimistic lock это **queuing mechanism** (ставит операции в очередь) и тоже затратен, т.к. другие операции ждут своей очереди.

    **READ_COMMITTED** стоит по умолчанию во многих DB (в MySQL стоит **REPEATABLE_READ**). Она эффективна, но для некоторых случаев нужно ее менять.

    В Oracle уровень изоляции Serializable при использовании MVCC (Multi-Version Concurrency Control) на самом деле это Snapshot Isolation level.
* Чтобы предотвратить lost update на уровне Read commited. **Способы предотвратить:**
    * Нужно использовать **optimistic lock** (блокировку на уровне приложения с указанием версии row) и уровень **read commited**
    * `pessimistic lock`
    * `SELECT FOR UPDATE` при этом все параллельные транзакции должны использовать `FOR UPDATE`, а не только та которую нужно "защитить" от **lost update**
    * Использовать **Repeatable Read**. При **Repeatable Read** транзакция будет прервана в случае нарушения **atomicity**.
* В MVCC нету **Dirty read** даже если поставить READ UNCOMMITTED
* READ UNCOMMITTED можно поставить если нужно увидеть изменение данных в реальном времени для отладки Note. Хотя при MVCC может не сработать.
* Repeatable read может вызвать много **deadlock** (но это зависит от движка DB). Источник: [тут](https://stackoverflow.com/a/2155265), [тут](https://thinkingeek.com/2007/09/23/repeatable-read-and-deadlocks-in-sql-server/)
  
    **Repeatable read** может вызвать много **deadlock**, если транзакции большие или они делают **read-write-read** транзакции (всмысле последовательности операций, `читают, пишут, опять читают` в рамках той же транзакции). **Как избежать:** использовать **read-write** транзакции, так будет взят только **exclusive lock** (т.к. мы делаем **update** данных после **read**) и **shared lock** не будет взят после **write** (что видимо даст параллельной транзакции работать).    
    **Другими словами нужно:** сначало делать **select**, потом **update/delete**, но еще раз **select** после этого не делать в той же транзакции.

## Transaction vs Lock

Этот раздел поясняющий понятия своими словами, компиляция советов из интернета.

- [подборка со stackoverflow](https://stackoverflow.com/questions/4226766/mysql-transactions-vs-locking-tables)
  
  - **lock и transaction** не связаны друг с другом. Внутри transaction можно использовать lock, чтобы блокировкть некоторые строки (для лучшего соблюдения ACID)
  - **lock предотвращает** изменения записей DB на которых сделан lock от изменения кем-то еще (т.е. используется чтобы предотвратить само появление ошибок)
  - **transaction предотвращает** следит за тем, чтобы конечный результат группы запросов был верен (т.е. откатывает все изменения в случае ошибки)
  - **lock** предотвращает **concurrent** операции (синхронизирует записи), **transaction** - **изолирует данные** (от ошибочного изменения)
  - **lock и transaction** ведут себя согласно спецификациям, но по разному. Можно добиться схожего поведения (блокировки данных) **и через lock, и transaction**.

## Transaction Isolation level

Источник: [ACID & Dirty read & Non-repeatable read & Phantom read](https://vladmihalcea.com/a-beginners-guide-to-acid-and-database-transactions), [lost update](https://vladmihalcea.com/a-beginners-guide-to-database-locking-and-the-lost-update-phenomena/), [Read & Write Skew](https://vladmihalcea.com/a-beginners-guide-to-read-and-write-skew-phenomena/)

Все уровни изоляции кроме **SERIALIZABLE** могут приводить к **data anomalies** (phenomena).

**Таблица того что предотвращает каждый из уровней изоляции**

| Isolation Level  | Dirty read | Non-repeatable read | Phantom read |
| ---------------- | ---------- | ------------------- | ------------ |
| READ_UNCOMMITTED | allowed    | allowed             | allowed      |
| READ_COMMITTED   | prevented  | allowed             | allowed      |
| REPEATABLE_READ  | prevented  | prevented           | allowed      |
| SERIALIZABLE     | prevented  | prevented           | prevented    |

**Феномены Из SQL стандарта:**

* **Dirty read** (при **READ_UNCOMMITTED**) - случается когда транзакция позволяет read uncommitted (еще незакомиченные данные) другим транзакциям, т.е. прочитанные данные потом могут откатиться. Это случается потому что не сделан lock на данных. Большинство DBs имеют by default **более высокий уровень изоляции,** (чем READ_UNCOMMITTED) т.к. этот плохо влияет на целостность данных. Чтобы установить этот уровень изоляции DB прячет uncommitted changes ото всех других транзакций и каждая из транзакций видит только свои изменения данных. Если DB использует **2PL**, то uncommitted rows защищены через сделанный на них write lock (предотвращает операции read этих rows пока данные не commited). Если DB использует **MVCC**, то движок DB может использовать **undo log** который содержит uncommitted record каждой прошлой версии данных, чтобы восстановить прошлое значение данных для запросов сделанных другими транзакциями (т.е. если данные будет читать параллельная транзакция, то она получит старое значение, а не то что в процессе изменения текущей транзакцией). Т.к. этот механизм (видимо имеется ввиду **undo log**) используется всеми другими уровнями изоляции, и он имеет какие-то оптимизации связанные с восстановление образов прошлых данных. **(прим.** Данный уровень изоляции можно использовать например если нужно посмотреть прогресс batch запроса SQL как я понял на реальных данных, когда видно как он меняет данные в самой DB.)
* **Non-repeatable read** (или **fuzzy reads**) - при повторном чтении в рамках одной транзакции ранее прочитанные данные оказываются изменёнными. Происходит если какая-либо транзакция читает DB row без shared lock на новых полученных (fetched) record (записях), тогда параллельная транзакция может изменить эту row до того как первая транзакция завершена. **Другими словами** начинается первая транзакция по чтению записи (record), данные читаются; теперь начинается вторая (параллельная) транзакция, которая изменяет данные; первая транзакция перечитывает данные тем же SQL запросом (видимо имеется ввиду в той же, первой, транзакции) и получает уже другой результат. **Это проблема,** потому что бизнес логика может принимать решение на основе самого **первого** результата чтения record до ее изменения в параллельной транзакции (т.е. например количество товара в магазине уже может быть равным нулю, хотя на момент первого чтения было больше). **Как это предотвращается на уровне DB.** При **2PL** делается shared lock при каждом чтении (read) и этот phenomenon будет предотвращен т.к. параллельные транзакции не смогут сделать exclusive lock на той же DB record. Большинства DB используют **MVCC** и shared lock больше необязательна для предотвращения **non-repeatable reads**, в этом случае делается проверка row version и если полученная (fetched) record до этого была изменена (между началом и канцом текущей параллельной транзакции), то текущая транзакция прервется (откатится). **Repeatable Read** and **Serializable** предотвращают **Non-repeatable read** по умолчанию. **Read Committed** может предотвратить этот phenomenon на уровне DB при использовании **shared locks** (e.g `SELECT ... FOR SHARE`) или **exclusive lock** (если ваша DB не поддерживает shared locks, e.g. Oracle). Некоторые ORM frameworks (e.g. JPA/Hibernate) имеют application-level repeatable reads (т.е. предотвращают repeatable reads на уровне приложения). Получается это потому что первая копия (snapshot) полученной entity попадает в кэш L1 (Persistence Context) и любой успешный запрос вернувший **туже DB row** будет использовать ту же Entity из кэша L1 (т.е. так предотвращается Non-repeatable read т.к. каждое чтение возвращает повторяющиеся данные, т.е. REPEATABLE READ, **Note.** в статье написано МОЖЕТ но там не указано нужно что-то дополнительное для этого делать или так работает по умолчанию).
* **Phantom read** - когда одна транзакция начитала данные по одному критерию, другая делает insert или delete этих данных (добавляет или удаляет подходящие под критерий) и мы работаем с неактуальными данными **Note.** это как **Non-repeatable read**, но вместо изменения ранее прочитанных данных происходит вставка новых данных или удаление существующих подходящих под тот же критерий (predicate) по которому данные были прочитаны. Разница в том, что раз менялись не данные, а добавлялись или удалялись записи, то lock нужно делать уже по другому. Это проблема т.к. бизнес логика может работать на основе полученных ранее данных, хотя они уже другие. Например, покупатель может принимать решение о выборе покупке уже загруженного списка товаров не зная, что лучших товар был добавлен сразу после получения (fetching) списка товаров. **2PL** предотвращает **Phantom read** используя **predicate locking** (один из видов lock как видно из названия видимо используется в нем условие по которому делался `SELECT` строк), **MVCC** обрабатывает **Phantom read** путем возвращения **consistent snapshots** (т.е. как я понимаю валидных копий записей с которыми происходит работа). Не смотря на это результат работы **MVCC** с **Phantom read** может всеравно вернуть старые записи (т.е. не предотвратить **Phantom read**). Даже если движок DB делает introspects (проверку) списка работы транзакций (transaction schedule) результат может быть не таким как у **2PL** (например, когда вторая транзакция делает insert без чтения того же набора records как у первой транзакции - цитата из статьи, не совсем ясная). Чтобы предотвратить **Phantom read** нужно использовать **Serializable** или использовать **share lock** или **exclusive range lock** (прим. тут **range lock** это вид lock на определенном наборе строк, как я понимаю). **Другими словами**, если верить vladmihalcea.com то предотвратить **Phantom read** при использовании **MVCC** трудно, по крайней мерер на уровне DB (не JPA) и это можно сделать например через явный pessimistic lock строк. Не нужно путать **Phantom Read** с **Write Skew**, т.к. **Write Skew** не подразумевает проблемы (феномены) с **range of records** (набором строк).

**Феномены Не из SQL стандарта:**

* **Lost Updates** (предотвращается **Repeatable Read** и выше изоляцией) - Случается когда параллельные транзакции меняют разные **columns** одной и той же **row**. Одна транзакция меняет данные, а другая параллельная сразу их затирает новыми, т.е. commit 1ой параллельной транзакции случается пока 2ая еще в процессе (как присваивание переменной в коде два раза подряд). Если параллельные транзакции меняют 1ин и тот же **column**, то **lost update НЕ случается**. Если не предусмотреть ситуацию когда **lost update** может случиться, то он может случиться (т.е. при написании программы нужно предусмотреть эту ситуацию)! Чтобы предотвратить его нужно делать `optemistic lock` в jpa (т.е. на уровне приложения) или `pessimistic lock` или `SELECT FOR UPDATE` или `Repeatable Read` (или `Serializable`). Причем **Repeatable Read** предотвращает **lost update** как и в случае с **read commited** которая может предотвратить **repeatable reads** (это происходит благодоря кэшу L1 если речь о JPA). Причем `pessimistic lock` в этом случае подойдет только если SELECT и UPDATE выполнены в одном запросе (т.е. обе параллельные **transactions** имеют `... FOR UPDATE`).
* **Read Skew** - Это когда есть несколько связанных rows (Entities в JPA), одна транзакция читает одну row (Entity в JPA), вторая параллельная транзакция меняет обе Entities, первая транзакция читает вторую Entity. И получается что одна Entity начитана со старыми данными, а вторая Entity с новыми. Это ломает **consistency**.
* **Write Skew** - Есть несколько rows (Entities в JPA). Обе параллельные транзакции их начитывают. Первая транзакция меняет данные одной Entity, вторая второй Entity. **(на самом деле** это определение может быть неточным, см. картинку в источнике) Т.е. нарушается синхронизация между несколькими связанными Entities. Для Oracle на момент написания предотвратить это можно только через optimistic или pessimistic lock (в том числе через `SELECT FOR UPDATE`), остальные DB вроде бы справляются в том числе в режиме **MVCC**.

Даже если движок конкретной DB может предотвращать при более низком уровне isolation проблемы более высокого уровня, не смотря на то что SQL стандарт говорит, что каком-то конкретном уровне изоляции проблема не может быть предотвращена, то **это не проблема**. Источник [тут](https://stackoverflow.com/a/53468822)

# Optimistic Lock SQL example
Source: [1](https://stackoverflow.com/a/58952004)

Читаем **version** перед запросом. Пользователь заполняет данные. При запросе отправляем эту версию и только если она совпала делаем **update**.

```sql
UPDATE item
SET name = 'newName', version = version + 1
WHERE id = 1 AND version = 1
```

# Query plan (execution plan)

<small>Источник: [лекция по Postgres](http://www.louisemeta.com/blog/pg-stat-statements), визуальное описание join ([1](https://bertwagner.com/2018/12/11/visualizing-nested-loops-joins-and-understanding-their-implications/), [2](https://bertwagner.com/2018/12/18/visualizing-merge-join-internals-and-understanding-their-implications/), [3](https://bertwagner.com/2019/01/02/hash-match-join-internals/)), общее описание оптимизаций ([1](http://ts-soft.ru/blog/sql-optimization-1), [2](http://ts-soft.ru/blog/sql-optimization-2)), описание цикла построения плана запроса ([1](https://vladmihalcea.com/relational-database-sql-prepared-statements/), [2](https://vladmihalcea.com/sql-execution-plan-oracle/)), план запрооса с оценками эффективности ([1](https://habr.com/ru/post/465547/). [2](https://habr.com/ru/post/465975/)), [типы hints](https://docs.microsoft.com/en-us/sql/t-sql/queries/hints-transact-sql?view=sql-server-ver15), [описание join от Microsoft](https://docs.microsoft.com/en-us/previous-versions/sql/sql-server-2008/ms191426(v=sql.100)?redirectedfrom=MSDN), [описание сложности hash join](https://www.sql.ru/forum/238015/oracle-hash-join-pochemu-to-rabotaet-medlenno?mid=2105105#2105105), [про cardinality](https://docs.microsoft.com/en-us/sql/relational-databases/performance/cardinality-estimation-sql-server?view=sql-server-ver15)</small>

**Query plan** (execution plan) - последовательность операций для получения данных из DB (e.g. реляционной DB).

**hint** - это спец. слова в SQL запросах, которые указывают какие именно алгоритмы использовать запросу при построении плана запроса. В разных DBs разные.

**Коротко как это работает:** SQL запрос перед выполнением передается в оптимизатор, он строит план запроса и оптимизирует его. Выбор плана состоит из способа поиска данных (**scan**) и способа физического соединения таблиц (**join**). Оценка скорости плана запроса (**cost**) состоит из сравневания: количества **CPU** и **I/O** операций, **cardinality**. **Цель оптимизации** - уменьшить количество этих операций. Для анализа плана запроса используется команда `explain`.

**Типы hints:**
1. **Query Hint** (**Scan** или fetching) данных выполняется несколькими алгоритмами: **full table scan** (sequential scan), **index scan** и другими (в разных DBs есть свои алгоритмы).
   * **Full table scan** просто перебирает строки, поиск медленнее чем по индексу, но нет трат ресурсов на переход по указателям в дереве индексов и сравнение хэш сумм дерева индексов, эффективна когда индекса нет или количество строк маленькое и поход по индексам займет больше ресурсов чем полный перебор, или при `select count(*)` при том что есть `null` значения. На диске кластер содержит больше данных чем одна запись (избыточна) и DB умеет заранее загружать следующие данные до их обработке, это ускоряет **Full table scan**.
   * **Index scan** это переход по дереву индексов, быстрый поиск для большой табл., но еще нужен переход от дерева индексов к самим значениям в таблице. Если используется Clustered Index, то значения хранятся в самом дереве индексов.
2. **Join Hint** (**Join**) делается алгоритмами: **Nested Loops Joins**, **Hash Joins**, **Sort Merge Joins**.
   * Если строк в табл мало, то используется **Nested Loops Joins**, как обычный вложенный цикл.
   * Если строк много, то используется **Hash Joins**, но т.к. сравниваются хэши, то есть ограничения на использования выражений и гарантия только для простых сравнений, e.g. `=` (хотя в разных DBs могут использоваться сложные техники и можно испольховать MIN() и MAX()); Создается hash таблица через разделение строк по buckets (узлам B-дерева). Требует дополнительных ресурсов т.к. сравнение по hash.
   * **Sort Merge Joins** - используется когда соединение по сортированному столбцу, работает как **Nested Loops Joins**, только новый шаг начинается не со следующего индекса, а с текущего (сортировка гарантировует - следующее значение не больше текущего). Теоритически быстрейший алгоритм.
3. **Table Hint** - **поведение оптимизатора на уровне таблиц** (locks, indexes, table scan, index seek).

**SQL statement lifecycle:**
1. **Parser** - принимает и валидирует **sql запрос** и создает **Query Tree** (syntax tree, parse tree). В виде **AST** (Abstract Syntax Tree). **Query Tree** - логическое представление запроса (т.е. операторы и значения из которых состоит запрос).
2. **Optimizer** - принимает **Query Tree** и создает **execution plan** (дерево операций которые нужны для получения данных); и кэширует **execution plan**. **Optimizer** имеет ограниченное время на поиск **execution plan** т.к. это затягивает время транзакции. Кэширует план запроса, чтобы ускорить работу, но кэшированный план может быть неоптимальный, нужно следить за этим (e.g. данные в табл могли измениться и план больше не подходит). Если в кэше плохой план, то это может замедлить работу. У кэша есть лимит и алгоритм устаревания, самый неиспользуемый план удаляется из кэша. Изменение структуры таблиц (DDL) может вызвать удаление **execution plan**. Лучший план запроса выбирается оптимизатором на основе статистики выполнения запросов и может меняться сам, но выбирается не всегда эффективно.
3. **Executor** - выполняет **execution plan** и возвращает **result set**

**Optimizer** - выполняет задачи:
1. Выбор лучшего **метода доступа** к данным для таблиц ссылающихся друг на друга: **table scan** или **index scan**
   * для **index scan** выберет лучший **index**
1. Выбор лучшего **типа join** для таблиц: **Nested Loops Joins**, **Hash Joins**, **Sort Merge Joins**
2. Выбор **порядка** таблиц в Join. Для **Nested Loops Join** первой в выражении должна идти табл. с меньшим числом строк. **Note.** Это делается автоматически, но не для всех типов join и в каждой DB алгоритм перестановки таблиц свой (e.g. при left join и cross join перестановки может не быть).

**Note.** Создание index и поиск по нему это дополнительные расходы на поиск по индексу и его изминение в случае обновление таблицы и увеличивает размера табл. Поэтому типы join которые не используют index эффективны с маленькими таблицами.

**Executor** - использует сгенерированный **execution plan** чтобы получить данные из **Storage Engine** и **Transaction Engine** для соблюдения транзакционности
 
**Алгоритмы выбора execution plan:**
* **Cost-Based Optimizer** (CBO) - самый популярный
* **Rule Based Optimization** (RBO)

**Стоимость плана запроса** (cost) состоит из:
1. Число **CPU cycles**
2. Число **I/O operations**
3. **Data cardinality** - количество различающихся values в столбце. High cardinality - когда много разных значений в столбце. Улучшение cardinality улучшает статистику запроса и оптимизатор выбирает лучший план. IDEs могут строить гистограмы показывающие **cardinality** столбца.

**selectivity** - параметр вычисляемый на основе **cardinality**, тоже учавствует в статистике для создания плана.
`selectivity = number of rows accessed/total number of rows`

**Основыне типы scan:**
1. **Full table scan** (sequence table scan) - описан выше
   * **Временная сложность:** O(n)
2. **Index scan** - описан выше
   * **Временная сложность:** O(log (n))

**Дополнительные типы scan** (спецефичны для DB):
1. **Index Only Scan**
2. **Bitmap Scan**
3. **TID Scan**

**Типы Join:**
1. **Nested Loops Joins** - join через вложенные циклы, на каждом шаге сравнивается value поля по которому делается join из одной табл. c полем в другой. Можно использовать когда нету index у поля. Если оба columns по которым будет join отсортированы, то будет работать быстрее, т.к. цикл может перескочить часть значений т.к. знает что они не подходят (т.к. они идут по порядку и понятно какое будет дальше). Сделать столбец сортированным можно через создание index или используя временный индекс (который может создать DB если посчитает что это дешево по ресурсам). Этот join эффективен когда хотя бы одна табл. маленькая (лучше обе маленькие), если этот join используется для больших табл., то возможно это ошибка выбора типа join. Этот join обычно требователен к CPU cycles. Чтобы ускорить его работу можно: 1. Использовать класторизованный индекс, 2. создать меньший index (by including a column in a key/column of an existing index), 3. Избавиться от `SELECT *` и возвращать только нужные столбцы.
   * **Временная сложность:** O(M*N) или O(M * log N) если по одному на одном из полей в join есть index
2. **Sort Merge Joins** - в теории самый быстрый join, но работает только когда поля по которым join отсортированы в обоих таблицах. Работает как **Nested Loops Joins**, но на каждом шаге поиск начинается не сначала, а со следующего шага, т.к. поля отсортированы, то все поля выше текущего шага меньше и не подходят под условие и поэтому их можно пропустить. Использует такие оптимизации как peek-ahead buffers, т.е. заранее читает следующие значения пока обрабатывает текущее, и другие оптимизации. Если поля по которым делается join имеют дубли, то создается временная **worktable** с дубликатами полей и используется сравнение этих дулей на следующем шаге цикла. **Sort Merge Joins** выбирается оптимизатором, если столбцы по которым будет join сортированы или если оптимизатору дешево отсортировать временно поле на время этого join. Оптимизатор выбирает **Sort Merge Joins** только если этот join правда эффективен и если он выбран, то с большей вероятностью это оптимальный тип join. Если оптимизатор сам временно сортирует поле для использования этого join, то нужно рассмотреть добавление index на это поле (т.е. сделать его сортированным, чтобы это не приходилось делать оптимизатору). При добавлении поля по которому нужно делать этот join в составной index последним столбцом эффективность падает не сильно (т.е. обычно это выгодно). Если таблицы содержат много дубликатов (например при `many-to-many`), то этот join может быть не эффективен, хотя и выбран оптимизатором. В случае когда много дубликатов **Nested Loops Joins** может быть эффективнее.
   * **Временная сложность:** O(M+N) в случае если есть index по полям, если индекса нет и сортировку делает оптимизатор, то - O(M log M + N log N), если только 1на табл имеет index то - O(M + N log N)
3. **Hash Match Joins** - самый универсальный тип join, хотя требует достаточно памяти, не требует сортированности полей. Состоит из 2х фаз: **Фаза 1 (build phase):** Создается хэш табл. из ключей 1ой табл. которая будет join, ключ bucket это хэш от поля по которому делается join, а значение это вся row таблицы. Как правило 1на row на 1ин bucket, кроме случая когда есть дубликаты полей по которому идет join или если случилась хэш коллизия (разные ключи имеют 1ин хэш). **Фаза 2 (probe phase):** Создается хэш таблица для 2ой таблицы которая join с 1ой табл. и хэши от ее ключей ищутся в 1ой хэш табл., если хэш совпал сверяется значение ключа. Этот join использует **CPU cycles** чтобы посчитать хэши, но зато может использовать временную табл. чтобы сделать join очень больших табл. При этом часть хэш табл. которая не помещается в память сохраняется на диск и будет подгружена когда понадобится. **Hash Match Joins** это блокирующая операция, т.к. пока не будет создана вся хэш табл. выполнение плана запроса заблокировано, поэтому этот join не всегда лучший. Использование временно табл., тоже не всегда эффективно, т.к. расходует CPU и I/O операции. Если можно добавить index, так чтобы оптимизатор выбрал другой тип join не использующий диск, то нужно это попробовать.
   * **Временная сложность:** O(M+N)

**На практике:**
```sql
-- Note. в разных DBs команды hint разные

-- смотрим план запроса
EXPLAIN SELECT * FROM one_million;

ANALYZE one_million; -- получаем статистику по табл. (по которой выбирается план)

-- выполняем и смотрим план запроса, чтобы получить настоящий план
EXPLAIN ANALYZE SELECT * FROM one_million JOIN half_million ON (one_million.counter=half_million.counter);

-- merge join hint в Postgres
SELECT /*+ USE_MERGE(a) */ b.bid, a.aid, abalance FROM branches b, accounts a WHERE b.bid = a.bid;

-- hash join + full table scan в Postgres
/*+ HashJoin(a b) SeqScan(a) */ SELECT FROM pgbench_branches b JOIN pgbench_accounts a ON b.bid = a.bid;
```

# Index
## Index tree
Источник: [1](https://habr.com/ru/company/mailru/blog/266811/), [2](https://habr.com/ru/company/citymobil/blog/514596/)

**Index tree** - двоичное дерево у которого нижний уровень это связный список, и только он хранит значения. Узлы выше хранят диапозоны для поиска (попадания на нужный диапазон нижнего списка узлов). По ветвям отсекаются поддеревья, у оставшейся ветви будет список значений, поиск конкретного значения далее идет по связному списку (как в бакетах структуры данных HashSet)

Если значения в столбце таблицы одинаковы, то ускорения фильтрации не будет, т.к. нельзя отсечь поддерево.

С диска загружаются не все узлы, а только нужные для работы - экономия ресурсов

**Скорость работы:** О(log(N))

Скорость перестройки индексов **О(log(N))**, поэтому **нужно создавать меньше индексов**, т.к. на перестройку каждого уходит время.

**Note.** Некоторые DB такие как MySQL и PostgreSQL **автоматически создают indexes** для **primary key** и **unique key** (т.е. в описании табл. будет видно что для этих столбцов созданы индексы)

```sql
CREATE INDEX idx_lastname
ON Persons (LastName); 

DROP INDEX index_name ON table_name;

-- дубликаты запрещены
CREATE UNIQUE INDEX index_name
ON table_name (column1, column2, ...)
```

## Index types

1. **B-tree Index (По умолчанию)**
   - **Описание**: Этот тип индекса является стандартным в PostgreSQL. Он используется для индексации данных, которые можно упорядочить (например, чисел, строк или дат). Это идеальный выбор для большинства запросов с операциями равенства и диапазонов.
   - **Скорость**: O(log n) — Быстрое выполнение операций поиска, вставки и удаления. Эффективен для небольших и средних таблиц, но производительность может снижаться при наличии большого объема данных.
   - **Структура данных**: Дерево поиска (B-дерево). Поиск происходит путем перемещения от корня дерева к листьям, где находятся значения. Каждый узел содержит диапазон значений, что позволяет быстро ограничивать область поиска.
   - **Когда применять**: Когда данные можно упорядочить, например, для колонок с числовыми или строковыми данными.
   - **Конкретный пример**: Индекс на поле даты в таблице заказов интернет-магазина для быстрого поиска заказов по дате.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_btree ON example_table (column_name);
     SELECT * FROM example_table WHERE column_name = 'some_value';
     SELECT * FROM example_table WHERE column_name BETWEEN 'start_value' AND 'end_value';
     ```
2. **Hash Index**
   - **Описание**: Использует хеш-функцию для индексации. Этот тип индекса не поддерживает операции диапазона, но эффективен для операций равенства. Не ищет по `is null`
   - **Скорость**: O(1) — Быстрое выполнение операций поиска и вставки, но ограничено только операциями равенства. Не подходит для операций диапазона. Быстрее для select и insert, но не update.
   - **Структура данных**: Хеш-таблица. Поиск выполняется путем хеширования значения и обращения к соответствующему бакету, где хранится список данных.
   - **Когда применять**: Когда вам нужно индексировать данные с операциями только равенства.
   - **Конкретный пример**: Индекс на поле "email" в таблице пользователей для быстрого поиска пользователя по адресу электронной почты.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_hash ON example_table USING hash (column_name);
     SELECT * FROM example_table WHERE column_name = 'some_value';
     -- Диапазоны не поддерживаются
     ```
3. **GiST (Generalized Search Tree) Index**
   - **Описание**: Гибкий индекс, который поддерживает поиск с использованием различных типов данных, таких как геометрические данные или полнотекстовый поиск.
   - **Скорость**: O(log n) — Зависит от типа данных и структуры. Эффективен для работы с многомерными данными, но может иметь дополнительные накладные расходы на поддержание структуры.
   - **Структура данных**: Обобщенное дерево поиска (generalized search tree). Поиск происходит через дерево, которое делит пространство на части, обеспечивая быстрый доступ к данным по их местоположению.
   - **Когда применять**: Для типов данных, которые не могут быть эффективно индексированы с помощью B-tree, например, для географических данных.
   - **Конкретный пример**: Индекс на поле "location" в таблице с географическими данными для поиска ближайших объектов.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_gist ON example_table USING gist (column_name);
     -- Поиск по расстоянию:
     SELECT * FROM example_table WHERE column_name <@ circle('(1,1)', 10); -- Найти все точки в круге с центром (1,1) и радиусом 10
     -- Пересечение:
     SELECT * FROM example_table WHERE column_name && box'(0,0,10,10)'; -- Найти все объекты, пересекающиеся с прямоугольником
     ```
4. **SP-GiST (Space-partitioned Generalized Search Tree) Index**
   - **Описание**: Расширение GiST, которое подходит для данных, которые можно разделить на части, например, для пространственных данных.
   - **Скорость**: O(log n) или лучше, зависит от типа данных и структуры. Может обеспечивать быстрее выполнение операций для специфичных типов данных, таких как многомерные или неполные.
   - **Структура данных**: Пространственно разделяемое дерево поиска. Поиск происходит путем разделения пространства на регионы, что позволяет эффективно обрабатывать многомерные данные.
   - **Когда применять**: Для географических или многомерных данных, которые имеют неполные или неупорядоченные данные.
   - **Конкретный пример**: Индекс на поле "coordinates" для обработки запросов о ближайших местах или объектах в географической информационной системе (ГИС).
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_spgist ON example_table USING spgist (column_name);
     -- Поиск по ближайшим соседям:
     SELECT * FROM example_table ORDER BY column_name <-> point'(1,1)' LIMIT 10; -- Найти 10 ближайших точек к (1,1)
     ```
5. **GIN (Generalized Inverted Index)**
   - **Описание**: Используется для индексации данных, которые могут содержать несколько значений, например, массивов или документов в формате JSON. Подходит для полнотекстового поиска.
   - **Скорость**: O(log n) для поиска, но может быть медленнее для операций вставки, особенно при частых обновлениях. Хорошо работает для поиска по множеству значений.
   - **Структура данных**: Обратный индекс (inverted index). Поиск происходит через обратный индекс, который хранит отдельные элементы коллекции, позволяя быстро находить все строки, содержащие нужные значения.
   - **Когда применять**: Для массивов, JSON данных или полнотекстового поиска.
   - **Конкретный пример**: Индекс на поле "tags" в таблице с товарами интернет-магазина для быстрого поиска по тегам товаров.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_gin ON example_table USING gin (column_name);
     -- Поиск по элементам массива:
     SELECT * FROM example_table WHERE column_name @> '{value1, value2}'; -- Найти строки, где массив содержит указанные значения
     -- Поиск по ключам в JSON:
     SELECT * FROM example_table WHERE column_name -> 'key' = 'value'; -- Найти строки, где значение по ключу 'key' равно 'value'
     ```
6. **RUM (Reverse Unified Memory) Index**
   - **Описание**: Это тип индекса, который является расширением GIN и позволяет эффективно индексировать данные с обратным поиском и поддерживать дополнительную функциональность.
   - **Скорость**: O(log n) для поиска, с дополнительными накладными расходами на поддержание структуры индекса. Может обеспечивать лучшую производительность для полнотекстового поиска по большому количеству данных.
   - **Структура данных**: Расширенный обратный индекс (inverted index). Поиск выполняется через обратный индекс с дополнительной возможностью обработки обратных запросов.
   - **Когда применять**: Для эффективного поиска по данным, содержащим большое количество записей, таких как полнотекстовые данные.
   - **Конкретный пример**: Индекс на поле "content" для быстрого полнотекстового поиска по новостным статьям в базе данных новостного сайта.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_rum ON example_table USING rum (column_name);
     -- Полнотекстовый поиск:
     SELECT * FROM example_table WHERE column_name @@ to_tsquery('some query');
     ```
7. **BRIN (Block Range INdex)**
   - **Описание**: Индекс для хранения больших объемов данных, где данные физически упорядочены в блоках. Он подходит для временных рядов и других типов данных с большим количеством последовательных значений.
   - **Скорость**: O(1) для поиска по диапазону, очень эффективен для чтения больших таблиц, но менее эффективен при частых изменениях данных.
   - **Структура данных**: Блочный диапазон (block range). Поиск происходит путем поиска в блоках данных, которые логически связаны друг с другом, что ускоряет поиск в упорядоченных данных.
   - **Когда применять**: Для больших таблиц с последовательными или слабо изменяющимися данными.
   - **Конкретный пример**: Индекс на поле "timestamp" в таблице логов для эффективного поиска по временным диапазонам.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_brin ON example_table USING brin (column_name);
     -- Поиск по диапазону:
     SELECT * FROM example_table WHERE column_name >= '2023-01-01' AND column_name < '2023-02-01';
     ```
8. **Bloom Index**
   - **Описание**: Специализированный индекс для быстрого выполнения запросов с множеством условий равенства. Он использует битовые массивы для проверки наличия данных.
   - **Скорость**: O(k), где k — количество проверяемых столбцов. Очень эффективен для многокритериальных запросов с равенствами, но может иметь ложные срабатывания.
   - **Структура данных**: Блум-фильтр. Поиск выполняется через битовую карту, которая позволяет быстро проверять наличие или отсутствие элементов с минимальными затратами памяти.
   - **Когда применять**: Когда вам нужно искать по нескольким столбцам с операциями равенства.
   - **Конкретный пример**: Индекс на поля "email" и "phone_number" в таблице пользователей для быстрого поиска пользователя по любому из этих двух параметров.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_bloom ON example_table USING bloom (column1, column2);
     -- Поиск по нескольким столбцам:
     SELECT * FROM example_table WHERE column1 = 'value1' AND column2 = 'value2';
     ```
9. **Expression Index**
   - **Описание**: Индекс, основанный на выражении, а не на конкретном столбце. Позволяет индексировать результаты выражений или функций.
   - **Скорость**: O(log n) — Как правило, это такая же скорость, как у обычного B-tree, но зависит от вычисляемых выражений.
   - **Структура данных**: B-дерево, если используется стандартный индекс. Поиск происходит как в обычном B-дереве, но на основе вычисленных значений выражений.
   - **Когда применять**: Когда вы хотите индексировать вычисляемое значение или результат функции.
   - **Конкретный пример**: Индекс на поле "lower(email)" в таблице пользователей для быстрого поиска по электронной почте, игнорируя регистр.
   - **Пример SQL**:
     ```sql
     CREATE INDEX idx_example_expression ON example_table ((lower(column_name)));
     -- Поиск по выражению:
     SELECT * FROM example_table WHERE lower(column_name) = 'some_value';
     ```
10. **Partial Index**
    - **Описание**: Индекс, который создается только для подмножества данных, соответствующих определенному условию. Это может значительно уменьшить размер индекса.
    - **Скорость**: O(log n) — Эффективен для подмножеств данных, но может быть менее эффективен, если условия фильтрации слишком сложные или часто изменяются.
    - **Структура данных**: Обычно использует B-дерево или другие структуры в зависимости от выбранного типа индекса. Поиск происходит через стандартное дерево с применением фильтрации.
    - **Когда применять**: Когда индексация нужна только для определенных строк, например, для активных или часто используемых данных.
    - **Конкретный пример**: Индекс на поле "status" в таблице заказов для быстрого поиска только активных заказов.
    - **Пример SQL**:
    ```sql
    CREATE INDEX idx_example_partial ON example_table (column_name) WHERE column_name IS NOT NULL;
    -- Поиск с условием:
    SELECT * FROM example_table WHERE column_name = 'some_value' AND status = 'active';
    ```
11. **Multicolumn Index**
    - **Описание**: Индекс, который используется для нескольких столбцов. Этот индекс эффективен при запросах, которые используют несколько столбцов в условиях поиска. Будет работать только для `(a), (a,b) и (a,b,c)`, но не будет работать для `(b), (b,c), (c)`
    - **Скорость**: O(log n) — Обычно работает так же эффективно, как и одиночные индексы, но может быть менее эффективен для запросов, которые используют только один из столбцов.
    - **Структура данных**: B-дерево или другие структуры в зависимости от типа индекса. Поиск происходит через объединение значений нескольких столбцов в одну структуру для более быстрого поиска.
    - **Когда применять**: Когда запросы часто включают несколько столбцов в условиях.
    - **Конкретный пример**: Индекс на полях "first_name" и "last_name" для быстрого поиска пользователей по полным именам.
    - **Пример SQL**:
      ```sql
      CREATE INDEX idx_example_multicolumn ON example_table (column1, column2);
      -- Поиск по нескольким столбцам:
      SELECT * FROM example_table WHERE column1 = 'value1' AND column2 = 'value2';
      ```
12. **Clustered index** - в Postgres нету, все индексы non-clustered. В index хранится значение, а не ссылка, чтение быстрее.

# Основные типы JOIN

**Заметки:**
1. **from table1 inner join table2 ... on ...** это синоним `from table1, table2 ... where ...`, `inner join ... on` считается лучше читаемым
2. просто `join` это синоним для `inner join`
3. **cross join** не бесполезен (как некоторые пишут), он может быть использован например для заполнение сетки значениями из табл. Для примера две табл size и color одежды, результат все размеры и цвета.
4. **self join** применим когда табл. ссылается сама на себя (будет работать быстрее чем `group by`). Самого оператора `self join` нету, нужно писать реализацию через: `where a.id <> b.id`
5. **left inner join** и **right inner join** не существует
6. Для работы `join` **не нужны** Primary Key и Foreign Key. Будет работать, если **типы** столбцов совпадают или сконвертированы в совместимый тип.

**Типы join:**
1. **inner join** - пересечение
2. **left (outer) join** - пересечение, плюс все строки из 1ой (левой) табл, если `on` не выполняется для строк то пустые ячейки заполняются null
3. **right (outer) join** - пересечение, плюс все строки из 2ой (правой) табл, если `on` не выполняется для строк то пустые ячейки заполняются null
4. **full (outer) join** - как `left outer join` и `right outer join` одновременно, если `on` не выполняется для строк то пустые ячейки заполняются null
5. **cross join** - все строки обоих табл. со всеми (**декартово произведение**). Отличается от `full outer join` тем, что соединение с пустой табл. тоже пусто (в `full outer join` соединение с пустой табл. будет иметь строки только одной из табл). Для `cross join` не нужно указывать условие (то что после `on`), пример:
    ```sql
    select *
    from a cross join b
    ```
6. **self join** - пересечение табл с самой собой. Полезно например, чтобы выбрать из одной табл. уникальные пары значений из столбцов без `group by`. Т.е. `self join` применим когда табл. ссылается сама на себя. Пример (в `self join` неравенство вместо равенства в `where`):
    ```sql
    SELECT a.name, b.name
    FROM a, b
    WHERE a.id <> b.id
    AND a.City = b.City
    ORDER BY a.City;
    ```
7. **natural join** - как `innet join`, но делается по **всем** столбцам **с одинаковым именем** (условие писать не нужно) и это может быть медленным. Результат будет все **разные colums** всех табл. которые **join**. Еще есть `natural left` и `natural right`
    ```sql
    select *
    from t1 natural join t2; -- t1: c1, c2; and t2: c1, c3; result is: c1, c2, c3
    ```
8. **Другие типы join** - они могут быть специфичными для определенных DBs или устаревшими

**Дополнительные варианты:**
1. **вариант left (outer) join**, только исключая само пересечение, результат только левая часть строк. АНАЛОГИЧНО и для `right (outer) join`
    ```sql
    select *
    from a
    left join b
    on a.id = b.id
    where b.id is null; -- "where a.id is null" для right (outer) join
    ```
2. **вариант full (outer) join**, только исключая само пересечение, результат только левая и правая часть строк
    ```sql
    SELECT a.name, b.name
    FROM a
    FULL OUTER JOIN a
    ON a.id = b.id
    where a.id is null or b.id is null;
    ```

**Пример inner join нескольких таблиц:**
```sql
select *
from a
inner join b on a.id = b.id
inner join c on b.id = c.id;
```
    
**Пример inner join через where нескольких таблиц:**
```sql
select *
from a, b, c
where a.id = b.id and b.id = c.id;
```

# Partitioning
Source: [1](https://docs.arenadata.io/ru/ADB/current/concept/data-model/tables/partitioning.html)

**Partitioning** - разделение 1ой табл на несколько частей. PostgreSQL автоматически вставляет данные в партицию на основе ключа (не нужно делать в приложении). Если запись не попадает в диапазон значений ни одной из партиций, она будет добавлена в **default** партицию.

PostgreSQL не поддерживает автоматическое создание новых партиций "на лету". Если запись попадает за пределы существующих диапазонов, она либо попадёт в партицию по умолчанию, либо произойдёт ошибка.

* Для разбиения выбирается столбец который часто используется в `WHERE`.  
* Если не использовать ключ партиционирования в запросах, PostgreSQL может проверять все партиции, это снижению производительности.  
* Не делать партиции слишком меленькими.  
* Не делать слишком много партиций (сотни или тысячи)  
* Использовать `PARTITION BY HASH` для лучшего распределения значений по партициям если нужно.  
* Каждая партиция имеет свои собственные индексы.  
* `CHECK` - автоматически добавляется к ключу партиции, используется для прунинга - исключения нерелевантных партиций из запроса  
* PostgreSQL не поддерживает **foreign key** в родительских таблицах с партициями (проверка ссылочной целостности делать в приложении).  
* `Unique` индексы можно создавать только на колонках, включающих ключ партиционирования.  
Partitioning удобно для исторических данных, партиции со старыми можно удалять.
* Типы
  * **Range partitioning** - разбиение по диапазонам значений, напр. датам
  * **List partitioning** - по конкретным значениям, напр. категориям
  * **Hash partitioning** - с использованием хэш-функции от значений (например, для равномерного распределения данных по партициям)
* Ограничения
  * максимум 32767 партиций
  * Если таблица содержит primary key или UNIQUE — они должны быть указаны в partiotioning key.
  * таблицы созданные с политикой DISTRIBUTED REPLICATED - не могут быть партиционированы
  * Загрузка данных в партиционированные таблицы неэффективна, рекомендуется загружать данные в промежуточную (staging) таблицу и затем применять к партиционированной таблице команду EXCHANGE PARTITION.
  * Оптимизаторы запросов могут выборочно сканировать записи партиционированной таблицы только в случае, если предикат запроса содержит: Immutable-операторы, такие как: =, <, <=, >, >= и <>, или функции с типом STABLE и IMMUTABLE (не VOLATILE).

```sql
CREATE TABLE orders ( order_id PRIMARY KEY, order_date DATE NOT NULL )
PARTITION BY RANGE (order_date);
-- PARTITION BY LIST (country);
-- PARTITION BY HASH (customer_id);

-- создание партиции
CREATE TABLE orders_2023_q1 PARTITION OF orders FOR VALUES FROM ('2023-01-01') TO ('2023-04-01');

-- DEFAULT партиция для тех кто не попадает не в одну
CREATE TABLE orders_default PARTITION OF orders DEFAULT;

-- 2023-02-15 попадет в orders_2023_q1, 2023-05-20 попадет в orders_2023_q2
INSERT INTO orders (customer_id, order_date, amount)
VALUES (1, '2023-02-15', 100.00), (2, '2023-05-20', 150.00);

-- PostgreSQL оптимизирует запросы, обращаясь только к релевантным партициям (так называемая прунинг партиций).
SELECT * FROM orders WHERE order_date >= '2023-01-01' AND order_date < '2023-04-01';

-- Удаление старых партиций: Если данные за определенный период больше не нужны
DROP TABLE orders_2023_q1;

-- Индексы на партициях: Индексы создаются отдельно для каждой партиции:
CREATE INDEX idx_orders_q1_customer_id ON orders_2023_q1 (customer_id);

```

# Row Pattern Recognition
https://modern-sql.com/blog/2017-06/whats-new-in-sql-2016

# Scrolled cursors
https://en.wikipedia.org/wiki/Cursor_(databases)#Scrollable_cursors

# Списки инструментов

* Для mysql http://shlomi-noach.github.io/awesome-mysql/
* Для Postgres https://github.com/dhamaniasad/awesome-postgres
* вики по sql инъекциям https://sqlwiki.netspi.com/
