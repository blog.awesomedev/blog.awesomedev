- [Common](#common)
- [Kafka](#kafka)
  - [Общее](#общее)
  - [Kafka на практике](#kafka-на-практике)
  - [Examples](#examples)
  - [Kafka Streams](#kafka-streams)
  - [Kafka SQL Streams, KSQL, ksqlDB](#kafka-sql-streams-ksql-ksqldb)
  - [Apache Kafka](#apache-kafka)
  - [Spring Kafka](#spring-kafka)
  - [Spring Cloud Stream](#spring-cloud-stream)
  - [Kafka Connector](#kafka-connector)
  - [Kafka ACL](#kafka-acl)
  - [Soft](#soft)
- [jms vs kafka](#jms-vs-kafka)
- [Apache Kafka vs. Integration Middleware (MQ, ETL, ESB)](#apache-kafka-vs-integration-middleware-mq-etl-esb)

Источники:
* общее о том как сервисы взаимодействуют и некоторые формулы расчета нагрузки [тут](https://dzone.com/articles/tens-of-thousands-of-socket-connections-in-java)
* общий состав элементов jms [тут](https://www.javacodegeeks.com/jms-tutorials)

# Common
Source: [1](https://habr.com/ru/company/southbridge/blog/550934/)

**Message Broker Life Cycle:**
1. Producer send message
2. Consumer **fetch** message with an id
3. message get **in-flight** state, message still exists in `kafka/jms server`
4. Consumer handle message and send to `kafka/jms server` signal **ack** or **nack**
5. `kafka/jms server` deletes the message if there is **ack** and retry if **nack** or **timeout**

**pull vs push** - model that used by message broker to send messages to consumers
1. **pull** - consumer send heartbeet to server each n sec. to say that it what to get another batch of messages
   * **Pros.** Batch controlled by consumer, high load controlled by consumer (better throughput)
   * **Cons.** Potential worse load balancing, higher delay
   * **Note**. Used by **Kafka**
2. **push** - server send message(s) to consumer
   * **Pros.** Lower delay, better load balancing
   * **Cons.** Worse consumer high load control
   * **Note**. Used by **RabbitMQ**

# Kafka
## Общее
Источники:  habr ([0](https://habr.com/ru/company/piter/blog/352978/), [1](https://habr.com/ru/company/avito/blog/465315/), [2](https://habr.com/ru/post/440400/), [3](https://habr.com/ru/post/354486/)), baeldung ([1](https://www.baeldung.com/apache-kafka-data-modeling), [2](https://www.baeldung.com/kafka-purge-topic), [3](https://www.baeldung.com/kafka-message-retention),
[3](https://www.baeldung.com/kafka-connectors-guide)),
medium Kafka Consumer Overview ([1](https://medium.com/@sdjemails/kafka-consumer-overview-cfcc25044c2e),
[2](https://medium.com/@sdjemails/kafka-technical-overview-15791ef6b7c4),
[3](https://medium.com/@sdjemails/kafka-producer-overview-4c44b1b9ece1),
[4](https://medium.com/@sdjemails/kafka-producer-delivery-semantics-be863c727d3f))

Kafka - horizontally scalable, fault tolerant and fast messaging system

что может: иметь 100ни нод, обрабатывать миллионы сообщений в сек, real time обработка сообщений (не дольше 10ms)

**Kafka** был разработан в компании LinkedIn в 2011 году.

**Note.** Kafka иногда называют (можно запомнить к собеседованию) распределенный горизонтально масштабируемый отказоустойчивый журнал коммитов.

**Плюсы kafka:**
* Decoupling of Data Streams. - потоки данных не зависят друг от друга **Note.** возможно имеется ввиду Reactive подход к обработке данных
* Real time instead of batch processing - работа с сообщениями сразу вместо ожидания пока они накопятся, а потом проведение операций сразу с группой

**Note.** kafka также как и JMS называют **middle layer** (но будет ли она им зависит от того как ее использовать)

* **для чего используют kafka**
  * Streaming processing
  * Tracking user activity, log aggregation, etc…
  * De-coupling systems
* **Broker** - сервер или несколько серверов (тогда между ними разделяется нагрузка). Если вы соеденены с одним Broker, то вы соеденены со всеми (со всем кластером, т.е. можно не думать о ручном соединении с каждым сервером и просто работать с ними как с одним). Если 1ин Broker падает, то данные **replicated** (будут получены) из другого.
  * Сообщения **immutable**
* **Record** (message, event) - состоит из: **key**, **value**, **timestamp**, optionally has **headers** (metadata)
  * Popular Producer Headers: 
  * Popular Consumer Headers: 
  * **Producer Record fields** - value - единственное обязательное поле 
    * partition
    * timestamp
    * key
    * value
* **Key** - используется для назначения partition, если нет, то будет назначена случайная partition
* **Topic** (log, event stream, group) - место где хранятся сообщения
* **Partition** - каждый topic разбит (можно разбить) на **Partition**s. Копии **Partition**s разбросаны по разным копиям Brokers (если Brokers несколько). В каждом **Broker** есть свой **leader** (лидер) и **ISRs** (in sync replicas, синхр. реплики), т.е. главный и второстепенные **Partition**s. Записать в **topic** идет через **leader**. Разделение на **partitions** увеличивает производительность, по умолчанию topic только 1ин **partition**. При отправке **Producer** генерирует hash для key сообщения используя номер **partition** (e.g. partition num + key = message index). Каждый **Partition** привязан к каталогу в файловой систему, поэтому их количество ограничено файловой системой (log файлы). **Partition** используются для **horizontal scaling**.
  * **Note.** Рекомендуемое число **Partition**s: `100*b*r`, где b - количество brokers, r - replication factor
* **Offset** - сообщение в **Partition** расположено со смещением (**offset**) относительно чего-то
  * **unique** в рамках partition
* **Producer** - приложение или кусок приложения посылающий сообщения в **Kafka** (в брокер(ы)) **Note.** (неважная инфа) **Producer** иногда называют **генератором**.
* **Consumer** - приложение или кусок приложения слушающий сообщения. **Note.** (неважная инфа) **Consumer** иногда называют **потребителями**.
* **Consumer Group** - несколько consumer можно соединить в группу, тогда **records** из **topic** они будут получать по очереди (т.е. разным consumer придут разные records, как балансировка нагрузки)
* **Zookeeper** - это DB, которая работает как Map (ключ-значение), оптимизировано для чтения и медленнее для записи. Она нужна для работы Kafka как зависимость и ее нужно разворачивать как отдельный сервер. Брокеры Kafka подписываются на события изменения данных в **Zookeeper** (в том числе на событие изменения **leader** брокера на другого **leader**). Zookeeper **отказоустойчив**.
  * **Он используется для хранения всевозможных метаданных, в частности:**
    * Смещение групп потребителей в рамках секции (хотя, современные клиенты хранят смещения в отдельной теме Kafka)
    * **ACL** (access control level, списки контроля доступа) — используются для ограничения доступа /авторизации
    * Квоты генераторов и потребителей — максимальные предельные количества сообщений в секунду 
    * Ведущие секций и уровень их работоспособности
    * Рекомендуется не меньше 2х нод
* `__consumer_offset` - спец топик с offset для каждого consumer
* 1ин consumer читает только 1ну partition
  * если количество consumers больше чем количество partitions, то часть consumers будут простаивать
  * если количество consumers меньше чем partitions, то 1ин consumer будет читать несколько partitions
* **heartbeat** - каждый consumer в consumer group делает запросы **coordinator** с опросом доступности nods кафки, если поставить слишком частыми, то при проблемах сети будут проблемы. При **timeout** считается что consumer потерян и начинается **Rebalance**
* **handshake processes**
  * Find coordinator - возвращает данные о coordinator broker
  * Join group - возвращает данные о leader и group metadata
  * Sync group - назначает partition какому-то consumer
  * Heartbeat - отправка producer сообщения broker что он жив
  * Leave group - producer может покинуть группу
* **Rebalance** - при добавлении/удалении consumer происходит Sync group, вся передача данных в consumer group будет остановлена пока partitions не будут переназначены
  * Это очень затратная операция, может вызвать загрузку CPU
* **Kafka delivery semantics** - проверять или нет, что сообщение доставлено и как себя вести в случае потерь
  * `at most once` - не получать дубликаты, скорее потеряет сообщение чем будет дубликат, используется для логов и метрик (higher throughput and low latency), нет retry. Producer не ждет ack от broker.
  * `at least once` - рекомендуемая обычно, без потерь, но с возможными дубликатами (moderate throughput and moderate latency). Ack отправляется producer когда leader получил сообщение, но до того как оно реплицировалось на другие инстансы кафки (может вызвать потери сообщений)
  * `exactly once` - только 1 всегда (lower throughput and higher latency). Когда сообщение получено leader и реплицировалось на количество follower brokers зависящих от replication factor и `min.insync.replica`. Риск потери сообщений минимально, т.к. retry в случае падения будет к новому leader. Если `min.insync.replica` недостаточно, то будет exception.
* **Safe producer** - настройки чтобы минимизировать потери данных
  * ```
    # Producer properties
    Acks = all (default 1)
    Retries = MAX_INT (default 0)
    Max.in.flight.requests.per.connection = 5 (default)

    # Broker properties
    Min.insync.replicas = 2 (at least 2) — Ensures minimum In Sync replica (ISR).
    ```
* **producer workflow** - процесс отправки сообщения
  * Serialize
  * Partition - решает в какой partition послать, если key нет, то round-robin. by default использует алгоритм **murmur2** для hashcode
  * Compress - сжатие, не включено по умолчанию
  * Accumulate records - по `batch.size` делает буфер для batch в рамках своей partition чтобы отправлять пачками, linger.ms - max задержка при отправке
  * Group by broker and send
* **Duplicate message detection** - алгоритм обработки дубликатов сообщений, если `acknowledgment` о доставке после отправки сообщения не получено producer'ом от broker, то будет повторная отправка дубликата из producer. На основе producerId+sequence_number сохраняется состояние (отправлено или нет), когда producer получил `ack` от broker, то помечает дубликат как sent.
* **Log Compaction** - функция удаления старых записей при получении **null**, записи удаляются не сразу, чтобы они могли обработаться (напр. delete сообщения для реплик). Compacted создаются отдельные топики, конфиг `cleanup.policy=compact` для топика, и `delete.retention.ms` (24h default) - сколько живет сообщение перед удалением чтобы успеть обработаться consumer
  * ```
    # topic config
    cleanup.policy=compact

    #
    delete.retention.ms (default 24 hours) — time before deleting data marked for compaction.
    min.cleanable.dirty.ratio (0.5 default)
    segnemt.ms (default 7 days) — Max time before closing active Segment.
    segment.bytes (default 1G) — Max Segment size.
    min.compaction.lag.ms (default 0) — time before the message can be compacted.
    min.cleanable.dirty.ratio (default 0.5) — Cleaning efficiency.
    ```
* **Message Retention** - сколько будет существовать сообщение в кафка перед удалением, в кафка обычно выбирают большой срок, напр. месяц или вечно, зависит от требований
  * ```
    # server config
    log.retention.hours
    log.retention.minutes
    log.retention.ms
    log.retention.check.interval.ms - как часто проверять retention у сообщений 

    # topic config
    ./alter-topic-config.sh test-topic retention.ms 300000
    ```
* **kafka api** - полный список api
  * **Consumer API** — API для потребителей (Consumers), которые подписываются на топики и читают данные из них.
  * **Producer API** — API для производителей (Producers), которые отправляют данные в Kafka.
  * **Kafka Streams API** — Надстройка над Consumer API. Высокоуровневое API для обработки потоковых данных в реальном времени, позволяющее анализировать и трансформировать данные прямо в Kafka.
  * **Kafka Connect API** — API для интеграции с внешними системами (например, базы данных, файловые системы), позволяющее легко подключать источники данных и приемники.
  * **Admin API** — API для администрирования и управления кластером Kafka, например, создания топиков, управления партициями и правами доступа.
  * **KSQL API** — работа с топиками и данными как в стиле sql

**Журнал коммитов** («журнал опережающей записи», «журнал транзакций») – это долговременная упорядоченная структура данных, причем, данные в такую структуру можно только добавлять. Записи из этого журнала нельзя ни изменять, ни удалять. Информация считывается слева направо; таким образом гарантируется правильный порядок элементов.  
**Note.** Т.е. это что-то вроде stack структуры, но удалять из нее нельзя.  
**Note.** `o(1)` (если известен ID записи) - временная сложность для чтения и записи (быстрее чем у структуры дерево).  
**Note.** Операции считывания и записи не влияют друг на друга (операция считывания не блокирует операцию записи и наоборот, чего не скажешь об операциях со сбалансированными деревьями).

**Особенности**
* Если данные записаны в **Partition** они не могут быть изменены (Immutability)
* Данные в **Partition** хранятся ограниченный срок (в настройках установлен limit по времени).
* **leader** это тот Broker где лежит файл log (с сообщениями), остальные серверы это те куда log копируется для надежности. log файл распределенный т.к. лежит на нескольких носителях (там лежат копии log файла).
* В Kafka нет **poll time**, т.е. consumer запросит новое сообщение сразу после **commit offset** (после обработки текущего)

**Note.** Разнесения Brokers по разным компьютерам называют горизонтальным маштабированием. А сами брокеры иногда называют узлами.

**Параметры kafka:**
* **kafka-topics**
  * **--zookeeper localhost:32181** - адрес zookeeper
  * **--partitions 1** - количество partitions
  * **--replication-factor 1** - replication-factor, количество копий реплик kafka
* **kafka-console-producer**
  * **--broker-list localhost:19092,localhost:29092** - список реплик (?) kafka к которым подключиться
* **kafka-console-consumer**
  * **--bootstrap-server localhost:19092,localhost:29092** - аналог параметра **--zookeeper**, в новых версиях kafka подключаться можно к самой kafka и она отправляет metadata по которым consumer понимаю как и что принимать. Если сервер к которому подключиться не **leader**, то kafka к которой подключаются отдаст consumer адрес **leader** и **consumer** сам переподключиться.
* **kafka-configs**
  * **--alter --add-config [PROPERTY_NAME]=[VALUE]** - добавить параметр
  * **--describe** - вывести параметры
  * **--alter --delete-config [PROPERTY_NAME]** - удалить параметр

## Kafka на практике
* Сообщения в kafka хранятся долго, так что при **падении и поднятии microservice** они получат все данные
* Можно создать спец **snapshot topic**s (название неофициальное) в kafka где хранить вообще все записи из DB табл., и заполнять их e.g. 1 раз в 2 недели, тогда **новые microservice** получат все записи когда будет первый deploy. Время их наполнения можно установить равным времени очистки обычных topic, тогда все записи которые очистились из обычного topic всегда могут быть начитаны из **snapshot topic**
* Имя **topic** может соотв. имени microservice и dto для какой-то записи, например **consumer-msUser-roleGroupDto**
* При задании name для topic можно использовать доп суфикс в названии для version, например для dto которое может поменять структуру: **consumer-msUser-roleGroupDtoV2**
* Spring Boot использует AutoConfigure только с **1ой** kafka, если нужно connection с множеством **kafka clusters**, то нужно создавать отдельный набор beans
* Если нужно соблюдать порядок отправки-приема сообщений, то отправлять можно только в **1ин** и тот же **partition**, выбор в какой **partition** отправится сообщения делается на основе **key** `(т.е. нужен алгоритм его составления)`. При этом если например есть 10 табл в **DB**, то каждая запись из них может быть отправлена в свой **partition**, если важен порядок для каждой конкретной отдельно, а не для всех `(т.е. алгоритм составления key будет использовать алгоритм в который для каждой табл. подставляется свои имя)`. Т.к. **partition** это по сути отдельный **thread** со своим порядком сообщений.
* Если нужно гарантированно доставить сообщение, то используется режим **exactly once**, при этом используется **kafka transaction** чтобы убедиться что сообщение доставлено, каждой **transaction** нужно задать уникальный **transactionId**.
* Каждому topic можно назначить отдельный сертификат отдельно для read or write, в Spring Boot нужно указать путь к соотв сертификату при использовании topic
* в Spring Boot авто конфигурируется только один Kafka cluster, если нужно несколько, то нужно создавать beans вручную
* **compacted topic** - режим compacted в kafka при котором в topic хранится только 1на последняя версия message по key. Для этого режима message обязан иметь key. Compact удаляет дубли не сразу, а через время указанное в настройках, т.е. дубли могут быть какое-то время и попадут в consumer. Чтобы удалить record из compacted topic нужно отправить null в record, удаление произойдет не сразу, а как и для обычной record когда истечет время хранения record.
  * **Note.** Полезно наприимер когда есть custom реализация Outbox паттерна при которой в kafka хранится **вся** record, а не только измененные поля и при этом самая последняя актуальная версия record.
* если в spring в качестве ключа передать null, то kafka все отправит только в 1 partiotion (возможно не для всех версий kafka client lib)
* если ключа нет, то kafka отправляет сообщения по partition алгоритмом round robin
* При **send** для record в kafka есть ожидание ответа для consumer что record доставлена. Поэтому send **нельзя делать в transaction**, т.е. из-за задержки transaction/connection может быть занята все время пока есть ожидание ответа, если есть какие-то активности kafka по синхронизации между brokers или др., то это время **может** быть существенно долгим.
* Если при обработке message не подавить Exception, но можно получить DDoS от kafka при retry

## Examples
Создание topic
```
kafka-topics --create --topic chess-moves --if-not-exists --partitions 1 --replication-factor 1 \
  --zookeeper localhost:32181
```
Отправка kafka-console-producer - утилита
```
kafka-console-producer --broker-list localhost:19092,localhost:29092,localhost:39092 \
--topic chess-moves \
--property parse.key=true --property key.separator=:
>{Player1: Rook, a1 -> a5}
>{Player2: Bishop, g3 -> h4}
```
Прием, kafka-console-consumer - утилита
```
kafka-console-consumer --bootstrap-server localhost:19092,localhost:29092,localhost:39092 \
--topic chess-moves --from-beginning \
--property print.key=true --property print.value=true --property key.separator=:
>{Player1: Rook, a1 -> a5}
>{Player2: Bishop, g3 -> h4}
```
Разделяем topic на partiotions
```
kafka-topics --alter --zookeeper localhost:32181 --topic chess-moves --partitions 3
```
Consumer для отдельного partition, нумерация с 0 (подписка на 2ую partition)
```
kafka-console-consumer --bootstrap-server localhost:19092,localhost:29092,localhost:39092 \
--topic chess-moves --from-beginning \
--property print.key=true --property print.value=true \
--property key.separator=: \
--partition 1
{Player2: Bishop, g3 -> h4}
{Player2: Bishop, h4 -> g3}
```
Устанавливаем replication-factor - количество log файлов для одного topic (количество brokers?)
```
kafka-topics.bat --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic out-topic
```
Создаем consumer group
```
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic users.registrations --group new-user
kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic users.registrations --group new-user
```
Список consumers
```
kafka-consumer-groups.sh --list --bootstrap-server localhost:9092 new-user
```
Список consumers в group
```
kafka-consumer-groups.sh --describe --group new-user --members --bootstrap-server localhost:9092
```
Список topics
```
kafka-topics.sh --list --zookeeper localhost:2181
kafka-topics.sh --bootstrap-server=localhost:9092 --list
```
Детали про topic
```
kafka-topics.sh --bootstrap-server=localhost:9092 --describe --topic users.registrations
```
Настройки kafka (параметры)
```
kafka-configs --zookeeper [ZKHOST] --entity-type topics --entity-name [TOPIC] --alter --add-config [PROPERTY_NAME]=[VALUE]
kafka-configs --zookeeper [ZKHOST] --entity-type topics --entity-name [TOPIC] --describe
kafka-configs --zookeeper [ZKHOST] --entity-type topics --entity-name [TOPIC] --alter --delete-config [PROPERTY_NAME]
```
**Настройки**
* Общий файл `/etc/kafka/config/server.properties`
* Отдельно для сервера `/etc/kafka/config/server-2.properties`
```properties
retries: 3 # переотправка в случае ошибки consumer или др
retry-backoff-ms: 20000
replication-factor: 3 // рекомендуется 3 для баланса, количество broker для резервных копий
reconnect-backoff-ms: 60000
max-poll-records: 1000 // batch пачка messages на 1 чтение, максимум за установленное время
session-timeout-ms: 300000
min.insync.replica - от него зависит режим ack

fetch.min.bytes
max.partition.fetch.bytes
fetch.max.bytes
enable.auto.commit

# producer props
batch.size - количество сообщений для batch, пачки формируются в рамках partition
linger.ms
key.serializer
value.serializer
compression.type - тип сжатия, отключено по умолчанию
buffer.memory - размер буфера для отправки batch сообщений
retries - количество retry, 0 by default, может вызвать out of order messages, можно поставить значение Max.INT
max.in.flight.requests.per.connection - количество которое отправляет без ack, 5 by default, чтобы исключить out of order messages нужно поставить в 1
max.request.size - max размер сообщения, 1мб by default
min.insync.replica
acks - 0 - at most once, 1 - at least once, all - exactly once
```

## Kafka Streams
Source: [1](https://www.baeldung.com/java-kafka-streams-vs-kafka-consumer), [2](https://kafka.apache.org/documentation/streams/)

api для работы с сообщениями как stream, меньше кода, построен на consumer api, не поддерживает batch обработку, поддержка мультипоточности, поддерживает stateful обработку (фции стримов windowing, aggregation)

* KStream - поток всех сообщений
* KTable - поток последних сообщений по key
* GlobalKTables 

```java
// <dependency>
//     <groupId>org.apache.kafka</groupId>
//     <artifactId>kafka-clients</artifactId>
//     <version>3.4.0</version>
// </dependency>

// <dependency>
//     <groupId>org.apache.kafka</groupId>
//     <artifactId>kafka-streams</artifactId>
//     <version>3.4.0</version>
//  </dependency>

StreamsBuilder builder = new StreamsBuilder();
KStream<String, String> textLines = 
  builder.stream(inputTopic, Consumed.with(Serdes.String(), Serdes.String()));

KTable<String, String> textLinesTable = 
  builder.table(inputTopic, Consumed.with(Serdes.String(), Serdes.String()));

GlobalKTable<String, String> textLinesGlobalTable = 
  builder.globalTable(inputTopic, Consumed.with(Serdes.String(), Serdes.String()));

ReadOnlyKeyValueStore<String, Long> keyValueStore = streams.store(StoreQueryParameters.fromNameAndType(
    "WordCountsStore", QueryableStoreTypes.keyValueStore()));

KStream<String, String> textLinesUpperCase = textLines
    .map((key, value) -> KeyValue.pair(value, value.toUpperCase()))
    .filter((key, value) -> value.contains("FILTER"));
```

## Kafka SQL Streams, KSQL, ksqlDB
Source: [baeldung ksqlDB](https://www.baeldung.com/ksqldb)

KSQL - sql для работы с потоками сообщений kafka

ksqlDB - DB построенная на Apache Kafka и Kafka Streams

```java
// CREATE STREAM readings (sensor_id VARCHAR KEY, timestamp VARCHAR, reading INT)
//   WITH (KAFKA_TOPIC = 'readings',
//         VALUE_FORMAT = 'JSON',
//         TIMESTAMP = 'timestamp',
//         TIMESTAMP_FORMAT = 'yyyy-MM-dd HH:mm:ss',
//         PARTITIONS = 1);

ClientOptions options = ClientOptions.create()
  .setHost(KSQLDB_SERVER_HOST)
  .setPort(KSQLDB_SERVER_PORT);

Client client = Client.create(options);
Map<String, Object> properties = Collections.singletonMap("auto.offset.reset", "earliest");
CompletableFuture<ExecuteStatementResult> result = 
  client.executeStatement(CREATE_READINGS_STREAM, properties);
```

## Apache Kafka
Source: [1](https://habr.com/ru/company/southbridge/blog/550934/), [2](https://kafka.apache.org/23/javadoc/index.html?org/apache/kafka), [acl](https://kafka.apache.org/23/javadoc/org/apache/kafka/common/acl/package-summary.html)

Apache Kafka - библиотека, работает с протоколом kafka, подключается, создает topics, создает consumer, делает send etc.  
**Pros.** Можно динамически читать по sheduller сообщения, менять broker, и алгоритмы и размеры чтения сообщений.

```java
// consume, tracing - supports tracer like jaeger
var props = Map.of(
  ConsumerConfig.GROUP_ID_CONFIG, groupId,
  ConsumerConfig.ISOLATION_LEVEL_CONFIG, IsolationLevel.READ_COMMITTED,
  ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers
);
ConsumerFactory<String, String> consumerFactory = new DefaultKafkaConsumerFactory<>(props);
consumerFactory.addListener(new MicrometerConsumerListener<>(meterRegistry)); // prometheus
var pureConsumer = consumerFactory.createConsumer(groupId, prefix, suffix, properties);
Consumer<K, V> consumer = new TracingKafkaConsumerBuilder<>(pureConsumer, tracer)
  .withDecorators(spanDecorators).withSpanNameProvider(consumerSpanProvider).build();
consumer.subscribe(Collections.singletonList(topicName)); // subscribe
consumer.assign(consumer.partitionsFor(topicName));
ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(pollTime)); // время чтения batch
for (TopicPartition partition : records.partitions()) {
    List<ConsumerRecord<String, String>> partitionRecords = records.records(partition);
    for (ConsumerRecord<String, String> record : partitionRecords) {
        offset = record.offset();
        var value = record.value(); // do smth
        var key = record.key();
    }
    consumer.commitSync(Map.of(partition, new OffsetAndMetadata(offset))); // commit
}
consumer.seek(partition, offset); // используем чтобы реализовать retry или пропуск
consumer.close(); // stop

// produce
@Autowired KafkaProperties kafkaProperties;
var props = Map.of(
  kafkaProperties.getSsl().buildProperties(), // ssl cert config
  kafkaProperties.getSecurity().buildProperties(),
  ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, servers,
  ProducerConfig.RETRIES_CONFIG, 0,
  ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG, sessionTimeout,
  ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
  ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class,
  ProducerConfig.MAX_REQUEST_SIZE_CONFIG, maxRequestSize
);
var producer = new KafkaProducer<String, String>(props);
try {
  producer.beginTransaction();
  Future<RecordMetadata> response = producer.send(record);
  producer.commitTransaction();
} catch (RecordProcessingException | IllegalStateException | ProducerFencedException
  | KafkaException | ExecutionException | InterruptedException | DeleteException e) {
  future.setException(e); log.debug(e);
} finally { producer.close(); }
```

## Spring Kafka
Source: [spring-kafka](https://docs.spring.io/spring-kafka/docs/current/reference/html), [тут пример по шагам на русском](https://habr.com/ru/post/440400/), [тут разные примеры](https://github.com/confluentinc/kafka-streams-examples#examples-apps), baeldung ([1](https://www.baeldung.com/spring-kafka), [2](https://www.baeldung.com/spring-cloud-stream-kafka-avro-confluent))

Spring Kafka - wrapper for Apache Kafka

Note. Kafka Actuator Health Indicator нет by default, можно реализовать custom

```java
@Configuration
public class KafkaTopicConfig {
  // 1. Создаем KafkaAdmin, все beans типа NewTopic создают новый topic
  @Value(value = "${kafka.bootstrapAddress}")
  private String bootstrapAddress;

  @Bean
  public KafkaAdmin kafkaAdmin() {
      Map<String, Object> configs = new HashMap<>();
      configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
      return new KafkaAdmin(configs);
  }
  
  @Bean
  public NewTopic topic1() { return new NewTopic("topicName", 1, (short) 1); }

  @Bean
  public NewTopic topic2() {
      return TopicBuilder.name("thing2").partitions(10).replicas(3)
              .config(TopicConfig.COMPRESSION_TYPE_CONFIG, "zstd").compact().build();
  }

  // 2. Настройка producer
  @Bean
  public ProducerFactory<String, String> producerFactory() {
    Map<String, Object> configProps = new HashMap<>();
    configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
    configProps.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    return new DefaultKafkaProducerFactory<>(configProps);
  }

  @Bean
  public KafkaTemplate<String, String> kafkaTemplate() {
      return new KafkaTemplate<>(producerFactory());
  }
}

// 3. Настройка consumer
@EnableKafka // включает поиск @KafkaListener
@Configuration
public class KafkaConsumerConfig {
    @Bean
    public ConsumerFactory<String, String> consumerFactory() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapAddress);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        return new DefaultKafkaConsumerFactory<>(props);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, String> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
        factory.setConsumerFactory(consumerFactory());
        return factory;
    }
}

// 4. Publishing
@Autowired
private KafkaTemplate<String, String> kafkaTemplate;

public void sendMessage(String message) {      
    ListenableFuture<SendResult<String, String>> future = kafkaTemplate.send(topicName, message);
    future.addCallback(new ListenableFutureCallback<SendResult<String, String>>() {
      @Override
      public void onSuccess(SendResult<String, String> result) {
          log.info(message, result.getRecordMetadata().offset());
      }
      @Override
      public void onFailure(Throwable ex) {
          log.info(message, ex.getMessage());
      }
    });
}

// 5. Consuming
@KafkaListener(topics = "topic1, topic2", groupId = "foo") // few topics, and consumer group
@KafkaListener(topics = "topicName") // without group
@KafkaListener(topicPartitions = @TopicPartition(topic = "topicName", partitions = { "0", "1" }))
void listenWithHeaders(@Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
      log.info(message, partition);
}

// initialOffset=0 значит читать ВСЕ сообщения topic с самого начала, т.е. с 1го элемента
@KafkaListener(
  topicPartitions = @TopicPartition(topic = "topicName",
  partitionOffsets = {
    @PartitionOffset(partition = "0", initialOffset = "0"), // выбираем какой partiotion
    @PartitionOffset(partition = "3", initialOffset = "0")}),
  containerFactory = "partitionsKafkaListenerContainerFactory")
void listenToPartition(@Payload String message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition) {
    log.info(message, partition);
  }
}

// 6. Создание filter к consumer, фильтруем оставляя только подходящие
// можно использовать класс KafkaMessageListenerContainer, он не thread safe и однопоточный
@Bean
public ConcurrentKafkaListenerContainerFactory<String, String> filterKafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, String> factory = new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(consumerFactory());
    factory.setRecordFilterStrategy(record -> record.value().contains("World"));
    // factory.setConcurrency(3); количество потоков
    return factory;
}

// 6.2 Подключаем filter
@KafkaListener(topics = "topicName", containerFactory = "filterKafkaListenerContainerFactory")
public void listenWithFilter(String message) {
    System.out.println("Received Message in filtered listener: " + message);
}

// 7. Producing Custom Messages
@Bean
public ProducerFactory<String, Greeting> greetingProducerFactory() {
    configProps.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
    return new DefaultKafkaProducerFactory<>(configProps);
}

@Bean
public KafkaTemplate<String, Greeting> greetingKafkaTemplate() {
    return new KafkaTemplate<>(greetingProducerFactory());
}

// 8.  Consuming Custom Messages (JsonDeserializer для MyClass.class)
@Bean
public ConsumerFactory<String, Greeting> greetingConsumerFactory() {
  return new DefaultKafkaConsumerFactory<>(props,
    new StringDeserializer(), // key Deserializer
    new JsonDeserializer<>(Greeting.class)); // value Deserializer
}

@Bean
public ConcurrentKafkaListenerContainerFactory<String, Greeting> 
  greetingKafkaListenerContainerFactory() {
    ConcurrentKafkaListenerContainerFactory<String, Greeting> factory =
      new ConcurrentKafkaListenerContainerFactory<>();
    factory.setConsumerFactory(greetingConsumerFactory());
    return factory;
}
// подключаем Deserializer
@KafkaListener(topics = "topicName", containerFactory = "greetingKafkaListenerContainerFactory")
public void greetingListener(Greeting greeting) {
    // process greeting message
}

// 9. Dynamic kafka listener, programmatically. Удобного способа нет.
props.put("metadata.max.age.ms", 5000); // установить время обновления metadata в меньшее (если нужно)
// 9.1 Регистрируем Component помеченный @KafkaListener вручную
class Foo {
	@KafkaListener(id = "${id.to.use}", topics = "kgh1059")
	public void listen(String in) { System.out.println(in); }
}
@Configuration
public class Config {
	@Bean
	public ApplicationRunner runner(KafkaListenerEndpointRegistry registry, GenericApplicationContext context) {
		return args -> {
			Properties props = new Properties();
			PropertiesPropertySource source = new PropertiesPropertySource("dynamicListenerId", props);
			context.getEnvironment().getPropertySources().addLast(source);
			Foo foo1 = new Foo();
			props.setProperty("id.to.use", "id1");
			context.registerBean("bean1", Foo.class, () -> foo1);
			context.getBean("bean1");
			Foo foo2 = new Foo();
			props.setProperty("id.to.use", "id2");
			context.registerBean("bean2", Foo.class, () -> foo2);
			context.getBean("bean2");
			registry.getListenerContainerIds().forEach(System.out::println);
		};
	}
}

// 10. multi kafka clusters, когда есть несколько kafka. See https://docs.spring.io/spring-kafka/docs/current/reference/html/#connecting
// 1. Make another one set of ConsumerFactory & KafkaListenerContainerFactory
// 2. use them in @KafkaListener
class Foo {

  @Bean
  ConsumerFactory<String, Greeting> otherConsumerFactory() { return new DefaultKafkaConsumerFactory(); }

  @Bean
  ConcurrentKafkaListenerContainerFactory<String, String> otherClusterFactory() {
      var factory = new ConcurrentKafkaListenerContainerFactory<String, String>();
      factory.setConsumerFactory(otherConsumerFactory());
      return factory;
  }

  @KafkaListener(containerFactory="otherClusterFactory")
  f1(String in) { System.out.println(in); }
}

// 11. используется org.springframework.kafka.event чтобы реагировать на события
// ListenerContainerIdleEvent - чтобы остановить чтение когда топик ничего не посылает N сек, считаем вычитанным
// https://docs.spring.io/spring-kafka/api/org/springframework/kafka/event/ListenerContainerIdleEvent.html
@EventListener
public void eventHandler(ListenerContainerIdleEvent event) { logger.info(event); }

// 11. ErrorHandlingDeserializer2
// https://stackoverflow.com/questions/54931121/handling-json-deserialisation-error-with-spring-kafkalistener/54931504#54931504

// 12. Actuator health indicator https://www.linkedin.com/pulse/springboot-kafka-health-indicator-zhifu-jin
class KafkaHealthIndicator implements HealthIndicator {
    @Autowired KafkaTemplate<String, Object> KafkaTemplate;

    @Value("${kafka.health.indicator.timeout.ms:100}") int timeout;

    @Override
    public Health health() {
        try {
            KafkaTemplate.send("kafka-health-indicator", "❥").get(timeout, TimeUnit.MILLISECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException | NullPointerException e) {
            return Health.down(e).build();
        }
        return Health.up().build();
    }
}

// структура Partition
public final class TopicPartition implements Serializable {
  private int hash = 0;
  private final int partition;
  private final String topic;
}

// структура ConsumerRecord
public class ConsumerRecord<K, V> {
  public static final long NO_TIMESTAMP = RecordBatch.NO_TIMESTAMP;
  public static final int NULL_SIZE = -1;
  public static final int NULL_CHECKSUM = -1;
  private final String topic;
  private final int partition;
  private final long offset;
  private final long timestamp;
  private final TimestampType timestampType;
  private final int serializedKeySize;
  private final int serializedValueSize;
  private final Headers headers;
  private final K key;
  private final V value;
  private volatile Long checksum;
}

// структура ProducerRecord
public class ProducerRecord<K, V> {

    private final String topic;
    private final Integer partition;
    private final Headers headers;
    private final K key;
    private final V value;
    private final Long timestamp;
}

// структура Metadata
public final class Metadata {
    private static final Logger log = LoggerFactory.getLogger(Metadata.class);
    public static final long TOPIC_EXPIRY_MS = 5 * 60 * 1000;
    private static final long TOPIC_EXPIRY_NEEDS_UPDATE = -1L;
    private final long refreshBackoffMs;
    private final long metadataExpireMs;
    private int version;
    private long lastRefreshMs;
    private long lastSuccessfulRefreshMs;
    private AuthenticationException authenticationException;
    private Cluster cluster;
    private boolean needUpdate;
    private final Map<String, Long> topics;     /* Topics with expiry time */
    private final List<Listener> listeners;
    private final ClusterResourceListeners clusterResourceListeners;
    private boolean needMetadataForAllTopics;
    private final boolean allowAutoTopicCreation;
    private final boolean topicExpiryEnabled;
}
```

## Spring Cloud Stream
Source: [baeldung](https://www.baeldung.com/spring-cloud-stream), [Spring Cloud Stream with Kafka](https://www.baeldung.com/spring-cloud-stream-kafka-avro-confluent)

**Spring Cloud Stream** - framework, общий API для работы с сообщениями в Kafka, RabbitMQ, Kafka Streams etc. Построен на Spring Boot and Spring Integration. Может использоваться сторонний сериализатор сообщений, напр Apache Avro
```
Bindings — a collection of interfaces that identify the input and output channels declaratively
Binder — messaging-middleware implementation such as Kafka or RabbitMQ
Channel — represents the communication pipe between messaging-middleware and the application
StreamListeners — message-handling methods in beans that will be automatically invoked on a message from the channel after the MessageConverter does the serialization/deserialization between middleware-specific events and domain object types / POJOs
Message Schemas — used for serialization and deserialization of messages, these schemas can be statically read from a location or loaded dynamically, supporting the evolution of domain object types
```
```java
// <dependency>
//     <groupId>org.springframework.cloud</groupId>
//     <artifactId>spring-cloud-starter-stream-rabbit</artifactId>
//     <version>3.1.3</version>
// </dependency>

// spring:
//   cloud:
//     stream:
//       bindings:
//         input:
//           destination: queue.log.messages
//           binder: local_rabbit
//         output:
//           destination: queue.pretty.log.messages
//           binder: local_rabbit
//       binders:
//         local_rabbit:
//           type: rabbit
//           environment:
//             spring:
//               rabbitmq:
//                 host: <host>
//                 port: 5672
//                 username: <username>
//                 password: <password>
//                 virtual-host: /

@Configuration
@EnableBinding(Processor.class)
public class Cfg {
    @StreamListener(Processor.INPUT)
    @SendTo(Processor.OUTPUT)
    public LogMessage enrichLogMessage(LogMessage log) {
        return new LogMessage(String.format("[1]: %s", log.getMessage()));
    }
}

public interface MyProcessor {
    String INPUT = "myInput";

    @Input
    SubscribableChannel myInput();

    @Output("myOutput")
    MessageChannel anOutput();
}

class Listener {
  @Autowired MyProcessor processor;

  @StreamListener(target = MyProcessor.INPUT, condition = "payload < 10")
  public void routeValuesToAnOutput(Integer val) {
      processor.anOutput().send(message(val));
  }
}
```

## Kafka Connector
Source: [apache](http://kafka.apache.org/documentation.html#connect), [baeldung](https://www.baeldung.com/kafka-connectors-guide), [habr](https://habr.com/ru/articles/779620/)

The Connector API allows building and running reusable producers or consumers that connect Kafka topics to existing applications or data systems. For example, a connector to a relational database might capture every change to a table.

## Kafka ACL
Source: [Kafka ACL](https://rafael-natali.medium.com/unlocking-kafka-security-with-access-control-lists-acls-a6aa1010984f)

Управления доступом, который определяет, кто может получать доступ к топикам или группам в Kafka и какие именно операции разрешено или запрещено выполнять пользователю или группе пользователей.

## Soft
* [kafka tool](https://www.kafkatool.com/) - программа позволяет подключаться к kafka и zookeeper и просматривать состояния **topic**s и изменять их данные. Для настройки нужно указать address и port от kafka и zookeeper.
* [Big Data Tools](https://www.jetbrains.com/help/idea/big-data-tools-support.html) - плагин idea

# jms vs kafka
Источник: [тут](https://stackoverflow.com/questions/42664894/jms-vs-kafka-in-specific-conditions), [тут](https://stackoverflow.com/questions/30453882/is-apache-kafka-another-api-for-jms)

* сообщения не удаляются из Kafka, хранятся сколько указано в настройках
* kafka отличается от jms (т.е. это не jms provider).  Т.е. код и спецификации jms и kafka отличается, сообщение можно обработать еще раз
* kafka имеет меньше features (функционала) чем jms (e.g. kafka не поддерживает транзакционность сама по себе, нужно использовать другие технологии для реализации транзакционности)
* использует не jms протокол и она ориентирована на performances.
* kafka не использует pont-to-point (queue), только Publish-and-subscribe (topic). 
* kafka лучше для **scalability** (разнесения по разным узлам) потому что она разработана как **partitioned topic log** (копии файлов с содержимым topic могут быть разнесены на разные сервера). 
* kafka может разделять поток сообщений на группы. Поэтому kafka имеет лучший ACLs (access control level, установку прав доступа на данные).
* consumer решает какое сообщений потребить на основе offset (смещения в topic относительно чего-то), это снижает сложность написания producer.
* kafka гарантирует **порядок** прихода **message** только для отдельного partition (если их несколько то сообщения с них будут приходить параллельно). Чтобы это обойти используются паттерны меняющие порядок прихода (e.g. на уровне consumer)

# Apache Kafka vs. Integration Middleware (MQ, ETL, ESB)
https://medium.com/@megachucky/apache-kafka-vs-integration-middleware-mq-etl-esb-friends-enemies-or-frenemies-ab02f6f2617b
