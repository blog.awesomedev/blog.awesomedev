[Awesome Postgres](https://github.com/dhamaniasad/awesome-postgres) - Postgres tools
***
[Useful DBA tools by Data Egret](https://github.com/dataegret/pg-utils) - tools to analyze Postgres performance
***
Enable plugins
```sql
-- extensions
create schema extensions;
grant usage on schema extensions to public;
grant execute on all functions in schema extensions to public;
grant usage on all types in schema extensions to public;

-- variant with alter
alter default privileges in schema extensions grant execute on functions to public;
alter default privileges in schema extensions grant usage on types to public;

-- pg_trgm
create extension if not exists pg_trgm;
alter extension pg_trgm set schema extensions;

-- uuid_generate_v4()
create extension if not exists "uuid-ossp";
alter extension "uuid-ossp" set schema extensions;

-- change default search path to public if needed (by default)
SET search_path TO public;
```
***
Create schema and user, grant permissions
```sql
-- schema owner
CREATE USER "user_ms_owner" WITH password 'user_ms_owner';

-- schema user
CREATE USER "user_ms_user" WITH password 'user_ms_user';

-- create schema
CREATE SCHEMA "ms_user" AUTHORIZATION "user_ms_owner";

GRANT USAGE ON SCHEMA "ms_user" TO user_ms_user;

ALTER DEFAULT PRIVILEGES FOR USER "user_ms_owner" IN SCHEMA "ms_user" GRANT SELECT,INSERT,UPDATE,DELETE,TRUNCATE ON TABLES TO user_ms_user;
ALTER DEFAULT PRIVILEGES FOR USER "user_ms_owner" IN SCHEMA "ms_user" GRANT USAGE ON SEQUENCES TO user_ms_user;
ALTER DEFAULT PRIVILEGES FOR USER "user_ms_owner" IN SCHEMA "ms_user" GRANT EXECUTE ON FUNCTIONS TO user_ms_user;
```
***
**Work with sequence**  
serial vs **identity** - it is the same thing in postgres, using sequence explicitly is more convenient because it has name and other parameters, if we use identity we do not have to use `nextval` for id.
```sql
CREATE SEQUENCE mysequence INCREMENT 1 START 1; -- by default

-- descending sequence
CREATE SEQUENCE three INCREMENT -1 MINVALUE 1 MAXVALUE 3 START 3 CYCLE;

SELECT setval('myseq', 42); -- set value,  Next nextval will return 43

SELECT nextval('foo."SQ_ID"'); -- get next sequence value

-- to drop identity to replace be sequence
alter table drop identity
```
***
Find table name and columns in special **information_schema** table
```sql
select t.table_schema, t.table_name, c.column_name, c.data_type
from information_schema.tables t
inner join information_schema.columns c on c.table_name = t.table_name
where t.table_schema not in ('pg_catalog')
  and c.table_name not like 'my_table1_name_prefix%'
  and c.table_name not like 'databasechangelog'
  and c.table_name not like 'databasechangeloglock'
  and c.column_name like '%column_name%'
  and c.data_type in('bigint', 'string', 'integer', 'text', 'smallint', '"char"', 'character varying', 'uuid', 'char')
order by t.table_schema, c.table_name;
```
***
**Work with JSON columns**
```sql
/*
    -> - get as json
    ->> - get json value as text
*/

-- test if json valid
SELECT id, cast(t1.name AS json) as json from t1;

-- get json prop value as text
SELECT
    id,
    name::json->'propName' as propName,
    name::json->'propName' ->> 'innerPropName1' as innerPropName1
    name::json->'propName' ->> 'innerPropName2' as innerPropName2
FROM t1;
```
***
**Fast trigram search.** We can use trigram search instead of **full text search** because it has no need in specific language behavior  
To speed up trigram search for a lot of related tables (deep search) we can use special specific tables that will be fulfilled on target table create/update/delete event. So we do exchange slower database background operations on faster select.
```java
class Detail {
  String name;
}
class Role {
  String name;
  Detail detail
}
class User {
  String name;
  Role role;
  UserSearch userSearch;
}
class UserSearch { // will be update automatically on User, Role, Detail create/update/delete events
  Long userId;
  String roleName;
  String roleDetailName;
}
```
```sql
-- slow
select *
from user
where name like %bla%
or exists (select 1 from role where name like %bla%)
or exists (select 1 from role where detail like %bla%)

-- fast (x10 times faster or more)
select *
from user u left join userSearch us on u.id = u.user_id
where u.name like %bla%
or us.roleName like %bla%
or us.roleDetailName like %bla%
```
***
Postgres `unique index` and `unique constraint` is the same and all based on `unique index`.

Postgres max varchar is **10485760** `varchar(10485760)`
***
**varchar vs varchar(n) vs text**  
[No performance difference between VARCHAR, VARCHAR(n) and TEXT](https://www.postgresql.org/docs/9.1/datatype-character.html) - use `varchar` (standard SQL type) without `size` if you need compatible with **RDBMS**  
`text` is a bit **faster** in rare situations, and you **save the cycles** for the check on the length, see [Any downsides of using data type “text” for storing strings?](https://serverfault.com/a/240907/98376)
```sql
-- text with CHECK works like VARCHAR(n)
ALTER TABLE tbl ADD CONSTRAINT tbl_col_len CHECK (length(col) < 51);
```
***
Convert **type** without table **rewriting**
```sql
-- For example: convert VARCHAR to TEXT
ALTER TABLE ... SET DATA TYPE
```

***
Posgres configuration
* `SHOW config_file;`
* `SHOW ALL;`
* `SELECT * FROM pg_settings;`
* `/var/lib/postgresql/data/postgresql.conf` - config file
  * `chown -R postgres:$(id -gn postgres) /var/lib/postgresql/data/postgresql.conf` - rights
***
**Statistic**

**Enable SQL query log**
```sh
echo "log_statement = 'all'" >> /var/lib/postgresql/data/postgresql.conf
```

**Enable statistic extension**
1. **Add option**
  ```sh
  echo 'shared_preload_libraries="pg_stat_statements"' >> /var/lib/postgresql/data/postgresql.conf
  ```
1. **Enable extension**
  ```sql
  create extension pg_stat_statements;
  alter extension pg_stat_statements set schema extensions; -- if we use separated extensions schema
  ```
1. **See statistic**
  ```sql
  select calls, rows, query from pg_stat_statements where query like '%from my_table%' order by calls desc
  ```
1. **Clear old statistic data**
  ```sql
  select pg_stat_statements_reset()
  ```
***
```sql
-- show all connections older than 5 seconds
SELECT
    pid,
    now() - pg_stat_activity.query_start AS duration,
    usename,
    query,
    state
FROM pg_stat_activity
WHERE (now() - pg_stat_activity.query_start) > interval '5 seconds'
  AND usename in ('user_name');

-- set max query column length
ALTER SYSTEM SET track_activity_query_size = 16384;

-- find big tables
SELECT nspname || '.' || relname AS "relation",
    pg_size_pretty(pg_relation_size(C.oid)) AS "size"
  FROM pg_class C
  LEFT JOIN pg_namespace N ON (N.oid = C.relnamespace)
  WHERE nspname NOT IN ('pg_catalog', 'information_schema')
  AND nspname = 'account'
  ORDER BY pg_relation_size(C.oid) DESC
  LIMIT 100;

-- check if index exists
select *
from pg_indexes
where tablename = 'index_name'
```