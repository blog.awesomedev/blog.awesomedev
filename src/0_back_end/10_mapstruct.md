https://mapstruct.org/documentation/stable/reference/html/

**Note.**  
If you use `Collection<?>` then it will be `ArrayList`.  
You can not use `@MappingTarget` with `Collection`

```java
// 1. standard abstract mapper
@Mapper
public interface AbstractMyMapper<S, T> {
   T toDto(S s); // makes new Object
   List<T> toDto(List<S> s);
   S toDomain(T t); // makes new Object
   List<S> toDomain(List<T> t);
   void updateDto(T t1, @MappingTarget T t2); // copy fields from t1 to t2 without new Object creation
   void updateDomain(S s1, @MappingTarget S s2);
   void updateDomain(List<T> t1, @MappingTarget List<T> t2); // ERROR! You can not do like this with Collection

   // update only if source field value is not null
   @BeanMapping(
      nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
      nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS
   )
   @Mapping(target = "name1", ignore = true) // ignore a field
   void updateDomainIfSourceIsNotNull(S s1, @MappingTarget S s2);

   @BeforeMapping
   default void beforeMapping(S source, @MappingTarget T target) {
      target.doSmth();
   }

   @AfterMapping
   default void afterMapping(S source, @MappingTarget T target) {
      target.doSmth();
   }
}

// 2. Mapper rules
// "uses = ..." - manually chooses which mapper to use for MyBean3 (only in case when mapstruct has a problem to do it automatically)
@Mapper(uses = {MyBean3Mapper.class})
public interface MyMapper extends AbstractMyMapper<User, UserDto> {

   // whole bean null mapping Strategy
   @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
   @Mapping(target = "name1", source = "name2")
   // default if null
   @Mapping(target = "name1", source = "name2", nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.SET_TO_DEFAULT, defaultValue = "defaultValue")
   @Mapping(target = "name3", expression = "java(false)") // any java code inside
   @Mapping(target = "name4", defaultValue = "MY_VALUE")
   @Mapping(target = "quality.document.organisation.name", constant = "NoIdeaInc" )
   @Mapping(target = "typeAccount", qualifiedByName = "findByCode") // qualifiedByName
   @Override
   UserDto toDto(User s); // no need to Override for collection, it is automatic

   @InheritInverseConfiguration // inherit mapping from vise versa rule
   @Override
   User toDomain(UserDto t);

   // will be used automatically for cast MyBean2 to String (search by field type)
   default String myBean2(MyBean2 myBean2) { return myBean2.getName(); }

   @Named("findByCode") default String findByCode() { return "test"; } // qualifiedByName

   // auto used to map User to U type
   @ObjectFactory
   default <U extends Entity> U lookup(User t, @Context ArticleRepository repo, @TargetType Class<U> targetType) {
      ComposedKey key = new ComposedKey(t.getName());
      CombinedOfferingEntity entity = repo.lookup( key );
      return (U) entity;
   }

   // Multiple Source Objects https://www.baeldung.com/mapstruct-multiple-source-objects
   @Mapping(source = "customer.firstName", target = "forename")
   @Mapping(source = "address.street", target = "street")
   DeliveryAddress map(Customer customer, Address address);

   // Nested bean properties to current target https://mapstruct.org/documentation/stable/reference/html/#mapping-nested-bean-properties-to-current-target
   @Mapping( target = "name", source = "record.name" )
   @Mapping( target = ".", source = "record" ) // record.* -> Customer.*
   @Mapping( target = ".", source = "account" ) // account.* -> Customer.*
   Customer customerDtoToCustomer(CustomerDto customerDto);

   // Shared (inherited) configurations https://mapstruct.org/documentation/stable/reference/html/#shared-configurations
   @MapperConfig(
      uses = CustomMapperViaMapperConfig.class,
      unmappedTargetPolicy = ReportingPolicy.ERROR,
      mappingInheritanceStrategy = MappingInheritanceStrategy.AUTO_INHERIT_FROM_CONFIG
   )
   public interface CentralConfig {
      @Mapping(target = "primaryKey", source = "technicalKey")
      BaseEntity anyDtoToEntity(BaseDto dto);
   }

   @Mapper(config = CentralConfig.class, uses = { CustomMapperViaMapper.class } )
   public interface SourceTargetMapper {
      @Mapping(target = "numberOfSeats", source = "seatCount") // + mapping from CentralConfig
      Car toCar(CarDto car)
   }
}

// 3. Use abstract class to use @Autowired
@Mapper
public abstract class AbstractMyMapperBean<S, T> {
   @Autowired MyService myService;
   T map(S s) { return myService.doSmth(s); }
}

// 4. Cycle Avoiding Mapping. Employee1 -> Employee2 -> Employee1
// Why? If you have Cycle of object references then new object will be created each time instead of reusing existed.
// It means for big quantity of objects you will get OutOfMemory exception error because quantity of object is a geometric progression.
// https://github.com/mapstruct/mapstruct-examples/tree/main/mapstruct-mapping-with-cycles
// https://www.baeldung.com/jackson-bidirectional-relationships-and-infinite-recursion
class Employee { Employee reportsTo; List<Employee> team; }
class EmployeeDto { EmployeeDto reportsTo; List<EmployeeDto> team; }

class CycleAvoidingMappingContext {
    private Map<Object, Object> knownInstances = new IdentityHashMap<Object, Object>();

    @BeforeMapping
    public <T> T getMappedInstance(Object source, @TargetType Class<T> targetType) {
      if (!targetType.equals(Employee.class)) { return null; }
      return (T) knownInstances.get(source);
    }

    @BeforeMapping
    public void storeMappedInstance(Object source, @MappingTarget Object target) {
      if (!targetType.equals(Employee.class)) { return null; }
      knownInstances.put(source, target);
    }
}

@Mapper
interface EmployeeMapper {
    Employee map(EmployeeDto employeeDto, @Context CycleAvoidingMappingContext context);
    EmployeeDto map(Employee employee, @Context CycleAvoidingMappingContext context);
}

// 5. Deep Clone, by default mapstruct uses shallow copy
// it means you no need to write separated mappers for inner dto objects
@Mapper(mappingControl = DeepClone.class) // enable deep copy
public interface Cloner {
    CustomerDto clone(CustomerDto customerDto);
}

// 6. get mapper with no DI
CarMapper mapper = Mappers.getMapper( CarMapper.class );

// 7. Mapping customization with decorators https://mapstruct.org/documentation/stable/reference/html/#customizing-mappers-using-decorators
// Why? When you need to get data from DB or by REST
@Mapper
@DecoratedWith(PersonMapperDecorator.class)
public interface PersonMapper {
    PersonDto map(Person person);
}

public abstract class PersonMapperDecorator implements PersonMapper {
     @Autowired @Qualifier("delegate") private PersonMapper delegate;

     @Override
     public PersonDto map(Person person) {
         PersonDto dto = delegate.map( person );
         dto.setName( person.getFirstName() + " " + person.getLastName() );
         return dto;
     }
 }
```
