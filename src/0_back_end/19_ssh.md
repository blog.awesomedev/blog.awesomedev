# Configure SSH RSA keys in Ubuntu for Auto Login

1. Generate private & public keys. Do not set **passphrase** for keys to use auto login
2. copy pub to `~/.ssh/blabla.pub`
3. `chmod 700 ~/.ssh`
4. `chmod 600 ~/.ssh/blabla.pub`
5. `chown $USER:$USER ~/.ssh -R`
6. edit `/etc/ssh/sshd_config`, add or change option `AuthorizedKeysFile %h/.ssh/blabla.pub %h/.ssh/blabla2.pub`
7. `systemctl restart ssh`
8. In **PuTTY** set path to **private** key `Connection > SSH > Auth > /home/blabla/myPrivateKey.ppk`
   1. If you use **PuTTY** then you **have to convert** private key to specific PuTTY format through **PuTTYgen**.

**Note.** If you set passphrase then use ` ssh-keygen -p -P oldpassphrase -N "" -f ~/.ssh/id_rsa` to delete passphrase. Start the command **from white space** to prevent saving the command in `~/.bash_history` file!

```sh
# Connect to ssh
ssh root@111.111.111.111

# Connect to ssh by key
ssh -i /root/privkey.ppk root@111.111.111.111

# Use Dynamic Port Forwarding. SSH will be like socks proxy. Instead of 44443 you can use any port
ssh -D 44443 root@111.111.111.111

# forward ports 11111 to 22222 and 33333 to 44444
ssh -L 22222:111.111.111.111:11111 44444:111.111.111.111:33333 root@111.111.111.111

# auto login by password
sudo apt install sshpass
sshpass -p my_password ssh -o StrictHostKeyChecking=no root@111.111.111.111
```