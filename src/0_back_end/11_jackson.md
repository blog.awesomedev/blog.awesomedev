- https://www.baeldung.com/category/json/jackson
- https://www.baeldung.com/jackson-ignore-properties-on-serialization
- https://www.baeldung.com/jackson-field-serializable-deserializable-or-not

**jackson** - библиотека для serialization/deserialization из/в json, yaml, xml, csv, properties и в многие бинарные форматы jsonb etc.

**jackson module** - подключаемое правило serialization/deserialization

```xml
<!--
    includes:
        jackson-annotations
        jackson-core
-->
<dependency>
    <groupId>com.fasterxml.jackson.core</groupId>
    <artifactId>jackson-databind</artifactId>
    <version>2.13.0</version>
</dependency>
```
```java
// -------------------------------------
// 1. Get ObjectMapper
ObjectMapper objectMapper = new ObjectMapper(); // 1.1 create new
ObjectMapper objectMapper = new Jackson2ObjectMapperBuilder() // 1.2 build with options or modules
    .serializationInclusion(Include.NON_NULL)
    .build();
ObjectMapper objectMapper = objectMapper.clone() // 1.3 clone new and configure
    .setSerializationInclusion(JsonInclude.Include.NON_NULL)
    .registerModule(new JavaTimeModule())
    .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
@Autowired ObjectMapper objectMapper; // 1.4 default in Spring
// -------------------------------------
// 2. Convert Object to Json

// 2.1 {"color":"yellow","type":"renault"}
Car car = new Car("yellow", "renault"); // {"color":"yellow","type":"renault"}
objectMapper.writeValue(new File("target/car.json"), car); // 2.1 to File
String str = objectMapper.writeValueAsString(car); // 2.2 to String
String str = objectMapper.writer().withDefaultPrettyPrinter().writeValueAsString(o); // 2.2 Pretty Print 1
String str = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(o); // 2.3 Pretty Print 2
// -------------------------------------
// 3. Convert Json to Object
Car car = objectMapper.readValue(json, Car.class); // 3.1 from String
Car car = objectMapper.readValue(new File("src/test/resources/json_car.json"), Car.class); // 3.2 File 1
Car car = objectMapper.readValue(new URL("file:src/test/resources/json_car.json"), Car.class); // 3.3 file 2

// 3.4 to Map
String json = "{ \"color\" : \"Black\", \"type\" : \"BMW\" }";
Map<String, Object> map = objectMapper.readValue(json, new TypeReference<Map<String,Object>>(){});

// -------------------------------------
// 4. Properties
// config that are usually used

// DeserializationFeature - from Json to Object options
objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
objectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
objectMapper.configure(DeserializationFeature.FAIL_ON_NUMBERS_FOR_ENUMS, false);
objectMapper.configure(DeserializationFeature.FAIL_ON_EMPTY_BEANS, false);

// SerializationFeature - from Object to Json options
// https://fasterxml.github.io/jackson-databind/javadoc/2.9/com/fasterxml/jackson/databind/SerializationFeature.html
objectMapper.enable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
objectMapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
objectMapper.enable(SerializationFeature.INDENT_OUTPUT); // Pretty Print Globally
// -------------------------------------
// 5. Annotation Configure

@JsonIgnoreProperties(ignoreUnknown=true)
class A {
    @JacksonFeatures(serializationDisable = {SerializationFeature.FAIL_ON_EMPTY_BEANS})
    MyBean myBean;
}

@JsonIgnoreProperties(value = { "intValue" }) // like @JsonIgnore
class A {
    @JsonProperty("strVal") String name1; // set name in json

    @JsonIgnore // disable getter/deserialization
    public String getPassword() { return password; }

    @JsonProperty // enable setter/serialization
    public void setPassword(String password) { this.password = password; }
}
// -------------------------------------
// serialization, use non-public method and fields
mapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
    .withFieldVisibility(JsonAutoDetect.Visibility.ANY)
    .withGetterVisibility(JsonAutoDetect.Visibility.NONE)
    .withSetterVisibility(JsonAutoDetect.Visibility.NONE)
    .withCreatorVisibility(JsonAutoDetect.Visibility.NONE));
// -------------------------------------

// get node value
String json = "{ \"color\" : \"Black\", \"type\" : \"FIAT\" }";
JsonNode jsonNode = objectMapper.readTree(json);
String color = jsonNode.get("color").asText();
// Output: color -> Black

// to objects
String jsonCarArray = "[{ \"color\" : \"Black\", \"type\" : \"BMW\" }, { \"color\" : \"Red\", \"type\" : \"FIAT\" }]";
List<Car> cars = objectMapper.readValue(jsonCarArray, new TypeReference<List<Car>>(){});

String jsonString = "{ \"color\" : \"Black\", \"type\" : \"Fiat\", \"year\" : \"1970\" }";

// serialization, use non-public method and fields
mapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

// ----------
// Ignore All Fields by Type
// 1. if we controll class (type)
@JsonIgnoreType
public class SomeType { ... }
// 2. if we do not control class (from a lib), map String[] to MyMixInForIgnoreType and Ignore
@JsonIgnoreType
public class MyMixInForIgnoreType {}
mapper.addMixInAnnotations(String[].class, MyMixInForIgnoreType.class);
// 3. use
String dtoAsString = mapper.writeValueAsString(dtoObject);
// ----------
//  Ignore Fields Using Filters
// 1. make filter
@JsonFilter("myFilter")
public class MyDtoWithFilter { ... }
// 2. add filter
SimpleBeanPropertyFilter theFilter = SimpleBeanPropertyFilter.serializeAllExcept("intValue");
FilterProvider filters = new SimpleFilterProvider().addFilter("myFilter", theFilter);
// 3. use
String dtoAsString = mapper.writer(filters).writeValueAsString(dtoObject);
// ----------

Car car = objectMapper.readValue(jsonString, Car.class);
JsonNode jsonNodeRoot = objectMapper.readTree(jsonString);
JsonNode jsonNodeYear = jsonNodeRoot.get("year");
String year = jsonNodeYear.asText();

// default ENUM fallback value
// 1. @JsonEnumDefaultValue - make default enum
// 2. READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE - enable default enum support
enum Right { DIR_GEN_READ, DIR_GEN_WRITE, MODIFY, @JsonEnumDefaultValue UNKNOWN; }
ObjectMapper().enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_USING_DEFAULT_VALUE);

// module
// 1 create JsonSerializer
public class FooSerializer extends JsonSerializer<Foo> {
  @Override
  public void serialize(Sponsor sponsor, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
      if (sponsor == null || isObjectEmpty(sponsor)) {
          jsonGenerator.writeNull();
          return;
      }
      jsonGenerator.writeStartObject();
      jsonGenerator.writeStringField("imgUrl", sponsor.getImgUrl());
      jsonGenerator.writeStringField("clickUrl", sponsor.getClickUrl());
      jsonGenerator.writeStringField("sponsorName", sponsor.getSponsorName());
      jsonGenerator.writeStringField("sponsorText", sponsor.getSponsorText());
      jsonGenerator.writeEndObject();
  }
}

// 2 create module
@Bean
public SimpleModule agJacksonModule() {
    final SimpleModule module = new SimpleModule();
    module.addSerializer(Foo.class, new FooSerializer());
    return module;
}

// create ObjectMapper
// 3 add module
@Bean
public JacksonObjectMapper jacksonMapper() {
    final JacksonObjectMapper mapper = new JacksonObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    mapper.enable(MapperFeature.SORT_PROPERTIES_ALPHABETICALLY);
    mapper.enable(SerializationFeature.ORDER_MAP_ENTRIES_BY_KEYS);
    mapper.registerModule(agJacksonModule());
    return mapper;
}
```