Источники: [тут](https://habr.com/ru/post/353238/), [тут](https://habr.com/ru/post/346634/), [тут](https://habr.com/ru/post/309556/), [тут понятно написано](https://habr.com/ru/post/337306/), [базовые команды](https://www.dataset.com/blog/create-docker-image/), [Building best practices](https://docs.docker.com/build/building/best-practices/)

# Common
* **Рекомендуется:** одно приложение - один контейнер: redis, postgres etc
* **Рекомендуется:** один **Container** - один процесс, легче масштабировать, каждый процесс изолирован от других (в своем контейнере), образ можно переиспользовать в составе других наборов
* **Image** - шаблон, командой `docker commit` можно создать свой вариант Image
* **Container** - процесс запущенного **Image**
* Можно иметь много **Container** одного **Image**
* Можно создать **Image** из **Container**
* Изменения в **Container** сохраняются сами между перезапусками.
* Если не указать имя контейнера `--name MyContainer1`, то оно будет случайным, при каждом таком запуске создастся новый **Container**
* Программа внутри Docker должна быть запущена на **0.0.0.0**, а не **127.0.0.1** потому что извне порты мапить можно только к **0.0.0.0** в режиме **bridge** (by default), если программа запущена на **127.0.0.1** то видна будет только в режиме **--net=host**, но при этом все **ports** будут видны из **host** (в системе) и могут конфликтовать
* Вместо **localhost:9999** можно использовать **host.docker.internal:9999** чтобы запущенный **container** в docker увидел внешний сервис запущенный ВНЕ docker (например запущенный локально Spring сервис)
* Вместо **localhost:9999** в запущенных `container` **в одной и той же docker сети** можно использовать **myContainerName:9999**, при этом **myContainerName** нужно указать в **links** в docker-compose чтобы **myContainerName** было видно другому `container`

**Commands**
```sh
docker pull postgres # установить образ
docker pull postgres:latest
docker pull postgres:9.6.20

docker images # список образов
docker ps # список контейнеров (запущенных образов) и их id
docker ps -a # в том числе остановленные
docker container ls -a -s # проверка состояния контейнера, -a/-all (показ в т.ч. остановленных), -s/-size
docker inspect postgres # инфа о контейнере включая его ip
docker rmi postgres # удаление образа
docker tag postgres1 postgres2 # переименование образа
docker logs -f postgres # просмотр логов
docker build . -t my_img_name # сборка образа из Dockerfile и установка в локальный репозиторий

docker volume create —-name my_volume # создать volume который можно монтировать: VOLUME /my_volume
docker volume ls # список
docker volume inspect my_volume # инфа
docker volume rm my_volume
docker volume prune # удалить все не используемые контейнерами

# --volume - устаревшая команда
# type - bind, volume или tmpfs
#       volume - хранятся на хост, для постоянных данных, путь на хост: /var/lib/docker/volumes
#       bind - связывает каталоги на хосте и в контейнере, для разработки, e.g. /home/user/app:/app
#       tmpfs - для временных или чувствительных данных в RAM, очистка при остановке контейнера
# source - путь в хост системе, destination - путь в контейнере, readonly - только чтение
docker container run --mount source=my_volume, target=/container/path/for/volume my_image
docker run --mount type=volume,source=volume_name,destination=/path/in/container,readonly my_image

docker history # инфа о слоях образа

# создать контейнеры
docker run --name MyContainer1 postgres
docker run --name MyContainer2 postgres

docker start container_id # запустить контейнер
docker stop container_id # остановка
docker container kill my_container # быстрая остановка
docker container kill $(docker ps -q) # для всех контейнеров

docker commit container_name image_name # создание image из контейнера
docker commit --pause=false nginx_base wo_pause # без остановки контейнера, может вызвать ошибки

# удаляет остановленные контейнеры, слои и сети, не связанные с используемыми образами
# которым не назначены имена и теги
docker system prune -a --volumes # -a/-all удаляет еще и все неиспользуемые

# запуск образа с указанием проброса порта из образа в localhost
docker run --name mysql -p 3306:3306 -e MYSQL_ROOT_PASSWORD=password mysql

docker exec -it postgres bash # запустить программу внутри образа (тут это консоль bash)
# -i или --interactive, не закрывать консоль
# -t или --tty, текущий терминал запуска контейнера можно использовать команд (соединяет с STDIN и STDOUT)

# запуск в режиме detached
docker run -d -p 80:3000 postgres

# копирование из контейнера с id `6a8151dc5b6b`
docker cp 6a8151dc5b6b:/var/lib/postgresql/data/postgresql.conf ./postgresql.conf

# копирование внутрь контейнера
docker cp ./postgresql.conf 6a8151dc5b6b:/var/lib/postgresql/data/postgresql.conf

# запуск в изолированной сети для безопасного запуска образа
# network create - создание docker сети
# --driver bridge - тип драйвера сети
# типы драйверов:
#   bridge — когда контейнеры должны быть изолированы друг от друга и иметь доступ к интернету через хост.
#   host — когда контейнерам требуется полный доступ к сети хоста, и изоляция между контейнерами не важна.
#   none — когда контейнерам не нужно сетевое взаимодействие.
#   overlay — когда нужно создать распределенную сеть для контейнеров на разных хостах в кластере Docker Swarm. Приложения общаются в сети докера как буд-то они на одной машине.
#   macvlan — когда вам нужно, чтобы контейнеры имели уникальные MAC-адреса и были видимыми в вашей локальной сети как отдельные устройства.
# --subnet 192.168.50.0/25 - диапазон адресов который выделен docker контейнерам
#           - если не указать будет задан автоматически и тогда будет риск пересечения адресов
#           - можно задать диапазоны: dev, test, prod: 192.168.30.0/25, 192.168.40.0/25, 192.168.50.0/25
#           - 192.168.50.0/25 - ...0-...127=128 адресов, /26 - 64 адреса, /24 - 256 адресов, /16=65536
#           - варианты диапазонов: 10.0.0.0/8, 172.16.0.0/12, 192.168.0.0/16
# custom_network - имя сети
docker network create --driver bridge --subnet 192.168.50.0/25 custom_network
docker run -d --name app1 --network custom_network your_image_1 # запуск контейнера в сети custom_network

docker diff mysql # покажет слои образа созданные командами RUN

docker export 6a8151dc5b6b > contents.tar # сохраняем всю файловую систему

ls -la /var/lib/postgresql/data/postgresql.conf # узнаем права на файл в контейнере
chown -R postgres:$(id -gn postgres) /var/lib/postgresql/data/postgresql.conf # меняем права на файл внутри контейнера
```

**Dockerfile**
```Dockerfile
FROM python:3.7.2-alpine3.8 # установить
LABEL maintainer="jeffmshale@gmail.com" # мете данные, напр. разраб
ENV ADMIN="jeff" # переменные среды в образе
ARG ADMIN2="jeff2" # как ENV, но видна только при сборке, не при работе

# команды RUN, COPY etc выполняются в контексте директории из WORKDIR
WORKDIR /usr/src/app

# создает слой, несколько команд обязательно через && (установка программ)
# новый RUN создает новый слой, поэтому лучше использовать только 1 RUN если файлы добавляются,
# а потом что-то удаляется, иначе в образе будут лишние файлы в старом слое
# Рекомендуется использовать RUN если слои независимы
RUN apk update && apk upgrade && apk add bash && \
    # устанавливаем только необходимые пакеты, а не рекомендованные
    --no-install-recommends && \    
    # чистим кэш примерно 100-150мб, альтернативно команда: --no-cache, но она не так надежна
    apt-get clean && rm -rf /var/lib/apt/lists/* \
RUN apk add mysql
# если можно запускать не от root, то можно добавить user
RUN groupadd -r postgres && useradd --no-log-init -r -g postgres postgres

COPY . ./app # копируем файлы внутрь образа
ADD https://raw.com/vid1.mp4 /my_app_directory # копируем в контейнер

# COPY vs ADD - COPY предпочтительна, ADD кроме всего поддерживает ссылки и может распокавать tar
# Когда нужен ADD, накат образа FS на диск: ADD rootfs.tar.xz /.

EXPOSE 3000 # порт который container будет слушать во время запуска

# анонимный том для хранения данных, напр. при пересоздании контейнера не удалятся
# и документирует образ показывая директорию для данных
VOLUME /var/lib/postgresql/data

# По умолчанию ENTRYPOINT это /bin/sh -c и ей передаются параметры из CMD
# Меняем ENTRYPOINT
ENTRYPOINT ["/bin/ping"]
CMD ["localhost"]

# Без ENTRYPOINT это команда передаст параметры в /bin/sh -c
CMD ["python", "./my_script.py"] # выполнить когда контейнер будет запущен
```
`.dockerignore` для файлов которые нужно игнорить при сборке образа, как .gitignore
```dockerignore
.git
.gitignore
Thumbs.db
node_modules/   # Исключить директорию с зависимостями Node.js
README.md
CHANGELOG.md
```

# docker compose
https://stackoverflow.com/a/41912295

# Soft
- [Awesome Docker](https://github.com/veggiemonk/awesome-docker)
- [Awesome-Selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted)