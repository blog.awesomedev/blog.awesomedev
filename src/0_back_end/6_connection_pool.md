
Source: [1](https://www.prisma.io/dataguide/database-tools/connection-pooling), [2](https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing), [3](https://learn.microsoft.com/en-us/azure/mysql/single-server/sample-scripts-java-connection-pooling)

**Connection Pool** - подход при котором 1 connection можно переиспользовать вместо пересоздания нового, использованные connections не закрываются, а возвращаются в pool

**Создание connection это тяжелая операция** которая включает: dns lookups, TCP handshake, TLS handshake, exchange connection preferences, database authentication, database authorization, query, tear down the database session, TLS encryption and TCP connection  
На сам процесс работы с connection тоже тратятся ресурсы, чем больше connections тем больше ресурсов.

**Connection влияет на:**  
**Для DB:** cpu, ram, cache  
**Для application:** cpu, ram, max db connections

**Connection Pool избавляет от** тяжелых операций пересоздания connections и позволяет не исчерпать connection limit на каждый client request

**external vs internal connection pool**  
**Internal** часто используется отдельные pool внутри каждого сервиса, но при этом 1 connect не могут переиспользовать несколько сервисов. [HikariCP](https://github.com/brettwooldridge/HikariCP/wiki/About-Pool-Sizing) - самый популярный Internal пулл  
**External** - переиспользование 1 connection на все сервисы требует дополнительный hop обращения по сети чтобы передать connection из pool, но зато может масштабироваться и переиспользоваться.  
**Пример External Connection Pool:** pgbouncer, pgpool

НЕКОТОРЫЕ **Алгоритмы External Connection Pool** - алгоритмы описаны отдельно т.к. особенность того что работают с несколькими сервисами, а не напрямую с client как в **Internal Pool**  
1 **transaction pooling** - connection закрывается вконце каждой transaction, позволяет быстро закрывать connections пока client выполняет другие операции между транзакциями  
2 **session pooling** - connection занят на время существования session (e.g. rest запроса), connection после возвращения в pool будет переиспользовано. Этот алгоритм уменьшает число доступных одновременно connections   
3 **statement pooling** - connection занято на время отдельных statement (sql операции), позволяет быстро открывать и закрывать connections, но может привести к неконсистентности транзакций. Позволяет большому количеству пользователей использовать ограниченный набор connections.

Общее правило настройки **Internal Connection Pool**: чем меньше потоков тем лучше, но исходить нужно из количества ядер процессора
```
Общая формула:
connections = ((core_count * 2) + effective_spindle_count)

core_count - количество ядер, hyperthreading (HT) не учитывается
effective_spindle_count - количество HDD (с SSD не формула не проверялась), если все данные в кэше, то == 0
```

**Pool-locking** - случается когда 1 актор на уровне приложения запрашивает очень много connections одновременно
```
Размер пулла чтобы избежать pool lock:
pool size = Tn x (Cm - 1) + 1
Tn - max количество потоков
Cm - max количество одновременных connection на один поток

Это min число чтобы избежать deadlock, больше ставить можно.
```
**Простой connection pool**  
1 При запросе connection создаем его и возвращаем  
2 Если connection есть, то возвращаем существующий  
3 Для проверки доступно ли connection из DB выполняем `select 1`  
4 В данной реализации 1 thread == 1 connection  
5 Алгоритм устаревания решает когда нужно закрыть connection если он не используется
```java
public class BasicConnectionPool implements ConnectionPool {
    private String url;
    private String user;
    private String password;
    private List<Connection> connectionPool;
    private List<Connection> usedConnections = new ArrayList<>();
    private static int INITIAL_POOL_SIZE = 10;
    
    public static BasicConnectionPool create(String url, String user, String password) throws SQLException {
        List<Connection> pool = new ArrayList<>(INITIAL_POOL_SIZE);
        for (int i = 0; i < INITIAL_POOL_SIZE; i++) {
            pool.add(createConnection(url, user, password));
        }
        return new BasicConnectionPool(url, user, password, pool);
    }
    
    @Override
    public Connection getConnection() {
        Connection connection = connectionPool.remove(connectionPool.size() - 1);
        usedConnections.add(connection);
        return connection;
    }
    
    @Override
    public boolean releaseConnection(Connection connection) {
        connectionPool.add(connection);
        return usedConnections.remove(connection);
    }
    
    private static Connection createConnection(String url, String user, String password) throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }
    
    public int getSize() { return connectionPool.size() + usedConnections.size(); }
}
```