- [Частые вопросы на собеседовании](#частые-вопросы-на-собеседовании)
- [Software design pattern (паттерны Gang of Four (GoF)](#software-design-pattern-паттерны-gang-of-four-gof)
  - [Behavioral patterns (Поведенческие)](#behavioral-patterns-поведенческие)
  - [Creational patterns (Порождающие)](#creational-patterns-порождающие)
  - [Structural patterns (Структурные)](#structural-patterns-структурные)
- [Enterprise Integration Patterns](#enterprise-integration-patterns)
- [Microservice Architecture pattern from practice](#microservice-architecture-pattern-from-practice)
- [Functional patterns](#functional-patterns)
- [Concurrency](#concurrency)
- [Architectural](#architectural)
  - [Service Locator](#service-locator)
  - [Гексагональная архитектура (a.k.a. Порты и адаптеры)](#гексагональная-архитектура-aka-порты-и-адаптеры)
  - [Microservice cache](#microservice-cache)
- [Data Processing Models](#data-processing-models)
- [Other patterns](#other-patterns)
- [POJO vs DTO vs VO vs Java Bean vs JDO](#pojo-vs-dto-vs-vo-vs-java-bean-vs-jdo)
- [Development principles (SOLID, DRY, KISS, YAGNI, DDD)](#development-principles-solid-dry-kiss-yagni-ddd)
- [is A vs Has A](#is-a-vs-has-a)
- [Composition, Aggregation, Association](#composition-aggregation-association)
- [Cohesion vs Coupling](#cohesion-vs-coupling)
- [Context](#context)
- [GRASP, General Responsibility Assignment Software Patterns](#grasp-general-responsibility-assignment-software-patterns)
- [Immutable object](#immutable-object)
- [Horizontal (`scale out`) vs Vertical scaling (`scale up`), Вертикальное vs Горизонтальное масштабирование](#horizontal-scale-out-vs-vertical-scaling-scale-up-вертикальное-vs-горизонтальное-масштабирование)
- [Fine-grained vs Coarse-grained](#fine-grained-vs-coarse-grained)
- [Список ООП vs functional концепций:](#список-ооп-vs-functional-концепций)
- [Game Programming Patterns](#game-programming-patterns)
- [Источники](#источники)

# Частые вопросы на собеседовании
- Паттерны GoF (в основном Singleton и все его типы, Strategy, Adapter, Decorator и множественное наследование, Facade, Factory, Builder, Iterator, Proxy)
- Delegate
- MVC
- DAO
- immutable объект
- SOLID
 
# Software design pattern (паттерны Gang of Four (GoF)

## Behavioral patterns (Поведенческие)

* **Observer** - получает оповещения от других объектов (Observables) о изменении их состояния (наблюдает за ними)
  * <details>
    <summary>Example</summary>

    **Пример**
    ```java
    interface Observer { void update(String event); }
      
    class EventSource {
        List<Observer> observers = new ArrayList<>();
      
        public void notifyObservers(String event) {
            observers.forEach(observer -> observer.update(event));
        }
      
        public void addObserver(Observer observer) {
            observers.add(observer);
        }
      
        public void scanSystemIn() {
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                notifyObservers(line);
            }
        }
    }

    public class ObserverDemo {
        public static void main(String[] args) {
            System.out.println("Enter Text: ");
            EventSource eventSource = new EventSource();
            
            eventSource.addObserver(event -> System.out.println("Received response: " + event));

            eventSource.scanSystemIn();
        }
    }
    ```
    </details>
* **Iterator** - может обходить связанные объекты не раскрывая их структуры (разные List, Set, Map etc). Выносит методы для обхода коллекций в отдельный класс с методами: `hasNext()`, `next()`, `previous()` etc.  
  * <details>
    <summary>Example</summary>

    **Пример Iterable** (с методами `next()` и `hasNext()` ) и **Пример Iterator** (с методом `iterator()`):
    ```java
    class MySet implements Iterable {
        Iterator iterator() { return new MySetIterator(); }
        class MySetIterator impliments Iterator {
            // ... доступ к внутренностям MySet и возврат ее элементов по next()
            boolean hasNext();
            Object next() { return someMySetEl; };
        }
    }
    ```
    </details>
* **Command** - объект команды содержит само действие (вызов методов других классов) и параметры. Команды в этом паттерне не методы, а сам объект целиком - это команда.
* **Strategy** - содержит ссылку на алгоритм с общим для всех таких же алгоритмов интерфейсом (методом). Устанавливая другие реализации алгоритма можно переключаться между ними. Пример: архиватор с разными методами сжатия.
* **Memento** - хранит состояние объекта и позволяет откатывать к прошлым состояниям
* **Template method** - переопределяет некоторые шаги алгоритмов предка, не меняя остальные шаги (обычное наследование)
* **Chain of responsibility** - цепочка обработчиков через которую проходит объект как конвеер
  * <details>
    <summary>Example</summary>

    ```java
    abstract class Logger { }
    class StdoutLogger extends Logger { }
    class EmailLogger extends Logger { }
    logger = new StdoutLogger(Logger.DEBUG); // первый обработчик
    logger.setNext(new EmailLogger(Logger.NOTICE)); // следующий обработчик в цепочке
    logger.message("Entering function y.", Logger.DEBUG); // обработчики запустятся по порядку StdoutLogger, EmailLogger
    ```
    </details>
* **Mediator** - объект **посредник** содержащий общий метод который могут вызывать другие объекты (e.g. JmsTemplate)
* **State** - меняет свой поведение в зависимости от своего состояния (как конечный автомат)
  * <details>
    <summary>Example</summary>

    ```java
    public class StateExample {
        public static void main(String[] args) {
            StateContext context = new StateContext();
            context.heat();
            context.freeze();
        }
    }

    interface State {
        String getName();
        void freeze(StateContext context);
        void heat(StateContext context);
    }

    class SolidState implements State {

        private static final String NAME = "solid";

        public String getName() {
            return NAME;
        }

        public void freeze(StateContext context) {
            System.out.println("Nothing happens.");
        }

        public void heat(StateContext context) {
            context.setState(new LiquidState());
        }

    }

    class LiquidState implements State {

        private static final String NAME = "liquid";

        public String getName() {
            return NAME;
        }

        public void freeze(StateContext context) {
            context.setState(new SolidState());
        }

        public void heat(StateContext context) {
            context.setState(new GaseousState());
        }

    }

    class GaseousState implements State {

        private static final String NAME = "gaseous";

        public String getName() {
            return NAME;
        }

        public void freeze(StateContext context) {
            context.setState(new LiquidState());
        }

        public void heat(StateContext context) {
            System.out.println("Nothing happens.");
        }

    }

    class StateContext {
        private State state = new SolidState();

        public void freeze() {
            System.out.println("Freezing " + state.getName() + " substance...");
            state.freeze(this);
        }

        public void heat() {
            System.out.println("Heating " + state.getName() + " substance...");
            state.heat(this);
        }
    }
    ```
    </details>
* **Visitor** - вынос логики обращение к объектам в отдельный объект (посетитель других объектов, похож на Facade)
  * <details>
    <summary>Example</summary>

    ```java
    public class Demo {
        public static void main ( String [] args ) {
            Point p = new Point2d( 1, 2 );
            p.accept(new Chebyshev());
            System.out.println( p.getMetric() );
        }
    }

    interface Visitor {
        public void visit ( Point2d p );
        public void visit ( Point3d p );
    }

    abstract class Point {
        int metric;
        public abstract void accept ( Visitor v );
    }

    class Point2d extends Point { // x,y
        void accept ( Visitor v ) { v.visit(this); }
    }

    class Point3d extends Point { // x,y,z
        void accept ( Visitor v ) { v.visit(this); }
    }

    class Euclid implements Visitor {
        public void visit ( Point2d p ) { doSmth(p); }
        public void visit ( Point3d p ) { doSmth(p); }
    }

    class Chebyshev implements Visitor {
        public void visit ( Point2d p ) { doSmth2(p); }
        public void visit ( Point3d p ) { doSmth2(p); }
    }
    ```
    </details>

## Creational patterns (Порождающие)

* **Singleton** - один экзепляр объекта на все приложение
  * <details>
    <summary>Типы реализации Singleton (ВАЖНО!)</summary>

      1. **Eager Singleton**
          ```java
          public class EagerSingleton {
              private static final EagerSingleton instance = new EagerSingleton();
              
              //private constructor to avoid client applications to use constructor
              private EagerSingleton(){}
              public static EagerSingleton getInstance(){ return instance; }
          }
          ```
      2. **Lazy Singleton**
          ```java
          public class LazySingleton {
              private static LazySingleton instance;
              private LazySingleton(){}
              public static LazySingleton getInstance(){
                  return instance == null ? instance = new LazySingleton() : instance;
              }
          }
          ```

      3. **Static Singleton**
          ```java
          public class StaticBlockSingleton {
              private static StaticBlockSingleton instance;
              private StaticBlockSingleton(){}
              
              //static block initialization for exception handling
              static{
                  try{
                      instance = new StaticBlockSingleton();
                  }catch(Exception e){
                      throw new RuntimeException("Exception occured in creating singleton instance");
                  }
              }
              public static StaticBlockSingleton getInstance(){ return instance; }
          }
          ```
      4. **Thread Safe Singleton**, с double check locking в Java для которой выполняется happen before
            ```java
            public class ThreadSafeSingleton {
                private static volatile ThreadSafeSingleton instance; // volatile чтобы взять переменную не из кэша CPU, а актуальную; вызывает happens before
                private ThreadSafeSingleton(){}
                public static synchronized ThreadSafeSingleton getInstance(){
                    if (instance == null) {	// сначало проверяем, только потом захватываем монитор
                        synchronized (DclSingleton.class) {
                            if (instance == null) {
                                instance = new DclSingleton();
                            }
                        }
                    }
                    return instance;
                }
            }
            ```
      5. **Enum Singleton**
            ```java
            public enum EnumSingleton {
                INSTANCE;
                public static void doSomething(){
                    //do something
                }
            }
            ```
    </details>

* **Builder** - создает сложный объект пошагово (инициализируя его). Может использовать один и тот же код для строительства разных объектов (с общим предком).

* **Factory** - еще называют "smart constructor", т.е. просто создает сложный объект, аналог конструктора.  
Самая простая Factory: `class A { make() { return new A(); } }`

* **Abstract Factory** - есть общий интерфес для всех наследников, они его переопределяют и создают каждый разные типы объектов, хотя имя метода то же. Используется в dependency injection/strategy.

* **Factory Method (Virtual Constructor)** - как Factory, только метод фабрики абстрактный и может быть переопределен в наследниках

* **Factory vs Builder** - Factory простая версия Builder. Можно рассматривать ее как обертку вокруг конструктора, с отличием от конструктора в том, что она создает объект за 1ин шаг. Factory создает подтипы одного и того же объекта. Builder сложнее и создает объект по шагам.

* **Prototype** - метод для клонирования объекта включая их **состояние** (e.g. метод `clone()` в Java или конструктор клонирования в C++)
  * альтернатива паттерну factory (позволяет сделать 1 клонер c многими методами вместо многих клонеров с 1 методом)
  * построения иерархий классов или фабрик, параллельных иерархии классов продуктов
  * удобно когда нужно клонировать что-то в разных состояниях

## Structural patterns (Структурные)

* **Proxy** - прокси между объектом к которому нужно получить доступ. Перехватывает вызовы методов объекта изменяет их. (e.g. реализацию отложенной инициализации затратных ресурсов при первом обращении)
  * **Note.** В AOP, если связывание (weaving) сделано через method invocation, то вызов метода proxy внутри другого метода того же proxy не вызовет сквозную задачу, это следствие паттерна proxy. Так работает Spring AOP, но не AspectJ при использовании weaving во время компиляции компилятором AspectJ (для него вызов метода proxy из другого метода proxy тоже будет проксирован).
  * <details>
    <summary>Пример Proxy</summary>

    ```java
    public class Math implements IMath {
        public double add(double x, double y) {
            return x + y;
        }
    }

    public class MathProxy implements IMath {
        private Math math;
        public double add(double x, double y) {
            if(math == null) {
                math = new Math(); // отложенная инициализация для экономии ресурсов
            }
            return math.add(x, y);
        }
    }

    IMath p = new MathProxy();
    ```
    </details>

* **Adapter (Wrapper)** - оборачивает вызов метода в свой метод и перед передачей параметров в обернутый метод "адаптирует" их (меняет формат, напр. xml в json). Может "оборачивать" object или class.  
Имеет общий interface со всеми необходимыми методами РАЗНЫХ объектов, так что наследовать Adapter может любой из них, а не нужный метод интерфейса можно просто не использовать.

* **Decorator** - подключает поведение к целевому объекту. альтернатива наследованию классов (дает сделать **множественное наследование классов** в языках где его нет). Класс-обертка получающая ссылку на классы того же типа что и он сам (т.е. все получаемые декоратором классы и сам декоратор наследуют общий интерфейс). Вызывает какие-то другие методы "до, во время или после" вызова "обернутого" метода.
  * <details>
    <summary>Пример decorator</summary>

    ```java
    public interface InterfaceComponent {
        void doOperation();
    }

    class MainComponent implements InterfaceComponent {
        @Override
        public void doOperation() {
            System.out.print("World!");
        }	
    }

    abstract class Decorator implements InterfaceComponent {
        protected InterfaceComponent component;
        
        public Decorator (InterfaceComponent c) {
            component = c;
        }
        
        @Override
        public void doOperation() {
            component.doOperation();
        }

        public void newOperation() {
            System.out.println("Do Nothing");
        }
    }

    class DecoratorSpace extends Decorator {
        public DecoratorSpace(InterfaceComponent c) {
            super(c);
        }
        
        @Override
        public void doOperation() {
            System.out.print(" ");
            super.doOperation();
        }
        
        @Override
        public void newOperation() {
            System.out.println("New space operation");
        }
    }

    // использование
    Decorator c = new DecoratorHello(new DecoratorComma(new DecoratorSpace(new MainComponent())));
    ```
    </details>

* **Facade** - скрывает сложность. содержит ссылки на несколько разных объектов. Его метод скрывает вызов нескольких методов этих разных объектов.

* **Bridge** - разделяет абстракцию и реализацию, чтобы они могли изменяться независимо
  * <details>
    <summary>Example</summary>

    ```java
    public interface Drawer { // реализацию
        public void drawCircle(int x, int y, int radius);
    }

    public class SmallCircleDrawer implements Drawer{
        @Override
        public void drawCircle(int x, int y, int radius) { }
    }

    public class LargeCircleDrawer implements Drawer{        
        @Override
        public void drawCircle(int x, int y, int radius) { }

    }

    public abstract class Shape {
        protected Drawer drawer;
        
        protected Shape(Drawer drawer){
            this.drawer = drawer;
        }
        
        public abstract void draw();
        
        public abstract void enlargeRadius(int multiplier);
    }

    public class Circle extends Shape{ // абстракцию
        public Circle(int x, int y, int radius, Drawer drawer) {
            super(drawer);
        }

        @Override
        public void draw() {
            drawer.drawCircle(x, y, radius);
        }
    }

    public class Application {

        public static void main (String [] args){
            new Circle(5,10,10, new LargeCircleDrawer()).draw();
            new Circle(20,30,100, new SmallCircleDrawer()).draw();
        }
        
    }
    ```
    </details>

* **Composite** - делает древовидную структуру, которая содержит объекты определенного типа (с одними методам интерфейса), вызов метода всего дерева проходит по всем вложенным объектам и вызывает для них один и тот же метод (операцию)

* **Flyweight** (Приспособленец) - хранит много общих объектов из разных мест программы чтобы экономить память (как кэш или pool в Java), используется когда создается много одинаковых объектов.
  * <details>
    <summary>Example</summary>

    ```java
    abstract class EnglishCharacter { // 1. общий интерфейс
        protected char symbol;
        public abstract void printCharacter();
    }

    public class CharacterA extends EnglishCharacter { // 2. создаем
        public CharacterA(){ symbol = "A"; }
        
        @Override
        public void printCharacter() {
            System.out.println("Symbol = " + symbol + "");
        }
        
    }

    public class CharacterB extends EnglishCharacter {
        public CharacterB(){ symbol = "B"; }
        
        @Override
        public void printCharacter() {
            System.out.println("Symbol = " + symbol + "");
        }
        
    }

    public class FlyweightFactory { // 3. храним как в кэше
        private HashMap<Integer, EnglishCharacter> characters = new HashMap();
        
        public EnglishCharacter getCharacter(int characterCode){
            EnglishCharacter character = characters.get(characterCode);
            if (character == null){
                switch (characterCode){
                    case 1 : {
                        character = new CharacterA();
                        break;
                    }
                    case 2 : {
                        character = new CharacterB();
                        break;
                    }
                }
                characters.put(characterCode, character);
            }
            return character;
        } 
        
    }

    EnglishCharacter character = new FlyweightFactory().getCharacter(1); // 4. используем
    character.printCharacter();
    ```
    </details>

# Enterprise Integration Patterns

* **Guarantee Delivery** ([1](https://www.enterpriseintegrationpatterns.com/GuaranteedMessaging.html)) - для обмена сообщениями (через JMS, Kafka etc), перед отправкой сообщение **сохраняется в локальную DB**, и удаляется оттуда только когда доставлено подписчику (подписчик ответит что сообщение доставлено), в случае падения и восстановления неотправленное сообщение resend. Чтобы сообщение не отправилось 2 раза после восстановления система: **1)** или **запрашивает по id** сообщения статус (отправлено или нет), **2)** или **переотправляет** второй раз без запроса, если повторное получение сообщение вызовет **idempotent** метод (т.е. повторная переначитка не изменит состояние подписчика).
* **Unit of Work** (UoW) - реализация логической транзакции, записывает все изменения объектов ассоциированных с таблицами в DB и делает `commit()` этих изменений как одну физическую транзакцию в DB. Этот паттерн не реализует всю логику сам, а использует другие паттерны. Каждая Entity (объект табл) хранится в `Map` в 1ом экземпляре, это избавляет от конфликтов. В методе `commit()` вычисляется разница между оригинальной Entity и измененной, чтобы сделать `commit()` только измененной части. Использует паттерн [Metadata Mapping](https://www.martinfowler.com/eaaCatalog/metadataMapping.html), описание маппинга на табл., чтобы определить изменения в Entity при `commit()`.
  * **Note.** Session из Hibernate ORM и других ORM это **Unit of Work** с доп. фичами, например commit() может совершаться автоматически время от времени (по алгоритму), а запись лищних изменений отсекаться алгоритмом **dirty checking**.
  * <details>
    <summary>Пример Unit of Work</summary>

    Источник: [1](https://github.com/iluwatar/java-design-patterns/tree/master/unit-of-work), [2](https://www.codeproject.com/Articles/581487/Unit-of-Work-Design-Pattern), [3](https://qna.habr.com/answer?answer_id=1294177#answers_list_answer)

    ```java
    public interface IUnitOfWork<T> {
        private final Map<String, List<Student>> context; // списки Entities для CRUD
        private final StudentDatabase studentDatabase; // имеет CRUD методы, которые вызывается в цикле для List<Student> из context
        String INSERT = "INSERT";
        String DELETE = "DELETE";
        String MODIFY = "MODIFY";
        void registerNew(T entity); // register методы добавляют Entities в context
        void registerModified(T entity);
        void registerDeleted(T entity);
        void commit(); // в цикле вызывает методы studentDatabase для Entities из context
    }
    ```
    </details>
* **DAO**

# Microservice Architecture pattern from practice

* **In short.** Как работает в целом.
  * **Saga** переносит реплики табл между сервисами
    * **Choreography** - просто отправляет реплики в момент их изменения в другие сервисы. Логика отката изменений не предусмотрена т.к. отправляются только consistent данные (уже после изменения в сервисе-источнике)
    * **Orchestration** - использует сервисы-прослойки которые контролируют отправку. Отправляются также только consistent данные, но позволяют писать логику отката данных в другоме сервисе (похоже на конечный автомат или BMPN движок).
  * **Transactional outbox** - используется в **Saga** для гарантированной доставки на случай падения сервисов. Использует спец. табл сервиса, где между падениями хранит данные чтобы не утерять и переотправить.
  * **Transactional inbox** - временно хранит пришедшие сообщения в табл сервиса и обрабатывает в отдельном фоновом потоке, переносит нагрузку с kafka на сам сервис. Обрабатывать можно не в 1 потоке. Для Message Broker которые не сохраняют порядок сообщений можно использовать для упорядочивания сообщений.
  * **Event souring** - используется в **Saga** для отправки реплик, отправляются только те поля по которым были create/update/delete
  * **CQRS** - используется чтобы через **view** делать **join** данных пришедших реплик чтобы составить **dto** не в коде, а на уровне базы данных. Предполагает что есть много разных **api** каждая из которых использет свое **dto**
  * **API Composition** - использует не реплики а **RPI** (e.g. REST, gRPC) запросы в другие сервисы для получения данных, проще, но данные перечитываются из других сервисах постоянно
  * **Externalized configuration** - применяет настройки к сервисам из хранилища без необходимости перекомпиляции, **configMap** из Kubernetes
  * **Circuit Breaker** - отслеживает запросы вернувшие ошибку, если их много, то перестает перенаправлять запросы на сломанные **pods**, как вариант может возвращать данные-заглушки, проверяет ожившие pods и если все ок начинает использовать их опять.
  * **Access Token** - jwt пользователя передается между сервисами по rest чтобы проверка прав срабатывала и при использовании запросов от сервиса к другим сервисам (e.g. API Composition). Как вариант может быть technical jwt если сервис сделал запрос к другому сервису сам и не имеет jwt пользователя, каждый сервис может иметь свои права доступа на определенные api других сервисов или не иметь.
  * **Service discovery** (e.g. Eureka) - часть на **client** используется чтобы регистрироваться в **service** части, это отдельный сервис который содержит пару `service_name-ip` чтобы обращаться к сервису по name
  * **API Gateway** - все обращения к сервисам проходят через один и тот же **service** с одним `domain name` и url (e.g. `myservice.com/api/mycluster/shop/...`), чтобы скрыть внутренние api сервиса, так же работает как балансировщик и может содержать проверку прав доступа (не сложную которая не требует данных из табл). Может стоять не только на входе в весь кластер сервисов, но и быть промежуточным между самими сервисами (e.g. для проверки ролей на api). Могут быть разные **API Gateway** для Web, Mobile etc.
    * **Also can work like (contain):** Load balancer (Zuul), ACL Security, Rate limiter
***
* **Idempotent Receiver** - **subscriber** который получает сообщение (e.g. через kafka/jms), он **Idempotent** на случай одновременной отправки одного и того же сообщения. **E.g.** Используется в **Transactional outbox** при отправке сообщений через **Message Relay** в режиме **at-least-once** после поднятия сервиса после падения.
* **Transactional messaging**
  * [Transactional outbox](https://microservices.io/patterns/data/transactional-outbox.html) - для рассылки сообщений о **transactions** другим **services** в Microservice Architecture. Альтернатива **2PC**, отправляет сообщения от нескольких копий сервисов (приложений) в порядке в котором записи в DB были `create/update/delete` и только при успешной транзакции. **Может** использоваться как часть паттерна **Saga** (механизм отправки сообщений и подписка на них). Состоит из 2х частей: **1)** Сохраняет в `outbox table` по событию `create/update/delete` как в очередь для отправки, **2)** **Message relay** - фоновый процесс который отправляет в **message broker** подписчикам.
  * **Implementation**
    * **Outbox.** В рамках одной **transactions** идет сохранение в **outbox table** (одна на все сервисы), отдельный процесс **message relay** читаем записи **outbox table** и отправляет сообщение в JMS/Kafka по **in order** помечая их отправленными. Если упадет **message relay** перед тем как пометит сообщение **отправленным**, то при старте переотправит еще раз, поэтому **subscriber** должен быть **idempotent** (не менять свое состояние для одних и тех же данных). Как вариант, каждое сообщение должно иметь **id** и когда **message relay** отправит заново, то **subscriber** сравнит **id** (из своей **inbox table**) и не запустит **update** (т.к. он уже был).
    * **Message Relay.** - отдельный процесс(ы), способы перехвата **update event** для **Message Relay**, чтобы отправить сообщения в отдельном потоке, в том числе после падания сервиса.
      1. **Transaction log tailing.** Перехват событий на уровне **DB**, через **transaction log** принадлежащий DB, куда записываются все успешные **transactions**, требует для каждой DB свой специфичный механизм. Процесс перехавата события называется [Log-based Change Data Capture](https://debezium.io/blog/2018/07/19/advantages-of-log-based-change-data-capture/) (CDC). **Плюсы:** быстро работает. **Минусы:** т.к. все на низком уровне, то реализация скрыта и нет удобного способа трансформировать сообщения или реагировать на условия (т.к. неудобно писать программный код). 
         * Example: [Debezium](https://debezium.io/) и [Eventuate Tram](https://github.com/eventuate-tram/eventuate-tram-core)
      2. **Polling publisher.** Использование **outbox table** для сообщений и их отправка в отдельном процессе **Message Relay**. **Плюсы:** работает со всеми DBs, прозрачная реализация.
         * Example: [Eventuate Tram](https://github.com/eventuate-tram/eventuate-tram-core)
    * **Why to use?**
      * Без этого при сообщение в JMS/Kafka отправится и при откатившейся **transaction**. А при успешной transaction, но падении системы отправки - сообщение не отправится. Отправка сообщения будет в той же транзакции что и `update/create/delete` при успешном выполнении, без этого нарушится порядок выполнения транзакций и отправки. Send происходит автоматически - меньше случайных ошибок с забыванием отправить.
      * **Почему нельзя использовать 2PC?** JMS/Kafka может не поддерживать **2PC**, даже если поддерживает, то **2PC** сильно связывает сервис(ы) отправляющий сообщения c JMS/Kafka и DB (это антипаттерн high coupling, т.е. система 2PC работает в сервисе как library).
    * **Features**
      * Делает **atomic update** записи в DB и ее отправку в JMS/Kafka. Решает проблему **dual write** в distributed system.
      * Режимы отправки/приема. Делается на уровне кода и/или настройкой **Kafka/JMS**.
        * **at-least-once** (частая реализация) - сообщений будет как минимум 1но (второе и более сообщение ничего не поменяет, только потратит ресурсы)
        * **exactly-once** (менее частая, с доп. проверкой, ресурсоемкая) - точно 1но
        * **at-most-once** (нарушает консистентность, для некритичных сообщений которые можно потерять) - не больше 1го, или ничего не отправлять при ошибке
    * **Альтернатива.** Паттерн **Event sourcing** - альтернатива для **Transactional outbox**
    * **Example**
      * <details>
        <summary>Example</summary>
        
        Источник: [1](https://medium.com/event-driven-utopia/sending-reliable-event-notifications-with-transactional-outbox-pattern-7a7c69158d1b), [2](https://microservices.io/patterns/data/transactional-outbox.html)
        ```sql
        -- OUTBOX table
        create table OUTBOX (
            id varchar(255) primary key,    -- message id
            aggregate_type varchar(255) not null, -- type of entity/record/row/table_name
            aggregate_id varchar(255) not null, -- entity/record/row id
            type varchar(255) not null, -- crud type
            payload text not null   -- json, entity fields
        );
        ```
        </details>
  * **Transactional inbox** - как работает: создаем **inbox** table в DB, сохраняем полученные сообщение в **inbox** используя ее в качестве очереди, отдельный поток(и) в фоне берут из **inbox** table записи и обрабатывают. Другой отдельный поток(и) удаляют обработанные сообщения для снижения нагрузки на DB.
    * **When to use?**
      * Message broker does not support **message order** - inbox restore the order
      * Message broker persist **messages short life time** - inbox persis messages temporary until proceed
      * Need to handle messages in background process(es) to **reduce CPU or RAM high load** - inbox is queue, background process(es) handle messages
      * Need **exactly-once** - inbox can **skip** messages that were handled already
    * **Why to use?**
      * Сохранить порядок сообщений, в том числе на основе поля **version** или **timestamp**
      * Сохранить полученное сообщение и обработать его попозже - снижение нагрузки на CPU и решить проблему того что **message broker** не должен долго хранить у себя сообщения (т.к. иначе нагрузка ляжет на **message broker**)
      * Пропускать уже обработанные сообщения если они были получены раньше - путем сравнения сообщения в статусе **done** в табл **inbox** и полученного
    * **Implementation features**
      * Удалять из **inbox** не обязательно сразу, можно через некоторое время, чтобы иметь возможность сравнивать их статусы и пропускать повторно пришедшие и просматривать историю обработки.
      * Можно запускать фоновые обработчики в несколько потоков, в том числе можно запускать асинхронно поток обработчик сразу после окончания транзакции сохранения в табл **inbox**
      * Можно использовать **at-least-once** для снижения нагрузки, если система **idempotent** и повторная обработка не вызовет проблем. И если повторная обработка не затратна по high load
      * Можно регулировать нагрузку на CPU сервиса путем добавления задержек в обработке
    * **Cons**
      * Нагрузка на DB - мы меняем нагрузку на message broker, RAM и CPU на нагрузку на DB
      * Больше кода
      * Увеличение времени обработки (задержки)
    * **Example**
      * <details>
        <summary>Example</summary>

        Source: [1]([https://medium.com/event-driven-utopia/sending-reliable-event-notifications-with-transactional-outbox-pattern-7a7c69158d1b), [2](https://microservices.io/patterns/data/transactional-outbox.html](https://softwaremill.com/microservices-101/))
        ```sql
        -- INBOX table
        create table INBOX (
            id varchar(255) primary key,
            payload varchar,
            version integer   -- optionally for message order, extracted from payload or message broker metadata
        );
        ```
        </details>
* **Data management**
  * [Event sourcing](https://microservices.io/patterns/data/event-sourcing.html) - при `create/update/delete` в **DB** сохраняется набор транзакций с полями и их значениями (т.е. только измененные поля **row**). Когда нужно запросить данные из table происходить **reconstruction** через **increment** всех **transactions**. После каждой **reconstruction** делается **snapshot** последней версии **row** на основе которой делается **increment** со следующим **event** (для performance)
    * **Pros.**
      * reliably publish events
      * audit log
      * history - can get state of any point in time
    * **Note.** `CQRS` pattern - один из подходов, где **reconstruction** делается через запрос к **view** которая и делает **reconstruct**
    * Note. **Transactional outbox** vs **Event sourcing** - формально **Transactional outbox** сохраняет всю **row** в **outbox table** и отправляет в другой **microservice**, а **Event sourcing** хранит набор **transactions** по которым можно сделать **reconstruction** и не занимается отправкой в другой **microservice**. На практике эти patterns могут использоваться вместе, **Event sourcing** создает сообщение и **Transactional outbox** и отправляет с гарантией.
    * <details>
      <summary>Example</summary>
      
      Источник: [1](https://medium.com/ssense-tech/event-sourcing-part-i-understanding-what-it-is-core-components-and-common-misconceptions-5dec65f6d61), [2](https://medium.com/ssense-tech/event-sourcing-part-ii-implementing-an-end-to-end-solution-68b7dbefe89), [3](https://microservices.io/patterns/data/event-sourcing.html)
      ```sql
      ```
      </details>
  * [Saga](https://microservices.io/patterns/data/saga.html) - альтернатива для [2PC](https://en.wikipedia.org/wiki/Two-phase_commit_protocol) `(Two-phase commit protocol)` транзакции, создает распределенную на несколько микросервисов транзакцию. Это коллекция локальных транзакций, когда 1на транзакция завершена она публикует событие, которое вызывает срабатывание следующей транзакции **или** при ошибке вызывается специальная **транзакция для отката** (т.е. обычный откат тут не сработает, нужно описать его логику). Варианты реализации: через **Choreography** или **Orchestration**.
    * **Types**
      * **Choreography** - каждая обычная транзакция (которая нуждается в транзакциях из других сервисов) публикует события (**e.g.** в kafka) по которым другие сервисы понимают какую локальную транзакцию выполнить у себя.
      * **Orchestration** - есть общий управляющий объект **Saga** через который идет управления транзакциями, т.е. он запускает локальные транзакции в других сервисах через публикацию событий (**e.g.** в kafka).
        - **E.g.** Создание `CreateOrderSaga` вызывает отправку запроса и авто подписку на ответ.
        - **E.g.** сам запрос выполнится объектом паттерна **Saga**, а сервис в данном случае просто **создает DoSmthTransactionSaga объект**, который сделает **request** и обработает результат.
      - **Choreography** vs **Orchestration** - в **Choreography** другие **сервисы получатели** получают через kafka/jms сообщение и сами запускают транзакцию, в **Orchestration** реализацию паттерна внутри **сервиса источника** события запускает транзакции сама (сообщает сервисам какую транзакцию им запустить).
    - **Way to determine outcome** - механизм через который клиент (e.g. браузер, клиент api) может понять что транзакция **Saga** завершена. Используется **1ин** из вариантов ([1](https://microservices.io/patterns/data/saga.html)):
      1. сервис возвращает ответ по **REST** когда Saga завершится (долгое ожидание ответа)
      2. сервис возвращает ответ по **REST** с **id** записи в DB когда **стартует Saga**, клиент **периодически** по **REST** пытается читать эту запись по id чтобы понять **закончилось** ли выполнение транзакции. **Note.** Это странный подход?
      3. сервис возвращает ответ с **id** записи в DB когда **стартует Saga**, когда транзакция завершится сервис возвращает событие-сообщение **успешно завершилась** (e.g. по websocket, web hook)
    - **Saga** - это **sequence of transactions**, каждая **transaction** отправляет event/message в message broker чтобы запустить следующую **transaction**. Если одна из **transactions** fails, то создаем **compensating transactions**, которые откатывают **все** прошлые **transactions**.
    - **compensating transaction** - транзакция которая меняет состояние записи в базе данных на исходное, аналог отката транзакции в **2PC** и **ACID**, набор таких транзакций для каждой из измененных записей в DB **Saga** использует чтобы откатить всю транзакцию
    - **Long Lived Transactions** (LLT) - **sequence of transactions** в **Saga**, между разными services, если она fail, то нужно создавать **compensating transaction** для каждой **transaction**. Механизм **compensating transaction** усложняет реализацию Saga, поэтому его нужно применять только когда он нужен.
      - **Note.** СЛово **Long** в данном случае лишнее, т.к. любую **transaction** в **Saga** можно назвать **Long**.
    - **Note.** Обе **Choreography** и **Orchestration** должны иметь возможность описать логику отката транзакций (использовать **compensating transaction**). Реализована через **transactionId**, номер транзакции хранится и при подписке на **domain event** запускается обработчик отката для этой **transactionId**, если нужно.
    - **Note.** Для отправки сообщений **Saga** использует паттерн **Event sourcing** или **Transactional Outbox**, без них порядок сообщений и целостность DB не гарантирована
    - **Note.** `Saga` управляет процессами, которые необходимо выполнять в `transaction`, сохраняя состояние выполнения и компенсируя неудачные процессы. Похоже на [Finite-state machine](https://en.wikipedia.org/wiki/Finite-state_machine) `(конечный автомат)` или [BPMN](https://en.wikipedia.org/wiki/Business_Process_Model_and_Notation) `(Business Process Model and Notation)`, только когда идет по шагам работает с **async** событиями. Можно сказать это работа с **async** событиями в стиле как если бы они были **sync** (более удобный код).
    - **Saga vs 2PC**
      - Для `Saga` **Orchestration** нужно писать логику отката **transaction** вручную.
      - `Saga` has no `I` from `ACID` - has no data **isolation**
      - `Saga` can be used with any DB that has no **2PC**
      - `Saga` services has **local table replicas** and can work when some another services **offline**
    - **Note** есть [библиотека Redux-Saga](https://redux-saga.js.org/) для **front-end** разработки с `Redux`, где **Saga** используется для **async** запросов (выполнения, остановки, отката по условиям).
  * [CQRS](https://learn.microsoft.com/en-us/azure/architecture/patterns/cqrs) - разделение операций на **read** и **write**, со своим **api** и **model** (table и/или dto с нужным набором полей/данных). **Command** - запрос который делает `update/create/delete`, **Query** - запрос который делает `select`.
      * **Коротко:** для read и write операций делаются разные table/view в DB, иногда даже разные DB (e.g. NoSQL), **read model** синхронизируется через **events** от **write model**, e.g. для **read** операций создается **view** (или materialized view) чтобы не делать лишние **join** или не собирать его в коде сложным mapping.
      * **Коротко для event sourcing:** чтобы реконструировать **model** из множества events (сделать increment всех events) **сложный select** из **read-only-replica** делается через **view**
          * **Note.** Это не лучший подход т.к. лучше иметь **snapshot** вариант table/entity с которым каждый event будет делать **increment**, для него и делать **query**. Т.е. в базе будет храниться актуальная версия/**replica**, на нее будут накладываться пришедшие по kafka/jms сообщения. 
      * **Как работает:** `write` операции порождают события которые обновляют состояние `model view`, `read` операции одной и той же `model` в DB могут иметь разные `api` для разных частей `model` для ускорения, т.е. разные `view` могут делать `join` разных `model` чтобы вернуть dto как одно целое
      * **Why to use?**
          * Улучшает performance, scalability, and security
          * `write model` не зависит от `read model` и обе могут быть нормализованы под свои операции (`write model` меняет только нужные ей данные, `read model` возвращает `dto` с нужным набором данных которых нет в `write model`). Т.е. **CQRS** для read может дать **больше чем 1но** api с разными dto.
      * **Features**
        * **CQRS** для `read` может дать **больше чем 1но** `api` с разными `dto` и разными `join` через `view`.
      * **When to use?**
          * Many users - can reduce DB locks
          * Complexity business logic - easy to split complex read and write logic
          * Many read operations - to get rid of common read-write excessive mode/dto
          * Separate read and write development - different teams
          * Refactoring - read and right models can evolve independently
          * Integration (e.g. event sourcing pattern) - fail of write model system should not to fail read model system
      * **Cons**
          * Нельзя генерировать CRUD код автоматически, например через ORM т.к. структуры read model и write model будут не совпадать, в некоторых случаях они будут даже в разных DBs
          * Complexity - сложнее реализовать
          * Messaging - нужно реализовать inbox, outbox patterns и соблюдать event order
          * Eventual consistency - read model обновляется с задержкой
      * **Pros**
          * Independent scaling - Нет лишний операция для write model и ead model, каждая работает только с данными которые ей нужны, меньше transaction locks, read & write systems can work independently
          * Optimized data schemas - write model и ead model используют model оптимальные для них, а не 1ну общую
          * Security - легче сделать ACL (сделать разные ролевые модели)
          * Separation of concerns - легче делать refactoring и писать business logic под конкретный case
      * **Alternative**
        * Composite API pattern - вместо обращение к replica происходит обращение по REST (или gRPC) и join данных делается в коде
  * [API Composition](https://microservices.io/patterns/data/api-composition.html) (or Aggregation) - происходит обращение по REST (или gRPC) и join данных делается в коде **in-memory**
    * **Features**
      * Types:
        * **Parallel** - parallel requests to another services
        * **Chain** - each next service do a request to another service
        * **Branch** - do request to service1 or serviceN depends on conditions
    * **Pros**
      * Simple
    * **Cons**
      * inefficient queries because in-memory joins
      * Can not react on events in another microservices
      * Repeated reads of the same external data each call from another service
    * **Alternative**
      * CQRS pattern
* **Cross cutting concerns**
  * [Externalized configuration](https://microservices.io/patterns/externalized-configuration.html) - config применяется в зависимости от среды (config для DB, network, credentials etc), config может читаться из внешнего источника (`configMap`), `environment variables` etc.
    * **Example:** [Spring Boot externalized configuration](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.external-config)
* **Communication style**
  * [Remote Procedure Invocation (RPI)](https://microservices.io/patterns/communication-style/rpi.html)
    * **Example:** REST, gRPC, Apache Thrift
    * **Pros**
      * Easy
      * No **message broker** (kafka/jms)
    * **Cons**
      * Services has to be always online (no local table replicas)
      * Tightly coupled (services has to know about api of another services)
      * Need to use **Service Discovery** (e.g. Eureka)
  * [Service Template](https://microservices.io/patterns/service-template.html) - template for all microservices
    * Cons. need to copy code each time, need update all code, need to have different template for all languages
    * Pros. Fast dev, common code, best practices
* **External API**
  * [API gateway](https://microservices.io/patterns/apigateway.html)
    * К нему могут быть подключены: Load Balancer (Zuul), ACL (Access Token), Rate limiter
  * [Backends for Frontends](https://microservices.io/patterns/apigateway.html) - multi API Gateways different for each Web, Mobile etc
* **Service discovery**
  * [Client-side discovery](https://microservices.io/patterns/client-side-discovery.html)
  * [Server-side discovery](https://microservices.io/patterns/server-side-discovery.html)
  * [Service registry](https://microservices.io/patterns/service-registry.html)
  * [Self registration](https://microservices.io/patterns/self-registration.html)
  * [3rd party registration](https://microservices.io/patterns/3rd-party-registration.html)
* **Security**
  * [Access Token](https://microservices.io/patterns/security/access-token.html) - **user jwt** который проверяется на **access** доступ к какому-то api на уровне **API Gateway** (api которое стоит за этим **API Gateway**). В классическом варианте проверяется только **roles** (роли). Или **jwt** проверяется в рамках **service**, если для проверки нужны данные из **DB**, права на конкретную **row**, а не все **api**. Передается **jwt** и в другие **services** через **http header** для **cross service requests** (чтобы права проверялись и там).
    * **Feature**
      * Может быть **technical jwt**, для запросов которые делает сам **service** без участия **user jwt** (e.g. scheduler, message consuming etc), **technical jwt** можно задать через **external configuration** (kubernetes configMap) или получать через replication
    * **Pros**
      * Can check cross service requests (jwt is passed across services through http header)
* **Reliability**
  * [Circuit Breaker](https://microservices.io/patterns/reliability/circuit-breaker.html) - requests are made through a proxy (circular breaker), if calls fail to frequently then proxy temporary forbid calls with some timeout, after timeout the proxy allows calls again
    * Cons. need to choose timeouts correctly
    * Pros. prevent DDoS, switch off failed microservice and all requests go to working microservices
* **Observability**
  * [Log aggregation](https://microservices.io/patterns/observability/application-logging.html) - log server to collect and see logs, e.g. ELK stack
  * [Application metrics](https://microservices.io/patterns/observability/application-metrics.html) - collect metrics and send to a metric server and use a UI to show it. Eg. actuator + prometheus + grafana
    * Note. **pull** - metric server makes a request to a microservice (e.g. http health request to a microservice), **push** - microservice send metric to a metric service (e.g. microservice send some count value to metric service)
  * [Audit logging](https://microservices.io/patterns/observability/audit-logging.html) - some business logic has an audit event to send some info to audit server, e.g. Spring Aspect around methods to send an audit info to audit service
  * [Distributed tracing](https://microservices.io/patterns/observability/distributed-tracing.html) - tracing server that collect **each request id**, start time, end time for each method (**span**). We can add custom **span** in code to track **custom** logic start/end time. E.g. Zipkin, Jaeger
  * [Exception tracking](https://microservices.io/patterns/observability/exception-tracking.html) - like **Log aggregation** but we track exceptions
  * [Health check API](https://microservices.io/patterns/observability/health-check-api.html) - microservice has a REST end point, the end point run a logic to check all the microservice health checks, a health service periodically sends requests to the `/health` end point. E.g. **Spring Actuator** create `/health` end point, **Eureka** registry service sends health check requests periodically.
  * [Log deployments and changes](https://microservices.io/patterns/observability/log-deployments-and-changes.html) - log deployment, e.g. kubernetes can do it
* **Design and implementation patterns**
  * [Anti-Corruption Layer](https://learn.microsoft.com/en-us/azure/architecture/patterns/anti-corruption-layer) - adapter or facade between old or different systems to convert request/response
    * Cons. latency, additional service
  * [Strangler Fig](https://learn.microsoft.com/en-us/azure/architecture/patterns/strangler-fig) - replace an (legacy) api of a service peace by peace and delete the api from an original (legacy) service
    * Cons. Need to support both new and legacy services
  * [Sidecar](https://learn.microsoft.com/en-us/azure/architecture/patterns/sidecar) - deploy part of an app as a **separated process** in the same docker container (the same host) or in a different container.

# Functional patterns

* **Pure function** - функция которая возвращает одни и те же значения для одних и тех же переменных. Она обязана возвращать значение (не может быть **void**) Т.е. ее состояние не меняется (например через ссылки на объекты, которые в ней содержатся и влияют на ее работу). И ее выполнение не имеет **side effect** (не изменяются local static variables, non-local variables. references). Это аналог математической функции.
* **Higher Order Component** (HOC) - это функция или модуль в js (т.к. модуль это по сути функция), которая оборачивает (содержит) другую функцию (модуль). Она преобразовывает передаваемые внутрь вложенного в нее модуля данные или возвращаемые вложенным модулем данные. Похоже на паттерн **Adapter** или **Proxy** из GoF.
* **Higher Order Function** - ф-ция, которая принимает другую ф-цию как параметр.
* **Projection Function** (отображение) - 
* **compose** - композиция, ф-ция применяет ф-ции по порядку к параметру
    ```
    compose(x)(fn1, fn2, fn3) == fn3(fn2(fn1(x)))
    ```
* **combine** - выполнить одну группу функций и передать их результат другой как входной параметр
    ```
    combine(f1(x), f2(y), f3(a, b) -> a + b) // a, b are results of f1 and f2
    ```

# Concurrency
Пусто

# Architectural

## Service Locator
**Service Locator** - состоит из:
* **Service** - общий интерфес для все сервисов: execute() и getName()
* **Initial Context** - получаем service по его name
* **Cache** - кэширует сервисы

Service Locator - содержит Initial Context и Cache. Если Service есть в Cache, то возвращает его, иначе достает из Initial Context. Возвращенные из Initial Context сервисы имеют общий метод execute()

**Dependency Injection** - 

**Dependency Injection vs Service Locator** - 

## Гексагональная архитектура (a.k.a. Порты и адаптеры)

* core (**domain**) — это сердце системы. Здесь сосредоточена бизнес-логика, модели данных и правила. Ядро ничего не знает о том, как и откуда поступают данные или как они отображаются.
* port (repository, controller etc **interfaces**) — это интерфейсы, которые ядро предоставляет внешнему миру или требует для своей работы. Они задают, что именно нужно ядру.
* adapter (repository, controller etc **implementation**) — это реализации портов, которые связывают ядро с внешним миром (например, базой данных, веб-сервером или внешними API).
<details>
  <summary>Example</summary>
  
  ```
  src/
  ├── core/
  │   ├── domain/
  │   │   ├── Booking.java          // Модель бронирования
  │   │   ├── Room.java             // Модель комнаты
  │   │   └── Guest.java            // Модель гостя
  │   ├── ports/
  │   │   ├── BookingRepository.java // Интерфейс для работы с бронированиями
  │   │   └── PaymentService.java    // Порт для работы с платежами
  │   └── services/
  │       ├── BookingService.java    // Логика бронирования
  │       └── AvailabilityService.java // Проверка доступности номеров
  ├── adapters/
  │   ├── db/
  │   │   ├── MySQLBookingRepository.java       // Репозиторий для MySQL
  │   │   └── RedisAvailabilityCache.java       // Кэш доступности номеров
  │   ├── payment/
  │   │   └── StripePaymentService.java         // Платежная система Stripe
  │   ├── http/
  │   │   ├── BookingController.java            // REST API для бронирований
  │   │   └── RoomController.java               // REST API для комнат
  │   └── ui/
  │       └── WebBookingAdapter.java            // Веб-интерфейс (например, React)
  └── Application.java                           // Главный класс
  ```
</details>

## Microservice cache
Source: [hazelcast patterns](https://hazelcast.com/blog/architectural-patterns-for-caching-microservices/)

* **Embedded Cache** - локальный кэш, часть приложения, каждая pod имеет свой
* **Embedded Distributed Cache** - как Embedded Cache, но синхронизируется по сети между разными pods
* **Client-Server Cache** - кэш развернут как DB и доступен по сети и поэтому медленнее, как redis
* **Cloud Cache** - как Client-Server Cache, но кэш во вне в облаке
* **Sidecar Cache** - как Embedded Distributed Cache, но кэш запущен как sidecar в docker, отдельное приложение в `localhost`
* **Reverse Proxy Cache** - кэш подключается к load balancer, например nginx, не нужно менять код в приложении
* **Reverse Proxy Sidecar Cache** - как Sidecar Cache, но запрос перехватает библиотека reverse proxy встроенная в pod (не в load balancer), не зависит от кода

# Data Processing Models
* OLTP - Online Transaction Processing - много транзакций с ACID, для банкинга или резервирования, e.g. PostgreSQL
* OLAP - Online Analytical Processing - хранит неструктурированные данные в спец базах для систематизации и аналииза e.g. dashboard, используется подход data warehouses, e.g. Apache Druid
* HTAP - Hybrid Transactional and Analytical Processing - OLTP+OLAP, как OLAP, но с тразакциями OLTP, для real time анализа данных, e.g. real time recommendations или live financial market analysi, e.g. CockroachDB
* Distributed Processing - e.g. Apache Hadoop
* Edge Processing - обработка данных на их источнике, уменьшает latency, e.g. navigation systems, e.g. AWS IoT Greengrass
* Stream Processing - real time process, low latency, e.g. Apache Kafka
* Batch Processing - обработка данных batch'ами по scheduler, e.g. Apache Hadoop

# Other patterns

* **Delegate** - принимает ссылку на объект, оборачивает вызов методов полученного объекта в свои методы, может наследовать интерфейс с методами. Делегирует свою работу другому объекту.
  * **Delegate vs Facade** - **Delegate** имеет ссылку только на один объект и он принимает ссылку на целевой объект **при создании Delegate** и делегирует действие другому объекту. **Facade** слой который скрывает сложность операции, которую реализует сам путем вызова каких-то методов.
* **SAM паттерн** - (не один из основных, возможно относит к функциональному программированию)
    https://github.com/jeffbski/redux-logic#implementing-sampal-pattern

# POJO vs DTO vs VO vs Java Bean vs JDO

* **POJO** (Plain Old Java Object) - публичный класс который не зависит от interfaces и frameworks, имеет public get/set, Java Bean это тоже POJO, но не всегда наоборот
* **Java Bean** - это class который: **public class**, has no **default args constructor**, has **getters/setters/is**, implements `java.io.Serializable` (или др., но класс должен быть **сериализуемым**). **Why:** чтобы механизмы могли находить методы (get/set) по соглашению о именах
* **DTO** (Data Transfer Object) - для передачи данных между приложениями, другое название Model слоя из MVC
* **VO** (Value Object) - как DTO, только для обмена данными внутри приложенки, а не с БД (как в случае с DTO)
* **JDO** (Java Data Objects) - вид POJO, из ORM/JPA, это domain/entity/model, т.е. представление записи из DB

# Development principles (SOLID, DRY, KISS, YAGNI, DDD)

* **SOLID principles** (принципы):
  1. **Single responsibility** - каждый класс должен отвечать за ОДНУ обязанность
  2. **Open-closed** - открыты для расширения, закрыты для изменения (наследование)
  3. **Liskov substitution** - ссылка базового может указывать на наследника
  4. **Interface segregation** (разделения) - разделять универсальные метод на много спец. методов (с меньшим количеством параметров)
  5. **Dependency inversion** - модули верхних уровней не зависит от модулей нижнего.
     * **Note.** т.к. система строится на основе абстракций (Абстракции не должны зависеть от деталей. Детали должны зависеть от абстракций)
* **DRY** or **DIE** (Don’t Repeat Youself or Duplication Is Evil) — не повторяйся. Нужно избегать повторений одного и того же кода. Лучше использовать универсальные свойства и функции.
* **KISS** (Keep It Simple, Stupid) — стоит делать максимально простую и понятную архитектуру, применять шаблоны проектирования и не изобретать велосипед.
* **YAGNI** (You Ain’t Gonna Need It) — реализовать только поставленные задачи и отказаться от избыточного функционала.
* **DDD** (Domain driven design) - принцип проектирования систем, часто применяется когда логика систем незнакома. **Note.** Это не универсальный подход, применяется по необходимости.
  * **Context**
  * **Domain** - область к которой относится программа (магазин, аэропорт etc), можно несколько
  * **Model** - модели для области **Domain** (entities, tables, classes)
  * **Ubiquitous Language** - язык описания программы

# is A vs Has A

**is A** (Inheritance) - является ЛИ класс наследником другого, Если есть **is A** мы делаем **extends**  
**Has A** (Composition) - содержит ли класс сущность, если есть **has a** мы делаем переменную класса (**embeded**)

`class Mercedes extends Car {} // Mercedes is A Car (use Inheritance)`  
`class Car { Radio radion } // Car has a Radio (use Composition)`

# Composition, Aggregation, Association
Source: [1](https://www.baeldung.com/java-composition-aggregation-association)

Типы связей между объектами.

* **Composition** (Has A) - сильная связь между связанным вложенным объектом, жизненный цикл вложенного прерывается когда прерывается цикл родителя
* **Aggregation** (Has A) - вложенные объекты не зависят от родителя, их можно заменять и они могут принадлежать нескольким родителям
* **Association** (не has-a, objects **know** each other) - как Aggregation, но логически объекты не зависят друг от друга, а только знают друг о друге. Явных отличий от Aggregation нет, только логически.

# Cohesion vs Coupling

**Cohesion** - (single responsibility), low Cohesion когда класс должен брать на себя как можно меньше ответственности (реализовывать как можно меньше функций, больше функций выносить в другие классы)

**Coupling** - (low coupling == хорошая инкапсуляция), классы должны как можно меньше знать друг и друге (т.е. должны видеть меньше public переменных друг друга)

# Context
Идея о том, что у всего должно быть имя (в виде строки). Чтобы получить доступ к объекту по имени, необходимо иметь некоторый интерфейс, а именно, Context. То есть контекст - это интерфейс, который позволяет по данной строке получить объект. Например:
```java
Context ctx = new InitialContext(env); // Create the initial context
Object obj = ctx.lookup(name); // Look up an object
if (obj == null) throw Exception("Problem looking up name + ");
obj.doSmth(); // do something
```

# GRASP, General Responsibility Assignment Software Patterns
Source: [wiki](https://ru.wikipedia.org/wiki/GRASP)

**GRASP** - не имеет четкой структуры, области применения, решаемой проблемы в отличии от GoF паттернов, только предоставляют принципы дизайна систем.

# Immutable object

* **Создание immutable объекта:**
  1. Не делать `setters` методы
  2. Все поля `private final`. **Note!** в Java **все** поля должны быть final даже если у них нет setter, из-за особенностей правил **happens before**
  3. Методы `final` (чтобы не дать override)
  4. Все изменения mutable полей возвращают НОВЫЙ объект. immutable поля возвращать как обычно.

Дополнительно: можно сделать `private constructor` и создавать в `factory methods`

* **Плюсы:**
  * могут использоваться в кэшах, работа с кэшированными объектами может быть быстрее (e.g пулы в Java основанные на Flyweight);
  * в многопоточности параллельные потоки не могут случайно изменить такие объекты.

**Note.** В некоторых коллекциях (например **Set** в java) где используется **hash code** добавленного обьекта изменение полей этого обьекта "испортит" hash code (он перестанет быть таким как при добавлении) и т.к. первоначально поиск идет по hash code, то работа такой коллекции сломается. Чтобы такого не случилось можно добавлять в коллекции immutable объекты. Такие объекты могут **кэшироваться автоматически**, доступ к ним быстрее и они быстрее работаютнапример в **Map** в качестве ключа.

# Horizontal (`scale out`) vs Vertical scaling (`scale up`), Вертикальное vs Горизонтальное масштабирование

**Вертикальное** (Horizontal) — это когда добавляют больше оперативки, дисков и т.д. на уже существующий сервер,  
**Горизонтальное** (Vertical) — это когда ставят больше серверов в дата-центры, и сервера там уже как-то взаимодействуют.

Плюс **Горизонтального масштабирования** в том, что если система рассчитана на добавления кластеров (node, серверов), то наращивание мощности **горизонтально** не требует остановки системы в процессе наращивания. Но сложность работы с такой системой выше. После определенного порога **горизонтальное масштабирование** становится гораздо дешевле **вертикального**.

Минус **Горизонтального масштабирования** - на отказоустойчивость и общение кластеров между собой расходуются ресурсы (CAP теорема).

Минус **Вертикального масштабирования** - единая точка отказа, если один сервер упал - упало все приложение.

# Fine-grained vs Coarse-grained
**Fine-grained** - обычные Service классы, маленького размера  
**Coarse-grained** - большие Service классы, их методы вызвают методы **Fine-grained** сервисов

# Список ООП vs functional концепций:

https://fullstackengine.net/fullstack-javascript-interview-questions/

# Game Programming Patterns
Source: [gameprogrammingpatterns](http://gameprogrammingpatterns.com/contents.html)

# Источники
* [sourcemaking.com Design Patterns](https://sourcemaking.com/design_patterns)
* [Cloud Design Patterns](https://learn.microsoft.com/en-us/azure/architecture/patterns/)
* [A pattern language for microservices](https://microservices.io/patterns)
* [The System Design Primer](https://github.com/donnemartin/system-design-primer)