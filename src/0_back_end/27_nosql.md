Обзор не реляционных баз данных

типы баз данных - OLTP, OLAP, типы баз данных (ClickHouse, Hadoop, MongoDB, Redis, TimeScaleDB, Casandra)

кэш - redis

***

### **Системы обработки данных** (Data Processing Systems):
Эти системы предназначены для обработки, анализа и хранения данных. В зависимости от того, как и для чего они используются, их классификация может быть следующей:
1. **OLTP (Online Transaction Processing)** — обработка транзакций в реальном времени.
2. **OLAP (Online Analytical Processing)** — аналитическая обработка данных.
3. **HTAP (Hybrid Transactional/Analytical Processing)** — гибридная обработка транзакций и аналитики.
4. **Real-Time Data Processing** — обработка данных в реальном времени.
5. **Batch Processing** — обработка данных пакетами, обычно в определенные интервалы времени (например, ежедневные отчеты).
6. **Stream Processing** — обработка потоковых данных, например, для мониторинга в реальном времени.
7. **Event-Driven Processing** — обработка данных на основе событий (например, при поступлении новых данных).

### **Типы баз данных** (Database Types):
Это архитектуры и структуры, которые используются для хранения и управления данными. Основные типы баз данных:
1. **Relational Databases** (Реляционные базы данных) — базы данных, основанные на таблицах (например, MySQL, PostgreSQL).
2. **NoSQL Databases** — базы данных, использующие другие модели данных, например, для неструктурированных данных.
   - **Document Stores** (хранение документов, например, MongoDB).
   - **Key-Value Stores** (например, Redis).
   - **Columnar Databases** (например, Apache Cassandra).
   - **Graph Databases** (например, Neo4j).
3. **Time-Series Databases** — оптимизированные для временных рядов (например, InfluxDB).
4. **In-Memory Databases** — базы данных, работающие в оперативной памяти (например, Redis).
5. **Distributed Databases** — распределенные базы данных, хранящие данные на нескольких серверах (например, Cassandra).
6. **Multimodel Databases** — базы данных, поддерживающие несколько моделей данных (например, ArangoDB).

Таким образом, **системы обработки данных** — это процессы и подходы к обработке информации, тогда как **типы баз данных** относятся к способам хранения этих данных.
***
### Типы баз данных:

1. **Relational Databases (Реляционные базы данных)**:
   - **Цель**: Управление структурированными данными с обеспечением целостности данных через нормализацию.
   - **Дизайн базы данных**: Применение реляционной модели, например, MySQL, PostgreSQL.
   - **Примеры систем**: MySQL, PostgreSQL, Oracle Database.
   - **Примеры запросов**:
     ```sql
     SELECT * FROM users;  -- Пример для MySQL/PostgreSQL
     SELECT name, age FROM employees WHERE department = 'Sales';  -- Пример для Oracle
     UPDATE products SET price = 10.99 WHERE id = 5;  -- Пример для PostgreSQL
     DELETE FROM orders WHERE order_date < '2025-01-01';  -- Пример для MySQL
     ```

2. **NoSQL Databases**:
NoSQL включает в себя различные типы баз данных, каждая из которых оптимизирована для определенного типа данных. Вот основные типы:

   - **Key-Value Stores**:
     - **Цель**: Хранение данных в виде пар "ключ-значение", что позволяет быстро получать данные по ключу.
     - **Дизайн**: Простой и быстрый доступ к данным.
     - **Примеры систем**: Redis, DynamoDB, Riak.
     - **Примеры запросов**:
       ```bash
       SET user:1000 '{"name": "Alice", "age": 30}';  -- Пример для Redis
       GET user:1000;  -- Пример для Redis
       DEL user:1000;  -- Пример для DynamoDB
       ```

   - **Document Stores**:
     - **Цель**: Хранение данных в виде документов, обычно в формате JSON или BSON, что подходит для работы с неструктурированными данными.
     - **Дизайн**: Денормализованные данные, оптимизированные для быстрого поиска по ключу.
     - **Примеры систем**: MongoDB, CouchDB.
     - **Примеры запросов**:
       ```bash
       db.users.find({ "name": "Alice" });  -- Пример для MongoDB
       db.users.insertOne({ "name": "John", "age": 30 });  -- Пример для MongoDB
       db.users.updateOne({ "name": "John" }, { $set: { "age": 31 } });  -- Пример для CouchDB
       db.users.deleteOne({ "name": "John" });  -- Пример для CouchDB
       ```

   - **Columnar Databases**:
     - **Цель**: Использование колонок для хранения данных, что идеально для аналитических запросов по большим данным.
     - **Дизайн**: Оптимизация для запросов, которые работают с большими объемами данных по столбцам.
     - **Примеры систем**: Apache Cassandra, HBase.
     - **Примеры запросов**:
       ```sql
       SELECT name, age FROM users WHERE age > 30;  -- Пример для Apache Cassandra
       INSERT INTO users (name, age) VALUES ('Alice', 29);  -- Пример для HBase
       ```

   - **Graph Databases**:
     - **Цель**: Идеальны для работы с взаимосвязями между данными (графы), например, социальных сетей.
     - **Дизайн**: Графовые структуры для представления сложных взаимосвязей.
     - **Примеры систем**: Neo4j, ArangoDB.
     - **Примеры запросов**:
       ```cypher
       MATCH (a:Person)-[:FRIEND_OF]->(b:Person) RETURN a, b;  -- Пример для Neo4j
       CREATE (a:Person {name: 'John', age: 30});  -- Пример для ArangoDB
       ```

   - **Wide-Column Stores**:
     - **Цель**: Строки могут иметь разные столбцы и несколько значений, что идеально подходит для хранения и анализа больших данных.
     - **Дизайн**: Оптимизация для масштабируемости с разными колонками для каждой строки.
     - **Примеры систем**: Apache Cassandra, HBase.
     - **Примеры запросов**:
       ```cql
       INSERT INTO users (name, age) VALUES ('John', 30);  -- Пример для Apache Cassandra
       SELECT name, age FROM users WHERE age > 25;  -- Пример для HBase
       ```

   - **Multimodel Databases**:
     - **Цель**: Поддержка нескольких типов моделей данных (например, реляционные, графовые, документные) в одной базе данных.
     - **Дизайн**: Возможность работы с несколькими типами данных в одной системе.
     - **Примеры систем**: ArangoDB, OrientDB.
     - **Примеры запросов**:
       ```aql
       FOR user IN users FILTER user.age > 30 RETURN user;  -- Пример для ArangoDB
       INSERT { name: 'John', age: 30 } INTO users;  -- Пример для OrientDB
       ```

   - **Time-Series Databases**:
     - **Цель**: Хранение и анализ данных, которые изменяются со временем, например, показателей температуры.
     - **Дизайн**: Специализированные базы данных для временных меток.
     - **Примеры систем**: InfluxDB, TimescaleDB.
     - **Примеры запросов**:
       ```sql
       SELECT mean(temperature) FROM weather WHERE time > now() - 1h;  -- Пример для InfluxDB
       INSERT INTO weather (time, temperature) VALUES (now(), 22.5);  -- Пример для TimescaleDB
       ```

   - **In-Memory Databases**:
     - **Цель**: Обработка и хранение данных в оперативной памяти для повышения скорости.
     - **Дизайн**: Все данные хранятся в памяти.
     - **Примеры систем**: Redis, Memcached.
     - **Примеры запросов**:
       ```bash
       SET key "value";  -- Пример для Redis
       GET key;  -- Пример для Memcached
       DEL key;  -- Пример для Redis
       ```

   - **Distributed Databases**:
     - **Цель**: Обработка данных, распределенных между несколькими узлами, для повышения доступности и масштабируемости.
     - **Дизайн**: Использование нескольких узлов для хранения данных.
     - **Примеры систем**: Apache Cassandra, Google Bigtable.
     - **Примеры запросов**:
       ```cql
       SELECT * FROM users WHERE city = 'San Francisco';  -- Пример для Google Bigtable
       INSERT INTO users (name, age) VALUES ('Alice', 30);  -- Пример для Apache Cassandra
       ```

### Системы обработки данных:

1. **OLTP (Online Transaction Processing)**:
   - **Цель**: Обработка транзакций в реальном времени с минимальной задержкой.
   - **Дизайн**: Реляционные базы данных с нормализованными структурами для минимизации избыточности и обеспечения целостности данных.
   - **Примеры систем**: MySQL, PostgreSQL, Oracle Database.
   - **Примеры запросов**:
     ```sql
     SELECT * FROM transactions WHERE transaction_id = 12345;  -- Пример для MySQL
     INSERT INTO transactions (user_id, amount) VALUES (1, 100);  -- Пример для PostgreSQL
     UPDATE users SET balance = balance - 100 WHERE user_id = 1;  -- Пример для Oracle Database
     DELETE FROM transactions WHERE transaction_date < '2025-01-01';  -- Пример для PostgreSQL
     ```

2. **OLAP (Online Analytical Processing)**:
   - **Цель**: Обработка больших объемов данных с целью анализа и получения бизнес-инсайтов.
   - **Дизайн**: Денормализованные структуры данных с многомерными моделями (например, звездная схема, снежинка).
   - **Примеры систем**: Microsoft SQL Server Analysis Services (SSAS), Google BigQuery.
   - **Примеры запросов**:
     ```sql
     SELECT product_category, SUM(sales) FROM sales_data GROUP BY product_category;  -- Пример для SSAS
     SELECT AVG(price) FROM products WHERE category = 'Electronics';  -- Пример для Google BigQuery
     ```

3. **HTAP (Hybrid Transactional/Analytical Processing)**:
   - **Цель**: Объединение транзакционной и аналитической обработки в одной системе для работы с данными в реальном времени и их анализа.
   - **Дизайн**: Гибридные архитектуры, которые поддерживают как транзакции, так и аналитическую обработку.
   - **Примеры систем**: SAP HANA, Google BigQuery.
   - **Примеры запросов**:
     ```sql
     SELECT product_id, SUM(sales) FROM sales_data WHERE transaction_date = '2025-01-01' GROUP BY product_id;  -- Пример для SAP HANA
     ```

4. **Real-Time Data Processing**:
   - **Цель**: Обработка данных сразу после их поступления с минимальной задержкой.
   - **Дизайн**: Платформы для потоковой обработки данных, например, Apache Kafka, Apache Flink.
   - **Примеры систем**: Apache Kafka, Apache Flink, Apache Storm.
   - **Примеры запросов**:
     ```sql
     SELECT COUNT(*) FROM transactions WHERE status = 'approved' AND timestamp > now() - INTERVAL '1 hour';  -- Пример для Apache Flink
     ```

5. **Batch Processing**:
   - **Цель**: Обработка больших объемов данных за определенный промежуток времени (например, ежедневно).
   - **Дизайн**: Системы для пакетной обработки данных, такие как Hadoop или Apache Spark.
   - **Примеры систем**: Apache Hadoop, Apache Spark.
   - **Примеры запросов**:
     ```sql
     SELECT COUNT(*) FROM users WHERE registration_date BETWEEN '2025-01-01' AND '2025-01-31';  -- Пример для Apache Spark
     ```

6. **Stream Processing**:
   - **Цель**: Обработка непрерывных потоков данных в реальном времени.
   - **Дизайн**: Используются распределенные платформы для потоковой обработки данных, например, Apache Kafka или Apache Storm.
   - **Примеры систем**: Apache Kafka, Apache Flink.
   - **Примеры запросов**:
     ```sql
     SELECT user_id, COUNT(*) FROM events WHERE event_type = 'purchase' GROUP BY user_id;  -- Пример для Apache Kafka
     ```

7. **Event-Driven Processing**:
   - **Цель**: Обработка данных на основе событий, что позволяет реагировать на изменения в системе.
   - **Дизайн**: Архитектуры, ориентированные на обработку событий, например, Event Sourcing или CQRS.
   - **Примеры систем**: Apache Kafka, AWS Lambda.
   - **Примеры запросов**:
     ```sql
     SELECT * FROM events WHERE event_type = 'purchase' AND timestamp > '2025-01-01';  -- Пример для AWS Lambda
     ```

8. **ETL (Extract, Transform, Load)**:
   - **Цель**: Извлечение данных из разных источников, преобразование их и загрузка в хранилище данных для дальнейшего анализа.
   - **Примеры систем**: Apache Nifi, Talend, Informatica.
   - **Примеры запросов**:
     ```sql
     SELECT * FROM source_table;  -- Пример для Informatica
     INSERT INTO destination_table SELECT * FROM source_table;  -- Пример для Talend
     ```

9. **Data Warehouses (Хранилища данных)**:
   - **Цель**: Хранение интегрированных и исторических данных для аналитической обработки.
   - **Примеры систем**: Amazon Redshift, Google BigQuery, Snowflake.
   - **Примеры запросов**:
     ```sql
     SELECT region, SUM(sales) FROM sales_data GROUP BY region;  -- Пример для Google BigQuery
     ```

10. **Data Lakes**:
   - **Цель**: Хранение больших объемов необработанных данных в их исходном виде (структурированные, неструктурированные, полуструктурированные).
   - **Примеры систем**: Hadoop, AWS S3, Azure Data Lake.
   - **Примеры запросов**:
     ```sql
     SELECT * FROM raw_data WHERE file_type = 'json';  -- Пример для Azure Data Lake
     ```

11. **Data Integration Systems**:
   - **Цель**: Интеграция данных из различных источников и их консолидация.
   - **Примеры систем**: Apache Camel, Microsoft SSIS.
   - **Примеры запросов**:
     ```sql
     SELECT * FROM source_system WHERE condition = 'value';  -- Пример для Microsoft SSIS
     INSERT INTO destination_system (col1, col2) SELECT col1, col2 FROM source_system WHERE condition = 'value';  -- Пример для Apache Camel
     ```

# Database vs Data Mart vs Data Warehouse vs Data Lake

Источник: [тут](https://stackoverflow.com/a/61751651), [тут](https://www.bigdataschool.ru/bigdata/lsa-data-warehouse-architecture.html)

1. **Database (База данных):**
   - **Для чего:** Это система для хранения и управления структурированными данными. Используется для оперативной работы с данными, таких как транзакции и запросы.  
   - **Основные структуры данных:**
     - Таблицы (реляционные базы данных).
     - Деревья (например, B-деревья, B+-деревья для индексации).
     - Ключ-значение (NoSQL базы, например Redis, DynamoDB).
     - Документы (NoSQL базы данных, например MongoDB).
     - Графы (графовые базы, например Neo4j).
   - **Основные алгоритмы поиска:**
     - Индексация с B-деревьями для быстрого поиска строк в таблицах.
     - Хеширование для доступа по ключу в NoSQL базах данных.
     - Поиск по графам: алгоритмы обхода (DFS, BFS) в графовых базах данных.
   - **Пример использования:**
     - Когда нужно применять: интернет-магазин хранит информацию о клиентах, заказах и продуктах для обработки транзакций и управления заказами.
     - Когда не нужно применять: хранение аналитических данных за последние 10 лет, так как база данных плохо масштабируется для больших объемов исторической информации.
2. **Data Mart (Витрина данных):**
   - **Для чего:** Локальное хранилище данных, предназначенное для анализа данных конкретного отдела или функции.  
   - **Основные структуры данных:**
     - Таблицы (реляционные структуры, оптимизированные под аналитические запросы).
     - OLAP-кубы (многомерные массивы данных для аналитики).
   - **Основные алгоритмы поиска:**
     - Многомерный поиск: алгоритмы агрегации для анализа данных в OLAP-кубах.
     - Индексация по столбцам для ускорения аналитических запросов.
   - **Пример использования:**
     - Когда нужно применять: в отделе продаж создается витрина данных, содержащая показатели продаж, скидок и прибыли для анализа эффективности маркетинговых акций.
     - Когда не нужно применять: если данные нужны нескольким отделам и интеграция лучше организована через централизованное Data Warehouse.
3. **Data Warehouse (Хранилище данных):**
   - **Для чего:** Централизованное хранилище для объединения данных из разных источников и проведения аналитики.  
   - **Основные структуры данных:**
     - Таблицы (реляционная модель, схемы "звезда" и "снежинка").
     - OLAP-кубы (для сложных аналитических вычислений).
   - **Основные алгоритмы поиска:**
     - Поиск с денормализацией для быстрого выполнения аналитических запросов в схемах "звезда" и "снежинка".
     - Алгоритмы агрегации для сложных аналитических расчетов (например, суммирование, подсчет среднего).
   - **Пример использования:**
     - Когда нужно применять: финансовая корпорация объединяет данные из разных филиалов для анализа прибыли, расходов и прогноза доходности.
     - Когда не нужно применять: для хранения сырых данных или неструктурированной информации, которая требует предварительной обработки (например, данные из сенсоров IoT).
4. **Data Lake (Озеро данных):**
   - **Для чего:** Хранилище для хранения больших объемов сырых данных, как структурированных, так и неструктурированных, для последующей обработки и анализа.  
   - **Основные структуры данных:**
     - Файлы (JSON, CSV, Avro, Parquet, ORC).
     - Объектные хранилища (например, Amazon S3, Azure Blob Storage).
     - Структуры "ключ-значение" (например, в Hadoop).
     - Неструктурированные данные (видеофайлы, изображения, логи).
   - **Основные алгоритмы поиска:**
     - Алгоритмы фильтрации данных для выбора данных из файлов (например, фильтрация JSON или CSV).
     - Поиск по объектным хранилищам: алгоритмы индексирования и метаданных (например, Hive Metastore).
     - Алгоритмы для обработки больших данных: MapReduce, Spark для параллельной обработки данных.
   - **Пример использования:**
     - Когда нужно применять: компания хранит данные от IoT-устройств, логи веб-сервера, аудио и видеофайлы для последующей аналитики и обучения машинных моделей.
     - Когда не нужно применять: для транзакционной работы с данными или создания отчетов в реальном времени, так как Data Lake не обеспечивает быстрого доступа к структурированным данным.


**Таблица плюсов и минусов архитектур:**

|                     | Database      | Data Mart (Top-down) | Data Warehouse        | Data Lake     |
| ------------------- | ------------- | -------------------- | --------------------- | ------------- |
| **Source**          | Single        | Single               | Multiple              | Multiple      |
| **Structure**       | Structured    | Structured           | Structured            | Raw           |
| **Purpose**         | Determined    | Determined           | Determined            | Undertermined |
| **Storage**         | Centralized   | Decentralized        | Centralized           | Centralized   |
| **Data Format**     | Detailed      | Summarized           | Detailed              | All           |
| **Flexibility**     | Low           | Medium               | Medium                | High          |
| **Primary Use**     | Transactional | Reporting            | Analytics & Reporting | Analytics     |
| **Cost**            | Low           | Medium               | Medium                | High          |
| **Data Volume**     | Low           | Low                  | Medium                | High          |
| **Development**     | Top-down      | Bottom-up            | Top-down              | All           |
| **Design Time**     | Medium        | Medium               | High                  | Low           |
| **Volatility**      | Medium        | Low                  | None                  | None          |
| **Data Operations** | CRUD          | CR                   | CRU                   | CR            |
| **Subject Area**    | Single        | Single               | Multiple              | Multiple      |
| **Design Schema**   | Relational    | Multi-dimensional    | Relational            | No Schema     |