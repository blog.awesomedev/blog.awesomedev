Почти все пункты из этого списка - частые вопросы на собеседовании, некоторые пункты не из вопросов к собеседованию, но встречаются на практике.

Что нужно дополнить:
```
Spring Security
Spring MVC
Описание работы транзакций и TransactionManager
```

Решения и best practice для Spring особенно в плане запросов в базу и их анализа https://github.com/andreipall/Spring-Boot-JPA

- [Основы Java EE касательно Spring](#основы-java-ee-касательно-spring)
  - [Простое подключение сервлета](#простое-подключение-сервлета)
  - [Обработка ошибок в Java EE сервлете](#обработка-ошибок-в-java-ee-сервлете)
  - [ServletConfig vs ServletContext](#servletconfig-vs-servletcontext)
  - [Структура приложения Java EE](#структура-приложения-java-ee)
  - [Как получить пути сервлета](#как-получить-пути-сервлета)
  - [HttpServletRequestWrapper и HttpServletResponseWrapper](#httpservletrequestwrapper-и-httpservletresponsewrapper)
  - [Каков жизненный цикл сервлета и когда какие методы вызываются?](#каков-жизненный-цикл-сервлета-и-когда-какие-методы-вызываются)
  - [ContextLoaderListener](#contextloaderlistener)
  - [ServletContainerInitializer и как он взаимодействует со Spring](#servletcontainerinitializer-и-как-он-взаимодействует-со-spring)
  - [Когда использовать WebApplicationInitializer или AbstractAnnotationConfigDispatcherServletInitializer](#когда-использовать-webapplicationinitializer-или-abstractannotationconfigdispatcherservletinitializer)
- [Spring Core](#spring-core)
  - [Общая инфа](#общая-инфа)
  - [Почему нужно выбрать Spring?](#почему-нужно-выбрать-spring)
  - [Шаги реализации функционала в Spring](#шаги-реализации-функционала-в-spring)
  - [Spring Life Cycle](#spring-life-cycle)
  - [Регистрация bean вручную](#регистрация-bean-вручную)
  - [SpringApplication, SpringApplicationBuilder](#springapplication-springapplicationbuilder)
  - [Выполнение операций при деплое приложения Spring (метод run())](#выполнение-операций-при-деплое-приложения-spring-метод-run)
  - [Как запустить что-то после полного запуска Spring](#как-запустить-что-то-после-полного-запуска-spring)
  - [Annotations](#annotations)
  - [`getBean()`](#getbean)
  - [What is a Spring Bean? Этапы создания Spring Bean](#what-is-a-spring-bean-этапы-создания-spring-bean)
  - [Базовые приемы работы с xml конфигурацией](#базовые-приемы-работы-с-xml-конфигурацией)
  - [Inversion of Control](#inversion-of-control)
  - [Spring учитывает только `class name` для bean и не учитывает сами `package`](#spring-учитывает-только-class-name-для-bean-и-не-учитывает-сами-package)
  - [Constructor Dependency Injection](#constructor-dependency-injection)
  - [Constructor injection vs Setter injection](#constructor-injection-vs-setter-injection)
  - [Scopes](#scopes)
  - [Spring Profiles \& Maven Profile](#spring-profiles-maven-profile)
    - [Spring Profile](#spring-profile)
    - [Profiles in Spring Boot](#profiles-in-spring-boot)
    - [Maven Profile](#maven-profile)
  - [Project Configuration with Spring](#project-configuration-with-spring)
  - [Properties with Spring and Spring Boot](#properties-with-spring-and-spring-boot)
  - [Наследование `@Transactional`](#наследование-transactional)
  - [Как работает транзакция?](#как-работает-транзакция)
  - [NoSuchBeanDefinitionException](#nosuchbeandefinitionexception)
  - [Как работают `@Transactional` и в каком случае они могут не работать](#как-работают-transactional-и-в-каком-случае-они-могут-не-работать)
  - [Spring Events](#spring-events)
    - [Common](#common)
  - [Transaction Management](#transaction-management)
    - [Common](#common-1)
    - [Programmatic Transaction Management in Spring](#programmatic-transaction-management-in-spring)
    - [TransactionalEventListener](#transactionaleventlistener)
  - [Spring Async](#spring-async)
  - [Spring Expression Language (SpEL)](#spring-expression-language-spel)
  - [Интернациализация](#интернациализация)
  - [Resource из Spring](#resource-из-spring)
  - [Поиск зависимостей в Spring, файл `spring.factories`](#поиск-зависимостей-в-spring-файл-springfactories)
  - [Utils classes in Spring](#utils-classes-in-spring)
- [Spring DI](#spring-di)
  - [CDI это Contexts and Dependency Injection](#cdi-это-contexts-and-dependency-injection)
  - [Конфликт связывания](#конфликт-связывания)
  - [FactoryBean](#factorybean)
  - [`@Autowired` in Abstract Classes](#autowired-in-abstract-classes)
  - [Autowiring of Generic Types](#autowiring-of-generic-types)
  - [Spring Component Scanning, исключает отдельный bean из AutoConfiguration](#spring-component-scanning-исключает-отдельный-bean-из-autoconfiguration)
  - [Injecting Collections](#injecting-collections)
  - [Injecting Prototype Beans into a Singleton Instance](#injecting-prototype-beans-into-a-singleton-instance)
  - [ScopedProxyMode](#scopedproxymode)
  - [custom scope, scopeName](#custom-scope-scopename)
  - [Circular Dependency, `BeanCurrentlyInCreationException`](#circular-dependency-beancurrentlyincreationexception)
  - [Внедрение сразу всех бинов определенного типа которые есть в приложении в коллекцию](#внедрение-сразу-всех-бинов-определенного-типа-которые-есть-в-приложении-в-коллекцию)
  - [Создание своего варианта `@Qualifier`](#создание-своего-варианта-qualifier)
  - [static методы помеченные `@Bean`](#static-методы-помеченные-bean)
  - [static классы помеченные `@Configuration`](#static-классы-помеченные-configuration)
  - [`@Import` классов с factory методами помеченными `@Bean` при том что эти классы не помечены `@Configuration`](#import-классов-с-factory-методами-помеченными-bean-при-том-что-эти-классы-не-помечены-configuration)
  - [`BeanDefinitionOverrideException`](#beandefinitionoverrideexception)
  - [`@RequestScope` vs `ThreadLocal`](#requestscope-vs-threadlocal)
- [Spring AOP](#spring-aop)
  - [Common](#common-2)
  - [AOP order](#aop-order)
  - [JDK Dynamic Proxy vs CGLIB](#jdk-dynamic-proxy-vs-cglib)
  - [AspectJ](#aspectj)
  - [Self Injection. Если нам все-таки надо добиться, что бы в случае Spring AOP код аспекта выполнялся при вызове proxy метода из другого proxy метода](#self-injection-если-нам-все-таки-надо-добиться-что-бы-в-случае-spring-aop-код-аспекта-выполнялся-при-вызове-proxy-метода-из-другого-proxy-метода)
  - [Пример обертки аннотации в свое AOP](#пример-обертки-аннотации-в-свое-aop)
  - [Method Info in Spring AOP](#method-info-in-spring-aop)
- [Misc](#misc)
  - [Logging](#logging)
  - [Как вызвать destroy для prototype бинов](#как-вызвать-destroy-для-prototype-бинов)
  - [Get Spring Application Context in static method](#get-spring-application-context-in-static-method)
  - [Deprecated классы](#deprecated-классы)
- [Exception handle](#exception-handle)
- [Reloading Properties bean (property который можно перезагружать динамически)](#reloading-properties-bean-property-который-можно-перезагружать-динамически)
  - [Отключение авто конфигурации Spring](#отключение-авто-конфигурации-spring)
  - [Способы обработки exceptions](#способы-обработки-exceptions)
  - [Описать `HandlerMethodArgumentResolver`](#описать-handlermethodargumentresolver)
  - [`data.sql` и `schema.sql` и `@Sql`](#datasql-и-schemasql-и-sql)
- [Spring Modules](#spring-modules)
  - [Spring MVC](#spring-mvc)
    - [Common](#common-3)
    - [Configuration](#configuration)
    - [Controller response](#controller-response)
    - [Spring MVC Interceptor](#spring-mvc-interceptor)
    - [Обычные Filter из java ee в Spring](#обычные-filter-из-java-ee-в-spring)
    - [Custom Data Binder in Spring MVC](#custom-data-binder-in-spring-mvc)
    - [ArgumentResolvers](#argumentresolvers)
  - [RestTemplate vs WebClient](#resttemplate-vs-webclient)
  - [Spring Scheduler](#spring-scheduler)
  - [Spring Retry](#spring-retry)
- [Spring Security](#spring-security)
  - [Common](#common-4)
  - [Как работает Spring Security](#как-работает-spring-security)
  - [Как работает filter chain (источник)](#как-работает-filter-chain-источник)
  - [Custom Filter in the Spring Security Filter Chain (источник)](#custom-filter-in-the-spring-security-filter-chain-источник)
  - [Custom Security Expression (источник)](#custom-security-expression-источник)
  - [OAUTH2 приминительно к Spring и JWT](#oauth2-приминительно-к-spring-и-jwt)
  - [Annotations](#annotations-1)
  - [AuthenticationManagerBuilder vs HttpSecurity vs WebSecurity](#authenticationmanagerbuilder-vs-httpsecurity-vs-websecurity)
- [Spring Boot](#spring-boot)
  - [Common](#common-5)
- [Spring JDBC](#spring-jdbc)
- [Spring Data](#spring-data)
  - [Spring Data JDBC](#spring-data-jdbc)
  - [Spring Data JPA](#spring-data-jpa)
    - [Common](#common-6)
    - [Dependencies](#dependencies)
    - [Interfaces and methods](#interfaces-and-methods)
    - [Use](#use)
    - [Custom SimpleJpaRepository](#custom-simplejparepository)
    - [Использование `@EntityGraph` в `JpaRepositor`](#использование-entitygraph-в-jparepositor)
    - [Unusual use](#unusual-use)
    - [Авто генерация SQL на чтение или фильтрацию по параметрам запроса REST](#авто-генерация-sql-на-чтение-или-фильтрацию-по-параметрам-запроса-rest)
- [Spring Cache, `@Cacheable`](#spring-cache-cacheable)
- [Spring Integration](#spring-integration)
- [Spring Batch](#spring-batch)
- [Spring Cloud](#spring-cloud)
- [Spring Boot Actuator](#spring-boot-actuator)
- [Spring Boot Maven Plugin](#spring-boot-maven-plugin)
- [Spring Statemachine](#spring-statemachine)
- [Ссылки](#ссылки)

# Основы Java EE касательно Spring
## Простое подключение сервлета
**Создание сервлета**
```java
@WebServlet(UrlPatterns = {"/index"}, , loadOnStartup = 1,
    initParams = {
        @WebInitParam(name = "host", value = "port"),
        @WebInitParam(name = "user", value = "email@mail.ru")
})
public class MyServlet extends HttpServlet {doPost(){} doGet(){}}
```
```xml
<servlet>
    <display-name>FirtsServlet</display-name>
    <servlet-name>mvc1</servlet-name>
    <servlet-class>com.dome.MyServlet</servlet-class>
    <init-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>classpath:spring/mvc1-config.xml</param-value>
    </init-param>
    <load-on-startup>1</load-on-startup>
</servlet>
<servlet-mapping>
    <servlet-name>FirstServletName</servlet-name>
    <url-pattern>/main.jsp</url-pattern>
</servlet-mapping>
```

**подключение фильтра**
```java
@WebFilter(urlPatterns = {"/*"},
    initParams = {
        @WebInitParam(name = "encoding",
            value = "UTF-8",
            description = "Encoding param")
})
public class MyFilter implements Filter {
    public void init(FilterConfig fc) throws ServletException {}
    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) {}
    public void destroy(){}
}
```
```xml
<filter>
    <filter-name>encodingfilter</filter-name>
    <filter-class>by.bsu.sample.filter.EncodingFilter</filter-class>
    <init-param>
        <param-name>encoding</param-name>
        <param-value>UTF-8</param-value>
    </init-param>
</filter>
<filter-mapping>
    <filter-name>encodingfilter</filter-name>
    <url-pattern>/myurl</url-pattern>
</filter-mapping>
```

**Просто подключение listener из Java EE (xml)**
**SessionListenerImpl**, как пример, наследует один из предопределенных классов
```java
@WebListener
public class SessionListenerImpl implements HttpSessionAttributeListener {
    // переопределенные методы
}
```
```xml
<listener>
    <listener-class>by.bsu.control.listener.SessionListenerImpl</listener-class>
    <context-param>
        <param-name>contextConfigLocation</param-name>
        <param-value>/WEB-INF/xyz.xml</param-value>
    </context-param>
</listener>
```

## Обработка ошибок в Java EE сервлете
```xml
<error-page>
    <exception-type>javax.servlet.ServletException</exception-type>
    <location>/AppExceptionHandler</location>
</error-page>
```

## ServletConfig vs ServletContext
**ServletContext** - один на всю приложенку, его параметры обьявляются в `<contex-param>`, через него можно получить доступ ко всем сервлетам приложения и программно регистрировать события, добавлять атрибуты, сервлеты etc

**ServletConfig** - у каждого сервлета свой, его параметры обьявляются в `<init-param>`

## Структура приложения Java EE
Ниже список каталогов и подкаталогов.
```
src/main + java, resources, filters, webapp
src/test + java, resources, filters, webapp
src/it - интеграционные тесты
src/assembly - Assembly descriptors
src/site - site
```

## Как получить пути сервлета
Берем context сервлета и достаем из него инфу.

`getServletContext().getRealPath(request.getServletPath())` - актуальный путь

`getServletContext().getServerInfo()` - инфа о сервере

`request.getRemoteAddr()` - ip

## HttpServletRequestWrapper и HttpServletResponseWrapper
**HttpServletRequestWrapper** и **HttpServletResponseWrapper** - можно расширить эти классы и переопределить только необходимые методы и получим свои реализации.

## Каков жизненный цикл сервлета и когда какие методы вызываются?
1. **Загрузка класса сервлета** — загрузка класса сервлета в память и вызов конструктора без параметров (при запросе или `loadOnStratup`)
2. **Инициализация класса сервлета** — инициализирует `ServletConfig` и внедряет его через `init()`. Это и есть место где сервлет класс преобразуется из обычного класса в сервлет.
3. **Обработка запросов** — Для каждого запроса клиента сервлет контейнер порождает новую нить (поток) и вызывает метод `service()` путем передачи ссылки на объект ответа и запроса.
4. **Удаление из Service** — контейнер останавливается или останавливается приложение, уничтожает классы сервлетов путем вызова `destroy()`

## ContextLoaderListener
**ContextLoaderListener** - слушатель из `web.xml` (или Java Config альтернативы), который запускает или останавливает **WebApplicationContext** если Spring используется в качестве веб приложения.

**ApplicationContext** запускается вручную или через **ContextLoaderListener** в JavaEE (MVC), с передачей ему пути к конфигурации (xml или `@ComponentScan`)

**ContextLoaderListener из Spring делает:**
1. Привязывает **lifecycle ApplicationContext** к **lifecycle ServletContext**.
2. Автоматически создает (запускает) **ApplicationContext**

Создаенный через **ContextLoaderListener** объект **WebApplicationContext**
дает доступ к **ServletContext** через **ServletContextAware**
и его метод `getServletContext()`

**Пример web.xml переопределения пути к /WEB-INF/applicationContext.xml:** (в примере параметр `contextConfigLocation` используется чтобы переопределить путь к конфигам по умолчанию):
```xml
<listener>
    <listener-class>org.springframework.web.context.ContextLoaderListener</listener-class>
</listener>

<context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>
        /WEB-INF/application-dao.xml
        /WEB-INF/application-web.xml
    </param-value>
</context-param>
```

## ServletContainerInitializer и как он взаимодействует со Spring
* **ServletContainerInitializer** (SCI) из Java EE
* **AbstractAnnotationConfigDispatcherServletInitializer** (из Spring, наследует SCI)
* **WebApplicationInitializer** (из Spring, наследует SCI)

**ServletContainerInitializer** (SCI) используется для динамической загрузки
компонентов (альтернатива для web.xml).
Базируется на SPI и имеет метод `void onStartup(Set<Class<?>> c, ServletContext ctx)`.
<br>
**Чтобы SCI авто подхватилось** нужно положить его наследника в txt файл:
`META-INF/services/javax.servlet.ServletContainerInitializer`
и в этом классе прописать пути к классам SCI (пакет+имя).

Начиная с Servlet 3.0 конфиги `web.xml` могут быть в `META-INF/web-fragment.xml` при использовании web fragments (для работы без `web.xml`)

**WebApplicationInitializer** можно рассматривать как аналог web.xml через него можно регистрировать сервлеты.
Его методы: `onStartup(ServletContext servletContext)`

**@HandlesTypes** добавляет .class первым параметром onStartup()
через Reflection API можно создать `MyClass.class.newInstance()`
внутри `onStartup()`, а потом добавить туда созданный `new MyClass();`

**Пример ServletContainerInitializer (Java EE):**
```java
@HandlesTypes({Page.class}) // добавляет этот .class первым параметром onStartup()
public class AppInitializer implements ServletContainerInitializer {
    @Override
    public void onStartup(Set<Class<?>> pageClasses, ServletContext ctx){
        ServletRegistration.Dynamic registration =
                ctx.addServlet("appController", AppController.class);
        pages.forEach(p -> registration.addMapping(p.getPath()));
    }
}
```

**Пример WebApplicationInitializer (Spring):**
```java
public class SpringWebAppInitializer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext appContext = new AnnotationConfigWebApplicationContext();
        appContext.register(ApplicationContextConfig.class);
 
        ServletRegistration.Dynamic dispatcher = servletContext.addServlet("SpringDispatcher",
                new DispatcherServlet(appContext));
        dispatcher.setLoadOnStartup(1);
        dispatcher.addMapping("/");
    }
}
```

**Источники:**
* http://samolisov.blogspot.com/2016/01/spring-framework.html
* https://stackoverflow.com/questions/26676782/when-use-abstractannotationconfigdispatcherservletinitializer-and-webapplication/26676881#26676881
https://dzone.com/articles/understanding-spring-web

## Когда использовать WebApplicationInitializer или AbstractAnnotationConfigDispatcherServletInitializer
**Когда использовать WebApplicationInitializer (Spring 3.1+) или AbstractAnnotationConfigDispatcherServletInitializer (Spring 3.2+):**
1. SpringServletContainerInitializer находит классы которые implements WebApplicationInitializer
2. WebApplicationInitializer - интерфес, 
3. AbstractAnnotationConfigDispatcherServletInitializer implements WebApplicationInitializer
    - это рекомендуемый путь, через него можно стартовать servlet application context и/или root application context
    - ПРЕИМУЩЕСТВО: не нужно вручную настраивать DispatcherServlet и ContextLoaderListener
    
**WebMvcConfigurerAdapter** (и прочие `...Adapter`) - для конфигурации приложения  
**vs AbstractAnnotationConfigDispatcherServletInitializer** - для bootstraping (загрузки) приложения

```java
public class MyWebApplicationInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    protected Class<?>[] getRootConfigClasses() {
        return new Class[] {RootConfig.class};
    }
    protected Class<?>[] getServletConfigClasses()  {
        return new Class[] {WebConfiguration.class};
    }
    protected String[] getServletMappings() {
        return new String[] {"/"};
    }
}
```

# Spring Core
## Общая инфа
* **Цель** - ослабление связей через DI и AOP и уменьшение количества кода. В качестве компонентов испольует POJO или JavaBean.
* **DI** - взаимодействие с объектом через интерфейc, не зная о реализации. IoC один из видом DI, реализован через паттерн ServiceLocator.
* Легко тестировать с DI, подставляя фективную реализацию. 

**Типы связывания:**
* **DI** - лучше
* **DL** (Dependency Lookup) - нужно использовать, когда невозможно DI, **e.g.** при инициализации через `ApplicationContext` автономного приложения (в случае MVC он сам все запускает)

**Типы Factory:**
1. **BeanFactory** - ядро DI (в т.ч. зависимости и life cycle). Позволяет РАЗДЕЛИТЬ конфиги и код.
2. **ApplicationContext** - расширение BeanFactory, кроме того содержит сервисы JMS, AOP, i18n, Event listeners, transactions, properties, MessageSource (для i18n) etc; регистрацию в BeanPostProcessor, BeanFactoryPostProcessor

Spring приложение можно создавать на основе **ИНТЕРФЕСОВ** и **БИНОВ** (в Spring Bean, не обязательно следовать JavaBean, особенно если DI через конструктор, т.к. не надо знать set/get методы)

**Spring Context** - конфигурационный файл о среде для Spring.
<br>
**Spring Context** - это **IoC** контейнер, создает, конфигурит бины читая мета инфу (конфиги).

* **IoC** - это (3и вида внедрения зависимости: конструктор, сетер, интерфес (CDL)):
    1. **Dependency lnjection** - внедрение зависимостей, напр xml или аннотации; внедряется IoC контейнером автоматически
        1. **Constructor Dependency Injection** - Внедрение зависимостей через конструктор
        2. **Setter Dependency Injection** - Внедрение зависимостей через метод установки
    2. **Dependency Lookup** - поиск зависимостей, когда зависимость вручную достается из контейнера: SpringContext.lookup('Blabla')
        1. **Dependency Pull** - Извлечение зависимостей вручную (чаще всего, прим. выше для DL)
        2. **Contextualized Dependency Lookup** (CDL) - реализуем метод из интерфейса контейнера в классе в котором нужна зависимость, этот метод сам вызовется и сделает set() зависимости: [пример](https://stackoverflow.com/a/42000709)

**IoC** - дает службы (сервисы) для взаимодействия компонентов со своими зависимостями.

**BeanFactory нужен если:** использовать приложение совместно с либами JDK 1.4 или не поддерживают JSR-250.

**Если вместе с BeanFactory нужно использовать AOP и транзакций** то при использовании **BeanFactory** необходимо добавить вручную регистрацию **BeanPostProcessor** и **BeanFactoryPostProcessor**. Т.к. они только в **ApplicationContext**.

**Типы ApplicationContext для разных типов конфигов:**
* **ClassPathXmlApplicationContext** - конфиги из classpach
* **FileSystemXmlApplicationContext** - файл конфигов
* **XmlWebApplicationContext** - по умолчанию по пути `/WEB-INF/applicationContext.xml`, или указать вручную (можно сразу несколько xml) добавив в web.xml слушатель ContextLoaderListener и параметр `<context-param>` с ключем `contextConfigLocation` и путями к xml

**Пример:**
```java
ConfigurableBeanFactory factory = new XmlBeanFactory(...);
// теперь зарегистрируем необходимый BeanPostProcessor экземпляр
MyBeanPostProcessor postProcessor = new MyBeanPostProcessor();
factory.addBeanPostProcessor(postProcessor);
// запускаем, используя factory
```

## Почему нужно выбрать Spring?
**Spring** - начинался как Inversion of Control и сейчас используется в большей степени для этого.

**Плюсы:**
* Фреймворк помогает избавиться от повторяющегося кода
* Использует проверенные годами паттерны
* экосистема с готовыми компонентами

**Минусы:**
* Заставляет писать код в определенной манере
* Ограничивает версии языка и библиотек
* Дополнительное потребление ресурсов

**Модули:**
* Core - DI, Internationalisation, Validation, AOP
* Data Access - поддержка доступа к данным через JTA (Java Transaction API), JPA (Java Persistence API), and JDBC (Java Database Connectivity)
* Web - поддержка Servlet API (Spring MVC) и Reactive API (Spring WebFlux) и дополнительно поддерживаются WebSockets, STOMP, and WebClient
* Integration - поддержка интеграции с Enterprise Java через JMS (Java Message Service), JMX (Java Management Extension), and RMI (Remote Method Invocation)
* Testing - unit and integration testing через Mock Objects, Test Fixtures, Context Management, and Caching

**Spring Projects:**
* **Boot** - template код для быстрого разворачивания приложения в том числе с встроенными контейнерами приложений
* **Cloud** - легкая разработка с использованием паттернов распределенных систем (distributed system patterns) service discovery, circuit breaker, and API gateway
* **Security** - authentication and authorization, защита от session fixation, click-jacking, and cross-site request forgery
* **Mobile** - обнаружение и поддержка мобильных устройств, в том числе особая работа с view (видимо из MVC)
* **Batch**

## Шаги реализации функционала в Spring
1. Создать интерфес с ф-цией
2. Создать его реализацию
3. Создать конфиги Spring (e.g. xml или аннотации)
4. Написать приложение использующее ф-цию интерфеса

## Spring Life Cycle
Источники: [тут](https://habr.com/ru/post/222579/), [тут](https://habr.com/ru/post/334448/), [тут](https://www.youtube.com/watch?v=BmBr5diz8WA)

**Note:** у бинов со scope `prototype` НЕ вызывается метод с анотацией `@PreDestroy`. НО вызывает c `@PostConstruct`.

**Как происходит запуск Spring и создание бинов в целом по шагам:**
1. Чтение конфигов классами `AnnotationConfigApplicationContext`, `ClassPathXmlApplicationContext` и прочими. Создаются `BeanDefinition` на основе конфигурации  (id, name, class, alias, init-method, destroy-method и др.), которая читается через обьекты реализации интерйеса `BeanDefinitionReader` (`XmlBeanDefinitionReader`, `AnnotatedBeanDefinitionReader`). Класс `ClassPathBeanDefinitionScanner` сканирует все `@Component` и находит `BeanDefinition`. Хранятся `BeanDefinition` в `Map<String, BeanDefinition> beanDefinitionMap = new ConcurrentHashMap<String, BeanDefinition>(64);`.
   * **Note.** При работе `AnnotatedBeanDefinitionReader` **1ый этап** это регистрация всех `@Configuration` для дальнейшего парсирования. **2ой этап** это регистрация специального `BeanFactoryPostProcessor`, а именно `BeanDefinitionRegistryPostProcessor`, который при помощи класса `ConfigurationClassParser` парсирует JavaConfig и создает `BeanDefinition`.
   * **Note.** Порядок чтения конфигов: **1)** xml, **2)** аннотации с указанием пакета для сканирования, **3)** аннотации с указанием класса (Java Config), **4)** Groovy конфиги. Spring также умеет читать Groovy конфиги через `GenericGroovyApplicationContext`
2. Настройка `BeanDefinition` через встроенные `BeanFactoryPostProcessor`
3. Создание кастомных `FactoryBean`, которые на следующем этапе будут использованы для создания бинов в случае если эти `FactoryBean` указаны для использования при создании каких-то бинов.
4. Создание бинов на основе `BeanDefinition`, бины создаются `BeanFactory` объектом, если для бина указан `FactoryBean`, то создание делегируется ему.
5. Настройка бинов через `BeanPostProcessor`, вызов экзепляров `BeanPostProcessor` последовательный. (e.g. тут происходит связывание). Например можно создать свою аннотацию, а потом обработчик этой своей аннотации. Класс реализующий `BeanPostProcessor` обязательно **должен быть бином**, а не просто обьектом.

**Note.** При `SCOPE_PROTOTYPE` вызов `BeanPostProcessor` будет происходить каждый раз при создании бина.

**Bean Life Cycle:** (hooks, Жизненный цикл)
- Spring IoC **start**
  1. Instantiation - создание бинов
     1. `constructor` + constructor injection
  2. Property Injection - связывание
     1. `setter` + setter injection
  3. `BeanFactoryPostProcessor.postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)` - вызывается до создания даже eager бинов, можно поменять `BeanDefinition`, например для inject встроенным классом строк в `@Value` поля
     * **Note.** В ConfigurableListableBeanFactory есть метод **getBeanDefinitionNames** через него можно получить BeanDefinitions и в цикле получить к ним доступ
  4. Вызов `...Aware` интерфейсов
     1. `setBeanName()` из BeanNameAware
     2. `setBeanClassLoader()` из BeanClassLoaderAware
     3. `setBeanFactory()` из BeanFactoryAware
     4. `setApplicationContext()` из ApplicationContextAware
     5. ...
  5. `BeanPostProcessor.postProcessBeforeInitialization(Object bean, String beanName)` - **BeanPostProcessor** вклинивается в процесс инициализации перед поподанием бина в контейнер, например для связывания бинов. Метод **должен** сделать `return bean;` иначе бин не будет создан.
  6. `@PostConstruct` - в отличии от **constructor** вызван когда зависимости заинжекчены. **Note.** видимо proxy на этом этапе еще не созданы, т.к. они создаются в `postProcessAfterInitialization` и следовательно вызвать их нельзя (e.g. на этом этапе нельзя вызвать метод из реализованного Repository в Spring Data JPA т.к. для него не отработает AOP).
     1. **Note!** `field @Autoried` **beans** видны только тут, в **constructor** они не видны (**beans** которые **inject** через **constructor** видны в **constructor**)
  7. `afterPropertiesSet()` из `InitializingBean` интерфейса
  8. `init-method` из xml конфигов и `@Bean(initMethod = "customInitMethod")`
  9.  `BeanPostProcessor.postProcessAfterInitialization(Object bean, String beanName)` - если нужно сделать proxy над обьектом, то его нужно делать **после** метода `init`, т.е. в этом методе, а не в `postProcessBeforeInitialization`. Метод **должен** сделать `return bean;` иначе бин не будет создан.
- Spring IoC **shutdown**
  1. `@PreDestroy` - не вызывается для **prototype**
  2. `destroy()` из `DisposableBean` интерфейса
  3. `destroy-method` из xml конфигов и `@Bean(destroyMethod = "customDestroyMethod")`
  4. `finalize()`

**Note.** Можно обьявить методы **init** и **destroy** глобально для всех бинов внутри:
```xml
<beans default-init-method="customInit" default-destroy-method="customDestroy">  
    <bean id="demoBean" class="com.howtodoinjava.task.DemoBean"></bean>
</beans>
```

**BeanPostProcessor** interface часть жизненного цикла, но используется и чтобы расширить функциональность самих модулей. Нужно наследовать и переопределить метод. напр.:
* `CommonAnnotationBeanPostProcessor`
* `RequiredAnnotationBeanPostProcessor`
* `AutowiredAnnotationBeanPostProcessor`

**BeanPostProcessor** - то место где бины связываются (напр `@Autowired` через `AutowiredAnnotationBeanPostProcessor`)

**BeanPostProcessor vs BeanFactoryPostProcessor**
1. **BeanFactoryPostProcessor** - вызывает переопределенный метод `postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)`, когда все определения бина загружены, но сам он не создан. Можно перезаписывать properties бина даже если бин eager-initializing. В этом случае есть доступ ко все бинам из контекста. Предустановленные BeanFactoryPostProcessor **меняют** само BeanDefinition, например устанавливает значение в `@Value`.
2. **BeanPostProcessor** - вызывается когда все определения бина уже загружены и сам бин только что создан Spring IoC -ом. (он наследуется самим классом бина???). Предустановленные BeanPostProcessor **меняют** уже созданные бины.

Пример
```java
public class CustomBeanFactory implements BeanFactoryPostProcessor {
    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        for (String beanName : beanFactory.getBeanDefinitionNames()) {
            BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
        }
    }
}
```

**Note.** **@PostConstruct** - в отличии от **constructor** вызвается когда зависимости уже **injected**.

**Note.** **afterPropertiesSet** и **destroy** более завязаны на Spring в отличии от...

**Реализация своего life cycle:**
 1. Наследовать Lifecycle и/или Phased
 2. Переопределить методы

```java
public interface Lifecycle {
    void start();
    void stop();
    boolean isRunning();
}

public interface LifecycleProcessor extends Lifecycle {
    void onRefresh();
    void onClose();
}

public interface SmartLifecycle extends Lifecycle, Phased {
    boolean isAutoStartup();
    void stop(Runnable callback);
}

// использование
@Component
public class DemoSmartLifecycle implements SmartLifecycle {
    private boolean isRunning;

    @Override
    public void start() {
        isRunning = true;
        log.info("[DemoSmartLifecycle]: Start");
    }

    @Override
    public void stop() {
        isRunning = false;
        log.info("[DemoSmartLifecycle]: Stop");
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }
}
```

**Note.** Аннотации `@Autowired`, `@Inject`, `@Resource` и `@Value` обрабатываются Spring реализацией `BeanPostProcessor`, поэтому вы не можете их применять в своих собственных `BeanPostProcessor` и `BeanFactoryPostProcessor`, а только лишь явной инициализацией через XML или `@Bean` метод.

**SmartInitializingSingleton** - запускается в конце **singleton pre-instantiation phase**
```java
@Component
public class DemoSmartInitializingSingleton implements SmartInitializingSingleton {
    @Override
    public void afterSingletonsInstantiated() {
        log.info("[SmartInitializingSingleton] afterSingletonsInstantiated");
    }
}
```

## Регистрация bean вручную

```java
 AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
 ctx.register(AppConfig.class);
 ctx.refresh();
 MyBean myBean = ctx.getBean(MyBean.class);
```

## SpringApplication, SpringApplicationBuilder
Source: [1](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/builder/SpringApplicationBuilder.html)
```java
// остановить приложение
SpringApplication.run(RsscollectorApplication.class, args).close();
System.exit(SpringApplication.exit(context));

// конфигурировать
new SpringApplicationBuilder(Application.class)
                .bannerMode(Banner.Mode.OFF)
                .logStartupInfo(false)
                .build()
                .run(args);
```

## Выполнение операций при деплое приложения Spring (метод run())
Источник: [1](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#boot-features-command-line-runner)

**Note.** Отметить его в Spring Life Cycle

Выполнение операций при деплое приложения Spring (метод run()):  
**CommandLineRunner** и **ApplicationRunner** почти тоже самое, только аргументы разные. Запускаются после создания **ApplicationContext** создан, но **до** запуска spring boot application.
* ApplicationRunner
* CommandLineRunner
```java
// если в CommandLineRunner будет Exception, то запуск приложения прервется
@Component
public class AppRunner implements CommandLineRunner {
    @Override // args - параметры запуска
    public void run(String... args) throws Exception {}
}

@Component
public class AppStartupRunner implements ApplicationRunner {
    @Override // В ApplicationArguments есть getOptionNames(), getOptionValues() and getSourceArgs()
    public void run(ApplicationArguments args) throws Exception { }
}
```

## Как запустить что-то после полного запуска Spring
Использовать событие **ApplicationReadyEvent** или **ContextRefreshedEvent** если нужно запускать что-то и когда новые бины добавляются в context (каждый раз)
```java
@EventListener(value = ApplicationReadyEvent.class)
public void initialize(ApplicationReadyEvent event) {
    ApplicationContext context = event.getApplicationContext(); // do smth
}

// при вызове event.getApplicationContext().refresh()
@EventListener
public void handleContextRefreshEvent(ContextRefreshedEvent evt) {
    // do smth
}
```

## Annotations
**Stereotyping Annotations это**: `@Component`, `@Controller`, `@Repository`, `@Service` (просто называются так)

* **Аннотации в пакетах `org.springframework.beans.factory.annotation` и `org.springframework.context.annotation`**
  * **`@SpringBootApplication` включает:** `@Configuration`, `@EnableAutoConfiguration`, `@EnableWebMvc`, `@ComponentScan`
  * **`@EnableAutoConfiguration`** - пытается угадать и создать конфиги: `DataSource`, `EntityManagerFactory`, `TransactionManager` etc
  * **`@ComponentScan`** - импортит все `@Configuration` классы своего пакета + указанные внутри нее, минус то что в `(exclude = Blabla.class)`
* **Стандартны в которых есть аннотации:**
  * JSR-250 - для JSE и JEE, такие как: : `@Resource, @PreDestroy, @PostConstruct, @RolesAllowed, @PermitAll, @DenyAll` etc
  * JSR-299 - из Contexts and Dependency lnjection for the Java ЕЕ Platform 
  * JSR-330 - из пакета `javax.inject.*`
* **Spring Annotation vs JSR-330**
  * **packages** - пакеты с аннотациями
    * **Spring Annotation**
      * `org.springframework.context.annotation.*`
      * `org.springframework.beans.factory.annotation.*`
    * **JSR-330**
      * `javax.inject.*`
  * `@Autowired` vs `@Inject` - `@Inject` не имеет параметра **required**
  * `@Component` vs `@Named` - одинаковые
  * `@Scope("singleton")` vs `@Singleton` - одинаковые, но в JSR-330 **Scope** по умолчанию **prototype**
  * `@Qualifier` vs `@Named` - одинаковые
  * `@Value`, `@Required`, `@Lazy` - в JSR-330 нет аналога

**Core annotations:**
* **DI-Related Annotations**
  * `@Configuration` - отмечает класс с `@Bean` для сканирования
    * нельзя делать их `@Bean`, **final** class & methods (за исключением **proxyBeanMethods=false**, который отключает runtime-generated subclass), **local class**
    * `@Configuration(proxyBeanMethods = false)` - `true` by default, включает принудительное создание proxy через CGLIB на уровне method interceptor (e.g. для `inter-bean references via direct method` - связывания bean через вызов их factory method) 
    * nested `@Configuration` classes должны быть **static** (т.к. должны создастся раньше своего outer class)
    * `@Enable...` аннотации (`@EnableAsync, @EnableScheduling, @EnableTransactionManagement, @EnableAspectJAutoProxy, and @EnableWebMvc` etc) могут включить их `@Configuration`, если проставлены над `@Configuration` (see `@Import`)
    * В обычном случае Spring сам определяет порядок зависимостей, в необычных можно использовать `@DependsOn` и `@AutoConfigureBefore/@AutoConfigureAfter/AutoConfigureOrder`
    * `@PostConstruct` работает в `@Configuration`
    * внутри `@Configuration` можно делать `@Autowired`
  * `@Component("fooFormatter")` - помечает класс как бин инстанс которого нужно создать, `@Service` и `@Repository` наследники `@Component` и Spring не смотрит на них самих, а только на `@Component`, когда регистрирует в `ApplicationContext`
  * `@ComponentScan("com.baeldung.autowire.sample")`
    * **в xml** можно использовать `<context:annotation-config/>` вместо этого
    * **в xml** `<context:component-scan base-package="com.baeldung" />`
    * `@ComponentScan(basePackages = "com.baeldung.annotations")` - сканировать пакет
    * `@ComponentScan(basePackageClasses = VehicleFactoryConfig.class)` - сканировать класс
    * `@ComponentScan` - сканировать текущий пакет и все под пакеты
    * можно ставить **несколько** таких аннотаций над одним классом
    * `@ComponentScans({@ComponentScan(basePackages = "com.baeldung.annotations"), @ComponentScan(basePackageClasses = VehicleFactoryConfig.class)})` - или массив аннотаций
  * `@Service` - ничего не делает, просто отмечает бин как бизнес логику
  * `@Repository` - ловит persistence exceptions и делает rethrow их как Spring unchecked exception, для этого используется **PersistenceExceptionTranslationPostProcessor** (т.е. добавляется AOP обработчика исключений к бинам с `@Repository`). SQL Exception транслируются в наследников класса `DataAccessExeption`
  * `@Resource` vs `@Inject` vs `@Autowired` - `@Resource` (JSR-250) и `@Inject` (JSR-330) из Java EE: `javax.annotation.Resource` vs `javax.inject.Inject`, `@Autowired` из Spring `org.springframework.beans.factory.annotation`.
    * `@Resource` - связывает в порядке: Name (имя в `@Bean(name="bla")`), Type, Qualifier (имя в `@Qualifier("bla")`, т.е. использовать нужно `@Resource` + `@Qualifier("bla")` над местом инжекта)
    * `@Inject` порядок связывания: Type, Qualifier, Name
    * `@Autowired` порядок связывания: Type, Qualifier, Name
    * Что и когда использовать:
      * По типу. Если есть разные singleton классы одинаковые на все приложение, то `@Inject` или `@Autowired`
      * По имени. Если приложение Fine-Grained (разделено на мелкие куски), имеет сложное поведение, то `@Resource` (т.е. если есть бины одинакового типа, но с разными именами и реализациями, их много и нужно в каждое конкретное место вставлять нужную реализацию)
  * `@Qualifier("main")` - связывание по **name** или **id** бина (видимо id это имя генерируемое автоматически, а name заданное), используется как пара к `@Autowired`, если типы совпадают чтобы не получить `NoUniqueBeanDefinitionException`, если стоит над классом, то работает как назначение имени, аналогично: `@Component("fooFormatter")` тоже что и `@Qualifier("fooFormatter")` над **классом**. Можно применять в **constructor параметре**, **setter параметре**, **над setter**, **над field**. Можно создать **свой вариант аннотации `@Qualifier`** проставив `@Qualifier` над созданной аннотацией.
    * **Для чего `@Qualifier`**: чтобы убрать конфликт (неодназначность), если 2+ beans имеют один тип, включает связывание **by name**.
    * `@Autowired Biker(@Qualifier("bike") Vehicle vehicle) {}`
    * `@Autowired void setVehicle(@Qualifier("bike") Vehicle vehicle) {}` - параметр set
    * `@Autowired @Qualifier("bike") void setVehicle(Vehicle vehicle) {}` - над методом set
    * `@Autowired @Qualifier("bike") Vehicle vehicle;`
  * `@Autowired` - Отмечает зависимость которую Spring будет resolve. Связывание **по типу по умолчанию**. `NoUniqueBeanDefinitionException` будет, если есть больше 1го кандидата на связывание и нет `@Qualifier` или `@Primary`. Если поле класса отмеченное `@Autowired` имеет имя такое как у связываемого бины, но в camelCase, то конфликта тоже не будет и связывание произойдет **по имени** (это fallback поведение Spring). Применяется к **constructor**, **setter**, or **field**. При использовании **constructor injection** (над конструктором) все аргументы конструктора обязательны.
    * **@Autowired над constructors не обязательно** - начиная с Spring 4.3, но обязательно если конструкторов **больше 1го**, в классах `@Configuration` в этом случае constructor тоже может быть пропущен (будет использован самый eager конструктор).
    * `@Autowired(required = true)` - атрибут **required = true** стоит **by default**, если зависимости нет при запуске Spring будет exception, если поставить в **false**, то exception не будет и если нет зависимости будет **null**
      * ```java
        // inject empty list if there are no MyBeans
        @Autowired(required = false) List<MyBean> myBeans = Collections.emptyList();
        ```
    * Можно использовать `@Autowired` над **несколькими constructors**, если на всех конструкторах **кроме 1го** стоит **required = false**, Spring использует самый жадный из конструкторов (с большим количеством параметров) чьи параметры (все) могут быть удовлетворены. Spring вызовет только 1ин конструктор для создания обьекта.
    * `@Autowired Car(Engine engine) {}` - **constructor**
    * `@Autowired void setEngine(Engine engine) {}` - **setter**
    * `@Autowired Engine engine;` - **field**
    * `@Autowired @Named("fineDay") Fine fine;` - **field** `by Name`
    * Неявный `@Autowired` в метод `@Bean` (direct `@Bean` method reference) - гарантирует строгую типизацию и существование метода
        ```java
        @Bean MyBean1 myBean1() { return new MyBean1(); }
        @Bean MyBean2 myBean2() { return new MyBean1(myBean2()); }
        ```
  * `@Inject` - как `@Autowired`
    * `@Inject @Named("fineDay") Fine fine;`
  * `@Named("myname")` - может работать и как `@Component` если стоит над классом и как `@Qualifier` если стоит вместе с `@Inject`, `@Autowire` или `@Resource`
  * `@Named` vs `@Qualifier` vs `@Resource`
  * `@Primary` - отмечает бин который будет выбран для авто связывания по умолчанию в случае конфликта. Если есть и `@Qualifier`, и `@Primary`, то **у `@Qualifier` приоритет**. Можно ставить рядом с `@Bean` или `@Component` (для класса)
  * `@Bean` - отмечает factory method который создает bean. Этот метод вызывается когда **bean зависимость запрошена** другим бином, имя бина такое как имя у **factory method** или указанное как `@Bean("engine")`. Все методы отмеченные `@Bean` должны быть в `@Configuration` классе.
    * **Note!** В некоторых или все **Config** классах (e.g. `@FiegnClient(configurations = {MyConfig})`? При использовании `@Autowired` с **List** каждый `@Bean` должен иметь свое **name** (т.е. **name** метода). Иначе может **beans** с совпадающими именами могут **перекрыть** друг друга тихо **без exception** (метод создания **bean может** даже не запуститься).
        ```java
        @Autowired List<MyBean> myBeans;
        @Bean MyBean MyBean1() { return new MyBean(); }
        @Bean MyBean MyBean2() { return new MyBean(); } // При MyBean1 совпаднии name ошибка!
        ```
    * `@Bean({"name1", "name2"}) MyBean myBean();` - при объявлении имен в аннотации имя метод `myBean` перестает работать быть именем бина
    * `@Bean(name = "name1")`
    * `@Bean(value = "name1")`
    * Все методы отмеченные `@Bean` должны быть в `@Configuration` классе для **нормальной** работы.
    * Если `@Bean` в `@Component` или в `POJO`, то работать будет в режиме **Bean Lite Mode** и **inter-bean references** работать не будет, если один **bean factory method** вызовет другой то CGLIB не сработает и это сработает как обычный **method invocation** без proxy. [источник](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/annotation/Bean.html) (не ясно можно ли делать inject таких бинов друг в друга иди нельзя только **bean factory method**)
    * **static `@Bean`** используются когда бины обрабатываются в `BeanFactoryPostProcessor` (BFPP) т.к. bean должен быть создан рано в lifecycle и они взаимодействуют с `@Autowired`, `@Value`, and `@PostConstruct` из `@Configuration`. **static `@Bean`** создается до своего `@Configuration` чтобы избежать **lifecycle conflicts**. Для **static `@Bean`** не будет работать Scope и AOP (`@Scope ` и `@Autowired` с другими beans), но это сработает т.к. `BeanFactoryPostProcessor` beans обычно не связаны с другими beans. **Note.** При попытке использовать **non-static `@Bean`** с типом который обрабатывается в `BeanFactoryPostProcessor` будет сообщение в логах с уровнем **INFO**. [источник](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/context/annotation/Bean.html) 
        ```java
        @Bean static PropertySourcesPlaceholderConfigurer pspc() { // обрабатывается в BeanFactoryPostProcessor
            // instantiate, configure and return pspc ...
        }
        ```
  * `@Required void setColor(String color) {}` - отмечает зависимость (e.g. проставляется над set) которая описана в xml, без нее будет BeanInitializationException.
    * `@Required @Autowired void setColor(String color) {}` - Применяется к `setter injection` чтобы сделать inject не опциональным, а обязательным (подобно обязательным параметрам при `constructor injection`).
  * `@Value` - делает inject файла или переменной property в поле бина. Применяется на **constructor**, **setter**, и **field**. Внутри можно использовать SpEL (выражения начинающиеся не с `$`, а с `#`). **для установки значения выражения в переменную (для работы с properties нужно указать @PropertySource):**
    * `Engine(@Value("8") int cylinderCount) {}`
    * `@Autowired void setCylinderCount(@Value("8") int cylinderCount) {}`
    * `@Value("8") void setCylinderCount(int cylinderCount) {}`
    * `@Value("8") int cylinderCount;`
    * `@Value("${engine.fuelType}") String fuelType;` - для файла `engine.fuelType=petrol`
    1. Выражение
        ```java
        @Value("${systemValue}")
        private String systemValue;
        ```
    2. Строка
        ```java
        @Value("string value")
        private String stringValue;
        ```
    3. Значение по умолчанию в случае ошибки:
        ```java
        @Value("${unknown.param:some default}")
        private String someDefault;

        // пустая строка
        @Value("${some.key:}")
        private String stringWithBlankDefaultValue;

        @Value("${some.key:true}")
        private boolean booleanWithDefaultValue;

        @Value("${some.key:42}")
        private int intWithDefaultValue;

        @Value("${some.key:one,two,three}")
        private String[] stringArrayWithDefaults;

        @Value("${some.key:1,2,3}")
        private int[] intArrayWithDefaults;

        // SpEL
        @Value("#{systemProperties['some.key'] ?: 'my default system property value'}")
        private String spelWithDefaultValue;
        ```
    4. Если property с одинаковым ключем обьявлены в **system property** и в **property file**, то приоритет у **system property**
        ```java
        // значение priority из system property
        @Value("${priority}")
        private String prioritySystemProperty;
        ```
    5. Массив
        ```java
        // listOfValues=A,B,C
        @Value("${listOfValues}")
        private String[] valuesArray;
        ```
    6. **SpEL**
       1. Из системной переменной:
           ```java
           @Value("#{systemProperties['priority']}")
           private String spelValue;
           ```
       2. Если в случае ниже **systemProperties** переменной не существует, то value будет **null**
           ```java
           @Value("#{systemProperties['unknown'] ?: 'some default'}")
           private String spelSomeDefault;
           ```
       3. Доступ к полю другого бина
           ```java
           @Value("#{someBean.someValue}")
           private Integer someBeanValue;
           ```
       4.  Разбиваем значения на **List** и устанавливаем
           ```java
           @Value("#{'${listOfValues}'.split(',')}")
           private List<String> valuesList;
           ```
        1. Разбиваем значения на **List** и устанавливаем or default value if empty
            ```java
            @Value("${com.my-param:}#{T(java.util.Collections).emptyList()}") // It's right! No error here!
            List<String> includes;
            com.my-param: "a,b,c"
            ```
    7. **Maps**
       1. Для Map, значения должны быть в одинарных кавычках:
           ```java
           // valuesMap={key1: '1', key2: '2', key3: '3'}
           @Value("#{${valuesMap}}")
           private Map<String, Integer> valuesMap;
           ```
        1. Взять value из map по ключу (key1 имя ключа), если такого ключа нет, то будет **exception**
            ```java
            @Value("#{${valuesMap}.key1}")
            private Integer valuesMapKey1;
            ```
        2. Значение по ключу, **без выброса exception** если ключа нету, тогда значение будет **null**
            ```java
            @Value("#{${valuesMap}['unknownKey']}")
            private Integer unknownMapKey;
            ```
        3. Значения по умолчанию для определенных ключей
            ```java
            @Value("#{${unknownMap : {key1: '1', key2: '2'}}}")
            private Map<String, Integer> unknownMap;
            
            @Value("#{${valuesMap}['unknownKey'] ?: 5}")
            private Integer unknownMapKeyWithDefaultValue;
            ```
        4. Фильтруем (filtered) значения перед inject
            ```java
            // только value > 1
            @Value("#{${valuesMap}.?[value>'1']}")
            private Map<String, Integer> valuesMapFiltered;
            ```
        5. inject всех **systemProperties**
            ```java
            @Value("#{systemProperties}")
            private Map<String, String> systemPropertiesMap;
            ```
  * `@DependsOn` - в ней можно указать имя зависимости бина, чтобы он загрузился до загрузки зависимого бина
    * `@DependsOn("engine") class Car implements Vehicle {}` - над классом указываем зависимость, которую нужно загрузить до класса. **Нужно** когда зависимость неявная, например JDBC driver loading или static variable initialization. В обычном случае Spring сам оприделяет последовать создания зависимостей.
    * `@Bean @DependsOn("fuel") Engine engine() {}` - над factory method зависимого бина.
  * `@Lazy` - отмечаем бины которые нужно создать **lazily** во время первого обращения к этому bean, **by default** они создаются **eager** во время запуска application context (**Note.** тут есть нюанс, что для field и setter они создаются при первом вызове? т.к. при constructor они создаются сразу при запуске контекста). Можно отмечать как `@Lazy(false)` чтобы переопределить глобальные значения и отключить lazy. **Рекомендуется** в обычных случаях не использовать `@Lazy`, т.к. bean может быть инициализирован не сразу (например через дни использования) и ошибки выявятся не скоро.
    * над `@Bean factory method` - влияет на этот метод
    * над `@Configuration class` - влияет на все методы класса
    * над `@Component class` - влияет на создание этого bean
    * над `@Autowired constructor, setter, field` - влияет на зависимость (via proxy)
  * `@Lookup` - для **inject prototype bean в singleton bean** при каждом вызове какого-то метода этого singleton бина (т.к. каждый раз создается новый prototype бин). И для **inject процедурным способом** (т.е. при вызове метода вручную видимо?). Можно использовать **abstract + `@Lookup`**, если **surrounding class** это **component-scan** класс (сканируемый на компоненты) или если **surrounding class** является **@Bean-manage** (бин управляемый контейнером).
      * **Процесс использоваения `@Lookup`**
        ```java
        // 1. inject prototype bean в singleton bean
        // создаем метод заглушку в singleton бине
        // Spring внутри создаст наследника этого класса и в нем переопределит этот метод
        // при КАЖДОМ вызове этого метода в singleton бине он будет возвращать новый prototype bean
        // Note. не важно что return из этого метода, оно будет заменено, рекомендуется null
        @Lookup
        public Passenger createPassenger() {
            return null;
        }

        // 2. inject процедурным способом
        // можно исопльзовать abstract + @Lookup если не используется component-scan или другое
        // @Bean-manage (видимо имеется ввиду что не отрабатывает авто поиск и создание бинов)
        // т.к. component-scan не учитывает abstract бины
        // 2.0 делаем бин
        @Component
        @Scope("prototype")
        public class SchoolNotification {
            private String name;
            public SchoolNotification(String name) {} // передаем параметр при создании и в зависимости от него строим логику
        }
        // 2.1 делаем метод источник бинов, принимает параметр который передается в конструктор бина
        // (abstract не обязательно, но можно в НЕКОТОРЫХ см. выше случаях)
        @Lookup
        protected abstract SchoolNotification getNotification(String name);
        // 2.2 получаем внутри метода по имени новый бин prototype
        public String f1(String name) {
            SchoolNotification notification = getNotification(name);
        }
        ```
  * `@Scope("prototype")` - обьявление scope над `@Component` или `@Bean`
    * **Отдельные варианты:** `@SessionScope`, `@RequestScope`, `@ApplicationScope` (других отдельных напр. для **singleton** нету, другие scope задаются параметрами `@Scope` аннотации)
      * **Note.** `@RequestScope` аналогично `@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyModel = ScopedProxyMode.TARGET_CLASS)` т.е. имеет `ScopedProxyMode.TARGET_CLASS` т.е. это proxy by default
  * `@Order(2)` - используется с `@Bean` или `@Component`, чтобы указать какой бин выбрать первым для связывания, чем меньше число тем выше приоритет
    * эта аннотация не решает конфликт связывания, а только указывает порядок e.g. для `List<MyBean>`???
    * эта аннотация не влияет на порядок создания beans
    * можно использовать `@Order` вместе с `@Primary` для связывания **by Type** - для приоритета связывания+создания???
    * `@Order(Ordered.LOWEST_PRECEDENCE)`
  * `@NonNull` - Spring в случае правильного определения класса, но при ошибках может заинжектить `null`, эта аннотация говорит не использовать `null`, может быть применена на **field**, **method parameter**, **method return value**. С этой аннотацией можно распознать проблемы.
    * `@Nullable` - можно применить например чтобы исключить проверку на `null` полей пакета помеченного `@NonNullFields`
    * `@NonNullFields` - применяется на всем пакете в файле `package-info.java`, говорит что все поля в пакете неявно `@NonNull`, применяется к **field**
    * `@NonNullApi` - как `@NonNullFields`, но применяется к **method parameter**, **method return value**
  * `@AliasFor` - ссылки в  аннотациях на одинаковые атрибуты с разными именами, в том числе из других аннотаций
    ```java
    public @interface ContextConfiguration {
        @AliasFor("locations") String[] value() default {}; // value тоже что и locations и наоборот
        @AliasFor("value") String[] locations() default {};
        
        @AliasFor(annotation = ContextConfiguration.class, attribute = "locations")
        String[] xmlFiles() // xmlFiles ссылка на locations из аннотации ContextConfiguration
    }
    ```
* **Context Configuration Annotations** - конфигурирование application context
  * `@Profile({"p1", "!p2"})` - отмечаем `@Component` или `@Bean` только если хотим, чтобы они создавались при определенном Spring профиле
  * `@Import(VehiclePartSupplier.class)` - отмечаем `@Configuration` и указываем внутри другой класс `@Configuration` чтобы импортировать один в другой
    ```java
    @Target({ElementType.TYPE}) // включение Config через простановку аннотации
    @Retention(RetentionPolicy.RUNTIME)
    @Inherited
    @Import({MyConfig.class}) // @Configuration
    public @interface EnableMyConfig { }
    ```
  * `@ImportResource("classpath:/annotations.xml")` - отмечаем `@Configuration`, импорт Spring XML из Spring (с bean definition `<bean>`)
    ```java
    @Configuration
    @ImportResource("classpath:/com/acme/database-config.xml") // DataSource bean
    public class AppConfig {
        @Inject DataSource dataSource; // from XML

        @Bean MyBean myBean() { return new MyBean(this.dataSource); }
    }
    ```
  * `@PropertySource("classpath:mysql.properties", ignoreResourceNotFound = true)` - отмечаем `@Configuration` class, после этого можно использовать **property** внутри этого класса и в аннотации `@Value`. Начиная с Java 8 эта аннотация `@Repeatable` и можно над классом ставить несколько таких аннотаций.
    * `@PropertySources({@PropertySource("classpath:/annotations.properties"), @PropertySource("classpath:vehicle-factory.properties")})` - можно указать массив аннотаций `@PropertySource` внутри `@PropertySources`
  * `@ConstructorBinding` - 
  * `@ConfigurationProperties` ([1](https://www.baeldung.com/configuration-properties-in-spring-boot)) - как `@Value`, авто связывание property и объекта
    * **Note.** Аннотация `@ConfigurationProperties` поддерживает **Bean Validation** `JSR-303`
    ```java
    //  com.my.container.items:
    //      item1:
    //          name: bla
    //          prop: myProp
    //      item2:
    //          name: bla
    //          prop: myProp
    class ItemInfo {
        String name;
        String prop;
    }

    // com.my.container.items
    @Configuration
    @ConfigurationProperties(prefix = "com.my.container") // com.my.container + .items
    class ItemPropertiesConfig { Map<String, ItemInfo> items; }
    ```
    * `@EnableConfigurationProperties(ItemPropertiesConfig.class)` - альтернатива проставлению `@Configuration` над классом `@ConfigurationProperties`
        ```java
        @Configuration
        @EnableConfigurationProperties(ItemPropertiesConfig.class)
        class AdditionalConfiguration {
            @Autowired ItemPropertiesConfig itemPropertiesConfig;
        }
        ```
    * **Property Conversion** - авто конвертация типов (как unboxing)
        ```java
        // conversion.timeInDefaultUnit=10
        // conversion.timeInNano=9ns
        // conversion.sizeInDefaultUnit=300
        // conversion.sizeInGB=2GB
        // conversion.sizeInTB=4
        @ConfigurationProperties(prefix = "conversion")
        class PropertyConversion {
            Duration timeInDefaultUnit; // 10 ms
            Duration timeInNano; // 0 ms, 9 nanosec
            @DataSizeUnit(DataUnit.TERABYTES) DataSize sizeInDefaultUnit;
            DataSize sizeInGB;
            @DataSizeUnit(DataUnit.TERABYTES) DataSize sizeInTB;
        }
        ```
    * **Custom Converter** - конвертер кастомного формата, авто поиск конвертера на основе class
        ```java
        // conversion.employee=john,2000
        @Configuration
        @ConfigurationProperties(prefix = "conversion.employee")
        class AdditionalConfiguration {
            @Autowired Employee employee;
        }

        @Component
        @ConfigurationPropertiesBinding
        class EmployeeConverter implements Converter<String, Employee> {
            @Override
            public Employee convert(String from) {
                String[] data = from.split(",");
                return new Employee(data[0], Double.parseDouble(data[1]));
            }
        }
        ```
    * **Immutable Binding** - можно привязывать к `final field`
        ```java
        @ConfigurationProperties(prefix = "mail.credentials")
        @ConstructorBinding
        public class ImmutableCredentials {
            private final String authMethod;
            private final String username;
            private final String password;
        }
        ```
  * `@AutoConfigureBefore` / `@AutoConfigureAfter` - конфиги после/до которых должен запуститься текущий
    * `@AutoConfigureOrder(2)` - аналог `@Order` но только между `@Configuration` классами, чтобы не влиять на все с `@Order`
    ```java
    @AutoConfigureOrder(2) // аналог @Order, чтобы не конфликтовать с ним, только для config классов
    @AutoConfigureBefore({LiquibaseAutoConfiguration.class})
    @Configuration
    class Cfg 
        @Autowired Docket docket;

        @PostConstruct // set config in @PostConstruct ?
        public void addGlobalRequestParameters() {
            docket.globalOperationParameters(List.of(
                    new ParameterBuilder()
                            .name(MY_PARAM_1).description("Param description.")
                            .modelRef(new ModelRef(Types.typeNameFor(Boolean.TYPE)))
                            .parameterType("query").required(false).build()
            ));
        }
    }
    ```
  * **ConditionalOn** ([1](https://reflectoring.io/spring-boot-conditionals/)) - create a `@Component/@Bean` if condition is true
    * `@ConditionalOnClass(name = "this.clazz.does.not.Exist")`
    * `@ConditionalOnMissingClass(value = "this.clazz.does.not.Exist")`
    * `@ConditionalOnProperty(prefix = "docker_42", name = "enabled", havingValue = "sms", matchIfMissing = false)`
    * `@ConditionalOnMissingBean` - create bean when no bean with the same type found. It is needed when we `exclude` a bean and want to replace the **excluded** bean by new one.
      ```java
      class Cfg {
          @ConditionalOnMissingBean // create MyService if there is no MyService bean already (e.g. old bean was excluded and will be replaced by the bean)
          // @ConditionalOnMissingBean(MyService.class) // excplicit parameter
          @Bean public MyService myService() { return new MyService(); }
      }
      ```
    * `@ConditionalOnExpression("${module.enabled:true} and ${module.submodule.enabled:true}")` - value из properties файла
    * `@ConditionalOnBean(OtherModule.class) @Component class A { }`
    * `@ConditionalOnSingleCandidate(DataSource.class)` - if there is one Bean only with the type
    * `@ConditionalOnResource(resources = "/logback.xml")` - если файл есть в classpath
    * `@ConditionalOnJndi("java:comp/env/foo")`
    * `@ConditionalOnJava(JavaVersion.EIGHT)`
    * `@ConditionalOnWebApplication`
    * `@ConditionalOnCloudPlatform(CloudPlatform.CLOUD_FOUNDRY)`
    * **Custom Condition**
        ```Java
        class OnUnixCondition implements Condition {
            @Override
            public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
                return SystemUtils.IS_OS_LINUX;
            }
        }
        ```
    * **Combining Conditions with OR**
        ```Java
        class OnWindowsOrUnixCondition extends AnyNestedCondition {
            OnWindowsOrUnixCondition() { super(ConfigurationPhase.REGISTER_BEAN); }
            @Conditional(OnWindowsCondition.class) static class OnWindows {}
            @Conditional(OnUnixCondition.class) static class OnUnix {}
        }

        @Bean
        @Conditional(OnWindowsOrUnixCondition.class) // use
        WindowsOrUnixBean windowsOrUnixBean() { return new WindowsOrUnixBean(); }
        ```
    * **Combining Conditions with AND** - just use multi OR conditions. Or use `AllNestedConditions` from **Spring Boot**
        ```Java
        // 1st variant, Spring Framework
        @Bean
        @ConditionalOnUnix
        @Conditional(OnWindowsCondition.class)
        WindowsAndUnixBean windowsAndUnixBean() { return new WindowsAndUnixBean(); }

        // 2nd variant, Spring Boot ONLY
        static class OnJndiAndProperty extends AllNestedConditions {
            OnJndiAndProperty() { super(ConfigurationPhase.PARSE_CONFIGURATION); }

            @ConditionalOnJndi()
            static class OnJndi { }

            @ConditionalOnProperty("something")
            static class OnProperty { }
        }
        ```
  * `@EntityScan`
* **Test**
  * `@TestConfiguration` - https://stackoverflow.com/a/50643036
* **Exception Handle**
  * `@ControllerAdvice` - из Spring MVC, контрлллер который перехватывает Exceptino глобально
  * `@RestControllerAdvice` - из Spring MVC, добавляет всем методам `@RequestBody`
  * `@ExceptionHandler(SQLException.class)` - метод из `@ControllerAdvice` или `@Controller` для конкретного типа ошибок
    * **Note.** В `@ExceptionHandler` можно перехватить и **Bean Validation** exceptions и другие, а не только те которые `throw` напрямую из кода
    * **Note.** Можно использовать `@ResponseStatus` с `@ExceptionHandler`
        ```java
        @ExceptionHandler(Exception.class)
        @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
        public void handleError(Exception ex) {
            // ...
        }
        ```
  * **Note.** В некоторых случаях рекомендуется возвращать `@ResponseStatus(404)` вместо прямого перехвата исключения (эта аннотация для метода контроллера).

**Spring MVC аннотации**
* `@RequestParam` - извлекает **parameters**, файлы etc из request
  * Note. Извлекает из body, query, form data - by default из всех возможных мест
  * `@RequestParam(name = “id”) fooId` или `@RequestParam(value = “id”) fooId` или `@RequestParam(“id”) fooId` или `@RequestParam String fooId` - разные варианты
  * `@RequestParam(required = false) String id` - **required = false** чтобы не получить ошибку если параметра нет в request, если параметра нет, то будет установлен **null**
  * `@RequestParam(defaultValue = "test") String id` - установка значение по умолчанию, после этого **required=false** необязательно
  * `@RequestParam Map<String,String> allParams` - маппинг всех параметров
  * `@RequestParam List<String> id` - Multi-Value Parameter, когда один параметр может иметь несколько значений, например `http://localhost:8080/api/foos?id=1,2,3` **или** `http://localhost:8080/api/foos?id=1&id=2`
* `@PathVariable` - извлекает **URI path parameters** из request, при этом метод или класс `@Controller` должен быть отмечен `@GetMapping("/foos/{id}")` и адрес `http://localhost:8080/foos/abc`
  * `@PathVariable(required = false) String id` - аналогично как в `@RequestParam`, если параметра нет, то не будет ошибки и переменная будет **null**. **Note:** если использовать **required = false**, то **могут** возникнуть конфликты в путях
* `@PathVariable` vs `@RequestParam` - в адресе `@RequestParam` параметры **URL encoded**, т.е. спец. символы экранируются и после преобразования в String исчезнут, например `/foos?id=ab+c` будет `ab c`
* `@SpringQueryMap` - ограничивает параметры из запроса только query `Dto getAllowedUsers(@SpringQueryMap Dto query);` (by default берутся из body, query, form data)
* `@Repository, @Service, @Configuration, and @Controller` - ведут себя как `@Component` и внутри своей реализации используют его
* `@Controller`
* **`@RestController`** == `@Controller` + `@ResponseBody`, после этой аннотации МЕТОДЫ контроллера не нужно помечать как `@ResponseBody`, можно просто `return myObj` и `myObj` сам преобразуется в JSON
* `@Cacheable("books")` - включается простановкой `@EnableCaching` над `@Configuration`
  * Создаем бин
    ```java
    @Bean
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("books");
    }
    ```
* `@PageableDefault` - указываем параметры `pageable` для **Spring Data**
    ```java
    Response<Page<ResourcesResp>> page(@PageableDefault(value = 15, sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable) {
    ```
    ```yml
    # application.yml
    spring.data.web.pageable.max-page-size: 1000000000 # для Spring Data JPA
    spring.data.rest.max-page-size: 1000000000 # для Spring Data REST
    ```
* `@SortDefault` - 

## `getBean()`
**Имеет 5 сигнатур:**
* `(Tiger) context.getBean("lion");` - **by name**, нужно вручную использовать приведение типа
* `context.getBean("lion", Lion.class);` - **by Name and Type**
* `context.getBean(Lion.class);` - **by Type**
* `(Tiger) context.getBean("tiger", "Siberian");` - **by Name** with Constructor Parameters, 2ой параметр передается в конструктор
* `context.getBean(Tiger.class, "Shere Khan");` - **by Type** With Constructor Parameters

**Note.** Берем bean по **class** + его **Generics**
```java
// т.к. бин создается автоматом при связывании, то у нас нету его class чтобы взять бин
@Autowired MyBean<MyGeneric1> myBean;

// массив имен бинов
String[] beanNames = ctx.getBeanNamesForType(ResolvableType.forClassWithGenerics(MyBean.class, MyGeneric1.class));

// Берем 1ый бин
MyBean<T> myBean = (MyBean<T>) ctx.getBean(beanNames[0]);

Map<String, Object> beans = context.getBeansWithAnnotation(Foo.class); // by annotation

// be Generic
ResolvableType resolvableType = ResolvableType.forClassWithGenerics(MyBean.class, genericObject.getClass());
ObjectProvider objectProvider = applicationContext.getBeanProvider(resolvableType);
MyBean myBean = objectProvider.getObject(); // exception if a bean is not unique
MyBean myBean = objectProvider.stream(); // unordered beans (ordered by registration)
MyBean myBean = objectProvider.orderedStream(); // ordered beans by @Order
```

## What is a Spring Bean? Этапы создания Spring Bean
**Spring Bean** - обьект который управляется **Spring IoC container**, создается им и управляется

**Inversion of Control** - процесс при котором обьект обьявляет свои зависимости без их явного создания (и IoC контейнер сам их подставляет)

Когда Spring создает обьекты они вызываются в порядке их обьявления в конфигурации.

**Этапы создания spring beans:**
1. Обьявить bean (**bean defenition**)
    ```java
    @Component
    public class Company {
        // this body is the same as before
    }
    ```
2. Создать **конфигурацию** бина (**bean metadata**)
    ```java
    @Configuration
    @ComponentScan(basePackageClasses = Company.class)
    public class Config {
        @Bean
        public Address getAddress() {
            return new Address("High Street", 1000);
        }
    }
    ```
3. Создать instance of **ApplicationContext**
    ```java
    ApplicationContext context = new AnnotationConfigApplicationContext(Config.class);
    Company company = context.getBean("company", Company.class); // проверяем
    ```

## Базовые приемы работы с xml конфигурацией
```java
// Static Factory
public class StaticServiceFactory {
    public static IService getNumber(int number) {}
}

// Factory Method (non static)
public class InstanceServiceFactory {
    public IService getNumber(int number) {}
}
```
```xml
<!-- Using Properties -->
<bean
  id="indexService"
  class="com.baeldung.di.spring.IndexService" />
<bean
  id="indexApp"
  class="com.baeldung.di.spring.IndexApp" >
    <property name="service" ref="indexService" />
</bean>

<!-- Using Constructor -->
<bean
  id="indexApp"
  class="com.baeldung.di.spring.IndexApp">
    <constructor-arg ref="indexService" />
</bean>

<!-- Static Factory -->
<bean
  id="indexApp"
  class="com.baeldung.di.spring.IndexApp">
    <constructor-arg ref="indexService" />
</bean>

<!-- Factory Method (non static) -->
<bean id="indexServiceFactory"
  class="com.baeldung.di.spring.InstanceServiceFactory" />
<bean id="messageService"
  class="com.baeldung.di.spring.InstanceServiceFactory"
  factory-method="getService" factory-bean="indexServiceFactory">
    <constructor-arg value="1" />
</bean>  
<bean id="indexApp" class="com.baeldung.di.spring.IndexApp">
    <property name="service" ref="messageService" />
</bean>
```

## Inversion of Control
**Inversion of Control** - принцип в OOP когда управление обьектами или куском программы передается контейнеру или фреймворку. В противопложность в обычном приложении используются вызовы функций библиотек. Чтобы определить функции нужно extends классы фреймворка.

Фичи:
* decoupling функционала от их реализации (т.е. можно подставлять разные реализации в runtime)
* модульность
* легко тестировать (подставляя fake реализации)

**IoC** - реализуется через паттерны: Strategy, Service Locator, Factory and Dependency Injection (DI)

**DI** - паттерн через который реализован IoC, контроль над зависимостями inverted (передан) конфигурации вместо. Inject одних обьектов в другие (зависимостей) делается на этапе сборки (хотя в некоторых ситуациях может делаться и во время выолпнения).

IoC container - главная характеристика IoC фреймворка. В Spring контейнер IoC это ApplicationContext класс (соотв. и обьект). ApplicationContext создает, конфигурирует, собирает (видимо имеется ввиду инжектит зависимости) обьекты, управляет их lifecycle.

Вариаци ApplicationContext:
* ClassPathXmlApplicationContext
* FileSystemXmlApplicationContext
* WebApplicationContext

Dependency Injection можно сделать через constructors, setters or fields. constructor injection **рекомендуется** для обязательных зависимостей, setters для необязательных.

**fields injection не рекомендуется** потому что:
* это более ресурсоемко чем constructor или setters injection, т.к. используется reflection api
* легко добавить слишком много зависимостей (`@Autowired`) и нарушить S из SOLID, с конструктором эта ошибка будет видна явно

**Wiring** - то что **Spring IoC** использует для **inject** зависимостей, бывают **типы wiring**:
* **no** - не используется autowiring, и нужно явно использовать name бина (видимо связывание по имени?)
* **byName** - autowiring по имени свойства (поля)
* **byType** - autowiring по типу поля
* **constructor** - подстановка в constructor при создании Spring Bean. Это саммый ранний этап из всех.

Все **singleton** бины создаются и настраиваются контейнером во время **инициализации** (запуска приложения). Можно использовать **lazy**, тогда бин будет создан во время 1го запроса, а не запуска. Плюсы в lazy - быстра инициализация, минусы - ошибки могут быть не найдены часы, дни и больше.

## Spring учитывает только `class name` для bean и не учитывает сами `package`
Source: [1](https://stackoverflow.com/a/57396751)

**Кратко:** классы с одинаковыми именами, но разными `package` считаются одним и тем же **bean**, и если их 1+, то при `@Autowiring` будет `ConflictingBeanDefinitionException`

В Spring при inject **by Class** не учитывая **package**, используется только имена самих классов! Т.е. `com.package1.MyClass == com.package2.MyClass`, имена классов из разных пакетов создадут конфликт при `@Autowiring MyClass myClass` и выбросят `ConflictingBeanDefinitionException` если использовать эти beans одновременно. Это можно изменить переопредлив `AnnotationBeanNameGenerator`, который возвращает `object.getSimpleName()` на возврат `package+class` (`full qualified name`). Или можно использовать `@Qualifier("myName")`
```java
static class BeanNameGeneratorIncludingPackageName extends AnnotationBeanNameGenerator {
    public BeanNameGeneratorIncludingPackageName() { }

    @Override
    public String buildDefaultBeanName(BeanDefinition beanDefinition, BeanDefinitionRegistry beanDefinitionRegistry) {
        return beanDefinition.getBeanClassName();
    }
}
```

## Constructor Dependency Injection
Constructor Dependency Injection - когда бины создаются и inject делается во время запуска.

С Spring 4.3 аннотация `@Autowired` над конструктором может быть пропущена, если конструктор 1ин, тоже самое касается класса `@Configuration`

## Constructor injection vs Setter injection

Источник: [тут](https://docs.spring.io/spring/docs/5.2.4.RELEASE/spring-framework-reference/core.html#beans-dependency-resolution)

**Рекомендованный подход:**
* **setter injection** - для необязательных (optional) зависимостей. При этом нужно или установить default значения таким полям, или делать `not null` проверки в местах использования кода. **Плюс:** позволяет сделать переконфигурирование или re-inject позднее (e.g. поэтому JMX MBeans используют `setter injection`). Хотя чтобы сделать обязательными над ними можно поставить `@Required`, но рекомендуется вместо этого использовать constructor injection т.к. происходить `injection with programmatic validation of arguments`
* **constructor injection** - для обязальеных зависимостей. Рекомендован для Spring т.к. компоненты (бины) становятся `immutable objects` и имеют проверку что зависимости не `null`. Компонент который использует `constructor injection` всегда возвращается клиенту (месту в коде куда вставляется зависимость) в виде полностью инициализированного объекта. **Минус:** конструктор может иметь много аргументов (это плохая практика), тогда бин можно разделить на мелкие куски.

**Note.** Если есть старый код из библиотек, то решать что использовать по ситуации, e.g. класс может не иметь setters. 

## Scopes
**Scope** - `СВОЕ ОПРЕДЕЛЕНИЕ!`, Это или context (e.g. набор переменных ссылок на **beans**) или это wrapper для `ThreadLocal`. **Scope** хранят **beans**. Для некоторых **scope** используется **не** `InheritableThreadLocal` (e.g. `@RequestScope`), т.е. новые `@Async` потоки не наследуют **Scope**.  
**Note.** Часто вместо создания нового **Scope** который виден и `@RequestScope` и `@KafkaListener` используется `ThreadLocal`.

**Scopes:**
* **spring core**
  * `singleton` (default, stateless) - один bean на все apllication, bean будет в application context
  * `prototype` (stateful) - при inject каждый раз новый bean, не будет помещен в application context
* **web-aware application context** - Действует, только если вы используете web-aware `ApplicationContext`
  * `request` - живет пока жив HTTP request
  * `session` - живет пока жив HTTP Session
  * `global session` - живет пока жива глобальная HTTP Session (обычно при использовании portlet контекста).
  * `application` - живет пока жив `ServletContext` (его lifecycle), как singleton но в `ServletContext`, может принадлежать нескольким `ApplicationContext` (т.к. в одном `ServletContext` может быть много `ApplicationContext`)
    * **Note.** т.к. у каждого `DispatcherServlet` может быть только **1ин** `ApplicationContext`
  * `websocket` - живет пока жив WebSocket session

**Способы задания:**
* `@Scope("singleton")`
* `@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)`
* `@Scope(value = WebApplicationContext.SCOPE_REQUEST, proxyMode = ScopedProxyMode.TARGET_CLASS)` - request scope
* `@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)` - session scope, как и для request scope
* `@Scope(scopeName = "websocket", proxyMode = ScopedProxyMode.TARGET_CLASS)`

`proxyMode = ScopedProxyMode.TARGET_CLASS` - **TARGET_CLASS** при указании **Request Scope** обязателен, т.к. во время создания web application context нету активного **request**, Spring создаст proxy обьект чтобы inject его как зависимость и создать target bean, когда он нужен для request.

**prototype vs singleton** - `@PreDestroy` не вызвается для prototype, потому что бин не в контексте. Spring не удаляет prototype бины.

**singleton vs application scope** ([1](https://stackoverflow.com/a/63875502)) - 

## Spring Profiles & Maven Profile
### Spring Profile
**@Profile on a Bean**
```java
@Component
@Profile("dev")
public class DevDatasourceConfig

@Component
@Profile("!dev")
public class DevDatasourceConfig
```
**Declare Profiles in XML**
```xml
<beans profile="dev">
    <bean id="devDatasourceConfig"
      class="org.baeldung.profiles.DevDatasourceConfig" />
</beans>
```
**Set Profiles Programmatically via WebApplicationInitializer interface**
```java
@Configuration
public class MyWebApplicationInitializer 
  implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.setInitParameter("spring.profiles.active", "dev");
    }
}
```
**Set Profiles Programmatically via ConfigurableEnvironment**
```java
@Autowired
private ConfigurableEnvironment env;
env.setActiveProfiles("someProfile");
```
**Context Parameter in web.xml**
```xml
<context-param>
    <param-name>contextConfigLocation</param-name>
    <param-value>/WEB-INF/app-config.xml</param-value>
</context-param>
<context-param>
    <param-name>spring.profiles.active</param-name>
    <param-value>dev</param-value>
</context-param>
```

**JVM System Parameter**
```sh
-Dspring.profiles.active=dev
```

**Environment Variable**
```sh
export spring_profiles_active=dev
```

**Устаовка профиля аннотацией**
```java
@ActiveProfiles("dev")
```

**Приоритет активации профилей по убыванию (приоритет у того что выше)**
1. web.xml
2. WebApplicationInitializer
3. JVM System parameter
4. Environment variable
5. Maven profile

**Default Profile**
<br>
По умолчанию каждый бин принадлежит **default** профилю. Через свойство `spring.profiles.default` можно установить свой профиль по умолчанию.

**list of active profiles programmatically**
```java
@Autowired
private Environment environment;
// список профилей
for (String profileName : environment.getActiveProfiles()) {}
```

**Получить активный профиль** или пустую строку, если default значения пустой строки не будет, то будет `IllegalArgumentException`
```java
@Value("${spring.profiles.active:}")
private String activeProfile;
```

### Profiles in Spring Boot
1. через конфиг
    ```properties
    spring.profiles.active=dev
    ```
2. Программно
    ```java
    SpringApplication.setAdditionalProfiles("dev");
    ```
3. Используя `spring-boot-maven-plugin` в `pom.xml`
    ```xml
    <plugins>
        <plugin>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-maven-plugin</artifactId>
            <configuration>
                <profiles>
                    <profile>dev</profile>
                </profiles>
            </configuration>
        </plugin>
        ...
    </plugins>
    ```

    После установки профиля запускаем
    ```sh
    mvn spring-boot:run
    ```
4. Задание специфичных properties файлов для профилей (называем добавляя имя профиля через дефиз):
`applications-{profile}.properties` и предшествует файлу по умолчанию (т.е. файл по умолчанию перекрывает его свойства). Файл `application.properties` будет работать для всех.
5. Через один файл
    ```yaml
    server:
        port: 9000
    ---
    spring:
        profiles: development
    server:
        port: 9001
    ---
    spring:
        profiles: production
    server:
        port: 0
    ```

### Maven Profile
Передача Spring профиля через maven в параметре `spring.profiles.active`
```xml
<profiles>
    <profile>
        <id>dev</id>
        <activation>
            <activeByDefault>true</activeByDefault>
        </activation>
        <properties>
            <spring.profiles.active>dev</spring.profiles.active>
        </properties>
    </profile>
    <profile>
        <id>prod</id>
        <properties>
            <spring.profiles.active>prod</spring.profiles.active>
        </properties>
    </profile>
</profiles>
```
В `application.properties`
```properties
spring.profiles.active=@spring.profiles.active@
```
При этом нужно включить фильтрацию ресурсов
```xml
<build>
    <resources>
        <resource>
            <directory>src/main/resources</directory>
            <filtering>true</filtering>
        </resource>
    </resources>
    ...
</build>
```
Включение профиля передачей параметра в maven
```sh
mvn clean package -Pprod
```

## Project Configuration with Spring
Задание файла конфигов (e.g. файл `persistence-dev.properties`)
```java
@PropertySource({ "classpath:persistence-${envTarget:dev}.properties" })
```
Установка свойств, где envTarget это
```sh
-DenvTarget=dev
```
Задание переменной через `pom.xml`
```xml
<plugin>
   <groupId>org.apache.maven.plugins</groupId>
   <artifactId>maven-surefire-plugin</artifactId>
   <configuration>
      <systemPropertyVariables>
         <envTarget>h2_test</envTarget>
      </systemPropertyVariables>
   </configuration>
</plugin>
```

## Properties with Spring and Spring Boot
Само подключение **properties** файлов реализовано через старый `PropertyPlaceholderConfigurer` или новый `PropertySourcesPlaceholderConfigurer`
```java
@Autowired
private Environment env;
dataSource.setUrl(env.getProperty("jdbc.url")); // если нету, вернет null
```
Указываем properties файл (по умолчанию `application.properties`)
```sh
java -jar app.jar --spring.config.location=classpath:/another-location.properties
```
Установка свойства
```sh
java -jar app.jar --property="value"
```
Установка **environment** свойства
```sh
java -Dproperty.name="value" -jar app.jar
```
Установка **environment** свойства **глобально**
```sh
export name=value
java -jar app.jar
```
Случайные свойства
```java
random.number=${random.int}
random.long=${random.long}
random.uuid=${random.uuid}
```
Используя бин
```java
@Bean
public static PropertySourcesPlaceholderConfigurer properties(){
    PropertySourcesPlaceholderConfigurer pspc  = new PropertySourcesPlaceholderConfigurer();
    Resource[] resources = new ClassPathResource[ ] { new ClassPathResource( "foo.properties" ) };
    pspc.setLocations( resources );
    pspc.setIgnoreUnresolvablePlaceholders( true );
    return pspc;
}
```

**`<property-placeholder> vs @PropertySource`**
* **`<property-placeholder>`**
  * Если свойство в Parent context, то `@Value` не берет значения из Child context
  * Если свойство в Child context, то `@Value` не берет значения из Parent context
  * environment.getProperty работать не будет т.к. свойства не пробросятся в environment
* **`@PropertySource`**
  * Если свойство в Parent context, то `@Value` работает и в Parent и в Child, `environment.getProperty` работает и в Parent и в Child
  * Если свойство в Child context, то `@Value` работает только в Child, `environment.getProperty` работает только в Child

## Наследование `@Transactional`
Источник: [1](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop)

Spring рекомендует чтобы `@Transactional` проставляли над классами и методами, а не интерфейсами. Ставить эту аннотацию над интерфейсами и их методами можно только если используется interface-based proxies (видимо имеется ввиду repository классы из spring data?). `@Transactional` - отмечена как `@Inherited` т.е. она наследуется классами. Но она не наследуется от методов интерфейса.

Только **public** методы **Java Dynamic Proxy** могут быть `@Transactional`, не public методы можно отметить, но при их вызове вообще ничего не произойдет в том числе ошибки.  
**Note.** в новых версиях Spring используется CGLIB и поведение возможно будет другим. Но это поведение можно изменить используя AspectJ и связывание на этапе компиляции.  
В новых версиях AOP работает через CGLIB для **protect** и **package** (если работа для них указана в правиле самого **AOP pointcut**). Когда-то даже при использовании CGLIB не работало AOP для protected и package, но в нновых версия Spring работает.

## Как работает транзакция?

**Используется:**
* EntityManager Proxy - вызывает SessinoFactory и создает Session ипользуя Thread (или берет уже созданную Thread)
* Transactional Aspect - класс TransactionInterceptor реализует саму логику around из AOP.
* Transaction Manager - если нужно создает Session (при первом вызове или берет из пулла созданную) и отвечает за создание транзакции на уровне DB (т.е. добавляет к SQL запросу создание транзакции).

## NoSuchBeanDefinitionException
`NoSuchBeanDefinitionException` будет если бин не найден и autowired стоит true (по умолчанию)

## Как работают `@Transactional` и в каком случае они могут не работать
Транзакцию можно проставлять только на public методах, т.к. `JDK Dynamic Proxy` срабатывает только для public. Хотя если используется CGLIB или AcpectJ, то возможно все будет работать по другому т.к. они создают прокси не только для public методов.

Если метод помеченный `@Transactional` класса вызывает другой такой же метод этого же класса, то новая транзакция **не запустится** (и вообще во всех подобных случаях AOP не сработает) даже с `REQUIRES_NEW` уровнем изоляции, это осбенность работы proxy паттерна в Spring AOP, т.к. вызов метода прокси будет из того же прокси и следовательно вызовется не прокси метод, а настоящий без AOP обертки. Чтобы решить эту проблему можно использовать вместо Spring AOP например AspectJ и связывание AOP на этапе компиляции (а не на этапе вызова как это делается по умолчанию).

## Spring Events

### Common
**Чтобы работать с событиями нужно:**
1. Создать событие `MySpringEvent extends ApplicationEvent`
2. заинжектить `ApplicationEventPublisher` и опубликовать  
   `@Autowired var applicationEventPublisher;`  
   `applicationEventPublisher.publishEvent(mySpringEvent);`
3. Альтернатива: implements ApplicationEventPublisherAware
4. ИЛИ слушатель должен реализовать `ApplicationListener`
       и метод `onApplicationEvent(MySpringEvent event)`
5. ИЛИ иметь аннтацию (в новых версиях)
   `@EventListener(condition = "#event.success")`

**Пример ApplicationEvent:**
```java
// 1. Создаем event:
public class CustomSpringEvent extends ApplicationEvent {
    String message;
}

// 2. Публикуем где-нибудь в коде event
@Autowired ApplicationEventPublisher applicationEventPublisher;
applicationEventPublisher.publishEvent(new CustomSpringEvent());

// 3. подписываемся на event
@Component
public class AnnotationDrivenContextStartedListener {
    // @Async
    @EventListener(condition = "#event.success")    // только для успешных событий
    public void handleContextStart(ContextStartedEvent cse) {
        System.out.println("Handling context started event.");
    }
}
```

Существует много готовых событий: ContextRefreshedEvent, ContextStartedEvent, RequestHandledEvent etc

**Note!** Событие Spring - `RefreshContext` может **часто** используется для того чтобы сделать что-то после инициализации всех сервисов Spring, но перед стартом **spring context** (e.g. `ApplicationContext`). Хотя с современным скорее всего **Life Cycle** можно подобрать нужный этап **Life Cycle** без использования `RefreshContext`.

**Пример:**
```java
public class ContextRefreshedListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent cse) {
        ApplicationContext context = event.getApplicationContext(); // получаем ApplicationContext из event
        System.out.println("Handling context re-freshed event. ");
        if (context.containsBean("accessControlRegisterService")) { // проверяем есть ли bean в context
            context.getBean(AccessControlRegisterService.class).doSmth(); // достаем и делаем что-то
        }
    }
}
```


## Transaction Management

### Common
Source: [spring Transaction Management](https://docs.spring.io/spring-framework/reference/data-access/transaction.html), [spring transaction strategies](https://docs.spring.io/spring-framework/reference/data-access/transaction/strategies.html), [baeldung transactions](https://www.baeldung.com/java-transactions), [baeldung programmatic transaction management](https://www.baeldung.com/spring-programmatic-transaction-management), [habr](https://habr.com/ru/articles/532000/)

```
PlatformTransactionManager - главный interface который реализуют

Иерархия:
TransactionManager
    PlatformTransactionManager
        AbstractPlatformTransactionManager
            DataSourceTransactionManager // JDBC
            JMSTransactionManager
            JPATransactionManager
            JTATransactionManager
            ...
```
```yml
logging:
   level:
      org.springframework.orm.jpa: DEBUG
      org.springframework.transaction: DEBUG
logging.level.org.springframework.transaction.interceptor: TRACE # start/end transaction
```
```java
// даже если подавить Exception транзакции оно все равно будет rollback и throw из аспекта
// org.springframework.transaction.UnexpectedRollbackException
try {
  doTx();
} catch(Exception ignore) { } // все равно будет re-throw
```
### Programmatic Transaction Management in Spring
Source: [1](https://www.baeldung.com/spring-programmatic-transaction-management)
```java
@EnableTransactionManager
class Cfg { }

TransactionAspectSupport.currentTransactionInfo(); // info
TransactionAspectSupport.currentTransactionStatus().hashCode() // transaction id

// 1 TransactionTemplate, with result
PlatformTransactionManager txManager;
TransactionTemplate txTemplate = new TransactionTemplate(transactionManager);
txTemplate.setIsolationLevel(TransactionDefinition.ISOLATION_REPEATABLE_READ); // config
txTemplate.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRES_NEW);
txTemplate.setTimeout(1000);
txTemplate.setReadOnly(true);
Long id = transactionTemplate.execute(status -> {
        Payment payment = new Payment().setAmount(1000L);
        entityManager.persist(payment);
        return payment.getId();
});
// 2 TransactionTemplate, without result
transactionTemplate.execute(new TransactionCallbackWithoutResult() {
    @Override
    protected void doInTransactionWithoutResult(TransactionStatus status) {
        Payment payment = new Payment().setAmount(1000L);
        entityManager.persist(payment);
    }
});
// 3 PlatformTransactionManager
DefaultTransactionDefinition definition = new DefaultTransactionDefinition();
definition.setIsolationLevel(TransactionDefinition.ISOLATION_REPEATABLE_READ);
definition.setTimeout(3);
TransactionStatus status = transactionManager.getTransaction(definition);
try {
    Payment payment = new Payment().setAmount(1000L);
    entityManager.persist(payment);
    transactionManager.commit(status);
} catch (Exception ex) {
    transactionManager.rollback(status);
}
```

### TransactionalEventListener
Источник: [1](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/transaction/support/TransactionSynchronization.html#beforeCommit-boolean-)

Привязывает событие к транзакции. Можно отследить события выполнения транзакции и выполнить что-то before/after.

**Note!** Если внутри `@TransactionalEventListener` вызывается другая транзакция, то она **должна** быть отмечена **REQUIRES_NEW** т.к. на самом деле текущая транзакция еще не отпустила ресурсы и если выполнить чтение данных, то он выполнится в текущей транзакции даже после `afterCommit/afterCompletion`!  
Это касается и событий транзакций перехваченных вручную через `TransactionSynchronizationManager`.

**Для чего:** e.g. выполнить отправку **JMS** после успешной **transaction**.

- `HibernateListener` vs `TransactionalEventListener` - чтобы отследить `AFTER_COMMIT` на уровне **Spring** можно использовать `@TransactionalEventListener`. При этом использовать `HibernateListener` в `Entity` куда делать **inject** слоя `Service`s, `Mapper`s etc - неправильно.
- `@TransactionalEventListener` vs `TransactionSynchronizationManager` - для создания `@TransactionalEventListener` нужно создать вручную его custom `Event` (**e.g.** `SaveUserEvent`) и делать `publishEvent(event)` вручную, а для `TransactionSynchronizationManager` это можно пропустить.
- **Список событий `@TransactionalEventListener`**
    ```
    AFTER_COMMIT (default setting) - specialization of AFTER_COMPLETION, used when transaction has successfully committed
    AFTER_ROLLBACK - specialization of AFTER_COMPLETION, used when transaction has rolled back
    AFTER_COMPLETION - used when transaction has completed (regardless the success)
    BEFORE_COMMIT - used before transaction commit
    ```

```java
// 1. @TransactionalEventListener
class A {
    @Autowired ApplicationEventPublisher applicationEventPublisher;

    public void save() {
        repository.save(new User())
        applicationEventPublisher.publishEvent(new SaveUserEvent(data)); // публикуем event
    }

    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
    public void handleCustom(SaveUserEvent event) { doSmth(); } // событие AFTER_COMMIT после commit транзакции
}

// 2. TransactionSynchronizationManager
// альтернатива HibernateListener,
// срабатывает уже после commit плюс необходим когда нужно Inject services & mappers
// (т.к. в Entity это делать нельзя или неправильно)
void f1() {
    TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
        @Override
        public void afterCommit() { doSmth(); }
    });
}
```

## Spring Async
Source: [1](https://www.baeldung.com/spring-async), [2](https://spring.io/guides/gs/async-method/)

* [Async](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/scheduling/annotation/Async.html)
* [AsyncResult](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/scheduling/annotation/AsyncResult.html) - to return Async result, we can use `CompletableFuture.completedFuture(Object)` instead
* [SimpleAsyncTaskExecutor](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/task/SimpleAsyncTaskExecutor.html) - default Executor
  * by default, Не переиспользует thread, количество threads не ограничено
* [SyncTaskExecutor](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/core/task/SyncTaskExecutor.html) - для синхронных задач без concurrency, например если нужно сделать **enable/disable** concurrency, то можно подставлять этот **executor**
  * Часто используется в тестах

```java
@EnableAsync // enable, false by default in Spring Boot
@Configuration
public class SpringAsyncConfig implements AsyncConfigurer {

    @Bean(name = "threadPoolTaskExecutor") // We CAN use name with THE SAME TYPE Executor!
    public Executor threadPoolTaskExecutor() {
        return new ThreadPoolTaskExecutor();
    }

    @Override // change DEFAULT @Async executor globally with AsyncConfigurer
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        threadPoolTaskExecutor.initialize();
        return threadPoolTaskExecutor;
    }

    // Exception Handler, like try-catch for Future.get() but for @Async
    @Override // custom AsyncUncaughtExceptionHandler
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return new CustomAsyncExceptionHandler() {
            @Override
            public void handleUncaughtException(
            Throwable throwable, Method method, Object... obj) {
                System.out.println("Exception message - " + throwable.getMessage());
                System.out.println("Method name - " + method.getName());
                for (Object param : obj) { System.out.println("Parameter value - " + param); }
            }
        };
    }
}

@Async // default Spring Executor
public void asyncMethodWithVoidReturnType() {   }

@Async("threadPoolTaskExecutor") // custom executor
public void asyncMethodWithVoidReturnType() {   }

@Async("syncTaskExecutor") // single thread executor, для тестов или чтобы отключить async
public void asyncMethodWithVoidReturnType() {   }

@Async // return Future
public Future<String> asyncMethodWithReturnType() {
    return new AsyncResult<String>("hello world !!!!");
}

@Async // return CompletableFuture
public CompletableFuture<String> findUser(String usernameString) throws InterruptedException {
    return CompletableFuture.completedFuture("blabla");
}
```

## Spring Expression Language (SpEL)
```java
${...} is the property placeholder syntax. It can only be used to dereference properties.
#{...} is SpEL syntax, which is far more capable and complex. It can also handle property placeholders, and a lot more besides.
Both are valid, and neither is deprecated.
```

```java
#{5}
#{spitter.name}
#{songSelector.selectSong()?.toUpperCase()}     // если null, то просто не выполнится без ошибки
#{T(java.lang.Math).PI}   // T() возвращает Class и можно использовать его методы
#{circle.radius ^ 2} // степень
#{admin.email matches '[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.com'}     // регулярки
#{cities[2]}
#{cities['Dallas']} // из Map
#{settings['twitter.accessToken']}  // properties из <util:properties id="settings" location="classpath:settings.properties"/>
#{systemEnvironment['HOME']}    // системная переменная
#{systemProperties['application.home']} // параметры запущенной JVM из командной строки
#{cities.?[population gt 100000]}   // создает новую коллекцию из элементов проходящих по условию
#{cities.^[population gt 100000]}   // первый элемент из выборки
#{cities.$[population gt 100000]}   // последний элемент из выборки
<property name="cityNames" value="#{cities.![name]}"/>  // массив свойств с именем name будут присвоены переменной cityNames
<property name="cityNames" value="#{cities.![name + ', ' + state]}"/>   // тоже только группа:  «Chicago, IL», «Atlanta, GA» и «Dallas, TX»
<property name="cityNames" value="#{cities.?[population gt 100000].![name + ', ' + state]}"/>   // можно объединить 2е операции
```

## Интернациализация
**Note.** Нужно дополнить.

файл конфигурации интернациализации по умолчанию `src/main/resources/messages.properties`

Добавить бин конфигов **CookieLocaleResolver**, чтобы локаль сохранялась в куки и передавалась на сервер.
```java
@Bean
MessageSource messageSource() {
    ResourceBundleMessageSource messageSource = new ResourceBundleMessageSource();
    messageSource.setBasename("messages");
    return messageSource;
}
```

## Resource из Spring
Прим. не разобрал тему, дописать

**Resource из Spring** - это то что можно вытащить из ServletContext или classpath для обычных сервлетов, низкоуровневый доступ из Spring к сервлетам
(напр. InputStreamReader для загрузки/выгрузки файла?)

`interface ResourceLoaderAware` обеспечивает объект ссылкой на `ResourceLoader`

Можно получить: `ResourceLoader.getResource(String location)`

1. UrlResource - является оберткой для java.net.URL
2. ClassPathResource - представляет ресурс, который может быть получен из classpath и поддерживает ресурсы как java.io.File
3. FileSystemResource - реализация для обработки java.io.File
4. ServletContextResource - реализация для обработки ServletContext ресурсов относительно корневой директории web-приложения.
5. InputStreamResource - реализация для обработки InputStream
6. ByteArrayResource - реализация для обработки массива байтов

## Поиск зависимостей в Spring, файл `spring.factories`
`src/main/resources/META-INF/spring.factories` - файл с путем к конфигам Spring **AutoConfiguration** `com.my.MyConfig`, при подключении jar с этим файлом конфиги сами включаются в контекст.  
Можно добавить `spring.factories` в модули maven, **AutoConfiguration** импортируется из модуля
```
org.springframework.boot.autoconfigure.EnableAutoConfiguration=\
org.springframework.boot.autoconfigure.admin.AConfiguration,\
org.springframework.boot.autoconfigure.admin.BConfiguration

org.springframework.boot.env.EnvironmentPostProcessor=\
org.springframework.cloud.kubernetes.profile.KubernetesProfileEnvironmentPostProcessor
```

## Utils classes in Spring
Source: [1](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/util)

**org.springframework.util** - various spring `Utils` classes

* **ClassUtils** - reflection api методы, в том числе для работы с **proxy bean**, например чтобы вернуть настоящие имя класса, а не имя класса-прокси
    ```java
    // MyBean proxy name - MyBean$213213
    // MyBean UserClass name - MyBean
    ClassUtils.getUserClass(MyBean.getClass()).getSimpleName();
    ```
* **TypeUtils** - reflection api методы
* **ReflectionUtils** - reflection api методы
* **StringUtils** - методы для работы со String
* **ObjectUtils** - аналог Objects из Java, но с доп. методами
  * unwrapOptional(optional)
  * isArray(arr)
  * isEmpty(arr)
  * containsElement(arr, el)
  * addObjectToArray(arr, el)
  * isCheckedException
  * caseInsensitiveValueOf(enumValues, enumValue)
  * nullSafeEquals(o1, o2)
  * nullSafeHashCode(obj)
* **CollectionUtils** - аналог CollectionUtils из Apache Common
* **StreamUtils** - методы для i/o stream
* **NumberUtils**
* **Base64Utils**
* **MimeTypeUtils**
* **DigestUtils**
* **FileSystemUtils**
* **FileCopyUtils**
* **ResourceUtils** - methods for `org.springframework.core.io.Resource`
* **SerializationUtils**
* **SystemPropertyUtils**
* **SocketUtils**
* **DataUnit**
* **colletions**
  * **LinkedMultiValueMap**
  * **LinkedCaseInsensitiveMap**

# Spring DI

## CDI это Contexts and Dependency Injection
пусто

## Конфликт связывания
**Note.** Под **конфликтом связывания** бинов понимается, когда или **типы бинов совпадают**, или связывание происходит с полем **ссылкой типа общего предка** (и тогда Spring не знает что выбрать). Чтобы решить такие конфликты используется: `@Autowired` вместе с `@Qualifier`, `@Primary` для связывания **по типу** или связывание **по имени** с `@Autowired` и именем поля как у класса.

## FactoryBean
Есть два типа бинов в Spring bean container, обычные бины и **factory beans**. **factory beans** могут создаваться сами, а не автоматически Spring фреймворком. Создать такие бины можно реализуя `org.springframework.beans.factory.FactoryBean`. **Используется** чтобы инкапсулировать сложную логику создания объекта. 

FactoryBean был придуман во времена xml конфигураций, чтобы можно было управлять созданием бинов. Считается что при JavaConfig использовать это нет необходимости.

```java
// интерфейс
public interface FactoryBean {
    T getObject() throws Exception;
    Class<?> getObjectType();
    boolean isSingleton();
}

// пример
public class ToolFactory implements FactoryBean<Tool> {
    @Override
    public Tool getObject() throws Exception {
        return new Tool(toolId);
    }
 
    @Override
    public Class<?> getObjectType() {
        return Tool.class;
    }
 
    @Override
    public boolean isSingleton() {
        return false;
    }
}
```

**Регистрируем через xml**
```xml
<beans ...>
    <bean id="tool" class="com.baeldung.factorybean.ToolFactory">
        <property name="factoryId" value="9090"/>
        <property name="toolId" value="1"/>
    </bean>
</beans>
```

**Регистрируем через аннотации**
```java
@Configuration
public class FactoryBeanAppConfig {
    @Bean(name = "tool")
    public ToolFactory toolFactory() {
        ToolFactory factory = new ToolFactory();
        factory.setFactoryId(7070);
        factory.setToolId(2);
        return factory;
    }
 
    @Bean
    public Tool tool() throws Exception {
        return toolFactory().getObject();
    }
}
```
Если нужно исполнить какие-то действия до `getObject()`, но после `FactoryBean`. Тогда нужно использовать `InitializingBean` или `@PostConstruct`.

**AbstractFactoryBean** - более удобный класс для реализации FactoryBean
```java
public class NonSingleToolFactory extends AbstractFactoryBean<Tool> {
    public NonSingleToolFactory() {
        setSingleton(false);
    }
 
    @Override
    public Class<?> getObjectType() {
        return Tool.class;
    }
 
    @Override
    protected Tool createInstance() throws Exception {
        return new Tool(toolId);
    }
}
```
Регистрация **AbstractFactoryBean**
```xml
<beans ...>
    <bean id="nonSingleTool" class="com.baeldung.factorybean.NonSingleToolFactory">
    </bean>
</beans>
```

## `@Autowired` in Abstract Classes
`@Autowired` для **abstract** классов **работает** с setter, **рекомендуется** сделать этот setter **final** (чтобы в наследниках не изменилось поведение). `@Autowired` **не работает** для constructor, но можно **наследовать** этот abstract класс и использовать `@Autowired` и `super(myBean)` в нем.

abstract класс не component-scanned, его нужно наследовать чтобы использовать (т.е. над ним не нужно ставить `@Component`)

**Note.** Т.е. если есть класс наследуемый классами `@Component`, то его нужно сделать **abstract** чтобы Spring не пытался сделать inject обьектов в его конструктор.

## Autowiring of Generic Types
Начиная с версии 4 используется ResolvableType, инкапсулирует java типы, чтобы потом обработать, возвращает обьект Class нужного класса.

```java
public abstract class Vehicle {}
public class Car extends Vehicle {}
public class Motorcycle extends Vehicle {}
```
1. Старый способ
```java
// 1. Делаем аннотацию
@Target({ElementType.FIELD, ElementType.METHOD, ElementType.TYPE, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Qualifier
public @interface CarQualifier {
}
// 2. Используем в 2х местах: 1) то что связывать и то 2) с чем
@Autowired
@CarQualifier // 1) сюда попадут только cars (хотя тип Vehicle)
private List<Vehicle> vehicles;

@Autowired
@CarQualifier
List<Object> beans; // 1) можно так, любой тип 

public class CustomConfiguration {
    @Bean
    @CarQualifier // 2) вот это попадет в List<Vehicle>
    public Car getMercedes() {}
}
```
2. Начиная с Spring 4.0
```java
@Autowired
private List<Vehicle> vehicles; // можно использовать в качестве параметра тип самого бина
```

## Spring Component Scanning, исключает отдельный bean из AutoConfiguration
**`@SpringBootApplication`** == `@Configuration` + `@EnableAutoConfiguration` + `@ComponentScan`
```java
@SpringBootApplication
public class SpringBootComponentScanApp {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(SpringBootComponentScanApp.class, args);
    }
}
```
Скан с фильтром
```java
// исключаем группу beans
@ComponentScan(excludeFilters = 
  @ComponentScan.Filter(type=FilterType.REGEX,
    pattern="com\\.baeldung\\.componentscan\\.springapp\\.flowers\\..*"))
// исключаем отдельный bean
@ComponentScan(basePackages = {"com.example"}, excludeFilters={
  @ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE, value=Foo.class)})
public class MySpringConfiguration {}
```

## Injecting Collections
```java
// List
public class CollectionsBean {
    @Autowired
    private List<String> nameList;
}
@Configuration
public class CollectionConfig {
    @Bean
    public List<String> nameList() {
        return Arrays.asList("John", "Adam", "Harry");
    }
}

// Map
public class CollectionsBean {
    private Map<Integer, String> nameMap;
    @Autowired
    public void setNameMap(Map<Integer, String> nameMap) {
        this.nameMap = nameMap;
    }
}
@Bean
public Map<Integer, String> nameMap() {
    Map<Integer, String>  nameMap = new HashMap<>();
    nameMap.put(1, "John");
    nameMap.put(2, "Adam");
    nameMap.put(3, "Harry");
    return nameMap;
}
```

Использование пустой коллекции by default (т.е. эту переменную потом используем)
```java
@Value("${names.list:}#{T(java.util.Collections).emptyList()}")
private List<String> nameListWithDefaultValue;
```

## Injecting Prototype Beans into a Singleton Instance
1. Через **ApplicationContext**. Но это нарушает принципы **IoC** и использует фичи Spring
    ```java
    public class SingletonAppContextBean implements ApplicationContextAware {
        private ApplicationContext applicationContext;
        public PrototypeBean getPrototypeBean() {
            return applicationContext.getBean(PrototypeBean.class);
        }
        @Override
        public void setApplicationContext(ApplicationContext applicationContext) 
        throws BeansException {
            this.applicationContext = applicationContext;
        }
    }
    ```
2. Получать **prototype** бин через вызов метода отмеченного `@Lookup`, использует CGLIB, см. пример выше в списке аннотаций
3. **javax.inject API** используя **Provider**
    ```java
    public class SingletonProviderBean {
        @Autowired
        private Provider<MyPrototypeBean> myPrototypeBeanProvider;
        public MyPrototypeBean getPrototypeInstance() {
            return myPrototypeBeanProvider.get();
        }
    }
    ```
4. **Scoped Proxy. by default Spring** имеет ссылку на реальный обьект чтобы делать injection, способ ниже указывает создавать **proxy** чтобы связать реальный обьект с зависимым (прим. уточнить инфу)
    ```java
    @Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE, 
    proxyMode = ScopedProxyMode.TARGET_CLASS)
    ```
5. **ObjectFactory Interface**, можно использовать чтобы создавать бины, это часть Spring и поэтому не нужны дополнительные шаги по конфигам
    ```java
    public class SingletonObjectFactoryBean {
        @Autowired
        private ObjectFactory<PrototypeBean> prototypeBeanObjectFactory;
        public PrototypeBean getPrototypeInstance() {
            return prototypeBeanObjectFactory.getObject();
        }
    }
    ```
6. **Create a Bean at Runtime Using java.util.Function**. Инжектим beanFactory (т.е. функцию) и используем его для создания бинов
    ```java
    public class SingletonFunctionBean {
        @Autowired
        private Function<String, PrototypeBean> beanFactory;
        public PrototypeBean getPrototypeInstance(String name) {
            PrototypeBean bean = beanFactory.apply(name);
            return bean;
        }
    }
    ```

## ScopedProxyMode
Изменение **ScopedProxyMode** может решить проблему inject бина с более узким scope в бин с более широким scope. Т.е. например сделать **inject бина `prototype` в бин `singleton`** путем inject прокси этого бина.  
**Note.** более узкий scope возможно можно назвать как более короткое время жизни бина с этим scope.

**Скоупы:**
* `ScopedProxyMode.NO` (default)
* `ScopedProxyMode.TARGET_CLASS` - используется CGLIB для создания proxy бина
* `ScopedProxyMode.INTERFACES` - используется JDK dynamic proxy для создания прокси бина (т.е. видимо прокси создастся только для public методов)

```java
@Bean
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE,
proxyMode = ScopedProxyMode.TARGET_CLASS)
public MyPrototypeBean prototypeBean () {
    return new MyPrototypeBean();
}
```

## custom scope, scopeName
пусто

## Circular Dependency, `BeanCurrentlyInCreationException`
Это когда бины одновременно и зависимости и зависимые (Bean A → Bean B → Bean A → ...). При **constructor injection** в этом случае появится `BeanCurrentlyInCreationException`, при остальных типах injection ошибок не будет, т.к. при **constructor injection** внедрение бина случается во время context loading, а в других случаях во время обращения.

Решения:
1. Правильно спроектировать приложение, тогда проблем не будет, но не во всех случаях то возможно: старый код, нет доступа к части кода.
2. Использовать `@Lazy`, тогда будет создан proxy и бин будет создан только при первом обращении к нему
    ```java
    @Autowired
    public CircularDependencyA(@Lazy CircularDependencyB circB) {
        this.circB = circB;
    }
    ```
3. Использовать **Setter/Field Injection**
4. Установить недостающие зависимости в `@PostConstruct`
    ```java
    @PostConstruct
    public void init() {
        circB.setCircA(this);
    }
    ```
5. Использовать `ApplicationContextAware` и `InitializingBean`: получить `ApplicationContext` в `ApplicationContextAware`, вытащить из него бин методом `context.getBean(MyBean.class)` и установить зависимость в методе `afterPropertiesSet()`
    ```java
    @Override
    public void afterPropertiesSet() throws Exception {
        circB = context.getBean(CircularDependencyB.class);
    }
    @Override
    public void setApplicationContext(final ApplicationContext ctx) throws BeansException {
        context = ctx;
    }
    ```

**Note.** SonarLint (и другие анализаторы) можгут ругаться на **circular dependencies** даже если в Spring оно работает, ругается просто на плохой код. И еще circular dependencies может быть не связано с inject зависимостей в самом Spring, а просто быть случаем когда Class1 зависит от Class2, а Class2 зависит от Class1 (точнее их объекты), например они передаются в конструкторы друг другу. Наприме даже если при использовании `@Autowired` для fields в Spring все работает, но SonarLint всеравно может ругаться на это как на circular dependencies.

## Внедрение сразу всех бинов определенного типа которые есть в приложении в коллекцию
Можно внедрить все созданные бины приложения определенного типа в коллекцию, например если имя не известно.
```java
@Autowired // или @Inject, или @Resource
private List<Fine> fine;
```

## Создание своего варианта `@Qualifier`
**Суть:** создаем свою аннотацию и используем в ней обычный `@Qualifier`, но зависимость и место куда ее внедрить свяжутся только если имя **нашей** аннотации и там, и там будет проставлено.  
**Для чего:** чтобы устранить неодназначность, альтернатива использование связывания `byName`

```java
@Qualifier
@Retention(RUNTIME)
@Target({METHOD, FIELD, PARAMETER, TYPE})
public @interface FineDayQualifier {}

@FineDayQualifier
public class FineDay implements Fine {}

@Component
public class AmbiguousInjectFine {
    @Inject
    @FineDayQualifier
    private Fine fine;
}
```

## static методы помеченные `@Bean`
see in annotation list & bean lite mode

## static классы помеченные `@Configuration`
see in annotation list & bean lite mode

## `@Import` классов с factory методами помеченными `@Bean` при том что эти классы не помечены `@Configuration`
see in annotation list & bean lite mode

## `BeanDefinitionOverrideException`
Источник: [1](https://www.baeldung.com/spring-boot-bean-definition-override-exception)

По умолчанию если есть несколько beans с одинаковым именем будет ошибка `BeanDefinitionOverrideException`  
**Note.** В **Spring 1** эта опция **by default** была **true**  
**Note.** Такое может быть если bean например из библиотеки
```properties
# позволяем иметь 2+ бинов с одинаковым именем, использоваться будет все равно только 1ый
spring.main.allow-bean-definition-overriding=true
```

## `@RequestScope` vs `ThreadLocal`
`@RequestScope` **bean** создается только при **request**, если **bean** содержит данные которые должны обрабатываться в `@Service` по `@KafkaListener` или `@JmsListener`, то bean `@RequestScope` не создастся и нужно использовать **ThreadLocal** (который можно хранить в `@Component` как в container).  
**Note.** В **tomcat** не создается новый **thread**, а переиспользуется из **thread pool**, поэтому **ThreadLocal** сохранит состояние между **request**, нужно предусматривать механизм **очистки** или делать **custom scope**.
```java
@Component class MyContainer { ThreadLocal<MyData> data = new ThreadLocal<>(); }
```

# Spring AOP
## Common
Источник: [1](https://www.baeldung.com/spring-aop), [2](https://www.baeldung.com/spring-aop-annotation), [3](https://www.baeldung.com/spring-aop-get-advised-method-info), [4](https://www.baeldung.com/spring-aop-pointcut-tutorial), [5](https://www.baeldung.com/spring-aop-advice-tutorial), [6](https://www.baeldung.com/spring-aop-vs-aspectj), [7](https://www.baeldung.com/cdi-interceptor-vs-spring-aspectj)

**AOP** - разбиение программы на модули применимые во многих местах

**Spring AOP** - proxy-based фреймворк

**Как работает:** через proxy объект. Если для объекта нужно AOP. То Spring создает для него proxy и возвращает его вместо объекта. Этот proxy может выполнить Advice перед выполнением метода целевого объекта. Поэтому у Spring AOP есть ограничения и он может быть применен только в контексте.

**Ограничения в Spring AOP:** аспекты не применяются к другим аспектам.

**Понятия Spring AOP:**
* **weaving** - вставка аспекта в точку кода.
    * Может быть при: компиляции, выполнении, во время загрузки класса load time weaving (LTW) для AspectJ
    (Spring AOP работает только для method invocation типа)
* **introduction** - внедрение
* **target** - изменяемый объект

**Типы weaving в AOP:**
1. **compile time** (AspectJ compiler) - вызовы создаются на этапе компиляции, это дает вызов функции прокси даже если его делает другая функция прокси из того же класса (в отличии от случая с **runtime**)
2. **load time** (AspectJ compiler)
3. **runtime** (CGlib или JDK dynamic proxy) - прокси паттерн в который обернуты **вызовы** функций, особенность: если вызвана функция прокси объекта и внутри нее вызвана другая функция прокси объекта, то эта другая функция не вызовется, т.к. вызов будет происходить внутри прокси и следовательно другая функция вызовется из самого объекта, а не прокси и обернута в AOP не будет (**это следствие из самого паттерна proxy**)

**Типы проксирования для AOP:** (через что создается proxy объект вокруг target)
* **JDK dynamic proxy** (по умолчанию в старых версиях Spring) - работает если объект реализовывает хотя бы 1 интерфейс у объекта, прокси создается на основе используемых объектом интерфесов. Могут перекрывать только public методы. Проксирует только **public** методы.
* **CGLib proxy** (по умолчанию в новых версиях Spring) - если интерфесы не реализовываются. Проксирует **public**, **protected** и **package** методы. Если мы явно не указали для среза (pointcut) ограничение **только для public методов**, то потенциально можем получить неожиданное поведение.  
    **Note.** Когда-то даже при использовании CGLIB не работало AOP для **protected** и **package**, но в новых версия Spring работает (но не для **private**).

**Spring AOP** - использует **runtime weaving**.

**Понятия классов и средств:**
1. **`@Aspect`** - класс который будет применен (т.е. его методы)
    (класс со сквазной функциональностью)
    * **Note:** `@Aspect` внутри себя содержит `@Pointcut`
2. **`@Pointcut`** - где будет примен родительский ему `@Aspect`,
    содержит pattern == класс + методы к которым будет применен аспект
    (к каким пакетам и методам будет применен). Как **predicate** для проверки того стоит ли сюда применять.
    * **Join Point** - конкретное место (**метод** или **Exception Handler**) для которого будет выполняется `@Aspect`
      * **Note.** В Spring AOP тип всегда **method execution**, для **Exception Handler** нету.
    * **Note:** Join Point это конкретное место, а `@Pointcut` это набор таких мест
3. **Advice** - код аспекта, который будет выполняться в местах подключения
    указывает и КОГДА будет выполняться код (after, before, ...)

**Типы advices:** `@Before`, `@After`, `@Around` == `@Before` + `@After`, `@AfterReturning`, `@AfterThrowing`

**Внедрений бинов создастся столько сколько instances бинов создано** (для singleton 1 раз, для prototype много)
<br>
Сам Proxy ничего не вызывает, он содержит цепочка interceptors.

Не зависимо от того, попадает или нет каждый конкретный метод целевого объекта под действие аспекта, его вызов проходит через прокси-объект.
<br>
(**т.е.** AOP будет действовать на все методы цели-класса даже если в аспекте указан только 1ин метод)

Будет создан как минимум один инстанс класса аспекта. (этим можно управлять)

**Настройка**
```xml
<!-- подключение -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-aop</artifactId>
</dependency>
```
```java
@Aspect // методы этого класса будут применены
@Component
class A {
    @Pointcut("execution(* *.f2(..))") // к чему применить
    private void f1(){} // должен быть void???
}

@EnableAspectJAutoProxy // включаем AOP, в Spring Boot включиться
@Configuration // вместо @Component в Aspect, если Aspect в library при подключении
class MoveSortToSpecificationConfig {
    @Bean MyAspect myAspect() { return new MyAspect(); }
}

// @DeclarePrecedence, possibly it from AspectJ and maybe it is supported in Spring AOP
@Aspect
@DeclarePrecedence("MyAspect*") // precedence symbols, you can use a lot of rules here
public class MyAspect {
    @Pointcut("execution(* *.f2(..))") void f1(){} // MyAspect + f2(..)
}
```

* **pointcut expression language** - выражения из `@Pointcut`
* **pointcut signature** - метод из `@Pointcut`
* **pointcut designator** (PCD) - к чему применить (method, type, annotation, arguments)
  * **execution()** - by method
  * **within()** - by class type
  * **this()** - by class interface (ancestor of the class, like instanceof)
  * **target()** - by the same exact class
  * **args()** - by args type and length
  * **`@target()`** - the same but for annotation
  * **`@args()`** - the same but for annotation
  * **`@within()`** - the same but for annotation
  * **`@annotation()`** - by exact annotation
    ```java
    @Pointcut("execution(public String com.baeldung.pointcutadvice.dao.FooDao.findById(Long))")
    @Pointcut("execution(* com.baeldung.pointcutadvice.dao.FooDao.*(..))")
    // * com. - любой тип в return
    // FooDao.* - любой метод
    // (..) - любые аргументы, любое количество

    @Pointcut("within(com.baeldung.pointcutadvice.dao.FooDao)")
    @Pointcut("within(com.baeldung..*)")
    // .FooDao - все методы которые return FooDao
    // baeldung..* - все метод которые return типа из любого sub package

    @Pointcut("this(com.baeldung.pointcutadvice.dao.FooDao)") // класс FooDao и все его наследники
    @Pointcut("target(com.baeldung.pointcutadvice.dao.BarDao)") // класс FooDao

    @Pointcut("execution(* *..find*(Long))") // args
    @Pointcut("execution(* *..find*(Long,..))") // args
    @Pointcut("com.xyz.myapp.SystemArchitecture.dataAccessOperation() && args(account,..)") // args
    @After("@args(myAnnotation, ..) && @args(Entity, ..)") // аннотации которыми помечены args?
    public void myAfterAdvice(JoinPoint thisJoinPoint, Entity entity) {
        log.info("After " + thisJoinPoint + " -> " + entity);
        log.info("Accepting beans with @Entity annotation: " + jp.getArgs()[0]);
    }

    // pointcut в качестве выражения
    @Pointcut("execution(@com.stackoverflow.MyAnnotation * *(..))")
    protected void myPointcut() {}

    @AfterReturning(pointcut = "myPointcut() && args(someId,..)") // аргумент someId передается ниже
    public void afterSuccessfulReturn(JoinPoint joinPoint, Integer someId) {
        // do smth
    }

    @Pointcut("execution(public !static * *(..))") // public, если указано явно то package и protected не будет
    void f1(){}

    @Pointcut("execution(public !static * *(..))") // static
    void f1(){}
    ```

**Combining Pointcut Expressions** - можно использовать `&&, || and !` operators
```java
@Pointcut("@target(org.springframework.stereotype.Repository)")
public void repositoryMethods() {}

@Pointcut("repositoryMethods() && execution(* *..create*(Long,..))")
public void entityCreationMethods() {}
```

**Advice Types**
```java
@Component
@Aspect
public class LoggingAspect {
    // Note. Изменить args в Before/After нельзя, только в Around.

    // 1. Before
    @Before("repositoryMethods()")
    public void logMethodCall(JoinPoint jp) {
        Method method = jp.getSignature();
    }

    // 2. AfterReturning
    @AfterReturning(value = "entityCreationMethods()", returning = "entity")
    public void logMethodCall(JoinPoint jp, Object entity) throws Throwable {
        eventPublisher.publishEvent(new FooCreationEvent(entity));
    }

    // 3. AfterThrowing
    @AfterThrowing(PointCut = "execution(* com.tutorialspoint.Student.*(..))", throwing = "error")
    public void afterThrowingAdvice(JoinPoint jp, Throwable error){ }

    // 4. After - выполняется после AfterReturning и AfterThrowing
    // Note. Несколько After привязанные к одному PointCut выполняются по порядку (по цепочке)
    @After(value = "execution(* com.javatpoint.service.EmployeeService.*(..)) and args(empId, fname, sname)")  
    public void afterAdvice(JoinPoint joinPoint, String empId, String fname, String sname) { }

    // 5. Around
    @Around("within(@org.springframework.stereotype.Repository *)")
    public Object measureMethodExecutionTime(ProceedingJoinPoint pjp) throws Throwable {
        Object[] args = pjp.getArgs(); // do smth before
        Object retVal = joinPoint.proceed(args); // can change args
        // do smth after
        return retVal; // @Around обязательно вернуть, иначе значение может быть потеряно
    }
}
```

## AOP order
Source: [1](https://docs.spring.io/spring-framework/docs/current/reference/html/core.html#aop-ataspectj-advice-ordering)
```java
// annotation order is not the same as aspect order! Aspect2 MAY exec before Aspect1 here
// 2 aspects над 1им и тем же методом сработают по очереди и не помешают паттерну proxy, это особенность Spring, хотя порядок выполнения не гарантирован он сработает
class A { @Aspect1 @Aspect2 f1() { } }

@Order(1) public class Aspect1 { } // 1. set order on creation

@Order(1) @Component class Cfg { // 2. set order on its config creation
    @Bean Aspect1 aspect1() { return new Aspect1(); }
}
```
```
в Spring AOP и в AspectJ по умолчанию стоит order - Ordered.LOWEST_PRECEDENCE (Integer.MAX_VALUE)

Если 2+ AOP advice привязаны к 1ой join point:
highest precedence - runs first for before advice
highest precedence - runs last for after advice

Если несколько advice объявлены в 1ом и том же AOP классе (e.g. 2+ @After), то Order - undefined
Документация рекомендует: или объединить 2+ @After в 1, или перенести их в разные классы (и назначить Order).

Чтобы управлять Order динамически можно implements org.springframework.core.Ordered.getOrder()

Порядок по умолчанию для типов AOP в порядке перечисления: highest to lowest precedence: @Around, @Before, @After, @AfterReturning, @AfterThrowing

Note. @After будет всегда вызван после @AfterReturning or @AfterThrowing

Для транзакций
@EnableTransactionManagement(order = Ordered.HIGHEST_PRECEDENCE)
```

## JDK Dynamic Proxy vs CGLIB
Source: [1](https://medium.com/@jalil-se/cglib-vs-jdk-proxies-in-spring-boot-and-which-one-to-use-and-when-fd8a00bd04f4), [2](https://www.kapresoft.com/java/2023/12/28/java-proxy-vs-cglib.html)

* CGLIB - lib которая внедряет через bytecode generation. Быстрее когда делается много вызовов за счет. спец. байт кода. Может сделать proxy из любого class кроме final class
  * Note. В целом CGLIB работает быстрее, но потребляем больше памяти.
* JDK proxies - используется reflection, создает proxy по interface который использует target class. Быстрее когда делается несколько взовов. Может сделать proxy только если у class есть interface с его methods.

```java
// JDK Dynamic Proxy
public class LoggingHandler implements InvocationHandler {
    private final Object target;

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("Method " + method.getName() + " is called with args: " + Arrays.toString(args));
        return method.invoke(target, args);
    }

    public static <T> T createProxy(T target, Class<T> interfaceType) {
        return (T) Proxy.newProxyInstance(
                interfaceType.getClassLoader(),
                new Class<?>[]{interfaceType},
                new LoggingHandler(target));
    }
}

// CGLIB
public class ProfilingInterceptor implements MethodInterceptor {
    @Override
    public Object intercept(Object obj, Method method, Object[] args, MethodProxy proxy) throws Throwable {
        long start = System.nanoTime();
        Object result = proxy.invokeSuper(obj, args);
        long elapsedTime = System.nanoTime() - start;
        System.out.println("Execution of " + method.getName() + " took " + elapsedTime + " nanoseconds.");
        return result;
    }

    public static <T> T createProxy(Class<T> clazz) {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(clazz);
        enhancer.setCallback(new ProfilingInterceptor());
        return (T) enhancer.create();
    }
}
```

## AspectJ
AspectJ можно подключить в дополнение.

## Self Injection. Если нам все-таки надо добиться, что бы в случае Spring AOP код аспекта выполнялся при вызове proxy метода из другого proxy метода
**Варианты:**
1. **refactoring** - надо писать код, так что бы обращения проходили через прокси-объект (В документации написано, что это нерекомендуемое решение).
2. **self injection** - заинжектить сервис сам в себя (`@Autowired` private MyServiceImpl myService;) и использовать метод myService.
   * (ТОЛЬКО для scope = singleton, для prototype вызовет БЕСКОНЕЧНО внедрение зависимости и приложенка не запустится)
   * **Cons.** 1) Can confuse. 2) Can cause **circular dependency**

## Пример обертки аннотации в свое AOP
```java
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface Repository { }

@Aspect
@Component
public class PerformanceAspect {
    @Pointcut("within(@org.springframework.stereotype.Repository *)")
    public void repositoryClassMethods() {};
 
    @Around("repositoryClassMethods()")
    public Object measureMethodExecutionTime(ProceedingJoinPoint joinPoint) throws Throwable {
        long start = System.nanoTime();
        Object returnValue = joinPoint.proceed();
        long end = System.nanoTime();
        String methodName = joinPoint.getSignature().getName();
        System.out.println(
          "Execution of " + methodName + " took " + 
          TimeUnit.NANOSECONDS.toMillis(end - start) + " ms");
        return returnValue;
    }
}
```

## Method Info in Spring AOP
Источник: [1](https://www.baeldung.com/spring-aop-get-advised-method-info)
```java
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface AccountOperation {
    String operation();
}

@Aspect
@Component
public class BankAccountAspect {
    @Before(value = "@annotation(com.baeldung.method.info.AccountOperation)")
    public void getAccountOperationInfo(JoinPoint joinPoint) {
        // Method Information
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();

        log("full method description: " + signature.getMethod());
        log("method name: " + signature.getMethod().getName());
        log("declaring type: " + signature.getDeclaringType());

        // Method args
        log("Method args names:");
        Arrays.stream(signature.getParameterNames()).forEach(s -> log("arg name: " + s));

        log("Method args types:");
        Arrays.stream(signature.getParameterTypes()).forEach(s -> log("arg type: " + s));

        log("Method args values:");
        Arrays.stream(joinPoint.getArgs()).forEach(o -> log("arg value: " + o.toString()));

        // Additional Information
        log("returning type: " + signature.getReturnType());
        log("method modifier: " + Modifier.toString(signature.getModifiers()));
        Arrays.stream(signature.getExceptionTypes()).forEach(aClass -> log("exception type: " + aClass));

        // Method annotation
        Method method = signature.getMethod();
        AccountOperation accountOperation = method.getAnnotation(AccountOperation.class);
        log("Account operation annotation: " + accountOperation);
        log("Account operation value: " + accountOperation.operation());
    }
}
```

# Misc

## Logging
Source: [1](https://docs.spring.io/spring-boot/docs/2.1.7.RELEASE/reference/html/boot-features-logging.html), [2](https://www.baeldung.com/spring-boot-logging)

## Как вызвать destroy для prototype бинов
1. `BeanFactoryAware` - получить в его методе бин и проверить BeanFactory.isPrototype, использовать в BeanPostProcessor ниже
2. `DisposableBean` - реализуем тут destroy() который мы вызовем для уничтожения каждого бина
3. `BeanPostProcessor` - в его наследнике можно создать List всех prototype и пройтись по ним уничтожив
**ПОЯСНЕНИЕ**: мы наследуем ВСЕ эти интерфесы своему классу, который будет отслеживать prototype бины, добавлять в List и вызывать для каждого destroy() в цикле.

**Пример:**
```java
@Component
public class DestroyPrototypeBeansPostProcessor implements BeanPostProcessor, BeanFactoryAware, DisposableBean {
    private BeanFactory beanFactory;
    private final List<Object> prototypeBeans = new LinkedList<>();
    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException { }
    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException { }
    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException { }
    @Override
    public void destroy() throws Exception { synchronized (prototypeBeans) { for(;;) { prototypeBean.destroy(); } } }
}
```

## Get Spring Application Context in static method
```java
// 1 Variant. With provider. (most reliable)
@Component
public class MyApplicationContextProvider implements ApplicationContextAware {

    public static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        MyApplicationContextProvider.applicationContext = applicationContext;
    }
}

// 2 Variant. Only if Request was made! With Request Context in Spring MVC. (not good)
public static ApplicationContext getAppContextFromRequest() {
    ServletContext servletContext = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest().getServletContext();
    return WebApplicationContextUtils.getWebApplicationContext(servletContext);
}
```

## Deprecated классы
- **WebMvcConfigurerAdapter** заменен на **WebMvcConfigurer**

# Exception handle
Source: [1](https://www.baeldung.com/exception-handling-for-rest-with-spring)

Суть: специальный aspect Spring MVC перехватывает все ошибки RuntimeException и подменяет response на кастомный. Есть и др. способы определить обработчик ошибок, переопределяя спец. интерфейсы.

```java
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(value = { IllegalArgumentException.class, IllegalStateException.class })
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        return handleExceptionInternal(ex, "error", new HttpHeaders(), HttpStatus.CONFLICT, request);
    }
}
```

# Reloading Properties bean (property который можно перезагружать динамически)
https://www.baeldung.com/spring-reloading-properties

## Отключение авто конфигурации Spring
**Для чего:** чтобы создать свою конфигурацию и не создавался лишний Bean конфигурации в **Spring Context**, который может случайно подставляться (`autowiring`) в то место, где нужна своя конфигурация (в зависимости от приоритета Bean). Плюс в **Spring Context** не будет лишнего Bean.

Через аннотации
```java
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, HibernateJpaAutoConfiguration.class})
```

Через **application.properties**
```properties
spring.autoconfigure.exclude=org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration, org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration
```

Авто конфигурация включается по `@ConditionOn` условию.

## Способы обработки exceptions
https://www.baeldung.com/exception-handling-for-rest-with-spring

## Описать `HandlerMethodArgumentResolver`
https://www.baeldung.com/spring-mvc-custom-data-binder

```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Version {
}

// 1. 
public class HeaderVersionArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(Version.class) != null;
    }

    @Override
    public Object resolveArgument(
      MethodParameter methodParameter, 
      ModelAndViewContainer modelAndViewContainer, 
      NativeWebRequest nativeWebRequest, 
      WebDataBinderFactory webDataBinderFactory) throws Exception {
 
        HttpServletRequest request 
          = (HttpServletRequest) nativeWebRequest.getNativeRequest();

        return request.getHeader("Version");
        // return resolveArgument(param, mav, req, binder, new ArrayList<HandlerMethodArgumentResolver>()); // для рекурсивной
    }

    // вариант с рекурсивной обработкой параметров метода из net.kaczmarzyk.spring.data.jpa.web
    Object resolveArgument(MethodParameter param, ModelAndViewContainer mav, NativeWebRequest req,
            WebDataBinderFactory binder, List<HandlerMethodArgumentResolver> recursiveCallers) throws Exception {
	    for (HandlerMethodArgumentResolver delegate : delegates) {
            if (!recursiveCallers.contains(delegate) && delegate.supportsParameter(param)) {
            	if (delegate instanceof RecursiveHandlerMethodArgumentResolver) {
            		return ((RecursiveHandlerMethodArgumentResolver) delegate).resolveArgument(param, mav, req, binder, recursiveCallers);
            	} else {
            		return delegate.resolveArgument(param, mav, req, binder);
            	}
            }
        }
        return null;
	}
}

// 2.
@Configuration
public static class Config extends WebMvcConfigurerAdapter {
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new HeaderVersionArgumentResolver());
    }
}
```

## `data.sql` и `schema.sql` и `@Sql`
https://www.baeldung.com/spring-boot-data-sql-and-schema-sql

# Spring Modules

## Spring MVC
### Common
**Путь запроса Spring MVC:**  
   * **браузер** > DispatcherServlet (сервлет, еще называется front controller, подключен e.g. в `web.xml` или в др.) >  
   * **Controller** (возвращает имя view прикрепленное к model, в model можно сетать данные) >  
   * **ViewResolver** (сюда request доставляет model из Controller) >  
   * **Сформированный view** (страницу jsp или др.) и упаковать ее с model (шаблонизировать)

**View** класс - обертка вокруг шаблонизатора добавляющая в страницу ссылки на служебные бины + model (bean, cookie, request etc)

**Resolvers:**
* **CommonsMultipartResolver** (или др. реализация MultipartResolver) - extends ViewResolver, нужно для upload файлов
* Сам класс **MultipartResolver**, а не сторонние реализации доступен с Servlet 3.0. Его можно объявить как `@Bean`:
  * `<bean id="multipartResolver" class="org.springframework.web.multipart.support.StandardServletMultipartResolver"></bean>`
  
**Для отправки файла использовать:**
```html
<form method="post" action="/form" enctype="multipart/form-data">
    <input type="file" name="file"/>
</form>
```
И перехват (для **StandardServletMultipartResolver** из Spring):
```java
@RequestMapping(value="/someUrl", method = RequestMethod.POST)
public String onSubmit(@RequestPart("meta-data") MetaData metadata, @RequestPart("file-data") MultipartFile file) {}
```
И перехват (для **javax.servlet.http.Part** из Java EE):
```java
@RequestMapping(value = "/form", method = RequestMethod.POST)
public String handleFormUpload(@RequestParam("name") String name, @RequestParam("file") Part file) {}
```

**HandlerMapping** - наследники этого класса использует **DispatcherServlet**
чтобы решить к какому Controller (и методу) отправить запрос

**HandlerMapping vs ViewResolver**
    - HandlerMapping привязывает к ссылке контроллер,
        а ViewResolver привязывает View (страницу)

**Пример 1:**
```java
@Bean("/welcome")
public BeanNameHandlerMappingController beanNameHandlerMapping() {
    return new BeanNameHandlerMappingController();
}
```
**Пример 2 (более полный конфиг): см. ниже**  
**Пример 3 (конфиги путей через Adapter, ВОЗМОЖНО не HandlerMapping):**
```java
@Configuration
@EnableWebMvc
public class AppConfig implements WebMvcConfigurer{
    @Override
    public void configurePathMatch(PathMatchConfigurer configurer) {
        configurer.setUseSuffixPatternMatch(false);
    }
}
```

### Configuration
**Порядок шагов включения:**
1. добавить (`@EnableWebMvc` устанваливает DispatcherServlet):
    ```java
    @EnableWebMvc
    class WebSecurityConfig extends WebSecurityConfigurerAdapter {}
    ```
2. Выбрать ViewResolver - соотв. url и шаблонов (путь к файлу), в том числе для BlaBlaMulrtipartBla для upload бинарных файлов
3. Выбрать View подходящий к шаблонизатору
4. `@EnableSpringDataWebSupport` включает Spring HATEOAS
5. Включить транзакции:
    1. `@EnableTransactionManagement`
    2. `@Bean PlatformTransactionManager transactionManager(){} // конфиги транзакции`
    3. `@Bean sessionFactory() // из hibernate, если нужно для transactionManager`
    4. `@Bean DataSource dataSource(){}`
6. Над методами `@Controller` поставить `@RequestMapping` или аналог `@PostMapping`/`@GetMapping`/etc
7. Для проверки на ошибки можно использовать Hibernate Validator:
    ```java
    String addSpitterFromForm(@Valid Spitter spitter, BindingResult bindingResult) {
        if(bindingResult.hasErrors()) { // Проверка ошибок
            return "spitters/edit";
        }
        // Сохранить объект Spitter
        spitterService.saveSpitter(spitter);
        // Переадресовать
        return "redirect:/spitters/" + spitter.getUsername(); 
    }
    ```

**Note:** можно вернуть: `new ResponseEntity(mRs, HttpStatus.OK)`
    - где первый параметр это Entity, а второй это status запроса

**Аннотации:**
1. `@ModelAttribute` - доступ к элементу который УЖЕ в model в `@Controller`
    1. НАД методом `@Controller` и тогда return значение попадает в model
        * `@ModelAttribute("vehicle") Vehicle getVehicle() {}`
    1. Перед ПАРАМЕТРОМ метода `@Controller`, если у него кастомное имя
        * `void post(@ModelAttribute("vehicle") Vehicle vehicleInModel) {}`
1. `@CrossOrigin` - для настройки CORS

**ModelAndView** использует ModelMap, которая использует Map.
<br>
Этот объект передается во View и генерирует ключи Map сам, на основе имени добавленного объекта.

```java
// объявление статических ресурсов
@Configuration
@EnableWebMvc
public class WebConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/resources/**").addResourceLocations("/public-resources/").setCachePeriod(31556926);
    }
    @Override // аналог ParameterizableViewController
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("home");
    }
}

// установка view resolver через ViewResolverRegistry
@Configuration
@EnableWebMvc
public class MvcWebConfig implements WebMvcConfigurer {
    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        registry.jsp("/WEB-INF/views/", ".jsp");
    }
}

// ParameterizableViewController (создание @Controller вручную)
@Configuration
@SpringBootApplication
public class Main {
    @Bean
    public ParameterizableViewController myViewController () {
        ParameterizableViewController c = new ParameterizableViewController();
        c.setViewName("myView");
        c.setStatusCode(HttpStatus.OK);
        return c;
    }
    
    @Bean
    public HandlerMapping myHandlerMapping () {
        SimpleUrlHandlerMapping m = new SimpleUrlHandlerMapping();
        Map<String, Object> map = new HashMap<>();
        map.put("/test", myViewController());
        m.setUrlMap(map);
        m.setOrder(1);
        return m;
    }
}

// создание ViewResolver вручную
@Configuration
public class MvcConfiguration {
    @Bean
    public InternalResourceViewResolver getViewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/");
        viewResolver.setSuffix(".jsp");
        return viewResolver;
    }
}
```

**HttpMessageConverter** - его наследники конвертят body запроса в параметры сервлета (из и в).
<br>
Напр. MappingJackson2HttpMessageConverter для Jackson

```java
@Configuration
@EnableWebMvc
public class ApplicationConfig extends WebMvcConfigurerAdapter { 
 @Override
 public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
    // конфиги всех converters
 } 
}
```

### Controller response
**ResponseEntity** - контейнер для response, содержит status, headers, body

```java
package org.springframework.http;
public class ResponseEntity<T> extends HttpEntity<T> {
	public ResponseEntity(HttpStatus status);
	public ResponseEntity(@Nullable T body, HttpStatus status);
	public ResponseEntity(MultiValueMap<String, String> headers, HttpStatus status);
	public ResponseEntity(@Nullable T body, @Nullable MultiValueMap<String, String> headers, HttpStatus status);
}

// 1. ResponseEntity builder
@PostMapping("test")
public ResponseEntity<?> save(@RequestBody Map<?,?> payload) {
    if(payload.isEmpty())
        return ResponseEntity.noContent().build();
    else
        return ResponseEntity.ok().body(testService.save(payload));
}

// 2. ResponseEntity constructor
@PostMapping
@ResponseStatus(HttpStatus.CREATED)
public ResponseEntity<Order> createEntity(@RequestBody MyDto dto) {
    return new ResponseEntity<>(dto, HttpStatus.OK);
}

// 3. empty body
@PostMapping
public ResponseEntity<?> update(@RequestBody BatchUpdateDto batchUpdateDto) {
    clientVipUpdateService.update(batchUpdateDto);
    return new ResponseEntity<>(HttpStatus.OK);
}
```

### Spring MVC Interceptor
**Spring MVC Interceptor** - это аналог Servlet Filter

**Методы: preHandle(), postHandle(), afterCompletion()**
<br>

**Для реализации нужно:**
1. extends HandlerInterceptor
2. ИЛИ implements HandlerInterceptorAdapter

**Методы:**
* **preHandle()** - вернет true / false (передать запрос дальше или нет)
* **postHandle(..., Model model)** - последний параметр model из view
* **afterCompletion()** - выполняется после всего в том числе работы view

**1. Пример создания Interceptor**
```java
@Component
public class ProductServiceInterceptor implements HandlerInterceptor {
   @Override
   public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
      return true;
   }
}
```

**2. Пример регистрации Interceptor:**
```java
@Configuration
@EnableWebMvc
public class WebMvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new AdminInterceptor())
            .addPathPatterns("/admin/*")
            .excludePathPatterns("/admin/oldLogin");
    }
}
```

### Обычные Filter из java ee в Spring
Кроме Interceptor в Spring Boot можно регистрировать обычные фильтры

```java
@Component
@Order(1)
public class MyFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, 
      ServletResponse response, 
      FilterChain chain) throws IOException, ServletException {}
}
```

```java
// регистрируем
@Bean
public FilterRegistrationBean<MyFilter> loggingFilter(){
    var registrationBean = new FilterRegistrationBean<>();
    registrationBean.setFilter(new MyFilter());
    registrationBean.addUrlPatterns("/users/*");
         
    return registrationBean; 
}
```

```java
// старый способ регистрации в Spring (ИЛИ через web.xml)
public class MyWebInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
	@Override
	protected Filter[] getServletFilters() {
		return new Filter[]{new ErrorHandleFilter()};
	}
}
```

### Custom Data Binder in Spring MVC
Источник: [1](https://www.baeldung.com/spring-mvc-custom-data-binder)

**Data Binder** - избавляют от ручного парсинга данных

**Типы Data Binder:**
1. **Converter** - для обычного типа
2. **ConverterFactory** - для объектов (Object tree, с общим предком), тип конвертера выбирается сам в зависимости от объекта
3. **HandlerMethodArgumentResolver** - парсер срабатывает до методов Controller и результат вставляется в argument метода Controller (e.g. если argument помечен какой-то аннотацией)

1) **Converter** - Конвертирует параметры запроса
```java
public enum Modes { ALPHA, BETA; }
// создаем конвернет (e.g. конвертер в Enum)
public class StringToEnumConverter implements Converter<String, Modes> {
    @Override
    public Modes convert(String source) {
        return Modes.valueOf(source.toUpperCase());
    }
}
// регистрируем
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverter(new StringToEnumConverter());
    }
}
// используем
@GetMapping("/entity/findbymode/{mode}")
public GenericEntity findByEnum(@PathVariable("mode") Modes mode) {
    // ...
}
```
2) **ConverterFactory** - Конвертирует Object tree, объекты request с общим предком
```java
public abstract class AbstractEntity { long id; } // общий предок

// конвертер factory для всех от общего предка
public class StringToAbstractEntityConverterFactory implements ConverterFactory<String, AbstractEntity>{
    @Override
    public <T extends AbstractEntity> Converter<String, T> getConverter(Class<T> targetClass) {
        return new StringToAbstractEntityConverter<>(targetClass);
    }

    private static class StringToAbstractEntityConverter<T extends AbstractEntity>
      implements Converter<String, T> { // конвертер создается каждый раз для наследников

        private Class<T> targetClass;

        public StringToAbstractEntityConverter(Class<T> targetClass) {
            this.targetClass = targetClass;
        }

        @Override
        public T convert(String source) {
            long id = Long.parseLong(source);
            if(this.targetClass == Foo.class) {
                return (T) new Foo(id);
            }
            else if(this.targetClass == Bar.class) {
                return (T) new Bar(id);
            } else {
                return null;
            }
        }
    }
}

// подключаем
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Override
    public void addFormatters(FormatterRegistry registry) {
        registry.addConverterFactory(new StringToAbstractEntityConverterFactory());
    }
}

public class Bar extends AbstractEntity { int value; } // наследник

// используем
@RestController
@RequestMapping("/string-to-abstract")
public class AbstractEntityController {
    @GetMapping("/bar/{bar}")
    public ResponseEntity<Object> getStringToBar(@PathVariable Bar bar) {
        return ResponseEntity.ok(bar);
    }
}
```

1) **HandlerMethodArgumentResolver** - сами парсим из запроса данные и вставляем в аргументы метода
```java
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface Version { }

// создаем HandlerMethodArgumentResolver
public class HeaderVersionArgumentResolver implements HandlerMethodArgumentResolver {
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(Version.class) != null;
    }

    @Override
    public Object resolveArgument(MethodParameter methodParameter, 
      ModelAndViewContainer modelAndViewContainer, 
      NativeWebRequest nativeWebRequest, 
      WebDataBinderFactory webDataBinderFactory) throws Exception {
        HttpServletRequest request = (HttpServletRequest) nativeWebRequest.getNativeRequest();

        return request.getHeader("Version"); // вытаскиваем данные в arg с аннотацией
    }
}

// подключаем
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new HeaderVersionArgumentResolver());
    }
}

// используем
@GetMapping("/entity/{id}")
public ResponseEntity findByVersion(@PathVariable Long id, @Version String version) {
    // return ...;
}
```

### ArgumentResolvers
Источник: [1](https://www.baeldung.com/spring-mvc-custom-data-binder)

Меняют параметры запроса, как фильтры обрабатывающие запрос до попадания в контроллер.
```java
// создаем Resolver
public class HeaderVersionArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterAnnotation(Version.class) != null;
    }

    @Override
    public Object resolveArgument(
      MethodParameter methodParameter, 
      ModelAndViewContainer modelAndViewContainer, 
      NativeWebRequest nativeWebRequest, 
      WebDataBinderFactory webDataBinderFactory) throws Exception {
        HttpServletRequest request = (HttpServletRequest) nativeWebRequest.getNativeRequest();

        return request.getHeader("Version");
    }
}

// подключаем Resolver
@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void addArgumentResolvers(
      List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(new HeaderVersionArgumentResolver());
    }

    // устанавливаем какие типы параметров поддерживаем (возвращаем для них true)
    @Override
    public boolean supportsParameter(MethodParameter methodParameter) {
        return methodParameter.getParameterType().equals(String.class);
    }
}

// используем
@GetMapping("/entity/{id}")
public ResponseEntity findByVersion(@PathVariable Long id, @Version String version) { return ...; }
```

## RestTemplate vs WebClient
Source: [1](https://www.baeldung.com/rest-template), [2](https://habr.com/ru/company/otus/blog/541404/)  
Use sources to rewrite part with WebClient!

Работа с HTTP запросами.  
**RestTemplate** условно deprecated (рекомендовано использовать **WebClient**). **WebClient** в отличии от **RestTemplate** - может рабоать в 2х режимах **synchronous and blocking**, можно использовать **WebClient** в режиме `.block()` как **RestTemplate**.

**RestTemplate** - использует внутри JDK HttpURLConnection, Apache HttpComponents and other. Кроме основных есть generalized методы exchange and execute (больше параметров).
```java
@Autowired RestTemplate restTemplate;
ObjectMapper mapper = new ObjectMapper(); // Jackson lib

// String, no parsing fail when error
ResponseEntity<String> response = restTemplate.getForEntity(url + "/1", String.class);
String result = response.getStatus(); // OK, NOT_FOUND etc
String result = response.getBody(); // {name: bob}

JsonNode root = mapper.readTree(response.getBody()); // parse manually
JsonNode name = root.path("name");

// DTO
MyDto dto = restTemplate.getForObject(url + "/1", MyDto.class);

// headers
HttpHeaders httpHeaders = restTemplate.headForHeaders(fooResourceUrl);
assertTrue(httpHeaders.getContentType().includes(MediaType.APPLICATION_JSON));

// postForObject()
HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
Foo foo = restTemplate.postForObject(url, request, Foo.class);

// put
restTemplate.put(url + /1, myDto, MyDto.class); // returns void

// postForLocation() 
HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
URI location = restTemplate.postForLocation(url, request);

// delete()
restTemplate.delete(entityUrl);

// exchange()
HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
ResponseEntity<Foo> response = restTemplate.exchange(url, HttpMethod.POST, request, Foo.class);

// exchange() - put
Foo dto = new Foo("newName");
dto.setId(1);
HttpEntity<Foo> requestUpdate = new HttpEntity<>(dto, headers);
template.exchange(url, HttpMethod.PUT, requestUpdate, Void.class);

// exchange() - callback
restTemplate.execute(url, HttpMethod.PUT, requestCallback(updatedDto), clientHttpResponse -> null);

// Submit Form Data
HttpHeaders headers = new HttpHeaders();
headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
headers.setContentType(HttpHeaders.AUTHORIZATION, "Basic " + getBase64EncodedLogPass())
MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
map.add("id", "1");
HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);
ResponseEntity<String> response = restTemplate.postForEntity(url+"/form", request , String.class);

// Use OPTIONS to Get Allowed Operations
Set<HttpMethod> optionsForAllow = restTemplate.optionsForAllow(url);
HttpMethod[] supportedMethods = {HttpMethod.GET, HttpMethod.POST, HttpMethod.PUT, HttpMethod.DELETE};
assertTrue(optionsForAllow.containsAll(Arrays.asList(supportedMethods)));

// Configure Timeout
RestTemplate restTemplate = new RestTemplate(getClientHttpRequestFactory());

private ClientHttpRequestFactory getClientHttpRequestFactory() {
    int timeout = 5000;
    RequestConfig config = RequestConfig.custom()
      .setConnectTimeout(timeout)
      .setConnectionRequestTimeout(timeout)
      .setSocketTimeout(timeout)
      .build();
    CloseableHttpClient client = HttpClientBuilder
      .create()
      .setDefaultRequestConfig(config)
      .build();
    return new HttpComponentsClientHttpRequestFactory(client);
}
```

**WebClient**  
Supports: get(), post(), put(), patch(), delete(), options() и head()

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-webflux</artifactId>
</dependency>
```
```java
HttpClient httpClient = HttpClient.create() // JDK
  .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 5000)
  .responseTimeout(Duration.ofMillis(5000))
  .doOnConnected(conn -> 
    conn.addHandlerLast(new ReadTimeoutHandler(5000, TimeUnit.MILLISECONDS))
      .addHandlerLast(new WriteTimeoutHandler(5000, TimeUnit.MILLISECONDS)));

WebClient client = WebClient.builder()
    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
    .clientConnector(new ReactorClientHttpConnector(httpClient)) // use HttpClient
    .baseUrl("http://localhost:8080")
    .defaultCookie("cookieKey", "cookieValue")
    .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE) 
    .defaultUriVariables(Collections.singletonMap("url", "http://localhost:8080"))
    .retryWhen(Retry.fixedDelay(3, Duration.ofMillis(100))) // retry
    .build();

// 1. Create Spec
ResponseSpec responseSpec = headersSpec.header(
    HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE)
  .accept(MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML)
  .acceptCharset(StandardCharsets.UTF_8)
  .ifNoneMatch("*")
  .ifModifiedSince(ZonedDateTime.now())
  .retrieve();

// 2. use Spec
Mono<String> response = headersSpec.exchangeToMono(response -> {
  if (response.statusCode()
    .equals(HttpStatus.OK)) {
      return response.bodyToMono(String.class);
  } else if (response.statusCode()
    .is4xxClientError()) {
      return Mono.just("Error response");
  } else {
      return response.createException()
        .flatMap(Mono::error);
  }
});

// block
Mono<ClientResponse> request = WebClient.create().get().uri(builder -> builder.scheme("http")
        .host("localhost").port(8080).path("concatenate")
        .contentType(MediaType.APPLICATION_JSON)
        .header("clientUniqueId", "")
        .header("language", "English")
        .queryParam("str1", str1)
        .queryParam("str2", str2)
        .retrieve()
            .onStatus(HttpStatus::is4xxClientError,
                    error -> Mono.error(new RuntimeException("API not found")))
            .onStatus(HttpStatus::is5xxServerError,
                    error -> Mono.error(new RuntimeException("Server is not responding")))
        .doOnError(error -> log.error("An error has occurred {}", error.getMessage())) // error handler
        .onErrorResume(error -> Mono.just(new User())) // fallback
        .build()).accept(MediaType.TEXT_PLAIN).exchange();
String result = request.flatMap(res -> res.bodyToMono(String.class)).block();
request.subscribe(System.out::println); // non block

// Multiple Calls
public Mono fetchUserAndItem(int userId, int itemId) {
    Mono user = getUser(userId);
    Mono item = getItem(itemId);

    return Mono.zip(user, item, (user, item -> { doSmth(user, item)}));
}
```

## Spring Scheduler
Source: [1](https://www.baeldung.com/spring-scheduled-tasks), [2](https://www.baeldung.com/shedlock-spring), [3](https://www.baeldung.com/spring-quartz-schedule)

**2 типа Scheduler в Spring**
1. **Spring Scheduler** - встроенный
2. **ShedLock** - подключаемая lib которая предотвращает параллельный запуск делая lock в спец. табл общий для всех instances базы данных. Например если Scheduler может сработать на нескольких pods одновременно. **Note.** ShedLock предполагает что часы на всех pods синхронизированы.
3. **Quartz Scheduler** - подключаемый, больше фич, встроенный аналог ShedLock

```java
// 1. просто задаем интервал
@Configuration @EnableScheduling
class Cfg {
    // @Autowired т.к. Thread создатся не при запуске и constructor inject не сработает
    @Autowired MyService myService;
    ┌───────────── second (0-59)
    │ ┌───────────── minute (0 - 59)
    │ │ ┌───────────── hour (0 - 23)
    │ │ │ ┌───────────── day of the month (1 - 31)
    │ │ │ │ ┌───────────── month (1 - 12) (or JAN-DEC)
    │ │ │ │ │ ┌───────────── day of the week (0 - 7)
    │ │ │ │ │ │          (or MON-SUN -- 0 or 7 is Sunday)
    │ │ │ │ │ │
    * * * * * *
    @Scheduled(
        cron = "0 * * * * *",
        cron = "@hourly", // @yearly, @monthly, @weekly, @daily
        initialDelay = 2000, // первое выполнение
        fixedDelay = 2000, // 
        fixedRate = 3000 // 
        fixedDelayString = "PT02S", // в ISO
        fixedRateString = "PT02S" // в ISO
    )
    @Async // без этого выполнение будет только после завершения текущего не смотря на delay
    public void fixedDelay() throws InterruptedException {
    }
}

// 2. Установить интервал в runtime невозможно, для этого есть SchedulingConfigurer
@Configuration
@EnableScheduling
public class DynamicSchedulingConfig implements SchedulingConfigurer {
    @Autowired TickService tickService;
    @Bean Executor taskExecutor() { return Executors.newSingleThreadScheduledExecutor(); }

    // перед каждым выполнением метод Trigger вощвращает новую установленную задержку
    @Override
    public void configureTasks(ScheduledTaskRegistrar taskRegistrar) {
        taskRegistrar.setScheduler(taskExecutor());
        taskRegistrar.addTriggerTask(
          new Runnable() { @Override public void run() { tickService.tick(); } },
          new Trigger() {
              @Override
              public Date nextExecutionTime(TriggerContext context) {
                  Optional<Date> lastCompletionTime =
                    Optional.ofNullable(context.lastCompletionTime());
                  Instant nextExecutionTime =
                    lastCompletionTime.orElseGet(Date::new).toInstant()
                      .plusMillis(tickService.getDelay());
                  return Date.from(nextExecutionTime);
              }
          }
        );
    }
}

// 3. by default Spring использует local single-threaded scheduler (SingleThreadScheduledExecutor) и запускает только 1 таску за раз, чтобы было несколько нужно обьявить custom TaskScheduler
// в Spring Boot это делается: spring.task.scheduling.pool.size=5
@Bean
public TaskScheduler  taskScheduler() {
    ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
    threadPoolTaskScheduler.setPoolSize(5);
    threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler");
    return threadPoolTaskScheduler;
}

// 4. для НЕ параллельного запуска на несколькоих копиях приложения

// 4.1 подключаем
// <dependency>
//     <groupId>net.javacrumbs.shedlock</groupId>
//     <artifactId>shedlock-spring</artifactId>
//     <version>4.27.0</version>
// </dependency>

// <dependency>
//   <groupId>net.javacrumbs.shedlock</groupId>
//   <artifactId>shedlock-provider-jdbc-template</artifactId>
//   <version>4.27.0</version>
// </dependency>

// 4.2 конфиг
@Configuration
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "10m")
@EnableAsync
public class SchedulerConfig {
    @Scheduled(cron = "0 0/15 * * * ?")
    @SchedulerLock(name = "TaskScheduler_scheduledTask", lockAtLeastFor = "PT5M", lockAtMostFor = "PT14M")
    @Bean LockProvider lockProvider(DataSource dataSource) {
        return new JdbcTemplateLockProvider(dataSource);
    }
}
```


## Spring Retry
Source: [baeldung](https://www.baeldung.com/spring-retry)

**RetryTemplate** - модуль, авто retry, e.g. при лагах сети. Есть конфиги: annotation, RetryTemplate, callbacks.

**ExhaustedRetryException** - если **max** количества попыток retry

```xml
<dependency>
    <groupId>org.springframework.retry</groupId>
    <artifactId>spring-retry</artifactId>
    <version>2.0.0</version>
</dependency>
```
```java
@Configuration
@EnableRetry // 1. enable
public class AppConfig {}

@Retryable // by default, all exceptions, 3 times, 1s - delay
void retry(String sql);

@Retryable(retryFor = SQLException.class) // for 1 SQLException exception only
void retry(String sql) throws SQLException;

@Recover
void recover(SQLException e, String sql);  // call after MAX attempt, 3 by default

@Retryable(retryFor = SQLException.class, maxAttempts = 2, backoff = @Backoff(delay = 100))
void retry(String sql) throws SQLException;

// RetryTemplate
@Bean
public RetryTemplate retryTemplate() {
    RetryTemplate retryTemplate = new RetryTemplate();
    
    FixedBackOffPolicy fixedBackOffPolicy = new FixedBackOffPolicy(); // backoff
    fixedBackOffPolicy.setBackOffPeriod(2000l);
    retryTemplate.setBackOffPolicy(fixedBackOffPolicy);

    SimpleRetryPolicy retryPolicy = new SimpleRetryPolicy(); // retry
    retryPolicy.setMaxAttempts(2);
    retryTemplate.setRetryPolicy(retryPolicy);

    retryTemplate.registerListener(new DefaultListenerSupport()); // add custom listener, see below
    
    return retryTemplate;
}

retryTemplate.execute(new RetryCallback<Void, RuntimeException>() {
    @Override
    public Void doWithRetry(RetryContext arg0) {
        myService.templateRetryService();
    }
});

// builder
RetryTemplate template = RetryTemplate.builder()
				.maxAttempts(3)
				.fixedBackoff(1000)
				.retryOn(RemoteServiceNotAvailableException.class)
				.build();

// Adding Callbacks, open - start retry, close - end retry
public class DefaultListenerSupport extends RetryListenerSupport {
    @Override
    public <T, E extends Throwable> void close(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        logger.info("onClose");
        super.close(context, callback, throwable);
    }

    @Override
    public <T, E extends Throwable> void onError(RetryContext context, RetryCallback<T, E> callback, Throwable throwable) {
        logger.info("onError"); 
        super.onError(context, callback, throwable);
    }

    @Override
    public <T, E extends Throwable> boolean open(RetryContext context, RetryCallback<T, E> callback) {
        logger.info("onOpen");
        return super.open(context, callback);
    }
}
```

# Spring Security
## Common
SecurityContext сделан через ThreadLocal

Добавить `@EnableWebSecurity class Cfg extends WebSecurityConfigurerAdapter {}` альтернатива: подключить и настроить:
```xml
<filter-name>springSecurityFilterChain</filter-name>
<filter-class>org.springframework.web.filter.DelegatingFilterProxy</filter-class>
```

`@EnableGlobalMethodSecurity(prePostEnabled = true)` включение AOP для Spring Data Rest (PreAuthorize/PreFilter/etc, JSR-250)
<br>
`@EnableWebSecurity` включает http.hasRole(...) и подобное


Настройка прав доступа:
```java
protected void configure(HttpSecurity http) {
    http.csrf().disable().authorizeRequests().antMatchers("/", "/list").hasRole("ADMIN")
        .and().formLogin().successHandler(mySuccessHandler).failureHandler(myFailureHandler)
        .and().logout().anyRequest().authenticated();
}
```

**Реализовать UserDitailsService и переопределить в нем loadUserByUsername(username):**
* в нем вытащить: user = usrRepository.get(username)
* и установить алгоритм хэширования: builder.password(new BCryptPasswordEncoder().encode(user.getPassword()));

**Пример (прим. возможно как вариант можно просто создать `@Bean` UserDetailsService):**
```java
@Override
protected void configure(AuthenticationManagerBuilder auth)
throws Exception {
    auth.authenticationProvider(authenticationProvider()); // регистрируем DaoAuthenticationProvider
}

@Bean
public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
    authProvider.setUserDetailsService(userDetailsService); // добавляем userDetailsService
    authProvider.setPasswordEncoder(encoder());
    return authProvider;
}
```

**GrantedAuthority** это тоже что и Role, его часто используют чтобы взять role (`Set<GrantedAuthorities> roles = userDetails.getAuthorities()` )

**Взять данные о user (др. назв. principle):**
```java
Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
String currentPrincipalName = authentication.getName();
UserDetails userDetails = (UserDetails) authentication.getPrincipal(); // берем все данные
SecurityContextHolder.getContext().getAuthentication(); // из контекста
Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
// можно инжектить в метод Controller:
@ResponseBody public String currentUserName(@Param() String param, Authentication authentication) {}
```

**Проверка прав (аннотации для методов сервиса или контроллера):**
* `@PostFilter/@PreFilter` - допускает вызов метода, но фильтрует результат (напр отдадим админу только сообщение с матами)
* `@PreAuthorize("hasRole('ROLE_ADMIN')")` - можно вызвать, если указанное выражение в ней true
* `@PostAuthorize` -- можно вызвать, но если вернет false, то исключение
* `@Secured("ROLE_SPITTER")` == @RolesAllowed({"SPITTER"})
* `@PostAuthorize("returnObject.spitter.username == principal.username")` - проверка на доступа к данным только пользователя username
* `@PreAuthorize ("#book.owner == authentication.name")`

**Другой спосб:** сделать отдельный repository для работы с пользователями в Data REST (export = false), но не делать его публичным, а использовать как внутренний для других repository (как обертки для него). И в них проверять права. (ЭТО ДОГАДКА)

**Spring security настройка сессии:**
* **always** – a session will always be created if one doesn’t already exist
* **ifRequired** – a session will be created only if required (default)
* **never** – the framework will never create a session itself but it will use one if it already exists
* **stateless** – no session will be created or used by Spring Security

**Пример:**
```java
@Override
protected void configure(HttpSecurity http) {
    http
        // other config goes here...
        .sessionManagement()
            .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
            .maximumSessions(2) // max одновременных сессий
            .expiredUrl("/sessionExpired.html") // on session timeout
            .invalidSessionUrl("/invalidSession.html") // wrong session
            .sessionRegistry(sessionRegistry());
}
@Bean // бин конфигов
public SpringSessionBackedSessionRegistry<S> sessionRegistry() {
    return new SpringSessionBackedSessionRegistry<>(this.sessionRepository);
}
```

**Timeout сессии:**
1. `server.servlet.session.timeout=60s` из Spring Boot
2. другие способы сводятся к получению session из ServletContext
    и прямой установке `getSession().setMaxInactiveInterval(15);`
    или в `web.xml`:
    ```xml
    <session-config>
        <session-timeout>20</session-timeout>
    </session-config>
    ```
    получить можно:
        `implements HttpSessionListener`, и различных Handler

## Как работает Spring Security
Источники: [легко читаемый источник всего Spring Security в целом](https://ru.wikibooks.org/wiki/Spring_Security/%D0%A2%D0%B5%D1%85%D0%BD%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%B8%D0%B9_%D0%BE%D0%B1%D0%B7%D0%BE%D1%80_Spring_Security), [официальная документация](https://spring.io/guides/topicals/spring-security-architecture)

**Это Core концепция, которая используется в реализациях таких как те что для Web и основаны на `Servlet Filters`.** Основной интерфейс `AuthenticationManager`, метод `authenticate=true` если аутенцифицироован, exception если нет и null если нельзя определить. `ProviderManager implement AuthenticationManager` делегирует к цепочке состоящей из `AuthenticationProvider` (он как `AuthenticationManager`). ProviderManager может поддерживать отдновременно много разных механизмов. Причем `ProviderManager` может быть родительским для других `ProviderManager` которые тоже указывают на свои версии `AuthenticationProviders` (получатся дерево из `ProviderManager`). Когда user аутенцифицироован дальше происходит авторизация (проверка прав доступа) для этого используется `AccessDecisionManager`, где `ConfigAttribute` это SpEL такие как `hasRole('FOO')`. Чтобы создать свои методы которые можно использовать внутри аннотаций таких как `@PreFilter` нужно **extends** класс `SecurityExpressionRoot` или `SecurityExpressionHandler`.

**Web Security** - один из механизмов для Web, внутри использует `AuthenticationManager`. Основана на **Filters** из JavaEE. Путь request примерно: `User -> Filter -> Filter -> Filter -> Servlet`

```java
// примеры интерфейсов и методов из Spring Security
public interface AuthenticationManager {
  Authentication authenticate(Authentication authentication)
    throws AuthenticationException;
}

public interface AuthenticationProvider {
	Authentication authenticate(Authentication authentication)
			throws AuthenticationException;
	boolean supports(Class<? extends Authentication> authentication); // группа обьектов с разными методами authentication
}

// пример AccessDecisionManager
boolean supports(ConfigAttribute attribute);
boolean supports(Class<?> clazz);
int vote(Authentication authentication, S object,
        Collection<ConfigAttribute> attributes);
```

**Note.** При Spring AOP на этапе вызова используется паттерн proxy и вызов security метода класса другого security метода из того же класса не будет обернут в AOP (это особенность proxy паттерна).

**Note.** По умолчанию Spring SecurityContext is thread-bound, т.е. не распостраняется на дочерние Thread

Включение
```java
@Configuration
@EnableGlobalMethodSecurity(    prePostEnabled = true, // Spring Security pre/post annotations
                                securedEnabled = true, // @Secured
                                jsr250Enabled = true) // @RoleAllowed
public class MethodSecurityConfig  extends GlobalMethodSecurityConfiguration {
}

// Переопределяем UserDetailsService и его метод loadUserByUserName и в нем читаем пользователя из DB

// устанавливаем если нужно PasswordEncoder бин, например с алгоритмом BCrypt
```
Использование
```java
// returnObject
@PostAuthorize
  ("returnObject.username == authentication.principal.nickName")
public CustomUser loadUserDetail(String username) {
    return userRoleRepository.loadUserByUserName(username);
}

// filterObject и filterTarget (указывает какой аргумент фильтровать)
@PreFilter
  (value = "filterObject != authentication.principal.username",
  filterTarget = "usernames")
public String joinUsernamesAndRoles(
  List<String> usernames, List<String> roles) {
  
    return usernames.stream().collect(Collectors.joining(";")) 
      + ":" + roles.stream().collect(Collectors.joining(";"));
}

// создание custom аннотации
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@PreAuthorize("hasRole('VIEWER')")
public @interface IsViewer {
}

// исползование custom аннотации
@IsViewer
public String getUsername4() {
    //...
}
```

## Как работает filter chain ([источник](https://stackoverflow.com/questions/41480102/how-spring-security-filter-chain-works))
Фильтр Spring Security встраивается как **один** фильтр в цепочке наследующий `Filter` из JavaEE, а уже внутри него определены свои наборы фильтров из Spring Security.

**Ключевые фильтры:**
- `SecurityContextPersistenceFilter` (restores Authentication from JSESSIONID)
- `UsernamePasswordAuthenticationFilter` (performs authentication)
- `ExceptionTranslationFilter` (catch security exceptions from FilterSecurityInterceptor)
- `FilterSecurityInterceptor` (may throw authentication and authorization exceptions)

**Порядок фильтров:**
1. `ChannelProcessingFilter`, because it might need to redirect to a different protocol
2. `SecurityContextPersistenceFilter`, so a SecurityContext can be set up in the SecurityContextHolder at the beginning of a web request, and any changes to the SecurityContext can be copied to the HttpSession when the web request ends (ready for use with the next web request)
`ConcurrentSessionFilter`, because it uses the SecurityContextHolder functionality and needs to update the SessionRegistry to reflect ongoing requests from the principal
3. Authentication processing mechanisms - `UsernamePasswordAuthenticationFilter`, `CasAuthenticationFilter`, `BasicAuthenticationFilter` etc - so that the SecurityContextHolder can be modified to contain a valid Authentication request token
4. The `SecurityContextHolderAwareRequestFilter`, if you are using it to install a Spring Security aware HttpServletRequestWrapper into your servlet container
5. The `JaasApiIntegrationFilter`, if a JaasAuthenticationToken is in the SecurityContextHolder this will process the FilterChain as the Subject in the JaasAuthenticationToken
6. `RememberMeAuthenticationFilter`, so that if no earlier authentication processing mechanism updated the SecurityContextHolder, and the request presents a cookie that enables remember-me services to take place, a suitable remembered Authentication object will be put there
7. `AnonymousAuthenticationFilter`, so that if no earlier authentication processing mechanism updated the SecurityContextHolder, an anonymous Authentication object will be put there
8. `ExceptionTranslationFilter`, to catch any Spring Security exceptions so that either an HTTP error response can be returned or an appropriate AuthenticationEntryPoint can be launched
9. `FilterSecurityInterceptor`, to protect web URIs and raise exceptions when access is denied

Получаем фильтры и работаем с ними:
```java
    @Autowired private FilterChainProxy filterChainProxy; // получаем бин

    public void getSecurityFilterChainProxy(){
        for(SecurityFilterChain secfc :  this.filterChainProxy.getFilterChains()){ // в цикле получаем цепочки
            for(Filter filter : secfc.getFilters()){   }
        }
    }
```

## Custom Filter in the Spring Security Filter Chain ([источник](https://www.baeldung.com/spring-security-custom-filter))
Создается реализацией `GenericFilterBean` которая наследует `javax.servlet.Filter`, но является Spring aware.

Можно ставить до или после определенно фильтра:
- `addFilterBefore(filter, class)` – adds a filter before the position of the specified filter class
- `addFilterAfter(filter, class)` – adds a filter after the position of the specified filter class
- `addFilterAt(filter, class)` – adds a filter at the location of the specified filter class
- `addFilter(filter)` – adds a filter that must be an instance of or extend one of the filters provided by Spring Security

```java
// реализуем
public class CustomFilter extends GenericFilterBean {
    @Override
    public void doFilter(
      ServletRequest request, 
      ServletResponse response,
      FilterChain chain) throws IOException, ServletException {
        chain.doFilter(request, response);
    }
}

// регистрируем, добавлем в цепочку
@Configuration
public class CustomWebSecurityConfigurerAdapter
  extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.addFilterAfter(new CustomFilter(), BasicAuthenticationFilter.class); // место в цепочке, до, после, вместо
    }
}
```
<details>
<summary>Регистрация в xml конфигурации</summary>

```xml
<http>
    <custom-filter after="BASIC_AUTH_FILTER" ref="myFilter" />
</http>
<beans:bean id="myFilter" class="org.baeldung.security.filter.CustomFilter"/>
```
**Позиции в xml:**
- `after` – describes the filter immediately after which a custom filter will be placed in the chain
- `before` – defines the filter before which our filter should be placed in the chain
- `position` – allows replacing a standard filter in the explicit position by a custom filter
</details>

## Custom Security Expression ([источник](https://www.baeldung.com/spring-security-create-new-custom-security-expression))
Деляться на 2 типа:
1. Наследованные от готового `PermissionEvaluator` и используемые для проверки прав доступа в своих собственных выражениях, но они немного ограничены в семантике.
2. Полностью свои выражения со своей логикой, наследуются от `SecurityExpressionRoot` и `MethodSecurityExpressionOperations` (с помощью него же можно переопределить готовые выражения и всегда возвращать из них `RuntimeException`, чтобы их отключить).

**1. Пример PermissionEvaluator**
```java
// реализация PermissionEvaluator
public class CustomPermissionEvaluator implements PermissionEvaluator {
    @Override
    public boolean hasPermission(Authentication auth, Object targetDomainObject, Object permission) {}
    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String targetType, Object permission) {}
    private boolean hasPrivilege(Authentication auth, String targetType, String permission) {}
}

// регистрируем PermissionEvaluator
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        DefaultMethodSecurityExpressionHandler expressionHandler = 
          new DefaultMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(new CustomPermissionEvaluator());
        return expressionHandler;
    }
}

// примеры использования аннотация над методами @Controller или других классов
@PostAuthorize("hasAuthority('FOO_READ_PRIVILEGE')")
@PostAuthorize("hasPermission(returnObject, 'read')") // returnObject это возвращаемый методом обьект
@PreAuthorize("hasPermission(#id, 'Foo', 'read')") // #id это имя параметра метода
```

**2. Пример MethodSecurityExpressionOperations**
```java
// реализуем выражение
// реализуем MethodSecurityExpressionOperations, при этом SecurityExpressionRoot это класс содержащий
// некоторые методы, чтобы в него отправить готовый Authentication и использовать this.getPrincipal() и т.д.
public class CustomMethodSecurityExpressionRoot
    extends SecurityExpressionRoot implements MethodSecurityExpressionOperations {
    
    public CustomMethodSecurityExpressionRoot(Authentication authentication) {
        super(authentication);
    }
 
    public boolean isMember(Long OrganizationId) {
        User user = ((MyUserPrincipal) this.getPrincipal()).getUser();
        return user.getOrganization().getId().longValue() == OrganizationId.longValue();
    }
}

// создаем handler выражения
public class CustomMethodSecurityExpressionHandler 
  extends DefaultMethodSecurityExpressionHandler {
    private AuthenticationTrustResolver trustResolver = new AuthenticationTrustResolverImpl();
 
    @Override
    protected MethodSecurityExpressionOperations createSecurityExpressionRoot(
      Authentication authentication, MethodInvocation invocation) {
        CustomMethodSecurityExpressionRoot root = 
          new CustomMethodSecurityExpressionRoot(authentication);
        root.setPermissionEvaluator(getPermissionEvaluator());
        root.setTrustResolver(this.trustResolver);
        root.setRoleHierarchy(getRoleHierarchy());
        return root;
    }
}

// регистрируем handler выражения
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class MethodSecurityConfig extends GlobalMethodSecurityConfiguration {
    @Override
    protected MethodSecurityExpressionHandler createExpressionHandler() {
        CustomMethodSecurityExpressionHandler expressionHandler = 
          new CustomMethodSecurityExpressionHandler();
        expressionHandler.setPermissionEvaluator(new CustomPermissionEvaluator());
        return expressionHandler;
    }
}

// используем
@PreAuthorize("isMember(#id)") public Organization findOrgById(@PathVariable long id) {}
```
**3. Пример отключения встроенных Expression** (по сути мы просто переопределяем их и всегда возвращаем из них RuntimeException)
```java
// создаем выражение
public class MySecurityExpressionRoot implements MethodSecurityExpressionOperations {
    public MySecurityExpressionRoot(Authentication authentication) {
        if (authentication == null) {
            throw new IllegalArgumentException("Authentication object cannot be null");
        }
        this.authentication = authentication;
    }
 
    @Override
    public final boolean hasAuthority(String authority) {
        throw new RuntimeException("method hasAuthority() not allowed");
    }
}

// создаем handler и регистрируем его, как в примере выше
```

## OAUTH2 приминительно к Spring и JWT
https://www.baeldung.com/spring-security-oauth-jwt

## Annotations
**Список**
* `@PreAuthorize` vs `@Secured` - в `@PreAuthorize` можно использовать SpEL, получать доступ к свойствам `SecurityExpressionRoot`, получать доступ к параметрам метода (аналогично для `@PostAuthorize`, `@PreFilter`, `@PostFilter`)
    ```java
    @PreAuthorize("#contact.name == principal.name")
    // @PreAuthorize("hasRole('ADMIN OR hasRole('USER')")
    public void doSomething(Contact contact)

    @Secured("ROLE_ADMIN")
    void а1(){}
    ```

## AuthenticationManagerBuilder vs HttpSecurity vs WebSecurity

* configure(AuthenticationManagerBuilder) - можно добавить users и их пароли в in memory БД
* configure(HttpSecurity) - для http
* configure(WebSecurity) - глобально

# Spring Boot
## Common
Source: [1](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html)

**starter** - это пакет у которого в зависимостях другие пакеты, чтобы подключать зависимости можно было несколькими строчками

Работа Spring Boot начинается с запуска `SpringApplication`, который запускает `ApplicationContext`:
```java
@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```

**Note.** Полный список параметров **application.yml** [тут](https://docs.spring.io/spring-boot/docs/1.5.4.RELEASE/reference/html/common-application-properties.html)  
**Spring beans** с параметрами из **application.yml** создаются автоматически. Но только в одном экземляре (**e.g.** нельзя настроить несколько `TransactionManager`, есть только **1ин** такой bean). И **не все параметры** можно настроить для некоторых **spring beans**.

Вместо `exclude = MyClass.class` можно использовать в `application.yml` параметры типа `spring.datasource.initialize=false` (e.g. отключает `DataSource`)

# Spring JDBC
Source: [1](https://www.baeldung.com/spring-jdbc-jdbctemplate)

**Spring JDBC** - модуль для работы с **DB**, `JdbcTemplate`, `NamedParameterJdbcTemplate` и `SimpleJdbc...` - основные классы для работы, `DataAccessException` - **root** exception

* **org.springframework.jdbc.core** - core JDBC functionality
  * JdbcTemplate, SimpleJdbcInsert, SimpleJdbcCall and NamedParameterJdbcTemplate
* **org.springframework.jdbc.datasource**
* **org.springframework.jdbc.object**
* **org.springframework.jdbc.support**

```xml
<!-- Spring Boot -->
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-jdbc</artifactId>
</dependency>
<dependency>
    <groupId>mysql</groupId>
    <artifactId>mysql-connector-java</artifactId>
    <scope>runtime</scope>
</dependency>
```
```properties
# Spring Boot
spring.datasource.url=jdbc:mysql://localhost:3306/springjdbc
spring.datasource.username=guest_user
spring.datasource.password=guest_password
```
```java
@Bean
public DataSource mysqlDataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName("com.mysql.jdbc.Driver");
    dataSource.setUrl("jdbc:mysql://localhost:3306/springjdbc");
    dataSource.setUsername("guest_user");
    dataSource.setPassword("guest_password");
    return dataSource;
}

int result = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM EMPLOYEE", Integer.class);
int result = jdbcTemplate.update("INSERT INTO EMPLOYEE VALUES (?, ?, ?, ?)", id, "Bill", "Gates", "USA");
int result = jdbcTemplate.execute("INSERT INTO Person(first_name,last_name) VALUES('Victor', 'Hugo')");

// NamedParameterJdbcTemplate, Named Parameters, MapSqlParameterSource
SqlParameterSource namedParameters = new MapSqlParameterSource().addValue("id", 1);
namedParameterJdbcTemplate.queryForObject("SELECT FIRST_NAME FROM EMPLOYEE WHERE ID = :id", namedParameters, String.class);

// Named Parameters, BeanPropertySqlParameterSource
var employee = new Employee().setFirstName("James");
String SELECT_BY_ID = "SELECT COUNT(*) FROM EMPLOYEE WHERE FIRST_NAME = :firstName";
SqlParameterSource namedParameters = new BeanPropertySqlParameterSource(employee);
int result = namedParameterJdbcTemplate.queryForObject(SELECT_BY_ID, namedParameters, Integer.class);

// Mapping Query Results to Java Object
public class EmployeeRowMapper implements RowMapper<Employee> {
    @Override
    public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
        Employee employee = new Employee();
        employee.setId(rs.getInt("ID"));
        employee.setFirstName(rs.getString("FIRST_NAME"));
        employee.setLastName(rs.getString("LAST_NAME"));
        employee.setAddress(rs.getString("ADDRESS"));
        return employee;
    }
}
String query = "SELECT * FROM EMPLOYEE WHERE ID = ?";
Employee employee = jdbcTemplate.queryForObject(query, new Object[] { id }, new EmployeeRowMapper());

// Exception Translation
public class CustomSQLErrorCodeTranslator extends SQLErrorCodeSQLExceptionTranslator {
    @Override
    protected DataAccessException customTranslate(String task, String sql, SQLException sqlException) {
        if (sqlException.getErrorCode() == 23505) {
          return new DuplicateKeyException("Custom Exception translator - Integrity constraint violation.", sqlException);
        }
        return null;
    }
}
CustomSQLErrorCodeTranslator customSQLErrorCodeTranslator = new CustomSQLErrorCodeTranslator();
jdbcTemplate.setExceptionTranslator(customSQLErrorCodeTranslator);

// SimpleJdbcInsert
SimpleJdbcInsert simpleJdbcInsert = new SimpleJdbcInsert(dataSource)
                                        .withTableName("EMPLOYEE")
                                        .usingGeneratedKeyColumns("ID");
Map<String, Object> parameters = new HashMap<String, Object>();
parameters.put("ID", emp.getId());
parameters.put("FIRST_NAME", emp.getFirstName());
parameters.put("LAST_NAME", emp.getLastName());
parameters.put("ADDRESS", emp.getAddress());
var int = simpleJdbcInsert.execute(parameters); // insert
Number id = simpleJdbcInsert.executeAndReturnKey(parameters); // get id

// SimpleJdbcCall, Stored Procedures
SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(dataSource).withProcedureName("READ_EMPLOYEE");
SqlParameterSource in = new MapSqlParameterSource().addValue("in_id", id);
Map<String, Object> out = simpleJdbcCall.execute(in);
Employee emp = new Employee();
emp.setFirstName((String) out.get("FIRST_NAME"));
emp.setLastName((String) out.get("LAST_NAME"));

// BatchPreparedStatementSetter - batch insert
int[] results = jdbcTemplate.batchUpdate("INSERT INTO EMPLOYEE VALUES (?, ?, ?, ?)",
    new BatchPreparedStatementSetter() {
        @Override
        public void setValues(PreparedStatement ps, int i) throws SQLException {
            ps.setInt(1, employees.get(i).getId());
            ps.setString(2, employees.get(i).getFirstName());
            ps.setString(3, employees.get(i).getLastName());
            ps.setString(4, employees.get(i).getAddress();
        }
        @Override
        public int getBatchSize() {
            return 50;
        }
    });

// NamedParameterJdbcTemplate - named params batch insert
SqlParameterSource[] batch = SqlParameterSourceUtils.createBatch(employees.toArray());
int[] updateCounts = namedParameterJdbcTemplate.batchUpdate( "INSERT INTO EMPLOYEE VALUES (:id, :firstName, :lastName, :address)", batch);
return updateCounts;
```

# Spring Data
Это проект содержащий разные модули. Слой доступа к данным с шаблонным кодом. Облегчает разработку.

**Popular modules:**
* Spring Data Commons - core
* Spring Data JDBC - use Spring JDBC
* Spring Data JPA - use JPA
* Spring Data REST
* ...

## Spring Data JDBC
Source: [1](https://docs.spring.io/spring-data/jdbc/docs/current/reference/html/), [2](https://www.baeldung.com/jdbc-vs-r2dbc-vs-spring-jdbc-vs-spring-data-jdbc), [3](https://www.baeldung.com/spring-data-jdbc-intro)

**Spring Data JDBC** - persistence framework, проще Spring Data JPA, использует **Spring JDBC**, **нету:** cache, lazy loading, write-behind, schema generation etc. **Имеет** простую встроенную ORM: mapped entities, repositories, query annotations, JdbcTemplate. `@Query` in **plain** SQL.

```xml
<dependency> 
    <groupId>org.springframework.boot</groupId> 
    <artifactId>spring-boot-starter-data-jdbc</artifactId>
</dependency> 
```
```java
public class Person {
    @Id private long id;
    private String firstName;
    private String lastName;
}

@Repository
public interface PersonRepository extends CrudRepository<Person, Long> {
    @Lock(LockMode.PESSIMISTIC_READ)
    List<Person> findByFirstName(String firstName);

    @Modifying
    @Query("UPDATE person SET first_name = :name WHERE id = :id")
    boolean updateByFirstName(@Param("id") Long id, @Param("name") String name);
}
```

## Spring Data JPA

### Common
**Spring Data JPA** - набор классов для удобного доступа к **JPA data sources** через **Java Persistence API (JPA)**

**Цель:** не писать одинаковый код, использовать шаблоны.

**Specification vs Query Methods** Источник: [1](https://reflectoring.io/spring-data-specifications/)  
- Недостатки **Query Methods**:
  - их много в интерфейсе
    - **Note.** Частично решается путем выноса похожих методов в общие интерфейсы-предоки
  - запросы трудно читать
  - если нужно изменить условие поиска, то приходится менять во всех методах (нельзя переиспользовать части запросов в других запросах)
- Недостатки **Specification**:
  - нельзя выбрать только часть полей, можно только Entity целиком
    - **Note.** Это ограничение можно обойти используя **EntityGraph**, но этого нет в официальной реализации
  - запросы трудно читать

### Dependencies
```xml
<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-jpa</artifactId>
</dependency>
```

### Interfaces and methods
**Особенности:**
1. **getReferenceById(id)** (ex `getOne(id)`) - lazy и может вызвать **lazy exception** если вызван вне транзакции (нереальный случай т.к. вызов вне транзакции - ошибка разработчика). Не использует дополнительный round-trip в DB в отличии от **findById()**
   1. Для **update by id** лучше использовать **getReferenceById(id)**, так обновиться только часть полей, а не все
   2. Как получить **reference** не по **id**, а по условию неясно, возможно такого способа нет
2. **findById()** ([1](https://vladmihalcea.com/spring-data-jpa-findbyid/) - `race condition`) - отдельный запрос нельзя использовать для проверки на существования **parent record**, т.к. она может быть удалена сразу после **findById(id)** и для этого используется лишний `select ...`. Т.е. ситуация когда **child record** привязывается и сохраняется к **parent record**, которой уже нет.
   1. **Note.** Видимо это не критично, т.к. обычно все выполняется в 1ой **Transactional**
3. Пока не выполнится **flush()** запись может быть не видна по id в **REQUIRES_NEW** транзакции
   1. Не точно
4. Если вызывать методы через `repositor.findAll(spec)` (или похожие), то будет сделать новый запрос к **DB**, если получать связные **Entity** через **getter** (e.g. `user.getRole()`), то **Entity** будет взята из **L1 cache**.
5. Параметр `@OneToMany(orphanRemoval = true)` лучше не использовать, если нужен контроль над удалением, например аудит или проверка прав доступа. Т.е. делать **cascade** удаление дочерних **Entity** через прямые вызовы **service**, а только потом удалять родительскую (e.g. нарушение проверки прав доступа прервет `@Transactional`)
6. **saveAndFlush()** - используется когда нужно без задержек отправить данные в DB, например при работе с **eventualy consistent** системами (JMS/Kafka и микросервисами) чтобы изменение увидели мгновенно параллельные приложения работающие с той же DB.
7. Даже если вызывать **save()/delete()** и пр. **в цикле**, то они автоматически будут сделаны **batch** (т.к. склеится большой insert и поделится на куски), размер **batch** зависит от настроек JPA
8. При использование **Page** вызывается затратный **count()** для подсчета размера **total**, если нужно быстрее, то использовать **List** или **Slice**
9. Методы Repository которые написаны через `@Query` или **query methods**, но не наследуются от базовых Repository не Transactional по умолчанию и их нужно отмечать `@Transactional`
10. `@Repository` можно не ставить над своим Repository, если наследуется **стандартный** Repository, т.к. Spring поймет что это бин автоматически
11. Для работы с **reactive** базами данных нужно и можно использовать специальные **reactive** классы **Spring Data Jpa** и нужно проверить что DB поддерживает **reactive**
12. В **Critera API** (Specification) нельзя задать параметр **limit** (из SQL), его можно установить только через **Pageable**. При этом задать `order by` можно
13. Чтобы кастомизировать **order by** через **Criteria API** и одновременно использовать **Pageable.sort** нужно кастомизировать `@Controller` или `Controller Resolver` и сделать перенос **sort** в **Specification**, чтобы не перезатироть **order by**, а добавлять новые.
14. Десереализация вложенных Entity из **ResultSet** (результата запроса) делается автоматически на основе установленных параметров **Hint** в **SimpleJpaRepository** (точнее там их можно подменить). Поэтому если есть проблемы с **distinct** при множественных **join** (когда появляются дубли вложенных Entity) придется делать кастомизацию **SimpleJpaRepository** для установки своих правил. Или использовать **EntityManager**
15. Если проект большой и кастомизация поведения сделана на уровне **Spring Data JPA**, то нужно поменить что в библиотеках при приведении типов или использовании базовых классов метод может быть недоступен. И что в этом случае нельзя использовать **EntityManager**, если поведение завязано например на слушатели событие из **Spring Data JPA**
16. Наследуем **JpaRepository** и **JpaSpecificationExecutor** своему **Repository** чтобы использовать **CRUD** и **Specification**
17. Если нужно что-то начитать за 1 запрос, то описываем **EntityGraph**, т.к. по умолчанию все Entity отмечаем **Lazy** чтобы не тянулись все связные таблицы при любом запросе
18. Если не хватает методов, например нужно начитать только часть полей по **Specification**, то кастомизируем **SimpleJpaRepository**
19. Если нужно сделать update для Entity и при этом событие update вызывает какие-то другие действия, то делаем через `save()` т.к. `@Query` не вызывает `HibernateListener` (по крайней мере стандартно)
20. Если нужно сделать update для Entity и одна из строк очень **длиная** (текст или Blob), то используем `@DynamicUpdate` над Entity или пишем кастомный `@Query`
21. Стараемся использовать **Criteria API** (или Specification), т.к. их можно склеивать друг с другом в отличии от **method query** и удобно хранить в Utils классах
22. Если нужно задать кастомный **план запроса**, entityGraph или timeout используем **Hint** из **Hibernate ORM**. В **Spring Data JPA** нельзя менять **QueryHint** динамически, **только 1 раз** прописать для методов **repository**. Чтобы динамически устанавливать **QueryHint** можно кастомизировать **SimpleJpaRepository** (для некоторых возможно придется сделать `.unwrap()` класса JPA)
23. Чтобы сделать **update by id** только части полей (а не всех) нужно начитать **Entity** через **getReferenceById(id)**

```java
@Indexed
public interface Repository<T, ID> { }

@NoRepositoryBean
public interface CrudRepository<T, ID> extends Repository<T, ID> {
	<S extends T> S save(S entity);
	<S extends T> Iterable<S> saveAll(Iterable<S> entities);
	Optional<T> findById(ID id);
	boolean existsById(ID id);
	Iterable<T> findAll();
	Iterable<T> findAllById(Iterable<ID> ids);
	long count();
	void deleteById(ID id);
	void delete(T entity);
	void deleteAll(Iterable<? extends T> entities);
	void deleteAll();
}

@NoRepositoryBean
public interface PagingAndSortingRepository<T, ID> extends CrudRepository<T, ID> {
	Iterable<T> findAll(Sort sort);
	Page<T> findAll(Pageable pageable);
}

@NoRepositoryBean
public interface JpaRepository<T, ID> extends PagingAndSortingRepository<T, ID>, QueryByExampleExecutor<T> {
	@Override List<T> findAll();
	@Override List<T> findAll(Sort sort);
	@Override List<T> findAllById(Iterable<ID> ids);
	@Override <S extends T> List<S> saveAll(Iterable<S> entities);
	void flush();
	<S extends T> S saveAndFlush(S entity);
	void deleteInBatch(Iterable<T> entities);
	void deleteAllInBatch();
	T getOne(ID id);
	@Override <S extends T> List<S> findAll(Example<S> example);
	@Override <S extends T> List<S> findAll(Example<S> example, Sort sort);
}

// For Specification
public interface JpaSpecificationExecutor<T> {
	Optional<T> findOne(@Nullable Specification<T> spec);
	List<T> findAll(@Nullable Specification<T> spec);
	Page<T> findAll(@Nullable Specification<T> spec, Pageable pageable);
	List<T> findAll(@Nullable Specification<T> spec, Sort sort);
	long count(@Nullable Specification<T> spec);
    boolean exists(Specification<T> spec);
}

// SPI
@NoRepositoryBean
public interface JpaRepositoryImplementation<T, ID> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
	void setRepositoryMethodMetadata(CrudMethodMetadata crudMethodMetadata);
	default void setEscapeCharacter(EscapeCharacter escapeCharacter) { }
}

// Implementation
@Repository
@Transactional(readOnly = true)
public class SimpleJpaRepository<T, ID> implements JpaRepositoryImplementation<T, ID> {
	private final JpaEntityInformation<T, ?> entityInformation;
	private final EntityManager em;
	private final PersistenceProvider provider;
	private @Nullable CrudMethodMetadata metadata;
	private EscapeCharacter escapeCharacter = EscapeCharacter.DEFAULT;
	public SimpleJpaRepository(JpaEntityInformation<T, ?> entityInformation, EntityManager entityManager) { }
	public SimpleJpaRepository(Class<T> domainClass, EntityManager em) { }
    // ...
}
```

### Use
```java
// 0. Entity
@MappedSuperclass
public abstract class AbstractMappedType {
    String attribute; // common field
    // ...
}

@Entity public class ConcreteType extends AbstractMappedType { }

// 0. Configuration
@Configuration // in Spring Boot it will scan root directory
@EnableJpaRepositories(
    basePackages = "com.acme.repositories", // scan by package
    basePackageClasses = MyRepo.class, // alternative, MyRepo - interface marker, type safe
    considerNestedRepositories = true // scan nested repository class
)
class ApplicationConfiguration { }

// 1. common repository
@NoRepositoryBean // проставляется над базовым классом `CrudRepository`, чтобы не создалась его сущность, чтобы использовать его в качестве **базового класса**
public interface MappedTypeRepository<T extends AbstractMappedType> extends Repository<T, Long> {
  @Query("select t from #{#entityName} t where t.attribute = ?1") // #{#entityName} == T
  List<T> findAllByAttribute(String attribute);
}

// 2. actual repository
@Repository
@Transactional(readOnly = true) // need for non standard READ method to be Transactional
public interface UserRepository extends MappedTypeRepository<User, Long> {

    // READ methods are Transactional now
    Page<User> findByLastname(String lastname, Pageable pageable); // use additional count(*)
    Slice<User> findByLastname(String lastname, Pageable pageable); // do not use additional count(*) for big ResultSet
    List<User> findByLastname(String lastname, Sort sort); // do not use additional count(*) for big ResultSet

    @Transactional // readOnly = false
    @Modifying // for create/update/delete
    @Query("update User u set u.firstname = ?1 where u.lastname = ?2")
    int setFixedFirstnameFor(String firstname, String lastname);

    // Not all Spring Data modules currently support Stream<T> as a return type. 
    // Should close stream!
    //      try (Stream<User> stream = repository.findAllByCustomQueryAndStream()) { stream.forEach(…); }
    @Query("select u from User u")
    Stream<User> findAllByCustomQueryAndStream();

    // Named Parameters
    @Query("select u from User u where u.firstname = :firstname or u.lastname = :lastname")
    User findByLastnameOrFirstname(@Param("lastname") String lastname, @Param("firstname") String firstname);

    Page<User> queryFirst10ByLastname(String lastname, Pageable pageable); // limit 10
}
```

### Custom SimpleJpaRepository
**Для чего:** расширить поведение методов функциями которые не реализованы в **Spring Data JPA** (e.g. выборка части полей, работа с Criteria API, простановка EntityGraph динамически, расширенная работа с subselect)
```java
```

### Использование `@EntityGraph` в `JpaRepositor`
Не существует способа динамического обьявления `@EntityGraph`, поэтому можно создать custom имплементацию.
```java
// 1. @EntityGraph
@Repository
public interface ImportMovieDAO extends JpaRepository<ImportMovie, Long> {
  @NotNull
  @Override
  @EntityGraph(value = "graph.ImportMovie.videoPaths")
  List<ImportMovie> findAll();
}

// 2. Стороняя библиотека Spring Data JPA EntityGraph https://github.com/Cosium/spring-data-jpa-entity-graph
// EntityGraphJpaSpecificationExecutor
@Repository
public interface UserRepository extends JpaSpecificationExecutor<User>, JpaRepository<User, Long>,
EntityGraphJpaSpecificationExecutor<User> {
}
List<User> users = userRepository.findAll(specification, new NamedEntityGraph(EntityGraphType.FETCH, "graphName"))

// 3. Имплементировать поддержку EntityGraph
// https://stackoverflow.com/a/30087275
@NoRepositoryBean // 3.1 интерфейс
public interface CustomRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
    List<T> findAll(Specification<T> spec, EntityGraphType entityGraphType, String entityGraphName);
    Page<T> findAll(Specification<T> spec, Pageable pageable, EntityGraphType entityGraphType, String entityGraphName);
    List<T> findAll(Specification<T> spec, Sort sort, EntityGraphType entityGraphType, String entityGraphName);
    T findOne(Specification<T> spec, EntityGraphType entityGraphType, String entityGraphName);
}

@NoRepositoryBean // 3.2 реализация
public class CustomRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CustomRepository<T, ID> {

    private EntityManager em;

    public CustomRepositoryImpl(Class<T> domainClass, EntityManager em) {
        super(domainClass, em);
        this.em = em;
    }

    @Override
    public List<T> findAll(Specification<T> spec, EntityGraph.EntityGraphType entityGraphType, String entityGraphName) {
        TypedQuery<T> query = getQuery(spec, (Sort) null);
        query.setHint(entityGraphType.getKey(), em.getEntityGraph(entityGraphName));
        return query.getResultList();
    }

    @Override
    public Page<T> findAll(Specification<T> spec, Pageable pageable, EntityGraph.EntityGraphType entityGraphType, String entityGraphName) {
        TypedQuery<T> query = getQuery(spec, pageable.getSort());
        query.setHint(entityGraphType.getKey(), em.getEntityGraph(entityGraphName));
        return readPage(query, pageable, spec);
    }

    @Override
    public List<T> findAll(Specification<T> spec, Sort sort, EntityGraph.EntityGraphType entityGraphType, String entityGraphName) {
        TypedQuery<T> query = getQuery(spec, sort);
        query.setHint(entityGraphType.getKey(), em.getEntityGraph(entityGraphName));
        return query.getResultList();
    }

    @Override
    public T findOne(Specification<T> spec, EntityGraph.EntityGraphType entityGraphType, String entityGraphName) {
        TypedQuery<T> query = getQuery(spec, (Sort) null);
        query.setHint(entityGraphType.getKey(), em.getEntityGraph(entityGraphName));
        return query.getSingleResult();
    }
}

// 3.3 подключаем
@EnableJpaRepositories(basePackages = {"your.package"}, repositoryBaseClass = CustomRepositoryImpl.class)
```

### Unusual use
```java
// 1. User repository outside Spring Container
RepositoryFactorySupport factory = new RepositoryFactorySupport();
UserRepository repository = factory.getRepository(UserRepository.class);

// 2. Get repository by Entity class
@Autowired ApplicationContext context;
Repositories repositories = new Repositories(context);
Optional<Object> repository = repositories.getRepositoryFor(entityClass);
JpaRepository repository = ((JpaRepository) repository.get());

```

### Авто генерация SQL на чтение или фильтрацию по параметрам запроса REST
Это фактически сериализацию SQL запроса в JSON и его дессериализация. Она ограничена базовыми операциями (т.к. сложна в реализации и чтобы не вызывать sql injection). Это запросы только на чтение!  
Так мы перекладываем построение запроса на фронт разработчиков (клиента).

**Для чего:** в реальности писать свой запрос на каждую варианцию фильтрации вручную долго.

**В чем проблема:** не существует готовых удобных систем авто построения SQL на основе параметров фильтрации. Все существующие библиотеки с недостатками.

Такой подход может использоваться при запросах между сервисами с паттернами как Transaction Outbox. E.g. чтобы проверить нет ли записи в локалиной таблице-репликации, и если нет, то переадресовать запрос сервису-источнику репликации. Т.е. тут происходит десериализация JSON в SQL, выполнение локально, а затем в случае если ничего не найдено этот же JSON переотправляется в другой сервис-источник репликации.

**Способы формирования запросов REST** (REST Query Language): (все способы формирования запросов, и динамические запросы на основе REST архитектуры запроса, для работы с Entity и их частями) [тут](https://www.baeldung.com/spring-rest-api-query-search-language-tutorial)  
- **REST Query Language**
  - [JPA Criteria](https://www.baeldung.com/rest-search-language-spring-jpa-criteria)
  - [JPA Specifications](https://www.baeldung.com/rest-api-search-language-spring-data-specifications)
  - [QueryDSL](https://www.baeldung.com/rest-api-search-language-spring-data-querydsl)
- Расширения (библиотеки и подходы)
  - [Advanced Search Operations](https://www.baeldung.com/rest-api-query-search-language-more-operations) - реализация Search (фильтрации) на Criteria API
  - [Implementing OR Operation](https://www.baeldung.com/rest-api-query-search-or-operation) - реализация OR на Criteria API
  - [RSQL](https://www.baeldung.com/rest-api-search-language-rsql-fiql) - библиотека динамически генерирующая SQL
  - [Querydsl Web Support](https://www.baeldung.com/rest-api-search-querydsl-web-in-spring-data-jpa)
  - [Пример реализации фильтрации для Specifications](https://reflectoring.io/spring-data-specifications/)

# Spring Cache, `@Cacheable`
Источник: [0](https://spring.io/guides/gs/caching/), [1](https://www.baeldung.com/spring-cache-tutorial), [2](https://www.baeldung.com/spring-boot-evict-cache)

`@EnableCaching` включает **post processor** который проверяет все beans на присутствие аннотаций `@Cacheable`, `@CachePut` and `@CacheEvict`. Авто конфигурируется `CacheManager` для использования подходящего **cahce provider** ([реализации кэша](https://docs.spring.io/spring-boot/docs/current/reference/html/spring-boot-features.html#boot-features-caching)). Вокруг каждого класса с кэшированными методам автоматически создается proxy для перехвата вызовов.  
Если подключенного jar с **cache provider** не найдено, то будет использоваться `cahce provider` **simple** `spring.cache.type=simple` основанный на `ConcurrentHashMap`.  
**Поиск cache provider в порядке:** JCache, EhCache, Hazelcast, Infinispan, Couchbase, Redis, Caffeine, Simple (**default** если не найдено).

**1 Подключение**
```xml
<dependencies>
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-cache</artifactId>
    </dependency>
</dependencies>
```
**2 Настраиваем конфигурацию**
```yml
spring.cache.type=none # можно отключить кэш поставив cache provider в no-op
spring.cache.type=simple # можно указать cahce provider вручную

spring.cache.ehcache.config=classpath:config/another-config.xml # для каждого cache provider свои параметры
```
**3 Включаем и используем**
```java
@SpringBootApplication
@EnableCaching // 1. включаем
public class CachingApplication {
  public static void main(String[] args) { SpringApplication.run(CachingApplication.class, args); }

    @Bean // 2. в Spring Boot бин CacheManager создается автоматически
    public CacheManager cacheManager() {
        return new ConcurrentMapCacheManager("addresses");
    }
}

// 3. Кастомизация в Spring Boot через CacheManagerCustomizer
@Component
public class SimpleCacheCustomizer implements CacheManagerCustomizer<ConcurrentMapCacheManager> {
    @Override
    public void customize(ConcurrentMapCacheManager cacheManager) {
        cacheManager.setCacheNames(asList("users", "transactions"));
    }
}

// 4. Использование, @Cacheable выполнится 1 раз и потом будет возвращать cache
@Cacheable({"addresses", "directory"})
public String getAddress(Customer customer) {

}

// 5. Указываем вытеснить все из кэша, переначитать и вернуть, на случай если cache слишком большой
@CacheEvict(value="addresses", allEntries=true) // allEntries=true значит ВСЕ
public String getAddress(Customer customer) {

}

// 5. Несколько правил
@Caching(
    cacheable = {
        @Cacheable("users"),
        @Cacheable("contacts")
    },
    put = {
        @CachePut("tables"),
        @CachePut("chairs"),
        @CachePut(value = "meals", key = "#user.email")
    },
    evict = { 
        @CacheEvict("addresses"), 
        // Вытеснить из cache если customer.name совпал
        @CacheEvict(value="directory", key="#customer.name") })
public String getAddress(Customer customer) {

}

// 6. @CachePut в отличии от @Cacheable ВЫПОЛНЯЕТ метод (начитку) 1ин раз и добавляет результат в кэш
@CachePut(value="addresses")
public String getAddress(Customer customer) {

}

// 7. Чтобы не указывать cache name отдельно для методов
@CacheConfig(cacheNames={"addresses"})
public class CustomerDataService {
    @Cacheable
    public String getAddress(Customer customer) {

    }
}

// 8. Условие выполнения, только если в кэше есть данные с customer.name=='Tom'
@CachePut(value="addresses", condition="#customer.name=='Tom'")
public String getAddress(Customer customer) {

}

// 9. Кэшировать на основе результата, толкьо если в результате меньше 64 символов
@CachePut(value="addresses", unless="#result.length()<64")
public String getAddress(Customer customer) {

}

// 10. Ручное управление кэшем
class Service {
    @Autowired CacheManager cacheManager;

    public void evictSingleCacheValue(String cacheName, String cacheKey) {
        cacheManager.getCache(cacheName).evict(cacheKey);
    }

    public void evictAllCacheValues(String cacheName) {
        cacheManager.getCache(cacheName).clear();
    }

    // Spring's task scheduler, очистка кэша по таймеру
    @Scheduled(fixedRate = 6000)
    public void evictAllcachesAtIntervals() {
        evictAllCaches();
    }
}
```

# Spring Integration
Spurce: [baeldung Spring Integration](https://www.baeldung.com/spring-integration),
[baeldung Spring Integration series](https://www.baeldung.com/tag/spring-integration),
[spring.io](https://spring.io/projects/spring-integration)

# Spring Batch
Source: [1](https://www.baeldung.com/introduction-to-spring-batch)

Суть: разбиваем код на задачи - job, каждая состоит из steps, можно переиспользовать steps в других jobs. Все это можно реализовать и вручную, но тут готовый функционал.

Spring Batch — это среда, созданная для пакетной обработки больших объёмов данных. В основе Spring Batch лежит понятие задания (Job). Каждое задание может состоять из нескольких этапов (Step). Этап в свою очередь может быть либо фрагментом произвольного кода (Tasklet), либо иметь более сложную структуру, состоящую из считывателей элементов (ItemReader), обработчиков элементов (ItemProcessor) и записывателей элементов (ItemWriter).

```java
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.repeat.RepeatStatus;

@Configuration
@EnableBatchProcessing //(1)
public class BatchConfig {
	@Bean // (2)
	public Tasklet clearTableTasklet(JdbcTemplate jdbcTemplate) {
        return (stepContribution, chunkContext) -> {
            log.info("Очистка таблицы sales_report");
            jdbcTemplate.update("delete from sales_report");
            return RepeatStatus.FINISHED;
        };
    }

	@Bean //(3)
	public Step setupStep(Tasklet clearTableTasklet,
                      	StepBuilderFactory stepBuilderFactory) {
        return stepBuilderFactory.get("clear-report-table")
            	.tasklet(clearTableTasklet)
            	.build();
    }
}

@Bean // (4)
public Step loadCsvStep(StepBuilderFactory stepBuilderFactory,
                        FlatFileItemReader<SalesReportItem> csvReader,
                        ItemProcessor<SalesReportItem, SalesReportItem> totalCalculatingProcessor,
                        JdbcBatchItemWriter<SalesReportItem> dbWriter) {
    return stepBuilderFactory.get("load-csv-file")
            .<SalesReportItem, SalesReportItem>chunk(10) // (5)
            .faultTolerant()
            .skip(IncorrectValueException.class) // (6)
            .skipLimit(3) // (7)
            .reader(csvReader) // (8)
            .processor(totalCalculatingProcessor) // (9)
            .writer(dbWriter) // (10)
            .build();
}

@Bean
@StepScope // (11)
public FlatFileItemReader<SalesReportItem> csvReader() {
    return new FlatFileItemReaderBuilder<SalesReportItem>().name("csv-reader")
            .resource(new ClassPathResource("report_data.csv"))
            .targetType(SalesReportItem.class)
            .delimited()
            .delimiter("|")
            .names("regionId", "outletId", "smartphones", "memoryCards", "notebooks").build();
}

@Bean // (12)
public ItemProcessor<SalesReportItem, SalesReportItem> totalCalculatingProcessor() {
    return item -> {
        if (BigDecimal.ZERO.compareTo(item.getSmartphones()) > 0 || BigDecimal.ZERO.compareTo(item.getMemoryCards()) > 0 || BigDecimal.ZERO.compareTo(item.getNotebooks()) > 0) {
            throw new IncorrectValueException();
        }
        item.setTotal(BigDecimal.ZERO.add(item.getSmartphones()).add(item.getMemoryCards()
                        .add(item.getNotebooks())));
        return item;
    };
}

@Bean // (13)
public JdbcBatchItemWriter<SalesReportItem> dbWriter(DataSource dataSource) {
    return new JdbcBatchItemWriterBuilder<SalesReportItem>()
            .dataSource(dataSource)
            .sql("insert into sales_report (region_id, outlet_id, smartphones, memory_cards, notebooks, total) values (:regionId, :outletId, :smartphones, :memoryCards, :notebooks, :total)")
            .beanMapped()
            .build();
}

@Bean // (14)
public Job importReportJob(JobBuilderFactory jobBuilderFactory, Step setupStep, Step loadCsvStep,
                            ReportImportListener reportImportListener) {
    return jobBuilderFactory.get("import-report-job")
            .incrementer(new RunIdIncrementer())
            .listener(reportImportListener) // (15)
            .start(setupStep)
            .next(loadCsvStep)
            .build();
}
// custom слушатель org.springframework.batch.core.listener.JobExecutionListenerSupport
@Service
public class ReportImportListener extends JobExecutionListenerSupport {
	@Override
	public void afterJob(JobExecution jobExecution) {
        if(jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info("Отчёт загружен в базу данных");
        }
    }

	@Override
	public void beforeJob(JobExecution jobExecution) { log.info("..."); }
}
```

# Spring Cloud
Source: [spring.io](https://spring.io/projects/spring-cloud)

Spring Cloud - набор инструментов для разработки с использованием паттернов распределенных систем (distributed systems, микросервисов, облачных). Т.е. configuration management, service discovery, circuit breakers, intelligent routing, micro-proxy, control bus, short lived microservices and contract testing.

* основные паттерны микросервисов
  * Distributed/versioned configuration
  * Service registration and discovery
  * Routing
  * Service-to-service calls
  * Load balancing
  * Circuit Breakers
  * Distributed messaging
  * Short lived microservices (tasks)
  * Consumer-driven and producer-driven contract testing
* Main Projects - тут не все компоненты
  * Spring Cloud Commons
    * Spring Cloud Commons - набор абстракций которые используются в других spring модулях Spring Cloud, обычно транзитивная
    * Spring Cloud Context - 
  * Spring Cloud Config (externalized configuration) - поддержка общих для всех под config (helm в kubernetes). e.g. `RefreshScope` над bean которая обновляет `@Value` в bean при смене config (application.yml). И событие `EnvironmentChangeEvent`
  * Spring Cloud Consul
  * Spring Cloud Circuit Breaker
  * Spring Cloud Gateway
  * Spring Cloud OpenFeign
  * Spring Cloud Vault
  * Spring Cloud Function
  * Spring Cloud Netflix
  * Spring Cloud Kubernetes
  * Spring Cloud for Amazon Web Services - интеграция с облачным сервисом AWS
  * Spring Cloud Task - для short lived микросервисов, запускающихся по шедулеру с аннотацией `@EnableTask`
* Spring Session - не часть Spring Cloud, но для обмена между микросервисами данными о аутентификации и др.  атрибутами HTTP сессии.

# Spring Boot Actuator
Source: [1](https://docs.spring.io/spring-boot/docs/2.5.4/reference/htmlsingle/#actuator)

**Spring Boot Actuator** - управляет сбором данных и настройками (через REST). Аудит, Логирование, Метрики подключаются через реализацию общего интерфейса, если нужно изменить их поведение, то нужно подключить или написать свои реализации. Может интегрироваться с такими инструментами как Zipkin.

`/health` используется для проверки запустился сервис или нет, каждый модуль spring после запуска сообщает в **Actuator** запустился он или упал (e.g. если **spring jms** вернет exception, то `/health` вернет `down` и сервис в kuber не будет отмечен как `up`, это можно отключить)

# Spring Boot Maven Plugin
Source: [1](https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle/)

# Spring Statemachine
Написать тут несколько простых заметок, т.к. может использоваться в реальных проектах

# Ссылки
* [Полный и удобный список ссылок на все проекты Spring](https://spring.io/projects/spring-boot)
* [Зависимости Spring Boot и их версии](https://docs.spring.io/spring-boot/docs/2.3.6.RELEASE/reference/html/dependency-versions.html)
* [Список большинства настроек application.yml](https://docs.spring.io/spring-boot/docs/current/reference/html/application-properties.html)
* [Настройка DataSource, в т.ч. 2х баз данных](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#data.sql.datasource)
* [Spring Boot Maven Plugin](https://docs.spring.io/spring-boot/docs/current/maven-plugin/reference/htmlsingle) - обязателен для сборки Spring Boot
* [Spring Initializr](https://start.spring.io/) - онлайн конструктор конфигураций сборочных систем для Spring Boot
* [Spring Boot Starter and Autoconfigurer](https://docs.spring.io/spring-boot/docs/current/reference/html/features.html#features.developing-auto-configuration)
* [Настройки appliation.yml для jackson](https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto.spring-mvc.customize-jackson-objectmapper) - тут только шаблог
  * [DeserializationFeature](https://fasterxml.github.io/jackson-databind/javadoc/2.6/com/fasterxml/jackson/databind/DeserializationFeature.html) - конкретные опции