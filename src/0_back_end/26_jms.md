# JMS
Источники: [тут](https://www.javacodegeeks.com/jms-tutorials)

**Java Message Service** (JMS) - это одно из MOM (message-oriented middleware API), для пересылки сообщений между двумя и более клиентами. Это реализация (implementation) для работы с producer–consumer problem. JMS часть Java EE. JMS это стандарт обмена сообщениями, который дает Java EE приложениям create, send, receive, and read messages. Позволяет обмен сообщениями между разными компонентами слабосвязно (loosely coupled), надежно (reliable), асинхронно.

Начиная с Java EE 1.4 JMS provider должен быть реализован всеми Java EE application servers (серверами приложений), JMS реализованы через Java EE Connector Architecture.

**Elements**
* **JMS provider** - реализация JMS на Java **или** реализация JMS в виде адаптера (прослойки) если реализация не на Java (т.е. провайдеры это ActiveMQ, RabbitMQ etc, т.е. провайдерами называют реализации JMS)
* **JMS client** - приложение или процесс, которые делают publishe/subscribe
* **JMS producer/publisher** - клиент который отсылает сообщения (в том числе и просто функции-отправители в рамках одного приложения)
* **JMS consumer/subscriber** - клиент который получает сообщения (в том числе и просто функции-получатели в рамках одного приложения)
* **JMS message** - объект обертка вокруг отправляемых или получаемых данных, т.е. это сообщения с содержимым
* **JMS queue** - структура (коллекция) содержащая полученные сообщения и ожидающие обработки, очередь не гарантирует порядок сообщений в том же порядке в котором они были отправлены, она гарантирует только то что сообщение будет обработано только один раз
* **JMS topic/log** - аналогично queue, но сообщения может получить множество разных получателей

**JMS broker** - это **JMS provider**, точнее это приложения для управления **queue** или **topic** сообщениями, можно сказать это процесс содержащий коллекции (topic, queue) сообщений. Если используется конвертер сообщений, то брокер может выполнять и функции конвертации при получении/отправке причем конвертацию можно описывать на разных спец. языках (как SQL например).

**Models**
* **Point-to-point** (queue, one to one) - представлен через **queue**, сообщения обрабатывает только один получатель
  * сообщение может быть восстановлено (вернуто) в очередь
  * только один получатель может получить сообщение
  * сильная coupling между отправителем и получателем
  * сложность работы с ней ниже
* **Publish-and-subscribe** (topic, one to many) - представлен через **topic**, сообщения обрабатывает множество получателей
  * слабая связанность (Decoupling)
  * сообщение не может быть восстановлено
  * сложность работы с ней выше

**Note.** Даже если listener для queue запущен в разных копиях одной и той же приложенки, то сообщение обработает только одна копия.

## Использование JMS
Сервера приложений Java EE используют одну из реализация JMS (JMS Provider). Чтобы использовать JMS в приложениях развернутых на этих серверах нужно создать через UI этого сервера или объявить в конфигурации этого сервера **Queue Factory** или **Topic Factory** с определенным именем. В самом приложении из JNDI (из контекста) по имени получить эту Factory, создать connection из нее, создать **session** из **connection**, создать объект **queue** или **topic** и связать с session. После этого можно отправлять или получать сообщения.
```java
InitialContext ctx=new InitialContext();
QueueConnectionFactory f=(QueueConnectionFactory)ctx.lookup("myQueueConnectionFactory"); // достаем Factory по имени
QueueConnection con=f.createQueueConnection(); // connection
con.start();
QueueSession ses=con.createQueueSession(false, Session.AUTO_ACKNOWLEDGE); // session
Queue t=(Queue)ctx.lookup("myQueue"); // queue или topic

// в классе receiver
QueueReceiver receiver=ses.createReceiver(t); 
receiver.setMessageListener(new MessageListener { // прием сообщения
     public void onMessage(Message m) {
     }
});

// в классе sender
QueueSender sender=ses.createSender(t);
TextMessage msg=ses.createTextMessage();
msg.setText("bla");
sender.send(msg); // отправка сообщения
```

## Фильтры в JMS (с примером)
пока пусто

## Interceptor в JMS (с примером)
пока пусто

## Spring JMS
пока пусто