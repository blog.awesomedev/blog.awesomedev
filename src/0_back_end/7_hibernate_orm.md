**Note.** Тем кто решится читать эту статью. Это личные заметки, переводы различных материалов. Раздел "Еще раз про транзакции" и все что ниже более актуально и перекрывает инфу которая выше, если в ней есть противоречия.

**Частые вопросы на собеседовании (в порядке убывания):**
* Допустимые типы полей в Entity
* Пример `session.createQuery()`, `createNativeQuery()`
* session.find(MyClass.class) - полиморфный запрос берет всех наследников табл через Inheritance Mapping Strategies
* Entity life cycle
* Transaction isolation level
* Transaction propagation level
* FetchMode, n + 1 problem, join fetch, `setFetchSize` и ограничение количества ответов, BatchSize
* FetchType
* Generate id strategy
* План запроса, как исправлять через hint, кэш плана запроса
* Sorting vs Ordering
* Примеры связей `@ManyToOne`, `@OneToMany`, `@OneToOne`; `CascadeType`, использование join column
* Criteria api
* L2 cache, как включить L2, query cache, cache strategy
* Методы Session, JPA методы vs Hibernate методы
* `@DynamicUpdate` и `@DynamicInsert`
* `@PrePersist`, `@PreUpdate`
* `@CreationTimestamp`, `@UpdateTimestamp`, `@Temporal(TemporalType.TIMESTAMP)`
* `@Enumerated(EnumType.STRING)`
* Optimistic lock
* `EntityGraph`
* Hints - не тоже что hints в sql
* Редкие темы
  * Flush strategy
  * Pessemistic lock
  * Lock scope
  * Cache region

***

- [Hibernate](#hibernate)
- [Hibernate vs JPA](#hibernate-vs-jpa)
- [Transaction](#transaction)
- [Cache](#cache)
  - [Подключение и примеры](#подключение-и-примеры)
  - [Когда не нужно использовать кэш L2 и Query cache](#когда-не-нужно-использовать-кэш-l2-и-query-cache)
  - [Cache region](#cache-region)
  - [Стратегии кэширования](#стратегии-кэширования)
- [Entity](#entity)
  - [Entity описание, операции, требования](#entity-описание-операции-требования)
  - [Custom types полей](#custom-types-полей)
- [Naming strategies](#naming-strategies)
- [AttributeConverter](#attributeconverter)
- [Generated properties](#generated-properties)
- [`@GeneratorType` annotation](#generatortype-annotation)
- [Сложные структуры](#сложные-структуры)
  - [`@Embeddable`](#embeddable)
  - [`@ElementCollection`](#elementcollection)
  - [Типы связей Entity](#типы-связей-entity)
    - [Примеры типов связей Entity](#примеры-типов-связей-entity)
  - [Mapped Superclass](#mapped-superclass)
  - [Inheritance Mapping Strategies](#inheritance-mapping-strategies)
  - [Используем ids совместно с связями через Entity](#используем-ids-совместно-с-связями-через-entity)
- [Fetching (Стратегии загрузки коллекций в Hibernate)](#fetching-стратегии-загрузки-коллекций-в-hibernate)
  - [Типы fetch](#типы-fetch)
  - [fetching стратегии](#fetching-стратегии)
  - [N + 1 selects problem](#n-1-selects-problem)
  - [JOIN FETCH (решение проблемы N + 1 selects)](#join-fetch-решение-проблемы-n-1-selects)
  - [List vs Set](#list-vs-set)
  - [Entity Graph (решение проблемы N + 1 selects)](#entity-graph-решение-проблемы-n-1-selects)
  - [Hibernate fetch стратегии, отличие от JPA: `FetchMode`, `SUBSELECT`, `@BatchSize`](#hibernate-fetch-стратегии-отличие-от-jpa-fetchmode-subselect-batchsize)
    - [`@BatchSize` c `FetchMode.SELECT`](#batchsize-c-fetchmodeselect)
    - [`FetchMode.SUBSELECT`](#fetchmodesubselect)
  - [`@LazyCollection`](#lazycollection)
- [Mapping](#mapping)
  - [Mapping annotations](#mapping-annotations)
  - [Типы Collection в связях](#типы-collection-в-связях)
  - [`AttributeOverride`](#attributeoverride)
- [Persistence Context](#persistence-context)
  - [Hibernate Entity Lifecycle](#hibernate-entity-lifecycle)
  - [Методы Session (включая JPA vs Hibernate методы)](#методы-session-включая-jpa-vs-hibernate-методы)
- [`unwrap()` method](#unwrap-method)
- [Bootstrap](#bootstrap)
- [Exceptions и их обработка](#exceptions-и-их-обработка)
- [Настройка Hibernate и Spring](#настройка-hibernate-и-spring)
  - [Как Transaction работает внутри](#как-transaction-работает-внутри)
  - [Аннотация `@Transactional`](#аннотация-transactional)
  - [Старый вариант с HibernateUtil](#старый-вариант-с-hibernateutil)
  - [Вариант с ручным созданием разных Bean в `@Configuration` для Hibernate + Spring](#вариант-с-ручным-созданием-разных-bean-в-configuration-для-hibernate-spring)
  - [xml конфигурация](#xml-конфигурация)
  - [Конфигурация в Spring Boot через файл настроек](#конфигурация-в-spring-boot-через-файл-настроек)
- [Misc](#misc)
  - [Кавычки в именах Entity, использование зарезервированных имен](#кавычки-в-именах-entity-использование-зарезервированных-имен)
  - [Тип в котором хранить деньги, BigDecimal](#тип-в-котором-хранить-деньги-bigdecimal)
  - [Особенности работы с БД MySQL](#особенности-работы-с-бд-mysql)
- [Про Hibernate Criteria API vs JPA Criteria API](#про-hibernate-criteria-api-vs-jpa-criteria-api)
- [JPA Criteria API](#jpa-criteria-api)
  - [Базовые](#базовые)
  - [Как работать с этим в целом (примеры)](#как-работать-с-этим-в-целом-примеры)
- [Примеры запросов на Criteria API](#примеры-запросов-на-criteria-api)
- [Specification (и ее связь с Criteria API), использование с find методом из Spring Data JPA](#specification-и-ее-связь-с-criteria-api-использование-с-find-методом-из-spring-data-jpa)
- [Еще раз Про транзакции](#еще-раз-про-транзакции)
  - [Locks in JPA](#locks-in-jpa)
  - [Transactoin Isolation Level in JPA](#transactoin-isolation-level-in-jpa)
  - [Optimistic locking, Version-less optimistic locking и `@Source`](#optimistic-locking-version-less-optimistic-locking-и-source)
  - [Атрибуты `@Version` для **optimistic lock**](#атрибуты-version-для-optimistic-lock)
  - [О том что изменение Optimistic lock version в child entity не увеличивает version в root entity и как решить эту проблему](#о-том-что-изменение-optimistic-lock-version-в-child-entity-не-увеличивает-version-в-root-entity-и-как-решить-эту-проблему)
  - [Detached entities anomaly при Version-less optimistic locking](#detached-entities-anomaly-при-version-less-optimistic-locking)
  - [работа с OptimisticLockException и о том что detached entities могут](#работа-с-optimisticlockexception-и-о-том-что-detached-entities-могут)
  - [Lock Scope](#lock-scope)
  - [Lock Timeout](#lock-timeout)
  - [Phisical vs logical transaction (Unit of Work)](#phisical-vs-logical-transaction-unit-of-work)
  - [Entity state transitions (Entity life cycle)](#entity-state-transitions-entity-life-cycle)
  - [Flush strategies](#flush-strategies)
  - [Dirty checking mechanism](#dirty-checking-mechanism)
  - [Transaction pitfalls in JPA (возможные проблемы транзакций в JPA)](#transaction-pitfalls-in-jpa-возможные-проблемы-транзакций-в-jpa)
  - [Propagation level](#propagation-level)
  - [Transactin in Spring](#transactin-in-spring)
    - [В Spring](#в-spring)
    - [В Spring Data](#в-spring-data)
    - [В Spring Boot](#в-spring-boot)
    - [О том как работают транзакции в Spring](#о-том-как-работают-транзакции-в-spring)
    - [Open Session in View (OSIV) паттерн](#open-session-in-view-osiv-паттерн)
  - [Global (XA) vs local transactions vs distributed transactions](#global-xa-vs-local-transactions-vs-distributed-transactions)
- [Про equals и `@NaturalId`](#про-equals-и-naturalid)
- [Кэширование и регионы кэша](#кэширование-и-регионы-кэша)
- [Связи](#связи)
  - [Common](#common)
  - [One-To-Many](#one-to-many)
  - [One-To-One](#one-to-one)
  - [Many-To-Many](#many-to-many)
  - [Non Primary-Key `@ManyToOne` mapping (mapping не по id)](#non-primary-key-manytoone-mapping-mapping-не-по-id)
- [CascadeType](#cascadetype)
- [Как правильно делать merge](#как-правильно-делать-merge)
- [DTO projection, ResultTransformer и про то что он делает SQL запросы эффективнее](#dto-projection-resulttransformer-и-про-то-что-он-делает-sql-запросы-эффективнее)
- [StatelessSession](#statelesssession)
- [Зачем нужно делать Detached объекты в Hibernate Session](#зачем-нужно-делать-detached-объекты-в-hibernate-session)
- [Hypersistence Optimizer](#hypersistence-optimizer)
- [`@DynamicUpdate` и `@DynamicInsert`](#dynamicupdate-и-dynamicinsert)
- [Collection vs Bag vs List vs Map vs Set](#collection-vs-bag-vs-list-vs-map-vs-set)
- [`MultipleBagFetchException`](#multiplebagfetchexception)
- [Sorting vs Ordering](#sorting-vs-ordering)
- [Типы id и их генерация](#типы-id-и-их-генерация)
- [Использование дат в Entity](#использование-дат-в-entity)
- [Авто подстановка в поле Entity текущей даты при сохранении Entity](#авто-подстановка-в-поле-entity-текущей-даты-при-сохранении-entity)
- [`@Size`, `@Length`, and `@Column(length=value)`](#size-length-and-columnlengthvalue)
- [`@Immutable`](#immutable)
- [Custom Types and `@Type`, Hibernate Types](#custom-types-and-type-hibernate-types)
- [Hibernate Event Listener, `@PrePersist`, `@PreUpdate` etc](#hibernate-event-listener-prepersist-preupdate-etc)
- [Hibernate Filter и фильтр related nested Entity collections](#hibernate-filter-и-фильтр-related-nested-entity-collections)
- [Hibernate Interceptors](#hibernate-interceptors)
- [Hibernate Second-Level Cache](#hibernate-second-level-cache)
- [Логирование и тюнинг запросов и др.](#логирование-и-тюнинг-запросов-и-др)
  - [Логирование в Hibernate ORM, Spring Data JPA, JDBC](#логирование-в-hibernate-orm-spring-data-jpa-jdbc)
- [Hibernate Pagination](#hibernate-pagination)
- [Способы создания query в одном разделе, могут спросить на собеседовании](#способы-создания-query-в-одном-разделе-могут-спросить-на-собеседовании)
- [JPA vs Hibernate](#jpa-vs-hibernate)
- [Second-Level Cache](#second-level-cache)
  - [common, Cache Concurrency Strategy](#common-cache-concurrency-strategy)
  - [Про сброс Second-Level Cache](#про-сброс-second-level-cache)
  - [Пошаговая настройка Second-Level Cache](#пошаговая-настройка-second-level-cache)
  - [Query Cache](#query-cache)
  - [How to work L2 cache](#how-to-work-l2-cache)
  - [Query Cache N+1 issue](#query-cache-n1-issue)
  - [Cache non-existing entity fetch results with JPA and Hibernate](#cache-non-existing-entity-fetch-results-with-jpa-and-hibernate)
- [JPA and Hibernate query hints](#jpa-and-hibernate-query-hints)
- [SQL query plan through Hibernate ORM hints](#sql-query-plan-through-hibernate-orm-hints)
- [Примитивный тип vs Обертки примитивных типов в качестве id для Entity](#примитивный-тип-vs-обертки-примитивных-типов-в-качестве-id-для-entity)
- [Enum и `@Enumerated`](#enum-и-enumerated)
- [`@Transient`](#transient)
- [`@Lob`](#lob)
- [`@NotFound`](#notfound)
- [`getMetamodel()` и получение имен таблиц, entity etc](#getmetamodel-и-получение-имен-таблиц-entity-etc)
- [Сохранить связные `Entity` можно только если они были до этого `persisted` или используя `CascadeType=PERSIST`](#сохранить-связные-entity-можно-только-если-они-были-до-этого-persisted-или-используя-cascadetypepersist)
- [При начитке id связанной Entity не будет избыточной начитки целой Entity](#при-начитке-id-связанной-entity-не-будет-избыточной-начитки-целой-entity)
- [Интеграция Hibernate Validator может обрабатывать Hibernate ORM (e.g. `@NotNull`)](#интеграция-hibernate-validator-может-обрабатывать-hibernate-orm-eg-notnull)
- [Join в hibernate нельзя делать, если нету связей `@ManyToOne` etc](#join-в-hibernate-нельзя-делать-если-нету-связей-manytoone-etc)
- [Замапить одну Entity на несколько табл можно, если не мапить часть полей то ошибки не будет](#замапить-одну-entity-на-несколько-табл-можно-если-не-мапить-часть-полей-то-ошибки-не-будет)
- [Soft delete, `@SQLDelete`, `@Where`, `@Loader`](#soft-delete-sqldelete-where-loader)
- [Batch операция в JPA](#batch-операция-в-jpa)
- [Connection Pool](#connection-pool)
- [Добавление спец. функций DB в Criteria API (JPA 2)](#добавление-спец-функций-db-в-criteria-api-jpa-2)
- [Отдельный функционал](#отдельный-функционал)
  - [specification-arg-resolver](#specification-arg-resolver)
  - [Hibernate Envers](#hibernate-envers)
  - [Hibernate Search](#hibernate-search)
  - [Blaze Persistence](#blaze-persistence)

# Hibernate

- **Прим.**
  - Этот гайд написан по [доке](http://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#basic-provided) в будущем его дополнить читая доку с начала до конца.
  - Полностью перенести инфу https://habr.com/post/265061/
  - Короткая официальная статья о том как работает Hibernate и его transactions https://developer.jboss.org/wiki/SessionsAndTransactions?_sscc=t#This_is_all_very_difficult_cant_this_be_done_easier
  - Описаны различные алгоритмы, transaction, locks https://vladmihalcea.com

**Hibernate ORM** - это реализация JPA интерфейса. Прослойка между JDBC и application.

**Domain Model** - связь между таблицами и классами Java. Обязана иметь id поле. Hibernate лучше работает если связываемый класс это Plain Old Java Object (POJO) / JavaBean. Но можно использовать любой класс.

- **Types**
  
  - **Value types** - типы которые встраиваются в Entity
  - **Entity types** - обычные Entity

- **Основные классы** (в пакете `org.hibernate.*`):
  
  - **SessionFactory** - thread-safe (and immutable) представляет собой mapping для domain model к database. Это фабрика для `Session` классов. Нужно создавать **1ну** на все application, создание трудозатратно. `SessionFactory` создает services которые Hibernate использует во всех `Session(s)` таких как кэш L2, connection pools, transaction system integrations, etc.
  - **Session** - single-threaded, short-lived object, представлена как "Unit of Work" (единица работы) из паттерна PoEAA (Patterns of Enterprise Application Architecture). `Session` это обертка для JDBC `java.sql.Connection` и играет роль фабрики для `org.hibernate.Transaction`. Она реализует защиту от `repeatable read` для **persistence context** (aka first level cache или кэш L1). Реализует паттерн **Unit of Work** (группу действие, которые или выполнятся все, или откатятся). Можно сказать, что Session это "ворота в DB": делает map для Entity, выполняет очередь SQL (сразу или lazy).
  - **Transaction** - single-threaded, short-lived object используется приложением чтобы разграничить рамки физической transaction. API которое изолирует application от незкоуровневого управления транзакциями.

- **session-per-operation** - антипаттерн, потому что Session не синхронизированный объект и возможны race condition и др. проблемы.

- **session-per-application** - антипаттерн, вместо него рекомендуют использовать кэш L2, т.к. session может выполнятся в разных потоках, то так лучше не делать. Исключение если в application только 1 поток, тогда можно попробовать использовать (т.е. Session можно использовать только в одном потоке)

- **session-per-request** (на практике используется это) - **стандартный** паттерн

- **session-per-request-withetached-objects** - user загружает данные из DB, session заканчивается пока user **долго принимает решение** (**long conversation**), когда user продолжает что-то делать начинается уже другая session вместо того чтобы держать открытой все это время первую session (так понял описание я) 

- **session-per-conversation** - паттерн, размер Session больше обычного и охватывает несколько transactions, но flush() делается после завершения всех transactions

- **Методы Session**
  
  - **sessionFactory.getCurrentSession()** - если Session не создан, то создастся, иначе получим ссылку на созданную. Не нужно делать **close()** и **flush()** (он будет сделан после **commit()** transaction)
  - **sessionFactory.openSession()** - нужно самому **close()** в finally блоке

# Hibernate vs JPA

**Hibernate vs JPA.** JPA это набор interfaces. Hibernate это implementation of JPA. Подключив Hibernate можно использовать только JPA тогда код будет совместим с другими реализациями JPA. А можно использовать Hibernate specific функции, у них больше возможностей и часто лучше реализация, но код будет не совместим с другими реализациями JPA.

Для mapping обычно используют JPA и там где JPA реализаций не достаточно, то Hibernate annotation. Даже в документации избегают xml mapping.

- **Аналоги классов Hibernate и JPA**
  - **SessionFactory** эквивалентно `EntityManagerFactory` в JPA. Реализация `SessionFactory` эквивалентна реализации `EntityManagerFactory`.
  - **Session** эквивалентно `EntityManager` из JPA
  - **Transaction** эквивалентно `EntityTransaction` из JPA
  - **ImplicitNamingStrategy** и **PhysicalNamingStrategy** эквивалентно `ImplicitNamingStrategyJpaCompliantImpl` из JPA. JPA не разделяет имена на logical and physical (т.е. в чистом JPA только 1ин один класс-генератор имен).

***

**Конвертация `EntityManager` в `Session` в Hibernate.** Например когда Hibernate работает в **JPA-mode** в котором нету доступа к методам Hibernate.

```java
EntityManager em = ...
Session session = em.unwrap(Session.class);

// прим. можно unpacking в Connection
Connection cn = em.unwrap(Connection.class);
```

***

| Hibernate LockMode                                                | JPA LockModeType            |
| ----------------------------------------------------------------- | --------------------------- |
| NONE                                                              | NONE                        |
| OPTIMISTIC READ                                                   | OPTIMISTIC                  |
| OPTIMISTIC_FORCE_INCREMENT, WRITE                                 | OPTIMISTIC_FORCE_INCREMENT  |
| PESSIMISTIC_READ                                                  | PESSIMISTIC_READ            |
| PESSIMISTIC_WRITE, ~~UPGRADE~~ UPGRADE_NOWAIT, UPGRADE_SKIPLOCKED | PESSIMISTIC_WRITE           |
| PESSIMISTIC_FORCE_INCREMENT, ~~FORCE~~                            | PESSIMISTIC_FORCE_INCREMENT |

# Transaction

**JPA vs JDO** (Java Data Objects). JDO это спецификация не только для реляционных DB, но и других типов DB. JPA можно рассматривать как подмножество JDO.

**Transaction** - это единица работы обладающая ACID. Еще говорят, что она выполняет Unit of Work.

**auto-commit** для transactions отключен по умолчанию (самим Hibernate или JEE в случае JEE окружения). Его можно включить, но он медленне обычных transactions.

`@Transaction` - есть реализация JPA и Spring. При изпользовании Spring нужно использовать реализацию из Spring. Делает AOP обертку вокруг метода в котором происходит транзакция (над которым проставлена аннотация), эта обертка начинает, завершает, откатывает транзакцию автоматически. Проставлять эту аннотацию можно над методами слоя `@Serive` или слоя `@Repository`, но в стандартном Spring MVC проставляют **только над `@Service`** (потому что метод `@Service` это единица работы).  
**Tip.** При использовании Spring аннотация **принадлежит Spring** и нужно использовать ее (`org.springframework.transaction.annotation.Transactional`), реализована с использованием его AOP. При использовании JavaEE нужно использовать аннотацию `javax.transaction.Transactional`.

- **programmatic transaction management** - вызов методов `getTransaction`, `commit`, `close`, `rollback` вручную
- **declarative transaction management** - использование аннотаций

Нужно помнить, что **в JPA может не сработать `@Transaction(readOnly=true)`** и транзакция всеравно будет способна изменять данные в DB, потому что JPA **(не точно!)** общая спецификация и в некоторых системах такое может быть позволено. **При этом** использование readOnly через JDBC (вызовм функций вручную) может работать как надо.

**JTA** (Java Transaction API) - API, часть Java EE для управления транзакциями. Состояние Transaction хранится в TLS (Thread Local Storage), поэтому она может быть распостранена (Propagation Level) на все методы в call-stack без явной передачи своего объекта-контекста. Может объединять несколько resources, если они использованы в одной транзакции (XA, Global transaction).

**Рекомендуется** использовать **Transaction** в JTA окружении (в application server) или использовать standalone реализацию JTA. Т.е. jar с реализацией может принадлежать application server или быть подключенной отдельно.

**JTA vs JDBC Hibernate transaction.** В JTA **сначало создается transaction** из JTA, потом Session, причем Hibernate привязывает Session к этой Transaction автоматически. Если JTA недоступно можно использовать Hibernate transaction поверх JDBC. В JDBC окружении **сначало создается Session**, а из нее обычная Transaction (внутри используется Thread из Java).

```java
// JTA transaction (может быть global или local)
UserTransaction tx = (UserTransaction)new InitialContext().lookup("java:comp/UserTransaction");
tx.begin();
// Do some work
factory.getCurrentSession().load(...);
tx.commit();
```

```java
// JDBC transaction (local only)
factory.getCurrentSession().beginTransaction();
// Do some work
factory.getCurrentSession().load(...);
factory.getCurrentSession().getTransaction().commit();
```

- bean-managed transactions (**BMT**) - 
- container-managed transactions (**CMT**) - 

**Проверить факт:**

- Если метод с @Transaction вызовется другим методом @Transaction **из того же** класса, то Propagation не сработает и метод выполнится в той же транзакции???

Пример:

```java
@Repository
class DAOA {
    // @Transactional в @Repository
    @Transactional
    List getUsers() {
        return Criteria.createCriteria(User.class).list();
    }
}

// @Transactional в @Service
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {ConstraintViolationException.class})
public Long saveTask(Long userId) {
    Session session = sessionFactory.getCurrentSession();
    User user = userDao.getUserByLogin("user1");
    Tasks task = new Tasks();
    task.setName("Задача 1");
    // ...
    task.setUser(user);
    session.saveOrUpdate(task);
    return task.getTaskId();
}
```                       

# Cache

1. **First Level Cache (L1)** - включен по умолчанию, кэш в пределлах сессии. Привязан к Session, его нельзя отключить.
2. **Second Level cache (L2)** - выключен по умолчанию, кэш между сессиями (уменьшает трафик к БД). Кэш привязан к фабрике сессий (поэтому существует между сессиями).
   - across session in an application
   - across applications
   - across clusters
3. **Query cache** (формально это расширение кэша L2, данные берутся из кэша L2) - выключен по умолчанию, кэш запросов, кэширует используя запрос в качестве ключа (как в Map); берет результат из кэша, если запросы совпадают. Эффективен для read-only операций. Используется для запросов, которые не сохраняются в L1 и L2


**Note.** Если данные в DB обновляются самой DB (например через trigger), то изменения не отразятся в кэше L1 (не будут видны в приложении). Нужно обновлять вручную `session.refresh(myTable)`

**Note:** связанные Entity с кэшируемыми Entity по умолчанию не кэшируются. Нужно указывать @Cache для них отдельно.

**Note:** В кэше объекты не хранятся, т.к. занимают много памяти. Сам кэш реализован что-то вроде Map в которой ключ объекта это id, а хранятся строки, числа.
В Query cache ключи это не id объектов, а параметры запроса.

**Note:** First Level Cache еще называют persistence context.
Т.е. кэш L1 это на самом деле одна из основ Hibernate ORM к которой привязаны объекты и транзакции???

**Note:** Hibernate ORM пытается достать запрошенный объект из кэша L1, если там нету, то из L2, если и там нету, то из БД.

**Note:** При каждом non hibernate запросе SQL происходит полная очистка кэша L2. Чтобы синхронизировать состояние табл. и кэша. Если мы знаем какие данные изменятся, то можно вручную указать region (набор данных) который нудно чистить, чтобы не чистить весь кэш L2. Делается это через методы: `addSynchronizedEntityClass, addSynchronizedQuerySpace etc`

**Note:** Аннотацию @Cache можно использовать над классами или полями.

**Note:** Collection Cache - это указание кэшировать в L2 поля которые Collection отмеченные связями @OneToMany etc, т.к. они не кэшируются даже если класс в котором они находятся отмечен кэшируемым.

- **Методы Session:**
  - flush() — синхронизирует объекты сессии с БД и в то же время обновляет сам кеш сессии.
  - evict() — нужен для удаления объекта из кеша cессии (напр. кода данные устарели, т.е. обновить принудительно).
  - contains() — определяет находится ли объект в кеше сессии или нет.
  - clear() — очищает весь кеш.

## Подключение и примеры

**1) First Level Cache (L1)**

```java
session = sessionFactory.openSession();
session.openTransaction();
User user = (User) session.get(User.class, 1);
user.setName("New Name");
user = (User) session.get(User.class, 1); //т.к. объект в кэше Level 1, то 2-го SELECT к базе нету
session.getTransaction().commit();
session.close();
```

**2) Second Level cache (L2)**

**Включение**

1. Установить: ```cache.provider_class=org.hibernate.cache.EnCacheProvider``` (или другую реализацию)
2. Добавить jar реализации L2 кэша
3. Установить `cache.use_second_level_cache=true`

**Библеотки с реализацией L2 кэша:**

1. **EHCache** - это быстрый и простой кэш. Он поддерживает read-only и read/write кэширование, а так же кэширование в память и на диск. Но не поддерживает кластеризацию.
2. **OSCache** - это другая opensource реализация кэша. Помимо всего, что поддерживает EHCache, эта реализация так же поддерживает кластеризацию через JavaGroups или JMS.
3. **SwarmCache** - это просто cluster-based решение, базирующееся на JavaGroups. Поддерживает read-only и нестрогое read/write кэширование. Этот тип кэширование полезен, когда количество операций чтения из БД превышает количество операций записи.
4. **JBoss TreeCache** - предоставляет полноценный кэш транзакции.
   
```java
// 1.
session = sessionFactory.openSession();
session.beginTransaction();
User user = (User) session.get(User.class, 1);
session.getTransaction().commit();
session.close();

// 1. Т. к. объект не изменился и включен кэш второго уровня, то и в другой сессии, объект достается из кэша.
session = sessionFactory.openSession();
session.beginTransaction();
User user = (User) session.get(User.class, 1); //тут SELECT не выполнится, достанется из кэша L2
session.getTransaction().commit();
session.close();
```

**3) Query cache (extension of L2)**

**Включение**
1. Включить кэш L2
2. Установить `cache.use_query_cache=true`
```java
session = sessionFactory.openSession();
session.beginTransaction();
Query query = session.createQuery("from User");
query.setCacheable(true); //кэшируем результат запроса с такими ПАРАМЕТРАМИ
List1 = query.list();
session.getTransaction().commit();
session.close();

session = sessionFactory.openSession();
session.beginTransaction();
Query query2 = session.createQuery("from User");
query2.setCacheable(true); //кэшируем результат запроса с такими ПАРАМЕТРАМИ
List2 = query2.list(); //т.к. user не менялся в базу отправить ТОЛЬКО один SELECT
session.getTransaction().commit();
session.close();
```

## Когда не нужно использовать кэш L2 и Query cache

1. Если данные изменяются другим application
2. Если кэшированные данные не переиспользуются (то и кэш не нужен)
3. Если есть системы аудита SQL запросов и БД (то кэш они не увидят, нужны реальные запросы к БД)
4. Когда кэш L1 содержит все запрашиваемые данные (тогда и L2 не нужен)
5. Когда читаемых данных из БД слишком много и кэш не справляется

## Cache region

**cache region** - это регион к которому привязан кэш (логическая область памяти).  Для каждого региона можна настроить свою политику кеширования. **Если регион не указано**, то используется регион по умолчанию с именем класса к которому применено кэширование: `region name == пакет + класс + имя_поля`  
Пример имени region: `org.models.MyEntity.myField`

**Note:**
Collection которые внутри классов привязаны к отдельным регионам (т.е. те `List/Set/Map/Collection` что со связями `@ManyToOne`, `@OneToMany` etc.). Поэтому на них не действует настройка класса в которых они расположены. И поэтому их нужно ОТДЕЛЬНО отмечать как кэшируемые. В большинстве случаев итмечать кэшируемыми Collections, если родительский класс кэшируем - это хорошо.

**Note:**
Кэш нужно чистить, если он устарел. Т.к. кэш L2 чиститься весь в ситуациях, когда данные БД изменены, чтобы синхронизироваться с кэшем. То чтобы не чистить весь кэш L2 можно указать конкретный region кэша (класс, поле, запрос?), который нужно почистить.

- **Для очистки кэша** используется метод evict() или указание CacheMode (storage CacheStoreMode и retrieval CacheRetrieveMode в JPA):
  
  1. `session.getSessionFactory().getCache().evictQueryRegion( "query.cache.person" );` - чистка по region
  2. `session.setCacheMode( CacheMode.REFRESH )` - установка режима кэша, который будет срабатывать автоматом
  3. `users.put( "javax.persistence.cache.storeMode" , CacheStoreMode.REFRESH );` - указание поведения кэша при добавлении в Map

- **Cache modes relationships** - это режимы автоматической работы кэша. Если их установить, то кэш будет очищаться автоматически в соотв. с режимом.
  Устанавливаются глобально или прямо в функции-операции. Список режимов:
  
  - **CacheMode.NORMAL**
  - **CacheMode.REFRESH**
  - **CacheMode.PUT**
  - **CacheMode.GET**
  - **CacheMode.IGNORE**

**Очистка кэша по region.** Т.е. методы очистки содержат слово synchronized в том числе в xml (в случае xml конфигов)
Т.е. synchronized - какой region синхронизировать с DB, т.е. обновить для него кэш:

```java
// чистим кэш L2 не весь, а только привязанный к классу Foo (т.е. только объект Foo удалиться)
nativeQuery.unwrap(org.hibernate.SQLQuery.class).addSynchronizedEntityClass(Foo.class);
```

**Можно назначать регион:**

```java
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "STATIC_DATA") //включаем кэш и назначение ему региона
query.setCacheRegion("STATIC_DATA"); // назначаем регион для Query
criteria.setCacheRegion("STATIC_DATA"); // назначаем регион для Criteria
```

## Стратегии кэширования

- **Типы**:
  
  1. **NONE**
  2. **READ_ONLY** - для данных, который часто читаются, но не меняются
  3. **NONSTRICT_READ_WRITE** - для данных, которые часто читаются и редко меняются; применяется когда для данных не будет конфликтующих транзакций
  4. **READ_WRITE** - для данных, которые часто читаются/пишутся. При этом нельзя использовать с serializable transaction isolation (т.е. менее надежная для транзакций)
  5. **TRANSACTIONAL** - Полностью защищенные транзакции. Для реализаций кэша вроде JBoss TreeCache (не во всех реализациях кэша есть этот тип)

- **Требования** для стратегий кэша:
  
  - **NONSTRICT_READ_WRITE, READ_WRITE**:
    1. Транзакция должна завершиться до (!) вызова Session.close() or Session.disconnect()
    2. Для JTA environment нужно настроить `hibernate.transaction.manager_lookup_class`
  - **READ_WRITE**:
    cache implementation класс должен поддерживать locking (блокировки). Встроенная реализация может не поддерживать.
  - **TRANSACTIONAL**:
    Для JTA environment нужно настроить `hibernate.transaction.manager_lookup_class`

- **Настройка стратегий кэша** (providers кэша):
  
  1. **hibernate.cache.use_minimal_puts** - оптимизирует L2 cache операции writes, но ценой более частых reads
  2. **Опции** - могут оптимизировать или форматировать данные ценой производительности других операций. Через свойство (или другое для hibernate) `javax.persistence.sharedCache.mode` 
     - **ENABLE_SELECTIVE** - по умолчанию. Не кэширует, пока entity не отметить вручную как @Cacheable
     - **DISABLE_SELECTIVE** - как ENABLE_SELECTIVE, только наоборот
     - **ALL** - принудительно кэшировать и не смотреть на @Cacheable
     - **NONE** - принудительно не кэшировать
       ```java
       @Entity
       @Cacheable //указываем что класс надо кэшировать
       @Cache( //специфическая аннотация hibernate для настройки кэша
       usage = CacheConcurrencyStrategy.READ_ONLY
       )
       class User {}
       ```
       
# Entity

## Entity описание, операции, требования

**Entity** это легковесный хранимый объект бизнес логики (persistent domain object).

Если понадобится посмотреть **возможные генерируемые типы полей в DB** установленные аннотацией `@Type`, то в [этой](http://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#basic-provided) таблице (их много).

- **Типы маппинга (Mapping types)**
  1. basic (e.g String, int)
  2. Embeddable
  3. Entity

- **Возможные типы полей Entity в JPA** (помимо custom типов):
  
  1. примитивные типы и их обертки Java,
  2. String,
  3. реализующие Serializable интерфейс
  4. enums;
  5. entity types;
  6. embeddable классы
  7. java.math.BigInteger, java.math.BigDecimal
  8. java.util.Date, java.util.Calendar, java.sql.Date, java.sql.Time, java.sql.Timestamp
  9. Collection, Set, List, Map

- **Возможные типы primary key для Entity в JPA** - типы помимо тех что в этом списке **не будут переносимы** в другие DB. Только целочисленные типы **автогенерируемы**.
  
  1. примитивные типы и их обертки Java,
  2. String,
  3. BigDecimal и BigInteger,
  4. java.util.Date, java.sql.Date

- **По умолчанию для стобцов в JPA** (даже если аннотаций не стоит, но есть `@Entity`)
  
  1. `@Basic(optional = false, fetch = FetchType.EAGER)` над всеми столбцами
  2. `java.sql.Clob` или `java.sql.Blob`, то оно преобразуется в `@Lob`
  3. если тип свойства `Serializable`, то оно преобразуется как `@Basic` в столбец, содержащий объект в сериализованном виде;
  4. для свойства аннотированного как `@Embeddable`, его mapping получит значение `@Embedded`;

- **Что может Entity в JPA**:
  
  - наследовать non-entity classes
  - наследовать entity classes
  - наследоваться non-entity классом
  - быть abstract

- **Что обязан Entity в JPA**:
  
  - иметь `@Entity` аннотацию или аналог в xml
  - содержать хотя бы 1ин** public** или **protected** конструктор без аргументов
  - быть top-level class
  - не быть enum или interface
  - не быть final class
  - не содержать final methods или final variables на которых использован mapping
  - Если объект Entity класса будет передаваться по значению как отдельный объект (detached object), например через удаленный интерфейс (through a remote interface), он так же должен реализовывать Serializable интерфейс
  - все поля должны быть private
  - иметь id (primary key)

- **Типы элементов Entity в JPA**:
  
  1. **property access** - public свойства классов
  2. **field access** - get/set для private полей

- **Основные операции с Entity в JPA** - EntityManager это интерфейс, который описывает API для всех основных операций над Enitity, получение данных и других сущностей JPA. По сути главный API для работы с JPA. Основные операции:
  
  1. Для операций над Entity: persist (добавление Entity под управление JPA), merge (обновление), remove (удаления), refresh (обновление данных), detach (удаление из управление JPA), lock (блокирование Enity от изменений в других thread),
  2. Получение данных: find (поиск и получение Entity), createQuery, createNamedQuery, createNativeQuery, contains, createNamedStoredProcedureQuery, createStoredProcedureQuery
  3. Получение других сущностей JPA: getTransaction, getEntityManagerFactory, getCriteriaBuilder, getMetamodel, getDelegate
  4. Работа с EntityGraph: createEntityGraph, getEntityGraph
  5. Общие операции над EntityManager или всеми Entities: close, isOpen, getProperties, setProperty, clear

## Custom types полей

Помимо стандартных типов полей в Entity можно создать свои.

- **Использование:**
  1. реализуем обертки для преобразование в данные подходящтие для JDBC и обратно через методы wrap и unwrap
  2. регистрируем обертки в Hibernate и используем нужный тип для которых Hibernate будет использовать обертки

**Можно создать custom type:**

```java
// РЕАЛИЗАЦИЯ оберток
// реализуем AbstractStandardBasicType или AbstractSingleColumnStandardBasicType. Можно и BasicType, но там много лишних методов
public class BitSetType
        extends AbstractSingleColumnStandardBasicType<BitSet>
        implements DiscriminatorType<BitSet> {
    public static final BitSetType INSTANCE = new BitSetType();
    public BitSetType() {
        super( VarcharTypeDescriptor.INSTANCE, BitSetTypeDescriptor.INSTANCE );
    }
    @Override
    public BitSet stringToObject(String xml) throws Exception {
        return fromString( xml );
    }
    @Override
    public String objectToSQLString(BitSet value, Dialect dialect) throws Exception {
        return toString( value );
    }
    @Override
    public String getName() {
        return "bitset";
    }
}

// также реализуем AbstractTypeDescriptor для SQL
// содержит много методов. Главные это wrap для трансформации DBC column value object в BitSet (нужный тип). И unwrap чтобы использовать в PreparedStatement
public class BitSetTypeDescriptor extends AbstractTypeDescriptor<BitSet> { }

// СПОСОБ 1
// регистрация класса BitSetType
ServiceRegistry standardRegistry =
    new StandardServiceRegistryBuilder().build();
MetadataSources sources = new MetadataSources( standardRegistry );
MetadataBuilder metadataBuilder = sources.getMetadataBuilder();
metadataBuilder.applyBasicType( BitSetType.INSTANCE );

// использование
@Entity(name = "Product")
public static class Product {
    @Type( type = "bitset" ) private BitSet bitSet;
}

// СПОСОБ 2
// регистрация класса BitSetType
@Entity(name = "Product")
@TypeDef(
    name = "bitset",
    defaultForType = BitSet.class,
    typeClass = BitSetType.class
)

// использование
public static class Product {
    private BitSet bitSet;
}
```

# Naming strategies

Представлено классами `ImplicitNamingStrategy` и `PhysicalNamingStrategy` классами. Есть устаревшие NamingStrategy и ImprovedNamingStrategy классы. **В Spring Boot** устанавливается `spring.jpa.hibernate.naming.physical-strategy=com.mypackage.MyStrategy` и `hibernate.implicit_naming_strategy`.

**1. ImplicitNamingStrategy** - используется когда mapping не устанавливает имена явно таблице или столбцу. Hibernate имеет несколько out-of-the-box реализаций. Можно создать свое правило именования.  
Можно использовать `org.hibernate.boot.MetadataBuilder#applyImplicitNamingStrategy` во время Bootstrap (запуска), чтобы установить эту стратегию.

**Другими словами:** `ImplicitNamingStrategy` - это если в mapping не указано, что стобцу табл. `accountNumber` соответствует атрибут Entity класса `accountNumber`, то правило `ImplicitNamingStrategy` само подберет их соответствие.

**Стратегии** `ImplicitNamingStrategy` (пакет `org.hibernate.boot.model.naming.*`) - чтобы их установить используется свойство `hibernate.implicit_naming_strategy`. В качестве значения можно использовать "short name" (список ниже), полную ссылку на класс который реализует `ImplicitNamingStrategy` или FQN класса.

- **Список стратегий:**
  - **default** - класс `ImplicitNamingStrategyJpaCompliantImpl`
  - **jpa** - класс `ImplicitNamingStrategyJpaCompliantImpl` из JPA 2.0
  - **legacy-hbm** - оригинальная `NamingStrategy` стратегия из Hibernate (условно старая)
  - `ImplicitNamingStrategyLegacyHbmImpl`
  - **legacy-jpa** - `ImplicitNamingStrategyLegacyJpaImpl` старая стратегия JPA 1.0, которая плохо описана
  - **component-path** - `ImplicitNamingStrategyComponentPathImpl` почти как `ImplicitNamingStrategyJpaCompliantImpl`, только использует полные пути в именах вместо частичных окончаний (ending property part)

**2. PhysicalNamingStrategy** - задает имена database objects (tables, columns, foreign keys, etc). Идея в том чтобы не писать hard-code имена в mapping, а использовать общее правило. Например можно для атрибут Entity класса `accountNumber` использовать сокращение `acct_num` в табл.

**ImplicitNamingStrategy vs PhysicalNamingStrategy**. В некоторых случаях **ImplicitNamingStrategy** можно использовать для тех же целей что и **PhysicalNamingStrategy**, но не нужно этого делать, потому что идея в логическом разделении этих операций. **PhysicalNamingStrategy** будет применена даже если `@Column(name = "...")` задана. **ImplicitNamingStrategy** будет применена только если `@Column(name = "...")` не задана.

```java
public class CustomPhysicalNamingStrategy implements PhysicalNamingStrategy {
    // методы
}
```

# AttributeConverter

Позволяет объявить converter который будет применен к полю Entity.

**Note.** По умолчанию Enum в upper case, чтобы использовать в lower case нужно написать AttributeConverter.

**Например**, если поле содрежит сумму в деньгах, то чтобы конвертировать деньги в нужный формат поля Entity.

- Бывают - если тип поля Entity будет Immutable, то **Immutable**, если тип поля Mutable, то **Mutable**:
  - для **Immutable** types - String, a primitive wrapper (e.g. Integer, Long) an Enum type или другой не изменяемый Object, то можно только переназначить и нельзя менять сам объект
  - для **Mutable** types - их структуру можно менять и для них Hibernate будет менять значения в БД. Не смотря на то что для них работает L2 кэш, dirty checking etc они всеравно медленнее чем **Immutable**.
  - **Immutable vs Mutable** - предпочтительно использовать immutable, если это возможно. Это эффективнее.

```java
// создаем (пример для Immutable типа)
@Converter
public class PeriodStringConverter
        implements AttributeConverter<Period, String> {
    @Override
    public String convertToDatabaseColumn(Period attribute) {
        return attribute.toString();
    }
    @Override
    public Period convertToEntityAttribute(String dbData) {
        return Period.parse( dbData );
    }
}

// используем
@Entity(name = "Event")
public static class Event {
    @Convert(converter = PeriodStringConverter.class)
    private Period span;
}
```

# Generated properties

Обычно чтобы достать обновленные данные из DB нужно сделать `refresh`. `@Generated` позволяет Hibernate реагировать на изменение данных самой DB и автоматически обновлять эти данные в Entity. Наприме в случае когда данные изменены через SQL trigger.

- **GenerationTime** - указывает когда Hibernate должен автоматически обновлять данные
  - `NEVER` (default)
  - `INSERT` - только для insert, например для `creationTimestamp` своства
  - `ALWAYS` - для insert и update

```java
@Entity(name = "Person")
public static class Person {
    // поля firstName, middleNameN, ... properties here
    // ...
    @Generated( value = GenerationTime.ALWAYS )
    @Column(columnDefinition =
        "AS CONCAT(" +
        "    COALESCE(firstName, ''), " +
        "    COALESCE(' ' + middleName1, ''), " +
        "    COALESCE(' ' + middleName2, ''), " +
        "    COALESCE(' ' + middleName3, ''), " +
        "    COALESCE(' ' + middleName4, ''), " +
        "    COALESCE(' ' + middleName5, ''), " +
        "    COALESCE(' ' + lastName, '') " +
        ")")
    private String fullName;
}
```

# `@GeneratorType` annotation

Анотация позволяет задать свой генератор значений полей.

**Для чего:** Например в поле `updatedBy` делать `set()` текущего вошедшего User, доставая этот объект например из сессии Spring Security.

```java
// создаем
public static class LoggedUserGenerator implements ValueGenerator<MyType> {
    public MyType generateValue(Session session, Object owner){
        // return my-value;
    }
}

// используем
@Entity(name = "Person")
public static class Person {
    @GeneratorType(type = LoggedUserGenerator.class, when = GenerationTime.ALWAYS)
    private MyType updatedBy;
}
```

# Сложные структуры

## `@Embeddable`

Это class который не используется сам по себе, используется только совместно с Entity. Служит для выноса общих атрибутов нескольких классов в отдельный объект.

Если `@Embeddable` принадлежит нескольким Entity, то у каждого Entity будут свои копии этого `@Embeddable`.  
Указывать `@Embedded` не обязательно.

`@Parent` можно получить ссылку на родителя для `@Embeddable` из `@Embeddable`.

- **Что может в JPA:**
  - содержать базовые типы
  - содержать другой `@Embeddable`
  - иметь связи с другими Entity или коллекциями Entity, если он не primary key ли ключ map'ы
  - быть Collection, быть key или value в Map: `Map<String, MyEmbeddable> m;`
- **Что обязан в JPA:**
  - быть отмечен @Embeddable
  - иметь только такие же типы атрибутов как Entity, но без primary key

```java
    @Entity
    public class Person {
        @Embedded public Address address;
        public Address address2;
    }

    @Embeddable
    public class Address {
        // доступ к полю person.address.owner == person
        @Parent public Person owner;

        @Column(name = "publisher_name")
        private String name;
    }
```

## `@ElementCollection`

`@ElementCollection` для встраивание non-entity коллекций (`@OneToMany` для entity), имеет unidirectional связь. На самом деле генерируется отдельная таблица для коллекции с внешним ключем к ней. Не имеет отдельного lifecycle.

**Note из документации:** Set эффективен, ordered List менее эффективен, Bag (unordered Lists) самый не эффективный.

`@CollectionOfElements` это старый проприетарный аналог для `@ElementCollection`.

Есть мнение, что с @ElementCollection лучше хранить базовые типы и их оболочки: Integer, String etc. Вместо использования List<String> или Set<String>.

```java
class User {
    String name;
    @ElementCollection //эта
    Set<Address> addresses = new HashSet<>();
}

//как сделать во встраиваемой коллекции поле id, точнее таблице для неё (по умолчанию его там нет)
//@CollectionId - аннотация Hibernate
class User {
    String name;

    @ElementCollection
    @CollectionId //теперь таблица с коллекцией будет иметь столбец id
    private Collection<Address> address = new ArrayList<>();
}

//задаем столбцу первичного id красивое имя (аннотациями hibernate)
class User {
    String name;

    @ElementCollection
    @GenericGenerator(name = "hilo-gen", strategy = "hilo") //hilo - это тип hibernate
    @CollectionId(
        columns = {
            @Column(name = "ADDRESS_ID"
        },
        generator = "hilo-gen",
        type = @Type(type = "long")
    )
    private Collection<Address> address = new ArrayList<>();
}

// установка имен табл. и столбца
@ElementCollection
@CollectionTable(
    name = "book_author",
    joinColumns = @JoinColumn(name = "book_id")
)
private List<Author> authors = new ArrayList<>();

// установка имен табл. и столбца
@Temporal(TemporalType.TIMESTAMP)
@ElementCollection
@CollectionTable(name = "phone_register")
@Column(name = "since")
private Map<Phone, Date> phoneRegister = new HashMap<>();
```

## Типы связей Entity

- Существуют следующие четыре типа связей
  1. **OneToOne** (связь один к одному, то есть один объект Entity может связан не больше чем с один объектом другого Entity ),
  2. **OneToMany** (связь один ко многим, один объект Entity может быть связан с целой коллекцией других Entity),
  3. **ManyToOne** (связь многие к одному, обратная связь для OneToMany),
  4. **ManyToMany** (связь многие ко многим)
  5. **@ElementCollection** - выделяют в unidirectional связь
- Каждую из которых можно разделить ещё на два вида:
  1. Bidirectional - с двух сторон
  2. Unidirectional - с одной, Entity доступен только с одной стороны связаного Entity

**Bidirectional** — ссылка на связь устанавливается у всех Entity, то есть в случае OneToOne A-B в Entity A есть ссылка на Entity B, в Entity B есть ссылка на Entity A, Entity A считается владельцем этой связи (это важно для случаев каскадного удаления данных, тогда при удалении A также будет удалено B, но не наоборот).

**unidirectional**- ссылка на связь устанавливается только с одной стороны, то есть в случае OneToOne A-B только у Entity A будет ссылка на Entity B, у Entity B ссылки на A не будет. 

### Примеры типов связей Entity

## Mapped Superclass

**Mapped Superclass** это класс от которого наследуются Entity, он может содержать анотации JPA, однако сам такой класс не является Entity, ему не обязательно выполнять все требования установленные для Entity (например, он может не содержать первичного ключа). Такой класс не может использоваться в операциях EntityManager или Query. Такой класс должен быть отмечен аннотацией MappedSuperclass или соответственно описан в xml файле.

```java
//просто предок @Entity который нельзя использовать, может не иметь @Id
@MappedSuperclass
class User {}

@Entity
class MyUser extends User {}
```

## Inheritance Mapping Strategies

Источник: [тут](https://www.baeldung.com/hibernate-inheritance)

- Стратегии - как наследуемые классы Entity будут связаны друг с другом в DB.
  - **MappedSuperclass** – the parent classes, can’t be entities
  - **single table strategy** (default) - все наследники в 1ой табл. **Плюс подхода:** скорость, **минус:** расход памяти на пустые столбцы всех наследников (даже если записи не существует пустой столбец для наследника будет существовать). Для наследованных табл. имя их классов хранится в столбце `DTYPE`.
  - **joined strategy** - своя табл на каждого наследника, id и общие для наследников поля будут в общей для всех наследников табл. Нумерация id будет общая. **Плюс** экономия памяти, **минус** скорость (от join всех табл.)
  - **tabel per class** - тоже что и **joined strategy**, только вместо табл. с общими полями будут связи с табл.-предком. **Плюс** еще большая экономия памяти. **Минус** плохой polymorphic relationships и много отдельных sql запросов или использования UNION.

**Особенности:**

* **single table** - быстрее всего, можно использовать **polymorphic queries**, в наследниках нельзя использовать **not null constraints** для столбцов. Это риск data **inconsistencies.** Предок не обязан иметь id.
* **joined** - рекомендуется, если нужно использовать **polymorphic queries**
* **tabel per class** - **polymorphic queries** для нее очень сложные (затратные) и поэтому нужно их избегать. Предок обязан иметь id. В этом случае super class тоже Entity.

**polymorphic queries** - это когда начитываем класс наследник Entity и Hibernate автоматически отдает нам всех Entity наследников.

Через предка `@MappedSuperclass` от которого наследуются Entity (стратегиями наследовния) можно начинать всех наследников (sub-class entities) через **polymorphic queries**. Даже через наследуемые **не Entity** классы и interfaces можно начинать наследников.

Если используемые в **polymorphic queries** классы не Entity, то в запросе нужно использовать fully qualified name (FQM, путь состоящий из названия пакета + имя класса), чтобы Hibernate мог их найти, т.к. класс не **managed.**

При этом начинатанные таблицы наследники будут автоматически join.

```java
// получим все записи таблиц наследников Person
assertThat(session.createQuery(
    "from com.baeldung.hibernate.pojo.inheritance.Person")
    .getResultList())
    .hasSize(1);

// или
session.find(Person.class, 1);
```

**Запретить Polymorphic Queries** можно указав параметр **@Polymorphism(type = PolymorphismType.EXPLICIT):**

```java
@Entity
@Polymorphism(type = PolymorphismType.EXPLICIT)
public class Bag implements Item { ...}
```

**Меняем имя столбца `DTYPE` через `@DiscriminatorColumn`:**

```java
// задаем имя вместо DTYPE для всех наследников этого класса
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
    name = "VEHICLE_TYPE", 
    discripinatorType = DiscripinatorType.STRING //тип столбца DTYPE
    //discripinatorType = DiscripinatorType.INTEGER
    //discripinatorType = DiscripinatorType.CHAR
)
class Vehicle {
    int id;
    String name;
}

// можно установаить в столбце DTYPE другое значение только для одного наследника
@Entity
@DiscriminatorValue("Bike")
class TwoWheelerVehicle extends Vehicle {
    private String SteeringWheel;
}
```

## Используем ids совместно с связями через Entity
Можно использовать **одновременно** ids и связями через Entity. Для этого нужно поставить  
`insertable = false, updatable = false`  
но **нельзя** делать `save` через них. И они **не начитаются** после `save`, только после `findOne/findAll/load/get`

```java
@JoinColumn(name = "question_id")
@ManyToOne(targetEntity = Question.class, fetch = FetchType.LAZY)
Question question;

@OneToMany
List<Branch> branches;

@Column(name = "question_id", insertable = false, updatable = false)
Long questionId;

@ElementCollection
@CollectionTable(name = "branch_calendar_relation", joinColumns = @JoinColumn(name = "calendar_id"))
@Column(name = "branch_id", insertable = false, updatable = false)
@LazyCollection(LazyCollectionOption.FALSE)
@Fetch(FetchMode.SUBSELECT)
List<Long> branchIds; 
```

# Fetching (Стратегии загрузки коллекций в Hibernate)

Ссылки:

* https://docs.jboss.org/hibernate/orm/current/userguide/html_single/chapters/fetching/Fetching.html
* https://dou.ua/lenta/articles/jpa-fetch-types/
* https://dou.ua/lenta/articles/hibernate-fetch-types/

## Типы fetch

1. **FetchType.EAGER** — загружать коллекцию дочерних объектов сразу же, при загрузке родительских объектов.
2. **FetchType.LAZY** — загружать коллекцию дочерних объектов при первом обращении к ней (вызове get) — так называемая отложенная загрузка

## fetching стратегии

1. **SELECT** - загружает связанные **коллекции отдельными запросами** (целые коллекции, а не записи каждой из них)
2. **JOIN** - загружать все связанные коллекции **одним запросом используя LEFT JOIN**

## N + 1 selects problem

Если есть OneToMany связь между табл. A и B. И пользователь выберет все записи из A. То на выборку из A получится **1 запрос** в DB, и на выборку всех записей из B, которые привязаны к выбранным записям из A потратиться **N запросов**, по 1му на каждую сущность из B. Т.е. запросы в связанную табл. B не будут объединены в 1ин запрос. Получается **N + 1** запрос на выборку вместо 1го запроса.

**Как избежать:**

1. Не использовать **eager**, считается что eager почти не имеет реального применения (использовать lazy, lazy стоит по умолчанию)
2. Использовать **Query**, написанный вручную
3. Использовать **Join Fetch** / **Entity Graphs**. Для join fetch нужно **поставить eager** (логично, иначе запросы будут только по мере надобности)

## JOIN FETCH (решение проблемы N + 1 selects)

На самом деле для **JOIN FETCH** используется **LEFT JOIN**.

> **JPQL и JPA Criteria** по запросу **fetch join** вернут декартово произведение (cartesian product). **DISTINCT** может убрать дубли строк в cartesian product.

**Note.** декартово произведение это **не тоже** что и **full outer join**. Возможно тут имеется ввиду что-то другое? (уточнить это). В документации про JOIN сказано: Inherently an EAGER style of fetching. The data to be fetched is obtained through the use of an **SQL outer join**.

> **Только одна связанная коллекция**, которая загружается стратегией JOIN может быть типа **java.util.List**, остальные коллекции должны быть типа **java.util.Set**. Иначе, будет выброшено исключение: `HibernateException: cannot simultaneously fetch multiple bags`

> При использовании стратегии загрузки JOIN методы **setMaxResults** и **setFirstResult** не добавят необходимых условий в сгенерированный SQL запрос. Результат SQL запроса будет содержать все строки без ограничения и смещения согласно firstResult/maxResults. Ограничение количества и смешение строк будет **применено в памяти** (но не к запросам). Также будет выведено предупреждение:
> 
> `WARN HHH000104: firstResult/maxResults specified with collection fetch; applying in memory!`  
> 
> Если фильтрация в памяти вызывает проблемы, не используйте **setFirsResult**, **setMaxResults** и **getSingleResult** со стратегией загрузки JOIN.

В JPQL и JPA Criteria стратегия загрузки по умолчанию: **SELECT**

Пример:

```sql
-- Загрузит только Employee,
-- department будет загружен при первом запросе
FROM Employee emp
JOIN emp.department dep

-- Загрузит Employee и department сразу одим запросом
FROM Employee emp
JOIN FETCH emp.department dep
```

**1. JPQL пример**  
**Возможная проблема:** результат запроса будет содержать слишком большое количество записей.  
**Решение:** использовать 2ва запроса вместо одного <sub>(нет, ниже пример не об этом)</sub>

```java
// JOIN FETCH
List<Book> books = em.createQuery("select b from Book b left join fetch b.authors order by b.publicationDate")
    .getResultList();
assertEquals(3, books.size())

// JOIN FETCH + distinct (избавляемся от дублей)
List<Book> books = em.createQuery("select distinct b from Book b left join fetch b.authors order by b.publicationDate")
    .getResultList();
assertEquals(2, books.size());
```

**2. JPA Criteria пример**

```java
// JOIN FETCH
CriteriaBuilder cb = em.getCriteriaBuilder();
CriteriaQuery<Book> cq = cb.createQuery(Book.class);
Root<Book> book = cq.from(Book.class);
book.fetch(Book_.authors, JoinType.LEFT);
cq.orderBy(cb.asc(book.get(Book_.publicationDate)));
TypedQuery<Book> q = em.createQuery(cq);
List<Book> books = q.getResultList();      
assertEquals(3, books.size());

// JOIN FETCH + distinct (избавляемся от дублей)
CriteriaBuilder cb = em.getCriteriaBuilder();
CriteriaQuery<Book> cq = cb.createQuery(Book.class);
Root<Book> book = cq.from(Book.class);
cq.distinct(true);
book.fetch(Book_.authors, JoinType.LEFT);
cq.orderBy(cb.asc(book.get(Book_.publicationDate)));
TypedQuery<Book> q = em.createQuery(cq);
List<Book> books = q.getResultList();
assertEquals(2, books.size());
```

## List vs Set

Hibernate **не может** загрузить **несколько разных List** из одной Entity в одном запросе. Нужно или использовать несколько запросов в каждом из которых загрузить свой List **или** использовать Set.

Для использования Set нужно реализовать **equals** and **hashCode**, т.к. они используются в сравнении HashSet (сравнение по хэшам).  
**Note.** реализовать **equals** and **hashCode** **только** способом рекомендованным в документации! обычные реализации не подойдут!

Для **Set** (unordered) **нельзя** использовать `@OrderBy` в отличии от **List** (ordered). Но можно использовать `@Sort` **вместе с** `SortedSet` (прим. есть еще `@SortComparator` для передачи `Comparator` и `@SortNatural` для использования с `Comparable`):

```java
@OneToMany
@OrderBy("lastname ASC")
public List<Rating> ratings;

@OneToMany
@Sort(type = SortType.COMPARATOR, comparator = TicketComparator.class)
public Set<Rating> ratings = new SortedSet<>();
```

## Entity Graph (решение проблемы N + 1 selects)

**Суть:** указываем какие из связанных сущностей (табл.) делать JOIN при запросе.

**Пример**

```java
// 1.
EntityGraph<Book> fetchAuthors = em.createEntityGraph(Book.class);
fetchAuthors.addSubgraph(Book_.authors);
List<Book> books = em.createQuery("select b from Book b order by b.publicationDate")
    .setHint("javax.persistence.fetchgraph", fetchAuthors)
    .getResultList();        
assertEquals(3, books.size());

// 2.
@Entity(name = "Project")
@NamedEntityGraph(name = "project.employees",
    attributeNodes = @NamedAttributeNode(
        value = "employees",
        subgraph = "project.employees.department"
    ),
    subgraphs = @NamedSubgraph(
        name = "project.employees.department",
        attributeNodes = @NamedAttributeNode( "department" )
    )
)
public static class Project {
    @Id
    private Long id;

    @ManyToMany
    private List<Employee> employees = new ArrayList<>();
}
```

## Hibernate fetch стратегии, отличие от JPA: `FetchMode`, `SUBSELECT`, `@BatchSize`

**Hibernate fetch vs JPA fetch:**  

1. **Hibernate Criteria** (речь не о HQL!) по умолчанию использует `FetchMode.JOIN` для `EAGER` и `FetchMode.SELECT` для `LAZY`
2. **HQL** при `@Fetch(FetchMode.JOIN)` игнорирует **LAZY** и **EAGER** и работает как `FetchMode.SELECT` в режиме **EAGER**. **Т.е.** HQL игнорирует `FetchMode.JOIN` и нужно использовать **join fetch** в **самом** HQL запросе
3. **Hibernate Criteria** при `@Fetch(FetchMode.JOIN)` работает в **EAGER**. (не точно, ПРОВЕРИТЬ!)

```java
// тут на самом деле работает как FetchMode.SELECT
// хотя помечено как FetchMode.JOIN
// чтобы работало как JOIN нужно использовать join fetch в самом запросе
List books = getCurrentSession().createQuery("select b from BookFetchModeJoin b").list();
assertEquals(4, books.size());

// вот так будет join fetch, поведение как FetchMode.JOIN
List books = getCurrentSession().createQuery("select b from BookFetchModeJoin b join fetch b.authors a").list(); 
```

### `@BatchSize` c `FetchMode.SELECT`

**@BatchSize** указывает сколько запросов использовать для загрузки связанных коллекций **каждой полученной записи** корнегово запроса.  
Т.е. если в табл. **A** 2 записи, и к каждой такой записи привязано по 4 записи из табл. **B**, то при `@BatchSize(size = 2)` будет `2 * 2 + 1 == 5` запросов вместо `2 * 4 + 1 == 9` запросов без `@BatchSize`.

Внутри **@BatchSize** использует **IN-restriction** (из SQL).

Может быть **EAGER** (2ой запрос выполнится сразу) или **LAZY** (второй запрос выполнится, когда данные понадобятся)

```java
@Entity
public class BookBatchSize extends AbstractBook {
    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SELECT)
    @BatchSize(size = 2)
    private List<Author> authors = new ArrayList<>();
}
```

### `FetchMode.SUBSELECT`

**FetchMode.SUBSELECT** использует 2 запроса, один для загрузки Entity и
второ в виде подзапроса для загрузки всех связанных записей.

Может быть **EAGER** (2ой запрос выполнится сразу) или **LAZY** (второй запрос выполнится, когда данные понадобятся)

```java
@Entity
public class BookFetchModeSubselect extends AbstractBook {

    @ManyToMany(fetch = FetchType.EAGER)
    @Fetch(FetchMode.SUBSELECT)
    private List<Author> authors = new ArrayList<>();

    @ManyToMany
    @Fetch(FetchMode.SUBSELECT)
    private List<Category> categories = new ArrayList<>();

    /*...*/
}
```

## `@LazyCollection`

**@LazyCollection** - нету в JPA, из Hibernate ORM. Указывает как загружать связанную коллекцию.
**Значения:** (`TRUE` и `FALSE` - deprecated и нужно использовать значения из `FetchType`)

* **FetchType.LAZY** (`TRUE`) - как lazy
* **FetchType.EAGER** (`FALSE`) - как eager
* **EXTRA** - нету в JPA, не загружает Collection целиком даже когда к ней было первое обращение, каждый элемент коллекции загружается отдельным запросом. Работает только с **ordered collections** (`List` + `@OrderColumn` или `Map`), для `Bag` (unordered `List`) ведет себя как `FetchType.LAZY`
  
  ```java
  @Entity
  public static class Department {
    @OneToMany(mappedBy = "department", cascade = CascadeType.ALL)
    @OrderColumn(name = "order_id")
    @LazyCollection( LazyCollectionOption.EXTRA )
    private List<Employee> employees = new ArrayList<>();
  }
  ```

# Mapping

## Mapping annotations

Генерированные имена могут быть некрасивыми, чтобы использовать свои нужно указать их явно:

```java
@Table(name = "my_name") class MyTable {}
```

```java
@ManyToOne
@JoinColumn(name = "item_type_id")
```

## Типы Collection в связях

- Источник:
  - http://viralpatel.net/blogs/hibernate-one-to-many-xml-mapping-tutorial/
  - https://stackoverflow.com/questions/13812283/difference-between-set-and-bag-in-hibernate

- **Bag** - концепция, НЕУПОРЯДОЧЕННАЯ коллекция, которая может содержать дубликаты.
  - В Java (JPA) ее аналог это List
- **Set** - концепция, НЕУПОРЯДОЧЕННАЯ коллекция (но МОЖЕТ быть отсортирована - через спец. параметр), тоже что и Bag, но хранить может только уникальные объекты. При попытке добавления дубликата, он заменит совпадающий объект.
  - В Java (JPA) ее аналог это Set

Map - тоже можно использовать (вот тут неточно!)

```java
@OneToMany()
@JoinColumn(name="A_ID", insertable=false, updatable=false)
@MapKeyColumn(name="B_ID") // вот тут в JPA в отличии от Hibernate немного по другому, посмотреть аннотацию @MapKey
private Map map = new HashMap();
```

## `AttributeOverride`

- `@AttributeOverride` - меняем название столбцов в таблице, если встраиваем одинаковые @Embedded объекты. Чтобы не было конфликта одинаковых имен столбцов. Так можно перекрывать и атрибуты от суперкласса @MappedSuperclass

- `@AttributeOverrides` - массив таких аннотаций, пара для каждого столбца
  
  ```java
  class User {
    String name;
  
    @Embedded
    @AttributeOverrides ({
        @AttributeOverride (
            name = "street", //старое имя столбца
            column = @Column(name = "homeStreet") //новое имя столбца
        ),            
        @AttributeOverride (
            name = "city",
            column = @Column(name = "homeCity")
        ),            
        @AttributeOverride (
            name = "phone",
            column = @Column(name = "homePhone")
        )
    })
    Address homeAdress;
  
    @Embedded
    Address workAdress;
  }
  ```
  
# Persistence Context

## Hibernate Entity Lifecycle

Методы `Session` (`EntityManager`) меняют состояние связей Entity с `persistence context`.

Говорят что entity ассоциирован с `persistence context` или ассоциирован с `Session`. Когда он связан с сессией и его изменения синхронизируются с DB.

`persistence context` - мето где `data` конвертируется в `entity`. Это implementation of **Unit of Work** паттерна, отслеживает загруженные в него данные и синхронизирует с DB. `Session` это implementation of `persistence context`.  
**lifecycle** - это то как entity связан с `persistence context`.  
Unit of Work - еше называют "business transaction".

- **Состояния Entity:**
  - **transient** - id обычно не назначен, объект ВНЕ (ДО открытия) сессии (объект никогда не был связан с сессией). В transient переходят и объекты после удаления delete() внутри сессии (потому что они уже не в БД)
  - **persistent** (managed) - id назначен, связан с сессией (ВНУТРИ неё), в том числе если он взят из базы через get().
    - **Переходит в этот state после методов:** save()/persist(), saveOrUpdate(), update()/merge(), lock(), get(), load()
  - **detached** - id назначен, объект ПОСЛЕ session.close() или evict() (entity больше не связан с сессией)
  - **removed** - id назначен, связан с сессией, но в очереди на удаление из DB

```java
session.openTransaction();
session.save(user); // user перешел в состояние persistent и его изменения отслеживаются
session.setName("my"); //если внутри сессии, автоматически выполнится UPDATE
session.setName("my 2"); // вызван будет только "my 2" UPDATE, то есть повторно не вызывается (кэширование L1)
session.getTransaction().commit();
```

## Методы Session (включая JPA vs Hibernate методы)

**JPA vs Hibernate методы** - изначально в Hibernate и JPA были свои методы. Затем в Hibernate добавили методы JPA, для большей совместимости. Некоторые методы похожи по функционалу, но их работа в может отличаться.

Важно понимать, что методы `persist, save, update, merge, saveOrUpdate` не меняют данные в DB сразу. Запросы SQL такие как UPDATE or INSERT отправляются только после flush и commit (т.е. в рамках Spring приложения это в конце методов сервиса отмеченных `@Transactional`)

- **save vs persist**
  - **save** генерирует и возвращает id, **persist** ничего не возвращает и может создать id когда захочет до flush(). При типе генерации id `Identity`, вне transaction или с `Flush.MANUAL` для **persist** Hibernate задержит выполнение insert и создаст временный id, но для **save** выполнит insert сразу и получит id из DB.
- **update vs merge** - **update** называют `Reattaching` и оно работает только в Hibernate, . Если обьекта нет в Session, то **рекомендуется update**, если был или не известно был ли, то **рекомендуется merge**. При **update** если Entity будет **detached**, то выбросится `NonUniqueObjectException`
- **saveOrUpdate**
- **replicate** — преобразует объект из detached в persistent, при этом у объекта обязательно должен быть заранее установлен Id. Данный метод предназначен для сохранения в БД объекта с заданным Id, чего не позволяют сделать persist() и merge(). Если объект с данным Id уже существует в БД, то поведение определяется согласно правилу из перечисления `org.hibernate.ReplicationMode`: 
  - `ReplicationMode.IGNORE` — ничего не меняется в базе.
  - `ReplicationMode.OVERWRITE` — объект сохраняется в базу вместо существующего.
  - `ReplicationMode.LATEST_VERSION` — в базе сохраняется объект с последней версией.
  - `ReplicationMode.EXCEPTION` — генерирует исключение.
- **merge** — преобразует объект из transient или detached в persistent. Если из transient, то работает аналогично persist() (генерирует для объекта новый Id, даже если он задан), если из detached — загружает объект из БД, присоединяет к сессии, а при сохранении выполняет запрос update
- **refresh** — обновляет detached-объект, выполнив select к БД, и преобразует его в persistent
- **persist** — преобразует объект из transient в persistent, то есть присоединяет к сессии и сохраняет в БД. Однако, если мы присвоим значение полю Id объекта, то получим PersistentObjectException — Hibernate посчитает, что объект detached, т. е. существует в БД. При сохранении метод persist() сразу выполняет insert, не делая select.
- **delete** - удаляет объект из БД, иными словами, преобразует persistent в transient. Object может быть в любом статусе, главное, чтобы был установлен Id.
- **save** — сохраняет объект в БД, генерируя новый Id, даже если он установлен. Object может быть в статусе transient или detached
- **update** — обновляет объект в БД, преобразуя его в persistent (Object в статусе detached)
- **get** - получает из БД объект класса-сущности с определённым Id в статусе persistent
- **scroll**
- **list**
- **iterate**
- **load**
- **evict**
- **find**

# `unwrap()` method
Метод который позволяет переключиться с использования JPA на использование реализаций из **JPA provider** (**e.g.** Hibernate ORM вместо JPA), т.к. в таких реализациях есть спец. методы с большими возможностями.
```java
EntityManager em = new EntityManager();

Session session = em.unwrap(Session.class); // 1. Session

SessionFactory sessionFactory = em.getEntityManagerFactory().unwrap(SessionFactory.class); // 2. SessionFactory

CriteriaQuery<Object> query = criteriaBuilder.createQuery();
TypedQuery<Object> typedQuery = em.createQuery(query);
Query query = typedQuery.unwrap(Query.class); // 3. Query
```

# Bootstrap

Тут будут краткие сведения о том как запускается и конфигурируется Hibernate и о классе ServiceRegistry

http://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#bootstrap

# Exceptions и их обработка

**Коротко.** Hibernate оборачивает Exceptions из JDBC в свои Exceptions. Spring в свою очередь оборачивает в свои Exceptions. При этом аннотация `@Repository` добавляет доп. обработку таким исключениям.

***

**1. В Spring для обработки ошибок транзакций.**  **1)** создать спец. контроллер и **2)** указать какое исключение обрабатывает метод:

1. Выбрасываемое исключение **должно быть наследником `RuntimeException` чтобы прервать программу**.
2. Для обработки создать спец. контроллер и указать какое исключение обрабатывает метод (указать тип исключения)ю Доп. инфа о обработчиках ошибок в Spring MVC: см. [@ControllerAdvice, @ExceptionHandler, HandlerExceptionResolver](https://www.journaldev.com/2651/spring-mvc-exception-handling-controlleradvice-exceptionhandler-handlerexceptionresolver)

```java
class RoleNotExistenceExeption extends RuntimeException {}

// этот метод выполнится если внутри @Transaction будет указанное исключение
@ControllerAdvice
public class ExceptionController {
    @ExceptionHandler(RoleNotExistenceExeption.class)
    public ModelAndView roleNotFoundPage(RoleNotExistenceExeption ex) {
        ModelAndView model = new ModelAndView();
        model.addObject("msg", ex.getMessage());
        model.setViewName("error/error");
        return model;
    }

    //@ExceptionHandler(RuntimeException.class)
    public ModelAndView roleNotFoundPage(RuntimeException ex) {
        ModelAndView model = new ModelAndView();
        model.setViewName("error/some");
        return model;
    }
}
```

***

**2. Поведение при Exception внутри транзакции:**

- **rollbackFor** указывает исключения, при выбросе которых должен быть произведён откат транзакции
- **noRollbackFor**, указывающий, что все исключения, кроме перечисленных, приводят к откату транзакции

```java
@Transactional(propagation = Propagation.REQUIRED, rollbackFor = {ConstraintViolationException.class})
public Long saveTask(Long userId) {
    Session session = sessionFactory.getCurrentSession();
    User user = userDao.getUserByLogin("user1");
    Tasks task = new Tasks();
    task.setName("Задача 1");
    ...
    task.setUser(user);
    session.saveOrUpdate(task);
    return task.getTaskId();
}
```

- При откате транзакции не происходит откат состояния (значение полей) в Entity учавствующих в транзакции? (**проверить**)

# Настройка Hibernate и Spring

## Как Transaction работает внутри

**Кратко.** Spring не управляет транзакциями. Он для описания правил, все управление делегируется к DB (возможно в случае Hibernate делегируется к Hibernate?). Spring внутри запускает AOP и создает proxy бинов для конфигурации. Реализует различные интерфейсы из JTA и свои собственные для декларативного управления. Активно используется класс бин TransactionInterceptor и регистрирует context. Создает TransactionManager, есть различные типы TransactionManager (можно создавать свои с различными правилами кэширования и прочими). SpringTransactionAnnotationParser парсит аннотации транзакций (можно написать свой). Класс PlatformTransactionManager тоже ктивно используется.

## Аннотация `@Transactional`
Source: [1](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/transaction/annotation/Transactional.html)

**By default**, если не установлены **rollbackFor** и **noRollbackFor** вызовется **rollback** только для **unchecked exception** (`RuntimeException` и `Error`). Обычные **checked exception** прервут код в точке их throw но не сделают **rollback**.  
Опции **rollbackFor** и **noRollbackFor** будут действовать только на текущий **Thread** (e.g. `@Async` и `REQUIRES_NEW` (???) работать не будут).

```java
@Transactional(
    isolation = DEFAULT,
    propagation = REQUIRED,
    readOnly = false,
    rollbackFor = MyExceptionCauseRollback.class,
    rollbackForClassName = {"MyExceptionCauseRollback"},
    noRollbackFor = MyRuntimeExceptionCauseNoRollback.class, // no rollback for RuntimeException?
    noRollbackForClassName = {"MyRuntimeExceptionCauseNoRollback"},
    timeout = 10, // seconds
    timeoutString = "10",
    transactionManager = "txManager2",
    value = "txManager2" // == transactionManager
)
void f1() { }
```

## Старый вариант с HibernateUtil

`HibernateUtil` - класс можно встретить в старом коде, обертка для создания `Session` как **Singleton** и работе с ней.

```java
public class HibernateUtil {
    private static SessionFactory sessionFactory;

    private static SessionFactory buildSessionFactory() {
        StandardServiceRegistry standardRegistry = new StandardServiceRegistryBuilder().
                configure("hibernate.cfg.xml").build();
        Metadata metadata = new MetadataSources(standardRegistry).getMetadataBuilder().
                build();
        SessionFactoryBuilder sessionFactoryBuilder = metadata.getSessionFactoryBuilder();
        SessionFactory sessionFactory = sessionFactoryBuilder.build();

        return sessionFactory;
    }

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            sessionFactory = buildSessionFactory();
        }
        return sessionFactory;
    }
}
```

## Вариант с ручным созданием разных Bean в `@Configuration` для Hibernate + Spring

- **Чтобы работали транзакции (настройка транзакций для Spring + Hibernate):**
  1. Создать `DataSource` для JDBC и указываем параметры подключения к DB
  2. Создаем менеджер транзакций (источник бина). **Можно указать несколько и тогда нужно каждой задать имя.** Если transaction manager только один, то имя можно не указывать, Spring сделает Autowired по типу transaction manager. В transaction manager передаем ссылку на `DataSource`
  3. Проставить аннотацию `@Transaction` Можно указать `@Transaction(value = "MyTxName")` имя конкретного менеджера, если их несколько.
  4. Использовать аннотацию `@EnableTransactionManagement` в `@Configuration` чтобы запустить работу с транзакциями (это аналог `<tx:annotationriven/>` из xml конфигурации)

## xml конфигурация

**Note.** На звания параметров `xml` и `Java Config` не всегда совпадают.

```xml
<!-- Пример файла src/main/resources/hibernate.cfg.xml -->
<!--
Файлы Entities обычно делят на куски
src/main/resources/user.cfg.xml
src/main/resources/phone.cfg.xml
-->
<hibernate-configuration>
    <session-factory>
        <property name="connection.driver_class">com.mysql.jdbc.Driver</property>
        <property name="connection.url">jdbc:mysql://...</property>
        <property name="connection.user">root</property>
        <property name="connection.password">dwqdqwdf</property>
        <property name="connection.show_sql">dwqdqwdf</property>
        <property name="hbm2ddl.auto">create</property> <!-- или update, или validation, create-drop -->

        <property name="connection.pool_size"> <!-- - чтобы настроить размер смотреть документацию -->
		<property name="dialect"> <!-- - какой SQL язык для базы использовать (каждый диалект это класс описывающий язык) -->
		<property name="cache.provider_class"> <!-- - использовать или нет кэш L2 -->
		<property name="show_sql"> <!-- - показывать или нет запросы в консоли при их выполнении -->

        <mapping resource="hibernate/user.cfg.xml" />
    </session-factory>
</hibernate-configuration>
```

**Специфичные атрибуты:**
- **bag** - видимо это обычный `List`
    ```xml
    <class name="Category">
        <id name="id" column="CATEGORY_ID"/>
        <!-- ... -->
        <bag name="items" table="CATEGORY_ITEM">
            <key column="CATEGORY_ID"/>
            <many-to-many class="Item" column="ITEM_ID"/>
        </bag>
    </class>
    ```

## Конфигурация в Spring Boot через файл настроек
тут будет описание

# Misc

## Кавычки в именах Entity, использование зарезервированных имен

Нельзя в использовать зарезервированные слова вроде **Order**, будет ошибка. Но можно включить обрамление имен в кавычки через глобальные настройки или указать имя в `@Column(name = "\"Order\"")`

## Тип в котором хранить деньги, BigDecimal

**BigDecimal** преобразуется в `decimal(19,2)` в SQL

```java
@Entity
class Item {
    private BigDecimal cost; //преобразуется в decimal(19,2) - в SQL
}
```

## Особенности работы с БД MySQL

Согласно этой статье https://www.thoughts-on-java.org/5-things-you-need-to-know-when-using-hibernate-with-mysql/

1. **По умолчанию hibernate 5 для mysql выбирает стратегию - GenerationType.TABLE**, но она медленная (до этого выбирал GenerationType.IDENTITY и она ОК). Поэтому для скорости НУЖНО указать вручную:
   ```java
   @Id
   @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
   @GenericGenerator(name = "native", strategy = "native")
   @Column(name = "id", updatable = false, nullable = false)
   private Long id;
   ```

**generator = "native"** - заставляет выбирать стратегию БД по умолчанию (в данном случае GenerationType.IDENTITY)

При этом самой быстрой стратегией считается GenerationType.SEQUENCE, но ее нету в MySQL

**2. Для работы с специфичными для БД типов данных нужно реализовать AttributeConverter**

**3. Диалекты MySQL**  
InnoDB - (рекомендуется) движок поддерживает транзакции и lock on row
MyISAM - не поддерживает транзакции стоит по умолчанию (видимо при использовании не сигнализирует через Hibernate о том что он не поддерживает транзакции и др. технологии)

различия в деталях: https://stackoverflow.com/questions/15678406/when-to-use-myisam-and-innodb

- (Deprecated) MySQLInnoDBDialect - добавляет type=InnoDB к таблицам (использует движок InnoDB). И еще зачем-то добавляет cascade delete по умолчанию
  - MySQLDialect - использует движок по умолчанию MyISAM (видимо обертка для MySQLMyISAMDialect)
  - MySQLMyISAMDialect - видимо диалект для движка MyISAM
  - MySQL57InnoDBDialect и MySQL57Dialect - последние на момент написания версии диалекта, наследующие всех предков

SpatialDialect - группа деалектов для работа с геометрическими типами данных в БД: точки, области и т.д.

ТАКЖЕ!!!
    В Hibernate 5.2 класс MySQL57InnoDBDialect - Deprecated

ТЕПЕРЬ!
    Чтобы выбрать dialect НУЖНО использовать
        1. org.hibernate.dialect.MySQL57Dialect
        2. hibernate.dialect.storage_engine=innodb
источник: https://stackoverflow.com/a/46746865

4. **Если нужно изменить параметры табл, такие как: encoding set, collation и др.**, то - нужно переопределить MySQL57InnoDBDialect и определить метод который создает конфигурационную строку при создании табл.

# Про Hibernate Criteria API vs JPA Criteria API

Есть 2ва варианта Criteria API в Hibernate, **старый** (Hibernate Criteria API) и **новый** (JPA Criteria API). Новый перинят из JPA и считается основным, старый доступен в Hibernate в виде расширения.

Начиная с Hibernate 5.2 спецификация Hibernate Criteria API уже **deprecated**, рекомендуется использовать реализацию из JPA: JPA Criteria API. Версия API от Hibernate будет портирована как extensions to the JPA и доступна в пакете `Criteria.` ( https://stackoverflow.com/a/31202152 )

**Ссылки:**

* [Официальная документация](https://docs.jboss.org/hibernate/entitymanager/3.5/reference/en/html/querycriteria.html)
* [How to query by entity type using JPA Criteria API](https://vladmihalcea.com/query-entity-type-jpa-criteria-api/)
* [Отличие jpa от hibernate для criteria](https://stackoverflow.com/questions/25536868/criteriaistinct-root-entity-vs-projectionsistinct)

# JPA Criteria API

## Базовые

* **Root** - корень выбранной табл. или группы табл., если запрос полиморфный
* **CriteriaBuilder** - используется чтобы добавлять условия к исходному Predicate (объединять несколько Predicate через `and()` например)
* **CriteriaQuery** - делает запросы с применением операторов distinct, subquery, having, groupBy etc к указанному Root
* **CriteriaUpdate**
* **CriteriaDelete**
* **Predicate** - условие использующиееся в запросе на основе которого строится результат
* **Criteria**
* **Query**
* **TypedQuery**
* **Subquery** - объект для формирования подзапроса, ему передается class другого типа, того чьей сущности принадлежит подзапрос
* **Example**
* **ParameterExpression** - обертка для разных типов данных, которую потом можно вложить в условие запроса: `q.select(c).where(cb.gt(c.get("population"), cb.parameter(Integer.class)));` Можно устанавливать параметр и без типа, но тогда проверка типа будет только при запросе

комбинация нескольких Predicate: https://stackoverflow.com/questions/43322504/criteria-query-combine-and-predicates-and-or-predicates-in-where-method

join + predicate (join используя predicate): https://stackoverflow.com/a/42222429

## Как работать с этим в целом (примеры)

1. Получаем **CriteriaBuilder** объект из **EntityManager** или **Session** (**не** создаем через `build()`, а получаем через `getCriteriaBuilder()`)
2. Создаем (как `build()`) объект **CriteriaQuery** из **CriteriaBuilder** через метод `criteriaBuilder.createQuery(User.class);` с указанием в нем класса **Entity** табл. к которой будем делать запросы (или общего класса предка для нескольких **Entity**, тогда запросы будут полиморфными - сразу к нескольким **Entity** наследникам)

**Обычный**

```java
// получаем builder
CriteriaBuilder cb = entityManager.getCriteriaBuilder();

// создаем query, при этом можно указывать класс предка нескольких Entity,
// тогда работать будет с его наследниками (полиморфные запросы)
CriteriaQuery<User> criteriaQuery = cb.createQuery(User.class);

// получаем Root
Root<Topic> root = criteriaQuery.from(Topic.class);

// условия запроса
criteriaQuery.where(
    builder.equal(root.get("owner"), "Vlad")
);

// результат
List<Topic> topics = entityManager
    .createQuery(criteria)
    .getResultList();
```

**Пример Subclass filtering**

```java
// sub class от основного класса (Subclass filtering)

// храним в отдельной переменной
Class<? extends Topic> sublcass = Post.class;

//...

// условие запроса
criteria.where(
    builder.and(
        builder.equal(root.get("owner"), "Vlad"),
        builder.equal(root.type(), sublcass) // проверяем чтобы тип был как у subclass
    )
);
```

**Пример ParameterExpression**

```java
CriteriaBuilder cb = em.getCriteriaBuilder();
CriteriaQuery<Country> q = cb.createQuery(Country.class);
Root<Country> c = q.from(Country.class);
ParameterExpression<Integer> p = cb.parameter(Integer.class); // получаем parameter
q.select(c).where(cb.gt(c.get("population"), p)); // устаналиваем parameter в условие
```

**Пример subquery**

```java
String projectName = "project1";
List<Employee> result = employeeRepository.findAll(
    new Specification<Employee>() {
        @Override
        public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
            Subquery<Employee> sq = query.subquery(Employee.class);
            Root<Project> project = sq.from(Project.class);
            Join<Project, Employee> sqEmp = project.join("employees");
            sq.select(sqEmp).where(cb.equal(project.get("name"),
                    cb.parameter(String.class, projectName)));
            return cb.in(root).value(sq);
        }
    }
);
```

аналогично sql запросу

```sql
SELECT e FROM Employee e WHERE e IN (
    SELECT emp FROM Project p JOIN p.employees emp WHERE p.name = :projectName
)
```

**Пример Example**

```java
// тут будет пример
```

**Пример Join**

```java
CriteriaBuilder cb = em.getCriteriaBuilder();
CriteriaQuery<BookWithAuthorNames> cq = cb
        .createQuery(BookWithAuthorNames.class);
// Define FROM clause
Root<Book> root = cq.from(Book.class);
Join<Book, Author> author = root.join(Book_.author);

// Define DTO projection
cq.select(cb.construct(
        BookWithAuthorNames.class,
        root.get(Book_.id),
        root.get(Book_.title),
        root.get(Book_.price),
        cb.concat(author.get(Author_.firstName), ' ',
                author.get(Author_.lastName))));

// Define WHERE clause
ParameterExpression<String> paramTitle = cb.parameter(String.class);
cq.where(cb.like(root.get(Book_.title), paramTitle));

// Execute query
TypedQuery<BookWithAuthorNames> q = em.createQuery(cq);
q.setParameter(paramTitle, "%Hibernate Tips%");
List<BookWithAuthorNames> books = q.getResultList();

for (BookWithAuthorNames b : books) {
    log.info(b);
}
```

**в criteria api можно сравнивать не только значения полей**
```java
User user = repository.findByIf(1L); // получаем ссылку на Entity в кжше L1
builder.equals(root.get("user"), user);
```

**если делать join в criteria api, то id результирующей табл будет принадлежать той табл. с которой сделан join (не базовой)**
```java
root.id == user.id; // true
Join<User, Company> join;
join.id == company.id; // true

root.get("compan").id; // альтернативно можно после join так
```

**Использование builder.literal**

```java
// некоторые ф-ции требуют чтобы 1ый параметр был Expression, чтобы можно было использовать String или Integer
// нужно преобразовать из через builder.literal()
builder.like(
  builder.literal("my phone number"), // просто строку использовать нельзя
  builder.concat(join.get("prefix"), "%")
) // аналог запроса: ... where '14' like concat(prefix , '%')
```

**Limit в Criteria API сделать нельзя**
```java
// ограничить количество результатов в Specification можно только параметром pageable
// sort при этом можно менять и в query
findAll(mySpecification, new PageRequest(0, 1, pageable.getSort()));
```

Источник: [тут](https://discourse.hibernate.org/t/how-can-io-a-join-fetch-in-criteria-api/846/6)  
**join fetch** - в отличии от **join** загружает поля обеих таблиц одним запросом.  
**Note.** Удалось найти только способ с приведением, просто последовательный вызов `.join` а потом `.fetch` приводит к дублированию join.
```java
// работает только с приведением типа к Join
Fetch<Ereturn, ProductItem> productItemFetch = root.fetch("productItems", JoinType.LEFT);
Join<Ereturn, ProductItem> productItemJoin = (Join<Ereturn, ProductItem>) productItemFetch;
```

**join + sort** - в **query** настраиваем **sort**, а в **builder** условие **where**
```java
String phone = "123";
final Specification<Country> spec = (root, query, builder) -> {
  Join<Country, Phone> join = root.join("phones"); // имя поля коллекции

  query.orderBy(  // sort использовать в criteria api можно
    builder.desc(
      builder.length(join.get("country"))
    ),
    builder.desc(join.get("priority")) // значение из join
  );

  return builder
    .and(
      builder.lessThanOrEqualTo(
        builder.diff(
          builder.literal(phone.length()),
          builder.length(root.get("phoneCode")) // значение из root
        ),
        root.get("length")
      ),
      builder.like(
        builder.literal(phone),
        builder.concat(join.get("country"), "%")
      )
    );
};
```

**conjunction & disjunction**
```java
Predicate pr1 = cb.like(article.get(Article_.code), "%" + searchQuery + "%");
Predicate pr2 = cb.like(article.get(Article_.oem_code), "%" + searchQuery + "%");
Predicate pr3 = cb.conjunction();

for (String str : busquedaSplit) {
    Predicate newPredicate = cb.like(article.get(Article_.description), "%" + str + "%");
    pr3 = cb.and(pr3, newPredicate);
}

disjunction = cb.or(pr1, pr2, pr3);

predicates.add(disjunction);
```

**Пустые Collections в in() подставлять нельзя**
```java
Specifications.where((root, query, builder) -> {
    final Join<Account, Client> join = root.join(Account.Fields.clientId, JoinType.LEFT);
    if(clientIds.isEmpty()) // правильно, вернем пустую specification если коллекция пуста
      return (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.conjunction().not();
    return join.get(Client.Fields.id).in(clientIds); // ошибка, если clientIds.isEmpty() == true
});
```

**multiselect** - чтобы вытащить только часть полей из Entity (не доступно через Specification)
```java
// тут будет пример
```

**EntityGraph можно использовать со Specification** Источник: [тут](https://stackoverflow.com/a/30087275) При этом подключать **EntityGraph** можно в **обычном** Criteria API (не Specification) через Session (в отличии от Spring Data Jpa Specification там вытащить часть полей можно в т.ч. через **Tuple**)
```java
// способ 1
@NoRepositoryBean // 1. интерфейс
public interface CustomRepository<T, ID extends Serializable> extends JpaRepository<T, ID>, JpaSpecificationExecutor<T> {
    // такие же методы можно делать для List<T> findAll, T findOne
    Page<T> findAll(Specification<T> spec, Pageable pageable, EntityGraphType entityGraphType, String entityGraphName);
}
@NoRepositoryBean // 2. реализация
public class CustomRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID> implements CustomRepository<T, ID> {
    private EntityManager em;
    public CustomRepositoryImpl(Class<T> domainClass, EntityManager em) {
        super(domainClass, em);
        this.em = em;
    }
    @Override
    public Page<T> findAll(Specification<T> spec, Pageable pageable, EntityGraph.EntityGraphType entityGraphType, String entityGraphName) {
        TypedQuery<T> query = getQuery(spec, pageable.getSort());
        query.setHint(entityGraphType.getKey(), em.getEntityGraph(entityGraphName));
        return readPage(query, pageable, spec);
    }
}

// 3. factory для реализации
public class CustomRepositoryFactoryBean<R extends JpaRepository<T, I>, T, I extends Serializable> extends JpaRepositoryFactoryBean<R, T, I> {
    protected RepositoryFactorySupport createRepositoryFactory(EntityManager entityManager) {
        return new CustomRepositoryFactory(entityManager);
    }
    private static class CustomRepositoryFactory<T, I extends Serializable> extends JpaRepositoryFactory {
        private EntityManager entityManager;
        public CustomRepositoryFactory(EntityManager entityManager) {
            super(entityManager);
            this.entityManager = entityManager;
        }
        protected Object getTargetRepository(RepositoryMetadata metadata) {
            return new CustomRepositoryImpl<T, I>((Class<T>) metadata.getDomainType(), entityManager);
        }
        protected Class<?> getRepositoryBaseClass(RepositoryMetadata metadata) {
            // The RepositoryMetadata can be safely ignored, it is used by the JpaRepositoryFactory
            //to check for QueryDslJpaRepository's which is out of scope.
            return CustomRepository.class;
        }
    }
}

// 4. подключение
@EnableJpaRepositories(
    basePackages = {"your.package"},
    repositoryFactoryBeanClass = CustomRepositoryFactoryBean.class
)

// способ 2
public interface BookRepository extends JpaSpecificationExecutor<Book> {
   @Override
   @EntityGraph(attributePaths = {"book.author"})
   List<Book> findAll(Specification<Book> spec);
}

// способ 3.
// есть неофициальная библиотека с EntityGraphJpaSpecificationExecutor и EntityGraphJpaRepository
// https://github.com/Cosium/springata-jpa-entity-graph
```
# Примеры запросов на Criteria API
1. **findAll()**
    ```java
    // select * from T where 1=1;
    (root, query, builder) -> {
        return builder.conjunction();
    }
    ```
2. **find empty**
    ```java
    // select * from T where 1=0;
    (root, query, builder) -> {
        return builder.conjunction().not();
    }
    ```
3. **findByValueIn(column, values)**
    ```java
    // select * from T where T.column in (values);
    (root, query, builder) -> {
        // String column, Collection<?> values
        return values.isEmpty()
            ? builder.conjunction().not() // если values is empty будет ошибка, поэтому возращаем пустоту
            // ? builder.conjunction() // как вариант можно вернуть все, если это поиск
            : root.get(column).in(values);
    }
    ```
4. **findByValueNotIn(column, values)**
    ```java
    // select * from T where T.column not in (values);
    (root, query, builder) -> {
        // String column, Collection<?> values
        return values.isEmpty()
            ? builder.conjunction().not() // если values is empty будет ошибка, поэтому возвращаем пустоту
            // ? builder.conjunction() // как вариант можно вернуть все, если это поиск
            : root.get(column).in(values).not();
    }
    ```
5. **findByColumnEquals(column, value)**
    ```java
    // select * from where T.column = value;
    (root, query, builder) -> {
        return builder.equal(root.get(column), value);
    }
    ```
6. **findByColumnNotEquals(column, value)**
    ```java
    // select * from where T.column <> value;
    (root, query, builder) -> {
        return builder.equal(root.get(column), value).not();
    }
    ```
7. **findByColumnIsNull(column)**
    ```java
    // select * from T where T.column is null;
    (root, query, builder) -> {
        return criteriaBuilder.isNull(root.get(column));
    }
    ```
8. **findByColumnIsNotNull(column)**
    ```java
    // select * from T where T.column is not null;
    (root, query, builder) -> {
        // return criteriaBuilder.isNull(root.get(column)).not(); // или так
        return criteriaBuilder.isNotNull(root.get(column));
    }
    ```
9. **distinct()**
    ```java
    // select distinct ...
    public static Specification<Object> distinct() {
        return (root, query, cb) -> {
            query.distinct(true);
            return null;
        };
    }
    Specification.where(distinct()).and(specification));
    ```
10. **orderBy(column, column, ...)** Можно добавить только **1ин** orderBy (но по нескольким полям)
    ```java
    // select distinct ...
    public static Specification<Object> orderBy(field1, field2) {
        return (root, query, cb) -> {
            query.orderBy(cb.asc(root.get(field1)), cb.desc(root.get(field2)));
            return null;
        };
    }
    Specification.where(orderBy()).and(specification));
    ```
11. **select * from a join (select max(date) as date from b group by (name))**  
    Можно сделать **join с subselect** использовав **view** в качестве **subselect** (сам Hibernate тут не формирует subselect, его делает DB)
    ```java
    /*  -- создаем view
        select name, max(date) as date
        from role
        group by name
    */

    @Entity
    class User {
        // Note! Возможно обязательно нужно ставить insertable = false, updatable = false иначе Exception
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumns({
                @JoinColumn(name = "roleName", referencedColumnName = "name", insertable = false, updatable = false),
                @JoinColumn(name = "date", referencedColumnName = "date", insertable = false, updatable = false)})
        UserView userView;

        String roleName;
        String date;
    }

    @Entity // View в базе данных, тут нет обычного Id, а эти нужны чтобы был какой-то Id
    @Immutable
    class RoleView implements Serializable {
        @Id String name;
        @Id LocalDate date;
    }

    // select * from user u
    // join (select * from role_view r where and u.roleName=r.name and u.date=max(r.date)) where 1=1;
    public static joinWithSubselect() {
        return Specification.where(specification).and((root, query, builder) -> {
            root.join("userView");
            return builder.conjunction();
        });
    }
    ```

# Specification (и ее связь с Criteria API), использование с find методом из Spring Data JPA

**Specification** - это interface из **Spring Data JPA** который получает как параметры **Root**, **CriteriaQuery**, **CriteriaBuilder** внутри метода реальзуется алгоритм Criteria API и метод возвращает predicate, который используется какой-либо функцией в запросе. Основное метод это **toPredicate()** и возвращает **Predicate**

Например в Spring можно передавать объект **Specification** в метод `find(Specification s)`. Т.е. сами методы `find(Predicate p)` принимают predicate, который возвращается методом `toPredicate()`

```java
public interface Specification<T> { // класс из Spring Data JPA
  Predicate toPredicate(Root<T> root, CriteriaQuery<?> query,
            CriteriaBuilder builder);
}
```

В **Spring Data JPA** нужно наследовать **Repository** интерфейс **JpaSpecificationExecutor** чтобы использовать **Specification** в методах `find()`

```java
public interface PersonRepository extends CrudRepository<Person, Long>, JpaSpecificationExecutor<Person> { }
```

На практике в коде приложения можно выделить отдельный package для классов реализующих **Specification**, например использовать эти классы для реализации разных алгоритмов поиска для REST API разных сущностей.

**Полный пример Specification**

```java
public class AccessTemplateSearchSpecification implements Specification<AccessTemplate> {
    private final AccessTemplateSearchCriteria accessTemplateSearchCriteria;

    @Override
    public Predicate toPredicate(Root<AccessTemplate> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        Predicate predicate = criteriaBuilder.and();

        if (accessTemplateSearchCriteria == null) {
            return predicate;
        }
        if (StringUtils.hasText(accessTemplateSearchCriteria.getComment())) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.like(root.get(AccessTemplate_.comment),
                            LikeQueryWrapperUtil.wrapForDb(accessTemplateSearchCriteria.getComment())));
        }
        if (accessTemplateSearchCriteria.getStartDate() != null) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThanOrEqualTo(root.get(AccessTemplate_.startDate),
                            accessTemplateSearchCriteria.getStartDate()));
        }
        if (accessTemplateSearchCriteria.getEndDate() != null) {
            predicate = criteriaBuilder.and(predicate, criteriaBuilder.lessThanOrEqualTo(root.get(AccessTemplate_.endDate),
                            accessTemplateSearchCriteria.getEndDate()));
        }

        return predicate;
    }
}
```
**Specifications** - класс работает как Utils (служебный) класс для `Specification`, реализует utils методы и может работать как builder разных выражений
```java
Specification<MyEntity> spec = Specifications // работает как builder
    .where(specification)
    .and(Specifications.where(Specifications.<MyEntity>where(
            justAfuncThatWillReturnASpec(
                    RequestParams.PARAM1, PARAM1, 'field1_name',
                    RequestParams.PARAM2, PARAM2, 'field2_name'
            )
        )
    );

// создаем пустой Specification
Specification<User> spec = Specifications<User>.where(null);
```
**SpecificationUtils** - общий подход к работа с `Specification`, делаем набор стандартных `Predicate`, чтобы использовать в запросах.
```java
// 1. Создаем
class SpecificationUtils {
    public static <T> Specification<T> findById(Object id) {
        return (root, query, builder) -> builder.equal(root.get("id"), id);
    }
    
    public <T> Specification<T> findByValueIn(String column, Collection<?> values) {
        return values.isEmpty()
            ? (root, query, builder) -> builder.conjunction().not() // find nothing
            : (root, query, builder) -> root.get(column).in(values); // select * from T where T.column in (values);
    }
}

// 2. Используем
// select * from T where T.id = 1 and T.role in ('user', 'admin');
repository.findAll(Specifications.where(findById(1L)).and(findByValueIn("role", Arrays.asList("user", "admin"))));
```

**See Specification to String for debug**
```java
CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
CriteriaQuery<Object> query = criteriaBuilder.createQuery();
try {
    Root<T> root = (Root<T>) query.from(MyEntity.class).alias("entity");
    query.select(root);
    query.where(specification.toPredicate(root, query, criteriaBuilder));
    TypedQuery<Object> typedQuery = entityManager.createQuery(query);
    String query = typedQuery.unwrap(Query.class).getQueryString(); // use org.hibernate.query.Query
} catch (Throwable ex){
    throw new IllegalArgumentException(ex);
}
```

**Convert Specification to Predicate**
```java
// A Specification
Pageable pageable; // a Pageable
Specification<User> specification; // A Specification

CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder(); // criteriaBuilder.createTupleQuery();
CriteriaQuery<User> query = criteriaBuilder.createQuery(User.class);
Root<User> root = query.from(User.class);

// 1. Specification to Predicate
Predicate predicate = specification.toPredicate(root, query, criteriaBuilder);

// 2. Sort to Order: javax.persistence.criteria.Order, org.springframework.data.jpa.repository.query.QueryUtils
List<Order> orders = QueryUtils.toOrders(pageable.getSort(), root, criteriaBuilder);

query.multiselect(root.get('id'), root.get('name'))
    .where(predicate)
    .orderBy(orders);
List<User> results = entityManager
    .createQuery(query)
    .setFirstResult(pageable.getPageNumber() * pageable.getPageSize())
    .setMaxResults(pageable.getPageSize())
    .getResultList();
Page page = new PageImpl<>(results, pageable, results.size()); // 3. Page
```

# Еще раз Про транзакции

## Locks in JPA

Источник: [тут](https://vladmihalcea.com/a-beginners-guide-toatabase-locking-and-the-lost-update-phenomena/)

**Note.** Не совсем по теме, но существует библиотека **com.vladmihalcea:db-util:0.0.1** у которой есть разные механизмы анализа SQL запросов и аннотация `@Retry` которая позволяет выполнить запросы повтора автоматически в случае `OptimisticLockException`

Несовсем ясная информация из статьи: Большинство реализаций **optimistic locking** проверяет только измененные данные. Но JPA имеет также **explicit optimistic locking**.

Для обработки **OptimisticLockException** в JPA рекомендуется получить Entity через reloading или refreshing, предпочтительно в новой transaction. И попытаться выполнить операцию снова.

В JPA для Entity с version атрибутом **optimistic locking** включена **by default**, но можно и явно указать блокировку в методах JPA для работы с Entity (пример ниже). В JPA при обнаружении конфликта выбрасывается **OptimisticLockException** и transaction помечается для rollback.

**Note.** **Optimistic lock** не предотвращает **затирание** самих полей, т.к. она не сранивает их значение (e.g. текстовое поле `String name`). Она служит только для предотвращения затирания данных параллельными транзакциями, но если операции последовательные, то затирание произойдет.

**JPA позволяет устанавливать** режимы блокировки для операций (т.е. устанавливать их передачей дополнительного параметра этим методам):

* **finding** an entity
* **locking** an existing persistence context entity
* **refreshing** an entity
* **querying** - в методах JPQL, Criteria или native queries

**Hibernate в дополнение позволяет устанавливать** режимы lock в методах:

* **getting** an entity
* **loading** an entity

```java
// Для @Version в Entity блокировка включена по умолчанию, но можно и указать явно в методах
// Пример установки блокировки в методе lock
session.lock(role.getUser(), LockModeType.OPTIMISTIC_FORCE_INCREMENT);
entityManager.find(Student.class, studentId, LockModeType.OPTIMISTIC);
entityManager.refresh(student, LockModeType.READ);

@NamedQuery(name="optimisticLock",
  query="SELECT s FROM Student s WHERE s.id LIKE :id",
  lockMode = WRITE)

@Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
@Query("SELECT c FROM Customer c WHERE c.orgId = ?1")
public List<Customer> fetchCustomersByOrgId(Long orgId);

// lock timeout
@Lock(LockModeType.PESSIMISTIC_READ)
@QueryHints({@QueryHint(name = "javax.persistence.lock.timeout", value = "3000")})
public Optional<Customer> findById(Long customerId);
```

**Типы Explicit lock в JPA**

* **NONE** - если не указано **Explicit lock** (не используется), то используется implicit locking (optimistic or pessimistic)
* **OPTIMISTIC** (READ) - применяет optimistic read lock ко всем Entity содержащим `@Version` атрибут. Когда используется OPTIMISTIC lock, то **persistence provider** защищает даные от **dirty reads** и **non-repeatable reads** (другими словами в этом случае persistence provider проверяет чтобы любая transaction упала (fails) при commit любых данных, которые **другая** параллельная транзакция: **1)** updated или deleted, но НЕ commited или **2)** updated или deleted и успешно commited)
* **OPTIMISTIC_FORCE_INCREMENT** (WRITE) - как OPTIMISTIC, но номер version увеличивается даже если никаких изменений внесено не было. В спецификации не указано должно это быть сделано сразу после commit или после flush.
* **PESSIMISTIC_READ** - делает shared lock, предотвращает updated и deleted. Используется когда в процессе работы с данными не может встретится **dirty reads**, которая может испортить данные. В некоторых DB не поддерживается, там есть только **PESSIMISTIC_WRITE**
* **PESSIMISTIC_WRITE** - делает exclusive lock, предотвращает read, updated и deleted. **Прим.** Некоторые DB реализуют **multi-version concurrency control**, который позволяет делать read данных на которых сделан lock. **Прим 2.** т.е. запрещает другим transaction делать PESSIMISTIC_READ и PESSIMISTIC_WRITE на тех же заблокированных элементах.
* **PESSIMISTIC_FORCE_INCREMENT** - как PESSIMISTIC_WRITE, но при этом увеличивает version атрибут для Entity с version атрибутом. Если работа идет с Entity у которой есть version атрибут, то использовать нужно этот lock. Некоторые persistence provider не поддерживают работы PESSIMISTIC_FORCE_INCREMENT с Entity без версий, тогда будет **PersistanceException**.

Существуют **Hibernate LockMode** и **JPA LockModeType** - в Hibernate режимов больше.

* **UPGRADE_NOWAIT** и **UPGRADE_SKIPLOCKED** - некий аналог **PESSIMISTIC_WRITE**. Эти режимы используют Oracle-style запросы `select for update nowait` и `select for update skip locked`
* Остальные режимы в Hibernate аналогичны режимам JPA

**Exceptions для pessimistic lock**:

* **PessimisticLockException**
* **LockTimeoutException**
* **PersistanceException**

## Transactoin Isolation Level in JPA

Источник: [тут](https://vladmihalcea.com/a-beginners-guide-toatabase-locking-and-the-lost-update-phenomena/), [про Repetable Reads в JPA](https://vladmihalcea.com/hibernate-application-level-repeatable-reads/)

**Lost update**. В JPA чтобы предотвратить **lost update** рекуомендуется использовать **Optimistic Locking** вместе с **read commited** (обычно быстрее работает). Тогда при ошибке будет **StaleObjectStateException** (Hibernate) или **OptimisticLockException** (JPA).  
Также можно использовать **Repeatable Read** или **pessimistic lock** (но это не так производительно). При **Repeatable Read** транзакция будет прервана в случае нарушения **atomicity**.  
Hibernate включает все строки изменения данных **в один UPDATE** (т.е. как я понимаю строит их последовтельно, что и может вызвать затирание и как результат lost update), эту стратегию можно изменить черех `@DynamicUpdate`, но это решение не всегда срабатывает эффективно. Очистка ненужных операций (т.е. оптимизация, которая называется persistence context write-behind policy в JPA) используется, чтобы уменьшить время на которое делается lock, но она увеличивает время между read и write (т.к. хранит данные в себе все время до того как сделан flush) и чем больше это время тем больше вероятность lost update.

**Repeatable Reads.** Кэш L1 в JPA гарантирует **session-level repeatable reads** (i.e. кэш L1 не дает перетереть уже загруженную Entity).  
Если conversation распостраненая на несколько requests (**long conversations**) можно получить **application-level repeatable reads** используя **optimistic lock** (т.е. требуется **application-level concurrency control strategy**). Long conversations является **stateful** поэтому мы можем иметь **detached objects** или **long persistence contexts**.
**Тут же советуется:** Использовать отдельные запросы SQL (e.g. Spring Data JPA), а не начитку Entity через Hibernate ORM в **той же** transaction которая делает **update**. Т.к. начитка Entity имеет смысл если они будут изменены и записаны обратно в DB. (видимо имеется ввиду делать **read-write** операции в транзакции вместо **read-write-read** i.e. не делать изменение и начитку данных по какой-то логике в одной и той же транзакции).

## Optimistic locking, Version-less optimistic locking и `@Source`
Во время оптимистичной блокировки hibernate при изменении любого поля Entity увеличивает номер версии. Чтобы исключить поле чтобы оно не вызывало изменение версии нужно использовать.
```java
@Entity
class MyEntity {
    @OptimisticLock(exclude = true) String field;
    @Version long version;
}
```

Кроме числового поле `@Version` может быть **Date** или **Calendar**.

При **optimistic locking** в табл есть поле version с номером версии. Но при **version less optimistick locking** его нет (уточнить инфу!!!)

**OptimisticLockType**
* `NONE` - optimistic lock отключена даже если есть `@Version`
* `VERSION` (default)
* `ALL` - все столбцы будут использованы для проверки version, т.е. будут использованы в качестве version поля
* `DIRTY` - только измененные будут использованы для проверки version, т.е. будут использованы в качестве version поля

`ALL` or `DIRTY` - включают **version-less optimistic locking**. version-less optimistic locking нужен для поддержки optimistic lock старого варианта optimistic lock в DB (в статье не ясно написано для поддержки optimistic lock на уровне старых DB т.к. у новых есть поддержка каких-то механизмов optimistic lock или это просто старый вариант optimistic lock сам по себе). В этих случаях нужно добавлять аннотацию `@DynamicUpdate` т.к. Hibernate включает все столбцы в SQL запрос при каждом update даже если они не изменились, потому что использует переиспользуется кэшированный SQL prepared statement. Если `DIRTY` нужно добавить еще и `@SelectBeforeUpdate`.
```java
@Entity
@OptimisticLocking(type = OptimisticLockType.DIRTY)
@DynamicUpdate // для ALL и DIRTY
@SelectBeforeUpdate // для DIRTY
class MyEntity {
    @OptimisticLock(exclude = true) String field;
    @Version long version;
}
```

**Version-less optimistic locking** - 

## Атрибуты `@Version` для **optimistic lock**

Это атрибуты, которыми можно менять поведение `@Version` проставленной на поле версии табл.

**Атрибуты @Version (для типа int):**

| Имя           | Описание                                                                                                                                                                                                                             |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| column        | Имя колонки, в которой находится номер версии. Опционально, по-умолчанию такое же как у имени свойства.                                                                                                                              |
| name          | Имя свойства персистентного класса.                                                                                                                                                                                                  |
| type          | Тип номера версии. Опционально, по-умолчанию integer.                                                                                                                                                                                |
| access        | Стратегия Hibernate для доступа к значению свойства. Опционально, по-умолчанию property                                                                                                                                              |
| unsaved-value | Показывает, что экземпляр только что создан и тем самым не сохранен. Выделяет из отсоединенных сущностей (detached). Значение по-умолчанию, undefined, показывает, что свойство-идентификатор не должно использоваться. Опционально. |
| generated     | Показывает, что свойство версии должно генерироваться базой данных. Опционально, по-умолчанию never.                                                                                                                                 |
| insert        | Включать или нет колонку версии в выражение SQL-insert. По-умолчанию true, but вы можете поставить это в false если колонка в БД определена со значением по-умолчанию 0                                                              |

**Атрибуты @Version (для типа Date):**

| Имя           | Описание                                                                                                                                                                                                                                                                                                                                                                                                   |
| ------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| column        | Имя колонки, в которой находится временная метка. Опционально, по-умолчанию такое же, как и имя свойства.                                                                                                                                                                                                                                                                                                  |
| name          | Имя JavaBeans-свойства типа Date или Timestamp у персистентного свойства.                                                                                                                                                                                                                                                                                                                                  |
| access        | Стратегия, которую Hibernate использует для доступа к значению свойства. Опционально, по-умолчанию property.                                                                                                                                                                                                                                                                                               |
| unsaved-value | Показывает, что экземпляр только что создан и тем самым не сохранен. Выделяет из отсоединенных сущностей (detached). Значение по-умолчанию, undefined,показывает что свойство-идентификатор не должно использоваться. Опционально.                                                                                                                                                                         |
| source        | Извлекает ли Hibernate метку из БД или из текущей JVM. БД-метки вносят дополнительный оверхэд, т.к Hibernate нужно запрашивать БД каждый раз для определения инкремента. Однако, БД-метки более безопасны при использовании в кластеризованном окружении. Не все диалекты БД поддерживают извлечение текущих временных меток из БД. Другие могут быть небезопасны для блокировок, из-за нехватки точности. |
| generated     | Генерируется ли метки средствами БД. Опционально, по-умолчанию never.                                                                                                                                                                                                                                                                                                                                      |

## О том что изменение Optimistic lock version в child entity не увеличивает version в root entity и как решить эту проблему
https://vladmihalcea.com/how-to-increment-the-parent-entity-version-whenever-a-child-entity-gets-modified-with-jpa-and-hibernate/

## Detached entities anomaly при Version-less optimistic locking

**detached entities anomaly** при **Version-less optimistic locking** возникает когда закрывается Persistence Context (Session). Изменить состояние **Entity** можно только **merge** и **update**, обе эти операции делают **select** чтобы получить последний snapshot из DB (видимо имеется ввиду версия в виде полей чтобы сравнить при version less optimistic lock нужно ли что-то делать) и сравнить ее с текущей Entity (видимо речь о образце entity в памяти). Но это (использование этого типа блокировки после update или merge) может привести к **lost update**. Это происходит потому что как только Session закрыта мы не можем использовать оригинальное состояние Entity в `UPDATE ... WHERE` запросе и поэтому новые данные могут быть перезаписаны старыми (т.е. случится **lost update**).

## работа с OptimisticLockException и о том что detached entities могут
https://vladmihalcea.com/how-to-prevent-optimisticlockexception-using-hibernate-versionless-optimistic-locking/

https://vladmihalcea.com/an-entity-modeling-strategy-for-scaling-optimistic-locking/

## Lock Scope

https://www.baeldung.com/jpa-pessimistic-locking

**Lock scope** - это параметр, который передается в методы JPA для работы и определяет поведение lock со связями (relationship) с Entity. Т.к. lock влияет на  joined inheritance и secondary tables. Не все persistence providers поддерживают lock scopes.

```java
Map<String, Object> properties = new HashMap<>();
map.put("javax.persistence.lock.scope", PessimisticLockScope.EXTENDED);
entityManager.find(Student.class, 1L, LockModeType.PESSIMISTIC_WRITE, properties);
```

**Типы Lock scope:**

* **PessimisticLockScope.NORMAL** (default) - блокирует только Entity с которой идет работа, при использовании `@Inheritance(strategy = InheritanceType.JOINED)` блокирует и придков класса Entity. Т.е. lock делается на many-to-one and one-to-one foreign keys, но lock на other side parent associations не делается.
* **PessimisticLockScope.EXTENDED** - как Normal, но в дополнение блокирует связные Entity в **"join table"** (@ElementCollection or @OneToOne, @OneToMany etc. with @JoinTable). Другими словами lock распостраняется и на **junction tables** (связь many-to-many). Этот тип lock (**прим.** видимо! имеется ввиду explicit lock с использованием данного scope) полезен только для защиты против удаления дочерних (children), но разрешает phantom reads и изменение children entity states.

```sql
-- PessimisticLockScope.NORMAL
-- Если есть два класса PERSON и EMPLOYEE, то lock будет выглядеть так, как я понял!
SELECT t0.ID, t0.DTYPE, t0.LASTNAME, t0.NAME, t1.ID, t1.SALARY 
FROM PERSON t0, EMPLOYEE t1 
WHERE ((t0.ID = ?) AND ((t1.ID = t0.ID) AND (t0.DTYPE = ?))) FOR UPDATE

-- PessimisticLockScope.EXTENDED запрос будет видимо выглядеть так
SELECT CUSTOMERID, LASTNAME, NAME
FROM CUSTOMER WHERE (CUSTOMERID = ?) FOR UPDATE

SELECT CITY, COUNTRY, Customer_CUSTOMERID 
FROM customer_address 
WHERE (Customer_CUSTOMERID = ?) FOR UPDATE
```

## Lock Timeout

Можно устаналивать Lock Timeout. По истечении выбросит **LockTimeoutException**. Если поставить значение ноль, то это равносильно **no wait**, но не все DB поддерживают такое.

```java
Map<String, Object> properties = new HashMap<>(); 
map.put("javax.persistence.lock.timeout", 1000L); 

entityManager.find(Student.class, 1L, LockModeType.PESSIMISTIC_READ, properties);
```

## Phisical vs logical transaction (Unit of Work)

**phisical transaction** - выполняется для каждого отдельного запроса, если сделана на уровне DB (т.е. сделана SQL запросом), даже если границы transaction не объявлены явно (BEGIN/COMMIT/ROLLBACK). Это поведение согласно **ACID**.

**logical transaction** - это **application-level unit of work**, которая распостраняется на множество **physical** (database) **transactions**.

Удержание соединения с DB в течении нескольких user requests, пока user думает это anti-pattern. **database server** может поддерживать ограниченное число соединений, обычно каждое из этих соединений переиспользуется (используется одно соединение на несколько users) через **connection pooling**. Удержание limited ресурсов ограничивает scalability. Поэтому database transactions должны быть как можно **короткими**, чтобы освободить соединение как можно **быстрее**.

Web application влечет использовыание **read-modify-write conversational** паттерна. Web conversation (обмен данными) состоит из множества user requests, все эти операции logicaly связаны с одной и той же **application-level transaction** (т.е. с logical transaction). Такая transaction тоже должна соблюдает ACID, т.к. другие users могут изменить Entities еще до того как shared lock будет released (отпущен). **database transaction ACID** могут предотвратить **lost updates** только в границах **одной** physical transaction. Чтобы предотвратить **lost updates** в границах logical transaction нужно чтобы она тоже соблюдала ACID.

Чтобы предовтратить **lost updates** для application level transaction (logical transaction) нужно поставить ей **repeatable reads** isolation level.

Spring предоставляет (provide) только logical transaction manager, а physical JTA transaction manager такие как Bitronix, Atomikos, RedHat Narayana нужно подключать вручную.

В разных DB операции delete, update, insert могут вести себя немного по разному при использовании locks.

JTA не поддерживает **transaction-scoped isolation levels**, поддерживает только `resource local` isolation levels (т.е. только для простого подключения к DB, не JTA), поэтому Spring предоставляет `IsolationLevelDataSourceRouter`, чтобы передавать transaction isolation levels.
**logical transaction** (e.g. @Transactional) isolation level в Spring проверяется **IsolationLevelDataSourceRouter** и connection отправляется к определенной реализации DataSource, которая обслуживаются определенным JDBC Connection со своим transaction isolation level.

**Spring transaction manager** это просто facade для `resource local` или `JTA transaction managers`. И вместе с этим используется AOP, поэтому преход с **resource local** на **XA** (JTA) transactions.

## Entity state transitions (Entity life cycle)

**Состояния:**

* **New** (Transient) - созданный новый объект, который не был ассоциирован с Hibernate Session (a.k.a Persistence Context) и не был mapped к какой-либо DB row.
* **Persistent** (Managed) - ассоциирован с DB row и управляется Persistence Context'ом. Любое изменение в этом состоянии будет detected и propagated на DB (во время **Session flush-time**, в самый последний момент, т.е. в конце logic transaction). В это состояние Entity переходит при `EntityManager#persist`
* **Detached** - когда Persistence Context is closed все entities становятся detached. Изменения Entities больше не отслеживаются и не будет синхронизированы с DB.
* **Removed** - JPA требует, чтобы Entities могли быть только **removed**, но в Hibernate можно на самом деле удалять Entities (**delete detached entities**), но только через `Session#delete`. В JPA Entity которая **removed** только ставится в очередь (scheduled) на удаление, а само удаление из DB командой `DELETE` будет только во время **Session flush-time**.

Чтобы заново **associate** entity которые **detached** есть несколько способов:

* **Reattaching** (Hibernate but not JPA 2.1) - метод `Session#update`, Hibernate Session может associate только **один** объект Entity к DB row. Потому что Persistence Context ведет себя как in-memory cache (first level cache) и только одно value (entity) ассоциировано с ключем (по которому из кэша достаются Entities). Entity может быть reattached только если нет другого JVM object, который mapped к той же самой DB row уже **associated** Hibernate Session.
* **Merging** - копирует **detached** entity state (source) в **managed** entity instance (destination). Если entity которая мержится не имеет эквивалента в Session, то он будет fetched из DB.

Состояние меняется через интерфейсы **Session** и **EntityManager**, при этом **Session** включает в себя все методы **EntityManager**.

Говорят, что flush-time state трансформируется в DB DML statement (состояние сущности преобразуется в SQL запросы).

## Flush strategies

Этот раздел **ВИДИМО** о функциях Hibernate которые **в Spring и EJB обернуты в AOP и паттерны** и пользователь с ними сталкивается **по минимуму**. Т.е. инфа скорее позновательна.

**Long conversations** - это все время пока user "думает" между операциями с данными, которые обернуты в транзакции. Сами Entity и Session при этом могут вести по разному.

**Single conversation** обычно распостранена на нсколько DB transaction. При этом только последняя **physical transaction** (в рамках обертки **application-level transaction** вокруг нее) должна выполнять операции модификации данных (DML) иначе **application-level transaction** не будет **Unit of Work** (т.е. не будет atomic). Есть несколько Hibernate features которые это реализуют:

* **Automatic Versioning** - automatic optimistic concurrency control, автоматически проверяет если было параллельное изменение данных во время **user think time**, эта проверка происходит вконце conversation.
* **Detached Objects** - это происходит при использовании **session-per-request** pattern, все entities будут detached во время **user think time**. При этом Hibernate позволяет сделать **reattach** этих entities и persist. Этот паттерн называется **session-per-request-withetached-objects**. Automatic versioning при этом используется для изоляции параллельных изменений.
* **Extended (or Long) Session (persistence context)** - при этом подходе **Session** может быть **disconnected** от нижележащего **JDBC connection** (т.е. connection к DB будет closed), после того как DB transaction уже **committed** и будет **reconnected** после того как новый запрос будет отправлен клиентом (**т.е.** будет переиспользована старая **original** Session). Этот паттерн называется **session-per-conversation** и делает **reattachment** ненужным. Automatic versioning используется при этом и flush **не будет** сделан автоматически, только явно.

**session-per-request-withetached-objects** и **session-per-conversation** паттерны - оба имеют преимущества и недостатки.

При использовании **Extended persistence context** чтобы выключить **persistence** во время **application-level transaction** есть несколько вариантов:

1. **disable automatic flushing** установкой **Session FlushMode** в **MANUAL**, при этом в конце обязательно вызвать **Session#flush()**
2. Или отметить все кроме последней transaction как **read-only**, для таких transaction выключится **dirty checking** и **automatic flushing**. При этом **read-only** распостраняется и в **JDBC Connection** и вызывает DB оптимизации. При этом последняя transaction должна быть **writeable**, чтобы сделать **flushed** и **committed**.

При **Extended persistence context** entities остаются attached между разными requests. Из недостатков - расход памяти и чем больше **persistence context** тем медленнее **dirty checking** оптимизация кэша.

Все **extended persistence context** устанавливают **default transaction propagation** в **NOT_SUPPORTED** which makes it uncertain if the queries are enrolled in the context of a local transaction or each query is executed in a separate database transaction.

В различных приложениях используются разные удобные механизмы чтобы работать с **Extended persistence context**, например Java EE может использоваться **@Stateful Session Beans** с **EXTENDED PersistenceContext**

При **Detached objects** подходе используется **intermediate physical transaction**. Чтобы сделать Entities опять **managed** есть несколько вариантов:

1. В Hibernate (не в JPA) есть **Session.update()**, если attached entity уже есть, то будет exception т.к. Session не можут иметь больше одной reference на entity 
2. **Detached objects** может быть **merged** со своим **persistent object equivalent**, если такой entity нет, то Hibernate сделает ее load (fetch) из DB. Сам по себе **Detached object** сделать **managed** нельзя. Этот паттерн может вызвать **lost update** если объект с которым мы делаем **merge** был изменен нами, изменения затрутся.

**Detached entities storage** - так можно назвать место, где хранятся **detached entities** во время **long conversation**. Это **stateful context** в котором можно найти те же самые **detached entities** (т.е. видимо те entities которые мы сделали detached). В Java EE есть встроенный функционал **Stateful Session Beans**, в других приложениях таких как Spring используется объект **HttpSession** для хранения данных (он с его методами get/set **не thread safe**), кроме этого используются third party решения такие как **Spring Web Flow** и **Seam**.

**Важная инфа:** В [источнике](https://vladmihalcea.com/preventing-lost-updates-in-long-conversations/) не совсем ясно написано, но как я понимаю, автор советует использовать **Optimistic locking** ( `@Version` ) **для всех Entities** чтобы убрать. Потому что само по себе **long conversation** перемещает границы транзакции с DB в application и чтобы сделать **application-level repeatable reads** (т.е. установить этот isolation level), но там отсутствует database locking и используется механизм concurrency control самого приложения. Само **Optimistic locking** работает и для DB, и для application (всмысле решает их проблемы? в источнике неясно написано).

**Persistence Context flush.** Hibernate задерживает **Persistence Context flush** до самого последнего момента. Эта стратегия называется **transactional write-behind** и эта одна из стратегий кэширования в программировании вообще, а не только для транзакций. **transactional write-behind** больше относится к самому Hibernate, а не к logical or physical transaction, это потому что **flush** может случится несколько раз за время какой-либо transaction. Операции **flush** видны только **текущей** транзакции, другим transactions они видны только после **commit**

**persistence context** (a.k.a. first level cache) ведет себя как buffer между entity state transitions и DB

В [теории кэширования](https://en.wikipedia.org/wiki/Cache_(computing)#WRITE-BEHIND) **write-behind** использует кэш для хранения синхронизированных данных (entities), и эти данные рано или поздно будут записаны в реальное хранилище (flushed).

**Reducing lock contention.** DML statement выполняются в transaction, в зависимости от transaction isolation level и locks (shared or explicit), делается lock на определенных rows. Уменьшение времени lock уменьшает вероятность **dead-lock** и согласно [scalability theory](https://vladmihalcea.com/the-simple-scalability-equation/) это увеличивает throughput. Locks всегда вызывают serial executions (последовательное выполнение) операций. И согласно Закону Амдала max скорость программы обратно пропорциональна самому медленному последовательному куску программы (т.е. чем меньше serial кусок кода, тем меньше замедленее, т.к. скорость работы всей программы не может быть меньше скорости работы самого медленного ее последовательного куска).

Даже при **READ_COMMITTED** операции UPDATE and DELETE захватывают locks чтобы не дать параллельным transactions изменять данные. Так что задержка (откладывание на последний момент) lock операций (e.g. UPDATE and DELETE) может увеличить производительность, но нужно удостоверится что не будет нарушена consistency.

**Batching.** Откладывание операций на последний момент (т.е. последний момент logic transaction) также плюс в том, что операции SQL можно выполнить за одну (Batching) и Hibernate использует **JDBC batching optimization.** (группировка multiple DML statements into в одну операцию) это упеньшает **database round-trips**

**Read-your-own-writes consistency.**  У flush есть режимы **Scope.** Scope для flush это **Persistence Context** (если используется Session) или **Query** (если используется **Query**, **Criteria** или **TypedQuery** )

**Таблица режимов flush** (**AUTO** стоит в default)

| JPA FlushModeType | Hibernate FlushMode | Hibernate implementation details                                                                                                                                           |
| ----------------- | ------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| AUTO              | AUTO                | The Session is **sometimes** flushed before query execution.                                                                                                               |
| COMMIT            | COMMIT              | The Session is **only** flushed prior to a transaction commit.                                                                                                             |
|                   | ALWAYS              | The Session is **always** flushed before query execution.                                                                                                                  |
|                   | MANUAL              | The Session can **only** be manually flushed.                                                                                                                              |
|                   | ~~NEVER~~           | **Deprecated.** Use MANUAL instead. This was the original name given to manual flushing, but it was misleading users into thinking that the Session won’t ever be flushed. |

Автор блога [рекомендует](https://vladmihalcea.com/howoes-the-auto-flush-work-in-jpa-and-hibernate/) выбирать режим flush **ALWAYS**, т.к. с **AUTO** есть очень много проблем. При этом **ALWAYS** в Hibernate работает примерно как **AUTO** в JPA (по крайней мере на момент написания). Как я понял причина в том, что отследить flush в случае **ALWAYS** легче, чем в случае **ALWAYS** когда некоторые запросы **могут** вызвать неожиданные flush по мере работы приложения. Одна из причин этого в том, что для **native SQL** (и `@NamedNativeQueries`) Hibernate не может распознать что делать, т.к. в него встроена только ограниченная поддержка SQL, а не специфичная для всех DBs.

В режиме flush **AUTO** можно указать Hibernate какую таблицу синхронизировать перед выполнением **native SQL,** запроса это также полезно для **Second Level Cache**

```java
session.createSQLQuery("select id from product").addSynchronizedEntityClass(Product.class).uniqueResult();
```

## Dirty checking mechanism

Тут будет описан dirty checking mechanism
https://vladmihalcea.com/the-anatomy-of-hibernateirty-checking/

**Если коротко:** hibernate в кэше содержит и копию оригинального объекта, и ту что меняется. Поэтому на каждый объект нужно в 2 раза больше памяти. Если хотя бы одно поле Entity менялось Hibernate всеравно отслеживает объект целиком. Отслеживается каждое поле каждого объекта. На большое количество объектов может загрузить CPU. Можно включить экспериментальный режим с использованием проверки через Bytecode, начиная с Hibernate 5 его работа улучшена.

При **read only** транзакции не проходят dirty checking в кэше и для них не работает **transaction log** (по некоторым статьям). Поэтому такие транзакции быстрее.

## Transaction pitfalls in JPA (возможные проблемы транзакций в JPA)

Источник [тут](https://www.ibm.com/developerworks/library/j-ts1/index.html) статья старая, но может быть частично актуальна. Ответ влада о том что `@Transactional` нужна даже если в методе сервиса просто выполняется чтение [тут](https://stackoverflow.com/a/26327536)

1. ORM фреймворк выполняет операции через методы **только внутри транзакции**, если транзакция не была начата, то SQL не сгенерируется и запрос в DB не выполнится. Операции будут выполнены над logic transaction (т.е. физическая транзакция в DB выполнена не будет), старт транзакции требуется для запуска синхронизации между кэшем Hibernate и DB. Транзакция требуется для **integrity** и **consistency** (I и C из ACID)
   ```java
   public class TradingServiceImpl {
       @PersistenceContext(unitName="trading") EntityManager em;
    
       public long insertTrade(TradeData trade) throws Exception {
          em.persist(trade);
          return trade.getTradeId();
       }
   }
   ```

2. **@Transactional annotation pitfalls.** В Spring нужно не просто использовать аннотацию, а включить менеджер транзакций в конфигурации. При этом propagation level по умолчанию REQUIRED, read only стоит в false, а isolation level тот что в DB по умолчанию (обычно READ_COMMITTED).

3. **@Transactional read-only flag pitfalls.** **Note!** Начиная с версии Spring 5.1 исправлен баг с пробросом **readOnly** флага в Session. До этого только устанавливался `FlushType.MANUAL` и поэтому **только** отключался **automatic dirty checking mechanism**. Сама операция чтения данных это извлечение данных из `ResultSet` (наз. hydration и она нужна для dirty checking mechanism), она нужна чтобы сравнить текущее состояние Entity (snapshot) чтобы знать нужно ли делать `UPDATE` во время flush-time. Также detached state (видимо имеется ввиду оригинал Entity в кэше, который используется для сравнения) используется механизмом **versionless optimistic locking** (тут речь видимо о механизме optimistic locking, который не использует поле version, а реализован как-то иначе) для построения `WHERE`. Поэтому detached state хранится в Hibernate `Session` если только не использован **read-only** mode. Как результат экномится много памяти, т.к. состояние entity не хранится все время жизни Persistence Context (кэша L1). **Другими словами**, инфа ниже о **readOnly** может быть неактуальная. При этом **read only** означает только **загрузку Entities в read only** режиме, и их **можно удалять** через методы session из JPA.
   1. При `readOnly = true` и `Propagation.SUPPORTS` транзакция все равно выполнится без ошибок. Т.к. при `SUPPORTS` транзакция не будет запущена и выполнится local (database) transaction (т.е. на уровне DB, а не logic), `readOnly = true` применяется только если транзакция стартовала, а раз она не стартовала, то он проигнорирован.
        ```java
        // 1. выполнится без ошибок, т.к. траназкция будет запущена на уровне DB, а не приложения и readOnly будет проигнорирован
        @Transactional(readOnly = true, propagation=Propagation.SUPPORTS)
        public long insertTrade(TradeData trade) throws Exception {
            //JDBC Code...
        }
        ```
   2. При `REQUIRED` транзакция стартует и поэтому будет `Exception` при попытке изменения данных внутри запущенной с флагом `readOnly` транзакции. **Note!** В примере ниже  в транзакции Spring выполняется JDBC код, не JPA.
        ```java
        // 2. будет Exception т.к. попытка выполнить update внутри транзакции с readOnly
        @Transactional(readOnly = true, propagation=Propagation.REQUIRED)
        public long insertTrade(TradeData trade) throws Exception {
            //JDBC code...
        }
        ```
   3. Т.к. внутри `@Transactional` выполняется JDBC код, а не JPA, то **транзакция** тут не нужен вообще для выполнения **readOnly** операций. Он может вызвать overhead и ненужный shared read locks (в зависимости от того какой isolation level стоит по умолчанию).
        ```java
        // 3. При этом для readOnly если внутри транзакции JDBC code транзакцию не нужно стартовать, т.к. это overhead и ненужный shared read locks
        @Transactional(readOnly = true, propagation=Propagation.REQUIRED)
        public long insertTrade(TradeData trade) throws Exception {
            //JDBC code...
        }
        ```
   4. Если внутри `@Transactional` с **readOnly** выполнится JPA код, то этот флаг может быть или проигнорирован и запрос выполнится (TopLink), или флаг проигнорирован и ошибки не будет, но запрос не выполнится (Hibernate). Для Hibernate будет установлен flush mode == `MANUAL` и insert не произойдет. Суть в том, что JPA не гарантирует строгое поведение флага **readOnly** 
        ```java
        // 4. В JPA при readOnly нет гарантий конкретного поведения
        @Transactional(readOnly = true, propagation=Propagation.REQUIRED)
        public long insertTrade(TradeData trade) throws Exception {
            em.persist(trade);
            return trade.getTradeId();
        }
        ```
   5. При использовании JPA предлагается выполнять запрос вне транзакции таким образом `@Transactional(readOnly = true, propagation=Propagation.SUPPORTS)`, т.е. выставив `SUPPORTS`. Или вообще не использовать `@Transactional` аннотацию
        ```java
        // 5. SUPPORTS или отсутствие @Transactional выключает транзакции, рекомендуется для readOnly операций
        @Transactional(readOnly = true, propagation=Propagation.SUPPORTS)
        public TradeData getTrade(long tradeId) throws Exception {
            return em.find(TradeData.class, tradeId);
        }
        ```
   6. Противоречие с предыдущей инфой о том, что использовать транзакцию с **readOnly** нужно. Ответ Vlad Mihalcea [тут](https://stackoverflow.com/a/26327536). Далее неточная инфа, уточнить! Использование `@Transactional` использует `TransactionalInterceptor` для правильной привязки Session (т.е. Session будет видна в методе пока не закончит работу метод) к методу для использования`OpenSessionInViewFilter` и поэтому предотвращает lazy loading exceptions. Также использование транзакции создает всего 1 изолированный connection к DB вместо отдельного connection на время всей транзакции, а если такой аннотации не будет, то создастся отдельный connection на каждый запрос (и это плохо). Это происходит в том числе, потому что любая операция выполняется **внутри transaction** и если аннотация `@Transactional` не проставлена включится **autocommit**.
4. **REQUIRES_NEW transaction attribute pitfalls**. `REQUIRES_NEW` всегда стартует новую транзакцию независимо от того есть существующая или нет. Если `updateAcct` выполнится до rollback, то откат `insertTrade` не приведет к откату `updateAcct` и связные данные будут иметь разные значения.

   ```java
   @Transactional(propagation=Propagation.REQUIRES_NEW) // Service1
   public void updateAcct(TradeData trade) throws Exception {...}
   
   @Transactional(propagation=Propagation.REQUIRES_NEW) // Service2
   public long insertTrade(TradeData trade) throws Exception {
      em.persist(trade);
      updateAcct(trade);
      //exception occurs here! Trade rolled back but account update is not!
   }
   ```
5. **Transaction rollback pitfalls**. Если в каком-то месте внутри транзакции случится **checked exception**, то с начиная с места где оно случилось работа будет остановлена. Но все данные транзакции измененные до этого место будут commited. Чтобы откатить транзакцию нужно указать явно в **rollbackFor** для какого **checked exception** ее откатить. Или можно **re-throw RuntimeException**
    ```java
    @Transactional(propagation=Propagation.REQUIRED)
    public TradeData placeTrade(TradeData trade) throws Exception {
        try {
            insertTrade(trade);
            updateAcct(trade);
            return trade;
        } catch (Exception up) {
            throw up; //log the error
        }
    }

    // фиксим вариант который выше, делаем откат в случае checked exception
    @Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
    public TradeData placeTrade(TradeData trade) throws Exception {
        try {
            insertTrade(trade);
            updateAcct(trade);
            return trade;
        } catch (Exception up) {
            throw up; //log the error
        }
    }

    // throw RuntimeException прерывает Transaction и родительские к ней, если она не REQUIRES_NEW
    @Transactional(propagation=Propagation.REQUIRED)
    public TradeData placeTrade(TradeData trade) throws Exception {
        try {
            insertTrade(trade);
            updateAcct(trade);
            return trade;
        } catch (Exception up) {
            //log the error
            throw new RuntimeException(); // прервать Transaction
        }
    }
    ```
6.  **Transaction rollback pitfalls 2.** Подавить **RuntimeException** (которое часто используется для прерывания **Transactions**) в **try-catch** для **Transaction** можно только для **REQUIRES_NEW**, т.к. любое **RuntimeException** которое **throws** из `@Transactional` **method** будет **bubbling** (всплывать) по всем родительским `@Transactional`. Т.е. любой выброс **RuntimeException** остановит изначальную `@Transactional`.
    ```java
    // 1. Service1. Error. RuntimeException can not be suppressed in try-catch for REQUIRED
    // Error. RuntimeException will rollback the Transaction despite suppressed RuntimeException
    @Transactional void f1() { try { f2(); } catch(Exception e) { } }

    // Service2
    @Transactional void f2() { throw new RuntimeException(); }

    // 2. Right example. Can suppress RuntimeException in REQUIRES_NEW
    // Service3. No error. RuntimeException wiill be suppressed
    @Transactional void f3() { try { f4(); } catch(Exception e) { } } // f4() with REQUIRES_NEW

    // Service4
    @Transactional(propagation=Propagation.REQUIRES_NEW)
    void f4() { throw new RuntimeException(); }
    ```
7. Что на самом деле выполнится (с точки зрения транзакций), если вызвать method1()? **Ответ:** «*В связи с тем, что для поддержки транзакций через аннотации 
   используется Spring AOP, в момент вызова method1() на самом деле 
   вызывается метод прокси объекта. Создается новая транзакция и далее 
   происходит вызов method1() класса MyServiceImpl. А когда из method1() 
   вызовем method2(), обращения к прокси нет, вызывается уже сразу метод 
   нашего класса и, соответственно, никаких новых транзакций создаваться не
   будет*». Если для создания обертки-прокси будет использован **JDK dynamic proxy** (создает прокси на основе интерфейсов наследованных классом), то ответ верный, если будет использован **CGLib** (создает прокси для всех методов класса), который создает классы наследники, а не proxy объект (и этот наследник работает в качестве прокси), то ответ тоже верный. Независимо от библиотеки внутри поведение Spring AOP одинаково. При использовании **AspectJ** в режиме связывания **на этапе компиляции** (compile-time weaving) будет **вызвана транзакция внутри транзакции**, при использовании связывания **на этапе загрузки** (Load-time weaving) создадутся те же прокси, только во время загрузки объектов и поведение будет как у **JDK dynamic proxy** и **CGLib**

   ```java
   public class MyServiceImpl {
     @Transactional
     public void method1() {
       //do something
       method2();
     }
   	// для JDK dynamic proxy, CGLib и AspectJ (со связыванием на этапе загрузки) создатуся прокси и вызова транзакции внутри транзаксии НЕ будет
       // для AspectJ со связыванием на этапе компиляции вызовы встроятся в места вызовов и новая транзакция стартует
       @Transactional (propagation=Propagation.REQUIRES_NEW)
       public void method2() {
           //do something
       }
   }
   ```

8. **`@Async` + `@Transactional` в Spring.** Метод `@Async` создает новую **thread**, поэтому **context** текущей транзакции на него не распостраняется (т.е. нужно создавать новую `@Transaction` внутри него). **Итог:** при доступе к **lazy property** не доступен **proxy** для **entityManager** - получаем ошибку.  
Как вариант можно обработать `Future` в TransactionTemplate. Есть [баг](https://github.com/spring-projects/spring-framework/issues/24309) по которому транзакция не запускается на методе который одновременно `@Async` + `@Transactional` т.к. не реализован `ordering` для `AOP` и **Spring** не запускает `AOP` метода в порядке объявления (даже при `Compile-Time Weaving` для AOP).
    ```java
    @Async
    @Transactional
    f1(){} // нет гарантий что метод выполнится в транзации, при обращении к lazy полям возможны ошибки

    // решение
    class Bean1 { @Async f1(){ bean2.f2(); } }
    class Bean2 { @Transactional f1(){} }
    ```
    **Note.** Для решения проблемы с `LazyInitializationException` в `@Async` методе при доступе к **lazy** полям можно использовать **inner join** или другой отдельный запрос для чтения связных `Entities`.  

1. **При передаче ссылки на `Entity` в `@Async` метод:** **Proxy** для этой `Entity` в этом методе `@Async` недоступен, поэтому получить из `Entity` можно только **eager** поля (значения которые уже там есть), а при доступе к **lazy** полям будет `LazyInitializationException`.  
**Решение:** делать второй запрос внутри `@Async` чтобы получить `Entity`. Или использовать начитывать нужные поля заранее до передачи `Entity` в `@Async` метод.
    ```java
    class Bean1 { @Transactional f1(User user){ bean2.f1(repository.user); }}
    class Bean2 { @Async f1(){ bean3.f2(user); bean4.f2(user); } }
    class Bean3 { @Transactional f1(User user){ user.getRoles(); } } // ошибка если roles - это lazy: LazyInitializationException
    class Bean4 { @Transactional f1(User user){
            repository.findById(user.id).getRoles(); // правильно, получили нормальный proxy для Entity
        }
    }
    ```
    **Note.** Нужно понять тот же ли L1 кэш Hibernate ORM для `@Async` методов, если нет, то повторно читать `Entity` не эффективно.
1. `hibernate.enable_lazy_load_no_trans` ([source](https://vladmihalcea.com/the-hibernate-enable_lazy_load_no_trans-anti-pattern/)) - на каждую попытку запроса связных Entity будет создана **temporary Session** и новую **transaction**, каждая из них создаст новый **connection**, это нагрузит **connetction pool**
   * **Note.** В **Spring Boot** опция называется `spring.jpa.properties.hibernate.enable_lazy_load_no_trans=true`
   * **Note.** В **integration test** если нельзя убрать `LazyInitializationException` быстро правильным способом, то `enable_lazy_load_no_trans` может помочь, но так лучше не делать.
2.   **REQUIRES_NEW** `transaction` останавливает родительскую `transaction`, поэтому если в родительской были выполнены `create/update` **Entity**, то эта **Entity** не будет видна в **REQUIRES_NEW** `transaction` (т.е. ссылка на **Entity** будет указывать на **Object** и связанный с **DB** и она не будет отражать актуальное состояние **row in DB**) даже если передать **id** этой **Entity** и начитать ее **findById**, т.к. запись во время **Local Transaction** (пока не выполнился **flush**) еще не в **DB**.
     * Можно выполнить `repository.flush()` в родительской `transaction` и использовать **findById** в **REQUIRES_NEW** `transaction`. Но так лучше не делать т.к. теряется оптимизация транзакции на уровне **Hibernate ORM** (теряется как минимум **dirty checking** и **flush** в конце транзакции, может теряться **batch** оптимизация для **insert**)
     * Чтобы убрать проблемы с **REQUIRES_NEW** нужно реструктурировать код или заменить его на `AfterCommit/AfterRollback`

## Propagation level

Источник [1](https://stackoverflow.com/questions/8490852/spring-transactional-isolation-propagation), [2](https://www.ibm.com/developerworks/library/j-ts1/index.html), [3](https://dzone.com/articles/spring-transaction-propagation)

Определяет как будет создаваться транзакция внутри транзакции. Методы DAO, т.е. слоя `@Repository` тоже могут быть отмечены аннотацией `@Transactional`. И Propagation определяет как будет ли создаваться новая транзакция если `@Transactional` есть и в слое `@Service`:

- Список:
  - **TIMEOUT_DEFAULT** (значение `-1`) - использовать propagation level по умолчанию (т.е. **REQUIRED** для Spring)
  - **Propagation.REQUIRED** (by **default**) — выполняться в существующей транзакции, если она есть, иначе создавать новую
  - **Propagation.REQUIRES_NEW** — всегда выполняться в новой независимой транзакции. Если есть существующая, то она будет остановлена до окончания выполнения новой транзакции. Сохдается новая полностью независимая транзакция, commit или rollback которой произойдет независимо от внешней транзакции, у нее свой isolation level, locks etc. Внешняя транзакция будет остановлена вначале выполнения новой транзакции и возобновлена после. **Прим.** (не точно!) если будет exception в новой транзакции (`@Transaction(rollBackFor=...)` параметр для отката), то откат произойдет только в новой транзакции, но не в родительской к ней. Если Runtime exception, то откат будет, а если checked exception, то нет.
  - **Propagation.MANDATORY** — выполняться в существующей транзакции, если она есть, иначе (если ее нету) генерировать исключение.
  - **Propagation.SUPPORTS** — выполняться в существующей транзакции, если она есть, иначе выполняться вне транзакции (т.е. просто команды как буд-то транзакция не запущена).
  - **Propagation.NOT_SUPPORTED** — всегда выполняться вне транзакции. Если есть существующая, то она будет остановлена.
  - **Propagation.NESTED** — выполняться в существующей транзакции, если она есть. Если вложенная транзакция будет отменена, то это не повлияет на внешнюю транзакцию; если будет отменена внешняя транзакция, то будет отменена и вложенная. inner transaction часть внешней транзакции. Если текущей транзакции нет, то просто создаётся новая. Достигается путем установки savepoints вокруг вызова inner transaction. При этом inner transaction часть внешней транзакции и будет commited вконце внешней транзакции.
  - **Propagation.NEVER** — всегда выполнять вне транзакции, при наличии существующей генерировать исключение.

```
+-------+---------------------------+------------------------------------------------------------------------------------------------------+
| value |        Propagation        |                                             Description                                              |
+-------+---------------------------+------------------------------------------------------------------------------------------------------+
|    -1 | TIMEOUT_DEFAULT           | Use the default timeout of the underlying transaction system, or none if timeouts are not supported. |
|     0 | PROPAGATION_REQUIRED      | Support a current transaction; create a new one if none exists.                                      |
|     1 | PROPAGATION_SUPPORTS      | Support a current transaction; execute non-transactionally if none exists.                           |
|     2 | PROPAGATION_MANDATORY     | Support a current transaction; throw an exception if no current transaction exists.                  |
|     3 | PROPAGATION_REQUIRES_NEW  | Create a new transaction, suspending the current transaction if one exists.                          |
|     4 | PROPAGATION_NOT_SUPPORTED | Do not support a current transaction; rather always execute non-transactionally.                     |
|     5 | PROPAGATION_NEVER         | Do not support a current transaction; throw an exception if a current transaction exists.            |
|     6 | PROPAGATION_NESTED        | Execute within a nested transaction if a current transaction exists.                                 |
+-------+---------------------------+------------------------------------------------------------------------------------------------------+
```

## Transactin in Spring

### В Spring

**TransactionManager** - это класс по которому создается `@Bean` (бин) для конфигурации транзакций. Это обертка поверх JDBC (точнее поверх **DataSource** из Spring, которая в свою очередь - обертка для JDBC с различными утилитами и `PlatformTransactionManager` если есть только один **DataSource** на все приложение) для транзакций. Используется в `@Transactional` аннотациях над методами `@Service`. DataSource взаимодействует с встроенными в DB менеджерами транзакций и уже сами DB выполняют транзакции (т.е. Spring **как бы** просто передает правила по которым DB должна работать с транзакциями, а сам ничего не делает).

**Несколько TransactionManager** возможно создать. Тогда в `@Transactional` аннотациях над методами `@Service` нужно указывать **имя TransactionManager**, которую нужно использовать в конкретном случае.

```java
// Пример настройки транзакций

// 1. Включить @EnableTransactionManagement в @Configuration

// 2. создание конфигурации менеджера транзакций
@Bean(name="order")
public HibernateTransactionManager txName() throws IOException {
    HibernateTransactionManager txName= new HibernateTransactionManager();
    txName.setSessionFactory(...);
    txName.setDataSource(...);
    return txName;
}
// 3. использование
public class TransactionalService {
    @Transactional("order")
    public void setSomething(String name) { ... }

    @Transactional("account")
    public void doSomething() { ... }
}
```

**В случае Spring JMS** - все тоже самое. Обертка поверх JMS внутри которого в Session из JMS выполняются транзакции через встроенный TransactionManager.

**Spring JTA vs EJB** (возможно инфа устарела). Гибкий rollback, не зависит от application server, не нужно JNDI, интеграция с разными технологиями.

- **типы TransactionManager:**
  - **LocalTransactionManager** - для 1 phase commit (local transaction)
  - **Global TransactionManager** - **TP monitor** (transaction processing monitor)
- **TransactionManager умеет**:
  - работа с несколькими DataSource одновременно
  - XA (global) транзакции
  - получать состояние транзакций (мониторинг)
  - откатывать транзакции
  - приостановку/возобновление, attach/detach к разным потокам (transaction propagation level)
- **Типы TransactionManager** в Spring:
  - **DataSource** transaction manager (**JDBC**)
  - **Hibernate** transaction manager (**Hibernate**)
  - **JPA** transaction manager (**JPA**)
  - **JTA** transaction manager (**JTA**)
  - **JMS** transaction manager
  - **Jdo** transaction manager
  - **специфические для платформ**: WebLogic, WebSphere transaction managers etc

**PlatformTransactionManager** - центральный interface транзакций в Spring (для своей реализации рекомендуется наследовать AbstractPlatformTransactionManager, т.к. там готовые методы).
    - Имеет методы: getTransaction(TransactionDefenition), commit(status). rollback(status)
    - в getTransaction(...) можно задать **TransactionDefenition**, который описывает **isolation level** и **propagation level**

**При тестировании** часто используется **rollback only** флаг, чтобы все транзакции автоматически откатывались.

### В Spring Data

В **Spring Data** методы transactional по умолчанию. В том числе для `@RepositoryRestController`?

### В Spring Boot

Если **springata*** or **spring-tx** найдены в classpath, то `@EnableTransactionManagement` включается автоматически. Конфигурация происходит через класс `TransactionAutoConfiguration` из **spring-boot-autoconfigure.jar**.

```java
// примерный код конфигурации внутри Spring Boot
@Configuration
@ConditionalOnClass({PlatformTransactionManager.class})
@AutoConfigureAfter({JtaAutoConfiguration.class, HibernateJpaAutoConfiguration.class, DataSourceTransactionManagerAutoConfiguration.class, Neo4jDataAutoConfiguration.class})
@EnableConfigurationProperties({TransactionProperties.class})
public class TransactionAutoConfiguration {
// ...
}
```

### О том как работают транзакции в Spring

Дополнить по этой ссылке: https://dzone.com/articles/howoes-spring-transactional

### Open Session in View (OSIV) паттерн

Эта проблема больше относится к **Spring,** но без **OSIV** использовать JPA трудно и для этого придется делать много **DTO** объектов на каждый запрос пользователя (это стандартный подход). И использовать mapper  - одну из Java библиотек (например Orca или MapStruct) которая делает set свойств начитанных Entity в DTO (т.е. только тех полей Entity которые нужно показать пользователю в отправленном клиенту объекте DTO).

Подробнее и с картинками о том как работает [тут](https://stackoverflow.com/a/37526397)

**Open Session in View** (OSIV) - паттерн, тесно связан процессом загрузки:

**Суть:** когда сервис начитал данные и отправил во view (страницу), то view может использовать свойства (переменные) полученных объектов (сами свойства могут быть другими объектами). Эти связанные с полученными объектами свойства должны бы подгрузиться как lazy, но транзакция которую начали в сервисе уже закончилась до того как отработало view.
Вот чтобы этого не произошло транзакция не завершается до того как завершит работу view и все запросы свойств полученных объектов превращаются в запросы внутри этой транзакции.

**Минусы:** если не понимать что делаешь можно получить проблемы вроде n+1 query. Если начитать много Entities может быть загружен кэш L1 и GC очистка для этих Entities во время выполнения не сработает.
**Плюсы:** код лучше читаем и меньше кода  
**Альтернатива:** писать запросы вручную.

Можно отключить **OSIV**, в Spring это не сделано по умолчанию для совместимости и чтобы не усложнять разработку. Реализовано это через `OpenEntityManagerInViewInterceptor`. Так `spring.jpa.open-in-view=false`. При этом при попытке использовать OSIV получим `LazyInitializationExceptions`.

**Совет по обходу OSIV** (на проверено):

1. Использовать `EntityGraph`. При этом сам `EntityGraph` можно указывать над методом репозитория или в параметре метода через неофициальную библиотеку [Spring Data JPA EntityGraph](https://github.com/Cosium/springata-jpa-entity-graph) (по мере выполнения, в Runtime)
2. Использовать `QueryDSL` вместе с `join`, при этом **не использовать** совместно `EntityGraph` и `join`

```java
// 1. EntityGraph
@Entity
@NamedEntityGraph(name = "Item.characteristics",
    attributeNodes = @NamedAttributeNode("characteristics")
)
public class Item {}

public interface ItemRepository extends JpaRepository<Item, Long> {
    @EntityGraph(value = "Item.characteristics")
    Item findByName(String name);
}

// 2. Spring Data JPA EntityGraph, в Runtime
productRepository.findByLabel("foo", EntityGraphs.named("Product.brand"));
```

## Global (XA) vs local transactions vs distributed transactions

- Источники:
  
  - http://samolisov.blogspot.com/2011/02/xa-jta-javase-spring-atomikos-2.html
  - https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-jta.html
  - https://docs.spring.io/spring/docs/2.0.8/reference/transaction.html

- **Коротко:**
  
  - **Global transaction** - может объединять в 1ой transaction несколько ресурсов: DB (JDBC connection pool если точнее), JMS etc. Реализована в JavaEE поэтому часто пишут "управляется в application server" потому что jar с реализацией это часть applicaton server, который реализует JavaEE???
  - **Local transaction** - работа с одним источником данных, напр. JDBC connection
  - **distributed transaction** (не относится к Spring JTA) - это когда не просто несколько источников данных, а несколько приложений на рзных applicaton server и в рамках этих приложений должна выполняться транзакция

**XA** (extended architecture) - **другой определение** (возможно неточное!). Это стандарт, который связывает источники данных (**XA-resources**) с transaction manager. Чтобы работало XA должно быть реализовано для DB.

- **Global** (**XA** (extended architecture), distributed) **transaction** - часть спецификации JTA (Java EE). Работает с несколькими источниками данных одновременно. Использует спецификация XA или его модицикации. Для работы СУБД, JMS и их драйвера (JDBC, JMS driver) должны поддерживать протокол XA из JTA. **Другими словами:** это транзакция внутри которой может выполнять много других транзакций к различным исчтоникам данных (DB или JMS).

Источники данных **должны реализовать XA object** (напр. класс XARessources). Чтобы можно было работать с XA. Для кажого источника данных свой XA. Общение с DB или JMS происходит через их XA объекты (напр. получение статусов транзакции).

**Использование Global transaction:** Spring (JtaTransactionManager) поддерживает **global transactions**. Поддержка включается сама, если используются реализации **Atomikos** or **Bitronix** или внутри подходящего Java EE Application Server (т.е. в classpath есть jar с реализацией XA и она настроена). Для **global transactions** можно исползовать стандартные средства как: `@Transactional`. Если нужно принудительно отключить **global transactions** и использовать **Local transaction**, то нужно установить `spring.jta.enabled=false`

- **XA** (extended architecture) - спецификация распределенных транзакций

- **XA-resources** - источники данных DB (JDBC connection pool если точнее), JMS etc

- **XA-transaction.** - завершается только если commit во все источники данных (XA-resources) завершился успешно, иначе rollback. Реализована через алгоритм **Two-Phase Commit** (2PC)

- **XA-transaction coordinator** - координатор XA транзакций

- **Как работает Two-Phase Commit** (2PC):
  
  - **Этап подготовки** (commit-request phase (or voting phase)) - **XA-transaction coordinator** рассылает команды (prepare message) к DB сделать транзакцию, у каждой транзакции свой номер (TID). Транзакции выполняются, но их состояние не фиксируется, хранится на диске.
  - **Этап фиксации** (commit phase) - если все транзакции успешны **XA-transaction coordinator** фиксирует их, если нет посылает команды к DB чтобы откатить.

XA **поддерживает и 1 phase commit** если есть только один источник данных (single resource).

- По умолчанию в Spring Boot есть jar реализией Global transaction?

- Как включить local transaction и какие у них плюсы/минусы?

# Про equals и `@NaturalId`

Источники: [тут](https://vladmihalcea.com/the-best-way-to-implement-equals-hashcode-and-tostring-with-jpa-and-hibernate/), [тут](https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/), [официальная документация](https://docs.jboss.org/hibernate/orm/5.4/userguide/html_single/Hibernate_User_Guide.html#mapping-model-pojo-equalshashcode)

Использовать `@NaturalId` (как unique ключ) поле в `equals / hashCode`. Если их нет, то нужно реализовать сравнение на основе id, но при этом добавить проверку `if(id != null)` т.к. пока Entity не начиталась id может быть `null`

Правильная реализация equals и hashCode для Entity не имеющей `@NaturalId`, ключевая особенность это проверка id != null в реализации, т.к. id может быть null пока данные не загружены (31 выбрано как число не делящееся на 2 и поэтому удобнее для хэшей, операции с 31 быстрее на некоторых CPU, занимает 8 бит и нужен только 1 сдвиг для изменения чего-то там. При этом hashCode постоянная, потому что сравнения по id в данном случае достаточно, а когда id == null мы можем положиться на сравнение с ссылкой на сам объект):

**Note.**
* При сравнении по id метод hashCode должен возвращать константу **обязательно**, т.к. иначе из-за того что до загрузки Entity в память id == null, то hashCode == 0 (хэш от null == 0) и Entity попадет в 1ин бакет (в HashSet или HashMap), а после загрузки id изменится и hashCode будет указывать уже на другой бакет. В итоге операции с Set такие как remove() просто не найдут Entity. **Другими словами:** hashCode не должен меняться **до** и **после** того как Entity будет **flushed**.
* Или id должны сравниваться только для non-transient entities

**Note.** Из этого следует, что если id у двух одинаковых Entity еще null (до загрузки Entity и назначения настоящего id), то equals для entity1.id == entity2.id **обязано** вернуть **false**, в стандартной реализации (e.g. через IDE или lombok) оно вернет **true**.

**Для чего нужны** `equals / hashCode`:
1. Чтобы можно было работать с `Set` (например при `@ManyToMany`)
2. Чтобы можно было делать re attach какой-либо Entity из одно persistence context в другой (в `new persistence context`)

hashCode т.е. для использования equals/hashCode они должны выполняться для состояний: 
```
transient
attached
detached
removed (as long as the object is marked to be removed and it still living on the Heap)
```

**Пример:**
```java
// правильная реализация,
// чтобы можно было использовать в Set и делать re attach к new persistence context
@Entity
public class Book implements Identifiable<Long> {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (!(o instanceof Book))
            return false;

        Book other = (Book) o;

        // проверяем на null, когда оба id еще null,
        // то для entity1.id == entity2.id обязано вернуть false
        return id != null &&
               id.equals(other.getId());
    }

    @Override
    public int hashCode() {
        return getClass().hashCode(); // обязано быть константой,
        // чтобы все попало в один и тот же backet при id == null и при id == число
        // return 31; // старый вариант возвращал одну константу для всех class
    }

    //Getters and setters omitted for brevity
}

// реализация для @NaturalId (раз есть NaturalId, то id не используем)
@Entity
public class Book {
    @Id @GeneratedValue private Long id;
    private String title;
    @NaturalId private String isbn;
 
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        Book book = (Book) o;
        return Objects.equals(getIsbn(), book.getIsbn());
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(getIsbn());
    } 
}
```

При этом будут использованы множества buckets в HashSet or HashMap (что в теории плохо). Но подразумевается что при работе с Hibernate число entities ограничивается (разработчиком), т.е. обычно мы не загружаем в память слишком много Entities. И **никогда нельзя** (имеется ввиду вообще, а не только этот случай) загружать тысячи entities `@OneToMany` **Set** т.к. performance penalty on the database side is multiple orders of magnitude higher than using a single hashed bucket.

**Note.** (Не проверено!) equals и hashCode имеет смысл реализовывать если используются коллекции типа `Set` (и др.) и при этом там могут быть объекты в разных состояниях (detached и пр.). При использовании Spring Data JPA разных состояний нет и поэтому реализовывать equals и hashCode не нужно. При этом в официальной документации написано, что equals и hashCode унаследованные от Object обычно достаточно для уникальности. А в статье Vlad Mihalcen написано, что при использовании evict() и merge() стандартных equals и hashCode недостаточно (но их используют не всегда).

**Note.** (Исследовать) как влияют (помогают ли) реализации equals/hashCode в Cascade операциях.

# Кэширование и регионы кэша
Тут будет описание

# Связи

## Common
`@ManyToOne` связь рекомендуется ставить **LAZY** почти всегда (в отличии от `@...ToMany` которые **LAZY** по умолчанию).

**bidirectional collections** считаются лучшей практикой чем **unidirectional**

**`add...` vs `set...`** - `setter` можно использовать только с 1 стороны **даже** если связь 2х сторонняя, метод `add..` можно использовать только если добавляемая в связь запись сохраняется с обоих сторон (т.е. может понадобиться доп. `update`).

`@JoinColumn` - устанавливает имя столбца для связи явно (может быть пропущена, если стратегия генерации имен сама его создаст). Обычно ставится со стороны `@ManyToOne`
```java
@ManyToOne
@JoinColumn(name = "client_id")
Client client;

@OneToMany
@JoinColumn(name = "id", referencedColumnName = "client_id") // со стороны @OneToMany
List<Client> clients;
```

## One-To-Many
Для связи **@OneToMany** нельзя ограничить число связанных элементов. Для многих случаев **@ManyToOne** достаточно и не нужно делать **@OneToMany**. В этом случае одним из вариантов начитки child entities это с использованием `entityManager.createQuery()` (т.е. просто JPQL запроса)

```java
// пример @OneToMany
@Entity
public class PostComment {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne(
        fetch = FetchType.LAZY
    )
    @JoinColumn(name = "post_id")
    private Post post;
}

@Entity
public class Post {
    @Id
    @GeneratedValue
    private Long id;

    @OneToMany(
        mappedBy = "post",
        cascade = CascadeType.ALL,  // при сохранении текущей сохраняем связанные
        orphanRemoval = true        // удаляем те которые не имеют связей с текущей
    )
    private List<PostComment> comments = new ArrayList<>();

    // добавляем добавляя связь
    public void addComment(PostComment comment) {
        comments.add(comment);
        comment.setPost(this);
    }

    // удаляем удаляя связь
    public void removeComment(PostComment comment) {
        comments.remove(comment);
        comment.setPost(null);
    }
}

// использование
post.addComment(comment);
entityManager.persist(post);
```

## One-To-One
PK and FK обчно индексированы, поэтому с общим ключом Primary Key (аннотация `@MapsId`) будет работать быстрее. Связанная сущность всегда ведет себя как **FetchType.EAGER** что не проставляй. Единственный способ избежать этого включить **Bytecode enhancement**, поставить `@LazyToOne(LazyToOneOption.NO_PROXY)` и **НЕ** использовать `@MapsId`, только тогда **EAGER** заработает.
(**Прим.** не смотря на эту инфу о неэффективности судя по всему использование **EAGER** не так неэффективно, т.к. далее в [источнике](https://vladmihalcea.com/the-best-way-to-map-a-onetoone-relationship-with-jpa-and-hibernate/) используется EAGER и **говорится о эффективности**)

```java
@Entity
public class PostDetails {
    @Id
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId // Primary Key дочерней будет Foreign Key родителькой
    private Post post;
}

@Entity
public class Post {
    @Id
    @GeneratedValue
    private Long id;

    @OneToOne(
        mappedBy = "post",
        cascade = CascadeType.ALL,
        orphanRemoval = true,
        fetch = FetchType.LAZY
    )
    private PostDetails details;

    // т.к. связь one to one проверяем что связанная существует
    // т.е. чтобы синхронизировать обе стороны связи
    public void setDetails(PostDetails details) {
        if (details == null) {
            if (this.details != null) {
                this.details.setPost(null);
            }
        }
        else {
            details.setPost(this);
        }
        this.details = details;
    }
}
```

Эффективный способ использования - не использовать **bidirectional**, т.к. обе Entity загружаются за один запрос (проставить связь только с одной стороны), пример:

```java
@Entity
public class PostDetails {
    @Id
    private Long id;

    @OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Post post;
}
```

## Many-To-Many

Методы **add и remove** использованы чтобы синхронизировать **bidirectional** связь. Методы **equals & hashCode** реализовывать **обязательно**, т.к. на них полагаются **add и remove** методы

```java
@Entity
public class Tag {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(mappedBy = "tags")
    private Set<Post> posts = new HashSet<>();

    // equals & hashCode реализовывать обязательно, т.к. на них полагаются **add и remove** методы
}

@Entity(name = "Post")
@Table(name = "post")
public class Post {
    @Id
    @GeneratedValue
    private Long id;

    @ManyToMany(
        cascade = {
            CascadeType.PERSIST,
            CascadeType.MERGE
        }
    )
    @JoinTable(name = "post_tag",
        joinColumns = @JoinColumn(name = "post_id"),
        inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags = new LinkedHashSet<>();

    public void addTag(Tag tag) {
        tags.add(tag);
        tag.getPosts().add(this);
    }

    public void removeTag(Tag tag) {
        tags.remove(tag);
        tag.getPosts().remove(this);
    }

    // equals & hashCode реализовывать обязательно, т.к. на них полагаются **add и remove** методы
}

// используем
Post post1 = new Post("JPA with Hibernate");
Tag tag1 = new Tag("Java");

post1.addTag(tag1); // или post1.removeTag(javaTag);
entityManager.persist(post1);
// post1.removeTag(javaTag); может удалить связь
```

## Non Primary-Key `@ManyToOne` mapping (mapping не по id)

Источник: [тут](https://vladmihalcea.com/how-to-map-a-manytoone-association-using-a-non-primary-key-column/)

В Hibernate можно делать mapping не по id, а по обычным полям. Для этого `Entity` **должно** `implements Serializable`

```java
@Entity(name = "Book")
public class Book implements Serializable {
    @Id
    @GeneratedValue
    private Long id;
 
    @NaturalId
    private String isbn;
}

@Entity(name = "Publication")
public class Publication {
    @Id
    @GeneratedValue
    private Long id;
 
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(
        name = "isbn",
        referencedColumnName = "isbn"
    )
    private Book book;
}
```

# CascadeType

Источник: [1](https://vladmihalcea.com/a-beginners-guide-to-jpa-and-hibernate-cascade-types/)

**Note.** `CascadeType` имеет смысл проставлять только с той стороны с который имеет смысл делать сохранение (или др.). Например если есть таблицы **Tag** и **Post** с `@ManyToMany`, то при создании **Tag** мы не устанавливаем ему новые записи **Post** (т.к. это не имеет смысла) и значит для **Tag** не нужно устанавливать `Cascade.PERSIST` and `Cascade.MERGE`.

**Note.** В hibernate orm `cascade=all` сохраняет и все связанные **transient** entities, но не нужно бездумно ставить `cascade=all` с обеих сторон, только с нужной чтобы не было лишнего insert когда hibernate сохранил по связи с одной стороны, а потом повторно сохраняет по связи с другой.

**Note.** `cacade = {}` (не включен) **by default**

**Note!** Не смотря на то что все связи **не cascade** по умолчанию нужно установить туда связанную сущность, чтобы сохранилось **id** (foreign key) при этом остальные поля связанной Entity не сохраняются (только id). Если не установить связанную entity и `constraint not null`, то будет `Exception`.

**Note.** Если для связанных Entities стоит `constraint not null`, то единственный способ сохранить эти entities - это **cascade**.

**Note.** Если связь **bidirectional** то не получится создать сначала родительскую Entity, а потом дочернюю (как в SQL), т.к. сделать `setEntity(...)/setEntities(...)` нужно с обеих сторон. Сработает только **cascade**. `@NotFound` тоже не сработает.

```java
@ManyToOne(cascade = {javax.persistence.CascadeType.PERSIST})
// @ManyToOne(cascade = {javax.persistence.CascadeType.ALL})
Manufacturer manufacturer;
// или
@Cascade(CascadeType.PERSIST)
Manufacturer manufacturer;
```

# Как правильно делать merge

Суть проблемы в том, что нужно добавить связные сущности к родительской и родительскую сущность в кажду связную (в цикле) перед merge.

```java
// ошибка
Post post = entityManager.createQuery(
    "select p " +
    "from Post p " +
    "join fetch p.comments " +
    "where p.id = :id", Post.class)
.setParameter("id", 1L)
.getSingleResult();

post.setComments(comments);

// ошибка
Post post = entityManager.createQuery(
    "select p " +
    "from Post p " +
    "join fetch p.comments " +
    "where p.id = :id", Post.class)
.setParameter("id", 1L)
.getSingleResult();

entityManager.detach(post);
post.setComments(comments);
entityManager.merge(post);

// правильно
Post post = entityManager.createQuery(
    "select p " +
    "from Post p " +
    "join fetch p.comments " +
    "where p.id = :id", Post.class)
.setParameter("id", 1L)
.getSingleResult();

entityManager.detach(post);

post.getComments().clear();
for (PostComment comment : comments) {
    post.addComment(comment);
    // тут метод addComment это не set, а реализованный метод
    // public void addComment(PostComment comment) {
    //     comments.add(comment);
    //     comment.setPost(this);
    // }
}

entityManager.merge(post);
```

# DTO projection, ResultTransformer и про то что он делает SQL запросы эффективнее

<small>Источник: [тут](https://vladmihalcea.com/the-best-way-to-map-a-projection-query-to-a-dto-with-jpa-and-hibernate/)</small>

**Суть:** Запрос напрямую мапится в DTO, без получения Entity. Этот подход один из рекомендованных, когда нужно получить часть полей.

**Note.** _ВОЗМОЖНО_ это слишком сложно и Spring Data JPA делает не таким нужным использование данного механизма.

**ResultTransformer** трансформирует результат запроса в DTO (это DTO еще может называться неофициально **DTO projections** и оно эффективно только при read only запросах). Т.е. он кастомизирует **ResultSet**. ResultTransformer **делает генерацию SQL запросав эффективнее.**

**Note.** В старых версиях Hibernate ORM `ResultTransformer` выглядел по другому (если встретятся старые примеры).

```java
// Способы сконвертировать напрямую в DTO

// 1. Tuple and JPQL
List<Tuple> postDTOs = entityManager.createQuery("""
    select p.id as id, p.title as title
    from Post p
    where p.createdOn > :fromTimestamp
    """, Tuple.class)
.setParameter(
    "fromTimestamp",
    Timestamp.from(LocalDate.of(2020, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC))
).getResultList(); // получаем
 
Tuple postDTO = postDTOs.get(0); // используем

// 2. Constructor Expression
List<PostDTO> postDTOs = entityManager.createQuery("""
    select new com.vladmihalcea.book.hpjp.hibernate.forum.dto.PostDTO(p.id, p.title)
    from Post p
    where p.createdOn > :fromTimestamp
    """, PostDTO.class)
.setParameter(
    "fromTimestamp",
    Timestamp.from(LocalDate.of(2020, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC))
).getResultList(); // получаем

// 3. Tuple and native SQL
List<Tuple> postDTOs = entityManager.createNativeQuery("""
    SELECT p.id AS id, p.title AS title
    FROM Post p
    WHERE p.created_on > :fromTimestamp
    """, Tuple.class)
.setParameter(
    "fromTimestamp",
    Timestamp.from(LocalDate.of(2020, 1, 1).atStartOfDay().toInstant(ZoneOffset.UTC))
).getResultList(); // получаем
  
Tuple postDTO = postDTOs.get(0); // используем

// 4. ConstructorResult
@NamedNativeQuery(
    name = "PostDTO",
    query = """
        SELECT
           p.id AS id,
           p.title AS title
        FROM Post p
        WHERE p.created_on > :fromTimestamp
        """,
    resultSetMapping = "PostDTO"
)
@SqlResultSetMapping( // конвертер в DTO
    name = "PostDTO",
    classes = @ConstructorResult(
        targetClass = PostDTO.class,
        columns = {@ColumnResult(name = "id"), @ColumnResult(name = "title")})
)

List<PostDTO> postDTOs = entityManager.createNamedQuery("PostDTO").setParameter(
    "fromTimestamp",
    Timestamp.from(LocalDateTime.of(2020, 1, 1, 0, 0, 0).toInstant(ZoneOffset.UTC))
).getResultList();

// 5. ResultTransformer and JPQL, это метод Hibernate ORM и он использует Reflection (и поэтому метод гибкий)
List<PostDTO> postDTOs = entityManager.createQuery("""
    select p.id as id, p.title as title
    from Post p
    where p.createdOn > :fromTimestamp
    """)
.setParameter(
    "fromTimestamp",
    Timestamp.from(LocalDateTime.of(2020, 1, 1, 0, 0, 0).toInstant(ZoneOffset.UTC))
)
.unwrap(org.hibernate.query.Query.class) // JPA запрос
.setResultTransformer(Transformers.aliasToBean(PostDTO.class)) // во что конвертировать
.getResultList();

// 6. ResultTransformer and a Native SQL query, это метод Hibernate ORM и он использует Reflection (и поэтому метод гибкий)
List<PostDTO> postDTOs = entityManager.createNativeQuery("""
    select p.id as "id", p.title as "title"
    from Post p
    where p.created_on > :fromTimestamp
    """)
.setParameter(
    "fromTimestamp",
    Timestamp.from(LocalDateTime.of(2020, 1, 1, 0, 0, 0).toInstant(ZoneOffset.UTC))
)
.unwrap(org.hibernate.query.NativeQuery.class) // SQL запрос
.setResultTransformer(Transformers.aliasToBean(PostDTO.class))
.getResultList();


// 7. Используем анонимный класс ResultTransformer
List<PersonAndCountryDTO> personAndAddressDTOs = entityManager.createQuery(
    "select p, c.name " +
    "from Person p " +
    "join Country c on p.locale = c.locale " +
    "order by p.id")
.unwrap( org.hibernate.query.Query.class )
.setResultTransformer(new ResultTransformer() {
    @Override
    public Object transformTuple(Object[] tuple, String[] aliases) {
        return new PersonAndCountryDTO((Person) tuple[0], (String) tuple[1]);
    }

    @Override
    public List transformList(List collection) { return collection; }
}).getResultList();
```

# StatelessSession

Источник: [1](https://stackoverflow.com/a/48980332)

# Зачем нужно делать Detached объекты в Hibernate Session

Т.к. если объектов в hibernate Session много, то может быть `OutOfMemoryException`. Можно делать `clear()` and `evict()`. Это может случится если **hibernate Session** существует пока жива **server session**.

Если нужно работать с большим числом данных, то тоже может понадобится сделать **detached**.

Другой вариант ответа: **detached entities** существует, чтобы **уменьшить количество lock** во время transactions, т.е. уменьшить время выполнения transactions.

Другой вариант ответа: Чтобы **хранить** Entity между несколькими Hibernate Session (между запросами пользователя), паттерн **session-per-request**

# Hypersistence Optimizer

Это неофициальный инструмент проверки того правильно ли используется mapping c Hibernate ORM

# `@DynamicUpdate` и `@DynamicInsert`

Hibernate при update делает update **всех** полей Entity даже если изменилось только **несколько**. Аннотация `@DynamicUpdate` делает так что в update запросе только те поля которые изменились. Эта аннотация **обязательна при использовании optimistic locking** (видимо под обязательно тут имеется ввиду рекомендуется?). По умолчанию Hibernate использует кэшированное SQL statement, а с этой аннотацией Hibernate отслеживает и проверяет текущее состояние Entity с измененным и **генерирует** новый SQL statement каждый раз при update. Т.е. `@DynamicUpdate` плохо **влияет на производительность** и ее нужно использовать если **в Entity много полей и меняются только несколько** из них или если **используется version less optimistic lock**.
```java
@Entity(name = "Post")
@Table(name = "post")
@DynamicUpdate
@DynamicInsert(value = true)
public class Post {}
```

# Collection vs Bag vs List vs Map vs Set

Источник: [1](https://thoughts-on-java.org/association-mappings-bag-list-set/), [2](https://vladmihalcea.com/the-best-way-to-use-the-manytomany-annotation-with-jpa-and-hibernate/)

**Допустимые типы коллекция в Hibernate:**

* **Collection** - поведение зависит от типа реализации на который указывает. Реализация по умолчани: ???
* **Set** - рекомендован для `@ManyToMany`, т.к. сам на уровне приложения (не DB) удаляет дубликаты может оказаться полезным там где дубликаты не нужны
* **SortedSet** - 
* **List** - рекомендован для остальных типов связи
* **Bag** - это `unordered List`, т.е. `List` над которым не стоит `@OrderBy` (при этом в целом, не конкретно тут, `orderBy` лучше чем `sortedBy`)
* **Map** - https://thoughts-on-java.org/map-association-java-util-map/
* **SortedMap** - 
* `org.hibernate.usertype.UserCollectionType` - Можно реализовать свои типы наследуя его, сам по себе не используется

В старых версиях Hibernate коллекции вели себя по другому и были не такими эффективными. Это нужно помнить при чтении документации.

**На практике:** почти всегда используется `List`, кроме `Set` для `@ManyToMany` и то не всегда. 

При этом рекомендуется инициализировать коллекции в Entity:

```java
@Entity
class User {
    // Set для @ManyToMany
    @ManyToMany
    Set<Employee> employees = new HashSet<>();

    // Для сортированного Set нужно использовать SortedSet и @SortNatural
    // внутри используется Comparable
    @OneToMany(cascade = CascadeType.ALL)
    @SortNatural
    // @SortComparator(ReverseComparator.class) - или кастомный Comparator
    SortedSet<Phone> phones = new TreeSet<>();

    Collection<User> users = new List<>(); // можно так использовать List (но возможно лучше не нужно)
}
```

Hibernate ORM использует не стандартные коллекции, а модифицированные классы коллекций, т.е. `new HashSet()` на самом деле будет не `HashSet`, а объектом другого класса, прокси. В документации написано, что-то про то, что hibernate не может различить коллекцию инициализированную `null` или инициализированную `пустой` коллекцией. На практике в коллекции был `null`, поэтому лучше инициализировать пустой коллекцией явно через `= new ArrayList()`.

# `MultipleBagFetchException`
Source: [1](https://vladmihalcea.com/hibernate-multiplebagfetchexception/)

Возникает если проставить `Fetch(JOIN)` (или **join fetch** в HQL) для **List** и **List** больше чем с 1ой стороны связи. Чтобы исправить нужно использовать **Set**, но при этом если связь **ManyToMany**, то получим **Cartesian Product** таблиц и тройное увеличение записей (e.g. `20*30*50`). Поэтому рекомендуется делать **2 отдельных** запрос с двух сторон связей.

**PASS_DISTINCT_THROUGH** - не посылает distinct в SQL, а удаляем дубли только в **parent-child joined ResultSet**

```java
List<Post> posts = entityManager.createQuery("""
    select distinct p
    from Post p
    left join fetch p.comments
    where p.id between :minId and :maxId""", Post.class)
.setParameter("minId", 1L)
.setParameter("maxId", 50L)
.setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
.getResultList();
 
posts = entityManager.createQuery("""
    select distinct p
    from Post p
    left join fetch p.tags t
    where p in :posts""", Post.class)
.setParameter("posts", posts)
.setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
.getResultList();
```

# Sorting vs Ordering

https://thoughts-on-java.org/ordering-vs-sorting-hibernate-use/

# Типы id и их генерация

**GenerationType из JPA**
* `GenerationType.AUTO` - jpa provider сам решает что делать
* `GenerationType.IDENTITY` - не очень для производительности, использует механизм DB которая сама генерирует id при insert.  Возвращаемый идентификатор имеет тип long, int или short.
* `GenerationType.SEQUENCE` - использует SEQUENCE из DB чтобы взять уникальный id, по умолчанию делает отдельный select для начитки этого уникального значения (это можно поменять в настройках), работает быстро. Возвращаемый идентификатор имеет тип long, int или short.
  * **Note.** При этом **by default** установленный в Entity id будет игнорироваться и будет взят следующийи из `SEQUENCE`.
* `GenerationType.TABLE` - редко используется, медленная, эмулирует SEQUENCE, использует pessimistic locks.

**Другие типы:**
* UUID - использует 128-битный UUID-алгоритм для генерации идентификаторов типа String, уникальных в пределах сети (используется IP-адрес). UUID кодируется как строка шестнадцатеричных цифр длиной 32-символа.
* hilo - использует hi/lo алгоритм для рационального генерирования идентификаторов типа long, int или short уникальных для таблицы или колонки (по умолчанию - hibernate_unique_key и next_hi, соответственно). Hi/lo алгоритм генерирует идентификаторы, которые уникальны только для конкретной базы данных. Не стоит использовать данную стратегию для соединений, установленных с помощью JTA или с помощью определяемых пользователем соединений.
* guid - использует генерируемую СУБД GUID-строку на MS SQL Server или MySQL.
* increment - генерирует идентификаторы типов long, int и short, которые являются уникальными только если никакой другой процесс не добавляет данные в ту же самую таблицу. Данную стратегию нельзя использовать в кластерном окружении.
* seqhilo - использует hi/lo алгоритм для рационального генерирования идентификаторов типа long, int или short. В качестве источника данных используются именованные последовательности.
* assigned - позволяет приложению самостоятельно устанавливать идентификатор объекта перед тем как вызвать save(). Данная стратегия установлена по-умолчанию и будет использоваться, если не указывать тег generator вообще.
* select - извлекает первичный ключ, назначенный триггером. Триггер выбирает некоторое уникальное значение строки и возвращает значение первичного ключа.
* foreign - использует идентификатор другого связанного объекта. Обычно используется в связке с <one-to-one> ассоциацией.

**Note.** **Техника установки имени `@SequenceGenerator`**  
**Note.** `allocationSize` указывает сколько id **резервирует** Hibernate ORM, должно совпадать с параметром `increment by` в `sequence` в DB. Значение **50** `by default` показывает шаг через который будет сделан **INCREMENT**.
```sql
-- postgres будет генерировать каждые 50 id
create sequence user_id_seq MINVALUE 1 MAXVALUE 999999999999999999999999999 START WITH 1 INCREMENT BY 50 NOCACHE NOCYCLE;
```
```java
@Entity
@SequenceGenerator(name = Client.ID_SEQ, sequenceName = Client.ID_SEQ, allocationSize = 50) // в кэш берем 50 id из sequence
public class Client {
    public static final String ID_SEQ = "client_id_seq";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ID_SEQ)
    Long id;
}
```

# Использование дат в Entity
По умолчанию дата сохраняется в полном формате `TIMESTAMP`  `год : мес : число : время` до милисекунд
```java
class User {
    @Temporal(TemporalType.DATE) Date time; //TemporalType.TIME, TemporalType.TIMESTAMP
}
```

# Авто подстановка в поле Entity текущей даты при сохранении Entity
```java
@CreationTimestamp
@Temporal(TemporalType.TIMESTAMP)
Date createDate;

@UpdateTimestamp
@Temporal(TemporalType.TIMESTAMP)
Date modifyDate;

@CreatedDate
long createdDate;

@LastModifiedDate
long modifiedDate;
```

# `@Size`, `@Length`, and `@Column(length=value)`
* `@Size(min = 3, max = 15)` - из JPA
* `@Length(min = 3, max = 15)` - аналог `@Size` из Hibernate
* `@Column(length=5)` (по умолчанию длина `255`, тип `int`) - используется для задания физической длинны строки столбца при DDL (генерации схемы столбца табл.) и преобразуется в `varchar(5)`, т.е. валидации на уровне Java приложения не будет, при вставке большего размера появится SQL exception. Ее можно использовать совмесно с `@Size` для валидации на уровне Java приложения

# `@Immutable`
https://www.baeldung.com/hibernate-immutable

`@Immutable` обычно отмечают Entity которые `View` из SQL или табл. которые не будут меняться (dictionaries), это работает как readOnly=true и отключает dirty checking ускоряя performance помимо блокирования табл. от изменений.

# Custom Types and `@Type`, Hibernate Types
<small>Источник: [тут](https://www.baeldung.com/hibernate-custom-types), [про Hibernate Types](https://vladmihalcea.com/java-map-json-jpa-hibernate/)</small>

**Note.** [Hibernate Types](https://vladmihalcea.com/a-beginners-guide-to-hibernate-types/) - неофициальный сборник custom типов для Hibernate.

**Для чего:** например можно хранить в `Map` локализации которые получать из `json` поля DB.

```java
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class Book {
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    private Map<String, String> properties = new HashMap<>();
}
```

# Hibernate Event Listener, `@PrePersist`, `@PreUpdate` etc
Источник: [1](https://stackoverflow.com/a/39618826), [2](https://vladmihalcea.com/hibernate-event-listeners/)

Listener vs Interceptor https://stackoverflow.com/a/45448865

```java
// 1. через аннотации
@Entity
public class Bar {
    @PrePersist void onPrePersist() {  }
    @PreUpdate void onPreUpdate() {  }
    @PreRemove void onPreRemove() {  }   
}

// 2. через Listener
public class AuditListener { @PrePersist @PreUpdate @PreRemove void listener(Object object) {  } } // 2.1 создаем, Object - Entity
@EntityListeners({AuditListener.class, PostInterceptor.class}) @Entity class Bar {  } // 2.2 подключаем

// 3.1 вручную через interface
@Component // из Spring
public class EventListener implements PostInsertEventListener,
        PostUpdateEventListener, PostDeleteEventListener { // PreUpdateEvent, PostCommitInsertEventListener etc
    @Override
    public void onPostInsert(PostInsertEvent event) { event.getId(); event.getEntity(); }

    @Override
    public void onPostUpdate(PostUpdateEvent event) { }

    @Override
    public void onPostDelete(PostDeleteEvent event) { }

    @Override // включаем listener для событий транзакций, e.g. видимо для commit событий: PostCommitInsertEventListener etc
    public boolean requiresPostCommitHanding(EntityPersister persister) { return true; } // обычно true
}

// 3.2 регистрируем вручную, если EventListener без @Component
void listenerRegistration(EventListener listener) {
    SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class); // т.к. в JPA такого нет
    EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
    registry.getEventListenerGroup(EventType.POST_INSERT).appendListener(listener);
    registry.getEventListenerGroup(EventType.POST_UPDATE).appendListeners(listener);
    registry.getEventListenerGroup(EventType.POST_DELETE).appendListeners(listener);
}
```

# Hibernate Filter и фильтр related nested Entity collections
Source: [1](https://thorben-janssen.com/hibernate-filter/), [2](https://docs.jboss.org/hibernate/stable/orm/userguide/html_single/Hibernate_User_Guide.html#pc-filtering)

**Hibernate Filter** - назначение фильтра для связных коллекций родительской Entity, не из JPA  
в Hibernate ORM невозможно отфильтровать вложенные дочерние коллекции (записи) обычными запросами (e.g. `criteria api` etc), фильтровать можно только parent Entity

**Note.** Как вариант: делать явный **join** с условием или с **subselect** (где часть записей уже отсечено)

**Pitfall.** При **2nd level cache** не работает. Нельзя использовать `@Filter` and `@Cache` одновременно, т.к. в **2nd level cache** хранятся не сортированные результаты

**Note.** Был **Deprecated** [createFilter](https://docs.jboss.org/hibernate/orm/5.5/javadocs/org/hibernate/Session.html#createFilter-java.lang.Object-java.lang.String-)

```java
// Class filter
@Filter(name = "dateFilter", condition = "birthDate >= :minDate and birthDate <= :maxDate")
@Entity
public class ChessPlayer {
    Date birthDate;
}

// Field filter
@Entity
class ChessGame {
    @Id Long id;
 
    @Filter(name = "dateFilter", condition = "date >= :minDate and date <= :maxDate")
    LocalDate date;
}

// use
@Autowired EntityManager em;
Session session = em.unwrap(Session.class);
Filter filter = session.enableFilter("proFilter"); // activate filter by name
filter.setParameter("minDate", 123); // set params
filter.setParameter("maxDate", 456);
var myEntity = em.find(MyEntity.class, 1L);
var myEntity2 = myRepository.findAll(); // we can use repository

```

# Hibernate Interceptors
https://www.baeldung.com/hibernate-interceptor

# Hibernate Second-Level Cache
https://www.baeldung.com/hibernate-second-level-cache

# Логирование и тюнинг запросов и др.

<small>**Источник:** vladmihalcea ([1](https://vladmihalcea.com/jpa-hibernate-query-hints/), [2](https://vladmihalcea.com/execution-plan-oracle-hibernate-query-hints/), [3](https://vladmihalcea.com/the-best-way-to-log-jdbc-statements/), [4](https://vladmihalcea.com/hibernate-performance-tuning-tips/), [5](https://vladmihalcea.com/hibernate-facts-always-check-criteria-api-sql-queries/), [6](https://vladmihalcea.com/why-you-should-always-use-hibernate-connection-provider_disables_autocommit-for-resource-local-jpa-transactions/)), baeldung ([1](https://www.baeldung.com/hibernate-common-performance-problems-in-logs). [2](https://www.baeldung.com/hibernate-query-plan-cache), [3](https://www.baeldung.com/sql-logging-spring-boot)) [тут](https://www.youtube.com/watch?v=b52Qz6qlic0), [тут](https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#best-practices)</small>

**НЕ ЗАКОНЧЕНО** - см. источники

* Не нужно использовать анти паттерны для решения [LazyInitializationException](https://vladmihalcea.com/the-best-way-to-handle-the-lazyinitializationexception/): **Open Session in View** и `hibernate.enable_lazy_load_no_trans`. Вместо этого нужно делать `JOIN FETCH` или [DTO projection](https://vladmihalcea.com/the-best-way-to-handle-the-lazyinitializationexception/)
* **benchmark**
  * **jmh** - benchmark который можно использовать для измерения производительности hibernate orm
* **statistic** - включаем статистику в Hibernate
    ```properties
    hibernate.generate_statistics=true
    org.hibernate.stat=DEBUG
    ```
* **Datasource proxy** - инструменты для просмотра реальных sql запросов, т.к. все действия с DB идут через JDBC, то нужен прокси для просмотра инфы - например параметров SQL+запросов, т.к. вместо SQL отправляется только код процедуры и список параметров.
    * **datasource-proxy**
    * **p6spy** - самый популярный
* **Query counting interceptor** - можно ставить interceptors в Hibernate ORM на JPA, Criteria, JDBC это дает считать количество запросов и сравнивать с тем что ожидаем
  * **db-util**
* **Log and format SQL statement** - включаем логирование запросов и доп. инфы
    ```properties
    # log4j
    log4j.logger.org.hibernate=INFO, hb
    log4j.logger.org.hibernate.SQL=DEBUG
    log4j.logger.org.hibernate.type=TRACE
    log4j.logger.org.hibernate.hql.ast.AST=info
    log4j.logger.org.hibernate.tool.hbm2ddl=warn
    log4j.logger.org.hibernate.hql=debug
    log4j.logger.org.hibernate.cache=info
    log4j.logger.org.hibernate.jdbc=info

    log4j.appender.hb=org.apache.log4j.ConsoleAppender
    log4j.appender.hb.layout=org.apache.log4j.PatternLayout
    log4j.appender.hb.layout.ConversionPattern=HibernateLog --> %d{HH:mm:ss} %-5p %c - %m%n
    log4j.appender.hb.Threshold=TRACE

    # hibernate
    spring.jpa.properties.hibernate.show_sql=true
    spring.jpa.properties.hibernate.format_sql=true
    spring.jpa.properties.hibernate.use_sql_comments=true

    # в spring data jpa
    spring.jpa.properties.hibernate.show_sql=true
    spring.jpa.properties.hibernate.format_sql=true
    spring.jpa.properties.hibernate.generate_statistics=true
    spring.jpa.properties.hibernate.use_sql_comments=true
    spring.jpa.properties.hibernate.type=trace

    # в spring настройка log через Dlogging (не проверено)
    logging.level.org.hibernate.SQL=DEBUG
    logging.level.org.hibernate.type=TRACE
    logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE

    # Для criteria api можно установить чтобы вклеивать параметры в сам запрос (чтобы в логах было видно, но возможно не обязательно) 
    hibernate.criteria.literal_handling_mode=INLINE

    # логирование JdbcTemplate Queries
    logging.level.org.springframework.jdbc.core.JdbcTemplate=DEBUG  # logging statements
    logging.level.org.springframework.jdbc.core.StatementCreatorUtils=TRACE # log parameters of prepared statements
    ```
* **JDBC tuning**
    1. выбрать хороший **connection pool** (новые реализации быстрее)
    2. Batching, fetching - настройка hibernate на max число запросов в batch
        ```properties
        hibernate.jdbc.batch_size=50 #
        hibernate.jdbc.fetch_size=50 ## иначе будет много roundtrip
        ```
* Использовать **native query** когда это будет быстрее (не все можно написать на Hibernate ORM). Можно использовать **save/update** через **Hibernate ORM**, а **get** через **JOOQ** (который умеет интегрироваться с Hibernate ORM).
* Использовать метод **session.doWork** который даст доступ к JDBC
* Не использовать **eager load** т.к. его нельзя изменить динамически на **lazy** (в обратную сторону можно)
* В методе **createQuery("select c from com.site.Client")** можно указать пакет если имена Entity дублируются (к одной табл привязано несколько Entity и у каждой отличается набор полей, в этих же Entity можно указывать разные параметры запросов для одной табл.) 
* Если табл. большая вместо **fetch join** лучше использовать subselect + BatchSize (в самом запросе указать BatchSize нельзя). Если различных запросов будет много, то будет перегружен кэш PreparedStatement, поэтому Hibernate ORM оптимизирует количество запросов при BatchSize
* Hibernate ORM при **BatchSize** использует **"слепое предсказывание"**, когда вместо запроса `where = 'name'` может быть `where 'name' in (...)`. Т.к. Entities все равно будут в кэше L1, то Hibernate предполагает что понадобится не 1 Entity, а и остальные тоже.
* Через `em.createQuery("select c from Client").setHint(QueryHint.FETCHGRAPH);` (фича JPA работает через `EntityManager`) можно указать **EntityGraph** чтобы начитать только часть полей Entity
* В batch операциях выполнять flush чтобы не держать все в памяти
* JDBC операция batch может делать batch только записей которые относятся к одинаковым таблицам. Т.е. если **чередовать сохранение** записей для **разных** Entity, то batch не сработает. Чтобы это обойти нужно включить группировку записей по их Entity перед `save()` (на самом деле batch будет, но только по 1му `Statement`, т.е. по сути не будет).
    ```properties
    hibernate.order_insert=true
    hibernate.order_update=true
    hibernate.jdbc.batch_versioned_data=true
    ```
    **Note.** Если в приложении используется "натуральное версионирование" (**optimistic lock** etc), то batch для этого включается `hibernate.jdbc.batch_versioned_data=true`, но оно работает только если DB его поддерживает (т.е. DB возврает количество обновленных строк в `update * ...`). Если DB не поддерживает будет **warning**, но не error (т.е. работать не будет, но и ошибки не будет).  
    **Note.** Для delete **нету order** перед batch и для чередующихся delete разных Entities не будет batch. Order нету т.к. изменение order для delete в cascade операциях приведет к ошибкам (не тот порядок удаления будет).  
    **Note!** Если использовать **identity identifier generator** то `order_insert` работать не будет, он будет отключен на **JDBC level** уровне!
* [1](https://vladmihalcea.com/improve-statement-caching-efficiency-in-clause-parameter-padding/) По умолчанию на каждый набор параметров **IN** генерируется свой набор параметров в **prepared statement** и на каждый такой запрос будет свой **execution plan**. Эффективность **IN** можно улучшить параметром `hibernate.query.in_clause_parameter_padding=true` этот параметр группирует **IN** с похожим количеством параметров (по количеству параметров степени 2) и для каждая группа использует один и тот же **query plan**.
* **L2 кэш** - то что хранится не в памяти (стороннее хранилище, e.g. диск, памяти, любое что-то что предоставляет Cache provider L2 кэша). L2 cache релизации поддерживают разные стратегии. Transactional кэш мало кто поддерживает, т.к. это тяжело, в этом случае кэш сохраняется даже если транзакция отвалилась.
* **Query cache** - хранит map где ключ формируется из id записей. Если записи есть в L2, то они будут взяты по id. Hibernate ORM следит была ли Entity **updated**, и если да, то делает **invalidated** Query cache и читает из DB. Или если id это immutable natural key (т.е. неизменяемый ключ), то данные точно в кэше т.к. update табл не сломает кэш.
* Использовать:
  * **read only**
  * подходящий **transactional isolation level**
  * ставить `@Immutable`, если Entity не меняется (как **read only** и отключение **dirty checking**)
  * `SatetlessSession` - можно использовать для некоторых операций, но осторожно, т.к. она отключает много вещей. Нету: L1 кэша, dirty checking. Есть: mapping on Entities, criteria api (т.е. остается только mapping который можно использовать для генерации запросов).
* Не обязательно использовать везде Entities. Можно напрямую мапить результат запроса в dto через `ResultTransformer` и `Transformer`. Или можно мапить результат запроса на `Map` (например когда динамическое количество полей).
    ```java
    (ClientDto) session
        .createQuery("select new ClientDto(c.id, c.name, sum(a.amount)) from Client c inner join c.accounts c where c.id=:id")
        .setParameter("id", 1)
        .uniqueResult();

    // альтернатива "new ClientDto(...)" это спец. метод setResultTransformer()
    session.createQuery("select c.id, c.name, sum(a.amount), from Client c inner join c.accounts c where c.id=:id")
        .setResultTransformer(Transformers.aliasToBean(ClientDto))
    ```
* `@DynamicInsert` и `@DynamicUpdate` иногда полезно, т.к. не обновляет все поля, а только часть. Например если одно из ненужных полей это большой **Blob file**, который ради обновления одного поля **String name** отправился вместе с запросом.
* **Id generators.** Не рекомендуется использовать `GenerationType.AUTO` т.к. с некоторыми DB выбирает непредсказуемую стратегию генерации id, может выбрать неэффективную стратегию **TABLE** (ее недостатки: лишние locks, select for update etc)
  * **Note.** `GenerationType.SEQUENCE` по умолчанию **может** (уточнить) каждый раз читать из DB новый id, это медленно. Можно включить начитку сразу наборов значений, чтобы Hibernate не лез в базу каждый раз через `hibernate.id.new_generator_mappings=true`
  * **Note.** Хороший способ - генерировать id самим на базе `timestamp+имя_компа+hash(прошлые_id)`. Такой подход используют twitter, instagram etc
* Использовать **native query**, когда это будет быстрее (`order by null` или `delete with join` в mysql). Но **native query** плохо работают с L2 кэшем, в этом случае нужно чистить регионы кэша по Entity, а query cache весь либо вызывать проверку на то что была ли табл. updated чтобы он обновил кэш (для этого есть инструменты)
* Не использовать **List**, **Array** и **OrderBy** в hibernate (т.е. в параметрах связей Entities) - видимо это замедляет
* Можно использовать **CQRS**
* **Когда использовать или нет ORM**
  * model простая, нагрузка маленькая - опционально
  * model простая, нагрузка высокая - не использовать ORM
  * model сложная, нагрузка маленькая - использовать ORM
  * model сложная, нагрузка высокая - использовать, но делать затратный тюнинг
* update обычно делают **точечно**, не использовать update через изменения всей Entity
* Нужно поставить `hibernate.connection.provider_disables_autocommit=true`когда `auto commit` уже отключен в **connection pool**, чтобы отключить дополнительную проверку на `auto commit` в самом Hibernate ORM
  * (т.к. иначе он проверяет каждый Connection на auto commit чтобы поменять его значение, чтобы контролировать что каждый unit-of-work JDBC Statements выполняется в рамках одной транзакции)

## Логирование в Hibernate ORM, Spring Data JPA, JDBC
Источник: [1](https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#configurations-logging), [2](https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#best-practices-logging), [3](https://docs.jboss.org/hibernate/orm/current/topical/html_single/logging/Logging.html), [4](https://thorben-janssen.com/hibernate-logging-guide/)

Тут будет отдельно про логирование. Ссылки на доп. источники выше.


```properties
# log4j
logging.level.org.hibernate=INFO, hb
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type=TRACE
logging.level.org.hibernate.hql.ast.AST=info
logging.level.org.hibernate.tool.hbm2ddl=warn
logging.level.org.hibernate.hql=debug
logging.level.org.hibernate.cache=info
logging.level.org.hibernate.jdbc=info
logging.level.org.hibernate.pretty=debug

log4j.appender.hb=org.apache.log4j.ConsoleAppender
log4j.appender.hb.layout=org.apache.log4j.PatternLayout
log4j.appender.hb.layout.ConversionPattern=HibernateLog --> %d{HH:mm:ss} %-5p %c - %m%n
log4j.appender.hb.Threshold=TRACE

# показывает ВСЕ логи, можно посмотреть что выведет и потом настроить в логгере вывод только нужного
log4j.logger.org.hibernate=DEBUG

# hibernate
spring.jpa.properties.hibernate.show_sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.use_sql_comments=true

# в spring data jpa
spring.jpa.properties.hibernate.show_sql=true
spring.jpa.properties.hibernate.format_sql=true
spring.jpa.properties.hibernate.generate_statistics=true
spring.jpa.properties.hibernate.use_sql_comments=true
spring.jpa.properties.hibernate.type=trace

# в spring настройка log через Dlogging (не проверено)
logging.level.org.hibernate.SQL=DEBUG
logging.level.org.hibernate.type=TRACE
logging.level.org.hibernate.type.descriptor.sql.BasicBinder=TRACE

# Для criteria api можно установить чтобы вклеивать параметры в сам запрос (чтобы в логах было видно, но возможно не обязательно) 
hibernate.criteria.literal_handling_mode=INLINE

# логирование JdbcTemplate Queries
logging.level.org.springframework.jdbc.core.JdbcTemplate=DEBUG  # logging statements
logging.level.org.springframework.jdbc.core.StatementCreatorUtils=TRACE # log parameters of prepared statements

# лог транзакций и isolation level в spring 
log4j.logger.org.springframework.transaction.support.AbstractPlatformTransactionManager=debug
log4j.logger.org.springframework.orm.hibernate5.HibernateTransactionManager=debug
log4j.logger.org.springframework.orm.jpa.JpaTransactionManager=debug
log4j.logger.org.springframework.jdbc.datasource.DataSourceTransactionManager=debug

# логирование transaction и Session и Connection
org.hibernate.resource.transaction=debug
org.hibernate.resource.jdbc=debug
org.hibernate.internal.SessionImpl=debug
org.hibernate.internal.SessionFactoryImpl=debug
org.hibernate.transaction.JDBCTransaction=debug
org.hibernate.jdbc.ConnectionManager=debug
```

# Hibernate Pagination
https://www.baeldung.com/hibernate-pagination

# Способы создания query в одном разделе, могут спросить на собеседовании
пока пусто
- EntityManager
- createQuery
- createQuery + DTO projection
- createNativeQuery
- @NamedQuery
- @NamedNativeQuery
- get
- полиморфный запрос
- load
- Criteria API
  - createQuery
  - createTupleQuery
- Criteria API + EntityGraph
- Spring Data JPA
  - getOne
  - findById
  - findAll
  - findAll(Specification)
  - findAll(Specification + join)
  - findAll(Specification + subselect)

# JPA vs Hibernate
https://stackoverflow.com/a/26825931

# Second-Level Cache
## common, Cache Concurrency Strategy
Кэш особенно эффективен для Entities с большим количеством связных сущностей.

**Cache Concurrency Strategy**
* `READ_ONLY` - для данных которые не меняются, изменение вызовет exception, работает быстро. Рекомендуется при редких операциях чтения
* `NONSTRICT_READ_WRITE` - не гарантирует **strong consistency**, работает как **eventual consistency**, т.е. есть временное окно пока данные в кэше будут актуальными. Рекомендуется если **update** данных редкий
* `READ_WRITE` - гарантирует **strong consistency**, достигается путем lock, который отпускается когда transaction уже commited. Все параллельные транзакции которые обращаются к entities на которых **в данный момент** lock загружают эти данные из DB. Нельзя использовать если нужен serializable isolation level, т.к. эта стратегия кэширования его не поддерживает (т.е. могут случаться phantom reads). Рекомендуется если данные могут быть прочитаны или изменены
* `TRANSACTIONAL` - для distributed XA transactions **Note.** уточнить как оно работает, по идеи это самый строгий и медленная стратегия. Работает только с JTA транзакциями.

**Note.** В блоге vlad mihalcen есть инфа которую сюда нужно перенести, [тут](https://vladmihalcea.com/howoes-hibernate-read_write-cacheconcurrencystrategy-work/)

**Entities хранятся в disassembled (hydrated) состоянии:**
* Id (primary key) is not stored (it is stored as part of the cache key)
* Transient properties are not stored
* Collections are not stored (see below for more details)
* Non-association property values are stored in their original form
* Only id (foreign key) is stored for ToOne associations

## Про сброс Second-Level Cache
При использовании HQL кэш сбрасывается только для Entity к которой обращается запрос. При native query кэш сбрасывается целиком (т.к. hibernate не может определить что именно изменилось в DB и на всякий случай сбрасывает весь кэш). Чтобы сброса кэша не произошло нужно указать Hibernate что выкинуть из кэша, остальное не указанное он не тронет.
```java
// кэш для Foo сбросится сам
entityManager.createQuery("update Foo set … where …").executeUpdate();

// сбросится весь кэш
session.createNativeQuery("update FOO set … where …").executeUpdate();

// указываем какой кэш сбросить чтобы остальной остался нетронутым
Query nativeQuery = entityManager.createNativeQuery("update FOO set ... where ...");
nativeQuery.unwrap(org.hibernate.SQLQuery.class).addSynchronizedEntityClass(Foo.class);
nativeQuery.executeUpdate();
```

## Пошаговая настройка Second-Level Cache
1. Включаем кэш и указываем его реализацию
    ```properties
    hibernate.cache.use_second_level_cache=true
    hibernate.cache.region.factory_class=org.hibernate.cache.ehcache.EhCacheRegionFactory
    ```

2. Указываем класс для L2 кэша
    ```java
    @Entity
    @Cacheable // не обязательно в Hibernate
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE) // включаем кэш и определяем стратегию
    public class Foo {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "ID")
        private long id;
    
        @Column(name = "NAME")
        private String name;
    }
    ```

3. Проверяем состояние кэша, используя `CacheManager` для доступа к кэшу
    ```java
    Foo foo = new Foo();
    fooService.create(foo);
    fooService.findOne(foo.getId());
    int size = CacheManager.ALL_CACHE_MANAGERS.get(0).getCache("com.baeldung.hibernate.cache.model.Foo").getSize();
    ```

4. Настраиваем сам **ehcache** кэш по себе (не настройки hibernate orm)
    ```xml
    <ehcache>
        <cache name="com.baeldung.persistence.model.Foo" maxElementsInMemory="1000" />
    </ehcache>
    ```

5. **Collection Cache** (кэш List, Set и прочих полей collection в Entity) не работает по умолчанию, его нужно включать в классе отдельно. И хранятся они в отдельных регионах кэша. Имя региона это FQN `(пакет + класс) + имя свойства` - это дает гибкость в настраиваимости параметров кэширования для каждого случая (e.g. eviction/expiration policy). **Причем** только ids сущностей в кэшируемой collection кэшуруются, что означает что помечать коллекцию из Entity как кэшируемую (т.е. видимо имеется ввиду, что хранить id дешево и поэтому ничего плохого не будет если закэшировать связанные коллекции?)
    ```java
    @Entity
    @Cacheable
    @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    public class Foo {
        @Cacheable
        @org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE) // указываем отдельно
        @OneToMany
        private Collection<Bar> bars;
    }
    ```

## Query Cache

**Использование:**
1. включение
    ```properties
    hibernate.cache.use_query_cache=true
    ```
2. Для каждого запроса нужно включить query кэш
    ```java
    entityManager.createQuery("select f from Foo f")
    .setHint("org.hibernate.cacheable", true)
    .getResultList();
    ```

**Query Cache Best Practices** (все относится к Query Cache)
* для коллекций внутри Entities кэшируются только ids, поэтому очень рекомендуется включать L2 кэш для таких коллекций
* каждый entry результата связан с одним набором параметров query (как сигнатура метода), поэтому кэшировать запросы где с большим набором комбинаций параметров не рекомендуется
* entities которые часто меняются не рекомендуется кэшировать, т.к. кэш будет invalidate как только поменяется даже просто что-то связанное с ними, при этом не важно что поменяется только часть данных, invalidate будет весь кэш
* по умолчанию **query cache** результаты хранятся в `org.hibernate.cache.internal.StandardQueryCache`
* для всех query cache результатов в org.hibernate.cache.spi.UpdateTimestampsCache хранятся last update timestamps (время последнего обновления кэша). Пока идет работа с query cache этот конкретный кэш с timestamps не должен быть evicted/expired. Рекомендуется отключить **automatic eviction and expiration**, т.к. это не потребляет много памяти. **Note.** этот пункт выглядит слегка странным, возможно я что-то не допонял

## How to work L2 cache
Источник: [1](https://vladmihalcea.com/how-does-hibernate-store-second-level-cache-entries/)

## Query Cache N+1 issue
Источник: [1](https://vladmihalcea.com/hibernate-query-cache-n-plus-1-issue/)

## Cache non-existing entity fetch results with JPA and Hibernate
Source: [1](https://vladmihalcea.com/jpa-hibernate-cache-non-existing-entity-fetch-results/)
# JPA and Hibernate query hints
Это не те же **hints**, которые в DB. Это опции **Java Persistence provider**. Метод тут: `javax.persistence.Query#setHint`. **E.g.** например устанавливает опции `PreparedStatement.setQueryTimeout`

```java
// Устанавливаем timeout
List<Post> posts = entityManager.createQuery(
    "select p " +
    "from Post p " +
    "where lower(p.title) like lower(:titlePattern)", Post.class)
.setParameter("titlePattern", "%Hibernate%")
.setHint("javax.persistence.query.timeout", 50)
.getResultList();

// Устанавливаем EntityGraph
PostComment comment = entityManager.find(
    PostComment.class,
    1L,
    Collections.singletonMap("javax.persistence.fetchgraph", entityManager.getEntityGraph("PostComment.post"))
);
```

В Hibernate в отличии от JPA устанавливается через класс `QueryHints`.

| column0                             | column1               | column2                                                                                               |
| ----------------------------------- | --------------------- | ----------------------------------------------------------------------------------------------------- |
| Query hint name                     | QueryHints constant   | Description                                                                                           |
| org.hibernate.cacheMode             | CACHE_MODE            | Equivalent to setCacheMode method of org.hibernate.query.Query                                        |
| org.hibernate.cacheRegion           | CACHE_REGION          | Equivalent to setCacheRegion method of org.hibernate.query.Query                                      |
| org.hibernate.cacheable             | CACHEABLE             | Equivalent to setCacheable method of org.hibernate.query.Query                                        |
| org.hibernate.callable              | CALLABLE              | Useful for named queries that need to be executed using a JDBC CallableStatement                      |
| org.hibernate.comment               | COMMENT               | Equivalent to setComment method of org.hibernate.query.Query                                          |
| org.hibernate.fetchSize             | FETCH_SIZE            | Equivalent to setFetchSizemethod of org.hibernate.query.Query                                         |
| org.hibernate.flushMode             | FLUSH_MODE            | Equivalent to setFlushMode method of org.hibernate.query.Query                                        |
| hibernate.query.followOnLocking     | FOLLOW_ON_LOCKING     | Override the useFollowOnLocking method of org.hibernate.dialect.Dialect                               |
| org.hibernate.lockMode              | NATIVE_LOCKMODE       | Specify a custom javax.persistence.LockModeType or org.hibernate.LockMode for the current query       |
| hibernate.query.passDistinctThrough | PASS_DISTINCT_THROUGH | Prevent the JPQL or Criteria API DISTINCT keyword from being passed to the SQL query                  |
| org.hibernate.readOnly              | READ_ONLY             | Equivalent to setReadOnly of org.hibernate.query.Query                                                |
| org.hibernate.timeout               | TIMEOUT_HIBERNATE     | Equivalent to setTimeout of org.hibernate.query.Query. The timout value is specified in seconds.      |
| javax.persistence.query.timeout     | TIMEOUT_JPA           | Equivalent to setTimeout of org.hibernate.query.Query. The timout value is specified in milliseconds. |

```java
// Пример для Hibernate ORM
List<Post> posts = entityManager.createQuery(
    "select p " +
    "from Post p", Post.class)
.setHint(QueryHints.READ_ONLY, true)
.getResultList();
```

# SQL query plan through Hibernate ORM hints
Source: [1](https://vladmihalcea.com/execution-plan-oracle-hibernate-query-hints/)

Проброс **query plan** через Hibernate ORM. На примере **Oracle DB**

1. **Вариант 1** - через Query Hint (комментарий после слова `select`)
    ```java
    List<Long> postIds = entityManager
    .createNativeQuery(
        "SELECT p.id " +
        "FROM post p " +
        "WHERE EXISTS ( " +
        "     SELECT 1 " +
        "     FROM post_comment pc " +
        "     WHERE " +
        "          pc.post_id = p.id AND " +
        "          pc.review = 'Bingo' " +
        ") " +
        "ORDER BY p.title ")
    .setFirstResult(pageStart)
    .setMaxResults(pageSize)
    .addQueryHint("GATHER_PLAN_STATISTICS")
    .addQueryHint("POST_WITH_BINGO_COMMENTS")
    .getResultList();
    ```
2. **Вариант 2** - через комментарии SQL (над запросом до слова `select`). Включаем комментарии в SQL т.к. в некоторых DBs через коммент задается план запроса
    ```yml
    hibernate.use_sql_comments=true
    ```
3. Запрос.
    ```java
    List<Long> summaries = entityManager.createNativeQuery(
        "SELECT p.id " +
        "FROM post p " +
        "WHERE EXISTS ( " +
        "     SELECT 1 " +
        "     FROM post_comment pc " +
        "     WHERE " +
        "          pc.post_id = p.id AND " +
        "          pc.review = 'Bingo' " +
        ") " +
        "ORDER BY p.title ")
    .setFirstResult(pageStart)
    .setMaxResults(pageSize)
    .unwrap(org.hibernate.query.Query.class)
    .addQueryHint("GATHER_PLAN_STATISTICS")
    .setComment("POST_WITH_BINGO_COMMENTS")
    .getResultList();
    ```
4. Результат
    ```sql
    /* POST_WITH_BINGO_COMMENTS */
    SELECT /*+ GATHER_PLAN_STATISTICS */ *
    FROM
    (SELECT row_.*, rownum rownum_
    FROM
        (SELECT p.id
        FROM post p
        WHERE EXISTS
            (SELECT 1
            FROM post_comment pc
            WHERE pc.post_id = p.id
                AND pc.review = 'Bingo' )
        ORDER BY p.title) row_
    WHERE rownum <= 30)
    WHERE rownum_ > 20
    ```

# Примитивный тип vs Обертки примитивных типов в качестве id для Entity
Статьи еще нет.

**Note.** Скорее всего лучше использовать обертки т.к. примитивный тип не может быть null, а может быть только значениями для типов в java по умолчанию и нельзя определить null поле в БД или нет. При этом пока значения из DB не загружены значение полей может быть не ининициализированно и если для null это будет понятно, то для примитивных типов с значениями по умолчанию - можно перепутать их с настоящими значениями (хотя попасть в такую ситуация и будет трудно).

# Enum и `@Enumerated`

Чтобы сохранить Enum в нужном строки в DB нужно отметить его `@Enumerated`.

**Note.** `@Enumerated(STRING)` сохраняет **upper case**, чтобы сделать **lower case** нужно писать свой конвертер.

```java
@Entity
public class User {
    @Convert(converter = StatusConverter.class) // чтобы сделать lower case нужно писать свой конвертер.
    @Enumerated(STRING) // сохраняет Enum в виде String, а не Integer. Только в upper case!
    private Status status;
}
```

# `@Transient`
Поля `static, transient, final` - тоже что и `@Transient`
```java
class User {
    @Transient String name; //не сохранять поле
    static String name2; //тоже что и @Transient
    transient String name3; //тоже что и @Transient
    final String name4; //тоже что и @Transient
}
```

# `@Lob`
`java.sql.Clob` или `java.sql.Blob`, то оно преобразуется в `@Lob`
```java
class User {
    @Lob String longDescription; //large object, большие строки
    @Lob byte [] b; //большой байтовый массив
}
```

# `@NotFound`

Стратегия поведения если связанной Entity не существует. При `NotFoundAction.IGNORE` вернет **null** вместо выброса Exception.

```java
@NotFound(action=NotFoundAction.IGNORE) // null если Product не существует вместо ошибки
private Product p;
```

# `getMetamodel()` и получение имен таблиц, entity etc
```java
@Autowired private EntityManager entityManager;

MetamodelImpl metamodel = (MetamodelImpl) entityManager.getMetamodel(); // получаем все мета данные

metamodel.getManagedTypes().forEach(type -> {
    if (type instanceof EntityTypeImpl) { // если это Entity, а не Embeded class или другое
        EntityTypeImpl entityTypeImpl = (EntityTypeImpl) type;
        String name = type.getName(); // получаем имя Entity
        Class<?> clazz = type.getJavaType(); // получаем class Entity
        String tableName = getTableName(clazz); // получаем имя табл.
        entityManager.getTransaction().begin();
        entityManager.createNativeQuery("truncate table " + tableName + " RESTART IDENTITY CASCADE").executeUpdate();
        entityManager.getTransaction().commit();
    }
});

// получаем имя таблицы по классу Entity, через SessionFactory
public String getTableName(Class entityClass){
    SessionFactory sessionFactory = ((Session) entityManager.getDelegate()).getSessionFactory();
    ClassMetadata classMetadata = sessionFactory.getClassMetadata(entityClass);
    SessionFactoryImpl sessionFactoryImpl = (SessionFactoryImpl) sessionFactory;
    AbstractEntityPersister entityPersister
            = (AbstractEntityPersister) sessionFactoryImpl.getEntityPersister(classMetadata.getEntityName());
    return entityPersister.getTableName();
}
```

# Сохранить связные `Entity` можно только если они были до этого `persisted` или используя `CascadeType=PERSIST`
В hibernate orm например при `user.setRole(role)` нужно чтобы role или была сохранена до этого set или начитана, т.е. чтобы она была в кэше. Иначе при дальнейшей работе с user будет transient excpetion когда entity пытаются установить связь с entity которой нет в кэше и она transient

```java
// 1.
userRepository.save(new User().setRole(new Role())); // ошибка, если для поля role не установлен CascadeType

// 2.
Role role = roleRepository.save(new Role());
userRepository.save(new User().setRole(role)); // правильно, объект с которым связываем должен быть в кэше L1

// 3. правильно, если связь user <- role отмечена cascade=CascadeType.PERSIST
userRepository.save(new User().setRole(new Role()));
```

# При начитке id связанной Entity не будет избыточной начитки целой Entity
Источник: [1](https://stackoverflow.com/a/54898454)

Если в hibernate сделать `user.role.getid()` то он загрузит не всю entity а только `id` (т.к. в DB у `user` есть поле `roleId`).  
Тоже касается запросов через Criteria API в которых используется `id`.

```java
// e.g. в Criteria API отдельного запроса lazy для id к связной сущности не будет, поле role.id берется как из текущей табл.
Specification<T> specification = (root, criteriaQuery, criteriaBuilder) -> criteriaBuilder.equal(root.get("role").get("id"), id);
repository.findAll(specification); // выполнит 1 запрос select
```

# Интеграция Hibernate Validator может обрабатывать Hibernate ORM (e.g. `@NotNull`)

Источник: [1](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#validator-checkconstraints-orm)

**Hibernate ORM** умеет использовать некоторые аннотации **Hibernate Validator**. Эти аннотации используются и для DDL (авто генерации таблиц как и e.g. `@Column`), поведение управляется параметром `hibernate.validator.apply_to_ddl` в `application.yml`.

**Note!** В документации **Hibernate Validator** написано, что для **lazy** полей (связей) аннотации нужно ставить над **getters**, если их поставить над **fields**, то будет ошибка. Как именно это работает с lombok нужно исследовать.

**Можно:** 1. проверять валидность, 2. использовать в DDL для авто генерации табл

```properties
spring.jpa.properties.hibernate.validator.apply_to_ddl=false    # включаем для @NotNull (Bean Validation) авто генерацию табл по аннотациям
spring.jpa.properties.hibernate.check_nullability=true          # включаем для @Column(nullable = false) не только генерацию DDL, но и проверку на null
```

```java
@Entity
public class Item {
    @NotNull // в основном проверка (лучше), т.к. сработает точно до любых запросов в DB
    private BigDecimal price1;
    
    @Column(nullable = false) // в основном генерация DDL (хуже), отдельной настройкой можно включить еще и проверку на null до запроса в DB, хотя скорее всего в Hibernate ORM проверка на null и так сработает
    private BigDecimal price2;
}
```

# Join в hibernate нельзя делать, если нету связей `@ManyToOne` etc

В Hibernate можно делать join только если на уровне entity проставлены связи `@ManyToOne` и прочие (не обязательно на уровне БД), если связей нету можно использовать подзапрос, но он медленнее. (это в отличии от БД где join можно делать даже если связей нету)

В обычном SQl можно делать join даже если связей между табл. нету.

# Замапить одну Entity на несколько табл можно, если не мапить часть полей то ошибки не будет
Замапить одну Entity на несколько табл можно, если не мапить часть полей то ошибки не будет. Это можно испольовать чтобы начитать только часть полей через get/load/findAll. Хотя хорошая практика читать их отдельными запросами с Criteria API (но только не через Specification) или через EntityGraph (для этого он и нужен).

# Soft delete, `@SQLDelete`, `@Where`, `@Loader`
Источник: [1](https://vladmihalcea.com/the-best-way-to-soft-delete-with-hibernate/)

Чтобы сделать **Soft delete** нужно над Entity объявить замену стандартным SQL запросам **CRUD**. Ставить `@Where` над полем имеет смысл только для **unidirectional** связей, для **bidirectional** не нужно (в данном случае **bidirectional** связь эффективнее чем **unidirectional**)  
**Note.** До **Hibernate 5.2** если есть `@OneToMany` нужно было над ней отдельно ставить `@Where`.
```java
@MappedSuperclass
public abstract class BaseEntity {
    boolean deleted; // храним столбец с состоянием
}

@Entity
@SQLDelete(sql = "UPDATE tag SET deleted = true WHERE id = ?") // кастомизация DELETE, замена на UPDATE
@Loader(namedQuery = "findTagById") // кастомизация SELECT по id
@NamedQuery(name = "findTagById", query =
    "SELECT t " +
    "FROM Tag t " +
    "WHERE " +
    "    t.id = ?1 AND " +
    "    t.deleted = false")
@Where(clause = "deleted = false") // кастомизация SELECT
public class Tag extends BaseEntity {
    @Id
    String id;
}
```

# Batch операция в JPA
Источник: [тут](https://vladmihalcea.com/the-best-way-too-batch-processing-with-jpa-and-hibernate/)

**Плюсы batch:**
1. Каждый набор данных можно обработать в отдельном thread
2. Каждый набор данных может делаться в отдельной транзакции, так что не нужно откатывать весь набор данных при ошибке **Note.** Длинные транзакции (long-running transactions) вредны для MVCC баз данных
3. Мы делаем **flush** после каждого batch (куска batch), чтобы не нагружать память

**Note.** В **Spring Data JPA** методах batch операция над коллекцией объектов выполняется **в одной транзакции** (по крайней мере по умолчанию). При этом параметр `batch_size` может изменить поведение.

**Когда это нужно:** если например каждая новая batch операция зависит от результата прошлой batch операции, т.е. если есть какая-то логика по которой выполняется batch.

```java
int entityCount = 50;
int batchSize = 25;
 
EntityManager entityManager = entityManagerFactory()
    .createEntityManager();
     
EntityTransaction entityTransaction = entityManager
    .getTransaction();
 
try {
    entityTransaction.begin();
 
    for (int i = 0; i < entityCount; i++) {
        if (i > 0 && i % batchSize == 0) {
            entityTransaction.commit();
            entityTransaction.begin();
 
            entityManager.clear(); // чистим память, чтобы не переполнилась (out of memory exception)
        }
 
        Post post = new Post(
            String.format("Post %d", i + 1)
        );
         
        entityManager.persist(post);
    }
 
    entityTransaction.commit();
} catch (RuntimeException e) { // чтобы не вызвать прервать программу при ошибке
    if (entityTransaction.isActive()) {
        entityTransaction.rollback(); // откатываем при ошибке
    }
    throw e;
} finally {
    entityManager.close(); // закрываем чтобы почистить ресурсы
}
```

# Connection Pool
<small>Источник: **vladmihalcea** ([1](https://vladmihalcea.com/the-anatomy-of-connection-pooling/), [2](https://vladmihalcea.com/connection-pool-sizing-with-flexy-pool/))</small>

**Connection Pool** - массив `Connection`s, которые переиспользуются вместо открытия новых, `connection.close()` возвращает Connection в **pool** вместо закрытия.

**Путь запроса без Connection Pool (connection life cycle):** application > `java.sql.DataSource` > `java.sql.Driver` > `java.sql.Connection` > `java.net.SocketFactory` > `java.net.Socket`

**Путь запроса с Connection Pool:** application > `java.sql.DataSource` > **Pool** > `java.sql.Connection`

**Почему это быстро:** уменьшает количество **I/O** операций, и операций создания/удаления **TCP**, и операций java **GC** (т.к. меньше `Object` создается)

**Почему это безопаснее:** Контроль трафика - хуки `wait Connection` (при **max pool size**) и `timeout` не дают приложению взять слишком много **database traffic** и положить **DB** (делая ее недоступной для других applications).

- **Основные параметры настроек Connection Pool:**
  - **min size, max size** - количество соединений. Если **max size** превышен, то ждем (`wait`) пока он не уменьшется и после этого получаем `Connection`
  - **max idle time** - время простоя соединений
  - **acquire timeout** - операция **acquire** берет `Connection` из **Pool**
  - **timeout retry attempts** - количество повторений запросов в случае ошибки

**Примеры пуллов:**  HikariCP - самый быстрый насегодня

**Note.** Для анализа **connection pool** можно использовать [FlexyPool](https://vladmihalcea.com/connection-pool-sizing-with-flexy-pool/) (может работать через логи [SLF4J](http://metrics.dropwizard.io/3.1.0/manual/core/#man-core-reporters-slf4j) или [JMX](http://metrics.dropwizard.io/3.1.0/manual/core/#man-core-reporters-jmx)). Пример экспорта логов в `csv` [тут](https://github.com/vladmihalcea/flexy-pool/wiki/Installation-Guide#customized-metrics)

```java
// Пример создания Connection Pool, это Singleton,
// в некоторых Application Server нужно обьявить в их настройках в jndi что это Singleton
class MyPoolSingleton extends org.apache.tomcat.jdbc.pool.DataSource { // из tomcat
    private static volatile MyPoolSingleton instance; //наверное можно AtomicReference
    private MyPoolSingleton() {
        PoolProperties p = new PoolProperties();
        p.setBla("...blabla..."); //устанавливаем много параметров типа timeout и max size
        this.setPoolProperties(p);
    }
    public static MyPoolSingleton getInstance() { //... реализация синглтона
        return instance;
    }
}

// Использование (не Hibernate ORM, он использует Pool указанный в настройках сам)
Statement stat = (Statement) DataSourceSingleton.getInstance().getConnection().createStatement();
ResultSet res = new Statement().executeQuery("SELECT ...");
while(res.next()) str = res.getString("filePath");
```

# Добавление спец. функций DB в Criteria API (JPA 2)

Источник: [тут](https://vladmihalcea.com/hibernate-sql-function-jpql-criteria-api-query/)

пока пусто

# Отдельный функционал
## specification-arg-resolver

**specification-arg-resolver** это неофициальная библиотека для описания **Specification** через аннотацию и альтернативное API.

```java
@RequestMapping("/customers")
public Object findByName(
        @And({
            @Spec(path="registrationDate", params="registeredBefore", spec=DateBefore.class),
            @Spec(path="lastName", spec=Like.class)}) Specification<Customer> customerSpec) {
    return customerRepo.findAll(customerSpec);
}
```

Подробнее [тут](https://blog.tratif.com/2017/11/23/effective-restful-search-api-in-spring/)

## Hibernate Envers
Источник: [тут](https://vladmihalcea.com/the-best-way-to-implement-an-audit-log-using-hibernate-envers/)

пока пусто

## Hibernate Search
Источник: [тут](https://github.com/hibernate/hibernate-search)

пока пусто

## Blaze Persistence
Источник: [1](https://vladmihalcea.com/blaze-persistence-jpa-criteria-queries/), [2](https://persistence.blazebit.com/), [3](https://github.com/Blazebit/blaze-persistence)

Альтернативная удобная реализация JPA Criteria API

