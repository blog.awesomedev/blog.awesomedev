https://habr.com/ru/articles/565000/

интерфейсы
Publisher - subscribe(Subscriber<? super T> s)
Subscriber - onSubscribe(Subscription s), onNext(T t), onError(Throwable t), onComplete()
Subscription - request(long n), cancel()
Processor<T, R> extends Subscriber<T>, Publisher<R>

модули
Reactor Test
Reactor Extra - доп операторы Flux
Reactor Netty - клиент/сервер на основе Netty альтернатива Servler API 3.1+ с back pressure
Reactor Adapter - адаптер RxJava2 и Akka Streams
Reactor Kafka - реактивный API для Kafka

подключение: spring-boot-starter-webflux (содержит reactor) и reactor-test

Reactor Core - Flux (много значений) и Mono (только одно)

примеры:
Flux<String> fluxColors = Flux.just("red", "green", "blue");
fluxColors.subscribe(System.out::println);
fluxColors.log().subscribe(System.out::println); - log регестрирует 
fluxColors.map(color -> color.charAt(0)).subscribe(System.out::println); - Преобразование
Flux.zip(fluxFruits, fluxColors, fluxAmounts).subscribe(System.out::println); - объединяет источников
fluxCalc.subscribe(value -> System.out.println("Next: " + value), error -> System.err.println("Error: " + error)); - Обработка ошибок
Flux.just(-1, 0, 1).map(i -> ...).onErrorReturn(ArithmeticException.class, "error"); - fallback
StepVerifier.create(fluxCalc).expectNextCount(1).expectError(ArithmeticException.class).verify(); - test

// custom Scheduler
установка:
publishOn - влияет на выполнение всех последующих операторов (если не указано иное)
subscribeOn - изменяет поток, из которого подписывается вся цепочка операторов, на основе самого раннего вызова subscribeOn в цепочке
типы:
parallel() - Фиксированный пул воркеров, настроенный для параллельной работы, создавая столько воркеров, сколько ядер ЦП.
single() - использует один и тот же поток для всех вызывающих, если нужно для каждого то Schedulers.newSingle ()
boundedElastic() - Динамически создает ограниченное количество потоков, переиспользует потоки, хороший выбор для обертывания синхронных вызовов
immediate() - немедленно запускается в исполняемом потоке, не переключая контекст выполнения
fromExecutorService(ExecutorService) - может использоваться для создания Планировщика из любого существующего ExecutorService
пример:
Scheduler schedulerA = Schedulers.newParallel("Scheduler A"); // пример создания и установки
Flux.just(1).map(i -> ...).subscribeOn(schedulerA).map(i -> ...).publishOn(schedulerA).blockLast();

// backpressure - consumer сообщает сколько ему сообщений отправить в след раз
Flux.range(1,5).subscribe(new Subscriber<Integer>() {
	Subscription s; int counter;
	@Override // ставим backpressure by default, s.cancel(); - остановить эмиссию и очистить ресурсы
	public void onSubscribe(Subscription s) { this.s = s; s.request(2); }
	@Override // вычисляем и ставим динамически
	public void onNext(Integer i) { ... s.request(2); }
	@Override public void onError(Throwable t) { ... }
	@Override public void onComplete() { ... }
});

// Publisher - cold и hot
cold - ничего не происходит, пока не подпишемся subscribe
hot - не зависит от подписчиков, сразу начинает отправлять, Schedulers.parallel() - планировщик по умолчанию для hot

создание hot Publisher, publish() - создать, connect() - старт:
ConnectableFlux<Long> intervalCF = Flux.interval(Duration.ofSeconds(1)).publish();
intervalCF.connect(); // start
intervalCF.subscribe(i -> System.out.println(String.format("Subscriber A, value: %d", i)));

// вызов блокирующих операций вроде rest, boundedElastic() - переиспользует отдельный поток из пулла
Mono blockingWrapper = Mono.fromCallable(() -> { return /* блокирующая операция */ });
blockingWrapper = blockingWrapper.subscribeOn(Schedulers.boundedElastic());

// Context - аналог ThreadLocal
Mono.just("anything").flatMap(s -> Mono.subscriberContext().map(ctx -> "Value stored in context: " + ctx.get(key)))
	.subscriberContext(ctx -> ctx.put(key, "myValue"));
	
Sink - api программного создания событий https://projectreactor.io/docs/core/release/reference/#producing

// режим отладки для debug
ReactorDebugAgent.init();
SpringApplication.run(Application.class, args);

just, fromArray, fromIterable, fromStream  - Создание новой последовательности
map, flatMap, startWith, concatWith - Преобразование существующей последовательности
doOnNext, doOnComplete, doOnError, doOnCancel - Заглядывать в последовательность
filter, ignoreElements, distinct, elementAt, takeLast - Фильтрация последовательности
onErrorReturn, onErrorResume, retry  - Обработка ошибок
elapsed, interval, timestamp, timeout  - Работаем со временем
buffer, groupBy, window  - Расщепление потока
block, blockFirst, blockLast, toIterable, toStream - Возвращаясь к синхронному миру
publish, cache, replay - Многоадресная рассылка потока нескольким подписчикам
---
Spring WebFlux - основан Reactor, работает в Netty (be default), Undertow, и Servlet 3.1+ Tomcat и Jetty
spring-boot-starter-webflux содержит spring-boot-starter-netty

@EnableWebFlux - включить

1. аннотации - декларативные end points
@PreAuthorize("hasRole('ADMIN')")
@GetMapping
public Flux<Student> listStudents(@RequestParam(name = "name", required = false) String name) {
	return studentService.findStudentsByName(name);
}

@PostMapping
public Mono<Student> addNewStudent(@RequestBody Student student) {
	return studentService.addNewStudent(student);
}

2. функциональные - programatic end points

@Configuration // 1. регистрируем маршруты
public class StudentRouter {
    @Bean
    public RouterFunction<ServerResponse> route(StudentHandler studentHandler){
        return RouterFunctions
            .route(
                GET("/students/{id:[0-9]+}")
                    .and(accept(APPLICATION_JSON)), studentHandler::getStudent)
            .andRoute(
                GET("/students")
                    .and(accept(APPLICATION_JSON)), studentHandler::listStudents);
    }
}

@Component // 2. возвращаем Mono из базы
public class StudentHandler {
    public Mono<ServerResponse> getStudent(ServerRequest serverRequest) {
        Mono<Student> studentMono = studentService.findStudentById(
                Long.parseLong(serverRequest.pathVariable("id")));
        return studentMono.flatMap(student -> ServerResponse.ok()
                .body(fromValue(student)))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> updateStudent(ServerRequest serverRequest) {
        final long studentId = Long.parseLong(serverRequest.pathVariable("id"));
        Mono<Student> studentMono = serverRequest.bodyToMono(Student.class);

        return studentMono.flatMap(student ->
                ServerResponse.status(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(studentService.updateStudent(studentId, student), Student.class));
    }
}

// 3. security имеет спец reactive фильтры и источник данных
@EnableWebFluxSecurity
@EnableReactiveMethodSecurity
public class SecurityConfig {

    @Bean
    public MapReactiveUserDetailsService userDetailsService() {

        UserDetails user = User
                .withUsername("user")
                .password(passwordEncoder().encode("userpwd"))
                .roles("USER")
                .build();

        UserDetails admin = User
                .withUsername("admin")
                .password(passwordEncoder().encode("adminpwd"))
                .roles("ADMIN")
                .build();

        return new MapReactiveUserDetailsService(user, admin);
    }

    @Bean
    public SecurityWebFilterChain securityWebFilterChain(ServerHttpSecurity http) {
        return http.authorizeExchange()
                .pathMatchers("/students/admin")
                .hasAuthority("ROLE_ADMIN")
                .anyExchange()
                .authenticated()
                .and().httpBasic()
                .and().build();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}

// web client
WebClient client = WebClient.create("http://localhost:8080");
Mono<Student> mono = client // 1 запись
	.get()
	.uri("/students/" + id)
	.headers(headers -> headers.setBasicAuth("user", "userpwd"))
	.retrieve()
	.bodyToMono(Student.class);
Flux<Student> flux = client.get() // page
	.uri(uriBuilder -> uriBuilder.path("/students")
	.queryParam("name", name)
	.build())
	.headers(headers -> headers.setBasicAuth("user", "userpwd"))
	.retrieve()
	.bodyToFlux(Student.class);
---
тест reactive controller

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class StudentControllerTest {
    @Autowired WebTestClient webClient;

    @Test
    @WithMockUser(roles = "USER")
    void test_getStudents() {
        webClient.get().uri("/students")
                .header(HttpHeaders.ACCEPT, "application/json")
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBodyList(Student.class);

    }

    @Test
    @WithMockUser(roles = "ADMIN")
    void testAddNewStudent() {
        Student newStudent = new Student();
        newStudent.setName("some name").setAddress("an address");

        webClient.post().uri("/students")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .body(Mono.just(newStudent), Student.class)
                .exchange()
                .expectStatus().isOk()
                .expectHeader().contentType(MediaType.APPLICATION_JSON)
                .expectBody()
                .jsonPath("$.id").isNotEmpty()
                .jsonPath("$.name").isEqualTo(newStudent.getName())
                .jsonPath("$.address").isEqualTo(newStudent.getAddress());
    }
}
---
WEBSOCKETS - для него есть спец методы для поддержки в WebFlux
RSOCKET - бинарный протокол для реактивных запросов https://docs.spring.io/spring-framework/docs/current/reference/html/web-reactive.html#rsocket

class WebSocketHandler { Mono<Void> handle(WebSocketSession session) } // при установке соединения
class WebSocketSession { // для обработки запросов
	Flux<WebSocketMessage> receive()
	Mono<Void> send(Publisher<WebSocketMessage> messages)
}
---
R2DBC (Reactive Relational Database Connectivity) - реактивная версия JDBC, набор interfaces для реализации
Spring Data R2DBC - из spring, легковестный ORM, аналог Spring Data JDBC
	ReactiveTransactionManager - reactive аналог TransactionManager в Spring
r2dbc-pool - реактивный pool

<dependency>
	<groupId>io.r2dbc</groupId>
	<artifactId>r2dbc-postgresql</artifactId>
	<scope>runtime</scope>
</dependency>

// spring boot
spring.r2dbc.url=r2dbc:postgresql://localhost/studentdb
spring.r2dbc.username=user
spring.r2dbc.password=secret

// repository
public interface StudentRepository extends ReactiveCrudRepository<Student, Long> {
    public Flux<Student> findByName(String name);
}

// ручное выполнение
@Bean
public ConnectionFactory connectionFactory() {
	return ConnectionFactories.get(
		ConnectionFactoryOptions.builder()
			.option(DRIVER, "postgresql")
			.option(HOST, "localhost")
			.option(USER, "user")
			.option(PASSWORD, "secret")
			.option(DATABASE, "studentdb")
			.build());
}

 public Flux<Student> findAll() {
        DatabaseClient client = DatabaseClient.create(connectionFactory);
        return client.sql("select * from student")
                .map(row -> new Student(row.get("id", Long.class), row.get("name", String.class), row.get("address", String.class))).all();
 }
 
// R2dbcEntityTemplate - аналог JdbcTemplate
@Autowired R2dbcEntityTemplate template;
public Flux<Student> findAll() { return template.select(Student.class).all(); }
public Mono<Void> delete(Student student) { return template.delete(student).then(); }

