до java 21

вопросы https://javarush.com/groups/posts/3243-razbor-voprosov-i-otvetov-na-sobesedovanii-chastjh-1
jdk 17 vs 21 https://jee.gr/from-java-17-to-java-21-and-how-to-do-it-a-comprehensive-api-comparison/
полезные правила good vs bad SEI CERT Oracle Coding Standard for Java https://wiki.sei.cmu.edu/confluence/display/java/2+Rules

SecurityManager
ZGC+ https://habr.com/ru/articles/269621/
io - актуализировать
nio

все exceptions
---
java -XX:+UseParallelGC -jar Application.java - запуск с VM параметром
---
Deprecations, Removals, and Restrictions

1 encapsulation of JDK internals - user can not use Reflection API to call Java API
2 floating-point operations - strict by default; jdk 17+ strictfp has no effect anymore
3 Ahead-of-Time (AOT) compilation - removed
4 RMI activation - removed
5 Applet API - marked for removal
6 ThreadGroup.allowThreadSuspension - removed
7 ObjectOutputStream.PutField - removed
8 enum.finilize(), Object.runFinilization() - removed
9 Thread start(), stop(), resume(), ThreadDead - removed
10 gc concurent mark sweep - removed
11 Runtime.exec - Deprecated, use ProcessBuilder 
12 Thread.getId() - Deprecated, use getName()
13 ClassSpecializer.Factory, ClassSpecializer.SpeciesData, Compiler, FdLibm.* (Math functions), MLet*, javax.management.remote.* (RMI-related classes) - removed

---
С Java 9 для некоторых программ при компиляции нужно включить модули:
javac --add-modules java.xml.ws UCPublisher.java
---
java 14+

Packaging Tool (JEP 392) - makes rpm, deb, exe, msi, dmg
1 lib\*.jar - dependency
2 demo.jar - main jar
jpackage --name demo --input lib --main-jar demo.jar --main-class demo.Main
---
jdk 18+, JEP 400 - файлы java в UTF-8 by default
---
new Double(1/0.).isInfinite(); //true
new Double(0/0.).isNaN(); //true

java 19+ исправлены методы Double.toString() and Float.toString()
Double.toString(1e23) // java 18, 9.999999999999999E22
Double.toString(1e23) // java 19+, 1.0E23
--
примитивные типы
	byte	8		-128..127
	short	16		-32768..32767
	int		32
	long	64
с плавающей точкой
	float	32		1.4e-045..3.4e+038
	double	64
символы (тоже целочисленный)
	char	16		положительные значения
логические
	boolean 1bit (true/false не преобразуются в 1/0)
--
оболочки типов
	BigDecimal - состоит из: integer value (precision) и 32 bit число сдвигая запятой (scale)
			т.е. получается float point, его часто используют для хранения денег (в формате 19.2, 19.3 или 19.4 в SQL базах которые памят свое значение на этот тип)
		precision - количество цифр числа (как length для строк)
		scale - количество цифр НЕ УЧИТЫВАЯ ЛИДИРУЮЩИЙ НОЛЬ на которое смещается влево запятая, отрициательное значение сдвигает запятую вправо
		0.1234 // scale = 5, precision = 4
		0.00001 // scale = 5, precision = 1
		toString - вывод меняется в зависимости от scale
			scale == 0	- integer выводится как есть
			scale < 0	- в E-Notation e.g. 5E+1 для 50=5*10
			scale >= 0 и precision - scale -1 >= -6	- вывод десятичного числа: 10000000 scale 1 == "1000000.0"
			во всех других случаях - использует E-notation
		методы:
			abs()
			add(BigDecimal)
			compareTo(BigDecimal)
			divide(BigDecimal)
			plus(BigDecimal)
		 	multiply(BigDecimal)
			pow(int n)
			setScale(int)
			precision()
			remainder(BigDecimal)
			signum()
			doubleValue()
			floatValue()
--
https://vertex-academy.com/tutorials/ru/java-8-uchebnik/

UT - Universal Time the same as GMT  
DST (daylight saving time) - summer time  
UTC vs GMT - In short: always use UTC. Difference being literally less than a second. GMT unofficially to refer to UTC, so difference are not big. In GMT the seconds are stretched as necessary, in UTC a second always has the same length.  
UTC (Coordinated Universal Time) - based on atomic clocks  
GMT (Greenwich Mean Time) - based on astronomy (Earth rotation observation)

Note. Хранить, использовать, передавать время на сервер нужно в java.time.LocalDateTime или java.time.LocalDate. Корректировать время по часовому поясу нужно клиенту сервиса (front end или другому клиенту). Как вариант можно использовать `OffsetDateTime` в **UTC** давая понять клиенту API что время в **UTC** и нужно скорректировать его в локальное (т.е. будет `date+time+z+0` и клиенту не придется гадать что за время к нему пришло).  
На уровне Java DTO objects можно использовать (хранить или обрабатывать в бизнес логике) часовые пояса `OffsetDateTime/ZonedDateTime`, но при сохранении в базу данных их нужно приводить к `LocalDateTime` (т.к. при чтении даты DB скорректирует ее на основе часового пояса сервера DB, а не клиента запроса).  
Получать ее через `LocalDateTime.now(ZoneOffset.UTC)

* **Deprecated** - all new api in `java.time.*` - all new api is **immutable** (thread safe)
  * `java.util.Date`, `java.sql.Timestamp` - modern: `java.time.Instant`
    * moment **in UTC**
  * `none` - modern: `java.time.OffsetDateTime`
    * moment with **offset from UTC**
  * `java.util.GregorianCalendar` - modern: `java.time.ZonedDateTime`
    * moment **with time zone**
  * `none` - modern: `java.time.LocalDateTime`
    * not moment, no offset, **no time zone**
  * Old `java.util.Date` is **mutable** (thread unsafe)
 
* **package**
  * java.time
  * java.time.chrono
  * java.time.format
  * java.time.temporal
  * java.time.zone
* **api**
  * ZoneId
  * `java.util.TimeZone`
  * Clock
  * Instant
  * Duration
  * LocalDate
  * LocalTime
  * OffsetTime
  * LocalDateTime
  * OffsetDateTime
  * ZonedDateTime
  * Period
  * Duration
  * DayOfWeek
  * Month
  * Year
  * YearMonth
  * java.time.Ser
  * `java.time.format.DateTimeFormatter`
  * `java.time.format.DateTimeFormatterBuilder`
  
// System
System.currentTimeMillis(); // milliseconds (difference) from January 1, 1970
System.nanoTime();

//ZoneId
ZoneId zone = ZoneId.of("Europe/Berlin");
ZoneId id = ZoneId.systemDefault();
Clock cl = new SystemClock(ZoneId.systemDefault());

// Clock - computer time api
// SystemClock - Clock impl
Clock cl = Clock.systemUTC(); // UTC
Clock cl = Clock.systemDefaultZone(); // local
Clock cl = SystemClock.UTC; // the same as Clock cl = Clock.systemUTC();
Clock cl = SystemClock.withZone(ZoneId.systemDefault());

LocalDateTime.now(); // local time
LocalDateTime.now(ZoneOffset.UTC); // UTC time

LocalDate –  дата без времени и временных зон;
LocalTime – время без даты и временных зон;
LocalDateTime – дата и время без временных зон;
ZonedDateTime – дата и время с временной зоной;
DateTimeFormatter – форматирует даты в строки и наоборот, только для классов java.time;
Instant – колличество секунд с Unix epoch time (полночь 1 января 1970 UTC);
Duration – продолжительность в секундах и наносекундах;
Period – период времени в годах, месяцах и днях;
TemporalAdjuster – корректировщик дат (к примеру, может получить дату следующего понедельника);
ChronoUnit - MILLENNIA, HALF_DAYS, DECADES, ...

* **LocalDateTime vs OffsetDateTime vs ZonedDateTime**
  * **LocalDateTime**
  * **OffsetDateTime**
  * **ZonedDateTime**

// создание
LocalDate.now(); // 2018-01-20
LocalTime.now(); // 08:07:35.113028
LocalDateTime.now();// 2018-01-20T08:07:35.113120
LocalDate.of(2020, Month.SEPTEMBER, 23); // 2020-09-23
LocalDate.of(2021, 1, 1); // 2021-01-01
LocalDate.ofYearDay(2020, 361); // 2020-12-26
LocalTime.of(23, 59, 59, 700); // 23:59:59.000000700
LocalTime.ofSecondOfDay(9_124); // 02:32:04
LocalTime.ofNanoOfDay(100_000_000_000L); // 00:01:40
LocalDateTime.of(2008, Month.JANUARY, 6, 20, 45, 2, 2); // 2008-01-06T20:45:02.000000002
LocalDateTime.of(2009, 1, 13, 9, 7); // 2009-01-13T09:07
LocalDateTime.of(LocalDate.now(), LocalTime.now()); // 2018-01-20T09:19:48.603054

// изменение
LocalDate now = LocalDate.now(); // 2018-01-21
LocalDate plus2Days = now.plusDays(2); // 2018-01-23
LocalDate plusWeek = now.plusWeeks(1); // 2018-01-28
LocalDate plus3Months = now.plusMonths(3); // 2018-04-21
LocalDate plusPeriod = now.plus(Period.ofDays(3)); // 2018-01-24
LocalDate plusMillennia = now.plus(1, ChronoUnit.MILLENNIA); // 3018-01-21
LocalDate minusDays = now.minusDays(3); // минус аналогично

LocalTime plusNanos = now.plusNanos(100_000); // 08:49:39.678803
LocalTime plusSeconds = now.plusSeconds(20); // 08:49:59.678703
LocalTime plusMinutes = now.plusMinutes(20); // 09:09:39.678703
LocalTime plusHours = now.plusHours(6); // 14:51:58.601216
LocalTime plusMillis = now.plus(Duration.ofMillis(100)); // 08:49:39.778703
LocalTime plusHalfDay = now.plus(1, ChronoUnit.HALF_DAYS); // 20:49:39.678703
LocalTime minusNanos = now.minusNanos(100_000); // минус аналогично

LocalDateTime minusWeeks = now.plusWeeks(3); // LocalDateTime аналогично
LocalDateTime minusDays = now.minusDays(7); // минус аналогично

// сравнение, внутри используется localDateTime.compareTo(...)
boolean after = now.isAfter(LocalDate.of(2017, 9, 23));// true
boolean before = now.isBefore(LocalTime.now().plusHours(2));
boolean after = LocalDateTime.now().isAfter(now.minusMonths(1)); // true
boolean isDateTimeAfter = localDateTime.compareTo(LocalDateTime.now()) // compareTo напрямую

// DateTimeFormatter форматирование
String basicIsoDate = now.format(DateTimeFormatter.BASIC_ISO_DATE); // 20180128
String isoDate = now.format(DateTimeFormatter.ISO_DATE); // 2018-01-28
String isoWeekDate = now.format(DateTimeFormatter.ISO_WEEK_DATE); // 2018-W04-7
String isoLocalDate = now.format(DateTimeFormatter.ISO_LOCAL_DATE); // 2018-01-28
String isoOrdinalDate = now.format(DateTimeFormatter.ISO_ORDINAL_DATE); // 2018-028
String french = now.format(DateTimeFormatter.ofPattern("dd MMM yyyy", Locale.FRANCE)); // 28 janv. 2018 custom format

String isoLocalTime = now.format(DateTimeFormatter.ISO_LOCAL_TIME); // 08:09:31.514569
String isoTime = now.format(DateTimeFormatter.ISO_TIME); // 08:09:31.514569
String nativeFormat = now.format(DateTimeFormatter.ofPattern("hh:mm:ss ")); // 08:10:43
String engFormat = now.format(DateTimeFormatter.ofPattern("h:mm a")); // 08:10 AM

String rfcFormat = now.format(DateTimeFormatter.ofPattern("E, dd MMM yyyy hh:mm:ss")); // Sun, 28 Jan 2018 08:24:31
String basicIsoDate = now.format(DateTimeFormatter.BASIC_ISO_DATE); // 20180128
String isoDateTime = now.format(DateTimeFormatter.ISO_DATE_TIME); // 2018-01-28T08:24:31.412511
String isoLocalDateTime = now.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME); // 2018-01-28T08:24:31.412511
String isoLocalDate = now.format(DateTimeFormatter.ISO_LOCAL_DATE); // 2018-01-28

// ZonedDateTime
ZonedDateTime now = ZonedDateTime.now(); // 2018-02-10T08:49:50.886682+01:00[Europe/Warsaw]
ZoneId zone = ZoneId.of("Europe/Kiev");
ZonedDateTime kievTime = ZonedDateTime.of(localDate, localTime, zone); // 2018-01-01T10:30+02:00[Europe/Kiev]
ZonedDateTime nyTime = kievTime.withZoneSameInstant(ZoneId.of("America/New_York")); // 2018-01-01T03:30-05:00[America/New_York]
List<String> zones = new ArrayList<>(ZoneId.getAvailableZoneIds()); // все зоны

// Period - для дат
Period period = Period.between(LocalDate.now(), LocalDate.of(2018, 9, 23));
int days = period.getDays();
Period period = Period.of(1, 15, 60).normalized(); // ограничить месяцы и дни (12, 31 и т.д.), т.к. не имеет ограничений

// Duration - для времени
Duration duration = Duration.between(LocalTime.of(10, 10,15), LocalTime.now()); // PT11H19M45S
Duration duration = Duration.between(LocalTime.now().atStartOfDay(), birthday.atStartOfDay());
int days = duration.toDays();

// ChronoUnit
long daysToBirthday = ChronoUnit.DAYS.between(LocalTime.now(), LocalDate.of(2018, 2, 11)); // 224

// TemporalAdjusters
LocalDate localDate = LocalDate.of(2018, Month.AUGUST, 24);
TemporalAdjuster fourthSunday = TemporalAdjusters.dayOfWeekInMonth(4, DayOfWeek.SUNDAY);
System.out.println(localDate.with(fourthSunday)); // 2018-08-26, print
TemporalAdjuster firstMonInMonth = TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY);
TemporalAdjuster firstDayOfMonth = TemporalAdjusters.firstDayOfMonth();
System.out.println(localDate.with(firstDayOfMonth)); // 2017-04-01
TemporalAdjuster nextTue = TemporalAdjusters.next(DayOfWeek.TUESDAY); // следующий вторник
System.out.println(localDate.with(nextTue)); // 2020-11-03
TemporalAdjuster nextOrSameWed = TemporalAdjusters.nextOrSame(DayOfWeek.WEDNESDAY);
--
// System
System.currentTimeMillis(); // milliseconds (difference) from January 1, 1970
System.nanoTime();

//ZoneId
ZoneId zone = ZoneId.of("Europe/Berlin");
ZoneId id = ZoneId.systemDefault();
Clock cl = new SystemClock(ZoneId.systemDefault());

// Clock - computer time api
// SystemClock - Clock impl
Clock cl = Clock.systemUTC(); // UTC
Clock cl = Clock.systemDefaultZone(); // local
Clock cl = SystemClock.UTC; // the same as Clock cl = Clock.systemUTC();
Clock cl = SystemClock.withZone(ZoneId.systemDefault());

LocalDateTime.now(); // local time
LocalDateTime.now(ZoneOffset.UTC); // UTC time
--

--
в char хранятся символы Unicode,
ASCII символы 0..127
ISO-Latin-1 - 0..255

16 ричные литералы с плавающей точкой обозначаются p вместо e:
0x12.2P4 == 0x12.2 * (2 * 2 * 2 * 2);
--
литералы - 'x', "hello", 100, используются для постоянных значений
	int x = 07; - восьмиричный
			0xA или 0Xa - шеснадцатиричные (регистр не значит)
--
Java 14+

Helpful Null Pointers - NPE has name to help find source of the NPE
--
Порядок инициализации таков:
    Статические элементы родителя
    Статические элементы наследника
    Глобальные переменные родителя
    Конструктор родителя
    Глобальные переменные наследника
    Конструктор наследника
	
	static block и static variable равны по приоритету и вызываются в порядке объявления
--
int[] x = new int[Integer.MAX_VALUE]; // error
int[] x = new int[Integer.MAX_VALUE - 8]; // правильно, 8 размер спец. заголовков
--
АВТОприводимости В char или boolean не существует
но ИЗ char есть
(не путать с назначением переменной char c = 88, так ОК)

при приведении дробного типа к целому отбросится дробная часть

Long i = (Long) null; // == null, работает, нужно когда Object[] содержит неизвестный массив Long i = (Long) obj[0]
--
//сравнение дробных чисел
System.out.println("|f3-f4|<1e-6: "+( Math.abs(f3-f4) < 1e-6 ));
1e-6 == 1 / (10)*6; //true
--
Java 15+ Text Blocks (JEP 378)

1 no need to use \n \"
2 trailing white space is removed
3 indentation remains untouched
4 indentation that is relevant only for better readability of the Java source code is removed
String block = """
  Multi-line text // removed indentation
   with indentation \ // escaped line break, NO line break here
    and "double quotes"! // NON removed indentation
  """.formatted("X");
--
Модификаторы доступа:
public - всем
package private - default без модификатора, в том же package
protected - package private + всем наследникам этого класса (т.е. в том же пакете НЕ наследникам + в наследниках)
private - только в самом классе

public, package private - могут стоять с interface, class, enum, record
protected, private - только с их members
--
Модификаторы классов и полей по умолчанию:

1. class field/method/nested class - package local, non-final, non-static
2. enum and nested enum - package local, final and static
3. interface field - public static final
4. interface method - public abstract
5. nested class in an interface - public static, non-final
6. record - private, final
7. sealed class or interface - all classes/interfaces 1) OR final 2) OR sealed 3) OR non-sealed (no modifier)

Note. Хотя enum в классе неявно static само слово static можно всеравно написать явно перед enum, но хотя оно еще и final само слово final написать явно нельзя.

При это единственное место где нужно использовать static с enum это при import static:
	import static my.embedded.EnumType.SOME_VALUE;
--
java 16+ record (JEP 395) - аналог @Data + immutable

1 private, final;
2 all fields constructor;
3 Getters, equals, hashCode, toString for all fields
4 Cannot define additional instance fields
5 Always extend the Record class
6 Can Define additional methods
7 Can Implement interfaces
8 Customize constructor and getters
9 Can use local method record

// 1 record
record Point(int x, int y) {
	Point { x = null } // custom constructor
	public int x() { return null; } // custom getter has the same name as var
}
// 2 local method record
void f1() {
	record R(int x) {}; 
	R r = new R(123); 
}

java 19+, JEP-405, JEP440: record patterns
// 0. обычный - старый, указываем тип location в коде выше
if (o instanceof Location location) { ...
// 1. inline - можно использовать name сразу без ссылки на Location
if (o instanceof Location (var name, var gpsPoint)) { f1(name) ...
// 2. destruct - как inline, но делаем destruct вложенной record GPSPoint
if (o instanceof Location (var name, GPSPoint(var latitude, var longitude))) { ... // тут используем переменные latitude или longitude
// 3. generic - передаем generic как inline
record Wrapper<T>(T t, String description) { // объявляем
if (wrapper instanceof Wrapper<Location>(var location, var description)) { ...
// 4. inline switch-case
Double result = switch (object) {
    case Location(var name, GPSPoint(var latitude, var longitude)) -> latitude;
    default -> 0.0;
};
// 5. чтобы избежать default запрещаем использовать всем кроме Location, чтобы срабатывал только case
public sealed interface ILocation permits Location {
    default String getName() {
        return switch (this) {
            case Location(var name, var ignored) -> name;
        };
    }
}
// 6. when - case выполнится только при условии when
String result = switch (object) {
    case Location(var name, var ignored) when name.equals("Home") -> new Location("Test", new GPSPoint(1.0, 2.0)).getName();
    case Location(var name, var ignored) -> name;
    default -> "default";
};

-----
в case допустимы byte, short, int, char, enum
	начиная с jdk7 и String (но он медленнее обычных)
	одинаковых значений в case быть не может,
		и там не могут стоять выражения, только готовые значения
-----
if, switch (в блоке case), try (включая finally и catch)
	блоки со своей областью видимости

Switch Expressions vs switch-case
1) нету fall-through semantics - no breaks
2) compiler guarantees switch exhaustiveness - only one case will be executed OR default

// Switch Expressions (JEP 361)
// 1 short
String group = switch (planet) {
  case MERCURY, VENUS, EARTH, MARS -> "inner planet";
  case JUPITER, SATURN, URANUS, NEPTUNE -> "outer planet";
  case null: -> "null"; // теперь можно null
  default -> "bla"
};

// 2 scope + yield
String group = switch (planet) {
  case EARTH, MARS -> {
    System.out.println("inner planet");
    System.out.println("made up mostly of rock");
    yield "inner";
  }
  case JUPITER, SATURN -> {
    System.out.println("outer planet");
    System.out.println("ball of gas");
    yield "outer";
  }
};

// return void
var r = switch (planet) {
  case EARTH, MARS -> System.out.println("inner planet");
  case JUPITER, SATURN -> System.out.println("outer planet");
}

JEP441: Pattern Matching for Switch
	switch (obj) {
		case String s -> System.out.println("Object is a string:" + s);
		case FruitType f -> System.out.println("Object is a fruit: " + f);
		case FruitType f when (f == FruitType.APPLE) -> { // Case Refinement, доп условие с when
			System.out.println("Object is an apple");
		}
		case FruitType.APPLE -> System.out.println("Object is an apple"); // Enum Constants
		case null -> System.out.println("Object is null"); // Switches and Null
		default -> System.out.println("Object is not recognized");
	}
--
try { return 1; }
finally { return 3; } // затрет return 1;

finally НЕ будет вызван если:
System.exit(), JVM crashes,
infinite loop в try or catch,
во всех случаях когда JVM внезапно завершилась
--
return; - можно вызывать в constructors
throws - можно в constructors
--
MyEnum mE = MyEnum.A;
mE == MyEnum.A; //true
switch(mE) { //тип перечисления ДОЛЖЕН быть один и тот же
	case A: //ДОЛЖЕН указыватся без имени класса
}

	MyEnum mE = (MyEnum.values())[0]; // == MyEnum.A, это Array
public static тип_перечисления valueOf(String) //возвращает константу перечисления с данным именем

члены перечисления - константы перечисляемого типа, они public static final
Перечисление не может быть суперклассом И не может наследовать другой класс.
НО оно МОЖЕТ реализовывать интерфесы.
Конструктор может быть только private или package (default)!
enum My implements MyInteface { // enum так часто используют как constants в коде вместо static final переменных
	A(0),B(5), C("str"), D {
		@Override void f1(){return 12345;} // Анонимный enum, Вместо наследования Enum можно использовать @Override
	};
	
	private int j; private String str;
	
	private My(int i){ j = i; } // private конструктор - хорошая практика, если использовать Enum как properties
	private My(String str){}
	
	void f1(){return j;}
}
My.A.f1(); //вызов из объекта

== - можно сравнить ссылки на перечисления
final int ordinal() //возвращает порядок константы на которой вызван, номерация с 0
final int compareTo(тип_перечисления enum) //сравнивает порядковые НОМЕРА констант, возвращает -1, 0, 1
equals(Object obj) //возвратит true только если константы совпали
== vs equals - можно использовать оба

Enumeration - аналог Iterator, не рекумендуется, но иногда используется
    boolean hasMoreElements()
    E nextElement()
Iterator и Enumeration:
    Enumeration в два раза быстрее Iterator и использует меньше памяти.
    Iterator потокобезопасен, т.к. не позволяет другим потокам модифицировать коллекцию при переборе.
    Enumeration можно использовать только для read-only коллекций. Так же у него отсутствует метод remove();
        Enumeration: hasMoreElement(), nextElement()
        Iterator: hasNext(), next(), remove()
--
Доступ к НЕ static членам внешнего класса из вложенного можно получить через имя класса (как для default метода в интерфейсе):
	A.this.x
--
A extends B & C & D - классом может быть один элемент B и класс может идти только первым в списке

Если есть interface и class с одинаковыми сигнатурами метода, то проблем не будет т.к. приоритет у реализации класса:
	interface C { default int f1() { return 1; } }
	class B { int f1() { return 2; } }
	class A extends B implemets C { } // ошибки НЕ будет, метод из class B перекроет метод С

Если есть 2 interfaces B и C с одинаковыми сигнатурами default методов будет ошибка - error: class inherits unrelated defaults for method() from types InterfaceA and InterfaceB
Как исправить: делать Override метода в классе и вызвать метод нужного интерфеса через super
	class A { @Override void f1() { InterfaceB.super.method(); }
--
если есть интерфейсы A и B с одинаковыми методами, то в C<D extends A> можно использовать <D extends B>? Если да, то это structural type, если нет, то это nominative type
--
(внутренний)
//к вложенному static доступ через имя
OuterClass.StaticNestedClass nestedObject = new OuterClass.StaticNestedClass();

inner enum по умолчанию static, хотя слово static всеравно можно написать

(вложенные)
class OuterClass {
    class InnerClass {}
}
OuterClass outerObject = new OuterClass();
//но ССЫЛКУ создать можно и без new
OuterClass.InnerClass innerObject = outerObject.new InnerClass();

private класс не виден снаружи и не удастся создать даже ссылку

class C implements A.B {} //использование

Вложенный класс НЕ может иметь static члены, КОТОРЫЕ НЕ final;
Вложенный класс НЕ может иметь static методы.
Вложенный класс может иметь static члены и методы, если он сам static
может иметь доступ только к static членам внешнего, если он сам static
Вложенный класс НЕльзя сериализовать отдельно от его хозяина.
--
 Наследование от внутренних классов
class A { class Inner {} }
class B extends A.Inner {
   // !!! B() {}  - не скомпилится !!
   B(A a) {
      a.super(); // это обязательно
   }
}
new B(new A()); // создание
---
void f1(String s, int ... arr);
--
static String.valueOf(примитивный_тип) - для примитивных типов строку из них
--
Все строки-константы, которые встречаются В КЛАССЕ (поля класса) автоматически интернированы.
(Зачем: при интернировании строк можно получить преимущество в использовании памяти, т.к. вы храните в ней лишь один экземпляр)
new String("test").intern() == "test"; // ==> true
--
Типы классов в java:
    Top level classes
    Interfaces (не могут быть локальными, т.к. Не могут быть объявлены как private/public/protected или static)
    Enum
    Static nested classes
    Member inner classes
    Local inner classes
    Anonymous inner classes
--
Object - предок всех объектов в Java, в том числе массивов (т.к. это тоже объект)

методы:
protected Object clone() throws CloneNotSupportedException - создаёт клон, клонировать можно только объекты реализующие interface Cloneable
    - protected чтобы в наследниках можно переопределить, но сам метод виден не был
    - чтобы самому определить какие классы можно clone()
boolean equals(Object object) - сравниваем объект
protected void finalize() - вызывается перед удалением объекта
final Class<?> getClass() - класс объекта во время выполнения
int hashCode() - хэш объекта; адрес объекта в памяти, если метод не реализован
String toString() - возвращает строку при выводе в поток
					при выводе в поток берется он вызывается автоматически, строка из этого метода - System.out.println(myObj);

final void notify() - возобновляет работу потока синхронизированного на объекте (или любого одного, если остановленных много)
final void notifyAll() - тоже для всех потоков
final void wait() - останавливает поток на объекте
final void wait(long милисек.) - останавливается, максимальное время остановки
final void wait(long милисек., int наносек)
--
Иерархия исключений:
	Throwable //корень (сам переопределяет Object?)
		Exception //пользовательские, от него наследовать при создании своих исключений
			RuntimeException //(непроверяемое) деление на ноль, выход за границы массива
							// его нужно использовать для своих exception если приложение на них рассчитано (e.g. в серверных приложения для отката transaction)
		Error //(непроверяемое) ошибки среды, не перехватываются как правило
Не обработанные исключения попадают в обработчик по умолнию. Он прерывает работу программы! И выводит его описание и трассировку стека.
throws НЕ ОБЯЗАТЕЛЬНО ОБЪЯВЛЯТЬ для Error и RuntimeException и их наследников
 из того же метода можно выбросить любого наследника исключений из throws
 throws исключения в переопределенных методах могут только сужаться. Исчезнуть совсем или быть наследниками типа исключения выбрасываемого в переопределяемом методе.
RuntimeException - можно перехватить в try-catch, но иногда не нужно т.к. если это RuntimeException, но рассчит на остановку программы.
RuntimeException - часто делаеют throw в thread чтобы остановить ее, тогда оно перехватывается в main thread в thread..setUncaughtExceptionHandler(...)

Что будет, если в static блоке кода возникнет исключительная ситуация?
    Если в явном виде написать любое исключение в static-блоке, то компилятор не скомпилирует исходники.
    Если unchecked исключение вывалится в static-блоке, то класс не будет инициализирован.
    
 Какое исключение выбрасывается при ошибке в блоке инициализации? (init тут видимо имеется ввиду не-статик блок)
    Для static: java.lang.ExceptionInInitializerError - если исключение наследуется от RuntimeException
    Для init:   exception, который и вызвал исключение - если он наследуется от RuntimeException.
    Верно для static и init:
        java.lang.Error — если исключение вызвано Error. 
        java.lang.ThreadDeath — смерть потока. Ничего не вываливается.
			ThreadDeath - c jdk 21+ удален
 
В JDK7:
1) try с ресурсами, при этом переменные из try считаются final
	try(FileOutputStream fin = new FileOutputStream("name"); FileOutputStream fin2 = new FileOutputStream("name")) {
	} catch(Exception e) {}
2) Re-throw, ислючение должно БЫТЬ final ИЛИ ИСПОЛЬЗОВАТЬСЯ КАК final (быть НЕЯВНЫМ)
try{
    throw new IOException(); //Здесь происходит что-то что выкидывает исключение
}catch(Exception e){
    throw e; //Здесь мы ловим что попало, например для логирования. А потом выбрасываем дальше. Это и есть Re-throw.
}
3) многократный перехват, Exception должно быть неявно final
catch(ArithmeticException | ArrayindexOutOfBoundsException e){} //e - final

в try с ресурсами:
1 для interfaces Closeable и AutoClosable вызовется метод close()
2 close() вызовется в рамках блока try вконце автоматом, т.е. до блока catch и finally
---
Heap pollution - это когда при присвоении var не совпадают типы Generic (parameterized type), throw ClassCastException
--
java.lang.NoSuchMethodError - когда метода нет в подключенной library и ошибка появляется только в runtime, но НЕ при compiling (т.е. только во время работы)
	это видимо потому что этот method вызывается не напрямую, а другим методом из другой library
	решение проблемы: использовать совместимые версии libraries
--
Уровни удержания?		
//java.lang.annotation.RetentionPolicy
	SOURCE - аннотация видна только в исходнике (напр. для аннотации по которой генерируется документация)
	CLASS (по умолчанию, если не указано) - только во время компиляции в файле .class
	RUNTIME - доступны во время компиляции и после неё в программе (файл .class)
	НО аннотации локальных переменных НЕ ВИДНЫ в .class никогда

@Target(перечисление ElementType) - только для других аннотация, показывает на каких элементах может быть применена аннотация
@Target({ElementType.FIELD, ElementType.PACKAGE})
@Target(ElementType.METHOD)

Перечисление ElementType:
	ANNOTATION_TYPE - аннотация
	CONSTRUCTOR - конструктор
	FIELD - поле
	LOCAL_VARIABLE - локальная переменная
	METHOD - метод: public @Recoпunended Integer fЗ(String str) {}
	PACKAGE - пакет: @PackageAnno package my;
	PARAMETER - параметры метода
	TYPE - класс, интерфейс или перечисление
	
	с JDK8:
		TYPE_PARAMETER - параметр типа (параметр обобщения)
			анотированный параметр обобщения
			class TypeAnnoDemo<@What (description  =  "Данные обобщенного типа" ) Т> {
		TYPE_USE - использование типа (анатируется ТИП), анатировать возвращаемый void нельзя
			 public @A Integer fЗ(String str) {} //тип возврата
			 
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@interface Inh {} //наследуется

@Retention(RetentionPolicy.RUNTIME)
@Repeatable(AContainer.class)
@interface A {
	int val();
	String str();
}

@Retention(RetentionPolicy.RUNTIME)
@interface AContainer {
	A[] value();
}

@A(val = 3, str = "ttt")
@A(val = 7, str = "ttt2")
void f1() {}

Типы которые могут использоваться в аннотациях:
int, double, String, Class, enum, другие аннотации (наследники Annotation?) и все массивы этих типов

Ограничения:
Аннотации не могут наследовать друг друга.
В их методах не может быть параметров.
Аннотации не могут быть обобщенными.
У методов в аннотацих не может быть throws

Нельзя пометить аннотацией поле класса-предка через класс-наследник (т.к. нельзя @Override).
--
ClassFile - jvm spec описывает структуру скомпиленного class

ClassFile {
    u4             magic;
    u2             minor_version;
    u2             major_version;
    u2             constant_pool_count;
    cp_info        constant_pool[constant_pool_count-1];
    u2             access_flags;
    u2             this_class;
    u2             super_class;
    u2             interfaces_count;
    u2             interfaces[interfaces_count];
    u2             fields_count;
    field_info     fields[fields_count];
    u2             methods_count;
    method_info    methods[methods_count];
    u2             attributes_count;
    attribute_info attributes[attributes_count];
}
--
java.lang.Math - набор мат методов
Math.toIntExact(long)
Math.incrementExact(long)
Math.subtractExact(long, long)
Math.decrementExact(long)
Math.negateExact(long),
Math.subtractExact(int, int)
--
OutputStream
	transferTo(OutputStream) - jdk21+, эффективная передача с input на output
OЬjectOutputStream			Поток вывода объектов
FileOutputStream			Поток вывода, записывающий данные в файл
ВUfferedOutputStream		Буферизированный поток вывода
BufferedWriter
InputStrea.Reade			Поток ввода, преобразующий байты в символы
								(для вкладывания байтовых потоков в символьные)
PrintStreaш
	(наследник OutputStream)	Поток вывода, содержащий методы print ()
								и println ()
PrintWriter					Поток вывода, содержащий методы  print()  и  println  ()
PushЬackReader				Поток ввода, позволяющий возвращать символы обратно
								в поток ввода

PrintStream - добавляет print() метод и не дает Exception, вместо этого флаг checkError
InputStrearnReader(Systern.in) - преобразует Stream в Reader
ReaderInputStream / WriterOutputStream - Reader / Writer в Stream
PrintWriter(OutputStream os) - преобразует Stream в Writer
BufferedReader/ВufferedOutputStream - в них оборачивают чистые Reader/Writer или Stream для буферизации
    методы: br.read(), br.readLine()
FilterReader / FilterWriter - можно переопределить методы read / write как обертки и менять их поведение
StringReader / StringWriter - читае / пишет в строку
ByteArrayOutputStream - содержит байты и методы read()/write()
    new ByteArrayOutputStream().toByteArray(); // == cast to byte[]
    
RandomAccessFile - напоминает использование совмещенных в одном классе потоков
    DataInputStream и DataOutputStream (они реализуют те же интерфейсы DataInput и DataOutput)
    имеет методы seek() - позволяет переместиться к определенной позиции и изменить хранящееся там значение
    содержит методы для чтения и записи примитивов и строк UTF-8.
RandomAccessFile может открываться: чтения («r») или чтения/записи («rw»), «rws» (сразу запись изменений без flush)

8. Какой класс-надстройка позволяет читать данные из входного байтового потока в формате примитивных типов данных?
    DataInputStream - Для чтения байтовых данных
        readByte() - для чтения байтов, не читает EOF байт (не различает)
        available() - сколько байт осталось до конца (т.к. через readByte() это не узнать)
        Конструктор: DataInputStream(InputStream stream)
        Методы: readDouble(), readBoolean(), readInt()
    ByteArrayInputStream - подходящий для передачи в DataInputStream (String можно преобразовать через getBytes() )
    
FilenameFilter:
FilenameFilter с методом accept() работает как фильтр списка файлов:
    String[] myFiles = directory.list(new FilenameFilter() {
    public boolean accept(File directory, String fileName) {
        return fileName.endsWith(".txt");
    }

Входные данные разбиваются регулярками на куски и читаются по очереди методом next()
sc = new Scanner(new FileReader("file.txt"))
Пример: sc.nextDouble() - читает и double и int (т.к. его можно преобразовать)

Читаем через буфер, чтобы не было переполнения!!!:
byte[] buffer = new byte[32768];
fin = new FileInputStream("name.io");
do {
    char c = (char) fin.read(buffer);
} while(i != -1);

Еще один пример чтения:
    FileInputStream fis= new FileInputStream("bla");
    byte[] buf = new byte[1024];
    int len;
    while((len=fis.read(buf))>0){
        zos.write(buf, 0, len);
    }

байтовые:
write(int байт) - зписывает только 8 младших бит int
	int a = 'A';
	System.out.write(a);
	
FileInputStream fi = new FileInputStream("/file.io");
//или так
FileInputStream fi = new FileInputStream(new File("/file.io"));
--
Console c = System.console();
String s = c.readLine(); // и много др. методов
PrintWriter pw = c.writer();
Reader r = c.reader();
--
Для хранения интернацилизаций (строк на разных языках) и переключения между ними:
ResourceBundle - , ListResourceBundle - массив значений, PropertyResourceBundle
ResourceBundle stats = ResourceBundle.getBundle("StatsBundle", currentLocale); // вытаскиваем StatsBundle_ja_JP с суфиксом ja_JP
--
iOb instanceof Gen<?> - проверка можно ли привести класс к Gen

null instanceof SomeClass == false
А ВОТ В ОБРАТНОМ ПОРЯДКЕ exception

// Pattern Matching (JEP 394) for Instanceof - type cast inside instanceof
if (o instanceof String s && !s.isEmpty()) {
  System.out.println(s.toUpperCase()); // no error, s is cated to String
}

--
Ограничивать можно ОДНИМ классом и несколькими интерфейсами
class Gen<T extends MyClass & MyInterface1 & MyInterface2 & MyInterface3> 

class A<T> {
	static T ob; //ошибка, нельзя параметры типа в static
	static T f1(){} //ошибка, нельзя static метод с параметром типа КЛАССА
	static <V>f2(V v){} //правильно, НО если параметр у функции свой отдельный от класса то можно
}
--
java 17+

Sealed Classes (JEP 409) - список classes которые могут наследовать данный class
Note: Only final, sealed or non-sealed class can extend sealclass

Every subclass or interface in the permits the list must use exactly one of the following modifiers: 
1 final (disallows further extension; for subclasses only, since interfaces cannot be final)
2 sealed (permits further, limited extension)
3 non-sealed (permits unlimited extension again)

// 1 c;ass - sub classes
public sealed class Parent permits ChildA, ChildB, ChildC { ... }

// 2 interface - sub interfaces + sub classes
sealed interface Parent permits ChildA, ChildB, ChildC { ... }

// without permits, inside the same file
public sealed class Parent {
  final class Child1 extends Parent {}
  final class Child2 extends Parent {}
  final class Child3 extends Parent {}
}
--
@FunctionalInterface - маркер, пишется над интерфейсами. Обозначает что интерфейс функциональный (т.е. содержит только один абстрактный метод). Для лямбда выражений. Не обязательна.
    Иначе в таком интерфесе можно будет писать любые методы.
    default методы быть могут в любом случае, а еще методы наследованные от Object?

Можно использовать готовые интерфейсы:
Function<String,  Integer> func = String::length;
			
Class::staticMethodName		(s) -> String.valueOf(s)
object::instanceMethodName	() -> object.toString()
Class::instanceMethodName	(s, s2) -> s.toString(s2)
ClassName::new				() -> new String()

super::имя_метода - ссылка на метод предка
this::имя_метода
EnclosingClass.this::method - для внутреннего класса можно так
MyClass::new - ссылка на конструктор, какой именно конструктор зависит от контекста
LinkedList::new - В ссылках на методы <> не пишется

ЛОКАЛЬНЫЕ переменные использованные внутри лямбда-ф. становятся неявно final

//конструктор массива
MyArrayClassFactory<A[]> mf = A[]::new;
A[] a = mf.func(2);
a[0] = new A();
a[1] = new A();

//1.
interface A {
	//B - ссылка на объект из которого будет вызов: String s = b.f1(n);
	String f1(B b, int n); //String s = (b, n) -> b.f1(n);
}
//2.
class B {
	String f2(int n) {
		return n + "_1";
	}
}
//3.
A a = B::f2;
String s = a.f1(new B(), 2); //тоже самое что: String s = new B().f2(2);

4. Ссылка на обобщенные методы
interface A<T> {
	int func(T val);
}
class B {
	static <T> int f1(T val) {
		return String.valueOf(val);
	}
}
A<T> mth = B::<String>f1; //тип после :: относится к методу

//5 применяются для фабрик объектов
для обобщенного конструктора:
interface A<T> {
	int func(T[] val);
}
class B<T> {
	B(T val) {
		val.bla();
	}
}
A<Integer> cnstr = B<Integer>::new; //тип перед :: относится к классу
B b = cnstr.func(); //вызов конструктора из ссылки на него
--
Currying, карирование - трансляция одной фции f(a,b,c) в серию фций: f(a)(b)(c)
java 8+, vavr

с java 8+ можно передать функцию в качестве параметра в другую фцию
в java только фции до 2х параметров, больше в библиотеке vavr

Arity - количество параметров карированной функции

Currying - это альтернатива встренным функциональным интерфейсам, можно создать builders для них
2ым параметром можно в generic можно обьявлять сколько угодно Function

java 8 Function (не карирование):
	BiFunction<String, String, Letter> SIMPLE_CREATOR = (salutation, body) -> new Letter(salutation, body);
	SIMPLE_CREATOR.apply("bla").apply("bla2")
1 карирование:
	Function<String, Function<String, Letter>> CURRIED_CREATOR = salutation -> body -> new Letter(salutation, body);
2 карирование с параметрами по умолчанию через apply:
	Function<String, Function<LocalDate, Function<String, Function<String, Function<String, Letter>>>>> CREATOR = returningAddress -> LETTER_CREATOR.apply(returningAddress).apply(CLOSING);
3 карирование с builder паттерном:
	MyFunc builer() { return p1 -> p2 -> p3 = new Letter(p1, p2, p3, salutation); }
4 карирование с использованием FunctionalInterface вместо Function для builder:
	interface AddReturnAddress { Letter.AddClosing withReturnAddress(String returnAddress); }
	interface AddClosing { Letter.AddDateOfLetter withClosing(String closing); }
	Letter.builder().withReturnAddress(RETURNING_ADDRESS).withClosing(CLOSING)

java.util.function - пакет
		cunsumer - то что принимает значения (потребляет) и НЕ ВОЗВРАЩАЕТ результат
			DoubleConsumer: void accept(double d)
		function - то что оперирует РАЗНЫМИ (или одним) объектами и даёт результат
			DoubleFunction: R apply(V v, T t)
		operator - опирирует двумя ОДНОТИПНЫМИ объектами и даёт результат
			DoubleOperator: R apply(Double i1, Double i2)
		predicate - принимает разные объекты и ВОЗВРАЩАЕТ boolean
			DoublePredicate: boolean test(V v)
				Predicate or(predicate)
				Predicate not(predicate)
				Predicate and(predicate)
				Predicate negate()
				Predicate isEqual(Object targetRef) - результат Objects.equals(Object, Object)
		supplier - НЕ ПРИНИМАЕТ ПАРАМЕТРЫ (объекты), сам что-то делает и возвращает результат
			DoubleSupplier: T get()

		Runnable - используется и для lambda, не из java.util.function, не только для Thread
			Runnable: run()
			
		Приставки:
			Bi - если принимает 2 аргумента (BiFunction)
			To - если приобразует объект в другой (ToIntBiFunction)
			Unary - если оперирует только одним параметром (UnaryOperator)

unary - Function
nullary - Supplier
binary - BiFunction

static <T> Function<T, T> identity() { return t -> t; } - значение в само себя
var map1 = list.stream().collect(Collectors.toMap(Function.identity(), Function.identity()));
var map2 = list.stream().collect(Collectors.toMap(t -> t, t -> t));

Function Composition - композиция фций в 1ну фцию
1 java.util.function.Function.compose(), LIFO execution flow
	Function<Integer, Integer> squared = e -> e * e;
	times2.compose(squared).apply(4); // Returns 32, times2(squared(4))
2 java.util.function.Function.andThen(), FIFO execution flow - противоположность compose()
	times2.andThen(squared).apply(4); // Returns 64, squared(times2(4))
3 пример:
	(String::trim).andThen(String::toLowerCase)
		.andThen(StringBuilder::new)
		.andThen(StringBuilder::reverse)
		.andThen(StringBuilder::toString).apply(" BLA "); // ALB
--
java.lang.ref.* WeakReference, PhantomReference, SoftReference

Strong Reference - обычная ссылка в Java

WeakReference
PhantomReference
SoftReference
--------------------------------
Service Provider Interface (SPI) - спецификация, которая позволяет реализовать directory service в виде плагинов для frameworks

По сути это интерфейс, который должен быть реализован кодом, который хочет работать как плагин и быть включен в другой код?

Используется в: JNDI и разных классах для доступа к данным (i/o, sound, db etc)
--------------------------------
JMX - Java Management Extensions
Source: [baeldung JMX Ports](https://www.baeldung.com/jmx-ports)
MBean - Managed Bean
	http://alvinalexander.com/blog/post/java/source-code-java-jmx-hello-world-application
	https://en.wikipedia.org/wiki/Java_Management_Extensions
	
	(потом надо описать?) Различают:
		Standard MBeans
		Dynamic MBeans
		Open MBeans
		Model MBeans
		MXBeans
		
	Как использовать:
	1. Создаем Bean
		class HelloMBean {
			String val;
			//getters, setters here
		}
	2. Регистрируем его в спец. сущности (созданное через фабрику)
		ManagementFactory.getPlatformMBeanServer().registerMBean(
			new HelloMBean(), new ObjectName("FOO:name=HelloBean")
		);
	3. Запускаем нашу программу со спец. командой в jconsole
		java -Dcom.sun.management.jmxremote \
			 -Dcom.sun.management.jmxremote.port=1617 \
			 -Dcom.sun.management.jmxremote.authenticate=false \
			 -Dcom.sun.management.jmxremote.ssl=false \
			 SimpleAgent
	4. Теперь видим значения MBean в специальном веб интерфейсе (и можем менять значения?)
	
	Для чего: для мониторинга программы?
--------------------------------
JNDI (Java Naming and Directory Interface) - это Java API, просто набор интерфейсов для получения данных из БД, properies, xml и т.д.
http://forum.vingrad.ru/articles/topic-157996.html

Сюда входят понятия:
    directory service (or name service) - связывает имя ресурса и адрес по которому он доступен
    Service Provider Interface (SPI) - спецификация, которая позволяет реализовать directory service в виде плагинов для frameworks

Используется для: сервера LDAP, RMI и Java EE использует JNDI для получение объекта по адресу, получение конфигов Java Servlet из web container и т.д.

Пример:
interface Context {
	Object lookup(String jndiName);
}
public class InitialContext {
   public Object lookup(String jndiName){
   
      if(jndiName.equalsIgnoreCase("SERVICE1")){
         System.out.println("Looking up and creating a new Service1 object");
         return new Service1();
      }
      else if (jndiName.equalsIgnoreCase("SERVICE2")){
         System.out.println("Looking up and creating a new Service2 object");
         return new Service2();
      }
      return null;		
   }
}
Context c = new InitialContext();
Service ob = (Service) c.lookup("java:global/CMT_Example/CartBean!com.example.beans.CartBean");
--------------------------------
LDAP (Lightweight Directory Access Protocol) - протокол доступа к БД, файлам и т.д. Сама БД за протоколом LDAP может быть любой.

Directory - термин означающий ключ-значение, связь ресурса и ссылки на него.
--------------------------------
5. Через Statement выполняем запрос:
	new Statement().executeQuery("SELECT * FROM users WHERE user='...")
	new Statement().executeUpdate("INSERT, UPDATE, DELETE ...");
	execute("any SQL or procedure"); // любой  SQL или процедура возвращающая много результатов
	
	try {
		Connection con = (Connection) DataSourceSingleton.getInstance();
		try {
			Statement stat = (Statement) con.createStatement();
			try {
				ResultSet res = new Statement().executeQuery("SELECT ...");
				while(res.next()) str = res.getString("filePath");
			} finally { if (res != null) res.close(); }
		} finally {if (stat != null) stat.close();}
	} catch(SQLException e) { e.printStackTrace(); }
	finally { if (con != null) con.close(); }
		
--
batch-команда из PreparedStatement
	Выполняет группу SQL
	conn.setAutoCommit(false); //отключаем автовыполнение запроса сразу?
	Statement st = conn.createStatement();
	st.addBatch("INSERT INTO ...");
	st.addBatch("INSERT INTO...");
	int [] sqlCount = st.executeBatch(); //число строк изменное конкретным запросом
	
	//в цикле, БД в зависимости от настроек может кэшировать PreparedStatement
	PreparedStatement ps = conn.preparedStatement("insert ... values(?,?,?)");
	for( : ) {
		ps.setInt(1, 354);
		ps.setString(2, "bla");
		ps.setInt(3, 1231);
		ps.addBatch(); //добавляем процедуру с новыми значениями
	}
	int rowCount[] = ps.executeBatch();
--
	--CallableStatement (extends PreparedStatement)
		Используется для вызова хранимых процедур БД.
		Может также получать возвращаемые процедурами значения (IN, OUT, INOUT).
		
		CallableStatement cs = conn.prepareCall("{call myProc(?, ?)}");
		cs.setInt(1, 12313);
		cs.registerOutParameter(2, java.sql.Types.VARCHAR); //второй параметр процедуры как return
		cs.execute();
		String lastName = cs.getString(2); //читаем возвращенное значение из второго параметра процедуры 
--
b = (byte) i; // b = i % sizeof(byte) если i>byte иначе b = i
если i больше типа byte, то b будет == i % byte (остатку от деления на размер типа)
--
@Override
public boolean equals(Object obj) {
    if (obj == null) return false;
    if (!(obj instanceof Student))
        return false;
    if (obj == this)
        return true;
    return this.getId() == ((Student) obj).getId();
}
--
Редирект:
	request.getRequestDispatcher("/jsp/result.jsp").forward(request, response);
	request.getRequestDispatcher("login.html").include(request, response); // как include в jsp, код продолжает работать с места вызова
        Ограничения include(): не может вызывать методы response, которые меняю headers для response, например setCookie()
	response.sendRedirect() - прямая переадрессация
	Отличие: в forward используется тот же самый request и он быстрее
---------
Отношения эквивалентности equals (для НЕНУЛЕВЫХ ссылок):
    симметричность - x.equals(y) == true, то y.equals(x) == true
    рефлексивность - x.equals(x) == true
    транзитивность - x.equals(y) == true и y.equals(z) == true, то x.equals(z)
    Еще свойства: постоянство и неравнество null
--
StringTokenizer (extends Enumeration) - Разделение текста на части (лексемы) по заданному разделителю (или нескольким)

StringTokenizer s = new StringTokenizer("val=5;val2=6;", "=;");
while(s.hasMoreTokens()) {
	String key = s.nextToken();
	String val = s.nextToken();
}
--
Arrays - набор методов для работы с массивами
List l = Arrays.asList("ab", "cd");
sort(arr, comporator) - использует merge sort OR Tim Sort в зависимости от типа элементов, всегода последовательна
parallelSort(arr)
		- если массив меньше чем java.util.Arrays.MIN_ARRAY_SORT_GRAN = 8192, то сортировка становится последовательной иначе использует ForkJoin common pool, значение подобрано разрабами jvm эмперически
		- по умолчанию TimSort, можно переключиться на устаревшую MergeSort через java.util.Arrays.useLegacyMergeSort=true
	SortedOps.java - implementation relevant to the streams
	Arrays.java - implementation of the Arrays helper, see the different sort methods
	TimSort.java - implementation of TimSort
	ComparableTimSort.java - variation for classes implementing Comparable
	DualPivotQuicksort.java - implementation of sorting for primitives
static int binarySearch(Object arr[], Object val) - только для сортированных массивов
static void parallelSort(тип[] arr) - сортирует сначало части массива, а потом целиком. Быстрее.
toString()
hashCode()
deepToString() - для массивов содержащих вложенные массивы
deepHashCode() - для массивов содержащих вложенные массивы
static <T, U> T[] copyOf(U[] источник, int длина, Class<? extends T[]> результирующий_тип)
	ЕСЛИ копия длиннее, то лишние элементы инициализированны по умолчанию
	arr = Arrays.copyOf(arr, N + 1);
--
Stream API - функциональная обертка вокруг data source, она не делает ничего дополнительного и ничего не хранит
каждый конвеерный метод создает новый stream
Stream API превращает набор циклов стрима на самом деле в 1 большой цикл, после вызова терминального метода
by default все stream api последовательные (как обычный цикл)

IntStream - и др для примитивов работают быстрее т.к. используют примитивы


parallelStream() не рекомендуется для долгих операций, работет с fork/join и долгие операции могут остановить работу всех параллельных стримов

Как вабрать использовать parallelStream() или нет:
1. не всегда выгодно использовать parallelStream() т.к. цена переключения thread context может быть выше чем последовательно обработать немного данных
	1.2 если у нас долгие или блокирующие операции (e.g. i/o), то они займут commonPool и др операциям не достанется потока, будут долго ждать
	1.3 неассаоциативные distinct() и reduce() или sorted() - это stateful intermediate операции и снижают эффективность распараллеливания, но доп. оптимизации все же есть
		note. неассаоциативные - где порядок операций играет роль (умножение, деление) - нельзя распараллелить, ассаоциативные - где не играет (сложение, вычитание) - можно распараллелить
	1.4 LinkedList и др структуры данных не поддерживают эффнктивный произвольный доступ, Arrays или ArrayList быстрее
	1.5 forEachOrdered() - не даст выигрыша т.к. последовательный
	1.6 почем sorted() нельзя нормально распараллелить
		1.6.1 все равно будет синхронизированная часть кода где параллельные результаты нужно объединить
		1.6.2 при сортировке нужно установить глобальный порядок элемента и сравнить с другими - это последовательная операция
		1.6.3 для малых данных расходы на параллельность будут выше чем - переключение контекста потоков
		1.6.4 TimSort - алгоритм сортировки по умолчанию, универсален и гибридный, заточен под частично отсортированные данные, не эффективен для параллельности
		1.6.5 при использовании структур с медленным произвольным доступом напр. LinkedList
		1.6.6 parallelStream().sorted() использует Arrays.parallelSort() и все ее правила
		1.6.7 сортировка в stream api - по умолчанию TimSort, можно переключиться на устаревшую MergeSort через java.util.Arrays.useLegacyMergeSort=true
	1.7 когда можно эффективно распараллелить sorted()
		1.7.1 для очень больших массивов - хотя выигрышь будт меньше чем для map или filter и но намного хуже чем O(n)
		1.7.2 если операция сравнения в Comparator для сортировки сложная, то параллельность будет быстрее последовательной обработки
		1.7.3 можно вынести сортировку из stream api и сортировать более эффективно - параллельными алгоритмами
		1.7.4 можно рассмотреть предварительную сортировку или выборку данных как способ ускорения - но это зависит от случая
2. parallelStream() использует fork-join framework common pool, он делит данные между worker threads
3. количество потоков common pool по умолчанию == (cpu core number - 1) или установить через -Djava.util.concurrent.ForkJoinPool.common.parallelism=4
	Этот параметр глобальный для всех common pool и НЕ РЕКОМЕНДУЕТСЯ менять параметр вручную
	по умолчанию потоков: ForkJoinPool.getCommonPoolParallelism() == Runtime.getRuntime().availableProcessors() - количество cpu (ядер)
4. Performance Implications - что влияет на производительность parallelStream()
	4.1 The Overhead - переключение между CPU Thread дорогое поэтому при малом количестве элементов parallelStream() не эффективен
	4.2 Splitting Costs - сложные data source разделяются по потоком дороже чем простые, e.g. array в parallelStream() будет быстрее чем LinkedList
	4.3 Merging Costs - объединение результатов parallelStream() в сложные коллекции дороже, чем например в sum
	4.4 Memory Locality - cpu могут предсказывать и подгружать данные в CPU cache заранее, если данные это ссылки коллекции, то это дороже, для  array of primitives быстрее
5. NQ Model - рекомендация Oracle когда использовать parallelStream()
		Если N*Q > 10 000, то выгодно использовать parallelStream(),
			где N - количество элементов, Q - количество ПРОСТЫХ (как сложение) операций на каждый элемент
			т.е. т.к. обычно с элементами делают простые операции, то если элементов > 10 000 то можно использовать parallelStream()
6. некоторые методы такие например как .sorted(), reduce(...)??? нельзя распаралелить и они делают stream не параллельным (хотя sort можно было бы реализовать паралельно)
7. Где быстрый
	7.1 map, filter, ассоциативный reduce, collect - можно распараллелить
	7.2 ArrayList - поддерживает произвольный доступн -  можно распараллелить
	7.3 большие объемы данных - эффективно параллелить т.к. затраты на переключение потоков меньше чем выигрыш распараллеливания
7. stream api нужно закрывать AutoCloseable.close() если его элементы это io или др внешние ресурсы, если объекты в памяти из List, Set, Array etc то не нужно
	try (Stream<String> stream = Files.lines(Paths.get("file.txt"))) { stream.forEach(System.out::println); } catch (IOException e) { }

Как сделать в Stream API кастомный fork-join framework common pool
NOTE:
	1. using the common thread pool is recommended by Oracle - Oracle не рекомендует использовать custom common pool для parallelStream()
	2. Его можно менять ТОЛЬКО в очень особых случаях.
	3. memory leak - custom ForkJoinPool будет существовать пока не вызван метод customThreadPool.shutdown();
		такой проблемы не будет с обычным common pool т.к. он статичный (один на всех)
	
	Example:
	ForkJoinPool customThreadPool = new ForkJoinPool(4); // 1. create, 4 - cpu thread number
	try {
		int sum = customThreadPool.submit(() -> listOfNumbers.parallelStream().reduce(0, Integer::sum)).get(); // 2. use
	} finally {
		customThreadPool.shutdown(); // 3. destroy, закрываем т.к. пока не вызвать этот метод ForkJoinPool будет существовать и можем получить OutOfMemoryError
	}

методы:
1. Конвейерные — возвращают другой stream, то есть работают как builder,
2. Терминальные — возвращают другой объект, такой как коллекция, примитивы, объекты, Optional и т.д.

создания стримов
	collection.stream();
	Stream.of("a1", "a2", "a3");
	Arrays.stream({"a1","a2","a3"});
	new HashMap().entrySet().stream(); // Map to Stream
	collection.parallelStream(); // параллельный
	collection.stream().sequential(); // parallel делаем sequential, отключаем многопоточность
	Stream<String> streamFromFiles = Files.lines(Paths.get("file.txt")); // каждая строка в файле будет отдельным элементом
	IntStream streamFromString = "123".chars(); // символы
	Stream.builder().add("a1").add("a2").add("a3").build();
	Stream<Integer> streamFromIterate = Stream.iterate(1, n -> n + 1); // бесконечный, с начальным значением
	Stream<String> streamFromGenerate = Stream.generate(() -> "a1").limit(3000); // бесконечный
	stream().empty()
	IntStream stream = IntStream.range(3, 8); // числовой
	LongStream.rangeClosed(2, 200); // от 1 до 100
	LongStream stream = LongStream.range(3, 8); // числовой
	DoubleStream stream = DoubleStream.range(3, 8); // числовой
	Stream<Integer> stream = intStream.boxed(); // unboxing оболочки stream - IntStream -> Stream<Integer>
	collection.stream().unordered(); // проставляет флаг, бесполезно с Set т.к. оно и так unordered; по доке возвращает unordered stream из ordered (если он уже не такой)

	// StreamSupport - низкоуровневые методы создания stream из supplier или spliterator
	StreamSupport.stream(supplier, characteristics, isParallel)
	StreamSupport.stream(spliterator, isParallel)
	StreamSupport.intStream(supplier, characteristics, isParallel)
	StreamSupport.intStream(spliterator, isParallel)
	StreamSupport.longStream(supplier, characteristics, isParallel)
	StreamSupport.longStream(spliterator, isParallel)
	StreamSupport.doubleStream(supplier, characteristics, isParallel)
	StreamSupport.doubleStream(spliterator, isParallel)
	
	// StreamSupport - на практике используется чтобы создать Stream из любого iterator (т.к. стандартного метода stream() в самом iterator нет)
	Stream<T> s = StreamSupport.stream(Spliterators.spliteratorUnknownSize(iterator, 0), false); // 1. Spliterators.spliteratorUnknownSize
	Stream<T> s = StreamSupport.stream(records.spliterator(), false); // 2. spliterator

конвейерных методов
	findFirst, findAny, anyMatch это short-circuiting методы, не обходит весь изначальный стрим, только до первого совпадения
	mapToInt - делает оболочки IntStream
	
	collection.stream().filter(«a1»::equals).count();
	collection.stream().skip(collection.size() — 1).findFirst().orElse(«1»);
	collection.stream().distinct().collect(Collectors.toList()); // если есть equals, то должен быть и hashCode, иначе не сработает
	collection.stream().map((s) -> s + "_1").collect(Collectors.toList());
	collection.stream().map(String::toUpperCase).peek((e) -> System.out.print("," + e)).collect(Collectors.toList());
	collection.stream().limit(2).collect(Collectors.toList());
	collection.stream().sorted().collect(Collectors.toList()); // sort
	collection.stream().sorted((o1, o2) -> -o1.compareTo(o2)).collect(Collectors.toList()) // sort with compareTo
	collection.stream().mapToInt((s) -> Integer.parseInt(s)).toArray();
	collection.stream().flatMap((p) -> Arrays.asList(p.split(",")).stream()).toArray(String[]::new);
	collection.stream().peek(e -> log(e); // как forEach, только конвеерный, часто используется в debug
	
терминальных методов
	collection.stream().findFirst().orElse(«1»)
	collection.stream().findAny().orElse(«1»)
	collection.stream().filter((s) -> s.contains(«1»)).collect(Collectors.toList())
	collection.stream().filter(«a1»::equals).count()
	collection.stream().anyMatch(«a1»::equals)
	collection.stream().noneMatch(«a8»::equals)
	collection.stream().allMatch((s) -> s.contains(«1»)) // allMatch is true for empty stream
	collection.stream().min(String::compareTo).get()
		collection.stream().min(String::compareTo).get()
	collection.stream().max(String::compareTo).get()
		collection.stream().reduce(Integer::max).orElse(-1)
		int computedAges = ages.parallelStream().reduce(
			0, // Identity - initial value
			(a, b) -> a + b, // Accumulator
			Integer::sum // Combiner - нужен если parallelStream, чтобы объединить куски
		);
		String result = collection.stream().reduce("", (str, element) -> str.toUpperCase() + element.toUpperCase());
	set.stream().forEach((p) -> p.append("_1"));
	list.stream().forEachOrdered((p) -> p.append("_new"));
	collection.stream().map(String::toUpperCase).toArray(String[]::new); // toArray
	collection.stream().reduce((s1, s2) -> s1 + s2).orElse(0)
	
дополнительных методов у числовых стримов
	collection.stream().mapToInt((s) -> Integer.parseInt(s)).sum()
	collection.stream().mapToInt((s) -> Integer.parseInt(s)).average()
	intStream.mapToObj((id) -> new Key(id)).toArray() // mapToObj

других полезных методов стримов
	collection.stream().isParallel()
	collection.stream().parallel()
	
методы из Collectors
	toList, toCollection, toSet
	toConcurrentMap, toMap
	averagingInt
	groupingBy 	// разделяет коллекцию на несколько частей и возвращает Map<N, List<T>>
	partitioningBy // как groupingBy, но на 2 части по predicate
	mapping // дополнительные преобразования значений для сложных Collector'ов
	stream.toList(); == stream.collect(Collectors.toList()); // сокращение
	stream.toSet(); == stream.collect(Collectors.toSet()); // jdk16+, сокращение
	
	//full convert through collect(), эта штука внутри collect называется аккамулятор (в данном случае это LinkedList)
	List<A> listFullV1 = listStream.collect(
		Collector.of( 
			() -> new LinkedList<>(), //Supplier как constructor БЕЗ параметров 
			(listA, element) -> listA.add(element), // Accumulator (adder) (2 parameters)
			(listA, listB) -> listA.addAll(listB), // merge/Combiner (joiner) (2 parameters) - нужен для параллельной работы потоков, чтобы join результаты
													// Collectors.toMap(key -> key, value -> value, (k1, k2) -> k1) - если ключи k1 и k2 совпадают, то использовать k1
			StringBuilder::toString // finisher (как post processor), НЕОБЯЗАТЕЛЕН, это то, что преобразует в КОНЕЧНЫЙ результат, как Collectors.toString()
				// (т.е. мы можем что-то поделать с этой своей коллекцией из collect, а потом объект этой коллекции преобразовать во что-то другое),
				// e.g. c -> new ArrayList<Integer>(c)
			Collector.Characteristics.CONCURRENT, // Characteristics, CONCURRENT - можно ли использовать adder/joiner в разных потоках (если есть - да)
			Collector.Characteristics.UNORDERED, // UNORDERED - сохраняют ли adder/joiner порядок элементов
			Collector.Characteristics.IDENTITY_FINISH // IDENTITY_FINISH - указывает что finisher это identity function и может быть пропущена (???)
		)
	);

	numbers.stream().collect(Collectors.summingInt(((p) -> p % 2 == 1? p: 0)))
	numbers.stream().collect(Collectors.averagingInt((p) -> p — 1))
	numbers.stream().collect(Collectors.summarizingInt((p) -> p + 3)) // SummaryStatistics, IntSummaryStatistics{count=4, sum=22, min=4, average=5.5, max=7}
	numbers.stream().collect(Collectors.partitioningBy((p) -> p % 2 == 0)) // Map<Boolean, List>
	strings.stream().collect(Collectors.groupingBy((p) -> p.substring(0, 1))) // Map<N, List<T>>
	// mapping - преобразование, в collect можно задавать цепочку (несколько последовательных Collectors)
	strings.stream().collect(Collectors.groupingBy((p) -> p.substring(0, 1), Collectors.mapping((p) -> p.substring(1, 2), Collectors.joining(":"))))
	strings.stream().distinct().collect(Collectors.toMap((p) -> p.substring(0, 1), (p) -> p.substring(1, 2))) // String to Map
	Arrays.stream(users).collect(Collectors.toMap(User::name, User::role, (key1, key2) -> key1)); // if key1==key2 resolve conflict (use ONLY one 1st element and remove 2nd) or IllegalStateException: Duplicate key
	Arrays.stream(users).collect(Collectors.toMap(User::name, User::role, (x, y) -> y, LinkedHashMap::new)); // users = [user1, user2]; convert to Map with order saving
	strings.stream().collect(Collectors.joining(": ", "<b> ", " </b>")) // join strings
	strings.stream().distinct().map(String::toUpperCase).toArray(String[]::new)

// прием: stateful lambda, distinct by any field, each step of Stream loop will see the same Set - сохранение состояния Set между вызовами фции, чтобы distinct была над всеми
public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
    Set<Object> seen = ConcurrentHashMap.newKeySet(); // will be like GLOBAL variable (despite a fact that it is local var!)
    return t -> seen.add(keyExtractor.apply(t)); // the same Set here
}
persons.stream().filter(distinctByKey(Person::getName)); // use of distinctByKey

// Spliterator (splittable Iterator)
Spliterator<A> spl1 = listStream.spliterator();
while( spl1.tryAdvance( (n) -> System.out.println(n) ) ); // hasNext() + next()
Spliterator<A> spl2 = spl1.trySplit(); // получим 2ую половину
spl1.forEachRemaining((n) -> System.out.println(n)); // forEachRemaining() метод из Iterator (он не многопоточный)
spl2.forEachRemaining((n) -> System.out.println(n));
forEachRemaining(); // на самом деле не только для splitterator и начинает беребор с того места куда дошел указатель iterator когда next() использовался в коде до места вызова этой ф-ции
--
Имеет поля int: DISTINCT, SIZED, STORED, IMMUTABLE, NONNULL, SUBSIZED, ORDERED. Получить характеристику можно с int characteristics().
Имеет вложенный интерфейсы Spliterator.OfDouble, Spliterator.OfInt, Spliterator.OfLong и подчиненный им Spliterator.OfPrimitive
--
codePoint - расширенный unicode - это int + int == long (64 bit)
int codePoint = Character.codePointAt(new char[] {'a', 'b'}, 2);
Character.toChars(cp)

jdk 21+
Character.isEmoji(codePoint)
--
	BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
--
Проверка на null, если сам обьект или одно из выражений в map() будет null, то exception не будет, а вернется то что в orElse()
(использование map() налогично myObj.prop1.prop2 с проверкой на null каждого свойства)
	Object myObjOrElse = Optional.ofNullabel(myObj) // вернет value как с optional.get() т.к. orElse() ИНАЧЕ было бы Optional<Object> myObjOrElse;
		.map(myObj -> myObj.prop1) // if myObj != null или вернет value из orElse
		.filter(prop1 -> prop1.smth() != smth2) // return Optional or empty()
		.orElse(new Object());

Optional
	of
	ofNullable - возвращает Optional-объект, а если нет дженерик-объекта, возвращает пустой Optional-объект
	empty() - пустой Optional-объект
	isPresent() - true/false
	ifPresent(val -> val.doSmth) - вызвать функцию если not null
    orElse() — возвращает объект по дефолту.
    orElseGet() — вызывает указанный метод.
    orElseThrow() — выбрасывает исключение
	get() — возвращает объект, если он есть
	map() — преобразовывает объект в другой объект.
	filter() — фильтрует содержащиеся объекты по предикату иначе вернет Optional.empty() (вернет один Optional, а не stream как в Stream API)
	flatMap() — возвращает множество в виде стрима, если значение не null,
				как map(), но применяется как ifPresent() и полученный Optional не оборачивает в Optional еще раз
				(применяется как ifPresent() только не в конце всего блока, как конвеерный метод, после которого уже можно сделать ifPresent())
	stream() - использование stream сразу с Optional: optional.stream().map(doSmth(el)).collect(toList())
				если Optional.empty() то stream() будет empty
				
// Optional можно конвертировать в stream
List<String> filteredList = listOfOptionals.stream()
  .flatMap(Optional::stream)
  .collect(Collectors.toList());
--
Generics

class C {
  int i = 5;
  void f1() {
	//(про похожий доступ во вложенных классах и простых иерфейсах ниже)
    //C.this.i == 5 работает ТОЛЬКО ВНУТРИ СВОЕГО класса
    //this.i == 5
    
}
--
PECS (Producer Extends Consumer Super) - применяется при работе с Collections, применяется с <?> wildcard
	if only pulling items from a generic collection, it is a producer and you should use extends
	if you are only stuffing items in, it is a consumer and you should use super
1. для методов чтения коллекции - producer - Collection<? extends Thing>
2. для добавления в коллекцию - consumer - Collection<? super Thing>
3. для producer+consumer ( createAndRead() ) не используются keywords, используют точный тип - Collection<Thing> things
		но такое использование это нарушение Comand-Query Separation Principle - раздение методов на читающие и пишущие

ковариант: Covariance: ? extends MyClass
Contravariance: ? super MyClass
Invariance/non-variance: MyClass

1. Ковариантность (? extends) - только подтипы, ссылка на типа предка может указывать на наследника
	- когда collection нужно читать
	Object[] objects = new String[10];
2. Контравариантность (? super) - только родители, можно добавить Dog или подтип Dog
	- когда collection нужно читать заполнять
	List<? super Dog> dogs = new ArrayList<Animal>();
	void add(List<? super Dog> dogs) { .. }
3. Инвариантность (invariant) - запрет Ковариантности, присвоение подкласса или суперкласса запрещено
	List<Object> objects = new ArrayList<String>(); - дженерики инвариантны

? - wildcard

class A<T> {
	static T ob; //ошибка, нельзя параметры типа в static
	static T f1(){} //ошибка, нельзя static метод с параметром типа КЛАССА
	static <V>f2(V v){} //правильно, НО если параметр у функции свой отдельный от класса то можно
	T f1(){return new T[12]} // ошибка, нельзя создать т.к. массив должен содержать инфу о типе своих объектов в runtime, которых тут нет
        // НО можно создать Object array и преобразовать: (T) new Object[12]
}

A<Integer> a[] = new A<>[10]; //ошибка, создать массив обобщенных объектов нельзя
A<?> a2[] = new A<?>[10]; //правильно, можно создать через метасимвол.

теперь в sameAvg() подходят Gen<Double> и Gen<Integer>
class Gen<T extends Number> { boolean sameAvg(Gen<?> t2) {} }

<S super T> // ошибка, super можно только для <?>
void f1(A<? super Number> a) //правильно

<параметры> возвращаемый_тип имя_метода() - обобщенный метод
class A { <T>A(T []arr) {} }
Gen.<MyClass, String>isIn(new MyClass[] { 1, 2 }, "test"); //вызов

При реализации ОГРАНИЧЕНИЕ (extends и/или super) повторять в интерфейсе не нужно. Причем повторять ограничение и невозможно - ошибка.

Обобшенный класс не может extends Throwable

ArrayList<ArrayList> нельзя использовать т.к. он не подтип ArrayList<List>
--
System
	int identityHashCode(obj) - вернет hash code, зависит от реализации jvm, вероятность дублирования этого hash code маленькая
		- hash code не уникален, но гарантируется что он не будет изменен все время существоваяния Object
		- identityHashCode(obj) используется внутри IdentityHashMap
		- для null == 0
	static void arraycopy(Object arrSource, int begin, Object dest, int beginDest, int quntity)
		- копирует массив
	static long currentTimeMills() - возвращает время в милисекундах с 1 янв 1970 года
	static Map<String, String> getenv() - возвращает переменные среды
	static Properties getProperties() - класс Properties связанные с JVM (описывает JVM)
	static String lineSeparator() - строка символов разделитей строк
	static void load(String имя_файла_библиотеки) - загружает динамическую библиотеку
	static void runFinalization() - вызов finalize() для неиспользуемых, но ещё утилизированных объектов
		jdk21+ runFinalization помечена for removal, но существует, можно использовать --finalization=disabled чтобы приверить используется ли она, если да, то заменить на try-with-resources или finally
		Enum.finalize() - для enum
		Runtime.runFinalization()
		System.runFinalization()
	static void gc() - вызов сборщика мусора
	static void exit(int code) - закрывает программу, 0 - ошибок нет
	
    меняем стандартные потоки:
	static void setErr(PrintStream err)
	static void setIn(InputStream in)
	static void setOut(PrintStream out)
--
Calendar c = Calendar.getInstance();
c.set(Calendar.HOUR);
System.out.println(c.get(Calendar.HOUR));
	//по умолчанию текущая дата, региональные настройки и пояс
	GregorianCalendar c = new GregorianCalendar(2018, 3, 30, 10, 21);
	.getTime();
	.getTimeZone().getDisplayName()
	
java.util.Date - mutable, старая ошибка проектирования
java.time - новый пакет для работы с датами, содержит inmutable классы
--
Pattern pat = Pattern.compile(".+?[\\n\\.\\?\\!]+");
Matcher mat = pat.matcher(text);
while (mat.find()) {
	sentenceArr.add(new Sentence(new StringBuilder(mat.group())));
}
--
Иерархия классов окон AWT:
			Component
			|		\
		Container	Canvas
			/	\
	Windows		Panel
		/
	Frame

Component - цвет фона, шрифт, события (так как в AWT источник и обработчик событий сам Component) и т.д.
Container - можно вкладывать в него другие Component
Canvas - пустое окно в котором можно рисовать
Panel - есть метод add() для добавления, простое окно без рамки, строк и т.д.
	После добавления в него компонентов их можно менять
		setLocation ( ), setSize ( ), setPreferredSize ( ) или setBounds ( )
Windows - окно верхнего уровня, оно ни где не содержится
Frame - полноценное окно, именного его наследовать чтобы создавать прилржение

class MyFrame extends Frame {
	//если объявить метод в него передасться объект для рисования
	public void paint(Graphics g) {
		g.drawString("Hello", 10, 40);
		g.drawLine(0, 0, 100, 90);
		//установка цвета
		g.setColor(new Color(100, 100, 255));
		super.paint(g);//кое где надо так из-за вызова не зависимых от ОС компоентов
	}
}

--
Random r = new Random();
r.nextInt(); // nextLong(), nextDouble(), nextBoolean()

Java 17+
Enhanced Pseudo-Random Number Generators (PRNG) - RandomGenerator interface

java.util.Random, SplittableRandom, SecureRandom - legacy classes NOW extend RandomGenerator

RandomGeneratorFactory.all()
	.map(fac -> fac.group()+ " : " + fac.name())
	.sorted()
	.forEach(System.out::println);
RandomGenerator randomGenerator1 = RandomGeneratorFactory.of("Xoshiro256PlusPlus").create(999);
System.out.println(randomGenerator1.nextInt(11));

--
StringBuilder/StringBuffer имеют буфер 16, если не указа
ensureCapacity() - задать емкость
capacity() - объем памяти
trimToSize() - обрезает буфер чтобы он по размеру был ближе к текущему количеству символов	
---
Компилятор неявно импортирует во все программы import java.lang.*
---
public interface A {
	//неявно public final static и ДОЛЖНА быть инициализированна
	public final static int MY_MY = 3; //в подклассе static переменная с таким же именем ПЕРЕКРОЕТ эту???
	//неявно public abstract
	public void f1();
}
---
transient int i = 1; - при сохранении объекта (например сериализации?) переменная не сохранится (т.е. её значение?)
---
java.nio.*
java.nio.channel - каналы
java.nio.charset - набор символов

Асинхронная передача данных в отличии от io:
1. Буфер - хранит данные
2. Канал - соединение с устройством ввода вывода
4. Набор символов - способ сопастовления байтов с символами
    Кодер - кодирует символ в байт
    Декодер - декодирует
5. Селектор - можно выполнять операторы ввода вывода для нескольких каналов одновременно

Суть: создаем channel, привязываем этот канал к буферу
    читаем / пишем через буфер,
    тогда не нужно вызывать rewind() или следить за буфером.

Суть Selector: регистрируем НЕСКОЛЬКО chanel в селектор,
    когда произойдет операция чтение / записи селектор сработает,
    кроме того селектор может "ожидать" ответа от потока (напр. сетевого).
    ГЛАВНОЕ ОТЛИЧИЕ ОТ io: для мониторинга используется ОДИН Thread.

Метод .rewind(); вызывается после каждого put() (изменения)
т.е. перед использованием любого write() (см. прим).
НО при чтении через buffer из nio, который связан с каналом,
НЕ НУЖНО вручную вызывать .rewind()

Прим.: ByteBuffer, MappedByteBuffer и прочие из пакета java.nio.channel

Пример 1 (прямо в канал):
    try(FileChannel ch = (FileChannel) File.newByteChannel(Path.get("file.txt"),
    StandardOpenOption.WRITE, StandardOpenOption.CREATE)){
        ByteBuffer buf = ByteBuffer.allocate(26);
        buf.forEach((v,k) -> buf.put(k));
        // ИЛИ for(int i=0; i<b.capacity(); i++){}
        buf.rewind(); // обнулить позицию на начало
        ch.write(buf);
    } catch(IOException e){}
    
Пример 2 (через буфер, который запишет все сам):
    try(FileChannel ch = (FileChannel) File.newByteChannel(Path.get("file.txt"),
    StandardOpenOption.WRITE, StandardOpenOption.READ,
    StandardOpenOption.CREATE)){
        MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_WRITE,0,26);
        channel.forEach((v,k) -> buf.put(k)); // пишет напрямую в файл через буфер
    } catch(IOException e){}

(чтение из канала до тех пор пока в файле не останется байтов)
Пример 3 (чтение из канала через SeekableFileChannel):
    try(SeekableFileChannel ch = Files.newByteChannel(Paths.get("file.txt"))){
        ByteBuffer buf = ByteBuffer.allocate(26); int length;
        do {
            length = channel.read(buf);
            if(length != -1){ // если в буфере что-то есть
                buf.rewind(); // перемотка на начало буфера
                for(int i=0;i<length;i++){
                    System.out.print((char)buf.get());
                }
            }
        } while{length != -1} // пока файл не законфился
    } catch(IOException e){}

Пример 4 (чтение из канала, через буфер):
    try(FileChannel ch = (FileChannel) Files.newByteChannel(Paths.get("file.txt"))){
        MappedByteBuffer buf = channel.map(FileChannel.MapMode.READ_ONLY,0,26);
        channel.forEach((v,k) -> buf.put(k)); // читаем файл через буфер
    } catch(IOException e){}
    
Пример 5 (копирование файла):
    try {
        Path in = Paths.get("io.txt"), Paths.get("out.txt");
        Files.copy(in, out, StandardCopyOption.REPLACE_EXISTING);
    } catch(IOException e) {}
    
Пример 6 (запись через поток в nio):
    byte[] bs = "Hello".getBytes();
    try(OutputStream os = new BufferOutputStream(Files.newOutputStream(Paths.get("file.txt")))) {
        bs.forEach(v -> os.write(v));
    } catch(IOException e) {}
    
Пример 6 (чтение через поток в nio):
    int content;
    try(InputStream is = new BufferInputStream(Files.newInputStream(Paths.get("file.txt")))) {
        bs.forEach(v -> is.write(v));
        do {
            content = is.read();
            if(content != -1) {
                System.out.print((char)content);
            }
        } while(content != -1); // пока не останется символов в is
    } catch(IOException e) {}
    
Пример 7 (перемещение файла):
    try {
        Path in = Paths.get("io.txt"), Paths.get("out.txt");
        Files.move(in, out, StandardCopyOption.REPLACE_EXISTING);
    } catch(IOException e) {}
    
Пример 8 (свойства файла):
    Files.exists(path);
    Files.isHidden(path);
    Files.isWritable(path);
    Files.isReadable(path);
    BasicFileAttributes fa = Files.readAttributes(path, BasicFileAttributes.class);
    fa.isDirectory();
    fa.isRegularFile();
    fa.isSymbolicLink();
    fa.size();
    fa.lastModifiedTime();
    
Пример 9 (селектор):

Пример 10 (отслеживание событий файловой системы):

---

JNDI (Java Naming and Directory Interface) - это Java API, просто набор интерфейсов для получения данных из БД, properies, xml и т.д.
new InitialContext(props).lookup("*name of resource* (DB)");
--------------------------------
JMX - Java Management Extensions
MBean - Managed Bean
	http://alvinalexander.com/blog/post/java/source-code-java-jmx-hello-world-application
	https://en.wikipedia.org/wiki/Java_Management_Extensions
	
	(потом надо описать?) Различают:
		Standard MBeans
		Dynamic MBeans
		Open MBeans
		Model MBeans
		MXBeans
		
	Как использовать:
	1. Создаем Bean
		class HelloMBean {
			String val;
			//getters, setters here
		}
	2. Регистрируем его в спец. сущности (созданное через фабрику)
		ManagementFactory.getPlatformMBeanServer().registerMBean(
			new HelloMBean(), new ObjectName("FOO:name=HelloBean")
		);
	3. Запускаем нашу программу со спец. командой в jconsole
		java -Dcom.sun.management.jmxremote \
			 -Dcom.sun.management.jmxremote.port=1617 \
			 -Dcom.sun.management.jmxremote.authenticate=false \
			 -Dcom.sun.management.jmxremote.ssl=false \
			 SimpleAgent
	4. Теперь видим значения MBean в специальном веб интерфейсе (и можем менять значения?)
	
	Для чего: для мониторинга программы?
--------------------------------
JavaBean - чтобы DI и др. генераторы и пр. находили сеттеры по соглашению имен.

LDAP (Lightweight Directory Access Protocol) - коротко:
    протокол для любой БД, сама БД видна как набор папок с данными (аналог Map, внутри сделана через дерево)
--------------------------------
userList.sort(Comparator.comparing(User::getName)

// partitioningBy разделяет коллекцию на две части по условию
Map<Boolean, List<Integer>> results=List1.stream().collect(Collectors.partitioningBy( n -> n < 0)); // делит List на 2ва List с ключем true/false по условию

// groupingBy
Map<String, List<Worker>> map1 = workers.stream()
       .collect(Collectors.groupingBy(Worker::getPosition)); // просто группировка
Map<String, Long> map3 = workers.stream() // название позиции (группы) и их количество
       .collect(Collectors.groupingBy(Worker::getPosition, Collectors.counting())); // подсчет для групп
Map<String, Map<Integer, List<Worker>>> collect = workers.stream()
       .collect(Collectors.groupingBy(Worker::getPosition, // название позиции + число лет + сами объекты группы
              Collectors.groupingBy(Worker::getAge))); // группировка по нескольким полям
Map<String, Long> phonesByCompany = phoneStream.collect(
        Collectors.groupingBy(Phone::getCompany, Collectors.counting())); // counting() - считаем элементы в каждой группе
Map<String, Integer> phonesByCompany = phoneStream.collect(
        Collectors.groupingBy(Phone::getCompany, Collectors.summingInt(Phone::getPrice))); // summing() - сумма для каждой группы
Map<String, Optional<Phone>> phonesByCompany = phoneStream.collect(Collectors.groupingBy(Phone::getCompany, 
                Collectors.minBy(Comparator.comparing(Phone::getPrice)))); // minBy() - минимум для каждой группы
--------------------------------
Objects - некоторые методы
	deepEquals(Object a, Object b)
	hash(Object... values) - один hash для нескольких Objects
	isNull(Object obj) / nonNull(obj)
	hashCode(Object o) - вернет 0 для null
	requireNonNull(o) - проверка на null
------------
java 9+
// private и private static методы в interface
interface InterfaceWithPrivateMethods {
	private static String staticPrivate() { return "static private"; }
	private String instancePrivate() { return "instance private"; }
	
	default void check() {
		staticPrivate();
		InterfaceWithPrivateMethods pvt = (new InterfaceWithPrivateMethods() {}).instancePrivate();
	}
}
------------
JSR 199 - Compiler API - служит заглушкой для JIT, есть метода компиляции в native
java.lang.Compiler - удален с jdk21+
------------
Улучшено Process API в новых версиях Java

java.lang.Runtime - средство взамодействия с внешней средой в которой выполняется приложение, возвращает Process и ProcessHandle через кооторые происходит управление
	Runtime getRuntime()
	addShutdownHook(Thread hook)
	availableProcessors()
	Process exec(String[] cmdarray, String[] envp, File dir)
	exit(int status)
	long freeMemory()
	long maxMemory() - макс RAM которое приложение может использовать
	Runtime.Version version() - версия jvm
	
java.lang.ProcessBuilder - с альтернатива Runtime.exec(...) c jdk21
	ProcessBuilder processBuilder = new ProcessBuilder(command, arguments);
	Process process = processBuilder.start();
	
	command() - команды запуска
 	directory()
 	environment()
	redirectError(File file)
	redirectInput(File file)
	redirectOutput(File file)

java.lang.Process - управление процессом
	Stream<ProcessHandle> children()
	Stream<ProcessHandle> descendants()
	void destroy()
	ProcessHandle.Info info()
	int exitValue()
	boolean isAlive()
	CompletableFuture<Process> onExit()
	long pid()
	ProcessHandle toHandle()
	int waitFor(long timeout, TimeUnit unit)
	InputStream getErrorStream()
	InputStream getInputStream()
	OutputStream getOutputStream()
	BufferedReader inputReader(Charset charset)

java.lang.ProcessHandle - управление процессом
	Stream<ProcessHandle> allProcesses()
	Stream<ProcessHandle> children()
	int compareTo(ProcessHandle other)
	ProcessHandle current()
	Stream<ProcessHandle> descendants()
	boolean destroy()
	boolean destroyForcibly()
	ProcessHandle.Info info()
	boolean isAlive()
	static Optional<ProcessHandle> of(long pid)
	CompletableFuture<ProcessHandle> onExit()
	Optional<ProcessHandle> parent()
	long pid()
	boolean supportsNormalTermination()
------------
java.util.logging.Logger - Java Logging https://docs.oracle.com/en/java/javase/23/core/java-logging-overview.html

Logger logger = Logger.getLogger("com.wombat.nose");
logger.fine("doing stuff");
logger.log(Level.WARNING, "trouble sneezing", ex);
Logger.getLogger("com.wombat").setLevel(Level.FINEST); // Changing the Configuration

FileHandler fh = new FileHandler("mylog.txt"); // перенаправление в файл
logger.addHandler(fh);
logger.setLevel(Level.ALL);
logger.info("doing stuff");

Handlers: StreamHandler (OutputStream), ConsoleHandler (System.err), FileHandler, SocketHandler, MemoryHandler - место записи
Formatters: SimpleFormatter, XMLFormatter - формат логов

------------
java.net.URL - создание, парсинг url https://www.baeldung.com/java-url

URL url = new URI("http://baeldung.com/articles?topic=java&version=8").toURL();
url.getPath() // /articles
url.getQuery() // topic=java&amp;version=8
------------
// создавать переменные можно вне блока try-with-resources
BufferedReader reader1 = new BufferedReader(new FileReader("journaldev.txt"));
try (BufferedReader reader2 = reader1) { System.out.println(reader2.readLine()); }
------------
<> можно использовать с anonymous inner class
FooClass<Integer> fc = new FooClass<>(1) { };
------------
Появились private в interfaces, как и в классе доступ к private из него же доступен
interface InterfaceWithPrivateMethods {
    private static String staticPrivate() { return "static private"; }
    private String instancePrivate() { return "instance private"; }
    default void check() {
        String result = staticPrivate();
        InterfaceWithPrivateMethods pvt = new InterfaceWithPrivateMethods() {
            // anonymous class
        };
        result = pvt.instancePrivate();
    }
}}
------------
Появилась утилита jshell для быстрого выполнения code snippets
jshell.exe "This is my long string. I want a part of it".substring(8,19);
------------
java.awt.image.MultiResolutionImage - тип содержит варианты изобрежения для разных разрешений
BufferedImage[] resolutionVariants = ....
MultiResolutionImage bmrImage = new BaseMultiResolutionImage(baseIndex, resolutionVariants);
Image testRVImage = bmrImage.getResolutionVariant(16, 16);
------------
Shutdown Hooks - способы остановки app, их тоже нужно планировать
0 - код нет ошибок, hooks могут зависнуть и не дать закрыться программе
SecurityManager - настройкой security policy можно отключить возможность вызова exit() и halt()
	выкинет SecurityException из checkExit метода

типа остановки
1 controlled - выполнятся hooks, потом остановка, срабатывает в том числе после самоостановки Threads (которые не daemon)
	System.exit(129); - вызывает внутри: Runtime.getRuntime().exit(0)
2 abrupt (halt) - hooks не выполняются, остановка
	Runtime.getRuntime().halt(129);
	
Runtime.getRuntime().addShutdownHook(new Thread(() -> System.out.println("shutdown")));
Runtime.getRuntime().removeShutdownHook(willNotRun); // удаление
------------
reactive in java - суть: поток не ждет ответа от других потоков (DB, rest), операция добавляется в очередь, которая обрабатывается в 1+ потоке в loop (основан на event loop патерне и observer)
	1 уменьшаем количество потоков - модель 1 запрос - 1 поток
	2 потоки не blocked
	3 back pressure - consumer может сообщить producer чтобы отправлял меньше событий
Reactive Manifesto - набор рекомендаций по реализации reactive, не lib а всей системы (e.g. микросервисов)
	Responsive - система должна быстро работать
	Resilient - система должно оставаться доступна в случае ошибок
	Elastic - должна маштабироваться (разнесение по pods в kuber)
	Message-Driven - обмениваться сообщениями (kafka)

Реализации Reactive Streams - общая спецификация для разных языков:
1 Reactive Extensions оно же RxJava - на данный момент менее развито чем Project Reactor и имеет отличия в поведении
2 Project Reactor - обычно используют это, в том числе в Spring
3 Flow API - встроенно в java, но не развитое для нормального использования, но используется в некоторых libs
------------
Flow API - новое api, аналог Reactive Stream API (реактивное), на сегодня лучше использовать Rx, т.к. это малоразвито
			- др. библиотеки (reactive database, network etc) могут использовать это api внутри реализаций
java.util.concurrent.Flow // содержит все классы api
java.util.concurrent.Flow.Publisher
java.util.concurrent.Flow.Subscriber - методы: onSubscribe, onNext, onError, onComplete
java.util.concurrent.Flow.Subscription
java.util.concurrent.Flow.Processor // трасформирует сообщение, использование - наследовать и переопределить метод

// создаем Subscriber - слушает события
class MySubscriber implements Subscriber<Employee> {
	private Subscription subscription;
	
	@Override
	public void onSubscribe(Subscription subscription) {
		System.out.println("Subscribed");
		this.subscription = subscription;
		this.subscription.request(1); //requesting data from publisher
		System.out.println("onSubscribe requested 1 item");
	}

	@Override
	public void onNext(Employee item) {
		System.out.println("Processing Employee "+item);
		this.subscription.request(1);
	}

	@Override
	public void onError(Throwable e) { System.out.println("Some error happened"); }

	@Override
	public void onComplete() { System.out.println("All Processing Done"); }
}

// использование
import java.util.concurrent.SubmissionPublisher;
SubmissionPublisher<Employee> publisher = new SubmissionPublisher<>();
publisher.subscribe(new MySubscriber()); // подключаем подписчика
publisher.submit(i); // вызываем событие
publisher.close();
subscription.cancel(); // можем отменить подписку

// трансформация сообщений через Processor из Employee в Freelancer
class MyProcessor extends SubmissionPublisher<Freelancer> implements Processor<Employee, Freelancer> {
	private Subscription subscription;
	private Function<Employee,Freelancer> function;
	
	public MyProcessor(Function<Employee,Freelancer> function) { super(); this.function = function; }  
	
	@Override
	public void onSubscribe(Subscription subscription) {
		this.subscription = subscription;
		subscription.request(1);
	}

	@Override
	public void onNext(Employee emp) {
		submit((Freelancer) function.apply(emp));  
	    subscription.request(1);  
	}

	@Override
	public void onError(Throwable e) { e.printStackTrace(); }

	@Override
	public void onComplete() { System.out.println("Done"); }
}
------------
Vector/SIMD (Single Instruction Multiple Data) - замена JIT при компиляции операций с массивами на SIMD операции - циклы на работы с массивами одной командой cpu
UseSuperWord - флаг jdk6+
------------
Vector API - экспериментальное api использует SIMD (Single Instruction Multiple Data) операции CPU для больших данных - одна команда вместо нескольких при работе с массивами
int[] result = IntVector.fromArray(SPECIES, arr1, 0).add(IntVector.fromArray(SPECIES, arr2, 0)); - одной командой вместо цикла
------------
JVM command line utilities

* java (Java Runtime Environment)
* javac (Java Compiler)
* javap (Java Class File Disassembler)
* jdeps (Java Class Dependency Analyzer)
* jlink (Java Linker)
* jshell (Interactive Java Shell)
* jar (Java Archive Tool)
* jarsigner (Jar Signing and Verification Tool)
* jps (Java Virtual Machine Process Status Tool)
* jstat (Java Virtual Machine Statistics Monitoring Tool)
* jinfo (Java Configuration Information Tool)
* jmap (Java Memory Map Tool)
* jhat (Java Heap Analysis Tool)
* jrunscript (Java Scripting Tool)
* jconsole (Java Monitoring and Management Console)
* jcmd (Java Command Tool)
  * Source: [oracle](https://docs.oracle.com/javase/8/docs/technotes/guides/troubleshoot/tooldescr006.html)
* jjs (Java JavaScript Tool)
* jdb (Java Debugger)
* **jvisualvm** (Java VisualVM - Java Profiling and Monitoring Tool)
  * Source: [visualvm](https://visualvm.github.io/)
* **jmc** (Java Mission Control)
  * Source: [wiki](https://en.wikipedia.org/wiki/JDK_Mission_Control)
* **keytool** (Key and Certificate Management Tool)
* **deprecated**
  * **jstack** (Java Thread Stack Analysis Tool) - use **jcmd**
------------
Stacktrace
Source: [habr 1](https://habr.com/ru/companies/jugru/articles/324932/), [habr 2](https://habr.com/ru/companies/jugru/articles/325064/), [baeldung StackWalker](https://www.baeldung.com/java-9-stackwalking-api), [baeldung Current Stack Trace](https://www.baeldung.com/java-get-current-stack-trace), [baeldung Diagnosing a Running JVM](https://www.baeldung.com/running-jvm-diagnose)

LIFO - структура, начало программы внизу

в любой точке: StackTraceElement[] stackTraceElements =Thread.currentThread().getStackTrace()
exceptions: e.getStackTrace();
------------
Heap dump - файл формата hprof, открыть в visualvm или jprofiler
Как читать: анализ утечек памяти и нагрузки когда создается много обьектов, смотрим classes обьектов
Чем открыть: eclipse memory analyzer tool (mat) - показывает подозрительные места
Когда снимать: с разных pods, несколько раз, чтобы точно попасть в пик нагрузки на RAM

снять:
1 jmap -dump:live,format=b,file=/tmp/dump.hprof 12587
	- live - снять только те которые не marked для очистки
2 jcmd 12587 GC.heap_dump /tmp/dump.hprof
3 java -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=<file-or-dir-path>
4 в idea в debug в меню memory
5 через JMX bean подключившись через JConsole или др.
	HotSpotDiagnostic > Operations > dumpHeap
6 через JMX - Programmatic
	MBeanServer server = ManagementFactory.getPlatformMBeanServer();
	HotSpotDiagnosticMXBean mxBean = ManagementFactory.newPlatformMXBeanProxy(server, "com.sun.management:type=HotSpotDiagnostic", HotSpotDiagnosticMXBean.class);
	mxBean.dumpHeap(filePath, live);
------------
Thread dump - какие потоки созданы, каким процессом заняты, сколько времени
Что показывает: Name, Priority, ID, Status, callstack

снять:
1 ps -eaf | grep java ИЛИ jps - найти pid
2 jstack PID >> mydumps.tdump
3 jcmd PID Thread.print
4 Java Mission Control (JMC), jvisualvm, jconsole
5 -XX:+UnlockDiagnosticVMOptions -XX:+LogVMOutput -XX:LogFile=~/jvm.log && kill -3 17264 - с параметром -3 и получаем thread dump
6 через JMX - Programmatic
    StringBuffer threadDump = new StringBuffer(System.lineSeparator());
    ThreadMXBean threadMXBean = ManagementFactory.getThreadMXBean();
    for(ThreadInfo threadInfo : threadMXBean.dumpAllThreads(lockedMonitors, lockedSynchronizers)) {
        threadDump.append(threadInfo.toString());
    }
    return threadDump.toString();
	
анализ проблем с потоками:
1 Synchronization Issues
	- RUNNABLE, BLOCKED, TIMED_WAITING - для поиска deadlock или thread contention (когда thread долго ждет пока другие thread завершаться)
2 Execution Issues
	- RUNNABLE - ищем если много нагрузки на CPU
	- BLOCKED - ищем если медленно работает приложение
3 Recommendations
	- снимать несколько thread dump каждые несколько мин, например 5 каждые 3 сек
	- давать имена threads
	- искать в основном BLOCKED или с большим временем жизни
	- top -H -p PID - найти связь потока и ядра CPU
	- некоторые threads берутся из thread pool, поэтому время жизни большое даже если все ок
------------
JEP408: Simple Web Server - встроенный простой http server
var server = SimpleFileServer.createFileServer(new InetSocketAddress(8080),
		server.createContext("/custom", new MyHttpHandler());
		Path.of("/<absolute path>/MyJava21Planet/httpserver"),
		SimpleFileServer.OutputLevel.VERBOSE);
server.start();

// custom response
class MyHttpHandler implements com.sun.net.httpserver.HttpHandler {
    @Override
    public void handle(HttpExchange exchange) throws IOException {
        if ("GET".equals(exchange.getRequestMethod())) {
            OutputStream outputStream = exchange.getResponseBody();
            String response = "It works!";
            exchange.sendResponseHeaders(200, response.length());
            outputStream.write(response.getBytes());
            outputStream.flush();
            outputStream.close();
        }
    }
}

curl http://localhost:8080

------------
JEP413: Code Snippets - в документации в комментах указываем примеры кода

пример кода @code внутри комментария:
/**
 * <pre>{@code
 *    if (success) {
 *        System.out.println("This is a success!");
 * </pre>
 * @param success
 */
 
inline:
/**
 * {@snippet :
 *    if (success) {
 *        System.out.println("This is a success!");
 * }
 * @param success
 */

внешний файл с кодом:
// 1. создаем пакет snippet-files
com.mydeveloperplanet.myjava21planet.snippet-files

// 2. указываем java файл с примером
/**
 * {@snippet file="SnippetsExternal.java" region="example3" }"
 */
public void example3(boolean success) {

// 3. пример кода
public class SnippetsExternal {
    public void example3(boolean success) {
        // @start region=example3
        if (success) {
            System.out.println("This is a success!");
        } else {
            System.out.println("This is a failure");
        }
        // @end
    }
}

javadoc src/com/mydeveloperplanet/myjava21planet/Snippets.java -d javadoc
------------
Core Dump (crash dump) - java код не может создать core dump, только Java Native Interface (JNI) код вызываемый из java может
------------
XML

Source: [mkyong Java XML Tutorial](https://mkyong.com/tutorials/java-xml-tutorials/), [baeldung XML Libraries Support in Java](https://www.baeldung.com/java-xml-libraries), [tutorialspoint Java XML](https://www.tutorialspoint.com/java_xml/java_xml_quick_guide.htm), [habr](https://habr.com/ru/articles/339716/)

* SAX (Simple API for XML)
* StAX (Streaming API for XML)
* DOM (Document Object Model)
* JAXB (Java Architecture for XML Binding)
---
SOAP, JAX-WS (Java API for XML Web Services), Spring WS

contract-first - сначала создается contract (wsdl), и по ней генерируются Java классы сообщения
contact-last - сначало создаются Java классы сообщений, а по ним генерируется contract (wsdl) через утилиты (напр. wsimport)

Утилиты:
1. schemagen - генерирует xsd по Java классам
2. wsgen - передаем endpoints, она генерирует artifacts (классы?) для сервисов, чтобы их можно было вызвать на клиенте. Она не нужна, если используется Endpoint.publish(), которая сама генерирует WSDL/schema. С флагом -wsdl может генерировать WSDL/schema.
3. wsimport - генерирует классы по WSDL
4. xjc - генерирует классы по xsd

как работать с wsdl (Web Services Description Language)
1. генерим wsdl контракт по xsd - wsimport -s . -p com.baeldung.jaxws.server.topdown employeeservicetopdown.wsdl
	При генерации Java object из wsdl через wsimport имя пакета будет как пространство имен наоборот из wsdl.
	wsdl обычно в WEB-INF/wsdl
1.2 генерируем классы по xml
2. создать SOAP клиент (напр. Spring WS)
3. при изменении сообщения редактирует wsdl
------------
Java Agent
Source: [baeldung Java Instrumentation](https://www.baeldung.com/java-instrumentation), [habr](https://habr.com/ru/articles/230239/)

JDK 21+ - dynamic loading вызывает warning, т.к. поддержка dynamic loading планируется к удалению
------------
Constant Pool, Autoboxing Caches, String Intern Pool

В Thread использования Autoboxing Caches и String Intern Pool может вызвать проблемы безопасности.
Для паролей в доке java рекомендуется использовать char[] вместо String, чтобы не было значений в String pool

1 Class File Constant Pool - хранит литералы и ссылки в class
	посмотреть пулл: javap -v name.class
	место хранения: в heap
	размер: 65535 записей (16 бит), изменить размер нельзя
	типы: CONSTANT_Integer, CONSTANT_Float, CONSTANT_Long, CONSTANT_Double, CONSTANT_String, CONSTANT_Class, CONSTANT_Fieldref, CONSTANT_Methodref, CONSTANT_InterfaceMethodref, CONSTANT_NameAndType, CONSTANT_MethodHandle, CONSTANT_MethodType, CONSTANT_InvokeDynamic, CONSTANT_Dynamic, CONSTANT_Module, CONSTANT_Package
	пример: class A { static Integer i = 1; }
2 Runtime Constant Pool - это runtime представления в памяти пулла Class File Constant Pool после загрузки
3 Constant Dynamic Pool - данные в него загружаются во время запуска для ОТЛОЖЕННЫХ (вычисляемых) констант
	место хранения: в heap объекты, часть Constant Pool, расположен в Metaspace (Method Area)
	размер: 65535, т.к. часть Constant Pool, изменить размер нельзя
	пример: Использование лямбда-выражения приводит к генерации записи типа CONSTANT_Dynamic
4 String Intern Pool - хранит только String, попадают при создании String литералов или вызове new String("").intern(), испольщует хэш-таблицу
	место хранения: в heap
	размер: -XX:StringTableSize=<размер>
	пример: String s = "s";
5 Autoboxing Caches - в heap, кэш оберток immutable объектов примитивных типов
	место хранения: хранится в static переменных оберток (Integer, Long etc), в heap хранится кэшированные объекты, в Metaspace инфа о class (ссылках?)
	типы:
		1 Integer Cache, размер -128 до 127, изменить -XX:AutoBoxCacheMax=<значение>
		2 Long Cache, размер -128 до 127, изменить нельзя
		3 Byte, Short и Character Cache, размер -128 до 127, для Character от \u0000 до \u007F (но может варьироваться), изменить нельзя
		4 Float и Double - не кэшируются
	пример: Integer third = 4;

Отдельно:
1 BigInteger cache - кэш -16 до 16, размер не меняется, BigInteger.valueOf(0) == BigInteger.valueOf(0) == true
2 BigDecimal cache - кэш 0 до 10 при scale=0 (только целых), размер не меняется, BigDecimal.valueOf(0L) == BigDecimal.valueOf(0L) == true
------------
ClassLoader - loading the class definition into runtime, вызывает рекурсивно loader вверх по дереву классов
суть: грузит классы в runtime, можно делать кастомные для поиска в других местах, можно грузить классы-драйверы во время работы jvm

java.lang.ClassLoader.loadClass() - загружает класс
java.net.URLClassLoader.findClass() - вызовется если класс не найден рекурсивно, ищет в file system
getParent() - parent ClassLoader
URL getResource(String name) - из class path
ContextClassLoader getContextClassLoader() - получаем loader который грузит class в конкретный thread

JNDI provider - могут иметь альтернативные реализации в rt.jar используются например в серверах приложений для загрузки классов

system class loader - из -classpath (-cp)
extension class loader - расширенный, из $ JAVA_HOME / lib / ext или свойства java.ext.dirs
bootstrap class loader - базовый, написан на байткоде, обычно грузит классы jre напр. $ JAVA_HOME / jre / lib и rt.jar

children class loaders are visible to classes loaded by their parent class loaders

public class CustomClassLoader extends ClassLoader { // кастомное место поиска класса
    @Override
    public Class findClass(String name) throws ClassNotFoundException {
        byte[] b = loadClassFromFile(name);
        return defineClass(name, b, 0, b.length);
    }
}
------------
GC

jdk 21+
	удалены опции UseBiasedLocking..., G1ConcRefinement...
	G1 region теперь может быть 512мб (раньше был 32мю), это может уменьшить фрагментацию и потребление памяти на 1.5%
	G1 использует только одну mark bitmap вместо 2х

суть: есть 3 области памяти, новые объекты перемещаются между 2мя первыми и на каждой итерации удаляются вышедшие за scope,
после порога-значения объекты переходят в old space где очистка будет реже
4ая область - metaspace - для static переменных
stack - не gc область, это scope методов

1 System.gc(), Runtime.gc() - запускают gc, но не гарантируют что он запустится
2 memory leak - в java когда есть strong references и память не удаляется
3 ThreadLocal - может быть причиной memory leak
4 gc - daemon thread
5 object.finalize() - вызывается перед очисткой, без гарантий что вызовется сразу
6 gc - запускается автоматически на основе heap size
7 java.lang.OutOfMemoryError - если памяти не будет
8 java7+ String Pool хранится в heap, по умолчанию размер 65536, чем больше размер тем меньше времени на добавление string в pool
9 java9+ Compact Strings - string хранятся как char[] или byte[] в зависимости от 2 байт utf-16 или 1 байт
10 java.lang.StackOverFlowError - когда переполнен стэк, например много recursive вызовов метода, увеличивается опцией -Xss

* **GC**
  * Serial - все в 1 поток, останавливает jvm чтобы почистить
  * Parallel - как Serial но использует несколько threads, плюс умнее алгоритм
  * G1 (Garbage-First) - default, как Parallel, но много маленьких heap
  * Z (GenZGC) - задержки не больше 10ms, нужно больше ресурсов, сложный алгоритм очистки, с jdk21+ появились young/old регионы памяти

когда object может быть очищен:
1 object = null или parent object = null
2 ссылка object вышла за scope
3 если есть только weak reference на object

какой gc выбрать:
1 Serial - мало ресурсов и не критичны тормоза
2 g1 - by default сбалансирован, но можно настраивать между паузами и пропускной способностью, с большими heap (Корпоративные приложения, системы управления ресурсами (ERP))
	- обработка данных в реальном времени с большими данными, минимальные паузы и предсказуемое время отклика (пропускная способность ниже чем у parallel gc)
3 z - если нужны малые задержки, и есть много ресурсов (Системы высокочастотной торговли)
	- heap больше чем у g1, пропускная способность ниже чем у parallel gc, паузы ниже чем у g1 (меньше 10ms)
	- паузы не зависят от размера heap т.к. очистка многопоточная, но задать время пауз явно нельзя
4 parallel gc - Подходит для многопоточных серверных приложений, где важна высокая пропускная способность и минимальное время сборки
	- сервеные системы обработки транзакций с большой пропускной способностью, ценой пауз (куча меньше чем у g1)
5 Shenandoah GC - аналогично z низкие задержки, но может тратить меньше heap за счет частых очисток/дефрагментаций и старым железом т.к. не использует спец команд cpu, но тратит больше cpu
	- как z но задержки даже меньше, но max паузы можно настроить -XX:shenandoahGuaranteedPauseTime что важно для real-time приложений
	- в отличии от z не так эффективно работает с огромными объемами памяти, паузы зависят от размера heap и частоты создания/удаления объектов в heap
6 остальные gc - экспериментальны или устаревшие

ZGC vs Shenandoah
	ZGC - когда heap огромный и время паузы не должно зависеть от размера heap. Если много долгоживущих объектов.
	Shenandoah - для real time приложений когда нужно гарантировать время паузы через параметр. При большой фрагментация у него лучше стабильность пауз. Если короткоживущие объекты часто создаются и их нужно чистить/дефрагментировать.

какие опции применять чаще:
1 -XX:ThreadStackSize или -Xss - увеличить размер стэка чтобы не было падений при recursive методах
	- можно ставить размер стэка потоку Thread(ThreadGroup, target, name, stackSize)
2 -Xms2G - увеличить heap чтобы не было OutOfMemoryError если приложение тяжелое
3 -XX:+HeapDumpOnOutOfMemoryError - снимать дамп при OutOfMemoryError
	- HeapDumpBeforeFullGC, HeapDumpAfterFullGC, ExitOnOutOfMemoryError, CrashOnOutOfMemoryError
4 -XX:NewSize - увеличить young space если создается много коротко живущих обьектов
5 -XX:MetaspaceSize - увеличить Metaspace если много static переменных и методов
6 удаленный дебаг при проблемах на сервере: -Xdebug -Xnoagent -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8000
7 -XX:-OmitStackTraceInFastThrow - отключить пропуск stacktrace если много однотипных Exception,  (для NullPointerException, ArrayIndexOutOfBoundsException, ArithmeticException, ArrayStoreException, ClassCastException)
	- fillInStackTrace - заполняет stacktrace при создании Exception и он в 150 раз медленнее чем создать обычных обьект
	- все равно рекомендуется включить иначе анализировать проблемы будет трудно
8 -XX:+PrintConcurrentLocks - для ReentrantLock выводить в thread dump инфу о том кто захватил lock (иначе не покажет)

память:
heap
 young (survivor 1, 2) - обычно 100-500MB, где создаются, если долго живут то переходят в old, 2 области survivor чтобы не было фрагментации
 old (tenured) - намного больше чем young, когда полно запускается gc
metadata (permgen) - 64MB (32bit) или 82MB (64bit), static methods, static object references, primitive variables

области памяти Run-Time Data Areas:
Shared Data Areas - общая для threads, создается при запуске jvm
	Heap - в зависимости от реализации может содержать Method Area
	Method Area - class and interface definitions (constructors, methods), выбирается авто размер, если не хватает памяти - OutOfMemory
	Run-Time Constant Pool - часть Method Area, interface names, field names, and method names, если не хватает памяти - OutOfMemory
Per-thread Data Areas - у каждой thread своя, разрушаются при завершении threads
	PC Register (program counter) - адрес исполняемой инструкции, для native method == undefined
	JVM Stack - стэк методов, может быть частью любой области, используется для создания stack trace, StackOverflow - если переполнен
	Native Method Stack - как JVM Stack, но для native методов

* Mark - mark unreachable objects
* Sweep - clear unreachable objects
* stop-the-world - process of clearing, no other jvm operation happens, starts when old generation full
* overhead - time of GC work
* roots - objects from wich gc stars to search unreachable objects and mark them
* young (Eden) space - where new objects created, 100-500MB usual size, survivor spaces used against fragmentation
  * survivor 1
  * survivor 2
* old space - larger than young space, gc starts when old space is full

* по каким параметрам выбирать gc
  * Heap Size
  * young generation space - part of Heap Size
  * CPUs - what gc type to choose
  * Pause Time - Pause of gc to clear memory, high pause == high latency
  * Throughput - time of app work, lower overhead time == higher Throughput
  * Memory Footprint - memory of GC process
  * Promptness - time after what dead objects cleared (memory reclaimed), larger the heap size == lower the promptness
  * Latency - low GC pauses == low Latency
* Deprecated
  * Concurrent Mark Sweep (CMS) - unpredictable pauses, more CPU, for heap<4GB, replaced by G1
* Experimental
  * Shenandoah
  * Epsilon - “no-op” collector, no memory clearing, no impact of a gc
---
как работают алгоритмы разных gc

1 Serial GC - однопоточный gc, для малопоточных приложений и мелких heap
	Для чего: небольшие утилиты
	Этапы работы:

	Параметры настройки:
		1 -XX:+UseSerialGC
		1 -XX:NewRatio
		2 -XX:SurvivorRatio
2 Parallel GC (Throughput Collector) - многопоточный gc, использует STW, сборка происходит в несколько потоков, оптимизирован для throughput 
	Для чего: где нужна большая throughput, например много транзакций или вычислений, пакетная обработка данных
	Этапы работы:

	Параметры настройки:
		1 -XX:+UseParallelGC - включает
		2 -XX:ParallelGCThreads=<N> - число потоков для параллельной сборки
		3 -XX:MaxGCPauseMillis=<time> - max время пауз, для настройки throughput и latency
		4 -XX:GCTimeRatio=<ratio> - отношение времени работы сборщика gc и времени работы приложения, чтобы настроить баланс
3 G1 GC (Garbage-First Collector) - heap разбивается на регионы, первыми чистятся регионы с наибольшим количеством мусора, это дает баланс между throughput и latency (pause time)
	Для чего: многопоточные серверные приложения
	Этапы работы:

	Параметры настройки:
		1 -XX:+UseG1GC
		2 -XX:MaxGCPauseMillis=<time> - max время паузы, e.g. системы онлайн торговли
		3 -XX:G1HeapRegionSize=<size> - размер регионов, влияет на их количество, если heap большая то можно увеличить размер регионов чтобы снизить нагрузку на cpu
		4 -XX:InitiatingHeapOccupancyPercent=<value> - процент заполнения кучи при котором запускается многопоточное маркирование
		5 -XX:G1NewSizePercent и -XX:G1MaxNewSizePercent - для оптимизации частоты minor GC
4 ZGC - gc потребляем больше heap, но делает очень короткие паузы. Время пауз не зависит от размера heap.
	Использует новые спец команды cpu для Colored Pointers (пометки неиспользуемых объектов через метаданные в значениях ссылок) и Load Barriers и возможности чтения данных даже во время их перемещения - минимальные паузы приложения.
	Маркирование и Релокация выполняются без STW приложения. Инкрементальная Компактизация (последовательная дефрагментация объектов пр очистке) происходят на лету.
	Паузы очень короткие, меньеш 10мс не зависимо от размера heap. Размер heap эффективно очищается даже при частой аллокации объектов.
	Для работы требует 64бит cpu с поддержкой команд colored pointers и Load Barriers.
	Этапы работы:

	Особенности:
		1 Маштабируемость - поддержка больших heap.
		2 Минимальные паузы - параллельная с работой приложения очистка gc.
		3 Низкая фрагментация - постоянная и частая очистка и дефрагментация heap без пауз приложения (параллельная очистка).
	Когда выбрать:
		1 Гиганские heap (десятки гигабайт и более).
		2 Для финансовых, облачных с миллиардами транзакций, для коротких паузы (ниже 10 мс) если это надо по SLA.
		3 Если cpu поддерживает colored pointers и load barriers.
	Время пауз:
		1 2–5 мс - обычно
		2 2–10 мс - при высокой аллокации
		3 <10 мс - Гигантские кучи
		4 Фрагментация - не влияет на паузы
	Параметры настройки:
		1 -XX:+UseZGC - включить
		2 -XX:ZUncommitDelay=<time> - uncommit delay задержка при возврате неиспользуемой памяти
			- для систем с динамической нагрузкой (циклической, не постоянной), где нужно оперативное освобождение неиспользуемой RAM
			- обычно 120 секунд, уменьшение времени освобождения до 5–10 сек полезно для переменной нагрузки, но уменьшит производительность если память востребована постоянно.
		3 -XX:ZFragmentationLimit=30 - max допустимый уровень фрагментации до запуска дефрагментации/очистки
			- 50 % - обычное значение (может меняться от реализации), уменьшение до 30% уменьшит latency и фрагментацию, но повысит нагрузку на CPU.
			- для систем с частой (де)аллокацией объектов чтобы всегда был маленький heap - можно уменьшить чтобы лучше чистить heap (улучшить компактизацию)
		4 -XX:ZAllocationSpikeTolerance=1000 - пик нагрузки на heap влияет на то когда запускать очистку (если у системы нерегулярные пики нагрузки на heap)
			- 1000 - обычное значение, значение выше допускает большие скачки аллокации без запуска очистки, например чтобы не запускать при коротковременных пиках
			- для систем с редкими нерегулярными пиками можно увеличить значение, уменьшение значения заставит чистить при любых мелких пиках (что плохо)
		5 -XX:ZMarkingThreads=<n> - количество потоков для многопоточной маркировки, обычно - равно числу ядер
		6 -XX:ZCollectionInterval=<time> - время между циклами сборки мусора, обычно - авто
5 Shenandoah GC - низкая предсказуемая задержка, не использует Load Barriers команды cpu, расходует больше cpu. Постоянно перемещает объекты в компактные регионы.
	Маркирование и Эвакуация (копирование) - понятия пометки и перемещения живых объектов в компаскные регионы.
	нкрементальная и Конкурентная Компактизация - компактизация и перемещение объектов выполняется короткими порциями (этапами) - дает очень низкие паузы при работе приложения.
	Постоянно параллельно дефрагментирует и чистит heap - подходит когда создается много короткоживущих объектов.
	Частота STW событий низкая.
	Этапы работы:

	Особенности:
		1 Предсказуемость пауз - паузы стабильны, их max время можно задать параметром и обменять на cpu. Паузы в среднем немного короче чем у Z.
		2 Агрессивная компактизация - постоянно дефрагментирует heap.
		3 Не зависит от cpu - может работать на старых cpu.
		4 Меньше throughput чем у Z, т.к. больше используется cpu.
	Когда выбрать:
		1 real-time приложения - с предсказуемыми паузами (ниже чем у z и не превышающими выбранное в настройках время) - онлайн-игры, системы высокочастотной торговли, интерактивные сервисы, но в обмен на cpu
		2 если создается много короткоживущих объектов - они будут быстро чиститься/дефрагментироваться
		3 поддерживает старые cpu
	Время пауз:
		1 1–2 мс - обычно
		2 1–10 мс - с гарантией через -XX:shenandoahGuaranteedPauseTime
		3 случайное - Гигантские кучи, паузы могут меняться, но ограничены -XX:shenandoahGuaranteedPauseTime
		4 Фрагментация - влияет на паузы
	Параметры настройки:
		1 -XX:+UseShenandoahGC - включить
		2 -XX:shenandoahMinEvacuationInterval=<time> - интервал между эвакуацией (очисткой/копированием выживших объектов) - для систем с частым созданием объектов
			- 50ms - обычно, уменьшение эвакуации до 20мс снизит фрагментацию, но повысит нагрузку CPU и увеличит количество пауз, 100ms - уменьшит нагрузку, но увеличит фрагментацию
			- если создание объектов частое, то уменьшить для частой очистки, если создание объектов редкое - увеличить для уменьшения нагрузки CPU
		3 -XX:shenandoahGuaranteedPauseTime=<time> - max гарантированное время паузы (для real-time систем)
			- 10 мс - обычное значение, max время остановки, снижение гарантирует короткие паузы ценой CPU overhead, высокое значение - ниже нагрузка cpu, но выше паузы
			- если система real time то ставить 10мс или ниже, если не критичная система - ставить 20-30мс
		4 -XX:ShenandoahNumWorkerThreads=4 - фоновые потоки для эвакуации и компакцизации, обычно - половина числа ядер системы
		5 доп. параметры для количества фоновых потоков - зависит от реализации Shenandoah GC	
---
JVM parameters https://www.baeldung.com/jvm-parameters
инфа:
-XX:+PrintFlagsFinal - опции jvm
-XX:+PrintStringTableStatistics - инфа string pool
-XX:StringTableSize=4901 - размер string pool
-XX:+HeapDumpOnOutOfMemoryError - делать heap dump при 
-XX:HeapDumpPath=./java_pid<pid>.hprof
-XX:OnOutOfMemoryError="< cmd args >;< cmd args >"
-XX:+UseGCOverheadLimit - время VM в gc после до OutOfMemory thrown
-XX:OnOutOfMemoryError="shutdown -r" - команда после проблемы
-XX:-TraceClassLoading - показывать загруженные classes
-XX:+TraceClassUnloading
-XX:-PrintCompilation
jvm
-d32bit - режим 32
оптимизации:
-XX:+UseStringDeduplication
-XX:+UseLWPSynchronization
-XX:LargePageSizeInBytes
-XX:+UseLargePages
-XX:+UseStringCache
-XX:+UseCompressedStrings
-XX:+OptimizeStringConcat
-XX:+UseBiasedLocking
-XX:CompileThreshold=10000
-XX:+AggressiveOpts
gc log
-XX:+UseGCLogFileRotation
-XX:NumberOfGCLogFiles=10
-XX:GCLogFileSize=50M
-Xloggc:/home/user/log/gc.log
-XX:+PrintGCTimeStamps - временные метки сборки gc
-XX:+PrintGCDateStamps
-verbose:gc
-XX:+PrintGCDetails
-XX:+PrintTenuringDistribution
-XX:-PrintAdaptiveSizePolicy - инфа по young space
gc type
-XX:+UseSerialGC
-XX:+UseParallelGC
-XX:+UseG1GC
-XX:+UseZGC
stack
-Xss - размер стэка, увеличить если много рекурсивных вызовов
young
-XX:NewSize=<young size>[unit] - 2228k
-XX:MaxNewSize=<young size>[unit] - Not limited
-XX:SurvivorRatio
old
-XX:OldSize - размер old
heap
-Xms2G
-Xmx5G
-XX:MinHeapFreeRatio
-XX:MaxHeapFreeRatio
metacpace:
-XX:MetaspaceSize=[size] 
-XX:MaxMetaspaceSize=[size] 
-XX:MinMetaspaceFreeRatio=1G
-XX:MaxMetaspaceFreeRatio=1G
-XX:AutoBoxCacheMax=NEWVALUE - размер пула
-Djava.lang.Integer.IntegerCache.high=NEWVALUE
--------------------------------
Модульность jigsaw
Модуль - группа пакетов и/или ресурсов. Сама jdk разделена на модули
Цель: избавиться от dependencies hell (дублирования либ), уменьшить размер

1. В корне пакетов создается файл module-info.java
2. В module-info.java описываются какие пакеты можно export в других модули (другие пакеты других модулей)
3. Если нужно получить доступ к классам пакета в другом module, то нужно в module-info.java текущего пакета импортировать его
4. Если requires не обьявлен, то класс нельзя сделать import никак, даже если он public
5. (неточно) Если модуль не создан, то пакет считается в Unnamed Module, и они работают с классами как обычно (для миграции)
6. Зацикленные зависимости сделать нельзя (в отличии от классов или packages)

package module com.baeldung.student.model
public class Student { }
// module-info.java
module com.baeldung.student.model {
    exports com.baeldung.student.model;
}
package com.baeldung.student.service
module com.baeldung.student.service {
    requires transitive com.baeldung.student.model; // теперь Student и все классы пакета, но не sub packages доступны для использования
	opens com.app; // открывает доступ к private/protected/package полям для reflection api, не поможет и setAccessable(true)
}
---------------
Possible Root Causes for High CPU Usage in Java
https://www.baeldung.com/java-high-cpu-usage-causes
---------------
Memory Leaks
Source: https://www.baeldung.com/java-memory-leaks, https://topjava.ru/blog/java-memory-leaks
---------------
High Load - что делать для анализа проблем и высокой нагрузки

поиск причин:
1. смотреть на график GC
2. снять thread dump и heap dump - снимать через периоды времени с разных pods
3. trace - jaeger ui, для интеграций добавлять span вручную
4. логи - переключаются на лету, смотреть в ELK, включать отдельные пакеты, отключить скрытие stack trace для некоторых Exception
5. снимать jfr который постоянно работает в фоне
6. postgres - таблица pg_active или др - смотреть active (долгие запросы) и idle in transaction (не во время закрытые транзакции висящие из-за кода)
	вывести топ запросов и пр в grafana
7. grafana - вывести cpu, ram, postgres, счетчики интеграций, счетчики rest
8. локально при разработке - нагрузку имитировать отправляя сообщения через jemeter по размеру, частоте, паралельным запросам и их количеству
9. нагрузочное тестирование - заполняется база валидными данными, разными средствами имитируется нагрузка
10. деструктивное тестирование - проверка поведение в экстремальных условиях, например при рестарте pods (тест перезагружает pod), проверка скорости старта и пр

как сделать систему устойчивую под нагрузкой:
Scalability and Statelessness
	1 stateless - приложения легче масштабировать
Database Optimization
	1 разделить базу на несколько - например в рамках разделения на сервисы или паттерн read model
	2 смотреть метрики grafana, вывести туда в том числе метрики postgres - топ запросов и idle in transaction
	3 Guava RateLimiter - чтобы убрать DDoS
	4 read model - отдельную базу для чтения, напр nosql, или clickhouse если нужны аналитические запросы или др
Hardware and Load Balancing
	1 load balancer - использовать hardware или software-based - внутри кластера (которые автоматически настраивается) и вне который round robin
	2 load balancer - использовать и для базы, например patroni
	2 partitions - 
	3 I/O и CPU - опеределить правильные требования, например через HT
Partitioning and Batch Updates
	1 Partition - парциционирование базы postgres, разделение по категориям записей таблицы
	2 асинхронное логирование и очередь для history таблиц
Caching
	1 распределенный кэш - т.к. только он сохранит консистентность для pods
	2 стратегии обновления/сброса кэша - если данные обновляются их кэш сбрасывается, e.g. как часть транзакции или в др время
		Facebook подход:
			кэш промах чтения - записываем в кэш и возвращаем
			запись - пишем в базу данных кэш удаляем (не обновляем)
	3 достаточно кэшировать 20% данных, к которым обращаются в 80% случаев — закон Парето
	
message broker (kafka)
1. replication factor - количество копий сообщений на broker, обычно 3 - баланс надежности и скорости
1.2 retention policy - время жизни сообщений 
1.3 транзакционность - политики exactly once или at most once для отправки от broker, producer, ack от consumer
2. выбор количества парциций - размер сообщения * скорость обработки, ставим количество consumer и partiotions чтобы скорость была нужной
3. выбор количества pods в kuber
	3.1 ставим 1 pod и нагружаем до 100% cpu & ram (saturation metric)
	3.2 поставить количество pods и ресурсов чтобы нагрузка была ниже 70% на случай пиков - operative range
	3.3 настроить HPA - авто добавление под в кубере, ставить минимум 2 поды в простое
4. настроить количество segments (частей парциций) и их размер, если есть проблемы производительности, время компатизации лога
5. время жизни сообщений
6. compacted topic - чтобы не хранить удаленные (удаляются не сразу давая время среагировать)
7. разделить на snapshot (как hot storage) топик и обычный, из snapshot читать при первом подключении
8. 1 топик - 1 группа - рекомендуется
9. ребалансировка - использует по умолчанию stop-the-world и все в группе ждут, затратно, есть другие алгоритмы

kuber
1. auto scale - для авто увеличения количества Pods
---------------
описание стандартного проекта на java

команда 15 чел
ца (целевая аудитория) - 300к
микросервисов 15
по 6 под на 2х плечах
500 messages per second with kafka 
1000 ops per second by rest.
подбор железа - на практике только на нт (нагрузочном тестировании)
процентиль 99% - 99% операций (запросов) выполняется быстро
диструктивное тестирование - убивают приложение и оно должно быстро подняться
---------------
sso (Single Sign On): oauth2, OpenID Connect (OIDC) - протоколы единой точки входа, для авторизации 1 раз на всех ресурсах

oauth2 - сайт просит у user access_toke, user дает его, сайт с ним идет в gmail
и берет часть данных на которые у него есть доступ (календарь, фото и т.д.)

scope - часть данных к которым сайт имеет доступ, для каждого набора данных выдается свой access_token
refresh_token - как access_token только живет дольше и позволяет запросить короткоживущий access_token, чтобы реже слать по сети access_token
client_id и client_secret - id клиента по которому gmail выдаст access_token, нужно хранить в секрете
access_token - это jwt типа bearer

resource owner - user профиль: логин, адрес ...
resource server - все что привязано к user: календарь, заметки и пр
client - сайт который получает доступ к тому что лежит на resource owner или resource server
auth server - выдает токен в обмен на логин/пароль или др данные (может быть одним целым с resource owner)

identity provider (IdP) - хранит user id и др инфу, класс программ для управления правами доступа
Identity and Access Management (IAM), IDP - аналогично

динамическая регистрация клиента: RFC 7591 и RFC 7592

типы авторизации:
1 Client Credentials - сервер 1 отправляет client_id и client_secret к gmail и получает access_token, с ним идет на сервер 2, который использует этот access_token
2 Resource Owner Password Credential - не рекомендуется, отправка логин/пароль + client_id и client_secret к gmail чтобы сайт получил access_token
3 Authorization Code Grant - рекомендуется
	1) redirect на страницу gmail и запрос прав, "разрешаю предоставить права тому сайту"
	2) если успешно редирект на URL с authorization code в GET - сайт получает этот authorization code из URL
	3) сайт делает запрос с authorization code в gmail и получает access_token
	4) сайт использует access_token чтобы получить доступ к scope на gmail
4 Implicit Grant - для без back-end, только выдает access_token и редирект назад в SPA фронт приложение

OpenID Connect (OIDC) - это надстройка над oauth2 с инфой о логине и профиле пользователя
id_token - доп. токен в дополнение к access_token, содержит инфу о user

SAML - устаревший протокол

пример запроса jwt для типа Authorization Code Grant, response_type=code - тип oauth2, redirect_uri - url на который редирект с authorization code
https://YOUR_DOMAIN/authorize?response_type=code&client_id=YOUR_CLIENT_ID&redirect_uri=https://YOUR_APP/callback&scope=SCOPE&audience=API_AUDIENCE&state=STATE

пример с jwt
curl --request GET --url https://api2.com/api --header 'authorization: Bearer ACCESS_TOKEN' --header 'content-type: application/json'
---------------
Spring Boot Cache with Redis
1. подключить maven artifact - spring-boot-starter-data-redis + @EnableCaching
2. конфиг spring boot
	spring.data.redis.host=localhost
	spring.data.redis.port=6379
3. cache config
	@Bean RedisCacheConfiguration cacheConfiguration() { // bean
		return RedisCacheConfiguration.defaultCacheConfig()
		  .entryTtl(Duration.ofMinutes(60)) // time-to-live (TTL) - время хранения в памяти до очистки
		  .disableCachingNullValues() // кэшировать null?
		  .serializeValuesWith(SerializationPair.fromSerializer(new GenericJackson2JsonRedisSerializer()));
	}
	@Bean RedisCacheManagerBuilderCustomizer redisCacheManagerBuilderCustomizer() { // custom serializer
		return (builder) -> builder
		  .withCacheConfiguration("itemCache", RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofMinutes(10)))
		  .withCacheConfiguration("customerCache", RedisCacheConfiguration.defaultCacheConfig().entryTtl(Duration.ofMinutes(5)));
	}
4. исползовать
	@Cacheable(value = "itemCache") Item getItemForId(String id) { return itemRepository.findById(id).orElseThrow(RuntimeException::new); }
5. ручное управление CacheManager, в т.ч. в тестах
    @Autowired CacheManager cacheManager;
	Cache cache = cacheManager.getCache("myCache"); // какой кэш
	String cachedData = (String) cache.get(param, String.class); // read
	cache.put(param, data); // write
	
	варианты CacheManager:
		SimpleCacheManager — простой кэш-менеджер
		ConcurrentMapCacheManager — лениво инициализирует, не рекомендуется для реальных проектов
		JCacheCacheManager, EhCacheCacheManager, CaffeineCacheManager — 3е стронние, гибко настраиваемые
6. если нужна другая структура данных redis - работает как с repository вручную
	Strings - by default, для string, json, number и пр что приводится в string
	Lists - очередей (FIFO), временных линий, сообщений в чатах
	Sets - как массив string, e.g. списки users
	Sorted Sets - sorted
	Hashes - ключ-значение, для объектов с их полями
	Bitmaps - с флагами состояний
	Streams - последовательность сообщений с временными метками и id
	HyperLogLog - для подсчета уникальных значений, можно только добавлять данные

    @Bean RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        template.setKeySerializer(new StringRedisSerializer());
        template.setHashKeySerializer(new StringRedisSerializer());
        template.setHashValueSerializer(new GenericJackson2JsonRedisSerializer());
        template.setValueSerializer(new GenericJackson2JsonRedisSerializer());
        return template;
    }
	redisTemplate.opsForHash().putAll(key, data); // write
	redisTemplate.opsForHash().entries(key); // read
---------------
Cassandra - дедецентрализовая DB, eventually consistent, в обмен на ограниченную гибкость запросов (поиск только по индексам, без join, денормализованные данные), нет внешних ключей и join. Оптимизирована для высокой скорости записи и при массовых вставках. Не поддерживает analytical queries (join, внешние ключи, агрегацию и пр). Связей между табл нет - если нужны связи то денормализуют табл. Работать с ней как через sql, но без связей, join, фильтры только по полям табл.

Технологии:
	1 LSM trees - высокая скорость записи, сохраняются в памяти, а потом делается flush на диск. Данные добавляются последовательно - append only.
	2 Commit log - накапливает инфу перед сохранением, 
	3 Memtable - табл в памяти, накапливает данные после Commit log
	4 SSTables - имутебильные файлы отсортированные таблицы, данные попадают сюда из Memtable, использует Bloom filters и хранит сортированные данные для быстрого поиска
	5 Compaction - бъединения нескольких SSTables, в ходе которого удаляются устаревшие версии данных и tombstones, как Vacuum может создавать нагрузки при компакцизации мусора
Понятия:
	1 Cassandra Query Language (CQL) - аналог sql
	2 KEYSPACE - как схема в postgres
Плюсы:
	1 Высокая скорость записи за счёт append-only операций и использования LSM trees.
	2 Горизонтальное масштабирование.
	3 Децентрализованна, использует commit log и использованию gossip-протоколов.
Минусы:
	1 point queries - бд позволяет только поиск без join, поиск только по индексу
	2 eventual consistency - задумана как в первую очередь распределенная бд
	3 compaction - операция как vacuum, нужна для очистки старых данных, требуется ее выполнять и это вызываает нагрузку
Параметры:
	1 Consistency level - сколько реплик должно подтвердить операцию перед фиксацией данных
		1 ONE / LOCAL_ONE - одна
		2 QUORUM / LOCAL_QUORUM (кворум) - более половины реплик, local - опеределенном датацентре
		3 ALL - все
		4 EACH_QUORUM - QUORUM во всех датацентрах
	2 Replication Factor - количество реплик. Каждая табл может иметь свои настройки репликаций.
	3 Read Repair (Чтение с восстановлением, чтении с высокой согласованностью) - исправляет расхождения между репликами
		1 read_repair_chance 0.2 - задает вероятность шанса восстановления (0.0-1.0)
		2 dclocal_read_repair_chance - тоже, но внутри одного датацентра
		Пример: CREATE TABLE users ( ... ) WITH read_repair_chance = 0.1;
	4 Hinted Handoff (Подтверждённая передача подсказок, повышения отказоустойчивости) - хранит hints на узле для реплик в других узлов, могут накапливаться на диске для мертвых узлов, не заменяет механизм репликации
		1 hinted_handoff_enabled=true - включение/отключение
		2 max_hint_window_in_ms=7200000 - время хранения hints
		3 hints_directory=/var/lib/cassandra/hints - каталог хранения hints
		4 hints_flush_period_in_ms=5000 - время flush на диск
	5 Replication Strategy Class (параметр class) - стратегию репликации
		1 SimpleStrategy - выбирает узел для хранения основной копии, затем распределяет реплики по кольцу в порядке обхода узлов, не подходит для многодатацентровых кластеров.
		2 NetworkTopologyStrategy - позволяет задать разное количество реплик для каждого дата-центра, учитывает топологию сети
			CREATE KEYSPACE my_keyspace WITH replication = { 'class': 'NetworkTopologyStrategy', 'DC1': 3, 'DC2': 2}; - DC1 - датацентр
		3 OldNetworkTopologyStrategy - старый неоптимальный аналог NetworkTopologyStrategy, не рекомендуется
	6 Clustering
	7 Partioning


0 создание схемы через утилиту cqlsh, cql - формает как sql
	docker exec -it cassandra cqlsh -u username -p password - подключаемся
	cat src/main/resources/cassandra/migrations/V1__Create_tables.cql | cqlsh - выполняем cql
	CREATE ROLE admin WITH PASSWORD = 'password' AND LOGIN = true AND SUPERUSER = true; - создать role
	CREATE USER 'my_user' WITH PASSWORD 'pass' AND SUPERUSER = false; - создать user
	CREATE KEYSPACE IF NOT EXISTS my_keyspace WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 3}; - создаем KEYSPACE с параметрами репликации
	CREATE TABLE IF NOT EXISTS my_keyspace.products ( id UUID PRIMARY KEY, name TEXT, price DECIMAL); - создаем табл в KEYSPACE
	GRANT SELECT, INSERT, UPDATE, DELETE ON KEYSPACE my_keyspace TO my_user; - назначить пользователя для KEYSPACE
	
	DESCRIBE KEYSPACES; - список KEYSPACES
	DESCRIBE TABLES; - таблицы в KEYSPACES
	DESCRIBE TABLE table1; - структура табл
	
	USE my_keyspace; - переключение на my_keyspace
	SELECT * FROM my_keyspace.users WHERE username = 'john_doe' ALLOW FILTERING; - выборка
	INSERT INTO users (id, first_name, last_name, email) VALUES (uuid(), 'John', 'Doe', 'john.doe@example.com'); - create
	UPDATE users SET last_name = 'Jones' WHERE id = 'your.UUID.here';
	DELETE FROM users WHERE id = 'your.UUID.here';
	CREATE INDEX ON users (email); - индекс
	SELECT * FROM sensor_data PER PARTITION LIMIT 10; - количество строк внутри партиции
	UPDATE users SET email='new@mail.com' USING TIMESTAMP 1625097600000000 WHERE id=1; - кастомную временную метку
	
	BEGIN BATCH - batch операции
	  INSERT INTO orders (...) VALUES (...);
	  UPDATE inventory SET count = count-1 WHERE product_id=123;
	APPLY BATCH;
	
	модификаторы:
		ALLOW FILTERING - отключает использование индекса, приводит к полному скану табл. как scan
			- не используют в проде (проектируют табл под нагрузки), на случай если не возможно создать вторичный индекс
		INSERT INTO orders (id, product) VALUES (123, 'Phone') USING TTL 86400; - время жизни записи
1 зависимости
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-data-cassandra</artifactId>
	</dependency>
2 настройка
	spring:
	  data:
		cassandra:
		  keyspace-name: my_keyspace
		  contact-points: localhost
		  port: 9042
		  local-datacenter: datacenter1
		  username: user
		  password: pass
		  schema-action: CREATE_IF_NOT_EXISTS
		cassandra.pool:
		  idle-timeout: 10s
		  max-queue-size: 256
		  max-requests-per-connection: 64
3 репозиторий
public interface UserRepository extends CassandraRepository<User, UUID> {
    @Query("SELECT * FROM users WHERE username = ?0 ALLOW FILTERING")
    List<User> findByUsername(String username);
}