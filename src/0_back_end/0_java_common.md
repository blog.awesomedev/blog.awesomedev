- base api: Object, Class, Objects, String, StringBuilder, Number, BigDecimal, Date, SSL, logger
- equals+hashCode
- https://www.baeldung.com/java-deprecated
- spi
***
- [SSL](#ssl)
- [JVM](#jvm)
  - [jar, war, ear](#jar-war-ear)
  - [Java Flight Recorder, JFR](#java-flight-recorder-jfr)


# SSL
Source: [1](https://habr.com/ru/company/dbtc/blog/487318/), [2](https://www.baeldung.com/java-ssl), [3](https://www.baeldung.com/java-keystore-truststore-difference), [4](https://colinpaice.blog/2020/04/05/using-java-djavax-net-debug-to-examine-data-flows-including-tls/), [5](https://russianblogs.com/article/44571446497/), [6](https://stackoverflow.com/questions/24555890/using-a-custom-truststore-in-java-as-well-as-the-default-one/24561444#24561444), [7](https://www.baeldung.com/java-keystore)

**KeyStore** - хранилище **private** (для подключения кого-то к нам) и **public** (для подключения к кому-то) ключей  
**TrustStore** - хранилище **только public** ключей

[KeyStore API](https://www.baeldung.com/java-keystore) - настройка и установка store программно в коде

(Trust)**StoreManager** - возвращает ключи и проверяет их наличие для **url** к которому идет попытка подключения при **handshake** (проверке ключей). В некоторых реализациях серверов можно указать **только 1 store**, и если нужно использовать несколько хранилищ, то нужно писать реализацию которая читает несколько хранилищ.  
Стандартное хранилище сертификатов встроенно прямо в java `C:\0_soft\jdk-11.0.13\lib\security\cacerts`, пароль по умолчанию **changeit**. Добавлять сертификаты в `cacerts` а не указывать отдельное хранилище - плохая практика.

**JKS** - популярный проприетарный формат store  
**PKCS12** - формат store для jdk9+ **by default**

**Параметры** - при установке параметров store будет не `C:\0_soft\jdk-11.0.13\lib\security\cacerts`, а установленный, store может быть только 1. Если нужно много, то нужно устанавливать **programmatically**
```sh
-Djavax.net.ssl.keyStore=c:/myStore
-Djavax.net.ssl.keyStorePassword=123
-Djavax.net.ssl.trustStoreType=JKS
```

**Иерархия классов:**  
**SSLSession** - содержит **SSLContext**, который содержит хранилище ключей **KeyManager** или **TrustManager** и api для работы с ними, в том числе api для **handshake** - верификации сертификатов и сопоставления URL (пути к ресурсу) и сертификата.  
Для создания **KeyManager** или **TrustManager** можно использовать **KeyManagerFactory** или **TrustManagerFactory** или определить свои реализации (классы) **KeyManager** или **TrustManager**.
```
KeMaterial > KeyManagerFactory|MyCustomKeyManager > KeyManager
  > SSLContext > SSLSocketFactory|SSLServerSocketFactory
    > SSLSocket<--->SSLEngine > SSLSession

KeMaterial > TrustManagerFactory|MyCustomTrustManager > TrustManager
  > SSLContext > SSLSocketFactory|SSLServerSocketFactory
    > SSLSocket<--->SSLEngine > SSLSession
```

**keytool** - утилита из java
```sh
# create certs
openssl req -nodes -new -x509 -keyout server.key -out server.cert

# create store with certs
keytool -import -file server.cert -alias serverTestCA -keystore serverTestCATrustStore3.jks -storepass changeit -deststoretype jks

# check store attributes
keytool -list -v -keystore serverTestCATrustStore
```

**Логирование**
```
-Djavax.net.debug=help # список комманд логирования
-Djavax.net.debug=ssl:all # все
-Djavax.net.debug=ssl:handshake:sslctx:keymanager:trustmanager # только определенное
```
**На практике**  
При использовании REST, Kafka etc, в том числе для **cross service interaction** и использовании **ssl** (http**s**) нужно создать сертификаты, и положить их в стандартный **trust store** или указать в настройках **feign** отдельный файл **trust store** с 1им (или несколькими) сертификатами
```java
  // Custom Trust Store
  // Source: https://stackoverflow.com/a/67964403
  // 1. создаем SSLSocketFactory, он хранит и использует trust store
  private static SSLSocketFactory sslSocketFactory(InputStream trustStoreStream, String trustStorePassword, String trustStoreType, String trustStoreAlgorithm)
          throws NoSuchAlgorithmException, KeyStoreException, CertificateException, IOException,  KeyManagementException {
      SSLContext sslContext = SSLContext.getInstance("TLSv1.2"); // обычно используется этот
      TrustManagerFactory tmf = createTrustManager(trustStoreStream, trustStorePassword, trustStoreType, trustStoreAlgorithm);
      sslContext.init(new KeyManager[]{}, tmf.getTrustManagers(), null);
      return sslContext.getSocketFactory();
  }

  // 2. создаем custom trust store
  private static TrustManagerFactory createTrustManager(InputStream trustStoreStream, String trustStorePassword, String trustStoreType, String trustStoreAlgorithm)
          throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
      KeyStore trustStore = KeyStore.getInstance(trustStoreType);
      trustStore.load(trustStoreStream, trustStorePassword.toCharArray());
      TrustManagerFactory tmf = TrustManagerFactory
              .getInstance(trustStoreAlgorithm);
      tmf.init(trustStore);
      return tmf;
  }

  // 3. создаем feign.Client которым заменим стандартный feign.Client (который использует стандартный store)
  public feign.Client makeMyFeignClient() {
      try {
          return new MyClient(
                  sslSocketFactory(
                          trustStoreLocationResource.getInputStream(),
                          trustStorePassword,
                          Optional.ofNullable(trustStoreType).orElse(KeyStore.getDefaultType()),
                          Optional.ofNullable(trustStoreAlgorithm).orElse(TrustManagerFactory.getDefaultAlgorithm())
                  ),
                  null
          );
      } catch (Exception e) {
          throw new RuntimeException("Error in initializing feign client", e);
      }
  }

  // 4. создаем bean
  @Bean
  public Client feignClient() { return makeFeignClient(); }

  // 5. подключаем кастомный конфиг с feign.Client
  @FeignClient(name = "bla", url = "/bla", configuration = CustomFeignClientConfig.class)
  public interface MyFeignClient {
      @GetMapping("/blabla")
      ResponseEntity<MyDto> getSmth(@RequestParam(name = "param") String param);
  }
```


# JVM

## jar, war, ear
Source:

## Java Flight Recorder, JFR
Source: [oracle](https://docs.oracle.com/javacomponents/jmc-5-4/jfr-runtime-guide/about.htm), [baeldung](https://www.baeldung.com/java-flight-recorder-monitoring), [habr](https://habr.com/ru/companies/krista/articles/532632/), [habr custom jfr](https://habr.com/ru/companies/krista/articles/552538/)

**JFR** - собрк метрик jvm на лету и сохранение в файл, можно выбрать период времени за который собрать метрику, можно собирать постоянно с перезаписью старой. Можно редактировать **profile** метрик выбирая что записывать, но **profiles** поставляемые с jdk лучше оптимизированы для записи на **production**.

**Why to use?** Если ошибку не получается найти по логам, аудиту приложения, метрикам в grafana, jaeger (трассировке). Содержит больше информации чем **thread dump** или **heap dump** (и включая их)

**JFR Data Flow** - хорошо оптимизирована, легковесный сбор метрик за счет сбора **events** событий jvm, нет влияния на оптимизации jvm. Может постоянно работать в **production** приложении. **Event** содержит **payload** с инфой о событии, можно конфигурировать какую инфу собирать в payload и на какие events срабатывать. Event содержит время начала/конца, id для thread, cpu, heap size до/после, stack trace на момент выполнения event. Сбор метрик внутри происходит через JFR APIs и хранится в маленьких thread local буферах, которые потом сливаются в in-memory buffer. Из in-memory buffer данные сливаются на disk. Т.к. i/o операции на disk дорогие, то нужно их минимизировать по возможности и выбирать данные для записи осторожно чтобы не загрузить i/o. Данные которые еще не слились на disk **не будут доступны в случае падения** jfr. Данные в jfr файле **могут быть не в том порядке** в котором они собраны в разных threads. Если не записывать данные на disk, а держать в памяти, то это облегчит сбор JFR метрик, но в памяти хранятся только последние данные и в случае ошибок будут доступны только они. 

**JFR Architecture** - состоит из 2х компонентов JFR runtime и Flight Recorder plugin for Java Mission Control (JMC)  
1 JFR runtime: 1) **agent** controls buffers, disk I/O, MBeans, and so on; 2) **producers** insert data into the buffer, при этом **producers** собирает инфу в том числ через Java API из сторонних applications (видимо работающих совмемстно с тем для которого запущен JFR)  
2 Flight Recorder plugin - плагин для JMC

**JFR собирает 4 типа event**  
1 instant event - случается сразу  
2 duration event - имеет время начало/конца и собирается когда закончено  
3 timed event - имеет опцию threshold по времени, будут записаны только events которые превысили лимит времени  
4 sample event/requestable event - срабатывают периодически чтобы собрать метрики за время

[Java Mission Control](https://www.oracle.com/java/technologies/jdk-mission-control.html) и др утилитами - может редактировать профили JFR, запускать/останавливать JFR подключаясь с application

**Profile.** Предустановленные профили поставляются с jdk в `JAVA_HOME/lib/jfr/`, профили проработаны разработчиками java и **хорошо оптимизированы** (лучше не менять) для работы в **production** чтобы снимать метрики в работающем приложении на лету. Профили можно смотреть/редактировать через [Java Mission Control](https://www.oracle.com/java/technologies/jdk-mission-control.html), **jprofiler** - удобный просмотр файла jfr  
1 **default.jfc** (Continuous) - `>1%` производительности  
2 **profile.jfc** (Profiling) - `2%` производительности, больше инфы, больше размер дампа jfr

**Note.** JFR имеет много опций и профили имеют много опций, если нужно найти определенную проблему нужно конфигурировать вручную.

```sh
# Note. В старой jdk 8 нужно было включать JFR параметром -XX:+UnlockCommercialFeatures

# По времени, последние 300 сек
-XX:StartFlightRecording=disk=true,maxage=300s,name=aged,settings=profile,filename=./aged.jfr

# По размеру, последние 200 мега байт
-XX:StartFlightRecording=disk=true,maxsize=200M,name=sized,settings=profile,filename=./sized.jfr

# При запуске в командной строке появится, 20144 PID номер процесса который использовать в jcmd для сбора
"Use jcmd 20144 JFR.dump name=1 to copy recording data to file."

# запуск для уже работающей jar
# disk=true - на диск, а не в памяти
jcmd 3328 JFR.start filename=aged.jfr disk=true maxage=300s name=aged settings=/my/path/profile.jfc
jcmd 3328 JFR.start filename=sized.jfr disk=true maxsize=200M name=sized settings=/my/path/profile.jfc

# stop
jcmd 3328 JFR.stop name=aged.jfr

# check
jcmd 3328 JFR.check name=aged.jfr

# Для обоих вариантов инициировать сбор дампа в файл нужно вручную командами
# т.е. записанные метрики будут сброшены в дамп этими командами и после этого их можно открыть в JMC
# где filename=... это путь к файлам
jcmd 20144 JFR.dump name=aged
jcmd 20144 JFR.dump name=sized

# запуск с custom профиль
java -XX:StartFlightRecording=settings=/path/to/custom/profile.jfc -version

# задаем диск для хранения кэша если делаем большой дамп
jcmd 28534 JFR.configure repositorypath=/path/to/disk/cahce maxchunksize=20m
```