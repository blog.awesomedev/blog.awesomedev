****
- [Common Concurrency Source](#common-concurrency-source)
- [Common](#common)
- [`java.lang`](#javalang)
  - [Thread](#thread)
    - [Common](#common-1)
    - [Thread.Builder](#threadbuilder)
    - [State](#state)
    - [Priority](#priority)
    - [Methods](#methods)
    - [`Thread.setDefaultUncaughtExceptionHandler`](#threadsetdefaultuncaughtexceptionhandler)
    - [Daemon Thread](#daemon-thread)
  - [ThreadGroup](#threadgroup)
  - [ThreadFactory](#threadfactory)
  - [ThreadLocal, InheritableThreadLocal](#threadlocal-inheritablethreadlocal)
  - [Scoped Values](#scoped-values)
  - [volatile](#volatile)
  - [VarHandle `java.lang.invoke`](#varhandle-javalanginvoke)
- [`java.util`](#javautil)
  - [Timer, TimerTask](#timer-timertask)
- [`java.util.concurrent`](#javautilconcurrent)
  - [Lock Objects `java.util.concurrent.locks`](#lock-objects-javautilconcurrentlocks)
  - [Synchronized Collections](#synchronized-collections)
  - [Concurrent Collections](#concurrent-collections)
  - [Synchronizers](#synchronizers)
    - [Common](#common-2)
    - [Semaphore](#semaphore)
    - [CountDownLatch](#countdownlatch)
    - [CyclicBarrier](#cyclicbarrier)
    - [Phaser API](#phaser-api)
    - [Exchanger](#exchanger)
  - [Executors](#executors)
    - [Common](#common-3)
    - [Executor](#executor)
    - [Executor parameters and when to use them](#executor-parameters-and-when-to-use-them)
    - [Fork/Join Framework](#forkjoin-framework)
    - [CompletableFuture](#completablefuture)
    - [FutureTask](#futuretask)
  - [Atomic `java.util.concurrent.atomic`](#atomic-javautilconcurrentatomic)
  - [Atomic Accumulator, Adder](#atomic-accumulator-adder)
  - [Structured Concurrency](#structured-concurrency)
- [Thread Synchronization Issues, Concurrency Problems, Deadlock, Livelock, Starvation, Race Condition, Resource Contention, Thread Leak](#thread-synchronization-issues-concurrency-problems-deadlock-livelock-starvation-race-condition-resource-contention-thread-leak)
- [Double checked locking](#double-checked-locking)
- [happens before](#happens-before)
- [Java Memory Model, JMM, JSR 133](#java-memory-model-jmm-jsr-133)
- [Green Threads](#green-threads)
- [Virtual Threads](#virtual-threads)
- [QA](#qa)
  - [Synchronize abstract method](#synchronize-abstract-method)
  - [Как принудительно остановить поток?](#как-принудительно-остановить-поток)
  - [В каком случае будет выброшено исключение InterruptedException, какие методы могут его выбросить?](#в-каком-случае-будет-выброшено-исключение-interruptedexception-какие-методы-могут-его-выбросить)
  - [Можно ли создавать новые экземпляры класса, пока выполняется static synchronized метод?](#можно-ли-создавать-новые-экземпляры-класса-пока-выполняется-static-synchronized-метод)
  - [Предположим в методе run возник RuntimeException, который не был пойман.](#предположим-в-методе-run-возник-runtimeexception-который-не-был-пойман)
  - [Какие стандартные инструменты Java вы бы использовали для реализации пула потоков?](#какие-стандартные-инструменты-java-вы-бы-использовали-для-реализации-пула-потоков)
  - [Что такое «атомарные типы» в Java?](#что-такое-атомарные-типы-в-java)
  - [Что такое ExecutorService?](#что-такое-executorservice)
  - [Зачем нужен ScheduledExecutorService?](#зачем-нужен-scheduledexecutorservice)
  - [Способы создания потока](#способы-создания-потока)

# Common Concurrency Source
* https://www.baeldung.com/java-concurrency
* oracle concurrency essential https://docs.oracle.com/javase/tutorial/essential/concurrency/index.html
* interview questions https://www.baeldung.com/java-concurrency-interview-questions
* heppense before https://docs.oracle.com/javase/specs/jls/se8/html/jls-17.html#jls-17.1
* https://jenkov.com/tutorials/java-concurrency
* thread
  * https://www.baeldung.com/java-start-thread
* thread pool
  * https://www.baeldung.com/thread-pool-java-and-guava
  * https://www.baeldung.com/java-executors-cached-fixed-threadpool
* ExecutorService
  * https://www.baeldung.com/java-executor-service-tutorial
  * https://www.baeldung.com/java-executor-wait-for-threads
* Fork/Join
  * https://www.baeldung.com/java-fork-join
* CompletableFuture
  * https://www.baeldung.com/java-completablefuture

# Common
* `java.lang`
  * Runnable, Thread
    * **Priority:** Thread.MIN_PRIORITY, Thread.MAX_PRIORITY, Thread.NORM_PRIORITY
    * **State:** Thread.State getState(), BLOCKED, NEW, RUNNABLE, TERMINATED, TIMED_WAITING, WAITING
    * **Deprecated Methods:** stop(), suspend(), destroy(), resume(), countStackFrames()
    * **Methods:** sleep(), start(), run(), join(), currentThread(), ...
    * **Static Methods:** 
    * Daemon Thread
    * Thread.UncaughtExceptionHandler
  * ThreadGroup
  * ThreadFactory
  * ThreadLocal, InheritableThreadLocal
  * volatile
  * Synchronized Collections
* `java.util`
  * Timer, TimerTask
* `java.util.concurrent`
  * Lock Objects `java.util.concurrent.locks`: ReadWriteLock, StampedLock, ReentrantLock, LockSupport
  * Concurrent Collections, Concurrent Queues
  * Synchronizers
    * Semaphore
    * CountDownLatch
    * CyclicBarrier
    * Phaser API
    * Exchanger
  * Executors
    * Executor Interfaces: Callable, Future, ScheduledFuture, ExecutorService, ScheduledExecutorService
    * Thread Pools: ThreadPoolExecutor, ScheduledThreadPoolExecutor
    * Fork/Join Framework: ForkJoinPool
    * CompletableFuture
    * FutureTask
  * Concurrent Random Numbers: `ThreadLocalRandom`
  * Atomics: `java.util.concurrent.atomic`: AtomicInteger, AtomicReference, AtomicStampedReference, DoubleAccumulator, LongAdder

# `java.lang`
## Thread

### Common
**Thread** - инкопсулирует поток. И всё общения с потоком происходит через этот класс и объект этого класса.

Аналогом `main` метода, для дочерних нитей служит метод `run` интерфейса `Runnable`.
Поток закончит выполнение, когда завершится его метод `run()` (в `Runnable`) или `call()` (в `Callable`). Для главного потока это метод `main()`.

Главный поток по умолчанию имеет имя **main**. Имя выводится через `toString()` (в поток, `println(thread)`)

Даже если метод `main()` уже завершился, но еще выполняются порожденные им потоки, система будет ждать их завершения.

У каждого объекта в Java есть УНИКАЛЬНЫЙ монитор неявно связанный с ним.  
Любой Object в Java - монитор  
Никакой другой поток не может исполнить этот метод ЭКЗЕМПЛЯРА потому что монитор для каждого экземпляра свой и привязан к нему через this.
```java
synchronized(this) { ... } // this - одновременно УНИКАЛЬНЫЙ монитор и Object в Java
```
***
**re-entrant mutex** - `synchronized` метод может быть вызван из другого синхронизированного метода  
Т.е. в Java если поток уже вошел в монитор, то он не обязан ждать освобождения монитора собой же.  
`(не отпуская монитор вызываем метод в котором тот же монитор и это работает)`
```java
// //методы разные, монитор тот же
void method1() { synchronized (this) { method2() } }
void method2() { synchronized (this) { } }
```
***
`synchronized` - вызвавший поток захватывает монитор
```java
// 1. синхронизация
synchronized void f1() { } // 1.
synchronized(this) { ... } // 2. когда нету доступа к коду класса

// 2. синхронизация статического метода
synchronized static void f1() { } // 1.
synchronized(MyObj.class) { ... } // 2.

// 3. Если в потоках используется какой то совместный ресурс, то доступ к нему должен быть синхронизирован
// Для Collection вместо синхронизации можно использовать потокобезопасные коллекции
List<String> list = new List();
var uniqueMonitorObject = new Object();
synchronized (uniqueMonitorObject) { list.blabla(); } // 1.
synchronized (list) { list.blabla(); } // 2.
```
***
**Thread interaction** (Взаимодействие потоков) - использование методов для управления потоками из других потоков, e.g. уступка монитора, завершение, ожидание (notify(), wait() и т.д.)  
реализовано `final` методами `notify(), notifyAll(), wait()` в `Object`  
методы `wait(), join и sleep` должны быть окружены блоками `try`
* `wait()` - входим в монитор, останавливаем поток, пока другой поток вошедший в этот монитор не вызовет `notify()`
  * перед возобновление ожидающего потока поток ждем пока поток вызвавший `notify()` не выйдет из монитора!
  * поток вызвавший `wait()` останавливается в месте её вызова, а после возобновления продолжает выполнения с места остановки в мониторе
* `wait(ms)` - с макс. временем ожидания
* `notify()` - возобновляет поток из которого был вызван метод `wait()`
	если остановленных потоков несколько, то возобновится 1 любой
	но пока находится в мониторе другие потоки синхронизированные на том же мониторе ждут пока он закончит блок
* `notifyAll()` - тоже только для всех потоков
* `Thread.yield()` - уступить ресурс, приостановить текущий поток и дать параллельным работать
	```java
	while(!msgQueue.hasMessages()) {		//Пока в очереди нет сообщений
		Thread.yield();		//Передать управление другим потокам
	}
	```
* `sleep()` может выполняться либо заданное кол-во времени (миллисекунды или наносекунды)
либо до тех пор пока он не будет остановлен ПРЕРЫВАНИЕМ
	* (в этом случае он сгенерирует исключение InterruptedException).
	```java
	Thread.sleep(2000);
	```
* В конструкторе Thread метод `wait` не вызывать. Т.к. объект ещё не создался, а он связан с потоком.
* У методов, приостанавливающих выполнение потока, таких как `sleep(), wait() и join()`
  * есть одна особенность — если во время их выполнения будет вызван метод `interrupt()` этого потока,
они, не дожидаясь конца времени ожидания, сгенерируют исключение `InterruptedException`.
```java
// Поток 1 выполняет:
static void f1() {
	synchronized(sync) {
		while(cond1)
			try {
				System.out.println("before wait");
				sync.wait(); //засыпает на этом месте
				System.out.println("after wait"); //когда проснется И ПОЛУЧИТ монитор начнет отсюда
			} catch(InterruptedException e) { }
	}
}
// Поток 2 выполняет:
static void f2() {
	synchronized(sync) {
		System.out.println("before notify");
		cond1 = false;
		sync.notify(); //пробудил Поток 1, но мешает ему войти в монитор (сам занимает монитор)
		System.out.println("before notify"); //выполнил эту строку и вышел из монитора, освободил место для Поток 1
	}
}
// Результат: 
// before wait		-	Поток 1 остановился на wait()
// before notify	-	Поток 2 запустил Поток 1, но занимает пока монитор сам и Поток 1 ещё ждет освобождения монитора
// before notify	-	Поток 2 выполняет synchronized блок до конца
// after wait		- 	Поток 1 получил монитор и продолжает с места, где остановился
```
***
**Stop, Interrupting a Thread** - чтобы остановить или прервать Thread нужно реализовывать методы вручную (или использовать Executor).  
**How?** В реализации при stop проверяем флаг stop в loop, чтобы обработать случай произвольного отпускания монитора потоком (это возможно в Java).   
**Why?**  
1-`Thread.stop()` стал **deprecated**, т.к. был неправильно реализован, замены в `Thread` нет.  
2- Метод `wait` останавливает поток, но возможен случай **ЛОЖНОЙ АКТИВАЦИИ**, когда поток начинает работать без вызова `notify()`. Поэтому `wait` вызывается внутри цикла с условием, чтобы в случае возобновления он опять остановился  
3- Нельзя остановить поток сразу, нужно дать ему время на завершение операций (i/o close, lock release etc). Для этого реализован interrupt  
**Source:** [How to Kill a Java Thread](https://www.baeldung.com/java-thread-stop), [Java Thread Primitive Deprecation](https://docs.oracle.com/javase/8/docs/technotes/guides/concurrency/threadPrimitiveDeprecation.html)
```java
// 1. кратко
public class ControlSubThread implements Thread {
	static Object sync = new Object();
	volatile boolean started = true; // volatile для параллельных потоков

	public void run() { } // code here

	static void stop() {
		started = true;
		synchronized(sync) {
			while(started) // проверяем volatile флаг перед блокировкой на общем мониторе
				try { sync.wait(); }
				catch(InterruptedException e) {}
		}
	}

	static void start() {
		started = false;
		synchronized(sync) {
				sync.notify();
		}
	}
}

// 2. полный пример, включая interrupt
public class ControlSubThread implements Runnable {
    private Thread worker;
    private int interval = 100;
    private AtomicBoolean running = new AtomicBoolean(false);
    private AtomicBoolean stopped = new AtomicBoolean(true);

    public ControlSubThread(int sleepInterval) { interval = sleepInterval; }
    public void start() { worker = new Thread(this); worker.start(); }
    public void stop() { running.set(false); }
    public void interrupt() { running.set(false); worker.interrupt(); }
    boolean isRunning() { return running.get(); }
    boolean isStopped() { return stopped.get(); }

    public void run() {
        running.set(true);
        stopped.set(false);
        while (running.get()) {
            try {
                Thread.sleep(interval);
            } catch (InterruptedException e) {
            	Thread.currentThread().interrupt();
                System.out.println("Thread was interrupted, Failed to complete operation");
	    }
            // do something, code here
        }
        stopped.set(true);
    }
}
```

### Thread.Builder
Source: [oracle](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/lang/Thread.Builder.html)

`java.lang.Thread.Builder` - builder for Thread and ThreadFactory, `inheritInheritableThreadLocals(true)` - наследовать или нет `InheritableThreadLocal`
```java
Thread.Builder builder = Thread.ofPlatform() // или Thread.OfVirtual
	.inheritInheritableThreadLocals(true)
	.uncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() { ... })
	.name("worker-", 0);
Thread t1 = builder.start(task1);
ThreadFactory tf = builder.factory();

ThreadFactory virtualThreadFactory = Thread.builder().virtual().factory(); // или так
```

### State

**Thread.State** - Состояние потока, может изменить ПОСЛЕ получения статуса, поэтому его нельзя использовать для синхронизации потока.

```java
Thread.State ts = thread.getState();
boolean state = ts == Thread.State.RUNNABLE;
```

```
* состояния (6 штук):
	1. готов к выполнению, когда получил время CPU (создание Thread?)
	2. выполняется (запуск start)
	3. приостановлен (sleep?)
	4. возобновлен (notify?)
	5. ожидает освобождения ресурса, заблокирован (wait?)
	6. прерван, прерванный поток нельзя восстановить (interrupt? или конец метода run())
```
```
Thread.State getState(); //State это enum
	BLOCKED - ожидает монитор (another thread in synchronized)
	NEW - ещё не запущен
	RUNNABLE - выполняется, или будет выполнятся когда наступит его очередь (по приоритету)
	TERMINATED - завершен
	TIMED_WAITING - поток временно преостановлен с таймером (sleep(222), wait(333), join(2222))
	WAITING - wait и join без таймера
```

### Priority
**Priority** - Приоритет потока используется особенно, когда есть потоки которые захватывают весь ЦП, ему пониают приоритет, чтобы дать другим возмость работать.

При использовании JVM с вытесняющей многозадачностью возникают разные нюансы.
Лучше использовать потоки, которые добравиольно уступают друг другу ресурсы (wait, notify, synchronized).

`Main Thread` **default** priority - 5 (NORM_PRIORITY) (в т.ч. для группы)  
Приитет от 1 до 10 (MIN_PRIORITY - MAX_PRIORITY)  
приоритет по умолчанию - 5 (NORM_PRIORITY)  
эти переменные из класса Thread и они static final  
Никогда не изестно как поведет себя приоритет потоков.
```java
int p = Thread.currentThread().getPriority();
public final void setPriority(int a);
Thread.MIN_PRIORITY, Thread.MAX_PRIORITY, Thread.NORM_PRIORITY
```

### Methods

```
Runnable - содержит только void run()

--------
constructors

Thread()
Thread(Runnable)
Thread(ThreadGroup)
Thread(ThreadGroup группа_потока, Runnable поток)
	Если группа потоков не указана, то запускаемый поток относится к той же группе что и родительский поток
Thread(String name)
Thread(Runnable r, String name)

--------
methods

static void sleep()
void start()
public void run()
final void join() throws InterruptedException //вызывающий поток ожидает когда указанный поток "присоеденится к нему" (завершится)
join(ms)
interrupt() //прерывает выполнение потока
isInterrupted() //
--------

static int activeCount() - количества потоков текущей группы
static int enumerate(Thread потоки[]) - копирует все потоки из текущей группы в массив потоков
long getID() - depricated, id потока
final getName() - замена для getID()
final getPriority()
Thread.State getThreadState() - состояние потока
final ThreadGroup getThreadGroup() - текущая группа
static boolean holdsLock(Object obj) - true если вызывающий поток владеет блокировкой obj
static boolean interrupted()
final Boolean isAlive()
final Boolean isDaemon() - демон поток или нет
final void setDaemon(boolean сщстояние) - помечает поток как демон
String toString()
static void yield() - может сделать паузу и позволить другим потокам начать исполнение

final Boolean isAlive() //true - поток рабоает, false - завершен
final void setPriority(int level) //установка приоритета потока
final int getPriority()
--------
static void sleep(long ms) throws InterruptedException
static void sleep(long ms, long nano) throws InterruptedException
--------
Методы Thread:
Thread(String name)
Thread(Runnable r, String name)
Thread(ThreadGroup tg, Runnable r, String name)
public static Thread currentThread() - ссылка на текущий поток
final String getName()
final void setName(String name)
final Boolean isAlive() //true - поток рабоает, false - завершен
final void setPriority(int level) //установка приоритета потока
final int getPriority()
static void sleep(long ms) throws InterruptedException
static void sleep(long ms, long nano) throws InterruptedException
public void run()
start()
final void join() throws InterruptedException //вызывающий поток ожидает когда указанный поток "присоеденится к нему" (завершится)
join(ms)
interrupt() //не прерывает выполнение потока, а просто устанавливает флаг,
			// поток (или его родительский поток) внутри себя или родительский должен почитать этот флаг и САМ остановится
			// после вызова сбрасывает флаг обратно на false!
isInterrupted() // проверяет флаг прерванности, не сбрасывает флаг!
--------
static Thread

final String getName()
final void setName(String name)
interrupted() //проверяет на прерванность и СБРАСЫВАЕМ ЕГО флаг
public static Thread currentThread() - ссылка на текущий поток
activeCount() //возвращает количество потоков текущей группы
yeild() //приостанавливает текущей поток чтобы позволить работать остальным
sleep(2000)
--------
Thread.State getThreadState() - состояние потока
final ThreadGroup getThreadGroup() - текущая группа
static boolean holdsLock(Object obj) - true если вызывающий поток владеет блокировкой obj
--------
join
Runnable r = () -> { System.out.println("qwe"); };
Thread t = new Thread(r);
t.start();
try { t.join(); //main ожидает пока не завершится поток }
catch(Exception e){}
-------
deprecated methods:
stop(), suspend(), destroy(), resume()
countStackFrames() - в нем вызываются suspend и destroy
```

### `Thread.setDefaultUncaughtExceptionHandler`
`Thread.setDefaultUncaughtExceptionHandler` - перехват RuntimeException в Thread, если объявить обработчик  
интерфейс `Thread.UncaughtExceptionHandler` - **implement** в class для обработки, `Thread, ThreadGroup`

1. если обработчик есть в Thread, то перехват
2. иначе если обработчик есть в ThreadGroup, то перехват
3. Иначе в jvm (т.е. будет падение jvm)
```java
interface Thread.UncaughtExceptionHandler {
	void uncaughtException(Thread t, Throwable e);
}

thread.setUncaughtExceptionHandler(new Thread.UncaughtExceptionHandler() {
    public void uncaughtException(Thread t, Throwable e) { System.out.println("exception " + e + " from thread " + t); }
});
```

### Daemon Thread
Source: [1](https://www.baeldung.com/java-daemon-thread)
```
Потоки-демоны в Java (копировано с сайтов)
	любой код (включая finally) в daemon потоках будет остановлен когда завершаться все обычные потоки
	статус daemon наследуется от родительского потока
	daemon не рекомендуется для io операций, т.к. finally не выполнятся в случае остановки daemon (при закрытии программы)
	Thread.join() в daemon может не заблокировать приложение от завершение (ошибка)
	//true - Thread поток завершится, когда main Thread завершится
	//false - Thread продолжит работу даже если main Thread завершится
	t.setDaemon(true); //устанавливатеся перед start()
	t.start();
	t.isDaemon()
```

## ThreadGroup
```
Группа потоков к которой относится Главный поток тоже называется main

ThreadGroup(String)
ThreadGroup(ThreadGroup parrent_group, String name)
tg1.list(); - получить список потоков
new Thread(tg1);


ThreadGroup
    Группа нитей образует дерево, в котором каждая другая группа нитей имеет родителя (кроме исходной).
    Поток имеет право доступа к данным из своей группы нитей,
        но не имеет такого доступа к другим группам или к родительской группе потоков.

--ThreadGroup
ThreadGroup(String)
ThreadGroup(ThreadGroup parrent_group, String name)

методы схожи с методами Thread + дополнительные для группы
tg1.list(); - получить список потоков

Пример
	ThreadGroup tg1 = new ThreadGroup("группа 1");
	Thread t1 = new Thread();
	Thread t2 = new Thread();
	Thread tArr[] = new Thread[tg1.activeCount()];
	tg1.enumerate(tArr); //копируем ссылки потоков в массив
	for(Thread t : tArr)
		t.doSmth();
	
Можно добавить поток в группу так: Thread t1 = new Thread(tg1,new MyRunnable(),"one"); 
```

## ThreadFactory
ThreadFactory - thread safe
```
--
ThreadFactory // просто Factory для Thread

public class MyThreadFactory implements ThreadFactory {
    @Override
    public Thread newThread(Runnable r) {
        return new Thread(r, name + "-Thread_" + threadId);
    }
}
new MyThreadFactory().newThread(new Task()).start();
--

```

## ThreadLocal, InheritableThreadLocal
```
ThreadLocal API https://www.baeldung.com/java-threadlocal

Работает как Map у которой ключ - текущий поток (только один)
При использовании Thread Pool может быть ОШИБКА, т.к. поток может иметь несколько ThreadLocal (или один ThreadLocal на несколько потоков)
	Решение: удалять вручную ThreadLocal каждый раз после завершения задачи, но это трудоемко
public class ThreadLocalWithUserContext implements Runnable {
	ThreadLocal<Integer> threadLocal = ThreadLocal.withInitial(() -> 1);
	threadLocalValue.set(1);
	Integer result = threadLocalValue.get();
	threadLocal.remove();
}
Решение 2: удалять ThreadLocal в hook
public class ThreadLocalAwareThreadPool extends ThreadPoolExecutor {
    @Override
    protected void afterExecute(Runnable r, Throwable t) {
        // Call remove on each ThreadLocal
    }
}

ThreadLocal - контейнер (похож на Map), который виден во всех частях ОДНОГО Thread, его часто используют напр. для Connection или Transaction в JavaEE
    initialValue() - важный метод для инициализации исходным значением
    остальные методы похожи на Map

Переменная в ThreadLocal будет удалена когда удален ее Thread.
ThreadLocal может использоваться вместо RequestScope для хранения данных в рамках запросов (это относительно старый подход)

Содержится в Thread.ThreadLocalMap threadLocals в типе WeakHashMap, the var uses lazy init, leave until Thread is alive
если на ThreadLocal нету в коде, то эта ThreadLocal может быть удалена т.к. WeakHashMap



--ThreadLocal (могут спросить на собеседовании)
для создания локальных переменных потоков, у каждого потока - своя копия переменной

У каждого потока - т.е. экземпляра класса Thread - есть ассоциированная с ним таблица ThreadLocal-переменных. Ключами таблицы являются cсылки на объекты класса ThreadLocal, а значениями - ссылки на объекты, "захваченные" ThreadLocal-переменными.

то есть это коробочка в которую можно положить что-то внутри потока, а потом достать. Внутри других потоков содержимое не видно.
кладем set(obj)
достаем get()
удалем remove()
initialValue() - работает как конструктор, инициализирует

Для чего оно надо: переменная доступна для вытаскивания не только в run() но и в любом методе вызванном в run() без передачи ему ссылки на переменную (во всем текущем потоке).
Это как static переменные, но только не одна на класс, а одна на поток.

пример 1
	static final ThreadLocal userName = new ThreadLocal();

	public static void main(String... args) {
		userName.set("Jane"); //кладем значение
		Runnable run = new Runnable() {
			@Override
			public void run() {
				String threadName = Thread.currentThread().getName();
				String name = userName.get(); //вытаскиваем значение
				System.out.println(threadName +": Welcome "+name);
			}
		};
		run.run();
	}

пример 2 - применение initialValue
	public class ConnectionAccess {
		private static ThreadLocal<Connection> connectionHolder = new ThreadLocal<Connection>() {
			public Connection initialValue() { //автоматически устанавливает значения всем созданным
				return DriverManager.getConnection(DB_URL);
			}
		};

		public static Connection getConnection() {
			return connectionHolder.get(); //вытаскиваем значение
		}
	}
```

## Scoped Values
Source: [baeldung](https://www.baeldung.com/java-20-scoped-values)

**Scoped Values** - preview api, эффективная замена Thread Local

## volatile

Source: [baeldung Volatile](https://www.baeldung.com/java-volatile), [baeldung Volatile Variables and Thread Safety](https://www.baeldung.com/java-volatile-variables-thread-safety), [baeldung Volatile vs. Atomic Variables](https://www.baeldung.com/java-volatile-vs-atomic), [baeldung Common Concurrency Pitfalls](https://www.baeldung.com/java-common-concurrency-pitfalls). [umd.edu JSR 133 (Java Memory Model)](http://www.cs.umd.edu/~pugh/java/memoryModel/jsr-133-faq.html). [oracle jls-17](https://docs.oracle.com/javase/specs/jls/se14/html/jls-17.html#jls-17.7), [oracle atomic](https://docs.oracle.com/javase/8/docs/api/index.html?java/util/concurrent/atomic/package-summary.html)

- **volatile** - keyword for **variables**, can be used with: `primitive type, enum, reference`
  - **Important points**
    - `volatile` Object **reference** can be **null**
    - `volatile` Object **reference** members (class fields) are **NOT** `volatile`
    - In `OLD` **JMM**, accesses to `volatile` variables could be reordered with `non volatile` variable accesses. It is fixed in `NEW` **JMM**
   - **Visibility Guarantee** - visible **current** value for all threads - делает var видной всем threads (переменная видна в обход L1/L2 cpu cache, или из L3 cpu cache общего для ядер или из RAM)
   - **Happens-Before Guarantee** - operation with vars strictly **ordered** - делает happens before для var (т.е. идущий за var код и выполнится за var и порядок выполнения не будет меняться при компиляции для оптимизации)
     - `volatile double/long` are **atomic** - делает **чтение** var типов double/long 64bit **atomic**, т.к. без **volatile** они **могут** читаться как 2 операции по 32bit разными threads
     - any write to a volatile field happens before every subsequent read the volatile field
 - **Why we need volatile?**
   - **Memory Visibility** - cpu cache some vars in L1/L2 caches and they are not visible for other cpu sometimes, cpu use **write buffer** for write operations and can **flush** it in any order in **any time**
   - **Reordering** - if we use independent cpu-instruction (code) then cpu can reorder work with them. What can reorder  cpu-instructions: 1) cpu write buffer flush, 2) cpu out-of-order execution technique, 3) JIT compiler
		```java
		// "ready" var does not depends on "number" and CAN be reordered by jvm
		number = 42; 
		ready = true;
		```
- **volatile vs synchronized** - volatile is **lightweight** synchronization, `current` var value are **visible** for threads **without** a **lock**
  - переменные в `synchronized` блоке будут видны **в обход кэшей CPU**, как при `volatile`, это следует из `"An unlock on a monitor happens-before every subsequent lock on that same monitor"` (одно из правил happens before, при unlock сбрасывает кэши, а при lock все данные видны в обход кэша). При этом `volatile` не делает **lock** на переменных, а `synchronized` делает **lock** и делает операции **atomic** и другие потоки не могут вклиниться между операциями (такие как increment++). В **double check lock** переменная `volatile` для первого сравнения до `synchronized` блока, т.к. внутри `synchronized` блока переменная и так видна в обход кэша CPU.
  - **When we can use volatile WITHOUT `synchronized` for multi-threading?**
    - **Multi threads ONLY read** and **ONE** thread **writes** to the volatile variable. The **ONE** reading threads see the **latest** value.
    - **When new value does not depend on the old value.** Multi threads can write **new value** to the var.
  - **When we CAN NOT use volatile WITHOUT `synchronized` for multi-threading?** - if we read a value of a shared variable to figure out the next value THEN we have to use `volatile` with `synchronized`
    - **non-atomic operations** - not `synchronized` sequence of operations on one or more vars. In the case may overwrite values of `volatile` vars.
    - **composite operations** (e.g. `increment`) - consist from multi operations (e.g. increment++ read/update/write). Some thread can change value between these multi operations. Time gap in between reading and writing the new value can create a **race condition**.
- **Piggybacking on the visibility properties of another `volatile` variable** (uses `Happens-Before Ordering of JMM`) - **flush** cpu cache & make **ordered** execution before work with a **volatile** var `for all vars BEFORE volatile` so we can use with `NON-volatile vars behave like volatile`, к моменту работы с volatile переменной состояние переменных **до** этой volatile переменной будет актуальным (ordering + visibility), поэтому даже если они не volatile можно считать что value актуальны.
	```java
	public class TaskRunner {
		static int number; // not volatile works like volatile HERE for work with "ready" var
		volatile static boolean ready; // "number" has current value before write to "ready"
	}
	```
- **Volatile vs. Atomic**
  - using of **atomic** is simple
  - `volatile` need `synchronized` to be **atomic** (**mutual exclusion** of threads) for more then one operation (composite operations like increment) - больше 1ой операции нужно обернуть в `synchronized` чтобы сделать их atomic
  - can use atomic **without** `synchronized` (operations like increment are **atomic**)
  - atomic `get()` and `set()` are **wrapper** for `volatile` 
  - atomic **performance** of `get()` and `set()` like with `volatile`
  - atomic `compareAndSet(), getAndIncrement()` are used **Compare-and-Swap** (CAS, **read-modify-write**) they are **faster** then `volatile` because use of CPU **atomic** instructions instead of **lock**
    - On **some** CPU the atomic CAS/FAA can use **lock** (not efficient!)
    - It uses **Compare-and-Swap** (CAS), CAS do not flush CPU cache, save CPU thread state, restore CPU thread state
    - `getAndIncrement()` also uses **Compare-and-Swap** (CAS) in a **loop** and **DO NOT** uses **Fetch-and-Add** (FAA) or other atomic instructions because **CAS** in **loop** is **faster**
  - `volatile+synchronized` is called **pessimistic lock**, `atomic` is called **optimistic lock**
- **Example. Multi threaded counter** (volatile + synchronized)
  1. **synchronized** makes happens-before for WRITE - `increment()`
  2. **volatile** makes happens-before for READ - `getValue()`
	```java
	// Example. Multi threaded counter
	// 1. synchronized makes happens-before for WRITE - increment()
	// 2. volatile makes happens-before for READ - getValue()
	// We do not use synchronized for READ because it slower!!!
	// Block synchronized is needed for increment because it is READ & WRITE operations and a thread can do something between them.
	//
	// NOTE!!! volatile makes happens-before EVEN for READ for double/long here!!!
	// For example work with double/long 64bit value may work like TWO operations! It can happen with other types too!
	// Another words READ or WRITE for non-volatile double/long are NOT atomic!
	// BUT volatile MAKES double/long READ & WRITE operations atomic!
	class SyncronizedCounter {
		volatile int counter = 0; // current value is visible in all threads
		synchronized void increment() { counter++; } // synchronized READ & WRITE
		int getValue() { return counter; } // READ current value without!!! synchronized is faster
	}
	```

## VarHandle `java.lang.invoke`
Source: [oracle VarHandle](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/invoke/VarHandle.html), [baeldung VarHandle](https://www.baeldung.com/java-variable-handles)

**VarHandle** - has method wrappers in modes: usual, volatile, compare-and-set (atomic)

# `java.util`
## Timer, TimerTask
```
--Timer и TimerTask
	Можно создать поток и запустить по таймеру в будущем (упрощают по сравнению с чистым Thread)
	
	Timer - запуск задания
	TimerTask - описание задания (реализует Runnable())
	
	Timer(String имя_потока, boolean демон_ли)
		демон выполняется пока работает запустившая его программа
	
	методы TimerTask:
		boolean cancel() - прерывает задание
		abstract void run()
		
	методы Timer
		boolean cancel() - прерывает таймер планировщика (задание не запустится?)
		int purge() - удаляет прерванное задание из очереди
		void schedule(TimerTask задание, long ожидание, long повтор)
		void schedule(TimerTask задание, Date заданное_время, long повтор) - время повтора относительно ПОСЛЕДНЕГО запуска
		scheduleAtFixedRate(...) - тоже самое, но время повтора фиксированно относительно САМОГО ПЕРВОГО запуска
		
	class MyTimerTask extends TimerTask {
		public void run() {
			//do smth
		}
	//main
		new Timer().schedule(new MyTimerTask(), 1000, 500);

Timer

TimerTask task = new TimerTask() {
    public void run() {
        System.out.println("Task performed on: " + new Date() + "n" 
          + "Thread's name: " + Thread.currentThread().getName());
    }
};
Timer timer = new Timer("Timer");
long delay = 1000L;
timer.schedule(task, delay);
--
Timer vs ScheduledThreadPoolExecutor https://www.baeldung.com/java-start-thread

Timer
	- использует Object.wait(long)
	- использует 1ин background поток, задачи запускаются по порядку и большая задача может задержать остальные
	- runtime exception в TimerTask убьет весь поток и вообще весь Timer
ScheduledThreadPoolExecutor
	- может использовать много потоков (настраивается)
	- может обработать RuntimeException через @Ovveride afterExecute() { } из ThreadPoolExecutor
	- RuntimeException в одной Task не убивает остальные
	- использует OS API для timezone, delay etc
	- есть API через которые одни задачи могут ждать завершение других (зависимостей) чтобы выполниться
	- есть API для thread life cycle
```

# `java.util.concurrent`

## Lock Objects `java.util.concurrent.locks`
Source: [baeldung](https://www.baeldung.com/java-concurrent-locks), [oracle javadoc](https://docs.oracle.com/en/java/javase/17/docs/api/java.base/java/util/concurrent/locks/package-summary.html), [oracle doc](https://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html), [oracle concurrency essential](https://docs.oracle.com/javase/tutorial/essential/concurrency/newlocks.html)

```
Блокировки - аналог synchronized блоков, внутри потока вызываются методы: 1. lock() 2. Доступ к общему ресурсы 3. unlock()
	Lock - интерфейс
	ReentrantLocks - позволяет повторную блокировку (синхронизацию) тем же потоком, сколько было повторов столько надо и методов unlock() вызвать
	ReadWriteLock - блокировка доступа потокам только на чтение из общего ресурса
	ReentrantReadWriteLock
	JDK8: появился StampedLock - в книге ничего


Блокировки (java.util.concurrent.locks.Lock) - аналог synchronized блоков,
	внутри потока вызываются методы:
1. lock() 2. Доступ к общему ресурсы 3. unlock():
		Lock lock = new ReentrantLock();
		lock.lock();
		// do smth
		lock.unlock();
	Lock - интерфейс
	ReentrantLocks - позволяет повторную блокировку (синхронизацию) тем же
	потоком, сколько было повторов столько надо и методов unlock() вызвать
	ReadWriteLock - блокировка доступа потокам только на чтение
		из общего ресурса
	ReentrantReadWriteLock
	JDK8: появился StampedLock - в книге ничего

--
Lock API https://www.baeldung.com/java-concurrent-locks

- lock и unlock может быть в разных методах (в отличии от wait/notyfy)
- может сделать так что монитор из всех потоков получит тот кто ждал дольше (установка fairness property)
- tryLock() проверить занят ли поток и только потом захватить монитор, иначе можно не захватывать и поток не будет заблокирован (в synchronized такой проверки нет)
- lockInterruptibly() - может interrupt поток в состоянии waiting (в synchronized block такой поток нельзя interrupt)

методы:
void lock() 
void lockInterruptibly()
boolean tryLock() // non-blocking version of lock(), true if locking succeeds
boolean tryLock(long timeout, TimeUnit timeUnit)
void unlock()
Lock readLock() // вернет ReadLock
Lock writeLock()

// 1. использование
Lock lock = ...; 
lock.lock();
try {
    // access to the shared resource
} finally {
    lock.unlock(); // рекомендуется в finally
}

// 2. ReentrantLock
boolean isLockAcquired = lock.tryLock(1, TimeUnit.SECONDS);
if(isLockAcquired) {
	try {
		//Critical section here
	} finally {
		lock.unlock();
	}
}

// 3. ReentrantReadWriteLock - отдельные lock для read/write
ReadWriteLock lock = new ReentrantReadWriteLock();
Lock readLock = lock.readLock();
Lock writeLock = lock.writeLock();
readLock.lock(); // войдет в монитор, если нет других write lock
writeLock.lock(); // войдет в монитор, если нет других read и write locks

// 4. StampedLock - может использовать optimistic lock
private StampedLock lock = new StampedLock();
long stamp = lock.writeLock(); // write
lock.unlockWrite(stamp);
long stamp = lock.readLock(); // read
lock.unlockRead(stamp);
long stamp = lock.tryOptimisticRead();
stamp = lock.readLock();
lock.unlock(stamp); // optimistic lock

// 5. Condition - позволяет остановить поток до выполнения Condition
ReentrantLock lock = new ReentrantLock();
Condition stackEmptyCondition = lock.newCondition(); // можно создать несколько Condition для 1го lock
Condition stackFullCondition = lock.newCondition();
tackFullCondition.await(); // как wait
stackFullCondition.signalAll(); // как notifyAll()

// 6. LockSupport - имеет методы lock и unlock потока
LockSupport.park(this);
LockSupport.unpark(waiters.peek());
--
```

## Synchronized Collections
Source: [1](https://www.baeldung.com/java-synchronized-collections)

**Synchronized Collections** - методы возвращают wrappers для Collection работающие как `synchronized block`

**Note.** `synchronized block` все равно нужно использовать при вызове **нескольких** методов, чтобы операции были **atomic**

* **Synchronized Collections vs Concurrent Collections**
  * **Synchronized Collections** - синхронизированы все методы, используется monitor (как `synchronized block`)
  * **Concurrent Collections** - намного быстрее чем **Concurrent Collections**, collection поделена на сегменты, синхронизированы сегменты, не вся коллекция, одновременно несколько threads имеет доступ к разным сегментам

```java
// methods
Collections.synchronizedCollection(new ArrayList<>());
Collections.synchronizedList(new ArrayList<>());
Collections.synchronizedMap(new HashMap<>());
Collections.synchronizedSortedMap(new TreeMap<>());
Collections.synchronizedSet(new HashSet<>());
Collections.synchronizedSortedSet(new TreeSet<>());

class A { // in synchronized block for atomic operations ( forEach() + add() )
    synchronized(syncCollection) {
        syncCollection.forEach(e -> nonCollection.add(e.toUpperCase()));
    }
}
```

## Concurrent Collections
Source: [1](https://habr.com/ru/company/luxoft/blog/157273/)
```
Особенности concurrent Collection:
безопасный многопоточные read/write
не бросают ConcurrentModificationException
каждый элемент будет пройден в loop только 1 раз, т.е. элементы будут как на момент создания и они МОГУТ (но не обязаны) отражать ИХ актуальные изменения другим кодом
```
```
non concurrent - concurrent
HashMap 		ConcurrentHashMap
TreeMap 		ConcurrentSkipListMap
TreeSet 		ConcurrentSkipListSet
Map subtypes 	ConcurrentMap
List subtypes 	CopyOnWriteArrayList
Set subtypes 	CopyOnWriteArraySet
PriorityQueue 	PriorityBlockingQueue
Deque 			BlockingDeque
Queue 			BlockingQueue


CopyOnWrite
	CopyOnWriteArrayList/CopyOnWriteArraySet
		- если много операций read и мало write; (частые чтения редкая запись)
		- изменяющие операции (add, set etc) реализованы через
			пересоздание Array и копирование старого содержимого в новый array
		- основа CopyOnWriteArraySet это CopyOnWriteArrayList
Scalable Maps (не интерфейс, просто группа)
	ConcurrentMap - interface
		- добавляет atomic опирации в Map
		- с обычным synchronized Map есть вероятность что 2 разных потока
			выполнят put() одновременно и значение затрется
			для решение добавлен метод putIfAbsent()
		- тоже для remove(k,v) / replace(k,v) / V replace(k,v)
	ConcurrentHashMap
		- synhronized сделан на bucket
		- iterator - fail safe, как listItterator (нет ConcurrentModificationException)
	ConcurrentNavigableMap
		- реализует NavigableMap: ceilingEntry(K key), floorEntry(K key), higherEntry(K key), firstEntry, pollFirstEntry, lastEntry, pollLastEntry ...
		- возвращает ConcurrentNavigableMap<K,V> объекты как VALUE
		- iterator - fail safe, как listItterator (нет ConcurrentModificationException)
	ConcurrentSkipListMap
		- многопоточный аналог TreeMap, основан на SkipList
	ConcurrentSkipListSet
		- основан на ConcurrentSkipListMap
Non-Blocking Queues
	ConcurrentLinkedQueue/ConcurrentLinkedDeque
		- использует CAS (Compare-and-swap)
		- ConcurrentLinkedDeque на 40% медленнее чем ConcurrentLinkedQueue
		- size() может работать долго
Blocking Queues
	ArrayBlockingQueue (extends BlockingQueue) / BlockingDeque
		- извлечение из пустой queue или добавление в полную
			заблокирует поток пока очередь не будет готова выполнить операцию
		- для обмена сообщениями между потоками через queue
	SynchronousQueues (extends BlockingQueue)
		- ПУСТАЯ очередь для обмена сообщениями между потоками через нее
		- позволяет вставить элемент в очередь только в том случае,
			если имеется поток, ожидающий поступления элемента для обработки
	DelayQueue
		- позволяет вытаскивать элементы из очереди только по прошествии некоторой задержки через getDelay
	LinkedBlockingQueue
		- алгоритм two lock queue, get/set имеют отдельные lock
		- быстреем чем ArrayBlockingQueue, но больше расход памяти
	PriorityBlockingQueue
		- многопоточная PriorityBlocking
	LinkedBlockingDeque (extends BlockingDeque)
		- список с одним локом
	LinkedTransferQueue (extends TransferQueue)
		- существует возможность заблокировать вставляющий «Producer» поток до тех пор, пока ДРУГОЙ поток «Consumer» не вытащит элемент из очереди
		- может быть с таймаутом
		- может быть с проверкой на наличие ожидающих «Consumer»
```

## Synchronizers

### Common
**Synchronizers** - синхронизация и взаимодействие потоков

* **Semaphore** - семофор (монитор с счетчиком, который синхронизирует ни один поток, а много)
* **CountDownLatch** - ждет пока не произойдет определенное количество событий,
а потом срабатывает (запускает свой Thread и при заравершении поток
уменьшает счетчик внутри себя методом countDown())
* **CyclicBarrier** - позволяет группе потоков войти в режим ожидания в заданной
точке выполнения (работать до определенной точки в main())
* **Exchanger** - Обмен данными между двумя потоками
* **Phaser** - синхронизирует потоки проходящие через несколько фаз операции.
  * Как CyclicBarrier только точек много, а потоки изнутри оповещают Phaser
	о достижении фаз. После приостановки на фазе (точке в коде главного
	потока) главный поток запускает следующую фазу.

### Semaphore
**Semaphore** - монитор с счетчиком

```java
Semaphore SEMAPHORE = new Semaphore(5, true); // true - в порядке очереди
semaphore.isFair() // true если в порядке очереди
SEMAPHORE.acquire(); // wait пока не освободится место
SEMAPHORE.release();
semaphore.getQueueLength()
semaphore.availablePermits() // свободные слоты
if (semaphore.tryAcquire()) { } // получит монитор только если есть незанятые (не делает wait)
tryAcquire(int permits, long timeout, TimeUnit unit) // permits счетчик можно уменьшать не на 1, а на любое число
```

### CountDownLatch
**CountDownLatch** - замок который позволяет работать остановленным потокам когда счетчик == 0
```java
CountDownLatch countDownLatch = new CountDownLatch(5);
countDownLatch.await(); // ждем пока CountDownLatch != 0 (e.g. основной поток ждет операции в дочерних)
public class Worker implements Runnable { // запускаем эти потоки, а потом вызываем countDownLatch.await(); в main Thread
    @Override public void run() {
        doSomeWork();
        countDownLatch.countDown(); // уменьшаем счетчик в потоках
    }
}
boolean completed = countDownLatch.await(3L, TimeUnit.SECONDS); // timeout на случай ошибок в потоке (когда не может уменьшить счетчик)
```

### CyclicBarrier
**CyclicBarrier** - как CountDownLatch, но ждать может много потоков, а не 1ин

* CountDownLatch vs CyclicBarrier
	* CountDownLatch - 1ин поток ждет пока остальные уменьшат `counter--`, ждет завершение N задач;
		* один поток может уменьшить `counter` на сколько угодно (в отличии от `CyclicBarrier`)
		оперирует задачами `Tasks`
	* `CyclicBarrier` - потоки жду друг друга
		* оперирует потоками `Threads`
```java
// указанный поток выполнится когда другие станут wait()
CyclicBarrier cyclicBarrier = new CyclicBarrier(3, () -> { doSmthThread(); });
class Task implements Runnable {
	@Override public void run() {
		try { barrier.await(); } // уменьшает счетчик барьера
		catch (InterruptedException | BrokenBarrierException e) { e.printStackTrace(); }
	}
}
// проверка был ли хоть 1 поток (использующий cyclicBarrier) interrupted
if (!cyclicBarrier.isBroken()) {
	// запуститься поток doSmthThread(); когда count==0
	new Task().start(); new Task().start(); new Task().start();
}
```

### Phaser API
* **Phaser API** - как CyclicBarrier и CountDownLatch, но количество потоков можно менять на лету (в CountDownLatch так нельзя)
	- как несколько барьеров (фаз), потоки останавливаются и ждут пока другие потоки дойдут до места count2++ пока все зарегистрированные не отработают на том же месте
	- т.е. фазы (код) можно стартовать повторно сколько угодно раз
* **methods**
  * `int arrive()` - увеличивает count но не делает wait потоку
  * `int awaitAdvance(int phase)` - если номер phase равен текущей, то wait(), если будет/станет другим - поток продолжит работу
  * `forceTermination()`
```java
Phaser ph = new Phaser(1); // задаем количество фаз ==1, это эквивалентно вызову ph.register() 1ин раз
class LongRunningAction implements Runnable {
	LongRunningAction(Phaser ph) { ph.register(); } // count1++, регистрируем, увеличит count потоков

	@Override public void run() {
		ph.arriveAndAwaitAdvance(); // count2++, wait пока count1 != count2 (зарегистрированные != прибывшие)
		try {
			Thread.sleep(20);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		ph.arriveAndDeregister(); // count1-- разрегистрируем
	}
}
executorService.submit(new LongRunningAction(ph)); // ... запускаем
ph.arriveAndAwaitAdvance(); // main thread ждет пока дочерние потоки не пройдут фазы
ph.getPhase() == 1 // номер фазы

// разрегистрируем main thread, когда count1==0 уничтожается Phaser (как деструктор видимо потому что занимает память)
ph.arriveAndDeregister();
// тут можно стартовать через submit (или др.) потоки еще раз и еще раз вызвать ph.arriveAndDeregister(); и тогда ph.getPhase() == 2
// так можно стартовать фазы сколько угодно раз
```


### Exchanger
**Exchanger** - обмен сообщениями через вызов метода exchange(T t) потоками ОДНОГО типа

**For what?** например можно создать паттерн pipeline для обмена сообщениями и сэкономить GC (для этого делаются readerExchanger и writerExchanger)
```java
Exchanger<String> exchanger = new Exchanger<>(); // создаем глобально, видна всем потокам
Runnable taskA = () -> {
	try {
		String message = exchanger.exchange("from A"); // останавливается и ждет вызова exchange() другими потоками
		assertEquals("from B", message);
	} catch (InterruptedException e) {
		Thread.currentThread.interrupt();
		throw new RuntimeException(e);
	}
};
Runnable taskB = () -> {
	try {
		String message = exchanger.exchange("from B");
		assertEquals("from A", message);
	} catch (InterruptedException e) {
		Thread.currentThread.interrupt();
		throw new RuntimeException(e);
	}
};
CompletableFuture.allOf(runAsync(taskA), runAsync(taskB)).join(); // запускаем потоки
```

## Executors

### Common
```
Исполнители - управление запуском и работай потоков, имеет доп. методы для работы с элементами concurrent
	Executor - интерфейс с методом void execute(Runnable r) запускает поток
	ExecutorService - интерфейс расширяет Executor, запускаеть много потоков, останавливать, возвращаеть результат работы потоков и т.д.
	ScheduledExecutorService - планирование запуска потоков
	ScheduledThreadPoolExecutor
	Executors - фабричные методы для получения ExecutorService, ScheduledExecutorService и т.д.
		Executors.newFixedThreadPool(1).execute(new MyThread());
--------
	Исполнители (Executor) - управление запуском и работай потоков,
        имеет доп. методы
        Цель:
            вместо создания новых потоков переиспользовать старые
            для экономии ресурсов из специального ThreadPool
	для работы с элементами concurrent
		Executor - интерфейс с методом void execute(Runnable r) запускает поток
		ExecutorService - интерфейс расширяет Executor, запускаеть много потоков,
		останавливать, возвращаеть результат работы потоков и т.д.
		ScheduledExecutorService - планирование запуска потоков
		ScheduledThreadPoolExecutor
		Executors - фабричные методы для получения ExecutorService,
		ScheduledExecutorService и т.д.
			Executors.newFixedThreadPool(1).execute(new MyThread());
	Callable, Future - для получения результата в главном потоке потом
	(после того как они появятся)
		1. Реализуем Callable и его метод V call()
		2. В main() делаем ссылку Future<V> f;
		3. запускаем на выполнение поток Callable:
			Future<V> f = Executors.newFixedThreadPool(1).submit(new MyCallable<V>(){...});
        4. CompletableFuture - класс, наследует Future, работает похоже на Promise в js.
        Tip. Ему не назначен Executor, поэтому выполнится в специальном
        ForkJoinPool.commonPool() (если это не поддерживает параллелизм по крайней
        мере 2го уровня, в этом случае new Thread будет создан, чтобы выполнить
        каждую задачу). Интерфейс маркер AsynchronousCompletionTask можно
        использовать для monitoring. Прим. можно вызвать get() или sleep() чтобы
        подождать выполнения методов описанных ниже.
            supplyAsync(Supplier) - можно вытащить результат работы callback через      
                get() - future.get(),
                Thread будет ждать результат get() как с обычный .join()
            runAsync(Runnable, Executors.newCachedThreadPool()) - вытащить результат нельзя
            thenAccept() - аналог then() из Promise в js
				CompletableFuture.supplyAsync(() -> "Hi")
					.handle((obj, err) -> { throw new Exception(err)}) // гибкая обработка ошибок
					.exceptionally(err -> (5)) // выполнится только при Exception
					.thenApply(result -> { return result }).thenApply(result -> { return result }).get()
            thenApply() - в отличии от thenAccept() возвращает результат
            thenApplyAsync() - выполнится не в текущем Thread, а в отдельном ForkJoinPool.commonPool, нету get()
            thenCompose() - можно создать цепочку thenCompose(), результат return попадает в следующий (как для Promise из js)
            thenCombine() - по зовершению 2х задач выполнить 3ю (на самом деле можно комбинировать много задач)
	TimeUnit - enum с методами преобравания единиц времени в другие,
        (используется для timeout потоков)
        используется в concurrent как параметр многих методов
		long t = TimeUnit.convert(long время, TimeUnit.HOURS);
```

### Executor
```
ExecutorService - для запуска процессов и получения результата https://www.baeldung.com/java-util-concurrent

Executors - helper для управления пулом потоков

Executor - один метод execute()
ExecutorService - добавляет методы завершения потока shoutdown() и получения результата Future

ForkJoinPool, ScheduledThreadPoolExecutor, ThreadPoolExecutor - реализации ExecutorService
ScheduledExecutorService - как ExecutorService, но может по расписанию
	executorService.scheduleAtFixedRate(() -> { }, 1, 10, TimeUnit.SECONDS); // 1 раз
	executorService.scheduleWithFixedDelay(() -> { }, 1, 10, TimeUnit.SECONDS); // периодически
	
Как работает: каждый поток в поле имеет очередь задач (берет с головы). Если собственная очередь опустела, то поток берет задачи из чужой очереди (берет с хвоста).

// пример 1, у Executor 1ин метод, идеален для создания event loop
Executor executor = Executors.newSingleThreadExecutor();
executor.execute(() -> System.out.println("Hello World"));

// пример 1 ExecutorService
ExecutorService executor = Executors.newFixedThreadPool(10);
Future f = executor.submit(new Runnable { }); 
executor.shutdownNow();
try { executor.awaitTermination( 20l, TimeUnit.NANOSECONDS ); } // ждем пока не закончится как join() с timeout
catch (InterruptedException e) { e.printStackTrace(); }

// пример 1 ScheduledExecutorService
ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
Future<String> future = executorService.schedule(() -> { }, 1, TimeUnit.SECONDS);

// пример 2 invokeAll
List<Future<String>> futures = executorService.invokeAll(callableTasks);

// пример 2 CachedThreadPool, maximumPoolSize - Integer.MAX_VALUE, corePoolSize - 0; когда потоки отработали, то после 60 сек будут самоудаляться
// CachedThreadPool реализован как FixedThreadPool, но с другими параметрами (т.е. поведение CachedThreadPool можно воссоздать или модифицировать через FixedThreadPool)
// CachedThreadPool использует внутри SynchronousQueue, а FixedThreadPool - LinkedBlockingQueue
// CachedThreadPool не ограничивает количество потоков, если нужно ограничение нужно писать код самому (много тяжелых потоков могут повесить приложение)
// CachedThreadPool подходит когда много короткоживущих потоков
ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();


Future<String> future = executorService.submit(() -> { });
if (future.isDone() && !future.isCancelled()) {
    try {
        str = future.get();
    } catch (InterruptedException | ExecutionException e) {
        e.printStackTrace();
    }
}


// правильная остановка ExecutorService с timeout; Иначе останется висеть в памяти и может помешать завершению jvm т.к. его потоки из пула не будут завершены
executorService.shutdown();
try {
    if (!executorService.awaitTermination(800, TimeUnit.MILLISECONDS)) {
        executorService.shutdownNow();
    } 
} catch (InterruptedException e) {
    executorService.shutdownNow();
}


ThreadPoolExecutor - реализация ExecutorService. Он выполняет переданную задачу (Callable или Runnable)
    Пул потоков содержит в себе ThreadPoolExecutor, который может содержать изменяющееся число нитей.
    Число нитей в пуле задается с помощью corePoolSize и maximumPoolSize.

Про .submit():
    1. Вызов метода get на объекте Future возвратит значение,
        который возвращает Callable (ИЛИ null, если используется Runnable)
    2. Метод имеет 2 checked-исключения: InterruptedException, который бросается,
        когда выполнение прервано через метод interrupt(), или ExecutionException
        если код в Runnable или Callable бросил RuntimeException,
        что решает проблему поддержки исключений между потоками.


Future vs ScheduledFuture

ScheduledFuture - имеет метод getDelay() возвращает оставшееся время ТЕКЩУЕЙ ЗАДАЧИ до ожидания результата
ScheduledFuture<Object> resultFuture = executorService.scheduleAtFixedRate(runnableTask, 100, 450, TimeUnit.MILLISECONDS);


java.util.concurrent содержит:
	Callable, Future - для получения результата в главном потоке потом (после того как они появятся)
		1. Реализуем Callable и его метод V call()
		2. В main() делаем ссылку Future<V> f;
		3. запускаем на выполнение поток Callable:
			Future<V> f = Executors.newFixedThreadPool(1).submit(new MyCallable<V>(){...});
                get() - похож на then() из Promise в js
                cancel(), isCancelled(), isDone()
	TimeUnit - enum с методами преобравания единиц времени в другие, используется в concurrent как параметр многих методов
		long t = TimeUnit.convert(long время, TimeUnit.HOURS);

Параметры Executor

```

### Executor parameters and when to use them

1. **Размер пула потоков (corePoolSize, maximumPoolSize, keepAliveTime)**  
   - **corePoolSize** - Минимальное количество потоков, которые создаются и поддерживаются в пуле. Потоки до этого значения остаются активными, даже если простаивают.  
     **Когда использовать** - Для задач с регулярным поступлением, требующих минимальной задержки.  
     **Минусы**: Если значение слишком велико, может привести к неэффективному использованию ресурсов.  
     **Лимит corePoolSize** - Слишком большое значение может привести к избыточному потреблению ресурсов (нагрузке на планировщик), особенно если задачи выполняются быстро и не требуют много потоков.  
     **Минусы**: Может привести к неоправданным затратам ресурсов, если количество потоков слишком велико.  
     **Пример кода**:  
     ```java
     int corePoolSize = Runtime.getRuntime().availableProcessors(); // если нужно зависеть от ядер cpu
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         corePoolSize,  // corePoolSize
         8,  // maximumPoolSize
         30, // keepAliveTime
         TimeUnit.SECONDS,
         new LinkedBlockingQueue<>()
     );
     ```
   - **maximumPoolSize** - Максимальное количество потоков, которые могут быть созданы в пуле. Используется при всплесках нагрузки, если задачи поступают быстрее, чем их успевают обрабатывать.  
     **Когда использовать** - Для приложений с периодической высокой нагрузкой.  
     **Минусы**: Слишком высокое значение может привести к перегрузке ресурсов.  
     **Лимит maximumPoolSize** - Установка слишком большого значения может вызвать нехватку ресурсов, особенно если задачи активны долгое время.  
     **Пример кода**:  
     ```java
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,  // corePoolSize
         8,  // maximumPoolSize
         30, // keepAliveTime
         TimeUnit.SECONDS,
         new LinkedBlockingQueue<>()
     );
     ```
   - **keepAliveTime** - Время, в течение которого потоки сверх `corePoolSize` остаются активными при отсутствии работы.  
     **Когда использовать** - Увеличить, если ожидаются всплески задач; уменьшить, чтобы освобождать ресурсы при редких задачах.  
     **Минусы**: Неправильно настроенный параметр может привести к ненужной нагрузке или задержкам.  
     **Лимиты keepAliveTime** - Если значение слишком велико, ресурсы могут оставаться занятыми долгое время, даже если задачи отсутствуют, что может привести к неэффективному использованию системы. При слишком малом значении потоки могут быть слишком быстро выгружены, что также приведет к лишним затратам на создание новых потоков.  
     **Пример кода**:  
     ```java
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,  // corePoolSize
         8,  // maximumPoolSize
         30, // keepAliveTime
         TimeUnit.SECONDS,
         new LinkedBlockingQueue<>()
     );
     ```
2. **workQueue** - Очередь для хранения задач перед их выполнением.  
   - **LinkedBlockingQueue** - Неограниченная очередь. Используется для обработки большого числа задач с произвольным временем ожидания.  
     **Пример**: REST API-сервер, обрабатывающий запросы пользователей. Очередь принимает все запросы и распределяет их на выполнение.  
     **Минусы**: Может переполнить память при слишком большом числе задач, не рекомендуется для систем с жесткими требованиями к времени отклика.  
     ```java
     BlockingQueue<Runnable> queue = new LinkedBlockingQueue<>();
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,
         8,
         60,
         TimeUnit.SECONDS,
         queue
     );
     ```
   - **ArrayBlockingQueue** - Ограниченная очередь. Используется, чтобы ограничить потребление ресурсов.  
     **Пример**: Обработчик сообщений для Apache Kafka, чтобы не допустить переполнения памяти, ограничив количество обрабатываемых сообщений.  
     **Минусы**: Может блокировать добавление задач, если очередь переполнена, не подходит для приложений с высокой нагрузкой.  
     ```java
     BlockingQueue<Runnable> queue = new ArrayBlockingQueue<>(10);
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,
         8,
         60,
         TimeUnit.SECONDS,
         queue
     );
     ```
   - **SynchronousQueue** - Очередь без буфера, где задачи сразу передаются потоку. Используется для задач с высокой конкуренцией, где важна минимальная задержка.  
     **Пример**: Сервер обработки запросов, где задержка критична, например в биржевых приложениях для обработки сделок в реальном времени.  
     **Минусы**: Не подходит для приложений, где задачи могут быть отправлены в очередь при большой нагрузке, может вызвать отклонение задач (если задана политика RejectedExecutionHandler). Т.е. не подходит для скачков нагрузки т.к. нет буфера.  
     ```java
     BlockingQueue<Runnable> queue = new SynchronousQueue<>();
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,
         8,
         30,
         TimeUnit.SECONDS,
         queue
     );
     ```
   - **PriorityBlockingQueue** - Очередь с приоритетами. Используется для задач, где важен порядок выполнения, например, на основе приоритетов.  
     **Пример**: Система планирования заданий, где задачи с высоким приоритетом (например, обновления системы) должны выполняться раньше фоновых задач.  
     **Минусы**: Может замедлять выполнение задач с низким приоритетом, если высокоприоритетные задачи поступают часто.  
     ```java
     BlockingQueue<Runnable> queue = new PriorityBlockingQueue<>();
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,
         8,
         60,
         TimeUnit.SECONDS,
         queue
     );
     ```
3. **threadFactory** - Пользовательская фабрика потоков для задания их параметров (имён, приоритетов).  
     **Минусы**: Плохая реализация может привести к чрезмерному потреблению памяти или некорректному поведению.  
     ```java
     ThreadFactory customThreadFactory = runnable -> {
         Thread thread = new Thread(runnable);
         thread.setName("CustomThread-" + thread.getId());
         return thread;
     };
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4,
         8,
         60,
         TimeUnit.SECONDS,
         new LinkedBlockingQueue<>(),
         customThreadFactory
     );
     ```
4. **RejectedExecutionHandler** - Обработчик задач, которые не могут быть выполнены.  
   - **AbortPolicy** - Выбрасывает исключение `RejectedExecutionException` при попытке добавить задачу в заполненную очередь. Используется для строгого контроля над задачами.  
     **Минусы**: Задачи могут теряться, если они не могут быть выполнены, без дополнительной обработки.  
     ```java
     RejectedExecutionHandler handler = new ThreadPoolExecutor.AbortPolicy();
     ThreadPoolExecutor executor = new ThreadPoolExecutor(
         4, 
         8, 
         60, 
         TimeUnit.SECONDS, 
         new LinkedBlockingQueue<>(), 
         handler
     );
     ```
   - **CallerRunsPolicy** - Задача выполняется в вызывающем потоке (там, где был вызван метод `execute`). Это помогает избежать потери задачи, но может замедлить выполнение новых задач, так как вызывающий поток будет занят.  
     **Минусы**: Может затруднить обработку новых задач, так как текущий поток будет занят выполнением отклоненной задачи.  
     ```java
     RejectedExecutionHandler handler = new ThreadPoolExecutor.CallerRunsPolicy();
     ```
   - **DiscardPolicy** - Просто игнорирует задачу, не предпринимая никаких действий. Подходит, если потеря задачи не критична.  
     **Минусы**: Потеря задач без уведомления о проблемах.  
     ```java
     RejectedExecutionHandler handler = new ThreadPoolExecutor.DiscardPolicy();
     ```
   - **DiscardOldestPolicy** - Удаляет старейшую задачу из очереди, чтобы освободить место для новой. Используется для задач, где важнее выполнение свежих данных.  
     **Минусы**: Потеря старых задач без уведомления.  
     ```java
     RejectedExecutionHandler handler = new ThreadPoolExecutor.DiscardOldestPolicy();
     ```
5. Примеры оптимальных параметров
    ```java
    // Пример для REST API: задачи I/O-интенсивные, краткоживущие запросы
    ThreadPoolExecutor executor = new ThreadPoolExecutor(
        Runtime.getRuntime().availableProcessors(), // corePoolSize: количество потоков равно числу процессоров
        Runtime.getRuntime().availableProcessors() * 2, // maximumPoolSize: вдвое больше corePoolSize для всплесков нагрузки
        30L, // keepAliveTime: 30 секунд для освобождения потоков при снижении нагрузки
        TimeUnit.SECONDS, // Единица измерения времени
        new LinkedBlockingQueue<>(1000), // Очередь: хранение до 1000 задач при перегрузке
        new ThreadPoolExecutor.CallerRunsPolicy() // Обработчик: задачи выполняются вызывающим потоком при переполнении
    );

    // Пример для Kafka: задачи I/O-интенсивные, обработка сообщений в реальном времени
    ThreadPoolExecutor executor = new ThreadPoolExecutor(
        4, // corePoolSize: количество потоков равно числу обрабатываемых партиций (например, 4 партиции)
        4, // maximumPoolSize: равно corePoolSize, так как дополнительные потоки не увеличат производительность
        10L, // keepAliveTime: 10 секунд, так как потоки постоянно заняты
        TimeUnit.SECONDS, // Единица измерения времени
        new SynchronousQueue<>(), // Очередь: задачи сразу передаются в потоки без накопления
        new ThreadPoolExecutor.AbortPolicy() // Обработчик: выбрасывает исключение при перегрузке
    );

    // Пример для CPU-интенсивных задач: REST API с вычислениями
    ThreadPoolExecutor executor = new ThreadPoolExecutor(
        Runtime.getRuntime().availableProcessors() - 1, // corePoolSize: чуть меньше числа процессоров
        Runtime.getRuntime().availableProcessors(), // maximumPoolSize: равно числу процессоров
        60L, // keepAliveTime: 60 секунд для освобождения потоков после всплесков нагрузки
        TimeUnit.SECONDS, // Единица измерения времени
        new ArrayBlockingQueue<>(500), // Очередь: ограниченная очередь на 500 задач
        new ThreadPoolExecutor.CallerRunsPolicy() // Обработчик: задачи выполняются вызывающим потоком
    );
    ```

### Fork/Join Framework
```
ForkJoinPool vs ExecutorService - в ForkJoinPool задача Task (поток) может разбиватьсяна Subtasks (потоки) рекурсивно, а потом результат объединяется (RecursiveTask).
	В ForkJoinPool незанятые потоки берут часть задач из очередей задач занятых потоков (work-stealing algorithm).

// пример ForkJoinPool https://www.baeldung.com/java-fork-join
ForkJoinPool commonPool = ForkJoinPool.commonPool();
ForkJoinPool forkJoinPool = new ForkJoinPool(2); // или так

// создание task
//Рекомендуется в обычных ситуациях использовать как можно меньше Thread Pools, в идеале использовать только commonPool
//Рекомендуется установить имеющий смысл лимит на разделение потоков
//Рекомендуется избегать блокировки потоков внутри ForkJoinTasks
public class CustomRecursiveTask extends RecursiveTask<Integer> { // или RecursiveAction если вернет void, а не Future
    private int[] arr;
    private static final int THRESHOLD = 20;
    public CustomRecursiveTask(int[] arr) { this.arr = arr; }

    @Override
    protected Integer compute() {
        if (arr.length > THRESHOLD) {
            return ForkJoinTask.invokeAll(createSubtasks()) // делим на subtask если нужно
              .stream()
              .mapToInt(ForkJoinTask::join)
              .sum();
        } else {
            return processing(arr);
        }
    }

    private Collection<CustomRecursiveTask> createSubtasks() {
        List<CustomRecursiveTask> dividedTasks = new ArrayList<>();
        dividedTasks.add(new CustomRecursiveTask(Arrays.copyOfRange(arr, 0, arr.length / 2)));
        dividedTasks.add(new CustomRecursiveTask(Arrays.copyOfRange(arr, arr.length / 2, arr.length)));
        return dividedTasks;
    }

    private Integer processing(int[] arr) {
        return Arrays.stream(arr)
          .filter(a -> a > 10 && a < 27)
          .map(a -> a * 10)
          .sum();
    }
}
forkJoinPool.execute(customRecursiveTask);// запуск или так 1
int result = customRecursiveTask.join();
int result = forkJoinPool.invoke(customRecursiveTask); // или так 2
customRecursiveTaskFirst.fork();// или так 3
result = customRecursiveTaskLast.join();

ExecutorService vs Fork/Join - нужно использовать ExecutorService для один-поток-одна-задача (e.g. транзакции), а Fork/Join там где задачу можно разбить на мелкие куски
У ExecutorService выше контроль над потоками


	Fork / Join Framework - облегченный тип потоков (классов) по сравнению с Thread. Выполняет задачи меньшим количеством потоков, что эффективнее (видимо от того, что у CPU немного ядер?)
		ForkJoinTask<V> - задачи
		ForkJoinPool (implements Executor, ExecutorService) - потоки выполняющие задачи
		RecursiveAction<V> расширяет ForkJoinTask, для задач не возвращающих значения
		RecursiveTask<V> - тоже для возвращающих
	<<<Описать>>>


Fork / Join Framework - облегченный тип потоков (классов) по сравнению с Thread. Выполняет задачи меньшим количеством потоков, что эффективнее (видимо от того, что у CPU немного ядер?)
	ForkJoinTask<V> - задачи
	ForkJoinPool (implements Executor, ExecutorService) - потоки выполняющие задачи
	RecursiveAction<V> расширяет ForkJoinTask, для задач не возвращающих значения
	RecursiveTask<V> - тоже для возвращающих
```

### CompletableFuture
```
4. CompletableFuture - класс, наследует Future, работает похоже на Promise в js. Tip. Ему не назначен Executor, поэтому выполнится в специальном ForkJoinPool.commonPool() (если это не поддерживает параллелизм по крайней мере 2го уровня, в этом случае new Thread будет создан, чтобы выполнить каждую задачу). Интерфейс маркер AsynchronousCompletionTask можно использовать для monitoring. Прим. можно вызвать get() или sleep() чтобы подождать выполнения методов описанных ниже.
	supplyAsync(callback) - можно вытащить результат работы callback через get()
	runAsync(callback) - вытащить результат нельзя
	thenAccept() - аналог then() из Promise в js
	thenApply() - в отличии от thenAccept() возвращает результат
	thenApplyAsync() - выполнится не в текущем Thread, а в отдельном
	thenCompose() - можно создать цепочку thenCompose(), результат return попадает в следующий (как для Promise из js)
	thenCombine() - по зовершению 2х задач выполнить 3ю (на самом деле можно комбинировать много задач)

Т.к. выполняется в commonPool, то если нужно выполнить долгу операцию нужно 2ым параметром передавать кастомный pool
--
CompletableFuture - расширяет Future методами комбинирования результатов и обработки ошибок

// Simple Future
CompletableFuture<String> completableFuture = new CompletableFuture<>();
Executors.newCachedThreadPool().submit(() -> {
        Thread.sleep(500);
        completableFuture.complete("Hello");
        return null;
});
String result = completableFuture.get();

// static factory
Future<String> completableFuture = CompletableFuture.completedFuture("Hello");

// короткий аналог Simple Future
CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "Hello"); // исполнится в ForkJoinPool.commonPool()
String result = future.get();

// с указанием pool
CompletableFuture<String> future = CompletableFuture.supplyAsync(() -> "Hi", Executors.newCachedThreadPool());

// цепочка, thenApply вернет результат, принимает Function
CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> "Hello");
CompletableFuture<String> future = completableFuture.thenApply(s -> s + " World");
String result = future.get();

// цепочка, thenApply НЕ вернет результат - void, принимает Consumer
CompletableFuture<Void> future = completableFuture.thenAccept(s -> System.out.println("Computation returned: " + s));

// НЕ цепочка, принимает Runnable, НЕ вернет result
CompletableFuture<Void> future = completableFuture.thenRun(() -> System.out.println("Computation finished."));

// композиция f3(f2(f1(p))); Последовательное выполнение
// отличие от thenApply(): вернет result, а не CompletionStage, т.е. сработает как flatMap()
CompletableFuture<Integer> future = CompletableFuture.supplyAsync(() -> 10)
	.thenCompose(result -> CompletableFuture.supplyAsync(() -> result * 2))
	.thenCompose(result -> CompletableFuture.supplyAsync(() -> result * 5));
	
// комбинация, по завершению двух задач выполнить третью; Параллельное выполнение
CompletableFuture<Integer> future1 = CompletableFuture.supplyAsync(() -> 10);
CompletableFuture<Integer> future2 = CompletableFuture.supplyAsync(() -> 2);
CompletableFuture<Integer> result = future1.thenCombine(future2, (a, b) -> a * b); // a,b - результаты future1, future2

// Параллельный запуск нескольких, результат нужно брать отдельно для каждого через get()
CompletableFuture<Void> combinedFuture = CompletableFuture.allOf(future1, future2, future3);

// как thenApply() но выполнится в ForkJoinPool.commonPool(), использует ForkJoinTask
CompletableFuture<String> future = completableFuture.thenApplyAsync(s -> s + " World");

// обработка ошибок
CompletableFuture<String> completableFuture =  CompletableFuture.supplyAsync(() -> {
      if (name == null) {
          throw new RuntimeException("Computation error!");
      }
      return "Hello, " + name;
  })
  .handle((result, exception) -> s != null ? s : "Hello, Stranger!"); // выполнится всегда, если ошибок не было exception будет пустым
  .exceptionally(err -> (5)); // выполнится ТОЛЬКО если будет ошибка
  
// принудительно вызываем ошибку в Future; как f.completedFuture("Hello")
completableFuture.completeExceptionally(new RuntimeException("Calculation failed!"));

// с jdk 9 https://www.baeldung.com/java-9-completablefuture

```

### FutureTask
FutureTask - реализаци Future

```java
FutureTask<String> future = new FutureTask<String>(new Callable<String>() {
public String call() { return searcher.search(target); }});
executor.execute(future);
```

## Atomic `java.util.concurrent.atomic`
Source: [baeldung atomic](https://www.baeldung.com/java-atomic-variables), [CAS & FAA](https://habr.com/ru/post/319036/), [oracle 11 atomic](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/package-summary.html), [baeldung Volatile vs. Atomic Variables](https://www.baeldung.com/java-volatile-vs-atomic), [baeldung Common Concurrency Pitfalls](https://www.baeldung.com/java-common-concurrency-pitfalls), [oracle jls-17](https://docs.oracle.com/javase/specs/jls/se14/html/jls-17.html#jls-17.7), [oracle atomic jdk 8 about weak](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/atomic/package-summary.html), [JSR-133 Cookbook](https://gee.cs.oswego.edu/dl/jmm/cookbook.html), [wiki Read–modify–write](https://en.wikipedia.org/wiki/Read–modify–write), [stackoverflow CAS vs synchronized performance](https://stackoverflow.com/questions/46092685/cas-vs-synchronized-performance), [baeldung ABA Problem](https://www.baeldung.com/cs/aba-concurrency), [baeldung AtomicStampedReference](https://www.baeldung.com/java-atomicstampedreference)

* **atomic**
  * is **toolkit** of classes that support **lock-free** thread-safe programming
  * **atomic** types does **not have** `equals, hashCode, compareTo` because they can not be used because they are **mutable**
  * [VarHandle](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/lang/invoke/VarHandle.html) has the same atomic methods
  * **atomic weak** methods in Java can be **spurious failure** - can return `false` with **NO REASON**. In the case we have to choose what to do.
    * weak может вернуть false даже если на самом деле внутри CPU проверка вернет true (но мы не увидим), в этом случае код нужно писать из расчета на это
  * **atomic weak** methods in Java **does not guarantee** operation **ordering** (`happens-before` ordering)
  * **atomic weak** methods useful for **updating** counters and statistics when such updates are **unrelated**
    * weak методы полезны для изменения переменных которые не связаны друг с другом, работает как volatile без synchronized, для них не соблюдается ordering, например полезно для счетчиков статистики производительности, когда порядок сложения не важен и не нужны гарантии записи в счетчик
  * **A-B-A problem of CAS** - if value set to: `1) old, 2) new, 3) old` then CAS can not detect that. In C++ to resolve that we can use `var=pointer+value` (we save not only a **value** but also **WHO** save the value). In Java we have **no pointer**, in Java `AtomicStampedReference` solve the problem.
    * Если сохранить 1ое/2ое/1ое значение, то невозможно через CAS понять что значение было изменено на 1ое после 2го, если шаг цикла CAS пропустил установку 2го значения и видит уже последнее 1ое. В C++ можно было прибавить pointer, как признак того какой код изменил значение. В Java для этого используется готовая реализация `AtomicStampedReference`
  * **synchronized vs atomic**
    * in many cases `atomic` operations implemented with **CAS** in **loop**, and it is **faster** then **lock monitor** with `synchronized`
      * **lock coarsening** - оптимизация слияние нескольких synchronized в 1ин, иногда его поведение непредсказуемо и он сливает несколько synchronized в цикле в 1ин synchronized вокруг цикла
      * **unfair synchronized** - is unfair, не честный, он может отпустить несколько потоков чтобы выполнить операцию CAS, чтобы они быстрее завершились, не смотря на то что есть другие потоки которые ждут дольше. Если CAS завершилась неудачей, то thread возвращается в очередь в статусе wait. В том числе это оптимизирует работу в случае если потоков слишком много (часть из них все равно завершится быстро, если они подходят под условие завершения)
    * **MAYBE** in **some** cases where MANY multi threads do **many failed CAS** operations **lock** can be **faster**, e.g. when we have very many threads
  * **No atomic for float/double** ([source](http://download.oracle.com/javase/6/docs/api/java/util/concurrent/atomic/package-summary.html)). **But** we can use `Float/Double.doubleToLongBits` and `Float/Double.longBitsToDouble`. Also here [Guava AtomicDouble](https://guava.dev/releases/22.0/api/docs/com/google/common/util/concurrent/AtomicDouble.html)
	```java
	// use Float as Atomic, AtomicFloat/AtomicDouble
	AtomicInteger bits;
	float initialValue;
	bits = new AtomicInteger(Float.floatToIntBits(123.45));
	var isChanged = bits.compareAndSet(Float.floatToIntBits(123.45), Float.floatToIntBits(67.8));
	bits.set(Float.floatToIntBits(67.8));
	initialValue = Float.intBitsToFloat(bits.get());
	initialValue = Float.intBitsToFloat(bits.getAndSet(Float.floatToIntBits(newValue)));
	var isChanged = bits.weakCompareAndSet(Float.floatToIntBits(123.45), Float.floatToIntBits(67.8));
	```
* **Why to use atomic?**
  * **Lock** (synchronized or lock) makes threads to be `blocked` or `suspended` - only one of threads can work in monitor. The process of suspending and then resuming a thread is **very expensive** (Save thread state, CPU cache flush). Atomic CAS works **without lock**, by checking a value by **one CPU instruction**, for increment and other operations it can use **CAS in a loop**.
    * не делает lock, все потоки в цикле используют CAS чтобы получить последнее значение volatile переменной и установить его, это дешевле чем делать сброс CPU кэша, сохранения состояния потока и восстановление состояния потока. CAS используется в реализации более высокоуровневых операций, таких как increment
  * Code is simple
* **Atomic algorithms**
  * **read-modify-write** - atomic operations like CAS, FAA, TAS etc
  * **Compare-And-Swap** (CAS) - only **one** CPU instruction check value of a var and set it if equal (return boolean `true`). **Do not suspend threads**. If CAS returns `false` then we need to choose: 1) Skip the operation and continue, 2) Retry the operation
    * **Note.** В каждом threads в **loop** используем **CAS** пока не вернет `true` чтобы установить value, только после этого продолжаем выполнение. Этот подход использует некоторые методы atomic классов внутри, т.е. они используют `compareAndSet(...)` в своей реализации. Цикл с проверкой CAS дешевле чем переключать context потоков.
    * **Cons.**
      * Code is more **complex** because we need to choose what to do
      * On **some** (rare) CPU works like **lock** and it is slow
      * **ABA problem** - need to solve that
      * **Starvation** - Multi threads can do CAS - need to solve that
    * **Pros.**
      * Threads are **not locked**
      * May reduce deadlocks
      * May reduce thread competition
  * **Fetch-And-Add** (FAA) - **DO NOT USED IN JAVA**, atomic increment, not implemented, multi thread expensive operation
    * In Java `incrementAndGet()` implemented with CAS in a loop
  * **Test-And-Set** (TAS) - **DO NOT USED IN JAVA**, like CAS but return **old** value
    * In Java TAS is implemented in `getAndSet()`
		```java
		// Source: https://stackoverflow.com/a/56725992
		// TAS can be used to implement a lock
		public class TASLock implements Lock {
			AtomicBoolean state = new AtomicBoolean(false);
			public void lock() { while (state.getAndSet(true)) {} }
			public void unlock() { state.set(false); }
		}
		```
  * **Compare-And-Exchange** (CAE) - **DO NOT USED IN JAVA**
    * `int compareAndExchange(1,2)`
* **Atomic vs volatile** - see volatile chapter
* **Implementation**
  * **Common Methods** for all Atomics
    * `get()` - like read `volatile`, uses `VarHandle.getVolatile​()`
    * `set(val)` - like write `volatile`, uses `VarHandle.setVolatile​(val)`
    * `incrementAndGet()` - atomic increment, like one operation
		```java
		public class SafeCounterWithoutLock {
			private final AtomicInteger counter = new AtomicInteger(0);
			int getValue() { return counter.get(); }
			void increment() { counter.incrementAndGet(); }
		}
		```
    * `lazySet()` - **eventually writes** (когда-нибудь в будущем) a value, may be **reordered** with **subsequent** operations
      * **Efficient** for set **null** for Objects that **will not used** in future to speedup **GC** (delaying the null volatile write)
    * `getAndSet()` - **set()** and return old value
    * `getAcquire()` - return **current** value, and prevent **reorder** for **previous** read/write operations (for **other** threads too)
      * Uses memory barrier to prevent reorder
    * `setRelease​(val)` - set value and prevent **reorder** for **next** read/write operations
      * Uses memory barrier to prevent reorder
    * `getOpaque()` - 
    * `setOpaque(val)` - 
    * `getPlain()` - like read for **non-volatile** var
    * `setPlain(val)` - like write for **non-volatile** var
    * `getAndUpdate(IntUnaryOperator updateFunction)` - return **old** value, set **new** value, can be repeated in a **loop** like **CAS** if update **fail** due to contention among threads
       * `updateFunction` should not have side effect (should not depend on global var)
    * `updateAndGet(IntUnaryOperator updateFunction)` - like `getAndUpdate` but return **new** value
       *  `updateFunction` should not have side effect (should not depend on global var)
    * `compareAndSet(old, new)` - try to set value, return `true/false`, used in a **loop** to implement some another methods (e.g. increment)
      * **Note.** `current value == old value == witness value` - is the same thing
      * Wrapper for **CAS** (Compare-And-Swap)
      * Is used for **implement** some other **atomic methods** in **loop** in different **threads** to check `true/false` and do something (e.g. **retry** set)
		```java
		public final int incrementAndGet() {
			for (;;) {
				int current = get();
				int next = current + 1;
				if (compareAndSet(current, next)) return next;
			}
		}
		```
	* `compareAndExchange(old, new)` - like `compareAndSet()` but return **current** value
	* compareAndExchangeAcquire
	* compareAndExchangeRelease
    * `weakCompareAndSet(old, new)` - **deprecated**, replaced by `weakCompareAndSetPlain()`
    * `weakCompareAndSetPlain(old, new)`
      * **spurious failure** - in Java **weak** methods can return `false` with **NO REASON**, even if `currentValue == expectedValue`
        * **Note.** Actually a reason of **spurious failure** is internal **memory contention** or some **CPU** events.
      * No order of operations (**no** `happens before` ordering)
      * Methods `compareAndSet()` vs `weakCompareAndSetPlain()` - for `weakCompareAndSetPlain()` **CAS** may work or may not, so we can use it but **without guarantee**, use for not important places
      * Usually **used** to collect performance **statistic** or in another not important counter
    * `weakCompareAndSetVolatile(old, new)`
      * like `weakCompareAndSetPlain` but uses `getVolatile()`, like `read volatile` var
      * `happens before` ordering like for `read volatile` var
    * `weakCompareAndSetRelease(old, new)`
      * like `weakCompareAndSetPlain` but with `setRelease(new)`
      * `happens before` ordering for **next** read/write operations
    * `weakCompareAndSetAcquire(old, new)`
      * like `weakCompareAndSetPlain` but uses `getAcquire()`
      * `happens before` ordering for **previous** read/write operations
  * **Common Methods for Number and Reference**
    * `accumulateAndGet(int x, UnaryOperator<V> accumulatorFunction)` - uses `compareAndSet` but call `accumulatorFunction(old, new)`, return **new** value.
      * **Note.** The function should be **side-effect-free**, since it may be re-applied when attempted updates fail due to contention among threads.
		```java
		var ai = new AtomicInteger(0);
		int newVal = ai.accumulateAndGet(old, (old, newVal) -> (old + newVal));
		```
    * `getAndAccumulate(int x, UnaryOperator<V> accumulatorFunction)`​ - like `accumulateAndGet` but return **old** value
      * **Note.** The function should be **side-effect-free**, since it may be re-applied when attempted updates fail due to contention among threads.
    * `updateAndGet​(UnaryOperator<V> updateFunction)`
    * `getAndUpdate​(UnaryOperator<V> updateFunction)`
  * **Common Methods for Number**
    * getAndAdd​
    * addAndGet​
    * decrementAndGet
    * incrementAndGet
    * getAndDecrement
    * getAndIncrement
    * doubleValue()
    * floatValue()
    * intValue()
    * longValue()
  * **Primitive**
    * [AtomicBoolean](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicBoolean.html)
    * [AtomicInteger](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicInteger.html)
    * [AtomicLong](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicLong.html)
    * [AtomicReference](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicReference.html)
  * **Array** - atomic operation for array, `volatile` access to **array** elements. Uses `VarHandle`
    * [AtomicIntegerArray](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicIntegerArray.html)
		```java
		AtomicIntegerArray array = new AtomicIntegerArray(10);
		AtomicIntegerArray array = new AtomicIntegerArray(new int[10]);
		int value = array.get(5);
		array.set(5, 999); // 5 - index, 999 - value
		boolean swapped = array.compareAndSet(5, 999, 123);
		```
    * [AtomicLongArray](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicLongArray.html)
    * [AtomicReferenceArray](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicReferenceArray.html)
  * **Updater** - use Reflection API wrapper of Generic type to make **atomic** methods. **When to use:** if you need update field of **MANY** non-atomic objects like `volatile` you can create only **ONE updater**
    * [AtomicIntegerFieldUpdater](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicIntegerFieldUpdater.html)
		```java
		// 1. usage
		AtomicIntegerFieldUpdater upd = AtomicIntegerFieldUpdater.newUpdater(A.class, name);
		var v = upd.compareAndSet(new A(), "old", "new");

		class A { // 2, use like ATOMIC method wrapper
			static var upd = AtomicIntegerFieldUpdater.newUpdater(A.class, String.class, name);
			void likeAtomicMethod() {
				if (upd.compareAndSet(this, oldBuffer, newBuffer)) {
					// inside object like set field here
				}
			}
		}
		```
    * [AtomicLongFieldUpdater](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicLongFieldUpdater.html)
    * [AtomicReferenceFieldUpdater](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicReferenceFieldUpdater.html)
  * **Stamped**
    * [AtomicStampedReference](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicStampedReference.html) - аналог optimistic lock, детект изменения значения даже если оно изменилось на себя, обычные atomic методы детектируют только изменение на другое значение. stamp - счетчик изменений как version в optimistic lock. Если false - кто-то уже менял ИЛИ value, ИЛИ stamp
		```java
		// детект если баланс изменялся туда-назад другой транзакцией (потоком)
		var account = new AtomicStampedReference<Integer>(100, 0); // 0 - stamp
		var isChanged = account.compareAndSet(current, current - funds, stamp, stamp + 1);
		```
  * **Markable**
    * [AtomicMarkableReference](https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/concurrent/atomic/AtomicMarkableReference.html) - доп. boolean поле marked которое показывает было ли значение изменено, тоже можно использовать для решения ABA problem, при каждом изменении менять с true на false и наоборот, как AtomicStampedReference но только boolean и true/false
		```java
		var employeeNode = new AtomicMarkableReference<Employee>(employee, true);
		var isMarked = employeeNode.compareAndSet(employee, newEmployee, true, false);
		while(!employeeNode.attemptMark(employee, false)); // если mark совпали, то установит новое value
		var marked = employeeNode.isMarked();
		var employee = employeeNode.getReference(); // get value
		```

## Atomic Accumulator, Adder
Source: [1](https://www.baeldung.com/java-longadder-and-longaccumulator)

Суть: это лямбда функции которые lock free, т.е. при выполнении в разных threads сохраняется консистентность переменных

Есть только: LongAccumulator, DoubleAccumulator, LongAdder, DoubleAdder

```java
LongAdder counter = new LongAdder();
Runnable incrementAction = () -> IntStream.range(0, numberOfIncrements)
	.forEach(i -> counter.increment());
for (int i = 0; i < numberOfThreads; i++) {
    executorService.execute(incrementAction);
}

LongAccumulator accumulator = new LongAccumulator(Long::sum, 0L);
Runnable accumulateAction = () -> IntStream.rangeClosed(0, numberOfIncrements)
  .forEach(accumulator::accumulate);

for (int i = 0; i < numberOfThreads; i++) {
    executorService.execute(accumulateAction);
}
```

## Structured Concurrency
Source: [oracle 1](https://docs.oracle.com/en/java/javase/21/core/structured-concurrency.html#GUID-97F95BF4-A1E2-40A3-8439-6BB4E3D5C422), [oracle 2](https://docs.oracle.com/en/java/javase/21/docs/api/java.base/java/util/concurrent/StructuredTaskScope.html)

В Java 21 это preview API, дополнить

# Thread Synchronization Issues, Concurrency Problems, Deadlock, Livelock, Starvation, Race Condition, Resource Contention, Thread Leak

* **Deadlock** (Взаимная блокировка) - два или более потока захватывают разные блокировки и ждут освобождения блокировки, удерживаемой другим потоком.
  * **Исправление:** 1) захватывать блокировки в одном и том же порядке потоками, 2) ReentrantLock.tryLock() с timeout, 3) Минимизировать область синхронизации
  ```java
  synchronized (lock1) { synchronized (lock2) { } } // поток 1, захват и вечное ожидание lock2
  synchronized (lock2) { synchronized (lock1) { } } // поток 2, захват и вечное ожидание lock1
  ```
* **Livelock** (Активная блокировка) - потоки постоянно изменяют своё состояние или уступают друг другу, и ни один из них не может продвинуться вперёд.
  * **Исправление:** 1) переписать алгоритм блокировок чтобы исключить, 2) уступать Thread.yield() или др. чтобы разорвать цикл блокировок, 3) Retry лимит попыток lock
  ```java
  while (condition) { // volatile condition = true
      Thread.yield(); // Поток постоянно уступает, не делая полезной работы
  }
  ```
* **Starvation** (Голодание) - один поток не получает доступ к ресурсу, потому что другие потоки постоянно его занимают (e.g. из-за приоритетов, планировщика задач системы etc).
  * **Исправление:** 1) использовать fair честные блокировки `ReentrantLock(true)`, 2) исправить вытеснение по приоритетам потоков нагрузке на приложение etc
  ```java
  // вытеснение по приоритету потоков, системой или др. или если в коде то:
  synchronized (lock) { while (true) { } } // Долгий процесс, часто или бесконечно удерживает lock
  ```
* **Race Condition** (Состояние гонки) - несинхронизированный доступ нескольких потоков к общему изменяемому состоянию, приводит к некорректным результатам (e.g. вывод в логи не попорядку)
  * **Исправление:** 1) синхронизация synchronized или lock потоков, 2) использовать atomic вместо lock
  ```java
  counter++; // при int операция инкремент (2 операции) в нескольких потоках не atomic (результат будет неверным т.к. переменная кэширована и increment это 2 операции куда вклинятся потоки)
  ```
* **Resource Contention** (Конкуренция за ресурсы) - потоки пытаются получить доступ к одному и тому же ресурсу, снижает производительность.
  * **Исправление:** 1) уменьшить количество synchronized/lock кода, 2) использовать atomic или  неблокирующие `Concurrent Collection`, 3) использовать Virtual Threads - снижает затраты на создание и управление потоками
  ```java
  synchronized (resource) { } // много потоков пытается получить ресурс и ждут очереди
  ```
* **Thread Leak** (Утечка потоков) - при создании потоков без корректного завершения (например, забытый вызов `shutdown()` у `ExecutorService`, бесконечные циклы в коде потока, нету `t.join()`, после вызова wait никогда не вызывается `notify()`)
  * **Исправление:** 1) shutdown() или shutdownNow() для ExecutorService, 2) try-with-resources для закрытия ресурсов, 3) использовать `Executors.newVirtualThreadPerTaskExecutor()` т.к. они быстрее создаются и завершаются
  ```java
  // Пример, Поток остаётся в состоянии ожидания, так как notify() не вызывается
  synchronized (lock) {
      try {
          lock.wait(); // остаётся в состоянии ожидания вечно, нет notify()
      } catch (InterruptedException e) {} }
  
  // исправление, авто закрытие чтобы не было утечки
  try (var executor = Executors.newVirtualThreadPerTaskExecutor()) {
      executor.submit(() -> {
          // Код задачи
      });
  }
  ```
* **Priority Inversion** (Инверсия приоритетов) - когда высокоприоритетный поток ожидает ресурс, удерживаемый низкоприоритетным, а промежуточные потоки препятствуют завершению низкоприоритетного потока
  * **Note.** В Java в отличии от др. систем нет наследования приоритетов потоков - поэтому этот способ нельзя использовать для исправления
  * **Исправление:** переписать логику чтобы уменьшить количество кода в lock/synchronized
  ```java
  // короткий пример привести трудно, суть что низкоприоритетный поток удерживает lock, необходимый высокоприоритетному
  ```
* **Spurious Wakeups** (Ложные пробуждения) - поток, ожидающий с помощью метода wait(), пробуждается без явного уведомления, что может привести к неверной обработке состояния
  * **Исправление:** использовать цикл while для проверки условия при wait, а не конструкцию if
  ```java
  // это пример исправление проблемы, а не проблемы, wait внутри while:
  synchronized (lock) { while (!condition) { lock.wait(); } } // если не проверять условие в while то потом может сам без причины пробудиться
  ```

# Double checked locking
Source: [cs.umd.edu](http://www.cs.umd.edu/~pugh/java/memoryModel/jsr-133-faq.html), [Use a correct form of the double-checked lock](https://wiki.sei.cmu.edu/confluence/display/java/LCK10-J.+Use+a+correct+form+of+the+double-checked+locking+idiom)
```
Сначала проверяется условие блокировки без какой-либо синхронизации;
поток делает попытку получить блокировку, только если результат проверки говорит о том, что получение блокировки необходимо.

Из-за того, что потоки могут для оптимизаций менять местами строки кода при выполнении,
    Double checked locking может НЕ СРАБОТАТЬ. Эта проблема привела к появлению happens before (volatile) в Java Memory Model
    
Часто ИСПОЛЬЗУЕТСЯ при реализации lazy инициализаций, например lazy Singleton переменная всегда volatile (в т.ч. ссылка как Я понял???).

Double checked locking - Блокировка с двойной проверкой
    - параллельный шаблон проектирования, сначало проверяется флаг (true/false) блокирован или нет, а только потом делается ПОПЫТКА синхронизации
    - На некоторых языках и/или на некоторых машинах невозможно безопасно реализовать данный шаблон. Поэтому иногда его называют анти-паттерном. Такие особенности привели к появлению отношения строгого порядка «happens before» в Java Memory Model и C++ Memory Model.

// double check lock
// 0. неверный но рабочий вариант, больше кода в synchronized
final class Foo {
  private Helper helper = null;
 
  public synchronized Helper getHelper() {
    if (helper == null) {
      helper = new Helper();
    }
    return helper;
  }
  // ...
}

// double check lock
// 1. acquire/release semantics for volatile
final class Foo {
  private volatile Helper helper = null;
 
  public Helper getHelper() {
    if (helper == null) {
      synchronized (this) {
        if (helper == null) {
          helper = new Helper();
        }
      }
    }
    return helper;
  }
}

// альтернатива double check lock
// 2. Static Initialization, eager static - гарантирует что объект создастся до того как станет видимым
// минус - решение не lazy
final class Foo {
  private static final Helper helper = new Helper();
 
  public static Helper getHelper() {
    return helper;
  }
}

// альтернатива double check lock
// 3. Initialize-on-Demand, Holder Class Idiom - работает быстрее чем double check lock
// минус - не может инициализировать lazy поля объекта
final class Foo {
  // Lazy initialization
  private static class Holder {
    static Helper helper = new Helper();
  }
 
  public static Helper getInstance() {
    return Holder.helper;
  }
}

// double check lock
// 4. Immutable - уменьшает количество unsynchronized read до 1го, за счет локальной переменной helper
final class Foo {
  private Helper helper = null;
  
  public Helper getHelper() {
    Helper h = helper;       // Only unsynchronized read of helper
    if (h == null) {
      synchronized (this) {
        h = helper;          // In synchronized block, so this is safe
        if (h == null) {
          h = new Helper(42);
          helper = h;
        }
      }
    }
    return h;
  }
}

// double check lock
// 5. ThreadLocal - используется для хранения, использование применяет happens-before
final class Foo {
  private final ThreadLocal<Foo> perThreadInstance =
      new ThreadLocal<Foo>();
  private Helper helper = null;
 
  public Helper getHelper() {
    if (perThreadInstance.get() == null) {
      createHelper();
    }
    return helper;
  }
 
  private synchronized void createHelper() {
    if (helper == null) {
      helper = new Helper();
    }
    // Any non-null value can be used as an argument to set()
    perThreadInstance.set(this);
  }
}
```
# happens before
Source: [oracle](https://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html#jls-17.4.5), [JSR-133 Cookbook](https://gee.cs.oswego.edu/dl/jmm/cookbook.html)
```
В JMM не используется понятие кэш CPU, используется Main Memory (не кэш) и Working Memory (кэш, память потока)

Каждый CPU хранит кэш, потоки в Java используют кэш CPU (на уровне железа). valotile говорит не использовать кэш.
Причем кэш CPU общий для всех потоков. ПОЭТОМУ если в Thread хотя бы ОДНА переменная valotile,
    то кэш выключится для ВСЕХ СОСЕДНИХ (других полей класса) переменных даже если они НЕ valotile.
    (ПРИМ. проверить ГАРАНТИРОВАННО ли соседние с valotile переменные тоже становятся valotile из-за общего кэша!!!)
    
Конструкции вызывающие happens before: final, volatile, or synchronized,
    Дополнительно: static initializers, и новые Thread могут видеть текущее состояние памяти родителя без synchronization
    (для всего остального порядок выполнения не гарантирован)

Выделяют
    synchronized Order - тоже игнорирует кэш CPU для потоков при входе в блок и записывает данные при выходе из блока.
    happens before Order - команды CPU которые говорят операциям не менять порядок операций местами даже если это выгодно.
        т.е. все операции ДО операции с valotile переменной будут в строгом порядке!
        
synchronized vs happens before (valotile) - это разные вещи, НЕ взаимозаменяемые.
    synchronized - может выполнять только ОДИН поток
        и внутри synchronized блока операции внутри JVM могут поменяться местами (в отличии от happens before которая все выполняет по порядку)
        НУЖНО ЗНАТЬ (общая инфа ???): в synchronized напрямую читает и пишет в кэш, другой поток не может загрузить свой context в тот же кэш (не будет переключения контекста).

Кроме того happens before автоматически действует для:
    Синхронизация и мониторы:
        1. Захват монитора (начало synchronized, метод lock) и всё, что после него в том же потоке.
        2. Возврат монитора (конец synchronized, метод unlock) и всё, что перед ним в том же потоке. 
        3. Возврат монитора и последующий захват другим потоком.
    Запись и чтение:
        4. запись в любую переменную и последующее чтение её же
        5. Всё, что в том же потоке перед записью в volatile-переменную, и сама запись.
        6. volatile-чтение и всё, что после него в том же потоке.
        7. Запись в volatile-переменную и последующее считывание её же. (аналогия: volatile запись как возврат монитора, а чтение как захват)
            (Для объектных переменных (например, volatile List x;) столь сильные гарантии выполняются для ссылки на объект, но не для его содержимого. ПРОВЕРИТЬ!)
    Обслуживание объекта: 
        8. static инициализации (блок и переменная)
        9. инициализация final в конструкторе
        10. Любая работа с объектом и finalize()
    Обслуживание потока: 
        11. Запуск потока и любой код в потоке.
        12. Зануление переменных, относящихся к потоку, и любой код в потоке.
        13. Код в потоке и join(); код в потоке и isAlive() == false.
        14. interrupt() потока и обнаружение факта останова.

happens-before relationship - гарантирует что переменная записанная 1им потоком будет видна (visible) в 2ом.
Когда использовать: если код x должен сработать до y, то между ними должно быть: x happens before y relationship
Причина: на уровне jvm одна операция может выполняться в несколько шагов (как запись в регистр, а потом операция), а 2ой поток вклинивается между ними.
	каждый поток хранит копию переменной в своем регистре и какое-то время она не видна другим потокам
	еще вариант - поток может читать код измененный в другом потоке, но оптимизатор может выкинуть это чтение т.к. оно по его мнению не влияет на код
		(а оно влияет, видимо речь про чтение регистров jvm и последующую запись)
	на обычных CPU кэши могут общаться между собой, поэтому снижение производительности при happens-before небольшое

- Initialization safety rule - правило happens-before по которому в constructor можно установить final поля для создания immutable thread safe objects
	- ВСЕ поля в immutable Object должны быть final даже если у них нет getter, только тогда это правило заработает!!!
	- или другое опеределение: все потоки будут видеть корректные значения final установленные в constructor даже если ссылка на этот object видна в других потоках во время последовательной инициализации final
	- это особое правило которое не комбинируется с другими happens-before, а дополняет его

constructor не может быть synchronized т.к. это не имеет смысла, т.к. к нему может иметь доступ только пораждающий поток (из документации)

Типы happens-before:
- Single thread rule - в одном потоке все операции это happens-before
- Volatile variable rule: volatile делает так что read & write переменной происходят последовательно всеми параллельными потоками (в 1ом потоке write происходит до read во 2ом)
- Monitor lock rule: отпуск монитора потоком A случится после захвата потоком B этого же монитора (отпуск и захват всегда последовательны - для одного монитора)
- Thread start rule: Thread.start() 1им потоком случится всегда до старта потока (который стартует)
- Thread join rule: код ПОСЛЕ a.join() 1им потоком случится всегда после остановки потока (который останавливается)
- Transitivity: если A happens-before B, B happens-before C, то A happens-before C
- Interruption rule: вызов interrupt в 1ом потоке будет до его детекта в потоке которые делают interrupt (т.е. до InterruptedException throwing, isInterrupted, interrupted)
- Finalizer rule: constructor() завершается до finilize()
Дополнительно:
	- volatile имеет тот же эффект как synchronized блок вокруг read/write операций (который тоже делает happens before), но без захвата monitor
	- каждый synchronized метод выполнится до другого synchronized метод того же монитора (object), последний увидит все изменения сделанные первым
	- данные установленные потоком видны в потоке созданном позднее
	- данные установленные в 1ом synchronized блоке видны потоку которые попадет в тот же монитор позднее (тот же lockObject)
	- final поля можно безопасно читать в других потоках
	- для 32bit java чтение long и double не atomic и для него нужно делать volatile т.к. читает по 32 bit
Важно:
	- volatile нужно менять в synchronized блоке т.к. последовательные операции не атомарны для параллельных потоков
		volatile int count = 0;
		Object lock = new Object();
		public void increment() {
			synchronized (lock) {
				count++; // в synchronized хотя и volatile т.к. read+write могут быть параллельными потоками
			}
		}}
	- valotile Object obj; действует только на ссылку, чтобы действовало на Object нужно всего его поля пометить valotile или final
```

# Java Memory Model, JMM, JSR 133
Source: [baeldung concurrency interview](https://www.baeldung.com/java-concurrency-interview-questions), [baeldung jmm interview](https://www.baeldung.com/java-memory-management-interview-questions), [habr jmm](https://habr.com/ru/post/440590/), [medium](https://medium.com/platform-engineer/understanding-java-memory-model-1d0863f6d973), [wiki](https://en.wikipedia.org/wiki/Java_memory_model), [oracle Threads and Locks](https://docs.oracle.com/javase/specs/jls/se7/html/jls-17.html), [JSR-133 Cookbook](https://gee.cs.oswego.edu/dl/jmm/cookbook.html)

`Дополнить`
```
Это спецификация описывающая как ведет себя общая память потоков когда к которой потоки имеют одновременный доступ.
Т.к. optimizer, compiler, CPU etc могут менять местами чтение/запись параллельных потоков к памяти, или даже удалять обращения.
Т.е. jvm писывает как ведет себя synchronized по отношению к другим synchronized и happens before (его правилам).
И если программа correctly synchronized (правильно расставлены synchronized), тогда она будет правильно работать (sequentially consistent)
```

# Green Threads
Source: [wikipedia Green thread](https://en.wikipedia.org/wiki/Green_thread)

**Green threads** - удалены из Java т.к. имели недостатки, есть альтернативные реализации: Kilim, Quasar.
Потоки которые управляются jvm (runtime library) и не привязаны к OS thread, а не OS (native thread),
это не virtual thread.
Эмуляция многопоточности в userspace, а не в kernel space.
Работают быстрее для activation, synchronization, медленнее с i/o, context switching.

# Virtual Threads
Source:
[oracle Virtual Threads](https://docs.oracle.com/en/java/javase/21/core/virtual-threads.html#GUID-DC4306FC-D6C1-4BCC-AECE-48C32C1A8DAA),
[baeldung Difference Between Thread and Virtual Thread in Java](https://www.baeldung.com/java-virtual-thread-vs-thread),
[netflixtechblog Java 21 Virtual Threads - Dude, Where’s My Lock?](https://netflixtechblog.com/java-21-virtual-threads-dude-wheres-my-lock-3052540e231d),
[baeldung Working with Virtual Threads in Spring](https://www.baeldung.com/spring-6-virtual-threads),
[baeldung How to use virtual threads with ScheduledExecutorService](https://www.baeldung.com/java-scheduledexecutorservice-virtual-threads),
[baeldung Reactor WebFlux vs Virtual Threads](https://www.baeldung.com/java-reactor-webflux-vs-virtual-threads),
[baeldung Scoped Values in Java](https://www.baeldung.com/java-20-scoped-values),
[baeldung New Features in Java 21](https://www.baeldung.com/java-lts-21-new-features),
[baeldung How Many Threads Can a Java VM Support?](https://www.baeldung.com/jvm-max-threads),
[baeldung Light-Weight Concurrency in Java and Kotlin](https://www.baeldung.com/kotlin/java-kotlin-lightweight-concurrency),
[baeldung @ConditionalOnThreading Annotation Spring](https://www.baeldung.com/spring-conditionalonthreading)

**Project Loom** - название проекта по созданию Virtual Threads

**Platform Thread** (`java.lang.Thread`) - wrapper вокруг OS Thread, количество ограничено количеством OS threads. Имеют stack и др. OS thread resources.  
**Virtual Thread** (`java.lang.VirtualThread`) - как **Platform Thread**, но при вызове `blocking I/O operation` поток virtual thread **suspended** пока эта blocking операция не будет выполнена и OS thread будет освобожден и может выполнять др работу для другой virtual thread.

Использование **Virtual Thread**:
* Имеет меньший call stack - зависит от i/o blocking operations
* Используется когда для threads проводят много времени для i/o blocking operations
* Не предназначены для выполнения CPU-intensive operations, они не быстрее Platform Thread 
* В jvm могут быть миллионы virtual threads, поэтому **THreadLocal** и **InheritedTHreadLocal** нужно использовать осторожно
* Virtual Thread предназначены для scale (**higher throughput**), не для speed (**lower latency**)
* carrier - называют Platform Thread на которую сделан mapping ее Virtual Thread
* mount - когда Virtual Thread привязана к Platform Thread
* **Pinned Virtual Thread** - когда virtual thread не может быть unmount: 1) поток в synchronized блоке, 2) вызов native method или foreign function (Foreign Function and Memory API)
* **Scheduling Virtual Thread** - это virtual thread которая запущена в Scheduler, она unmount когда: 1) sheduler заснул, 2) когда выполняет io blocking operation
* концепция при которой в java нельзя создавать много потоков не работает с virtual threads, теперь можно
* использовать blocking threads дешево с virtual thread
* с virtual threads нужно писать код именно в synchronous style с blocking operations, т.к. если код будет асинхронным (реактивным), то разница в скорости выполнения будет мала (т.к. реактивный код и так быстрый)
* virtual threads проще дебажить чем асинхронный
* асинхронный код (reactive) и фреймворки не сильно выиграют от virtual threads. Асинхронные фремворки **не рекомендуется** использовать с virtual threads, т.к. асинхронные фреймворки используют для реализации ThreadLocal, а при использовании virtual threads может создаваться **очень много ThreadLocal** т.к. таких потоков очень много.
  * **Scoped Values** - preview в jdk21, эффективная замена Thread Local
* если есть **synchronized** блок с доступом к общему для потоков ресурсу, то потоки могут зависнуть, т.к. они **pinned** и не могут быть **unmount**
  * В Java 21 для ускорения и чтобы потоки не зависали нужно заменить `synchronized` на `ReentrantLock`, но только `synchronized` для долгих операций, для коротких заменять `synchronized` не нужно
  * Note. Данная проблема решится в новых версиях jvm [1](https://news.ycombinator.com/item?id=39012631), [2](https://mail.openjdk.org/pipermail/loom-dev/2024-May/006632.html), пока не решилась можно использовать lock вместо synchronized
* virtual threads нельзя использовать в pool, нужно просто создавать Virtual Threads без pool, т.к. pools созданы для потоков которых не хватает (Platform Threads)
* **Note.** При использовании connection pool он уже в себе имеет Semaphore с лимитом connections, не нужно дополнительно ограничивать лимит при его использовании
* **Scheduler** для virtual thread можно реализовать через sleep или SingleThreadExecutor, через pool нельзя, в jdk21 нет встроенной поддержки Scheduler для virtual thread (как для обычных потоков). Через SingleThreadExecutor можно выполнять virtual thread периодически
* Platform Thread не управляются sheduler который управляет virtual thread (нет траты ресурсов на переключение между потоками), эффективнее для CPU intensive и realtime задачами

```java
// 1. Создание через Thread.Builder
Thread t = Thread.ofVirtual().name("MyThread").start(task);

// 2. Создание через Executors, НЕ НУЖНО использовать pool для virtual thread типа sharedThreadPoolExecutor
try (ExecutorService myExecutor = Executors.newVirtualThreadPerTaskExecutor()) {
    Future<?> future = myExecutor.submit(() -> System.out.println("Running thread"));
    future.get();
    System.out.println("Task completed");
}

// 3. ограничиваем количество потоков используя Semaphore, например 10 одновременных запросов по rest
// использовать Executors.newFixedThreadPool(10) нельзя, и любой другой pool
Semaphore sem = new Semaphore(10);
Result foo() {
    sem.acquire();
    try { return callLimitedService(); }
	finally { sem.release(); }
}

// альтернатива synchronized для Java 21 для долгих операций, будет не нужна в новых версиях
lock.lock(); // ReentrantLock
try { frequentIO(); }
finally { lock.unlock(); }

// Scheduler через sleep - выполнится только 1 раз, не периодически
ExecutorService virtualThreadExecutor = Executors.newVirtualThreadPerTaskExecutor();
virtualThreadExecutor.submit(() -> {
        try {
            Thread.sleep(Duration.of(delay, unit));
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
        task.run();
    }
}

// Scheduler через SingleThreadExecutor
// schedule(), scheduleAtFixedRate(), and scheduleWithFixedDelay() - могут выполняться периодически
static Future<?> schedule(Runnable task, int delay, TimeUnit unit, ExecutorService executorService) {
    return executorService.submit(() -> {
        ScheduledExecutorService singleThreadScheduler = Executors.newSingleThreadScheduledExecutor();

        try (singleThreadScheduler) {
            singleThreadScheduler.schedule(task, delay, unit);
        }
    });
}
```

# QA

## Synchronize abstract method
abstract нельзя использовать с synchronized, но можно:
```java
public synchronized final void foo() {
    doFoo();
}
protected abstract void doFoo();
```

## Как принудительно остановить поток?
```
В Java 8 нет метода, который бы принудительно останавливал поток.
Класс Thread содержит в себе скрытое булево поле, которое называется флагом прерывания. Установить этот флаг можно вызвав метод interrupt() потока.
Проверяем флаг прерывания:
    interrupt() - ставит флаг прерывания в true???
    bool isInterrupted() - проверяем флаг прерывания
    Thread.interrupted() - проверяем флаг прерывания и СБРАСЫВАЕМ ЕГО

В Java 8 нет метода, который бы принудительно останавливал поток.
Класс Thread содержит в себе скрытое булево поле, которое называется флагом прерывания. Установить этот флаг можно вызвав метод interrupt() потока.
Проверяем флаг прерывания:
    interrupt() - ставит флаг прерывания в true???
    bool isInterrupted() - проверяем флаг прерывания
    Thread.interrupted() - проверяем флаг прерывания и СБРАСЫВАЕМ ЕГО
    
в java предпочтительнее для прерывания потока использовать свои реализации с проверкой своего флага прерывания
чтобы прерываемый поток подождал своей блокировки, а не прерывался сразу,
это нужно например чтобы корректно завершалась работа с ресурсами работу с которыми нельзя прервать, например с InputStream/OutputStream
Другими словами: нужно реализовать метод с public флагом (или методом) прерывания, чтобы фоновый поток мог управлять дочерним
реализуется через проверку флага:
public class ControlSubThread implements Runnable {
    private final AtomicBoolean running = new AtomicBoolean(false);
    private Thread worker;
    public void interrupt() { // может выкинуть InterruptedException при sleep или другой блокировке
        running.set(false);
        worker.interrupt();
    }
    public void run() { 
        running.set(true);
        while (running.get()) {
            try { 
                Thread.sleep(interval); 
            } catch (InterruptedException e){ // может случиться если interrupt() вызван когда Thread блокирован или sleep
                Thread.currentThread().interrupt();
            }
            // тут код потока
            } 
    }
}

прерывание в ExecutorService
    executor.shutdownNow();  // will interrupt the task
    executor.awaitTermination(3, TimeUnit.SECONDS); // timeout
```
## В каком случае будет выброшено исключение InterruptedException, какие методы могут его выбросить?
```
Методы, требующие обработку этого исключения: wait, sleep, join.
Исключение будет выброшено, если флаг interrupt у потока true.
```

## Можно ли создавать новые экземпляры класса, пока выполняется static synchronized метод?
Да.

## Предположим в методе run возник RuntimeException, который не был пойман.
```
Если в дочернем потоке упадет Exception, то метод run() аварийно завершится и исключение будет передано в главный поток.
    Далее в консоль будет выведен стектрейс, приведенный ниже.
Если исключение не обрабатывать, то нить (вызванная в методе run()) просто аварийно завершится.
    Восстановить работу нити после такого сценария нельзя, можно только создать нить заново.
```

## Какие стандартные инструменты Java вы бы использовали для реализации пула потоков?
```
Коротко:
    можно использовать LinkedList queue и private final Thread[] pool;
    при execute(Runnable r) добавлять в pool
```
## Что такое «атомарные типы» в Java?
```
Имеют операцию compare-and-set, быстрая.

compareAndSet() - механизм оптимистичной блокировки и позволяет изменить значение value,
    только если оно равно ожидаемому значению (т.е. current).

В пакет java.util.concurrent.atomic входят 9 видов атомарных переменных (AtomicInteger;
    AtomicLong; AtomicReference; AtomicBoolean;
    формы для массивов атомарных целых чисел;
    длинные (long);
    ссылки;
    а также атомарные с пометкой Класс эталона (reference), которые атомарно обновляют две величины).

С модификатором volatile, то гарантируется выполнение отношения happens-before,
    что ведет к тому, что измененное значение этой переменной увидят все потоки.
```

## Что такое ExecutorService?
```
ExecutorService исполняет асинхронный код в одном или нескольких потоках.
Создание инстанса ExecutorService’а делается
    либо вручную через конкретные имплементации (ScheduledThreadPoolExecutor или ThreadPoolExecutor),
    но проще будет использовать фабрики класса Executors:
        service.submit(new Runnable() {}); // 
```
## Зачем нужен ScheduledExecutorService?
```
Иногда требуется выполнение кода асихронно и периодически или требуется выполнить код через некоторое время.
Он позволяет поставить код выполняться в одном или нескольких потоках и сконфигурировать интервал или время, на которое выполненение будет отложено.

Пример:
    Executors.newSingleThreadScheduledExecutor().schedule(new Runnable() { ... }, 5, TimeUnit.SECONDS);
```
## Способы создания потока

1. **Runnable** - better then **Thread**, has no class inheritance
   1. Implement code container `class MyRunnable implements Runnable`
   2. Add code to `public void run() { ... }` - thread **entry point**
   3. Run thread `new Thread(new Runnable(){...}).start();`
2. **Thread**
   1. Override `class Thread { start() { ... } }`: `new Thread(Thread(){start();...}){...}`
   2. Run thread `new Thread(new Runnable(){...}).start();`
3. **Executor and Callable**
	```java
	ExecutorService pool = Executors.newFixedThreadPool(3);
	// call() аналог run()
	Callable<Integer> callable = new Callable() { call() { return "OK"; } };
	Future<Integer> future = pool.submit(callable); // submit() аналог start()
	String result = future.get(); // get() == join()
	```
4. **Executor and Runnable** `Executor.execut(new Runnable(){ run(){} });` **Note.** `future.get()` возвращает значение (ИЛИ `null`, если используется `Runnable`, а не `Callable`)

Thread will **stop** when `run()` or `start()` is end.
