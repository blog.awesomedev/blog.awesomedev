Список необходимых знаний для понимания Android.

**Android** - система на ядре Linux, к которой привязаны проприетарные драйвера и доработки для работы с спец. оборудованием. Ядро может быть настроено на малое энергопотребление и прочее для мобильных устройств. Часто имеет своб реализацию API для работы с Bluetooth и прочим оборудованием (в отличии от стандартных реализаций для настольных систем).  
В системе установлена модифицированная Java машина (для скорости) в которой запускаются программы.  
Для разработки программ используется спец. фреймворк, для доступа к API устройства через Java прослойку. Также доступны некоторые API самого Linux. Можно писать код и на C++. Интерфейс и права доступа приложений часто описываются с помощью спец. конфигурационных файлов через GUI в Android Studio.

***

**Recovery** - аналог UEFI/BIOS для Android, на самом деле в отличии от них содержит урезанное ядро Linux, без драйверов. В отличии от OS может писать в защищенные разделы системы, т.е. например может изменить параметры или дать программе права **root**. Стандартный Recovery постовляемый с устройством чаще всего может только сбрасывать настройки (но есть и исключения).

***

Источник: [1](https://www.oxygensoftware.ru/ru/news/articles/264-all-truth-about-edl), [2](https://www.getdroidtips.com/5-methods-to-enter-edl-mode-on-any-xiaomi-smartphones-qualcomm/), [известное исследование EDL и эксплоит для старых устройств](https://alephsecurity.com/2018/01/22/qualcomm-edl-1/?resub), 4pda:[1](https://4pda.ru/forum/index.php?showtopic=643084&view=findpost&p=37986712)  
**Emergency Download Mode** (EDL, testpoint, режим 9008) - режим в котором доступна запись и извлечение данных с любых разделов. На разных устройствах доступ можно получить по разному, через: adb, fastboot, замыкание контактов, удержание различных кнопок при подключении кабеля. Этот режим может быть и заблокирован.

Для чего EDL? Через него можно восстановить любой телефон с убитой прошивкой (например при неудачной прошивке). Извлечь разделы системы для создания кастомных прошивок. Установить кастомный bootloader

EDL обычно используется для CPU от **Qualcomm**, при этом официальные утилиты для работы с ним нельзя просто скачать, их раздают только клиентам компании, но в сети много утекших утилит. И есть неофициальные реализации.

Чтобы работать в этом режиме с начало нужно загрузить в специальные неперезаписываемый раздел спец. файл **prog_firehose** ("программер" на рус. жаргоне). Файл может иметь формат **.elf**, **.mbn** или другой. Начальный загрузчик на плате который по цепочке запускает загрузчик **prog_firehose** проверяет подпись **prog_firehose**. В норме только производитель может подписать данный файл. На практике в сети много утекших файлов. Некоторые производители открыто выкладывают такие файлы. В старых устройства проверка подписи может не работать из-за багов или ее может не быть совсем. Еще способ перейти в EDL - замкнуть контакты на eMMC (почти гарантированный), но эта плата расположена глубже в телефоне и до нее труднее добраться.  
**Note.** Xiaomi обычно поставляет этот файл со своими прошивками, но не факт что их можно прошить через что-то кроме **MiFlash** или даже через нее если у вас нет аккаунта сервис центра.

Для доступа к EDL нужен специальный драйвер для USB. С которым телефон начинает работать в режиме **COM** порта (через который и идет работать с разделами телефона). Для Linux есть неофициальные open source драйвера от [edl](https://github.com/bkerler/edl). Для Windows скорее всего только утекших от **Qualcomm**.

На современных устройствах даже данные извлеченные через EDL зашифрованы и прочитать их нельзя.

Некоторые криминалистические утилиты могут использовать эксплоиты для старых устройств и баги в реализации EDL для извлечения данных. При этом крупные производители такие как Google и Samsung хорошо защищают свои телефоны от этого. Но в замен доступ к их EDL может и быть совсем заблокированным.

Загрузка файла **prog_firehose** происходит по спец. протоколу **Sahara**. После загрузки файла **prog_firehose** в режиме EDL будет доступно извлечение разделов (системы) и файлов по протоколу **Firehose**.  
**Note.** Существует также старый протокол **dload**

* **Неофициальные утилиты:**
  * [sahara](https://github.com/openpst/sahara) - может загружать **prog_firehose**, но не может работать с разделами по протоколу **Firehose**
  * [edl](https://github.com/bkerler/edl) - может загружать файл **prog_firehose** и писать/читать разделы

Схема разделов. **PBL** - первичный загрузчик, который после загрузки **prog_firehose** передает управление ему и позволяет по протоколу **Firehose** изменять разделы системы, в том числе **fastboot**.
```
[Primary Bootloader (PBL)]
 |
 `---EDL---.
            [Programmer (Firehose)]
            `- Commands (through USB)

[Primary Bootloader (PBL)]
|
`---NORMAL BOOT---.
                  [Secondary Bootloader (SBL)]
                  |-. 
                  | [Android Bootloader (ABOOT)]
                  | `-.    
                  |   [boot.img]
                  |   |-- Linux Kernel
                  |   `-- initramfs
                  |       `-.
                  |         [system.img]
                  |
                  `-[TrustZone]
```
***

**Custom Recovery** - так можно назвать проекты, которые модифицируют родной recovery (а точнее создают заново на основе родного ядра системы и device tree), чтобы можно было получить доступ к системному разделу телефона и менять любые параметры или устанавливать custom firmwares (форки Android).

* **Список проектов Custom Recovery**
  * [TWRP](https://twrp.me/Devices/) - самый популярный
    * Обычно чтобы создать **TWRP** берут основу от похожего устройства **или** используют минимальный вариант **TWRP**, который может только вытащить информацию о устройстве по которой потом можно создать полноценный **TWRP**: 
    * Библиотека которая может вытащить инфу о устройстве чтобы создать TWRP: [libbootimg](https://github.com/Tasssadar/libbootimg)
  * [SkyHawk Recovery Project (SHRP)](https://skyhawkrecovery.github.io/Devices.html) - ответвление TWRP с большими возможностями:
    * прошивка утилиты **Magisk**
    * включение **Camera 2 api** (это API может быть недоступно в некоторых устройствах по каким-то причинам или отключено, но оно нужно для хорошей работы программ для фото и видео)
    * файл менеджер с поддержкой работы с **zip**
    * поддержка **модулей** расширяющих возможности **SHRP**
  * [clockworkmod](https://www.clockworkmod.com/) - самый старый проект, уже мало популярный, сейчас он разрабатывает в основном утилиты для Android, например:
    * [Набор драйверов adb для Windows](https://adb.clockworkmod.com/)

[Code Aurora](https://www.codeaurora.org/) - проект куда производители телефонов публикуют свои наработки по ядру Linux, оттуда можно брать код чтобы адаптировать под смартфон

[Project Treble](https://source.android.com/devices/architecture#hidl) (или HAL) - прослойка между прошивками устройств и Android, позволяет работать любой версии Android со старыми прошивками, т.е. обновлять Android без обновления старых прошивок устройств телефона. Низкоуровневые драйверы сохраняются в специальном разделе `/vendor`. А остальная часть операционной системы в системном образе **GSI**.

[GSI-прошивка](https://source.android.com/setup/build/gsi) (Generic System Image) - образ системы, файл `.img`, прошивается в раздел `/system`

* **Типы разделов Android:**
    - [Проверить какой тип разделов можно тут](https://github.com/phhusson/treble_experimentations/wiki) - тут же ссылки и на другой код связанный с устройствами, например на **microG**. И набор команд для прошивки некоторых телефонов 
    - A/B структура разделов
    - Aonly

[microG Project](https://microg.org/) - альтернативная open source реализация closed source библиотек и программ от Google, без которых в Android не работают важные программы и функции (e.g. навигация). Используется в некоторых (если не во всех) Custom ROMs. В том числе это альтернатива `Google Play Services API` и `Google Maps Android API`

[Android Open Source Project (AOSP)](https://source.android.com/) - оригинальный Android от Google, без сервисов Google, на его основе создаются все прошивки Android. При этом может содержать отправку телеметрии рекламодателям, поэтому его обычно переделывают и изначально в нем мало настроек, их тоже добавляют.

**Android Read Only Memory** (Android ROMs, Custom ROMs, Custom Firmwares) - модифицированная прошивка Android, добавлены опции, часто вырезана телеметрия. Есть прошивки в которых оставлена правильная работа Google Service и там телеметрию отключить нельзя, есть альтернативная реализация Google Service, но она может содержать ошибки и ненадежна.  
**Custom ROM** которые полностью проходят тесты на безопасность и работоспособность мне не известны. Разве что те которые идут вместе со спец. моделями телефонов специально для программистов или в рамках экспериментов компаний вроде OnePlus или Oppo, когда они устанавливают LineageOS родной прошивкой.

[AndroidOne](https://www.android.com/one/) - программа Google с немного модифицированным оригинальным Android, по ней все производители которые используют AndroidOne должны во время выпускать обновления, не должны менять состав прошивки (не устанавливать туда рекламу и не собирать телеметрию без разрешения). Имеет оригинальные Google Services, работают все программы от Google, немодифицированная прошивка работает предсказуема со всеми программами Android. Имеет меньше настроек чем оригинальный Android.  
**Может** содержать измененные программы для спец. устройств, например если камера имеет необычные функции, то приложение Camera поставляется измененным для работы с этими функциями.

* **Список популярных и относительно безопасных Custom ROM**
  * **Прошивки, которые выглядят более вменямыми чем другие**
    * [LineageOS](https://wiki.lineageos.org/devices/) (раньше называлась `CyanogenMod`) - самая популярная кастомная прошивка, вырезана телеметрия, добавлены дополнительные настройки, расширенное управление правами доступа приложений
    * [AOSP Extended](https://stats.aospextended.com/) - доработанная **AOSP**, берет некоторые улучшения из различных других Custom ROMs (например от **LineageOS**). Note. Мало поддерживаемых устройств, она не устарела?
    * [Pixel Experience](https://download.pixelexperience.org/devices) (также имеют расширенную версию `Plus` c доп. настройками) - создана как можно ближе к оригиналу Android используемому в телефонах **Google Pixel**, нацелена дать нормально работать с сервисами Google и в стиле оригинального Android.
    * [crDroid](https://crdroid.net/dl.php) - основан на **LineageOS**, добавлены дополнительные настройки
    * [EvolutionX](https://evolution-x.org/#/download) - цели как у **Pixel Experience**, имеет доп. настойки, настроена на максимальную производительность в видео играх и графике
  * **Прошивки с кривыми сайтами, что не вызывает доверия** (но для некоторых устройств существуют только эти прошивки)
    * [Resurrection Remix OS](https://get.resurrectionremix.com/) - берет некоторые улучшения из различных других Custom ROMs (например от **LineageOS**), много фич. Note. Странный кривой сайт без нормального меню.
    * [Paranoid Android](https://paranoidandroid.co/downloads) - кастомная прошивка с доп. доработками безопасти и стабильности, плюс доп. конфигурация UI. Note. Очень странный кривой сайт
    * [Havoc-OS](https://sourceforge.net/projects/havoc-os/files/) (и [тут](https://github.com/Havoc-OS))
    * [Bliss ROM](https://blissroms.com/)



**Device Tree** - дерево устройств, информация о том какие устройства есть в телефоне, чтобы **Custom ROM** мог на нем работать, также нужны дополнительные оригинальные драйвера из родной прошивки или работа через специальную прослойку для дрйверов (эту прослойку создала Google специально, чтобы производители могли обновлять прошивку не затрагивая драйвера). Обычно **Device Tree** как раз формируют с помощью урезанного **custom recovery** и используют для создания **Custom ROM** или полноценного **custom recovery**.  
Существуют различные программы для чтения и записи в разделы Android, которые имеют необычный формат или странно разделены на куски.  
Часто обновления **оригинальной** прошивки обновляют драйвера и меняют структуру разделов телефона. Поэтому перед прошивкой Custom ROM часто рекомендуют сначало обновиться до последней версии оригинальной прошивки.

**Таблица разделов Android** - диск Android разбит на множество разделов, иногда имеющих необычные файловые системы. Раздел может быть разбит на куски так что делит даже файлы на куски (как разрезан). Некоторые системные разделы содержат драйвера в том числе для **GSM**, или спец. файлы содержащие **IMEI**, или камеры. И если услучайно удалить такой системный раздел можно сделать телефон рабочим навсегда и исправить его смогут только на спец. оборудовании сервисных центров (или не смогут). В некоторых телефонах системные разделы запись в которые может сломать телефон защищены от записи, в некоторых нет и телефон сломать возможно перепроишвкой не в тот раздел.

# Компиляция TWRP
* **Шаги:**
  1. Вытащить оригинальное device tree
  2. Вытащить оригинальныю таблицу разделов (быть осторожным чтобы не затереть драйвера GSM и прочего - иначе сломается)
  3. Вытащить конфиги для дисплей
  4. Вставить в TWRP и скомпилировать