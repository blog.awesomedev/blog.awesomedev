Источник: [тут](https://archive.org/details/academictorrents_e31e54905c7b2669c81fe164de2859be4697013a/lectures/001-intro/Compilers+0.1+01-02+Structure+of+a+Compiler+(13m53s).mp4)

# Common

**Части компилятора:**
1. Lexical Analysis - делит программу на tokens, можно сказать пары `<Class, String>`
2. Parsing - формирует из tokens дерево и разбивает tokensa на группы: identifiers, operators, digits, spaces etc
3. Semantic Analysis - ищет ошибки по смыслу программы, например: анализ типов (программу понять сложно, на этом шаге выполняется только приблезительный анализ)
4. Optimization - замена конструкций на более эффективные и очистка неиспользуемого кода
5. Code Generator - генерация кода, замено псевдо кода который создал компилятор на команды CPU

**В современных компиляторах:**
1. Lexical Analysis, Parsing - эти части маленькие, т.к. большая часть работы перекладывается на готовые библиотеки
2. Semantic Analysis - эта часть средних размеров
3. Optimization - большая часть компилятора
4. Code Generator - маленькая часть, т.к. работает на базе готовых решений

**Почему языков программирования много:** т.к. разные языки для разных специализаций и совместить все фичи в одном трудно.

**Языки можно разделить на (специализации):**
* **Scientific computing:** хорошая поддержка FP чисел (floating point), хорошая поддержка arrays и операций с ними, хорошая поддержка параллелизма
* **Business applications:** persistence (транзакционность и надежность), report generation (логирование, дебаг, отслеживание операций), data analysis (операции над данными и их анализ). E.g. **SQL**
* **System programming:** resource control (контроль над ресурсами памяти и системы), real time сщтыекфштеы (быстрая работа в реальном времени и быстрые ответы устройствам). E.g. **C/C++**


# Lexical Analysis

**Token Class** == **Class** of the Token - тип (роль) токена, например: Identifier, Keywords, brakets, Numbers, Whitespace etc

**Цель Lexical Analysis** - оперелить роль для всех tokes (на этом этапе tokens это strings). Т.е. составить пары: `<Class, String>`. Эти пары будут переданы на этап **Parsing**

```
код -> LA -> <Class, String> -> P -> ...
```

**E.g.** выражение `foo = 42` будет разбито на массив пар: `<Id, "foo">, <OP, "=">, <Int, "42">`