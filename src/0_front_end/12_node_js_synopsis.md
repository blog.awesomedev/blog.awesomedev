* [Node Version Manager (nvm) for Windows](https://github.com/coreybutler/nvm-windows) - менеджер пакетов
* [Node.js](https://nodejs.org/en/)

```bash
npm update                              # обновить пакеты проекта
npm update -g                           # обновить пакеты глобально
npm update -D && npm update -S          # обновить пакеты проекта
npm i -g npm@latest       # установка последней версии npm
npm audit fix   # Angular иногда просит набрать ее, чтобы пофиксить уязвимости этой командой

npm install -g @angular/cli # установка глобально

# зависимости для Windows
npm i -g --production --vs2015 --add-python-to-path windows-build-tools
npm i -g --production --add-python-to-path windows-build-tools node-gyp

# зависимости для многих проектов
sudo npm i -g --unsafe-perm typescript tslint protractor web-ext
```

**Пример package.json**
```json
{
  "name": "my-app",
  "version": "1.0.0",
  "description": "my-app",
  "main": "index.js",
  "repository": {
    "type": "git",
    "url": "git+https://url.com/repo.git"
  },
  "author": "my name",
  "license": "MIT",
  "bugs": {
    "url": "https://url.com/repo/issues"
  },
  "homepage": "https://url.com/repo#readme",
  "devDependencies": {
    "clean-webpack-plugin": "^3.0.0",
    "copy-webpack-plugin": "^5.0.4",
    "terser-webpack-plugin": "^1.4.1",
    "webpack": "^4.39.3",
    "webpack-cli": "^3.3.7"
  },
  "dependencies": {
    "lodash.clonedeep": "^4.5.0"
  },
  "scripts": {
    "prod": "webpack --config webpack.config.js --mode production",
    "watch": "webpack --config webpack.config.js --watch --mode development",
    "build": "webpack --config webpack.config.js --mode development"
  }
}

```