**Stack**
* [nvm](https://github.com/coreybutler/nvm-windows) - установщик Node.js
* [Node.js](https://nodejs.org) - установить через **nvm**
* [Electron](https://www.electronjs.org/) (с TypeScript) - обертка вокруг движка Chromium с доп. функциями
* [Angular](https://angular.io/)
* [better-sqlite3](https://github.com/JoshuaWise/better-sqlite3)
* [TypeORM](https://typeorm.io)
* [typeorm-model-generator](https://github.com/Kononnable/typeorm-model-generator) - генерирует entity TypeORM по DB
* ionic + typeorm https://github.com/coturiv/ionic-typeorm-starter

**Как вариант:**
* [Готовый шаблон Electron + Angular](https://github.com/maximegris/angular-electron)
* [Готовые шаблоны Electron приложений](https://www.electronjs.org/docs/tutorial/boilerplates-and-clis)
* [Инструкция по правильной установке Node.js](https://github.com/JoshuaWise/better-sqlite3/blob/master/docs/troubleshooting.md)
* [Angular с Electron по шагам](https://www.sitepoint.com/build-a-desktop-application-with-electron-and-angular/)
* [ionic + TypeORM](https://www.techiediaries.com/ionic-angular-typeorm-custom-webpack-configuration/) - инструкция
* [ionic-typeorm-starter](https://github.com/coturiv/ionic-typeorm-starter) - неофициальный, но новый пример
* [Добавление electron к ionic](https://devdactic.com/ionic-desktop-electron/)

# Для Windows перед установкой всех пакетов
```sh
npm install --global --production --vs2015 --add-python-to-path windows-build-tools
npm install --global --production --add-python-to-path windows-build-tools node-gyp

# опционально читать https://www.npmjs.com/package/node-gyp и установить windows-build-tools вручную
npm explore npm -g -- npm install node-gyp@latest # обновить node-gyp
```

# ionic

Источник: [тут](https://www.youtube.com/watch?v=Jrj2qMPWCZ0)

**Stack**
* https://ionicframework.com/getting-started

**Install**
* `npm install -g @ionic/cli`
* `ionic start photo-gallery tabs --type=angular --capacitor`
* `ionic serve` - пробуем запустить
* `npm install ngx-electron electron`
* `npm install -D electron-packager`
* `ionic build` - это нужно запустить т.к. чтобы все заработало в промежутке нужно собрать
* `npx cap add electron`
* `npx cap open electron`
* `ionic build && npx cap copy` - запускать при изменении проекта
* `npx cap open electron` - после команды выше можно запустить эту, откроет окно electron
* В `index.html` поставить `<base href="./" />`
* Добавить в `app.module.ts` строку `import { NgxElectronModule } from 'ngx-electron';` и добавить `NgxElectronModule` в `imports: [NgxElectronModule]`
* В component достать плагины `import { Plugins } from '@capacitor/core';` и `const { LocalNotifications, Clipboard, Modals } = Plugins;`
* Добавить `import { ElectronService } from 'ngx-electron';` в component Angular и использовать
* Добавить `"electron:win": "electron-packager ./electron SimonsApp --overwrite --asar=true --platform=win32 --arch=ia32 --prune=true --out=release-builds --version-string.CompanyName=CE --version-string.FileDescription=CE --version-string.ProductName='Simons Electron App'"` в `package.json`

**Update**
* `ng update @angular/cli @angular/core --allow-dirty`