Список плагинов на js + описание категорий UI и особенностей для которых они используются

# Js frameworks, engines, libraries, builders
* **Frameworks** - современные фреймворки для создания сайтов и веб приложений (в т.ч. **Progressive Web App**)
  * [Angular](https://angular.io/)
  * [Create React App](https://github.com/facebook/create-react-app) - на самом деле это библиотека, но ее чаще всего используют с redux, redux-logic, redux-observables, mobx и получается framework; Последние версии React поддерживают hooks - замену HOC, что делает react пригодным для использования без библиотек.
  * [React Native](https://reactnative.dev/) - для разработки кросс плотформенных приложений, не использует DOM, исползует средства системы, по сути реализация native приложения вместо использования движка js (на самом деле используется встроенный свой движок)
  * [Ionic Framework](https://ionicframework.com) - кросс платформенный фреймворк, можно писать на React или Angular
* **JS engines** - выполнение js кода, возможна компиляция в исполняемый файл
  * **QuickJS** - быстрый js движок от создателя QEMU, умеет компилировать в native исполняемые файлы, без DOM
  * **Hermes** - быстрый движок от facebook, оптимизированный для запуска **React Native**
* **Template engines** - шаблонихаторы, в настоящее время шаблонизаторы используются реже, вместо них используют Virtual DOM или другие системы детекции (ту что в Angular например)
  * [Nunjucks](https://mozilla.github.io/nunjucks/) - от Mozilla
  * [Dust.js](https://www.dustjs.com/) - от linkedIn, считается одним из самых быстрых
  * [hyperHTML](https://github.com/WebReflection/hyperHTML) - интересный проект, пока я считаю его сырым, это легкая альтернатива Virtual DOM (т.е. react и прочим рисовальщикам страниц)
* **DOM & BOM manipulation, utils js plugins** - manipulation with DOM, BOM and various js functions
  * [jQuery](https://jquery.com/)
    * [jQuery UI](https://jqueryui.com/) - дополнение к jQuery для построения UI и работы с его событиями, например с анимациями, готовые компоненты: таблицы, формы, кнопки etc. Готовые layouts и css стили, готовый grid.
    * [jQuery Mask Plugin](https://igorescobar.github.io/jQuery-Mask-Plugin/) - плагин на jQuery для масок полей ввода
    * [jQuery Validation Plugin](https://jqueryvalidation.org/) - плагин для валидации полей ввода
    * [DataTables plug-in for jQuery](https://github.com/DataTables/DataTables)
    * [Accordion.JS](https://github.com/awps/Accordion.JS)
  * [lodash](https://lodash.com) - много готовых функций для работы с arrays и objects, как RxJs основанных на теории кортежей (подобно SQL); кроме этого много и других функций. Сами функции можно импортировать по отдельности от всей библиотеки.
    * [cloneDeep](https://lodash.com/docs/#cloneDeep) - клонирование объекта рекурсивно со всеми sub properties
    * [delay](https://lodash.com/docs/#delay)
    * [throttle](https://lodash.com/docs/#throttle)
    * [debounce](https://lodash.com/docs/#debounce) - вызывает функцию с задержкой времени, сбрасывая задержку при повторном вызове функции - позволяет избавиться от множественных вызовов ограничив их минимальным промежутком времени.
    * [isNull](https://lodash.com/docs/#isNull)
    * [isNaN](https://lodash.com/docs/#isNaN)
    * [isNil](https://lodash.com/docs/#isNil)
    * [isObject](https://lodash.com/docs/#isObject) - проверка на `arrays`, `functions`, `objects`, `regexes`, `new Number(0)`, and `new String('')`
* **JS. Bundlers, compilers, obfuctators, deobfuctators, parsers** - современные компиляторы и оптимизаторы jsмогут перекрывать возможности друг друга и не всегда понятно что есть что. Например **webpack.js** это больше **компилятор и оптимизатор** к которому можно подключать плагины, а **gulp.js** это больше **система сборки** к которой можно подключить **webpack.js**
  * [webpack](https://webpack.js.org/) - собирает файлы в 1ин или несколько js файлов, оптимизирует, может извлекать их них или встраивать в них HTML, css, другие ресурсы, может подключать локлальный веб сервер для быстрой авто перезагрузки страницы (через плагин) и авто rebuild при любом изменении файла, подключать компилятор TypeScript и другие и еще много других функций.
  * [rollup.js](https://rollupjs.org/guide/en) - система сборки похожая на webpack
  * [gulp.js](https://gulpjs.com/) - система сборки, в отличии от **webpack.js** можно писать гибкие этапы компиляции, использует **webpack.js** как компилятор и оптимизатор
  * [Babel](https://babeljs.io/) - компилятор который можно подключать к системам сборки (webpack, gulp). Компилирует новые версии js, react и других языков (над множеств js) в обычный js (в указанную версию) с добавлением полифилов на js если нужно.
  * [UglifyJS](https://github.com/mishoo/UglifyJS) - парсит, сжимакт, обфусцирует, деобфусцирует JavaScript. Можно подключать плагином к webpack, использовать как утилиту командной строки, подключать на в html страницу.
  * [Terser](https://github.com/terser/terser) - аналог **UglifyJS**, который поддерживает новые фичи на момент написание, но **Terser** не поддерживает некоторые функции **UglifyJS**, например не может форматировать код
  * [prettier](https://github.com/prettier/prettier) - форматирует код, позволяет задать свои правила форматирования
  * [JSFuck](https://github.com/aemkei/jsfuck) - обфускатор js кода
  * [Google Closure Compiler](https://github.com/google/closure-compiler) - online приложение и программа на Java для оптимизации и minification js кода
  * [JavaScript obfuscator](https://github.com/javascript-obfuscator/javascript-obfuscator) - obfuctator
* **CSS. Bundlers, compilers, obfuctators, deobfuctators, parsers**
  * [UglifyCSS](https://github.com/fmarcia/UglifyCSS) - аналог **UglifyJS** для **CSS**, но не делает сложный парсинг, в основном только сжимает и удаляет комментарии
  * [CSSTree](https://github.com/csstree/csstree) - парсинг CSS текста в AST объекты и обратно
  * [postcss-media-query-parser](https://github.com/dryoma/postcss-media-query-parser) - парсинг css для [CSS media query](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries)
  * [postcss-selector-parser](https://github.com/postcss/postcss-selector-parser) - парсинг css для **CSS selector**
  * [parse-srcset](https://github.com/albell/parse-srcset) - парсинг css для [HTML5 srcset](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/img#attr-srcset=)
  * [parse-css-font](https://github.com/jedmao/parse-css-font) - парсинг css для [font property](https://developer.mozilla.org/en-US/docs/Web/CSS/font#font-variant-css21=)
* **UI js plugins**
  * **Form plugins** - плагины для элементов форм, полей ввода, кнопок, контроль полей ввода, валидация, форматирование и пр.
    * **Autocomplete plugins** - поля для стилизованного авто завершение текста при вводе (поиск)
      * [autoComplete.js](https://github.com/TarekRaafat/autoComplete.js)
      * [Awesomplete](https://github.com/LeaVerou/awesomplete/)
      * [Horsey](https://github.com/bevacqua/horsey/)
      * [Autocomplete.js](https://github.com/lorecioni/autocomplete.js)
    * **Multiple Select Plugins** - стилизованный select с дополнительными функциями: панель отображения выбранного, поиск, auto complete
      * [SelectPure](https://github.com/dudyn5ky1/select-pure)
      * [multiple-select](https://github.com/wenzhixin/multiple-select)
      * [multi.js](https://github.com/fabianlindfors/multi.js)
    * **Checkbox, Radio, Range plugins** - плагины для анимации и кастомизации Checkbox, Radio, `<input type="range">`, эти элементы неудобны в стандартных вариантах и их сложнее стилизовать вручную.
      * [icheck-material](https://github.com/bantikyan/icheck-material/) - для Checkbox и Radio
      * [rangeslider.js](https://github.com/andreruffert/rangeslider.js)
    * **Mail plugins** - стилизация и поведение ссылок на email и формы для работы с email
      * [MailtoUI](https://mailtoui.com/)
    * **File uploader plugins** - кнопки и области на странице для выгрузки файлов в интернет, много опций, интеграция с онлайн сервисами, камерой для Android etc.
      * [Uppy](https://github.com/transloadit/uppy)
    * **Mask plugins** - маски ввода, правила допустимых символов и разметка с подсветкой для ввода в input и другие элементы
      * [Cleave.js](https://nosir.github.io/cleave.js/)
    * **Date & time picker plugins** - UI для выбора даты и времени, в том числе диапазонов и показ календаря
      * [flatpickr](https://github.com/flatpickr/flatpickr)
      * [pickadate.js](https://github.com/amsul/pickadate.js)
      * [Date Range Picker](https://github.com/dangrossman/daterangepicker)
  * **Toolkit plugins** - создают сетки и другие структуры для размещения элементов, имеют готовые css и js для создания большинства популярных элементов, анимации, реакции на жесты, горячие клавиши etc.
    * [Bootstrap](https://getbootstrap.com/)
    * [jQuery UI](https://jqueryui.com/)
    * [Formstone](https://github.com/Formstone/Formstone/) - набор кастомизаций, стилизаций, реакций на события etc
    * [Flowy](https://github.com/alyssaxuu/flowy) - готовый компонент со всеми элементам для построения блок-схем
  * **Layout plugins** - создают сетки и другие структуры для размещения элементов
    * [Magic Grid](https://github.com/e-oj/Magic-Grid)
  * **Visualization and graphic plugins**
    * [D3.js](https://d3js.org/) - визуализация разным структур данных: графики, графы, деревья и прочее
    * [Chart.js](https://www.chartjs.org/) - построение графиков
    * [uPlot](https://github.com/leeoniya/uPlot) - может рисовать всевозможные типы графиков
  * **Sticky plugins** - для закрепление элементов в областях при скролинге
    * [HC-Sticky](https://github.com/somewebmedia/hc-sticky)
    * [sticky-js](https://github.com/rgalus/sticky-js)
  * **Table plugins** - добавляют таблицам сортировку, фильтраци, pagination etc
    * [Vanilla-DataTables](https://github.com/Mobius1/Vanilla-DataTables/)
  * **Tooltip & Popover plugins** - плагины для показа стилизованных tooltip и Popover (вслывающий контент)
    * [Tippy.js](https://github.com/atomiks/tippyjs)
    * [Tooltipster](https://github.com/calebjacob/tooltipster)
    * [Protip](https://github.com/wintercounter/Protip)
    * [Tipped](https://github.com/staaky/tipped)
    * [Popper.js](https://github.com/popperjs/popper-core)
  * **Notification plugins** - стилизованные уведомления
    * [SimpleNotification](https://github.com/Glagan/SimpleNotification)
    * [Push](https://github.com/Nickersoft/push.js)
  * **Spinner plugins** - показ анимации загрузки с или без показа прогресса загрузки
    * [spin.js](https://github.com/fgnass/spin.js)
    * [Three Dots](https://github.com/nzbin/three-dots/)
    * [text-spinners](https://github.com/maxbeier/text-spinners)
    * [SpinThatShit](https://github.com/MatejKustec/SpinThatShit)
  * **Progress Bar Plugins** - показ анимации загрузки с показом прогресса загрузки
    * [nanobar](https://github.com/jacoborus/nanobar/)
  * **Modal plugins** (popup plugins) - всплывающие окна всех типов
    * [SweetAlert2](https://github.com/sweetalert2/sweetalert2) - кастомизация и силизация Alert окон
  * **Accordion plugins** - создание элементов типа `Accordion`
    * [Squeezebox.js](https://github.com/nobitagit/squeezebox-vanilla)
    * [Handorgel](https://github.com/oncode/handorgel/)
    * [Accordion](https://github.com/michu2k/Accordion/)
  * **Dropdown Menu plugins** - выпадающее меню
  * **Tabs plugins**
    * [Tabby](https://github.com/cferdinandi/tabby)
  * **Scrolling animation plugins** - анимирует или меняет поведение scroll, например делает его плавным или с эффектом отскока, или медленно проявляющиеся следующие части страницы
    * [Lax.js](https://github.com/alexfoxy/lax.js)
    * [josh.js](https://github.com/sdether/josh.js)
    * [Locomotive Scroll](https://github.com/locomotivemtl/locomotive-scroll)
    * [AOS](https://github.com/michalsnik/aos)
    * [Jump.js](https://github.com/callmecavs/jump.js)
    * [OverlayScrollbars](https://github.com/KingSora/OverlayScrollbars) - скрывает реальный scroll и заменяет на кастомный
    * [FullScreen Slider](https://github.com/ykob/fullscreen-slider)
    * [ScrollReveal](https://github.com/jlmakes/scrollreveal)
  * **Slider plugins** - создает слайдер анимированные и кастомизированные элементы с эффектами
    * [Swiper](https://github.com/nolimits4web/swiper)
    * [Lory](https://github.com/loryjs/lory)
  * **Pagination plugins** - делят страницу на куски между которыми можно переключаться
    * [Paged.js](https://gitlab.pagedmedia.org/tools/pagedjs/)
  * **Select box & pills plugins** - создает и оформляет pills элементы в том числе внутри текстовых или других блоков
    * [Choices.js](https://github.com/jshjohnson/Choices)
  * **Media players** - плагины плеера для аудио, видео
    * [Bideo.js](https://github.com/rishabhp/bideo.js) - добавляет видео на страницу как фон
  * **Image viever plugins** - просмотр изображений, в том числе анимированных с возможностью запуска по правилам или по клику
    * [Freezeframe.js](https://github.com/ctrl-freaks/freezeframe.js/) - плеер для gif
    * [baguetteBox.js](https://github.com/feimosi/baguetteBox.js)
    * [lightgallery.js](https://sachinchoolur.github.io/lightgallery.js/)
  * **Map pluggins** - отоброжение и кастомизация карт
    * [Leaflet](https://github.com/Leaflet/Leaflet)
  * **Currency plugins** - работа с валютами с учетом особенностей работы с числами в js, форматирование и интернациализация валют
    * [currencyFormatter.js](https://github.com/osrec/currencyFormatter.js)
  * **Online WYSIWYG editors** - различные онлайн редакторы документов, блоков etc
    * [Editor.js](https://github.com/codex-team/editor.js)
    * [MediumEditor](https://github.com/yabwe/medium-editor)
    * [TinyMCE](https://github.com/tinymce/tinymce)
    * [SheetJS](https://github.com/SheetJS/sheetjs) - как Excel
  * **Online code & text highlight editors** - редакторы кода и текста с подсветкой синтаксисам и автодополнением
    * [CodeMirror](https://github.com/codemirror/CodeMirror)
  * **Effects & animation plugins** - различные эффекты и анимации для различных элементов
    * [wiv.js](https://github.com/jjkaufman/wiv.js) - добавляет анимированные рамки для div
    * [Textillate.js](https://github.com/jschr/textillate) - анимация ввода текста
    * [darkmode.js](https://github.com/sandoche/Darkmode.js) - добавляет темную тему сайту
    * [Scene.js](https://github.com/daybrush/scenejs) - библиотека анимации
    * [simpleParallax.js](https://github.com/geosigno/simpleParallax.js/) - добавляет parallax animation на любое изобращение
    * [Focus Overlay](https://github.com/mmahandev/focusoverlay) - создает эффект overlay (любой) для элементов в фокусе, фокус эмулируется и его можно устанавливать любому элементу
    * [Choreographer-js](https://github.com/christinecha/choreographer-js) - библиотека анимации
    * [anime.js](https://github.com/juliangarnier/anime/) - библиотека анимации
    * [vivus.js](https://github.com/maxwellito/vivus) - анимированная рисовка SVG изображений по сценарию (e.g. на события)
  * **Hot key plugins**
    * [Hotkeys](https://github.com/jaywcjlove/hotkeys/)
    * [Egg.js](https://github.com/mikeflynn/egg.js/)
  * **Skeleton Screen (aka Content Placeholder) UI plugins** - пока страница загружается показывает скелет страницы с пустыми блоками данных, данные каждого блока загружаются независимо друг от друга
    * [Content Placeholder for Tailwind CSS](https://github.com/javisperez/tailwindcontentplaceholder)
  * **Placeholder plugin** - поведение и эффекты placeholder в полях `input`, `textarea` etc
  * **Breadcrumbs Plugins** - путь по подразделам статей, опционально переход к подразделы по клику на часть пути. Т.к. routing часто зависит от js фреймворка, то часто плагины специфичны и сделано для опеределенных фреймворков
    * [ngx-breadcrumbs](https://github.com/peterbsmyth/ngx-breadcrumbs) - авто генератор **Breadcrumbs** на основе **Angular Route**
* **Extended API and functionality**
  * **Database plugins** - обертки вокруг api для localStorage, IndexedDB, Cookies, in memory реализация SQL баз и т.п.
      * [localForage](https://localforage.github.io/localForage/) - key-value база, использует IndexedDB, может использовать localStorage
      * [Dexie.js](https://dexie.org/) - обертка IndexedDB, оптимизирует запросы
      * [ZangoDB](https://github.com/erikolson186/zangodb) - MongoDB на IndexedDB
      * [JsStore](https://jsstore.net/) - реализация SQL на IndexedDB
      * [MiniMongo](https://github.com/mWater/minimongo) - MongoDB на localStorage
      * [PouchDB](https://pouchdb.com/) - CouchDB на IndexedDB
      * [idb](https://www.npmjs.com/package/idb) - улучшенный api для IndexedDB
      * [idb-keyval](https://www.npmjs.com/package/idb-keyval) - key-value база, использует IndexedDB
      * [sifrr-storage](https://www.npmjs.com/package/@sifrr/storage) - key-value база, использует IndexedDB, может использовать localStorage или Cookies
      * [lovefield](https://github.com/google/lovefield) - SQL на IndexedDB
      * [$mol_db](https://github.com/hyoo-ru/mam_mol/tree/master/db) - TypeScript для IndexedDB
      * [SQLite](https://github.com/sql-js/sql.js/) - SQLite на js, in-memory
      * [TypeORM](https://github.com/typeorm/typeorm) - ORM на js, поддерживает MySQL / MariaDB / Postgres / CockroachDB / SQLite / Microsoft SQL Server / Oracle / SAP Hana / sql.js / MongoDB
      * [absurd-sql](https://github.com/jlongster/absurd-sql) - реализация SQLite которая использует IndexedDB для хранения данных, сохраняя их страницами. По тестам проекта операции чтения работают быстрее чем native операции самого IndexedDB
  * **Date & time plugins** - работа с датой и временем включая часовые пояса
    * [Luxon](https://github.com/moment/luxon) - самая популярная для обычной работы с датами
    * [Day.js](https://github.com/iamkun/dayjs)
    * [date-fns](https://github.com/date-fns/date-fns)
    * [js-Joda](https://js-joda.github.io/js-joda/)
    * [Moment.js](https://github.com/moment/moment/) - устаревшая, но популярная библиотека, рекомендуется использовать **Luxon**
  * **Video**
    * [ffmpeg.wasm](https://github.com/ffmpegwasm/ffmpeg.wasm) - ffmpeg на js
    * [videojs-record](https://github.com/collab-project/videojs-record) - запись аудио или видео с устройств (e.g. камера) или онлайн трансляции
* **Misc js plugins**
  * [mathjs](https://github.com/josdejong/mathjs) - выполнения математиеческих выражений написанных текстом на спец. языке
  * [WebTorrent](https://github.com/webtorrent/webtorrent) - торрент клиент на js через WebRTC который можно встраивать на страницы или в приложения на js
  * [zone.js](https://www.npmjs.com/package/zone.js) - библиотека, часть Angular, добавляет обертки вокруг async функций и позволяет отслеживать их и результат их работы и обновлять по этим событиям UI, т.к. в стандартном js для этого механизмов нету (`async`, `setTimeout` и прочее).
  * [FileSaver.js](https://github.com/eligrey/FileSaver.js/) - можно сохранять сгенерированные внутри браузера файлы на диск, показывает окошко сохранения и имеет некоторый контроль над процессом сохранения, но из-за ограничений браузера контроль не полный и нельзя сделать нормальный revoke ссылки на файл (по крайней мере пока что)
  * [StreamSaver.js](https://github.com/jimmywarting/StreamSaver.js) - как **FileSaver.js**, но работает асинхронно и дает сохранять в виртуальную файловую систему (не в chromes sandboxed file system!)
  * [iro.js](https://iro.js.org/) - color picker
  * [pagemap](https://github.com/lrsjng/pagemap) - mini map для страниц
  * [Moveable](https://github.com/daybrush/moveable) - делает элементы перетаскиваемыми, вращаемыми etc
  * [Shave](https://github.com/yowainwright/shave) - динамическое обрезание текста в блоке `текст...` и стилизация
  * [Loud Links](https://github.com/MahdiF/loud-links) - добавляет звуки на действия на странице
  * [whatwg-mimetype](https://github.com/jsdom/whatwg-mimetype) - манипуляции с mime типами
  * [content-security-policy-parser](https://github.com/helmetjs/content-security-policy-parser) - [Content-Security-Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Security-Policy) (CSP) parser
* **Development plugins**
  * [winston](https://github.com/winstonjs/winston) - logger для js