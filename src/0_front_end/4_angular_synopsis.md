- [Ключевые источники](#ключевые-источники)
- [Useful commands](#useful-commands)
  - [Tools](#tools)
  - [angular](#angular)
- [Angular life cycle](#angular-life-cycle)
- [Общая структура](#общая-структура)
- [Создание фэйкового api](#создание-фэйкового-api)
- [Components](#components)
- [Directives](#directives)
- [Binding types](#binding-types)
- [Фичи с TypeScript](#фичи-с-typescript)
- [ViewChild, ContentChild, ViewChildren, ContentChildren](#viewchild-contentchild-viewchildren-contentchildren)
- [`<ng-content>` vs `*ngTemplateOutlet`](#ng-content-vs-ngtemplateoutlet)
- [Providers](#providers)
- [modules and components (Примеры кода) в Root Component](#modules-and-components-примеры-кода-в-root-component)
- [`entryComponents: []` (deprecated)](#entrycomponents-deprecated)
- [viewProviders](#viewproviders)
- [События инициализации (дописать!!!)](#события-инициализации-дописать)
- [HostListener, HostBinding](#hostlistener-hostbinding)
- [Inject DOM Element](#inject-dom-element)
- [Изменение DOM (ViewContainerRef)](#изменение-dom-viewcontainerref)
- [Angular CDK](#angular-cdk)
- [Router](#router)
- [`DomSanitizer` и отображение `data:base64` в `<img>` по `onchange`](#domsanitizer-и-отображение-database64-в-img-по-onchange)
- [Angular Change Detection](#angular-change-detection)
- [Change Detection. React vs. Angular](#change-detection-react-vs-angular)
- [Решение ошибки `expression has changed after it was checked`](#решение-ошибки-expression-has-changed-after-it-was-checked)
- [Ссылки на новые фичи в Angular](#ссылки-на-новые-фичи-в-angular)

# Ключевые источники
* https://angular.io/docs - документация
  * https://angular.io/guide/deprecations - **deprecated** функции и переход с них
  * https://angular.io/resources?category=development - инструменты и плагины
  * https://angular.io/guide/typescript-configuration - конфигурация компилятора TypeScript
  * https://angular.io/guide/angular-compiler-options - конфигурация компилятора TypeScript специфичная для Angular
* Про структуру приложения (core, feature shared модули) [тут](https://angular.io/guide/styleguide#overall-structural-guidelines)
* [Understanding Angular modules (NgModule) and their scopes](https://medium.com/@cyrilletuzi/understanding-angular-modules-ngmodule-and-their-scopes-81e4ed6f7407) - почему структура модулей именно такая
* Про виды модулей [тут](https://angular.io/guide/module-types#summary-of-ngmodule-categories) (Domain, Routed, Routing, Service, Widget, Shared)
* [Manipulation techniques using ViewContainerRef](https://indepth.dev/exploring-angular-dom-manipulation-techniques-using-viewcontainerref/) - как изменять DOM, удалять, создавать (например для скрытия частей страницы для незарегистрированных)
* Router
  * серия статей про то как работает router ([Router States and URL Matching](https://medium.com/angular-in-depth/angular-routing-series-pillar-1-router-states-and-url-matching-12520e62d0fc), [Navigation Cycle](https://medium.com/angular-in-depth/angular-router-series-pillar-2-navigation-d050286bf4fa), [Lazy Loading, AOT, and Preloading](https://medium.com/angular-in-depth/angular-router-series-pillar-3-lazy-loading-aot-and-preloading-a23a046c51f0))
  * про отмену через `switchMap` события устаревшей операции перехода [тут](https://medium.com/angular-in-depth/new-in-angular-7-better-navigations-79267db452c0)
  * про использование `UrlTree` в `Guard` и приоритет guards [тут](https://medium.com/angular-in-depth/new-in-angular-v7-1-updates-to-the-router-fd67d526ad05)
* [Angular Material](https://material.angular.io/guide/getting-started)
* Решения - примеры готового кода
  * `directive` удаляет или добавляет элемент в зависимости от прав пользователя [тут](https://medium.com/mikael-araya-blog/controlling-visibility-of-component-based-on-user-claims-using-angular-directives-deb350b7493)
  * Обработка ошибок и авторизация [тут](https://jasonwatmore.com/post/2020/09/09/angular-10-role-based-authorization-tutorial-with-example)
* [1](https://www.freecodecamp.org/news/everything-you-need-to-know-about-ng-template-ng-content-ng-container-and-ngtemplateoutlet-4b7b51223691/), [2](https://dev.to/mustapha/angular-build-more-dynamic-components-with-ngtemplateoutlet-3nee) - `ngTemplateOutlet` и `ngTemplateOutletContext`, передача шаблона внутрь компонента
* [Angular Directive Functionality Lazy](https://netbasal.com/make-your-angular-directive-functionality-lazy-344a4bd9434b) - способ через дерективу применяет плагины на js к элементу, причем только к видимым (с отключением Angular Changes Detection чтобы не было лишних обработок)
* Angular Builder https://medium.com/angular-in-depth/angular-cli-under-the-hood-builders-demystified-v2-e73ee0f2d811

# Useful commands
## Tools
* Angular Console https://angularconsole.com/ - gui консольных команд
* https://angular.io/cli/generate
* https://update.angular.io/ - web страница помогает сделать update проекта на новую версию Angular
* https://angular.io/cli/new - список команд `ng new`
* https://angular.io/cli/build
* https://github.com/browserslist/browserslist#queries - управление полифилами для разных браузеров, можно исключить лишние и уменьшить размер

## angular
```bash
npm install -g @angular/cli                                         # установка
sudo npm upgrade --unsafe-perm -g @angular/cli                      # обновление
sudo npm i -g --unsafe-perm typescript tslint protractor web-ext    # зависимости

# обновление с игнорированием конфликта пакетов
# и игнорированием требования сделать git commit перед обновлением
# после обновления возможно нужно будет установить некоторые зависимости вручную,
# какие именно будет написано в консоли после обновления
ng update @angular/cli @angular/core --allow-dirty

# создание mini проекта, app нужно всегда указывать
# --minimal - создать без тестов
# --skip-git - не создавать git
# --skip-install - не устанавливать зависимости при создании
# --inline-style false - стили в отдельном файле
# --inline-template false - шаблоны в отдельном файле
# --routing - роутинг по умолчанию
ng new my-best-app --style=scss --routing --minimal --skip-git --skip-tests --inline-style false --inline-template false

# короткая версия mini проекта
ng n my-best-app --style=scss --routing --minimal -g -S -s false -t false --skip-install

ng generate component --skip-tests order-list   # генерация без тестов

# отключаем генерацию тестов для component
ng config schematics.@schematics/angular.component.spec false
ng config schematics.@schematics/angular.module.spec false
ng config schematics.@schematics/angular.service.spec false
ng config schematics.@schematics/angular.directive.spec false

# включаем генерацию файлов template и style для mini проекта (по умолчанию inline файлы)
ng config projects.my-app.schematics.@schematics/angular:component.inlineTemplate false
ng config projects.my-app.schematics.@schematics/angular:component.inlineStyle false

# отключить предупреждение что глобальная версия Angular выше локальной
# использоваться в этом случае будет локальная независимо от того включена или отключена опция
ng config -g cli.warnings.versionMismatch false

ng update --all # устарело, обновляет пакеты самого Angular
ng update       # обновляет пакеты самого Angular

# после указания ./ как пути можно будет открывать приложение локально
ng build --progress --base-href ./
```
***
```bash
### Создание Workspace с application:
ng new my-app --createApplication false --minimal   # создаем workspace
ng g application my-app --style=scss --routing --minimal    # создаем application
ng generate library foo-lib --prefix=foo    # создаем library если нужно
ng build foo-lib    # собираем library отдельно, --prod указывать нельзя
ng build my-app --prod  # собираем my-app, можно с --prod
ng serve my-app     # запуск
ng test foo-lib     # тесты
ng test my-app
ng generate component poster --project=foo-lib      # генерация component с указанием в каком application или library генерировать
cd dist/foo-lib && npm publish      # публикация lib в npm репе (сначало нужно создать аккаунт)
```
***
**Отключение строгой проверки типов и импортов**
```json
// tsconfig.json
{
  "compilerOptions": {
    "noImplicitAny": false
  }
}
```
***
**Отключение тестов вручную в angular.json**
```json
// Отключение тестов для всех в angular.json
// (если workspace пустой доп. атрибуты в этот файл добавятся после создания 1го приложения):
// (ЭТОТ КОНФИГ ДОБАВЛЯТЬ в КАЖДОЕ приложения workspace ИЛИ ГЛОБАЛЬНО для всех)
"schematics": {
        "@schematics/angular": {
            "component": {
                "spec": false
            },
            "module": {
                "spec": false
            },
            "service": {
                "spec": false
            }
        }
}
```
Создать общий каталог assets для всех проектов так (прим. различные библиотеки дополняющие Angular могут иметь эту фичу из коробки)
```json
// Создать общий каталог assets для всех проектов так:
// в /project создать каталог shared-assets и добавить в файл проекта (для КАЖДОГО под проекта):
"assets": [
    "projects/popup-app/src/favicon.ico",
    "projects/popup-app/src/assets",
        {
            "glob": "**/*",
            "input": "./libs/shared-assets/",
            "output": "./assets"
        }
    ],
```
***
Можно переопределить поведение для --prod в angular.json, опции ставить аналогичные тем что в projects.{project-name}.architect.build.options: `projects.{project-name}.architect.build.configurations.production`
***
Сокращения команд
```bash
ng g app # ng g application
ng g lib # ng g library

ng update
ng add

ng g class <name> [options]
ng g interface <name> <type> [options]
ng g library <name> [options]
ng g module <name> [options]
ng g pipe <name> [options]
ng g service <name> [options]
ng g serviceWorker [options]
ng g universal [options]
ng g guard <name> [options]
ng g enum <name> [options]
ng g directive <name> [options]
ng g application <name> [options]
ng g appShell [options]
ng g interceptor token

ng new [name] --minimal # генерирует проект без тестовых файлов и с inline шаблонами (на деле тестов нет только e2e, spec файлы при генерции всеравно есть; inline шаблон только для app компонента)
ng new [name] --style=scss # генерация проекта с scss вместо css
ng new [name] --routing # при создании добавиться routing модуль
ng new [name] --prefix=myapp # меняем prefix с app на что-то другой (нужно при разработки библиотек?)
ng new [name] --skip-git # не создавать git при создании проекта
ng generate module orders --routing
ng generate component orders/order-list
ng generate module heroes/heroes --module app --flat --routing

# не создавать приложение по умолчанию в workspace, чтобы использовать много приложений сгенерированных ng g application my-app
ng new my-app --createApplication false

# создаем с export для component из модуля (import делается по умолчанию)
ng generate component orders/order-list --skip-tests --export=true
```
# Angular life cycle

**Note.** Делать `implements OnInit` и прочие не обязательно, но это дает дополнительные type checking (т.е. TypeScript не хранит инфу о типах)

* **ngOnInit** - выполняется после связи поле с свойствами класса. В нем предлагается fetch data. Выполняется только 1 раз.
  * Использовать fetch data и некоторые subscribe в constructor - плохо.
  * constructor должен только инициализировать переменные.
  * ngOnChanges - вызывается после constructor, но перед ngOnInit (и еще много раз во время работы). В этом месте можно отловить первый раз когда input привязанные к поля устанавливаются.
* **ngOnDestroy** - выполняется до уничтожения directive (или компонета)
  * это место чтобы оповестить другие компоненты приложения, что данный компонент недоступен
  * это место для того чтобы почистить то, что не чистит сборщик мусора автоматически. Отписаться от Observables и DOM events, остановить interval timers. Unregister all callbacks that this directive registered with global or application services.
* **ngOnChanges(changes: SimpleChanges)** - вызывается как только произойдет изменение input у component (или directive)
  * при этом changes содержит инфу о измененных объектах
  * вызывается только для input связанных с полями класса. Если с полем класса связана ссылка на объект и свойство этого объекта изменено, то метод НЕ вызовется
* **ngDoCheck** - вызывается как только Angular заметит любое изменение, в том числе и ссылок на объект (и UI в том числе событий)
  * extends ngOnChanges
  * предлагается использовать его там, где ngOnChanges не подходит (там где Angular не детектит изменения)
  * внутри ngDoCheck нужно написать свою реализацию проверки изменилась ли ссылка (сравнения в if). Т.к. хотя он вызывается при любом изменении, но данных этих изменений не содержит)
  * ngDoCheck вызывается ОЧЕНЬ часто, на любое изменение (в том числе UI) поэтому его нужно использовать осторожно. Нужно использовать легкий код в нем.
* **ngAfterViewInit** - вызывается один раз когда инициализируется дочерний компонент @ViewChild (и directive?). 
  * Вызывается только при первом изменении component (его шаблона)?
  * `@ViewChild(ChildViewComponent) viewChild: ChildViewComponent;`
  * вызывается после первого ngAfterContentChecked
* **ngAfterViewChecked** - вызывается когда Angular закончил проверку изменений в component и его детях
  * вызывается после `ngAfterViewInit` и после каждого следующего `ngAfterContentChecked`
  * вызывается **ОЧЕНЬ часто**, в нем нужно писать свою логику сравнения на изменения (через if). Нужно использовать легкий код в нем иначе будут тормоза.

* **ngAfterViewInit и ngAfterViewChecked** - вызываются после того как шаблон сформирован (composed). unidirectional связь не дает сделать update после того как View was composed (т.е. нельзя менять уже сформированное View?). Поэтому нужно подождать (например вызовом tick) перед тем как обновить View, в пример это сделано так: `this.logger.tick_then(() => this.comment = c);`
  * если попытаться обновить View без задержки, то случиться ошибка

* **ngAfterContentInit** - вызывается после того как Angular вставляет внешние данные в component view или directive
  * вызывается 1 раз после **ngDoCheck**
  * **AfterContent** - вообще это проброс куска htm из родителя в дочерний компонент и только через него можно получить доступ к:
    ```ts
    @ContentChild(ChildComponent) contentChild: ChildComponent;
    <after-content>
    ```
* **ngAfterContentChecked** - вызывается после того как Angular проверил данные вставленные в component view или directive
  * вызывается после ngAfterContentInit и каждого следующего ngDoCheck
  * тут не нужна задержка как в ngAfterViewChecked, component обновляется сразу

**AfterContent** вызываются до **AfterView** - между вызовом AfterContent и AfterView есть "окно" в котором можно вызывать изменение view

# Общая структура

**Структура файлов проекта на Angular**
```
myproject
    src
        app
            core        - module that imports services, guards and so on to 'root' (app aware singletones)
                service     - services visible in across the app
                    api
                        base-api.service    - standard CRUD REST api
                        user-api.service
                        role-api.service
                    guard - check if role has rights to open a component
                    resolver - fetch data and wait until load end
                    auth.service - contain Subject with login state, any component can subscribe on it
                    init.service - init events in constructor
                interceptor
                    auth-interceptor - check jwt and add jwt to a request
                component   - components visible only in root module
                directive   - directive visible only in root module
                module      - module visible only in root module
            feature     - not a module, just a folder for feature modules: tab, tabs, product-list ...
            model       - ot a module, just a folder for models of business data: User, Item, Product
                user.ts
                role.ts
            shared          - shared directives, components, module. Each feature module import shared module
                directive   - shared directives
                    isAuth      - check if user is authorized to see a component
                    hasRole     - check if user has a role to see a component
                component   - shared components
                    Loader                  - show progress for a request (mostly for upload)
                    Spinner                 - show a spinner during a request
                    PageNotFoundComponent   - page for invalid urls
                module      - shared modules
                pipe
                    FilterByPredicatePurePipe - filter string input by predicate
            util            - various constants and util functions
```

- **Элементы приложения, которые нужно реализовать или установить в Angular для нормальной работы:**
  1. Standard Components
     1. **ModalComponent** - для всплывающих окон, и **ModalService** для загрузки туда данных
        1. [реализация через динамическое создание компонента](https://angular.io/guide/elements#example-a-popup-service)
        2. [реализация через outlet](https://angular.io/guide/router-tutorial-toh#displaying-multiple-routes-in-named-outlets)
        3. [реализация через сервис с установкой через npm](https://github.com/ngneat/dialog) и [lazy loading](https://netbasal.com/lazy-load-modal-components-in-angular-8cb54bba7bf7) загрузка dialog
     2.  **DialogComponent** и **DialogService** - стандартные диалоги
     3.  **AlertComponent** и **AlertService** - стандартные сообщения
     4.  **Tab** плагин - генерация табов
         1.  https://github.com/karanhudia/angular-tabs-component
     5. **SpinnerComponent** - для показа spinner вместо компонента пока тот загружается
     6. **LoaderComponent** - для показа loader вместо компонента данные для которого подгружаются через http, плюс показ прогресса загрузки
  2. Auth Security
     1. **IsAuthDerictive** - для скрытия/показа элементов незарегистрированным/зарегистрированным
        1. **AuthService** - отправляем подписанным **IsAuthDerictive** состояние
     2. **HasRoleDerictive** -  для скрытия/показа элементов имеющим/неимеющим роль
        1. **RoleService** - отправляет подписанным **HasRoleDerictive** роли
     3. **JwtInterceptor** - для прикрепления **jwt** к http запросам и обновления **jwt** по **refreshToken**
     4. **JwtService** - для получения jwt (входа) и его сохранения в `localStorage`, и для чтения jwt в **JwtInterceptor**
  3. Error Handler
     1. **PageNotFoundComponent** - для показа страницы ошибок, в т.ч. redirect сюда через interceptor (тут должны быть кнопки Back и Home)
     2. **ErrorInterceptor** - для redirect на страницу ошибок при http ошибках
  4.  **InitService** - точка входа, в его constructor запускаем работу некоторых элементов, например **AuthService**
  5.  `Resolver`'s - нужны т.к. `module`s создаются не сразу и нужно показывать какую-то анимацию до загрузки и отрисовки компонента
  6.  Анимация добавления новых элементов в array, чтобы не было дергания [тут](https://stackoverflow.com/a/61517726)
  7.  [cdkScrollable](https://material.angular.io/cdk/scrolling/overview) - виртуальный скролинг, отрисовывает только элементы до которых доскролили (те что влазят в height окна)
  8.  Подключить плагины анимации скролинга если нужно: [Locomotive Scroll](https://github.com/locomotivemtl/locomotive-scroll) или [Lax.js](https://github.com/alexfoxy/lax.js) etc
  9.  [ngx-infinite-scroll](https://github.com/orizens/ngx-infinite-scroll) - infinite scroll, загружает новые элементы когда скролинг достиг конца
  10. [breadcrumb](https://medium.com/@bo.vandersteene/angular-5-breadcrumb-c225fd9df5cf) - реализация breadcrumb
  11. Подключить [normalize.css](https://necolas.github.io/normalize.css/)  
         `npm install normalize.css` - устанавливаем
         ```css
         @import '~normalize.css'; /* самым первым в app/styles.css */
         ```
  12. **NGXS** или **Akira** или **NGRX** или **rx-angular** - это Redux для Angular, нужно чтобы не писать свой кэш когда одни и те же данные нужно нескольким соседним компонентам без общего предка.
  13. Доп. инструмент
      1.  [from-event](https://github.com/ngneat/from-event) - преобразует ViewChild and ContentChild в RxJS stream
      2.  [@UntilDestroy()](https://github.com/ngneat/until-destroy) - обертка вокруг destroy подхода для RxJs subsribers через `takeUntil`
      3.  [forms-manager](https://github.com/ngneat/forms-manager) - менеджер форм, позволяет хранить значение полей при переходе по страницам и возврате и при обновлении, при открытии и закрытии в localStorage
      4.  [transloco](https://github.com/ngneat/transloco) - удобное api для интернациализации
      5.  [cashew](https://github.com/ngneat/cashew) - http кэш
      6.  [error-tailor](https://github.com/ngneat/error-tailor) - удобное добавление и управление сообщениями об ошибках в Angular Forms
      7.  [Apollo Angular](https://www.apollographql.com/docs/apollo-server/) - генератор запросов для API на graphQL

# Создание фэйкового api

* [json-server](https://github.com/typicode/json-server) - для генерации api, в том числе поддерживает sort, paging etc
* [Faker.js](https://github.com/marak/Faker.js/) - для генерации фэйковых данных

# Components
* **Directive** - содержит логику, но не структуру
* **Component extends Directive** - содержит логику + может содержать много Directive (которые могут указывать на Component)
* **ПОЭТОМУ:** везде где использован Directive может быть использован и Component

1. Компонент принято всегда делать **export** из их модулей
2. Компонент не должен fetch или save данные напрямую, только через сервисы или предоставлять fake данные. Это должно работать через сервис.
3. template reference - это указатель на component найденный компилятором в шаблоне
4. `constructor(@Optional() private loggerService: LoggerService)` - @Optional() подавляет ошибку, если inject класса не существует, тогда значение будет null
5. emitDecoratorMetadata: true - стоит по умолчанию в tsconfig.json и отвечает за сохранение инфы о типах в TypeScript
6. `providers: [Logger] == [{ provide: Logger, useClass: Logger }]` - useClass указывает какой класс будет использован для inject, например если ссылка на абстрактных класс, то в useClass могут быть его разные реализации
   1. `[{ provide: Logger, useClass: Logger, deps: [Logger, UserService] }]` - одна из форм с зависимостями в deps
   2. `{ provide: OldLogger, useExisting: NewLogger}]` - useExisting в отличии от useClass создает связь не с классом, а с token. Если token не существует, то ошибка.
        ```ts
        providers: [
        {provide: B, useClass: A}, // T[B] => I[A]
        {provide: C, useExisting: B}] // T[C] => T[B] => I[A], связь идет с токеном B, хотя он не класс
        ```
   3. `[{ provide: AppConfig, useValue: HERO_DI_CONFIG })]` - useValue может inject уже созданные переменную, function, object, string напрямую
   4. Factory providers - создание зависимости во время работы.
      1. Нужно для:
          1. Выбора inject класса  во время работы. Например разные реализации UserSrv, для авторизованых и нет (чтобы не усложнять код сервиса авторизационной логикой)
          2. Для создания inject класса, которые не разрабатывался для DI
       1. Как использовать:
            ```ts
            export let heroServiceProvider = { 
                provide: HeroService,
                useFactory: (logger: Logger, userService: UserService) => new HeroService(logger, userService.user.isAuthorized),
                deps: [Logger, UserService]
            };
            @Component({
                selector: 'app-heroes',
                providers: [ heroServiceProvider ],
                template: `
                    <h2>Heroes</h2>
                    <app-hero-list></app-hero-list>
                `
            })
            export class HeroesComponent { }
            ```
7. Поиск injectors идет по иерархии: component => ngModule
8. `@Self` - поиск зависимости начинатется только с local injector (самого себя): `constructor(@Self() public dependency: Dependency)`
9.  `@Host` - поиск injector будет вестись пока не будет достигнут родительский component текущего injector: `constructor(@Host() public dependency: Dependency)`
10. `@SkipSelf()` - если пометить зависимости так `constructor(@SkipSelf() public dependency: Dependency)` то поиск зависимостей не будет идти в самом классе, а начнется с родителя
11. Еще один способ сделать inject, прямое создание (из контекста?)
    ```ts
    export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
    providers: [{ provide: APP_CONFIG, useValue: HERO_DI_CONFIG }]
    constructor(@Inject(APP_CONFIG) config: AppConfig) {} // тут обязательно использовать @Inject ???
    ```
12. Если DI зависимость не будет найдена, то значение DI переменной null
13. Интересный факт, можно добавить localStorage как зависимость
    ```ts
    export const BROWSER_STORAGE = new InjectionToken<Storage>('Browser Storage', {
        providedIn: 'root',
        factory: () => localStorage
    });
    @Injectable({
        providedIn: 'root'
    })
    export class BrowserStorageService {
        constructor(@Inject(BROWSER_STORAGE) public storage: Storage) {}
    }
    ```
14. Привязываем свойство класса к содержимому элемента: `<span [innerHTML]="title"></span>`
15. @Output
    1. EventEmitter используется в общении родительского и дочернего компонентов, для передачи значения из дочернего в родителя по событию.
        В некоторых статьях не советуют использовать его оборачивая в Observable и вообще не использовать ни для чего другого кроме общения родителя и дочернего компонентов.
    2. `@Output('myClick') clicks = new EventEmitter<string>();`
    3. @Directive({ //длинный вариант
        outputs: ['clicks:myClick']  // propertyName:alias
    })
    1. (без @Output)
        ```js
        <button (click)="onClickMe()">Click me!</button>
        export class ClickMeComponent { onClickMe() {} }
        ```
    2. (без @Output)
        <input (keyup)="onKey($event)">
         onKey(event: any) { this.values += event.target.value + ' | '; }
    3. Пояснение передачи event без @Output
       1.  Передавать event это сомнительная практика!
       2.  `onKey(event: KeyboardEvent) { //` можно так ограничить тип event, но работает не на всех типах event
            ```ts
            this.values += (<HTMLInputElement>event.target).value + ' | '; // приводим к тип event для input
            ```
       3. `<input #box (keyup)="0"> //привязываем 0 к событию, теперь минимальные требования по обработке события выполнены и каждое нажатие Angular будет обновлять компонент`
       4. `<input #box (keyup.enter)="onEnter(box.value)"> // работает только когда нажат Enter и передает только value`
16. Один из вариантов использования useExisting для inject его component
     1. Старый вариант с forwardRef: `providers: [{ provide: Parent, useExisting: forwardRef(() => AlexComponent) }],`
     2.  Более новый вариант поместить логику в функцию: `const provideParent = (component: any) => { provide: Parent, useExisting: forwardRef(() => component) };`
     3. А потом использовать: `providers:  [ provideParent(AliceComponent) ]`

# Directives
```html
<!-- for -->
<div *ngFor="let hero of heroes" />
<div *ngFor="let item of items; let i = index" />

<!-- since Angular 5 -->
<div *ngFor="let item of items; index as i" />

<!-- есть еще odd, even, last -->
<div *ngFor="let user of userObservable | async as users; index as i; first as isFirst" />

<div *ngFor="let item of items; index as i; trackBy: trackByFn" />

<!-- ng-template и старые вариант for: ngForOf -->
<ng-template ngFor let-item [ngForOf]="items" let-i="index" [ngForTrackBy]="trackByFn">
    <li>...</li>
</ng-template>

<!-- ngSwitch -->
<div [ngSwitch]="hero?.emotion">
    <app-happy-hero    *ngSwitchCase="'happy'"    [hero]="hero"></app-happy-hero>
    <app-sad-hero      *ngSwitchCase="'sad'"      [hero]="hero"></app-sad-hero>
    <app-confused-hero *ngSwitchCase="'app-confused'" [hero]="hero"></app-confused-hero>
    <app-unknown-hero  *ngSwitchDefault           [hero]="hero"></app-unknown-hero>
</div>

<!-- <select> -->
<select [(ngModel)]="hero">
    <ng-container *ngFor="let h of heroes">
        <ng-container *ngIf="showSad || h.emotion !== 'sad'">
        <option [ngValue]="h">{{h.name}} ({{h.emotion}})</option>
        </ng-container>
    </ng-container>
</select>

<!-- two-way binding -->
<input [(ngModel)]="hero.name">

<!-- старый вариант on-click="onSave()" -->
<li (click)="onSelect(hero)">

<!-- if -->
<div *ngIf="selectedHero"></div>

<div [class.selected]="true/false" />

<!-- если class уже объявлен, то [class] перезатирает его -->
<div class="bad curly special" [class]="badCurly" />

<!-- для нескольких классов -->
<div [ngClass]="{'extra-sparkle': isDelightful,'extra-glitter':isGlitter}" />
<div [ngClass]="['first', 'second']" />
<div [ngClass]="myClasses">

<!-- установка атрибута colspan=2 -->
<div [attr.colspan]="1 + 1" />

<!-- camelCase для style тоже валидно -->
<div [style.font-size.em]="isSpecial ? 3 : 1" />
<div [style.font-size.%]="!isSpecial ? 150 : 50" />
<div [ngStyle]="{'max-width.px': widthExp}" />
```

# Binding types
```
One-way
{{expression}}
[target]="expression"
bind-target="expression"

One-way
(target)="statement"
on-target="statement"

Two-way
[(target)]="expression"
bindon-target="expression"
```

# Фичи с TypeScript
```ts
let s = e!.name; // строгая проверка на null в TypeScript
```

# ViewChild, ContentChild, ViewChildren, ContentChildren

<small>Источник: [тут](https://medium.com/@tkssharma/understanding-viewchildren-viewchild-contentchildren-and-contentchild-b16c9e0358e) </small>

* `@ViewChild` - чтобы получить доступ к дочернему компоненту
* `@ContentChild` - чтобы получить доступ к переменной из дочернего компонента, когда сама переменная передана из родителя в дочерний.
  * в дочерний компоненту из родителя можно передать html (тип `ElementRef`) или directive (тип класса дерективы)
  * `@ContentChild(#name)` - по id
  * `@ContentChild(ChildDirective)` - по классу
* `@ViewChildren` - привязывает массив компонентов, QueryList as an observable collection, выполняется когда что-то добавляется или удаляется из коллекции
    @ViewChildren(Pane) panes !: QueryList<Pane>;
* `@ContentChildren` - тоже что и @ViewChildren только для @ViewChild

**Note.** Слово `static` используется для обратной совместимости со старым кодом в `@ViewChild`, хотя возможно что-то без него вообще работать не будет.

# `<ng-content>` vs `*ngTemplateOutlet`
`<ng-content>` удобнее и с ним меньше проблем, с `*ngTemplateOutlet` может появится ошибка `expression has changed after it was checked`, если данные изменяются уже после вставки в `ngTemplateOutlet`  
Но через `<ng-content>` нелья вывести в цикле `*ngFor` элементы с **одинаковым** селектором, в `*ngTemplateOutlet` это можно передавая ссылку на `TemplateRef`
```ts
// 1. Пример <ng-content>
@Component({ // 1. содержимое
  selector: 'app-content',
  template: `<ng-content></ng-content>`
})
export class ContentComponent {}

@Component({ // 2. контейнер
    selector: 'app-modal',
    template: `<ng-content select="app-content"></ng-content>`
})
export class ModalComponent {}

@Component({ // 3. использование
    selector: 'app-root',
    template: `<app-modal><app-content> blabla </app-content></app-modal>`
})
export class AppComponent {}
// ---------------------
// 2. Пример *ngTemplateOutlet
@Component({ // 1. содержимое, тут обязательно <ng-template> чтобы взять TemplateRef
  selector: 'app-content',
  template: `<ng-template #templateRef><ng-content></ng-content></ng-template>`
})
export class ContentComponent {
    @ViewChild('templateRef') public templateRef: TemplateRef<any>;
}

@Component({ // 2. контейнер
    selector: 'app-modal',
    template: `<ng-container *ngTemplateOutlet="content?.templateRef"></ng-container>`
})
export class ModalComponent implements AfterViewChecked {
    @ContentChild(ContentComponent) content: ContentComponent = null;
    constructor(private cdRef: ChangeDetectorRef) {}
    // hack fix, это плохо, нужно не всегда, в случае проблем в логах, есть лучшие способы решить это
    ngAfterContentInit() { this.cdRef.detectChanges(); }
}

@Component({ // 3. использование
    selector: 'app-root',
    template: `<app-modal><app-content> blabla </app-content></app-modal>`
})
export class AppComponent {}
```

# Providers

**providedIn** - лучший вариант потому что включает tree-shaking (очистку не используемых частей)
Инжектить сервисы через shared module плохо!  
**Note:** If you provide your service within a sub-module, it will by available application wide unless the sub-module is lazy loaded.

```ts
// Варианты использования
// 1. рекомендуется
@Injectable({providedIn: 'root'})
export class UserService {}

// 2. инжектим в конкретный класс (иначе сервис недоступен)
@Injectable({providedIn: UserModule})
export class UserService {}

// 3. старый способ (можно использовать в lazy модулях); переопределяет сервисы объявленные в providedIn?
@NgModule({providers: [UserService]})
export class UserModule {}

// 4. инжектим сервис только в конкретный компонент (можно использовать в lazy модулях)
//при этом service НЕ singleton и может использоваться как кэш (напр. если он нужен разным компонентам с разными сохраненными данными. В некоторых статьях советуют использовать такой подход в качестве кэша. Сервис уничтожится когда уничтожится компонент - экономия памяти?
@Component({providers: [UserService]})
export class UserComponent {}

// Использование: инжектим в конкретный класс (иначе сервис недоступен)
constructor(private heroService: HeroService) { }
```

# modules and components (Примеры кода) в Root Component

**Root Component** состоит из:
* **AppComponent** - объявлен в declarations и в bootstrap чтобы инжектится в index.html
* `@NgModule({` - УСЛОВНО модули делятся на 2 типа:
    1. с сервисами [+ компоненты] - импортируется в root (core module). Нельзя импортировать повторно в lazy модули.
    2. с компонентами - импортируются в каждый модуль (shared module). Если такой модуль импортирован повторно, то Angular авто очистит дубль (т.е. все ОК, так можно и это встречается например при импорте в core module (root) модуля shared)
* `declarations: [AppComponent]` - тут объявить все **components**, **directives** and **pipes** модуля. Каждый из них может быть объявлен только в одном declarations. Их нужно **re-export** потому что они **private scope**.
* **imports** - импортим модули чей функционал нужен (модули которые нужны в нескольких модулях, а не только конкретном засовываем в shared module и импортим его)
    ```ts
    imports: [ // импортим модули чей функционал нужен (модули которые нужны в нескольких модулях, а не только конкретном засовываем в shared module и импортим его)
        BrowserModule,
        FormsModule,
        HttpClientModule
    ],
    ```
* `providers: []`, - импорт модулей с компонентам (модули с сервисами provided в root), их НЕ нужно re-export потому что часто они **global scope** (root)
  - если это обычный module, то сервисы в нем доступны всему приложению?
  - если это lazy module, то сервисы доступны только в нем?
  - `forRoot()` - возвращает instance класса в обертке ModuleWithProviders (ngModule + provider) чтобы импортировать в core: components + services
  - `forChild()` - импортирует только components без сервисов (чтобы импортировать в components отличные от core без сервисов)
* **bootstrap** - AppComponent для запуска
    ```ts
    bootstrap: [AppComponent]
    })
    export class AppModule { }
    ```

# `entryComponents: []` (deprecated)
Теперь **deprecated**, Angular сам вычисляет может ли Component быть вызван и если да, то не очищает его импорт.

entryComponents (дополнить) - на практике объявляются при использовании сторонних библиотек, чтобы загрузить их компоненты.  
Это динамически добавляемые модули (во время выполнения программы) через ViewContainerRef.createComponent(). Свойство entryComponents говорит компилятору компилировать их и создать factories для них.

Если не добавить динамический component в entryComponents, то получится ошибка. Т.е. Angular не сможет скомпилить их.

- это компонент который грузится по требованию (imperatively) в DOM. Компонент который грузится через вставку его селектора в шаблон (declaratively) в DOM это НЕ entryComponents.
- AppComponent это entryComponents, потому что index.html это не шаблон (и следовательно загрузки компонентов по селектору через него нет)
- большинство случаев не требует entryComponents
- компонент в route definitions это entryComponents. Потому что route ссылается на компоненты по типу класса (а не селектору шаблона). Он загружается в RouterOutlet
- 2 типа entryComponents:
  - bootstrapped root component,
  - A component you specify in a route definition
- Свойство @NgModule.bootstrap загружает объявленное в нем как entryComponents (просто аналог entryComponents). Объявлять одни и те же components одновременно в @NgModule.bootstrap и entryComponents - плохо!
- другие виды entryComponents загружаются другими средствами, например через router.
- Компилятор сам добавляет "router component" в entryComponents из route или bootstrap. Если использовать ручную загрузку компонента, то придется добавить его в entryComponents
- загрузка компонента bootstrap вручную
    class AppModule implements DoBootstrap {
        ngDoBootstrap(appRef: ApplicationRef) {
            appRef.bootstrap(AppComponent); // Or some other component
        }
    }
- entryComponents нужны чтобы работал tree shaking, он удаляет все неиспользуемое и может удалить компоненты загруженные вручную (но то что в entryComponents не трогает) 

The entryComponents array is used to define only components that are not found in html and created dynamically with ComponentFactoryResolver. Angular needs this hint to find them and compile. All other components should just be listed in the declarations array.

# viewProviders

**viewProviders** - видны в component и его view child, но не видны в content child. А provider видны везде.  
  - нужно например чтобы ограничить инжекс сервисов из библиотек
```ts
@Component({
    viewProviders: [
        MySrv
    ],
})
```

# События инициализации (дописать!!!)

**Predefined tokens** - это предопределенные токены (имена: PLATFORM_INITIALIZER и прочие) для provider атрибута.
Их можно использовать для изменения поведения приложения.

```ts
// то есть эта штука объявляется на модуле и действует внутри него???
export const APP_TOKENS = [
    { provide: PLATFORM_INITIALIZER, useFactory: platformInitialized, multi: true    }, - callback загрузки приложенки
    { provide: APP_INITIALIZER, useFactory: delayBootstrapping, multi: true },  - callback перед инициализацией приложенки
    { provide: APP_BOOTSTRAP_LISTENER, useFactory: appBootstrapped, multi: true }, - callback загрузки каждого компонента
];
```
    
**NG_VALIDATORS** - используется для кастомного валидатора template driven form

Можно объявить много (multi) провайдеров (providers) для одного и того же токена.
Например так можно для NG_VALIDATORS объявить несколько кастомных валидаторов.
Для этого используется свойство multi: true

Пример (в заинжекшеном сервисе получим массив классов если имя одно и тоже):
```ts
providers: [
    { provide: 'SuperProvider', useClass: class A {}, multi: true },
    { provide: 'SuperProvider', useClass: class B {}, multi: true}]

constructor(@Inject('SuperProvider') private testInjection) {
```

**ROUTES** - тоже токен который как-то используется внутри, чтобы комбинировать sets of routes в одно значение.
    
В этом месте можно зарегистрировать кастомный валидатор форм NG_VALIDATORS (есть еще NG_ASYNC_VALIDATORS).
Для этого еще нужно `implements AsyncValidator`
```ts
@Directive({
selector: '[customValidator]',
providers: [{provide: NG_VALIDATORS, useExisting: CustomValidatorDirective, multi: true}]
})
class CustomValidatorDirective implements Validator {
validate(control: AbstractControl): ValidationErrors | null {
    return { 'custom': true };
}
```

# HostListener, HostBinding
Источник: [тут](https://angular.io/api/core/HostListener), [тут](https://angular.io/api/core/HostBinding)

**HostListener** - для связи СОБЫТИЙ с методами дерективы

**HostBinding** - для связи со СВОЙСТВАМИ (т.е. свойствами объекта js привязанного к тегу в DOM) компонента на котором проставлена деректива (т.е. их изменения)
Они работают для Directive и Component

Если из HostListener `return false;` то это тоже что и `preventDefault`

```ts
@Directive({
    selector: '[tohValidator]'
})
export class ValidatorDirective {
    @HostBinding('attr.role') role = 'button';
    @HostBinding('class.pressed') isPressed: boolean;
    @HostBinding("style.cursor") get getCursor(){ return "pointer"; }
    @HostListener('mouseenter') onMouseEnter() {
        // do work
    }
    @HostListener('mousedown') hasPressed() {
        this.isPressed = true;
    }
}

// длинный вариант
@Directive({
    selector: '[tohValidator2]',
    host: {
        '[attr.role]': 'role',
        '(mouseenter)': 'onMouseEnter()'
    }
})
export class Validator2Directive {
    role = 'button';
    onMouseEnter() {
        // do work
    }
}

// Передача параметров через @HostListener:
@HostListener('click', ['$event.target'])
onClick(btn) { console.log(btn); }
```

# Inject DOM Element
```ts
@Directive({
selector: '[appHighlight]'
})
export class HighlightDirective {
private el: HTMLElement;

constructor(el: ElementRef) {
    this.el = el.nativeElement;
}
```

# Изменение DOM (ViewContainerRef)

Источник: [тут](https://indepth.dev/exploring-angular-dom-manipulation-techniques-using-viewcontainerref/)

[ViewContainerRef](https://angular.io/api/core/ViewContainerRef) - используется для скрытия или показа компонентов по условию (полученному например через subscribe). Может создавать компоненты.

Для чего: например показать/скрыть элемент для определенной роли пользователя, или показать/скрыть spinner component пока не завершилась работа resolver.

Пример использования: [тут](https://angular.io/guide/dynamic-component-loader)

```ts
// 1. показываем/скрываем component
constructor(private templateRef: TemplateRef<any>, private viewContainerRef: ViewContainerRef) {}

ngOnInit() {
    if (isAuthed) {
        // показываем содержимое (или возвращает после удаления)
        this.viewContainerRef.clear();
        this.viewContainerRef.createEmbeddedView(this.templateRef);
    } else {
        this.viewContainerRef.clear(); // удаляем содержимое
    }
}

// 2. создаем component
@ViewChild(AdDirective, {static: true}) adHost: AdDirective; // берем component по directive
const componentFactory = this.componentFactoryResolver.resolveComponentFactory(adItem.component);
const viewContainerRef = this.adHost.viewContainerRef;
viewContainerRef.clear();
const componentRef = viewContainerRef.createComponent<AdComponent>(componentFactory); // создаем
componentRef.instance.data = adItem.data; // устанавливаем созданному component данные

```

# Angular CDK
Источник: [тут](https://material.angular.io/cdk/categories)

Это официальный набор доп. инструментов для разработки из Angular Material, но которые можно использовать отдельно.

**Для чего:** например встроенный инструмент создания overlay

```
npm install --save @angular/cdk
```

# Router

Router. Общий процесс, когда маршруты добавляются создается набор сервисов и компонентов, который можно использовать через DI  
**import route module** дожен быть последним, если точнее перед module на который он действует. Это нужно потому что иерархия роутеров такая же как у модулей, иначе совпадет не тот путь.

- **Жизненный цикл маршрута:**
  - Parse: разбирает  сроку запроса
  - Redirect: выполняет перенаправление (если нужно)
  - Identify: определяет стейт
  - Guard: запускает гарды конкретного стейта
  - Resolve: резолвит данные стейта
  - Activate: активирует компонент
  - Manage: слушает изменения, чтобы повторить процесс сначала

- **Router events - события которые выполняются при проходе жизненного цикла маршрута:** (это не полный список без описания, полный тут https://angular.io/guide/router )
  - NavigationStart
  - RouteConfigLoadStart
  - RouteConfigLoadEnd
  - GuardsCheckStart
  - ChildActivationStart
  - ActivationStart
  - GuardsCheckEnd
  - ResolveStart
  - NavigationCancel
  - NavigationError
  - Scroll
  - ...
  - **Для чего:** например для показа loader или spinner при открытии страницы и его прятании при завершении навигации или работы resolver. Пример реакций на событие подгрузки инфы тут https://gitlab.com/service-work/is-loading

- **ParamMap API**, `params.get('id')`
  - has(name)
  - get(name)
  - getAll(name) - массив значений параметра с одним именем
  - keys - массив имен параметров
  - **Note.** Если при переходе component уже активирован, то он будет переиспользован

**вот так параметры можно брать** только если component никогда не будет переиспользован (на него никто не перейдет повторно),
    иначе нужно подписываться на ActivatedRoute и брать параметр оттуда.
    Почему?  https://angular.io/guide/router#snapshot-the-no-observable-alternative
```ts
// 1.
ngOnInit() {
    // ДОСТАЕМ ПАРАМЕТР СПОСОБ 1
    // этот id не изменится даже при повторном переходе в тот же component, использовать осторожно
    let id = this.route.snapshot.paramMap.get('id');
}

// 2.
// берем параметр из Route
// При этом switchMap при переходе по другому маршруту этого же component завершает
// предыдущий запрос в service отменяя старый, если старый запрос все еще весит
// (это его отличие от flatMap). Видимо (!) тут завершается действие предыдущего слушателя события?
ngOnInit() {
    // ДОСТАЕМ ПАРАМЕТР СПОСОБ 2
    this.hero$ = this.route.paramMap.pipe(
        switchMap((params: ParamMap) =>
            this.service.getHero(params.get('id')))
            // подписка тут т.к. ngOnInit активируется 1 раз, а подписка на paramMap вечна
            // (напр при переиспользовании component, когда мы переходим по одному и тому же component,
            // но с разными данными, переоткрываем)
    );
}
```

**matrix URL notation** - это когда параметры разделены ;  
`localhost:4200/heroes;id=15;foo=foo`

Router маршруты принято выносить в отдельный модуль.  
В простых приложенках можно оставить routers в component

baseUrl в tsconfig.json задает корень для абсолютного пути, еще можно указать его командой Angular при сборке

`pathMatch: 'full'` - только полное совпадение ссылки  
`pathMatch: 'prefix'` - выберется первый url префикс которого совпадет

Запоминать позицию скролинга при возврате Назад. Работает только со static страницам.  
Для запоминания динамического скролинга можно использовать ViewportScroller для управления скроллом: https://stackoverflow.com/a/51713404
```ts
RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled'
})
```

**ExtraOptions** - объект параметров передающийся в RouterModule.forRoot(routes, { СЮДА }  
несколько примеров ниже

```ts
// Включаем скролинг к якорю при переходе.
@NgModule({
imports: [
    RouterModule.forRoot(routes, {
    scrollPositionRestoration: 'enabled',
    anchorScrolling: 'enabled', // включаем скроллинг к якорю при переходе (для скролинга нужно передать название якоря в параметре?)
    scrollOffset: [0, 64] // [x, y] //смещение относительноя якоря к которому скролим
    })
],
exports: [RouterModule]
})
export class AppRoutingModule {}
```

**NavigationExtras** - объект содержащий разные параметры передаваемые при переходе вручную.  
(якорь в Angular называют fragment или anchor)
```ts
let navigationExtras: NavigationExtras = {
    queryParams: { page: 2 }, // просто передача параметра /results?page=2
    fragment: 'anchor', // якорь в html к которому будет скролинг
    { preserveQueryParams: true }, // сохраняет параметры при переходе: /results?page=1 to /view?page=1
    { preserveFragment: true }, // сохраняет якорь html при переходе
    { skipLocationChange: true }, //навигация не попадает в историю браузера
    { replaceUrl: true }, //навигация с перезаписью истории браузера
    queryParamsHandling: "merge", //установка стратегии обработки параметры, merge объединяет текущие с новыми: from /results?page=1 to /view?page=1&page=2
    { relativeTo: this.route } // переход будет относительно адреса
};

// Navigate to the login page with extras
this.router.navigate(['/login'], navigationExtras);
```

Способы скролинга к якорю (авто переход к якорю будет выполнен только если установить `anchorScrolling: 'enabled'` в Routers):
```html
<a [routerLink]="['somepath']" fragment="Test">Jump to 'Test' anchor </a>
this._router.navigate( ['/somepath', id ], {fragment: 'test'});
```

```ts
// Пример Router
const appRoutes: Routes = [
    { path: 'crisis-center', component: CrisisListComponent },
    { path: 'hero/:id',      component: HeroDetailComponent },
    {
        path: 'heroes',
        component: HeroListComponent,
        data: { title: 'Heroes List' } //передача данных
    },
    { path: '',
        redirectTo: '/heroes', //переадресация
        pathMatch: 'full' //только полное совпадение ссылки, нужно для пустой строки
    },
    { path: '**', component: PageNotFoundComponent }
];

@NgModule({
imports: [
    RouterModule.forRoot(
        appRoutes,
        { enableTracing: true } // <-- debugging purposes only
    )
    // other imports here
],
...
})
export class AppModule { }

// Пример 2 (с дочерними компонентами)
const crisisCenterRoutes: Routes = [
{
    path: 'crisis-center',
    component: CrisisCenterComponent,
    children: [
    {
        path: '',
        component: CrisisListComponent,
        children: [
        {
            path: ':id',
            component: CrisisDetailComponent,
            canDeactivate: [CanDeactivateGuard],
            resolve: {
            crisis: CrisisDetailResolverService
            }
        },
        {
            path: '',
            component: CrisisCenterHomeComponent
        }
        ]
    }
    ]
}
];

@NgModule({
imports: [
    RouterModule.forChild(crisisCenterRoutes)
],
exports: [
    RouterModule
]
})
export class CrisisCenterRoutingModule { }

// Простой пример
const routes: Routes = [
    {
        path: '',
        component: CustomerListComponent
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CustomersRoutingModule { }
```

* `[routerLinkActive]` vs `routerLinkActive` - 
* `[routerLink]` vs `routerLink` - первый для массива, второй для строки адреса
  - `[routerLink]="[{ outlets: { popup: ['compose'] } }]"`
  - `[routerLink]="['/hero', hero.id]"`
  - `routerLink="/sidekicks"`
```html
<!-- Переход по маршрутам -->
<a routerLink="/crisis-center" routerLinkActive="active">Crisis Center</a>
<a [routerLink]="[{ outlets: { popup: ['compose'] } }]">Contact</a>
<router-outlet #routerOutlet="outlet"></router-outlet>
<router-outlet name="popup"></router-outlet>
<a routerLink="/user/bob" routerLinkActive="active-link" [routerLinkActiveOptions]="{exact: true}">Bob</a>
<a routerLink="/user/bob" routerLinkActive="class1 class2">Bob</a>
<a routerLink="/user/bob" [routerLinkActive]="['class1', 'class2']">Bob</a>
<a routerLink="/user/bob" routerLinkActive #rla="routerLinkActive">
    Bob {{ rla.isActive ? '(already open)' : ''}}
</a>
<div routerLinkActive="active-link" [routerLinkActiveOptions]="{exact: true}">
    <a routerLink="/user/jim">Jim</a>
    <a routerLink="/user/bob">Bob</a>
</div>
<a [routerLink]="['/hero', hero.id]">
```

* **Навигация вручную**
  - `this.router.navigate` vs ...
  - `this.router.navigate(['/heroes', { id: heroId, foo: 'foo' }]);` - с передачей параметров
  - `this.router.navigate(['../', { id: crisisId, foo: 'foo' }], { relativeTo: this.route });` - относительно другой ссылки
  - `this.router.navigate([{ outlets: { popup: null }}]);` - чистка outlet ставя в null
  - `this.router.navigateByUrl(`${my}/user`);` - навигация по ссылке в виде строки, а не массива
  - Чтобы вернувшись в MyListComponent из MyDetailComponent увидеть выделанным выбранный пункт нужно при навигации передать параметр на основе которого выделить элемент:
    ```ts
    this.router.navigate(['/heroes', { id: heroId, foo: 'foo' }]); // берем id и выделяем в списке по id
    ```

- **Классы rout** - когда роутинг завершается формируется дерево объектов ActivatedRoute. Их можно инжектить через Route сервис
    1. **Router.events** - содержит много событий по навигации, роутингу, скролингу. Эти события логируются, если `enableTracing == true`
    2. **ActivatedRoute** - Инжектится в component. Имеет кучу параметров url, params, outlet, children (child routes), routeConfig и т.д. Этот класс можно не делать **unsubscribe** в деструкторе component, он одно из исключений. Чистится сам. Вреда от отписки нет, если хочется можно отписаться вручную.
        ```ts
        @Component({...})
        class MyComponent {
            constructor(route: ActivatedRoute) {}
        }
        2.
        export class Routable2Component implements OnInit {
            constructor(private activatedRoute: ActivatedRoute) {}
        
            ngOnInit() {
                this.activatedRoute.url
                    .subscribe(url => console.log('The URL changed to: ' + url));
            }
        }
        ```
    3. **ActivatedRouteSnapshot** - router.snapshot
    4. **RouterState** - router.routerState
    5. **router.routerState**
    6. **Router** - инжектится в компонент (еще тут техника, ловим событие в constructor, а подписываемся в onInit)
        ```ts
        @Component({})
        export class Routable1Component implements OnInit {

        navStart: Observable<NavigationStart>;

        constructor(private router: Router) {
            // Create a new Observable the publishes only the NavigationStart event
            this.navStart = router.events.pipe(
                filter(evt => evt instanceof NavigationStart)
            ) as Observable<NavigationStart>;
        }

        ngOnInit() {
            this.navStart.subscribe(evt => console.log('Navigation Started!')); //пример подписки на событие старта router
        }
        ```
    7. **Location** - рекомендуется использовать Router, Location рекомендуется только для normalized URLs outside of routing.
        ```ts
        // Как я понял этот сервис взаимодействует с самим браузером.
        // Кроме того содержит разные методы для работы с url, обрезать, соединять параметры и т.д.
        export class PathLocationComponent {
            location: Location;
            constructor(location: Location) { this.location = location; }
        }
        ```
    8. **Анимация в при роутинге** (ДОПИСАТЬ!!!)
            https://angular.io/guide/router#adding-routable-animations
        ```ts
        // Подключаем модуль
        @NgModule({
            imports: [
                BrowserAnimationsModule,
            ],
        })
        ```

- **Lazy routing** - 

# `DomSanitizer` и отображение `data:base64` в `<img>` по `onchange`
Чтобы защитить приложение все значение устанавливаются через обертку, это может сломать например установку `data:base64` в `url=`. Единственный способ использовать - отключить эту защиту.
```ts
constructor(private sanitizer: DomSanitizer) {
  this.trustedUrl = sanitizer.bypassSecurityTrustUrl('data:base64...'); // can use in template now
}
```

# Angular Change Detection
Источник: [1](https://habr.com/ru/company/infopulse/blog/358860/), [2](https://blog.angular-university.io/how-does-angular-2-change-detection-really-work/)

# Change Detection. React vs. Angular
Источник: [1](https://www.fulcircle.io/posts/react_vs_angular_part_1/), [2](https://www.fulcircle.io/posts/react_vs_angular_part_2/)

# Решение ошибки `expression has changed after it was checked`
Источник: [1](https://stackoverflow.com/questions/39787038/how-to-manage-angular2-expression-has-changed-after-it-was-checked-exception-w)

Запуск детекци вручную. Статья о том что это и как правильно это исправлять [тут](https://blog.angular-university.io/angular-debugging/)
```ts
import { ChangeDetectorRef } from '@angular/core';

constructor(private cdRef: ChangeDetectorRef) {}

// или
ngOnInit() {
  this.cdRef.detectChanges();
}

// или
ngAfterViewChecked() {
  this.cdRef.detectChanges();
}

// или
ngAfterContentInit() {
  this.cdRef.detectChanges();
}

// или, если нужно менять каждый раз при вводе, например при input
ngOnChanges() {
  this.cdRef.detectChanges();
}
// или
ngDoCheck {
  this.cdRef.detectChanges();
}
```

# Ссылки на новые фичи в Angular
* https://blog.ninja-squad.com/2017/11/02/what-is-new-angular-5/
* https://blog.ninja-squad.com/2017/12/07/what-is-new-angular-5.1/
* https://blog.ninja-squad.com/2018/01/11/what-is-new-angular-5.2/
* https://blog.ninja-squad.com/2018/05/04/what-is-new-angular-6/
* https://blog.ninja-squad.com/2018/07/26/what-is-new-angular-6.1/
* https://blog.ninja-squad.com/2018/09/07/angular-cli-6.2/
* https://blog.ninja-squad.com/2018/10/18/what-is-new-angular-7/
* https://blog.ninja-squad.com/2018/10/19/angular-cli-7.0/
* https://blog.ninja-squad.com/2018/11/22/what-is-new-angular-7.1/
* https://blog.ninja-squad.com/2018/11/27/angular-cli-7.1/
* https://blog.ninja-squad.com/2019/01/07/what-is-new-angular-7.2/
* https://blog.ninja-squad.com/2019/01/09/angular-cli-7.2/
* https://blog.ninja-squad.com/2019/01/31/angular-cli-7.3/
* https://blog.ninja-squad.com/2019/05/29/angular-cli-8.0/
* https://blog.ninja-squad.com/2019/05/29/what-is-new-angular-8.0/
* https://blog.ninja-squad.com/2019/07/03/angular-cli-8.1/
* https://blog.ninja-squad.com/2019/07/03/what-is-new-angular-8.1/
* https://blog.ninja-squad.com/2019/08/01/angular-cli-8.2/
* https://blog.ninja-squad.com/2019/08/01/what-is-new-angular-8.2/
* https://blog.ninja-squad.com/2019/08/22/angular-cli-8.3/
* https://blog.ninja-squad.com/2020/02/07/angular-cli-9.0/
* https://blog.ninja-squad.com/2020/02/07/what-is-new-angular-9.0/
* https://blog.ninja-squad.com/2020/03/26/what-is-new-angular-9.1/
* https://blog.ninja-squad.com/2020/03/26/angular-cli-9.1/
* https://blog.ninja-squad.com/2020/06/25/what-is-new-angular-10.0/
* https://blog.ninja-squad.com/2020/06/25/angular-cli-10.0/
* https://blog.ninja-squad.com/2020/09/03/what-is-new-angular-10.1/
* https://blog.ninja-squad.com/2020/09/03/angular-cli-10.1/
* https://blog.ninja-squad.com/2020/10/22/what-is-new-angular-10.2/
* https://blog.ninja-squad.com/2020/11/11/angular-cli-11.0/
* https://blog.ninja-squad.com/2020/11/11/what-is-new-angular-11.0/
* https://blog.ninja-squad.com/2021/01/21/angular-cli-11.1/
* https://blog.ninja-squad.com/2021/01/21/what-is-new-angular-11.1/
* https://blog.ninja-squad.com/2021/02/11/angular-cli-11.2/
* https://blog.ninja-squad.com/2021/02/11/what-is-new-angular-11.2/
* https://blog.ninja-squad.com/2021/05/12/angular-cli-12.0/
* https://blog.ninja-squad.com/2021/05/12/what-is-new-angular-12.0/
* https://blog.ninja-squad.com/2021/06/25/angular-cli-12.1/
* https://blog.ninja-squad.com/2021/06/25/what-is-new-angular-12.1/
* https://blog.ninja-squad.com/2021/08/04/angular-cli-12.2/
* https://blog.ninja-squad.com/2021/08/04/what-is-new-angular-12.2/
* https://blog.ninja-squad.com/2021/11/03/angular-cli-13.0/
* https://blog.ninja-squad.com/2021/11/03/what-is-new-angular-13.0/
* https://blog.ninja-squad.com/2021/12/10/angular-cli-13.1/
* https://blog.ninja-squad.com/2021/12/10/what-is-new-angular-13.1/
* https://blog.ninja-squad.com/2022/01/27/angular-cli-13.2/
* https://blog.ninja-squad.com/2022/01/27/what-is-new-angular-13.2/
* https://blog.ninja-squad.com/2022/03/16/what-is-new-angular-13.3/
* https://blog.ninja-squad.com/2022/06/02/angular-cli-14.0/
* https://blog.ninja-squad.com/2022/06/02/what-is-new-angular-14.0/
* https://blog.ninja-squad.com/2022/07/21/angular-cli-14.1/
* https://blog.ninja-squad.com/2022/07/21/what-is-new-angular-14.1/