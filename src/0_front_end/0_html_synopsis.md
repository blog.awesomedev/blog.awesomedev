Источник https://github.com/bendc/frontend-guidelines

# Entity символы
Entity символы (напр. в CSS) - те которые нужно экранировать

`«<»` и `«&»` будут представлены как `«&lt;»` и `«&amp;»`

# CDATA - character data

Это текстовые данные в XML. Не нужно экранировать `<`, `&` etc.
```
<![CDATA[Within this Character Data block I can]]>
```

# Основные теги

div, span, a, html, head, body, li, ul, ol, button, input
- div
- span
- a
- html
- head
- body
- video
- audio

# table

- table
- thead
- tbody
- tr
- td
- th

# meta

# doctype