Источник https://github.com/bendc/frontend-guidelines

# Основа

https://www.w3schools.com/css/css_border.asp

https://www.w3schools.com/css/css_background.asp

https://www.w3schools.com/css/css_margin.asp

https://www.w3schools.com/css/css_padding.asp

https://www.w3schools.com/css/css_outline.asp

https://www.w3schools.com/css/css3_colors.asp

https://www.w3schools.com/css/css_pseudo_classes.asp

https://www.w3schools.com/css/css_pseudo_elements.asp

# Основа. Специфика

https://www.w3schools.com/css/css_combinators.asp

https://www.w3schools.com/css/css_attribute_selectors.asp

https://www.w3schools.com/css/css_units.asp

https://www.w3schools.com/css/css_specificity.asp

https://www.w3schools.com/css/css3_mediaqueries.asp

# Основа. Позиция и отображение

https://developer.mozilla.org/ru/docs/Web/CSS/box-sizing

https://www.w3schools.com/css/css_positioning.asp

https://www.w3schools.com/css/css_display_visibility.asp

https://www.w3schools.com/css/css_inline-block.asp

https://developer.mozilla.org/en-US/docs/Web/CSS/vertical-align

https://www.w3schools.com/css/css_overflow.asp

https://www.w3schools.com/css/css_image_transparency.asp

https://www.w3schools.com/css/css_align.asp

https://www.w3schools.com/css/css_float.asp

# Основа. Текст

https://www.w3schools.com/css/css_font.asp

https://www.w3schools.com/css/css3_fonts.asp

https://developer.mozilla.org/en-US/docs/Web/CSS/text-align

https://developer.mozilla.org/en-US/docs/Web/CSS/text-align-last

https://developer.mozilla.org/ru/docs/Web/CSS/word-break

https://developer.mozilla.org/ru/docs/Web/CSS/text-justify

https://developer.mozilla.org/en-US/docs/Web/CSS/overflow-wrap

https://developer.mozilla.org/en-US/docs/Web/CSS/writing-mode

https://developer.mozilla.org/en-US/docs/Web/CSS/text-overflow

https://developer.mozilla.org/en-US/docs/Web/CSS/user-select

https://developer.mozilla.org/ru/docs/Web/CSS/line-break

# Основа. Элементы

https://www.w3schools.com/css/css_link.asp

https://www.w3schools.com/css/css_form.asp

https://www.w3schools.com/css/css_table.asp

https://www.w3schools.com/css/css_list.asp

https://developer.mozilla.org/en-US/docs/Web/CSS/cursor

# Дополнительно
https://developer.mozilla.org/en-US/docs/Web/CSS/user-modify

https://developer.mozilla.org/en-US/docs/Web/CSS/column-count

https://developer.mozilla.org/en-US/docs/Web/CSS/pointer-events

# Дополнительно. Эффекты

https://www.w3schools.com/css/css3_gradients.asp

https://developer.mozilla.org/en-US/docs/Web/CSS/linear-gradient

https://developer.mozilla.org/ru/docs/Web/CSS/radial-gradient

https://www.w3schools.com/css/css3_shadows.asp

https://developer.mozilla.org/en-US/docs/Web/CSS/text-shadow

https://www.w3schools.com/css/css3_text_effects.asp

https://www.w3schools.com/css/css3_2dtransforms.asp

https://www.w3schools.com/css/css3_3dtransforms.asp

https://developer.mozilla.org/ru/docs/Web/CSS/border-radius

https://www.w3schools.com/css/css3_transitions.asp

https://www.w3schools.com/css/css3_object-fit.asp

https://www.w3schools.com/css/css3_animations.asp

https://www.w3schools.com/css/css3_border_images.asp

# Пример layout с inline-block

# Flexbox
Источник: [1](https://developer.mozilla.org/ru/docs/Learn/CSS/CSS_layout/Flexbox), [2](https://developer.mozilla.org/ru/docs/Web/CSS/flex)

```css
container {
  display: flex;  /* вкдвключаем flex */
  display: inline-flex ;  /* делает контейнер блочным */
  flex-direction: column; /* расположить элементы по столбцам; или row */
  flex-wrap: wrap; /* не вылезать за контейнер если нет места */
  flex-flow: row wrap; /* сокращение для flex-direction и flex-wrap */
  align-items: center; /* как выровнять элементы: слева, центр, право; как выравнивание по горизонтали для column */
  justify-content: space-around; /* авто растановка пространства вокруг элементов, как выравнивание по вертикали для column */
  justify-content: start; /* или end, также сдвигает элементы влево-вправо, похоже на align-items */
}

child {
  flex-grow: 2; /* сколько единиц (по ширине для column и высоте для row) занимает элемент, единицы рассчитываются автоматически */
  flex-shrink: 2; /* фактор сжатия элемента если он не помещается в контейнер */
  flex-basis: 200px; /* 200px - min значение (по ширине для column и высоте для row) */
  flex: 2; /* аналог: flex-grow, flex-shrink, flex-basis */
  flex: 2 200px;
  align-self: flex-end; /* как выровнять элемент игнорируя align-items: слева, центр, право */
  order: 1; /* позиция */
}
```

# Grid

https://developer.mozilla.org/ru/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout

# Flexbox vs Grid
Источник: [1](https://developer.mozilla.org/ru/docs/Learn/CSS/CSS_layout/Flexbox)

**Правило:** Если нужно расположить элементы в один ряд (вертикально или горизонтально) - выбирать flexbox. Если в сразу двух направлениях (сетки разных типов в том числе несимметричные) - выбирать grid.

# Cross browser CSS & normalize.css

https://necolas.github.io/normalize.css/

# Особенности работы с CSS в js
пусто

# Online generators

* gradiante generator
  * https://www.colorzilla.com/gradient-editor/
  * https://cssgradient.io/
* flexbox generator
  * https://the-echoplex.net/flexyboxes/
  * https://loading.io/flexbox/
  * https://www.cssportal.com/css-flexbox-generator/
* CSS Grid Generator
  * https://grid.layoutit.com/
  * https://cssgrid-generator.netlify.app/
* Box Shadow CSS Generator
  * https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Background_and_Borders/Box-shadow_generator
  * https://html-css-js.com/css/generator/box-shadow/
  * https://www.cssmatic.com/box-shadow
*  Text Shadow CSS Generator
   * https://html-css-js.com/css/generator/text-shadow/
   * https://css3gen.com/text-shadow/
*  Border CSS Generator
   * https://cssgenerator.org/border-css-generator.html
*  Border Radius CSS Generator
   *  https://cssgenerator.org/border-radius-css-generator.html
*  Transform CSS Generator
   *  https://cssgenerator.org/transform-css-generator.html
* Color & Converter CSS Generator
  * https://cssgenerator.org/rgba-and-hex-color-generator.html

