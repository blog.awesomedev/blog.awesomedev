# IDE
**IDE** (Integrated development environment) - среда разработки.

* **IDE** - what it can do
  * **text editor**
    * **intelligent code completion** (code auto completion, e.g. `IntelliSense` from **VSCode**)
    * **static code analyzer, linter** (e.g. like separate `SonarlLint`, `JSLint`, `ESLint`, `TSLint` etc) - builtin tool, check a code for errors, bad style, security issues by sets of rules that are used to build a graph by code to check it (graph like **state machine** or **tree structure** of rules). **Note.** linter is often **static code analyzer** with additional checking (**e.g.** looking for dependencies, calculating metrics etc)
      * **code quality analysis**
      * **code style analysis**
      * **code security analysis**
      * **external code analyzer plugins** - can use external tools like `SonarlLint`, `TSLint` etc
    * **dynamic code analysis** - available not for all languages and runtime environments, check errors that can be revealed at runtime only: memory leaks, uninitialized accesses, concurrency issues, undefined behavior situations etc
    * **code assistance**
      * **intelligent code completion** - auto code completion
      * **sanitizers** - modify data to make it safe and/or usable
        * **e.g.** auto escape quotes on copy/paste event etc, escaping SQL
      * **code hints** - better or alternative variants
      * **code generating** - generate getters/setters/hashCode/equals/clone/copy/toString etc
      * **code templates** (snippets)
      * **semantic coloring** and **highlights**
      * **working with documentation** (auto documenting, auto loading)
      * **navigation by code**
      * **managing code imports**
      * **marks in a code** - `// TODO`, `// FIXME`, other special comments, `code bookmarks`
      * search by all code including dependencies' libraries
      * **code comments generating** - can generate comments according a programming language code style
        * **Note.** Also can generate special **documentation comments** to let **generate code documentation** by code comments
    * **spell checking** - spell check code considering **camelCase** style and warnings about non **ASCII** (non latin) letters. Also has spell checking for multi languages word for **string constants** and **property files**.
  * **compiler** & **interpreter** - to compile & run
  * **dependency management** - analysis & gui by using an internal or external tool, dependencies cycling fixing, dependencies version conflict fixing, dependencies graph
  * **build automation** (e.g. maven, ant, make)
    * **Note.** Use its own builtin plugins or separated tools depends on configuration and IDE.
  * **debugger** & **remote debugger**
  * **live edit** - auto build & deploy on code change
  * **hot swap** - compile & auto run changed code only on the fly (**e.g.** compile only changed `Class`)
  * **disassembler** - get original code from binary files.
    * **Note.** IDE can use its own decompilation library (like **Idea** for Java).
    * **Note.** For non binary **minified** files (e.g. JavaScript) it can **deminify** and **format code**. Also it can use `.map` files to show **original JavaScript file** (by virtually joining JavaScript file and its `.map` file).
  * **VCS** (Version Control System, **e.g.** `git`) - GUI and console for **VCS**es and command shorthands
  * **console** - has a built in console with an advance functionality, various console tabs for logging, running, working with **DB**, **VCS** etc
  * **database tools** - GUI for working with DBs, DB driver auto downloading, executing, DB graphic schema 
  * **xml/xsd/dtd tools** - validate & edit xml, xsd, dtd; generate xsd by xml, xml by xsd; generate classes by xsd
  * **OS-level virtualization containers tools** - GUI for Docker & Kubernetes
  * **application servers tools** - GUI for running by **servers** and **application servers**: change parameters, start/stop, logging, hot swap etc
  * **testing tools** - run unit & integration tests all or some, **code coverage** tool show what code was covered by tests, tests' exploring (convenient GUI for test results)
  * **jvm tools** - analysis of jvm parameters and statistic, GUI with memory and CPU consumption
  * **Framewroks' tools** - special tools for various Framewroks (e.g. end points GUI for Spring MVC, JavaFX editor etc)
  * **API automated testing tools** - to test **REST**, **SOAP** etc API (send, receive, batch operation, auto compare results)
    * **Note.** It is like **SoapUI** or **Postman** tools