# VSCode
## Awesome Visual Studio Code
* https://github.com/viatsko/awesome-vscode

## General
- [Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker) - проверка орфографии
  - [Russian - Code Spell Checker](https://marketplace.visualstudio.com/items?itemName=streetsidesoftware.code-spell-checker-russian)
  - **Note.** Просто установить недостаточно, нужно указать в настройках проверку `en,ru` для обоих языков и увеличить max количество найденных ошибок до `9999999`
- [Window Colors](https://marketplace.visualstudio.com/items?itemName=stuart.unique-window-colors) - каждое окно имеет свой цвет
- [title](https://marketplace.visualstudio.com/items?itemName=nilpatel.title) - можно присвоить свое имя окну
  - **Note.** Присвоить имя можно только открытому каталогу. Имя должно иметь большую длину.
- [Activitus Bar](https://marketplace.visualstudio.com/items?itemName=Gruntfuggly.activitusbar) - боковая панель кнопок в статус баре
  - **Note.** Не находит кастомные кнопки добавленные другими extensions
- [Prettier - Code formatter](https://github.com/prettier/prettier-vscode) - дополнительный набор форматирования для разных языков
- [Rainbow CSV](https://github.com/mechatroner/vscode_rainbow_csv) - подсветка CSV
- [XML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml) - errors info, dtd/xsd validation, xsl, autocomplete, renaming etc
- [XML Tools](https://marketplace.visualstudio.com/items?itemName=DotJoshJohnson.xml) - форматирование, XQuery, XPath, minifying
- [SQLTools](https://marketplace.visualstudio.com/items?itemName=mtxr.sqltools) - набор инструментов для sql: форматирование, подключение, генератор SQL запросов insert
- [YAML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-yaml)
- **Git**
  - [GitLens](https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens)
  - [Git History](https://marketplace.visualstudio.com/items?itemName=donjayamanne.githistory)
  - [Git Graph](https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph)
- **Markdown**
  - [Github Markdown Preview](https://marketplace.visualstudio.com/items?itemName=bierner.github-markdown-preview) - стиль github
  - [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) - много разного, некоторые горячие клавиши нужно настроить вручную, для gitlab нужно изменить тип генерируемого TOC или может не работать 
  - [Markdown PDF](https://marketplace.visualstudio.com/items?itemName=yzane.markdown-pdf) - авто конвертация при сохранении в html или pdf
  - [Markdown Shortcuts](https://marketplace.visualstudio.com/items?itemName=mdickin.markdown-shortcuts) - то чего нет в Markdown All in One, например авто преобразование в список при выделении строк через меню

## Configuration
```json
{
    "editor.minimap.enabled": false,
    "workbench.activityBar.visible": false,
    "telemetry.enableCrashReporter": false,
    "telemetry.enableTelemetry": false,
    "markdown.extension.toc.slugifyMode": "gitlab",
    "markdown.extension.print.imgToBase64": true,
    "markdown.extension.preview.autoShowPreviewToSide": true,
    "cSpell.language": "en,ru",
    "window.zoomLevel": -1,
    "markdown.extension.katex.macros": {

    },
    "cSpell.enabled": true,
    "extensions.ignoreRecommendations": true,
    "workbench.colorTheme": "Default Light+",
    "markdown.extension.tableFormatter.normalizeIndentation": true,
    "workbench.startupEditor": "newUntitledFile",
    "editor.suggestSelection": "first",
    "editor.acceptSuggestionOnCommitCharacter": false,
    "editor.acceptSuggestionOnEnter": "off",
    "security.workspace.trust.enabled": false,
    "extensions.supportUntrustedWorkspaces": {

    },
    "security.workspace.trust.untrustedFiles": "open",
    "editor.fontLigatures": false,
    "update.mode": "manual",
    "update.showReleaseNotes": false,
    "extensions.autoUpdate": false,
    "editor.bracketPairColorization.enabled": true,
    "editor.guides.bracketPairs": true,
    "editor.guides.highlightActiveIndentation": true,
    "cSpell.checkLimit": 9999999,
    "cSpell.blockCheckingWhenTextChunkSizeGreaterThan": 9999999,
    "cSpell.blockCheckingWhenLineLengthGreaterThan": 9999999,
    "cSpell.maxNumberOfProblems": 9999999,
    "cSpell.minWordLength": 3,
    "json.schemas": [
    

    ]
}
```
