# IntelliJ IDEA
* **plugins**
  * [Big Data Tools](https://plugins.jetbrains.com/plugin/12494-big-data-tools)
  * lombok
  * Rainbow Brackets
  * Rainbow CSV
  * Indent Rainbow
  * MapStruct Support
  * GitToolBox
  * JPA Buddy
  * SonarLint
  * Maven Helper - показывает конфликты зависимостей, Dependency Analyzer
    * **Note.** Появится вкладка Dependency Analyzer в pom.xml файлах. Для exclude конфликтов: правой кнопкой мыши по красной надписи конфликты -> `exclude`.
  * liquibase - может делать changeSet из существующей БД (прим. на практике нужно проверить нет ли проблем и его обычно не используют)
  * Grep Console - подсветка синтаксиса в логах консоли и поиск по reg exp
  * **bitucket**
    * Bitbucket Linky
    * Bitbucket Pull Requests
  * **Gerrit**
    * Gerrit
  * [Diffblue Cover](https://plugins.jetbrains.com/plugin/14946-diffblue-cover) - авто генерация классов тестов
* **управление**
  * ctrl + f - поиск по стр.
  * ctrl + r - поиск и замена по стр.
  * ctrl + shift + f - поиск по коду всего проекта или указанных каталогов (напр. выбранных в панели файлов)
  * ctrl + shift + r - поиск и замена по коду всего проекта или указанных каталогов (напр. выбранных в панели файлов)
  * ctrl + n - поиск по именам классов
  * ctrl + shift + n - поиск по именам классов
  * ctrl + i - на названии класса, это имплементировать методы даже default
  * ctrl + shift + u - переключение регистра выделенной строки
  * shift + shift - поиск по всему включая библиотеки
  * если вместо f в сочетания для поиска нажимать r - то откроется диалог поиска и замены (вместо только поиска для f)
  * shift + alt - и кликание по тексту установит много кореток сразу, чтобы редактировать несколько линий одновременно
  * tab - нажатые после ввода имени тега создаст сам этот тег
  * home - переход к началу строки
  * end - переход к концу строки
  * ctrl+space - выбор из всех вариантов функций, если выделить название функции, то замена на выбранную
  * alt+enter на классе - заглушки-реализации методов
  * alt+enter на подсказке idea - исправление предупреждение или выбор вариантов того же кода (e.g. stream api or loop)
  * нажатие на значек возле абстрактного метода перебросит к реализации метода
  * Alt+Insert - вставляет настроенный в Idea текст **Copyright**, если DevOps настроен так что не пропускает commit без него, то без него будет ошибка
  * ctrl + click - по классу или переменной - все места использования
    * Alt+F7 - по классу или переменной - все места использования в подробном виде в отдельной панели
  * Ctrl+Shift+F9 - hot swap открытого java файла
  * ctrl+k - commit
  * ctrl+alt+k - commit+push
  * ctrl+alt+h - на методе, граф кто вызывает метод и какие методы вызывает этот метод (Callers & Callee)
  * ctrl+h - на классе, граф наследования вверх или вниз
* **Настройка**
  * Отключить чтобы не нарушало целостность файлов
    * `Settings → Editor → General → Ensure line feed at file end on Save`
  * Если добавить в конфигурацию запуска war файл приложения и путь к серверу, то idea сможет использовать hot swap при котором перезагружаться будут только отдельные измененные методы, а не все приложение. Но новые методы так создавать нельзя, только менять готовые. Работает не со всеми серверами приложений java.
  * Конфигурирование заголовков файлов (Files Header)
    1. Вставить шаблон
        ```
        /**
        * ${NAME}.
        *
        * @author ${USER}
        */
        ```
    2. Если необходимо переопределить имя пользователя, то переходим в **Help | Edit custom VM options**
        ```
        -Duser.name=My_Name
        ```
        Перезагружаем intellij idea, после чего применится новое имя
  * Увеличиваем размер логов в консоли idea (чтобы новые не затирали старые)
    * `Preferences/Settings -> Editor -> General -> Console`, check the box next to Override console cycle buffer size, set to a larger number than 1024 KB. При этом авто перенос строк в консоли может тормозить приложение на длинных логах.
  * `Build, Execution, Deployment > Build Tools > Maven > importing` - включить загрузку исходников и документации
  * `Settings | Editor | Code Style | Java | Imports | Import Layout`
    * class count to use `*` - отключить, поставив 32000
    * names count to use static import `*` - отключить, поставив 32000
    * import layout - в окне отдельно указать группу импортов пакета своего проекта
  * `File | Settings | Build, Execution, Deployment | Compiler | Annotation Processors` - включить процессор аннотаций
  * Editor | File Encoding - по default обычно везде UTF-8, но если нет, то можно поменять
  * Editor | File Encoding | Default encoding for properties files - установить UTF-8 в случае проблем с кодировкой, но вообще UTF-8 обычно стандарт